<?php
/**
* Zoylo_AdminUserRole - Version 1.0.0
* Copyright © 2019 Zoylo All rights reserved.
* Author : Ajeshbabu Gowrisetti
*/
namespace Zoylo\AdminUserRole\Observer;
class Saverolename implements \Magento\Framework\Event\ObserverInterface
{
  /**
   * @var observer
   * Update userRoleName column with role name when save or updating
  */
  public function execute(\Magento\Framework\Event\Observer $observer)
  {
     $user = $observer->getEvent()->getObject();
     $userId = $user->getId();
     if($userId!=''){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	$userFactory = $objectManager->get('\Magento\User\Model\User');
        $user = $userFactory->load($userId);
        $role = $user->getRole();
        $roleData = $role->getData();
        //$roleName = $roleData['role_name'];
        if(isset($roleData['role_name']) && $roleData['role_name']!='') {
          $roleName = $roleData['role_name'];
          $user->setUserRoleName($roleName);
          $user->save();
        }
        //$user->load($userId);
        
     }   
  }
}