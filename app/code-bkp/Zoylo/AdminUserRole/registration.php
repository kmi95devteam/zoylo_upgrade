<?php
/**
* Zoylo_AdminUserRole - Version 1.0.0
* Copyright © 2019 Zoylo All rights reserved.
* Author : Ajeshbabu Gowrisetti
*/
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Zoylo_AdminUserRole',
    __DIR__
);