<?php

namespace Zoylo\Csquare\Block\Adminhtml\OnHoldOrders;

use Magento\Framework\AuthorizationInterface;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended {

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;
    protected $_reasons;
    protected $_zoyloskugroup;

    /**
     * Authorization object.
     *
     * @var AuthorizationInterface
     */
    protected $_authorization;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Zoylo\Csquare\Model\OnHoldReportFactory $onholdOrdersFactory
     * @param \Zoylo\Csquare\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Backend\Helper\Data $backendHelper, \Zoylo\Csquare\Model\OnHoldReportFactory $onholdOrdersFactory, \Zoylo\Csquare\Model\Reasons $reasons, \Zoylo\Csquare\Model\Product\Attribute\Source\ZoyloSkuGroup $zoyloskugroup, \Magento\Framework\Module\Manager $moduleManager, array $data = []
    ) {
        $this->_onholdOrdersFactory = $onholdOrdersFactory;
        $this->_reasons = $reasons;
        $this->_zoyloskugroup = $zoyloskugroup;
        $this->moduleManager = $moduleManager;
        $this->_authorization = $context->getAuthorization();
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection() {
        $collection = $this->_onholdOrdersFactory->create()->getCollection();

        $collection->getSelect()
                ->joinLeft(['cpe' => $collection->getTable('catalog_product_entity')], 'main_table.product_sku = cpe.sku', ['product_id' => 'cpe.entity_id'])
                ->joinLeft(['ev1' => $collection->getTable('eav_attribute')], 'ev1.attribute_code = "ucode_id"', ['ucode_attribute_id' => 'ev1.attribute_id'])
                ->joinLeft(['cpev1' => $collection->getTable('catalog_product_entity_varchar')], 'cpe.entity_id = cpev1.entity_id AND cpev1.attribute_id = ev1.attribute_id', ['ucode_id' => 'cpev1.value'])
                ->joinLeft(['ev2' => $collection->getTable('eav_attribute')], 'ev2.attribute_code = "name" AND ev2.entity_type_id = 4', ['name_attribute_id' => 'ev2.attribute_id'])
                ->joinLeft(['cpev2' => $collection->getTable('catalog_product_entity_varchar')], 'cpe.entity_id = cpev2.entity_id AND cpev2.attribute_id = ev2.attribute_id', ['name' => 'cpev2.value'])
                ->joinLeft(['ev3' => $collection->getTable('eav_attribute')], 'ev3.attribute_code = "zoylo_sku_group"', ['product_type_attribute_id' => 'ev3.attribute_id'])
                ->joinLeft(['cpev3' => $collection->getTable('catalog_product_entity_varchar')], 'cpe.entity_id = cpev3.entity_id AND cpev3.attribute_id = ev3.attribute_id', ['product_type' => 'cpev3.value'])
                ->joinLeft(['ev4' => $collection->getTable('eav_attribute')], 'ev4.attribute_code = "pack_size"', ['pack_size_attribute_id' => 'ev4.attribute_id'])
                ->joinLeft(['cpev4' => $collection->getTable('catalog_product_entity_varchar')], 'cpe.entity_id = cpev4.entity_id AND cpev4.attribute_id = ev4.attribute_id', ['pack_size' => 'cpev4.value']);

        $collection->getSelect()->group('main_table.product_sku');
        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns() {
        /* $this->addColumn(
          'id',
          [
          'header' => __('ID'),
          'type' => 'number',
          'index' => 'id',
          'header_css_class' => 'col-id',
          'column_css_class' => 'col-id',
          'name' => 'id'
          ]
          ); */

        $this->addColumn(
                'product_sku', [
            'header' => __('SKU ID'),
            'index' => 'product_sku',
            'class' => 'xxx',
            'name' => 'product_sku'
                ]
        );

        $this->addColumn(
                'ucode_id', [
            'header' => __('UcodeId'),
            'index' => 'ucode_id',
            'class' => 'xxx',
            'name' => 'ucode_id',
            //	'filter' => false,
            'sortable' => false
                ]
        );

        $this->addColumn(
                'name', [
            'header' => __('Product Name'),
            'index' => 'name',
            'class' => 'xxx',
            'name' => 'name',
            //	'filter' => false,
            'sortable' => false
                ]
        );

        $this->addColumn(
                'pack_size', [
            'header' => __('Pack Size'),
            'index' => 'pack_size',
            'class' => 'xxx',
            'name' => 'pack_size',
            //	'filter' => false,
            'sortable' => false
                ]
        );

        $this->addColumn(
                'product_type', [
            'header' => __('Product Type'),
            'index' => 'product_type',
            'class' => 'xxx',
            'name' => 'product_type',
            'sortable' => false,
            'type' => 'options',
            'options' => $this->_zoyloskugroup->getOptionArray()
                ]
        );

        $this->addColumn(
                'order_ids', [
            'header' => __('Total Order Count'),
            'index' => 'order_ids',
            'class' => 'xxx',
            'name' => 'order_counts',
            'sortable' => false,
            'filter' => false,
            'renderer' => 'Zoylo\Csquare\Block\Adminhtml\OnHoldOrders\Renderer\OrderCount'
                ]
        );
        $this->addColumn(
                'order_id', [
            'header' => __('Current Orders On Hold'),
            'index' => 'order_id',
            'class' => 'xxx',
            'name' => 'order_id',
            'sortable' => false,
            'filter' => false,
            'renderer' => 'Zoylo\Csquare\Block\Adminhtml\OnHoldOrders\Renderer\OrderIds'
                ]
        );

        $this->addColumn(
                'due_to_price', [
            'header' => __('Onhold due to Price'),
            'index' => 'due_to_price',
            'class' => 'xxx',
            'name' => 'due_to_price',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'Zoylo\Csquare\Block\Adminhtml\OnHoldOrders\Renderer\DueToPrice'
                ]
        );

        $this->addColumn(
                'due_to_quantity', [
            'header' => __('Onhold due to Quantity'),
            'index' => 'due_to_quantity',
            'class' => 'xxx',
            'name' => 'due_to_quantity',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'Zoylo\Csquare\Block\Adminhtml\OnHoldOrders\Renderer\DueToQuantity'
                ]
        );

        $this->addColumn(
                'both', [
            'header' => __('Onhold due to Both(Price and Quantity)'),
            'index' => 'both',
            'class' => 'xxx',
            'name' => 'both',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'Zoylo\Csquare\Block\Adminhtml\OnHoldOrders\Renderer\DueToBoth'
                ]
        );

        $this->addColumn(
                'reason', [
            'header' => __('Reason'),
            'index' => 'reason',
            'class' => 'xxx',
            'name' => 'reason',
            'type' => 'options',
            'options' => $this->_reasons->getOptionArray()
                ]
        );

        $this->addColumn(
                'diff_price', [
            'header' => __('CSquare MRP'),
            'index' => 'diff_price',
            'class' => 'xxx',
            'name' => 'diff_price',
            'filter' => false,
            'sortable' => false
                ]
        );

        $this->addColumn(
                'diff_qty', [
            'header' => __('Diff Qty'),
            'index' => 'diff_qty',
            'class' => 'xxx',
            'name' => 'diff_qty',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'Zoylo\Csquare\Block\Adminhtml\OnHoldOrders\Renderer\Quantity'
                ]
        );

        $this->addColumn(
                'created_date', [
            'header' => __('First Occurrence'),
            'index' => 'created_date',
            'type' => 'datetime',
            'name' => 'created_date'
                ]
        );

        $this->addExportType('*/*/exportOnHoldReportCsv', __('CSV'));
        $this->addExportType('*/*/exportOnHoldReportExcel', __('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl() {
        return $this->getUrl('csquare/*/grid', ['_current' => true]);
    }

    /**
     * Add column filtering conditions to collection
     *
     * @param \Magento\Backend\Block\Widget\Grid\Column $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column) {

        if ($this->getCollection()) {
            $field = $column->getFilterIndex() ? $column->getFilterIndex() : $column->getIndex();
            if ($column->getFilterConditionCallback()) {
                call_user_func($column->getFilterConditionCallback(), $this->getCollection(), $column);
            } else {
                $condition = $column->getFilter()->getCondition();
                if ($field && isset($condition)) {

                    if ($field == "ucode_id") {
                        $this->getCollection()->addFieldToFilter('cpev1.value', $condition);
                    } elseif ($field == "name") {
                        $this->getCollection()->addFieldToFilter('cpev2.value', $condition);
                    } elseif ($field == "product_type") {
                        $this->getCollection()->addFieldToFilter('cpev3.value', $condition);
                    } elseif ($field == "pack_size") {
                        $this->getCollection()->addFieldToFilter('cpev4.value', $condition);
                    } elseif ($field == "reason") {
                        if (in_array(3, $condition)){ // 1 is for quantity , 2 is for price, 3 is for both
                            $this->getCollection()->addFieldToFilter(
                             array('attribute' => 'reason', 'eq' => 'reason'),
                             array('attribute' => '1', 'eq' => '2')
                             );
                        }else{
                            $this->getCollection()->addFieldToFilter('reason', $condition);
                        }
                    } else {
                        $this->getCollection()->addFieldToFilter($field, $condition);
                    }
                }
            }
        }
        //echo $this->getCollection()->getSelect();
        return $this;
    }

}
