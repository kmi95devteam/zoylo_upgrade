<?php

namespace Zoylo\Csquare\Block\Adminhtml\OnHoldOrders\Renderer;

use Magento\Framework\DataObject;

class Quantity extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
   

    /**
     * get order Id
     * @param  DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
		$data = $row->getData();
		$sku = $data['product_sku'];
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$collection = $objectManager->create('\Zoylo\Csquare\Model\OnHoldReport')
									->getCollection()
									->addFieldToFilter('product_sku', $sku);
		$qty = 0;
		foreach($collection as $data){
			 $qty+= $data['diff_qty'];
		}
		if ($qty>0){
            return $qty;
        }else {
            return false;
        }
        
    }
}