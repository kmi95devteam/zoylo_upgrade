<?php

namespace Zoylo\Csquare\Block\Adminhtml\OnHoldOrders\Renderer;

use Magento\Framework\DataObject;

class OrderIds extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
   

    /**
     * get order Id
     * @param  DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
		$data = $row->getData();
		$total_orderids = $data['product_sku'];
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$collection = $objectManager->create('\Zoylo\Csquare\Model\OnHoldReport')
									->getCollection()
									->addFieldToFilter('product_sku', $total_orderids);
		
		foreach($collection as $data){
			 $orderids[] = $data['order_id'];
			 
		}
		//echo "<pre>";
		//print_r($orderids);die;
		$List = implode(', ', $orderids); 
		return $List;
        
		
		
		
    }
}