<?php

namespace Zoylo\Csquare\Block\Adminhtml\OnHoldOrders\Renderer;

use Magento\Framework\DataObject;

class DueToBoth extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
   

    /**
     * get order Id
     * @param  DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
		$qtyReason = array();$priceReason = array();$both = array();
		$data = $row->getData();
		$sku = $data['product_sku'];
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$collection = $objectManager->create('\Zoylo\Csquare\Model\OnHoldReport')
									->getCollection()
									->addFieldToFilter('product_sku', $sku);
		
		foreach($collection as $data){
			if($data['reason'] == 1){
				$qtyReason[] = $data['reason'];
			}elseif($data['reason'] == 2){
				$priceReason[] = $data['reason'];
			}elseif($data['reason'] == 3){
			   $both[] = $data['reason'];
			}
		}

		if (count($both)>0){
           return count($both);
        } else {
           return false;
        }
        
    }
}