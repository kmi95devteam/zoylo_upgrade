<?php
namespace Zoylo\Csquare\Block\Adminhtml\Order;
use Magento\Framework\AuthorizationInterface;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Zoylo\Csquare\Model\OrdersFactory
     */
    protected $_productsFactory;

    protected $_status;

    /**
     * Authorization object.
     *
     * @var AuthorizationInterface
     */
    protected $_authorization;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Zoylo\Csquare\Model\OrdersFactory $ordersFactory
     * @param \Zoylo\Csquare\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Zoylo\Csquare\Model\OrdersFactory $ordersFactory,
        \Zoylo\Csquare\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_ordersFactory = $ordersFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        $this->_authorization = $context->getAuthorization();
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_ordersFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'name'=>'id'
            ]
        );
        $this->addColumn(
            'source_order_id',
            [
                'header' => __('Source Order Id'),
                'index' => 'source_order_id',
                'class' => 'xxx',
                'name'=>'source_order_id'
            ]
        );
		$this->addColumn(
            'target_order_id',
            [
                'header' => __('Target Order Id'),
                'index' => 'target_order_id',
                'class' => 'xxx',
                'name'=>'target_order_id'
            ]
        );
		
		$this->addColumn(
            'fullfilment_status',
            [
                'header' => __('Fullfilment Data'),
                'index' => 'fullfilment_status',
                'class' => 'xxx',
                'name'=>'fullfilment_status'
            ]
        );
		$this->addColumn(
            'csquare_invoice_id',
            [
                'header' => __('Csquare Invoice Id'),
                'index' => 'csquare_invoice_id',
                'class' => 'xxx',
                'name'=>'csquare_invoice_id'
            ]
        );
        $this->addColumn(
            'response',
            [
                'header' => __('Response/Reason'),
                'index' => 'response',
                'class' => 'xxx',
                'name'=>'response'
            ]
        );

        $this->addColumn(
            'magento_order_status',
            [
                'header' => __('Magento Order Status'),
                'index' => 'magento_order_status',
                'class' => 'xxx',
                'name'=>'magento_order_status'
            ]
        );

        $this->addColumn(
            'updated_by',
            [
                'header' => __('Updated By'),
                'index' => 'updated_by',
                'class' => 'xxx',
                'name'=>'updated_by'
            ]
        );

                
        $this->addColumn(
            'created_at',
            [
                'header' => __('Created date'),
                'index' => 'created_at',
                'type' => 'datetime',
                'name'=>'created_at'
            ]
        );

		$this->addColumn(
            'updated_at',
            [
                'header' => __('Updated date'),
                'index' => 'updated_at',
                'type' => 'datetime',
                'name'=>'updated_at'
            ]
        );
		$this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'index' => 'status',
                'type' => 'options',
                'name'=>'status',
				'options' => $this->_status->getOptionArray()
            ]
        );
                
		 $this->addColumn('action', array(
                'header' => __('Action'),
                'width' => '100',
				'filter' => false,
                'renderer'  => 'Zoylo\Csquare\Block\Adminhtml\Order\Renderer\Url',
              
        ));


        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        
        $this->setMassactionIdField('order_report');
        //$this->getMassactionBlock()->setTemplate('Zoylo_Csquare::form/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('order_report_ids');

        $aclResource = 'Zoylo_Csquare::order_report_delete';
        if ($this->_authorization->isAllowed($aclResource) === true) {
            $this->getMassactionBlock()->addItem(
                'delete',
                [
                    'label' => __('Delete'),
                    'url' => $this->getUrl('csquare/*/massDelete'),
                    'confirm' => __('Are you sure?')
                ]
            );
        }

        $aclResource = 'Zoylo_Csquare::order_report_change_status';
        if ($this->_authorization->isAllowed($aclResource) === true) {
            $statuses = $this->_status->getOptionArray();

            array_unshift($statuses, ['label' => '', 'value' => '']);
            $this->getMassactionBlock()->addItem(
                'status',
                [
                    'label' => __('Change Status'),
                    'url' => $this->getUrl('csquare/*/massStatus', ['_current' => true]),
                    'additional' => [
                        'visibility' => [
                            'name' => 'status',
                            'type' => 'select',
                            'class' => 'required-entry',
                            'label' => __('Status'),
                            'values' => $statuses
                        ]
                    ]
                ]
            );
        }

        $aclResource = 'Zoylo_Csquare::manual_sync';
        if ($this->_authorization->isAllowed($aclResource) === true) {
            $this->getMassactionBlock()->addItem(
                'Manual Sync',
                [
                    'label' => __('Manual Sync'),
                    'url' => $this->getUrl('csquare/*/massManualSync')
                ]
            );
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('csquare/*/grid', ['_current' => true]);
    }
}