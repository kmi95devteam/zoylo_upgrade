<?php

namespace Zoylo\Csquare\Block\Adminhtml\Quality;

use Magento\Framework\AuthorizationInterface;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended {

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Zoylo\Csquare\Model\OrdersFactory
     */
    protected $_productsFactory;
    protected $_status;

    /**
     * Authorization object.
     *
     * @var AuthorizationInterface
     */
    protected $_authorization;
    protected $_orderCollectionFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Zoylo\Csquare\Model\OrdersFactory $ordersFactory
     * @param \Zoylo\Csquare\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Backend\Helper\Data $backendHelper, \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory, \Zoylo\Csquare\Model\Status $status, \Magento\Framework\Module\Manager $moduleManager, array $data = []
    ) {
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        $this->_authorization = $context->getAuthorization();
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection() {
        $collection = $this->_orderCollectionFactory->create()->addAttributeToSelect('*')->addFieldToFilter('status', 'ready_for_packing_cs');

        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns() {
        $this->addColumn(
             'qc_status', [
            'header' => __('QC Status'),
            'index' => 'qc_status',
            'class' => 'xxx',
            'name' => 'qc_status',
            'type' => 'options',
            'options' => array("Failed" => "QC Failed", "pending" => "QC Pending", "" => "QC Pending")
                ]
        );
        $this->addColumn(
            'increment_id', [
            'header' => __('Order Id'),
            'index' => 'increment_id',
            'class' => 'xxx',
            'name' => 'increment_id'
                ]
        );
        $this->addColumn(
                'target_order_id', [
            'header' => __('Target Order Id'),
            'index' => 'target_order_id',
            'class' => 'xxx',
            'name' => 'target_order_id'
                ]
        );
        $this->addColumn(
                'origin', [
            'header' => __('Origin'),
            'index' => 'origin',
            'class' => 'xxx',
            'name' => 'origin'
                ]
        );
        $this->addColumn(
                'qc_user', [
            'header' => __('QC user verified'),
            'index' => 'qc_user',
            'class' => 'xxx',
            'name' => 'qc_user'
                ]
        );
        $this->addColumn(
                'created_at', [
            'header' => __('Purchased Date'),
            'index' => 'created_at',
            'class' => 'xxx',
            'type' => 'datetime',
            'name' => 'created_at'
                ]
        );
        $this->addColumn(
                'updated_at', [
            'header' => __('Last Modified Date'),
            'index' => 'updated_at',
            'class' => 'xxx',
            'type' => 'datetime',
            'name' => 'updated_at'
                ]
        );
        $this->addColumn('comments', array(
            'header' => __('Comments'),
            'width' => '100',
            'renderer' => 'Zoylo\Csquare\Block\Adminhtml\Order\Renderer\Comments',
        ));
        $this->addColumn('action', array(
            'header' => __('Action'),
            'width' => '100',
            'renderer' => 'Zoylo\Csquare\Block\Adminhtml\Order\Renderer\Url',
        ));



        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl() {
        return $this->getUrl('csquare/*/grid', ['_current' => true]);
    }
	
protected function _addColumnFilterToCollection($column)
    {
	
        if ($this->getCollection()) {
            $field = $column->getFilterIndex() ? $column->getFilterIndex() : $column->getIndex();
            
            if ($column->getFilterConditionCallback()) {
                call_user_func($column->getFilterConditionCallback(), $this->getCollection(), $column);
            } else {
                $condition = $column->getFilter()->getCondition();

                if ($field && isset($condition)) {
					if($field == "qc_status"){ 
					
						if(in_array('Failed',$condition)){
							$this->getCollection()->addFieldToFilter('qc_status', $condition);
						}elseif(in_array('pending',$condition)){
							$this->getCollection()->addFieldToFilter('qc_status', array('null' => true));
						}else{
							$this->getCollection()->addFieldToFilter('qc_status', $condition);
						}
						
                        return $this;
					}else{ 
					
						$this->getCollection()->addFieldToFilter($field, $condition);
                        return $this;
						
					}
                }else{
                    if($field == "qc_status"){
						$this->getCollection()->addFieldToFilter('qc_status', array('null' => true));
                        return $this;
					}
                } 
            }
        }
        
        
    }
	
	

}
