<?php
namespace Zoylo\Csquare\Block\Adminhtml\Form;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Zoylo\Csquare\Model\ProductsFactory
     */
    protected $_productsFactory;

    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Zoylo\Csquare\Model\ProductsFactory $productsFactory
     * @param \Zoylo\Csquare\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Zoylo\Csquare\Model\ProductsFactory $productsFactory,
        \Zoylo\Csquare\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_productsFactory = $productsFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_productsFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'name'=>'id'
            ]
        );
        $this->addColumn(
            'sku',
            [
                'header' => __('SKU'),
                'index' => 'sku',
                'class' => 'xxx',
                'name'=>'sku'
            ]
        );
		 $this->addColumn(
            'ucode_id',
            [
                'header' => __('Ucode Id'),
                'index' => 'ucode_id',
                'class' => 'xxx',
                'name'=>'ucode_id'
            ]
        );
		 $this->addColumn(
            'product_data',
            [
                'header' => __('Product Data'),
                'index' => 'product_data',
                'class' => 'xxx',
                'name'=>'product_data'
            ]
        );
		

        $this->addColumn(
            'created_at',
            [
                'header' => __('Created date'),
                'index' => 'created_at',
                'type' => 'date',
                'name'=>'created_at'
            ]
        );

		$this->addColumn(
            'updated_at',
            [
                'header' => __('Updated date'),
                'index' => 'updated_at',
                'type' => 'date',
                'name'=>'updated_at'
            ]
        );
		$this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'index' => 'status',
                'type' => 'options',
                'name'=>'status',
				'options' => $this->_status->getOptionArray()
            ]
        );


        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        
        $this->setMassactionIdField('id');
      //  $this->getMassactionBlock()->setTemplate('Zoylo_Csquare::form/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('zoylo_product');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('csquare/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        array_unshift($statuses, ['label' => '', 'value' => '']);
        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('csquare/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('csquare/*/grid', ['_current' => true]);
    }

}