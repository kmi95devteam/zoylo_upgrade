<?php

namespace Zoylo\Csquare\Plugin\Model;

use Magento\Framework\DataObject;

   
class Sequence
{
    protected $connection;

    public function __construct(\Magento\Framework\App\ResourceConnection $connection) {
        $this->connection = $connection;
    }

    public function aroundGetNextValue(\Magento\SalesSequence\Model\Sequence $subject, callable $proceed)
    {
        return $this->getIncrementIds();

    }
    // this methid ensure and check for random number in order table 

    public function getIncrementIds(){
        $connection =  $this->connection->getConnection();
        $sql = "SELECT temp.rnd FROM sales_order c,(SELECT FLOOR( 10000000 + ( RAND( ) *89999999 ) ) as rnd FROM sales_order LIMIT 0,50) AS temp
               WHERE c.increment_id NOT IN (temp.rnd) LIMIT 1";
        $result = $connection->fetchAll($sql);
	$next = $result[0]['rnd'];
        return $next;
    }

}