<?php
namespace Zoylo\Csquare\Plugin\Block\Widget\Button;

use Magento\Backend\Block\Widget\Button\Toolbar as ToolbarContext;
use Magento\Framework\View\Element\AbstractBlock;
use Magento\Backend\Block\Widget\Button\ButtonList;

class Toolbar
{
    public function beforePushButtons(
        ToolbarContext $toolbar,
        \Magento\Framework\View\Element\AbstractBlock $context,
        \Magento\Backend\Block\Widget\Button\ButtonList $buttonList
    ) {
        if (!$context instanceof \Magento\Sales\Block\Adminhtml\Order\View) {
            return [$context, $buttonList];
        }

        $order = $context->getOrder();
        $status = $order['status'];
        $buttonList->remove('order_hold');
        $buttonList->remove('order_emails');
        if ($status == 'ready_for_packing_cs'):
            $buttonList->remove('order_edit');
            $buttonList->remove('order_invoice');
            $buttonList->remove('order_hold');
            $buttonList->remove('order_ship');
            $buttonList->remove('order_cancel');
            $buttonList->remove('order_reorder');
            $buttonList->remove('order_creditmemo');
        endif;

        return [$context, $buttonList];
    }
}