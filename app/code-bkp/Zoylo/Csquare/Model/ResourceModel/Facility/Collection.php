<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Zoylo\Csquare\Model\ResourceModel\Facility;
 
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Zoylo\Csquare\Model\Facility', 'Zoylo\Csquare\Model\ResourceModel\Facility');
    }
 
    
}
