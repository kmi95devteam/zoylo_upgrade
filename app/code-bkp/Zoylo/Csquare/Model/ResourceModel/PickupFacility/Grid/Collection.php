<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Zoylo\Csquare\Model\ResourceModel\PickupFacility\Grid;

/**
 * Customers collection for customer_grid indexer
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Zoylo\Csquare\Model\PickupFacility', 'Zoylo\Csquare\Model\ResourceModel\PickupFacility');
    }
}
