<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Zoylo\Csquare\Model\ResourceModel\Products;
 
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Zoylo\Csquare\Model\Products', 'Zoylo\Csquare\Model\ResourceModel\Products');
        //$this->_map['fields']['page_id'] = 'main_table.page_id';
    }
 
    
}
