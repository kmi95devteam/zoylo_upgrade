<?php
namespace Zoylo\Csquare\Model\ResourceModel;


class UnicommerceLogs extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}
	
	protected function _construct()
	{
		$this->_init('unicommerce_failed_sync_orders', 'id');
	}
	
}