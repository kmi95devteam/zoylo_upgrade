<?php

namespace Zoylo\Csquare\Model;

use Zoylo\Csquare\Helper\Data;

class ProductSync {
	
	protected $product;
	
	/**
     * 
     * @param Data $data
	 * @param \Zoylo\Csquare\Model\CsquareToMagento\Product $product
     */
    public function __construct(
    Data $data,
	\Zoylo\Csquare\Model\CsquareToMagento\Product $product
    ) {
        $this->_data = $data;
		$this->product = $product;
    }
	
	
	public function productSync() {
	
		$dataLog = $this->_data;

		$entity = 'product'; // based on entites we need to declre in xml file and read here dynamically

		$dataLog->infoLog(__METHOD__, "Cron Job is Started For " . $entity . " "
				. date('Y-m-d H:i:s'), 'ProductSync');
		$this->manipulateEntity($entity);
		$dataLog->infoLog(__METHOD__, "Cron Job is End For " . $entity . " "
				. date('Y-m-d H:i:s'), 'ProductSync');
		
	}
	 
	public function manipulateEntity($entity) {
	
		$entityApiClass = $this->_data->getApiClass($entity);
        $entityApiClass = $entityApiClass->create();
        if (!$entityApiClass) {
            return;
        }
		try {
            $this->processPendingRec($entityApiClass, $entity);
        } catch (\Exception $ex) {
            $this->_data->criticalLog(__METHOD__, $ex->getMessage(), "productApiException");
        }
		return true;
	}
	
	public function processPendingRec($entityApiClass, $entity) {
        try {
		
            do {
                $getRecordsO = $entityApiClass->getCollection()
                                ->addFieldtoFilter('status', ['in' => [1]])->setPageSize(50)->setCurPage(1);
								 
                $getRecords = $getRecordsO->getData();
                if ($getRecordsO->getSize() > 0) {
                    //call processlock till you get the list of ids
                    $arrayOfIds = $this->processLockIds($getRecords, $entityApiClass);
                    if (!empty($arrayOfIds)) {
                        $this->createObject($entity, $arrayOfIds);
                    }
                } else {
                    return;
                }
            } while (true);
        } catch (\Exception $ex) {
            $this->_data->criticalLog(__METHOD__, $ex->getMessage(), "productApiException");
        }
    }
	
	
	public function processLockIds($getRecords, $entityApiClass) {
        try {
                $processedIds = array();
                foreach ($getRecords as $msgId) {
                    try {
                        $status = $entityApiClass->load($msgId['id']);
                        if ($status->getStatus() != 2) {
                            $processedIds[] = $status->getId();
							$status->setStatus(2);
                            $status->save();
                        }  
                    } catch (exception $e) {
                     
                    }
                }
                return $processedIds;
        } catch (\Exception $ex) {
            $this->_data->criticalLog(__METHOD__, $ex->getMessage(), "productApiException");
        }
        return;
    }
	
	/** 
	 * function to operate each entity
     */
    public function createObject($entity, $arrayOfIds) {

		 try {
            switch (trim($entity)) {
			
                case "product":
                    $object = $this->product->createProductsList($arrayOfIds);
                    break;
            }
            return $object;
        } catch (\Exception $ex) {
            $this->_data->criticalLog(__METHOD__, $ex->getMessage(), "productApiException");
        }
		
	}
	
	
	  

}