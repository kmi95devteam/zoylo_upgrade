<?php
namespace Zoylo\Csquare\Model;
class UnicommerceLogs extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'zoylo_csquare_unicommercelogs';

	protected $_cacheTag = 'zoylo_csquare_unicommercelogs';

	protected $_eventPrefix = 'zoylo_csquare_unicommercelogs';

	protected function _construct()
	{
		$this->_init('Zoylo\Csquare\Model\ResourceModel\UnicommerceLogs');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}