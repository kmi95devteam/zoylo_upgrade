<?php
namespace Zoylo\Csquare\Model;

use Zoylo\Csquare\Helper\Data;
use Zoylo\Csquare\Api\Data\OrderDataInterfaceFactory;
class OrderInfoManagement implements \Zoylo\Csquare\Api\OrderInfoManagementInterface
{

	protected $ordersfactory;
	
	/**
	* @var Data
	*/
	protected $data;
	
	
	/**
	* @var \Zoylo\Csquare\Api\Data\OrderDataInterfaceFactory
	*
	*/
	protected $orderDataInterfaceFactory;
	
	/**
		* 
		* @param \Magento\Framework\Stdlib\DateTime\DateTime $date
		* @param Data $data
		*
	*/
	
	public function __construct(\Magento\Framework\Stdlib\DateTime\DateTime $date,
	\Zoylo\Csquare\Model\Ordersfactory  $ordersfactory,
	Data $data,
	OrderDataInterfaceFactory $orderDataInterfaceFactory) {
		$this->date = $date;
		$this->_ordersfactory = $ordersfactory;
		$this->data = $data;
		$this->orderDataInterfaceFactory = $orderDataInterfaceFactory;
	} 
	
	
    /**
     * {@inheritdoc}
     */
	public function saveOrderInfo($order)
	{
		$dataLog = $this->data;
		
		try {
			
			$is_enabled = $this->data->isEnabled();
			
			if ($is_enabled == 0) 
			{
				throw new \Exception("Please Enable the C-square Module");
			}
			
			$dataLog->infoLog(__METHOD__, "" . json_encode($order), "OrderInfo");
			$result = [];
			foreach ($order as $key => $orders) {
				
				$source_order_id = $orders['source_order_id'];
				$target_order_id = $orders['target_order_id'];
				$fullfilment_status = $orders['fullfilment_status'];
				
				
				$orderModel = $this->_ordersfactory->create();
				$orderModel->setSourceOrderId($source_order_id);
				$orderModel->setTargetOrderId($target_order_id);
				$orderModel->setFullfilmentStatus($fullfilment_status);
				$orderModel->setCreatedAt($this->date->gmtDate());
				$orderModel->setUpdatedAt($this->date->gmtDate());
				$orderModel->setStatus(1);
				$orderModel->save();
				$id = $orderModel->getId();
				$result[] = $id;
				
			}
			
			$message = __('Order Data Info');
			$data = [
			'code'             		=> 1000,
			'success'               => 'true',
			'message'               => $message,
			'ids'  					=> $result
			];
			$result = $this->orderDataInterfaceFactory->create();
			$result->setData($data);
			
			
		}
		catch (\Magento\Framework\Exception\LocalizedException $e) {
			$message = $e->getMessage();
			$data = [
			'code'                  => 1100,
			'success'               => 'false',
			'message'               => $message,
			'ids'  					=> ''
			];
			$result = $this->orderDataInterfaceFactory->create();
			$result->setData($data);
			$dataLog->criticalLog(__METHOD__, $e->getMessage(), "OrderInfoException");
			} catch (\Exception $e) {
			$message = $e->getMessage();
			$data = [
			'code'                  => 1100,
			'success'               => 'false',
			'message'               => $message,
			'ids'   				=> ''
			];
			$result = $this->orderDataInterfaceFactory->create();
			$result->setData($data);
			$dataLog->criticalLog(__METHOD__, $e->getMessage(), "OrderInfoException");
			}catch (CouldNotSaveException $e) {
			$message = $e->getMessage();
			$data = [
			'code'                  => 1100,
			'success'               => 'false',
			'message'               => $message,
			'ids'  					=> ''
			];
			$result = $this->orderDataInterfaceFactory->create();
			$result->setData($data);
			$dataLog->criticalLog(__METHOD__, $e->getMessage(), "OrderInfoException");
		}
		
		return $result;
	}
	
	
}
