<?php

namespace Zoylo\Csquare\Model;

class Reasons
{
    /**#@+
     * Blog Status values
     */
    const STATUS_QUNATITY_DIFF = 1;

    const STATUS_PRICE_DIFF = 2;
	
    const STATUS_BOTH = 3;
	

    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public static function getOptionArray()
    {
        return [self::STATUS_QUNATITY_DIFF => __('Quantity'), self::STATUS_PRICE_DIFF => __('Price'), self::STATUS_BOTH => __('Both')];
    }

    /**
     * Retrieve option array with empty value
     *
     * @return string[]
     */
    public function getAllOptions()
    {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * Retrieve option text by option value
     *
     * @param string $optionId
     * @return string
     */
    public function getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}