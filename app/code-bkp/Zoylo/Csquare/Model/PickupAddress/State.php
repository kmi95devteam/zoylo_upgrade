<?php

namespace Zoylo\Csquare\Model\PickupAddress;

/**
 * Class AttributeOptions
 * @package Acme\DependentFields\Model\Example
 */
class State implements \Magento\Framework\Option\ArrayInterface
{
    /** @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory */
    private $regionColFactory;


    /** @var array */
    private $items;

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory
     */
    public function __construct(
        \Magento\Directory\Model\RegionFactory $regionColFactory
    ) {
        $this->regionColFactory = $regionColFactory;
    }

    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        if (is_null($this->items)) {
            $this->items = $this->getOptions();
        }

        return $this->items;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Eav\Attribute[]|\Magento\Framework\DataObject[]
     */
    public function getAttributes()
    {
        $collection = $this->getCollection();
        $collection->addFieldToFilter('country_id', 'IN');
        return $collection->getItems();
    }

    /**
     * @return array
     */
    private function getOptions()
    {
        $items = [];
        foreach ($this->getAttributes() as $attribute) {
            $items[] = [
                'label' => $attribute->getName(), 'value' => $attribute->getName(),
            ];
        }
        return $items;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection
     */
    private function getCollection()
    {
        return $this->regionColFactory->create()->getCollection();
    }
}