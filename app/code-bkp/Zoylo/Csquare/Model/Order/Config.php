<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Zoylo\Csquare\Model\Order;

/**
 * Order configuration model
 *
 * @api
 * @since 100.0.2
 */
class Config extends \Magento\Sales\Model\Order\Config
{
 
    public function getStatesCustom()
    {
        $statesCustom = [];
        $toShowStates = array('Cancelled','Undelivered','Closed','Complete','Ready to deliver','Payment Review','On Hold Prescription','Payment Pending'); // default magento status mapped to state
        foreach ($this->_getCollection() as $item) {
            if ($item->getState() && in_array($item->getData('label'), $toShowStates)) {
                $statesCustom[$item->getState()] = __($item->getData('label'));
            }
        }
        return $statesCustom;
    }
}
