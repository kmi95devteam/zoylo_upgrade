<?php

namespace Zoylo\Csquare\Model;

use Zoylo\Csquare\Helper\Data;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Framework\DB\TransactionFactory;
use Magento\Sales\Model\OrderRepository;
use Magento\Quote\Model\QuoteRepository;
use Magento\CatalogInventory\Api\StockRegistryInterface as StockRegistry;
use Magento\Quote\Model\Quote\Item\ToOrderItem as ToOrderItemConverter;

class OrderSync {

    /**
     * @var ToOrderItemConverter
     */
    private $toOrderItem;

    /**
     * @var array
     */
    private $editedItems = [];

    /**
     * @var StockRegistry
     */
    protected $stockRegistry;

    /**
     * @var OrderRepository
     */
    private $ordersRepository;

    /**
     * @var QuoteRepository
     */
    private $quoteRepository;

    /**
     * @var Data
     */
    protected $_data;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dataTime;

    /**
     * @var \Magento\Framework\DB\Transaction
     */
    private $saveTransaction;

    /**
     * scopeConfig for system Congiguration
     *
     * @var string
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $_orderRepository;

    /**
     * @var \Magento\Sales\Model\Service\InvoiceService
     */
    protected $_invoiceService;

    /**
     * @var \Magento\Framework\DB\Transaction
     */
    protected $_transaction;

    /**
     * @var \I95Dev\BranchInformation\Model\BranchFactory $branchFactory
     */
    protected $_branch;

    /**
     * @var InvoiceSender
     */
    protected $invoiceSender;

    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $_salesOrderModel;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var Customer Model
     */
    protected $_customerModel;

    /**
     * Constants for the on hold cs status
     *
     * @deprecated
     */
    const ON_HOLD_CS = 'on_hold_cs';

    /**
     * Constants for the order processing cs status
     *
     * @deprecated
     */
    const ORDER_PROCESSING_CS = 'order_processing_cs';

    /**
     * Constants for the customer_payment_pending_cs status
     *
     * @deprecated
     */
    const CUSTOMER_PAYMENT_PENDING_CS = 'customer_payment_pending_cs';

    /**
     * Constants for the ready_for_packing_cs status
     *
     * @deprecated
     */
    const READY_FOR_PACKING_CS = 'ready_for_packing_cs';

    /**
     * Constants for the order_cancelled_cs status
     *
     * @deprecated
     */
    const ORDER_CANCELLED_CS = 'canceled';

    const SPECIAL_CHARACTERS = '_-+~/\\<>\'�?:*$#@()!,.?`=%&^{}[]';

    protected $helperapi;
    protected $objectManager;
    protected $transportBuilder;
	protected $onHoldReportFactory;

    /**
     * 
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dataTime
     * @param Data $data
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param InvoiceSender $invoiceSender
     */
    public function __construct(
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Stdlib\DateTime\DateTime $dataTime, \Magento\Sales\Api\OrderRepositoryInterface $orderRepository, \Magento\Sales\Model\Service\InvoiceService $invoiceService, \Magento\Framework\DB\Transaction $transaction, InvoiceSender $invoiceSender, \I95Dev\BranchInformation\Model\BranchFactory $branchFactory, \Magento\Sales\Model\OrderFactory $salesOrderModel, \Magento\Catalog\Model\ProductFactory $productFactory, \Magento\Customer\Model\CustomerFactory $customerModel, TransactionFactory $transactionFactory, OrderRepository $ordersRepository, QuoteRepository $quoteRepository, Data $data, StockRegistry $stockRegistry, ToOrderItemConverter $toOrderItem, \Magecomp\Smsfree\Helper\Apicall $helperapi, \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder, \Zoylo\Csquare\Model\OnHoldReportFactory $onHoldReportFactory, \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_data = $data;
        $this->_dataTime = $dataTime;
        $this->_orderRepository = $orderRepository;
        $this->_invoiceService = $invoiceService;
        $this->_transaction = $transaction;
        $this->invoiceSender = $invoiceSender;
        $this->_branch = $branchFactory;
        $this->_salesOrderModel = $salesOrderModel;
        $this->_productCollectionFactory = $productFactory;
        $this->_customerModel = $customerModel;
        $this->saveTransaction = $transactionFactory->create();
        $this->ordersRepository = $ordersRepository;
        $this->quoteRepository = $quoteRepository;
        $this->stockRegistry = $stockRegistry;
        $this->toOrderItem = $toOrderItem;
        $this->helperapi = $helperapi;
        $this->transportBuilder     = $transportBuilder;
        $this->objectManager = $objectManager;
		$this->onHoldReportFactory = $onHoldReportFactory;
    }

    public function orderSync($status = NULL) {

        $isEnabled = $this->_scopeConfig->getValue('csquare/settings/enabled', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        if ($isEnabled) {
            $orderSyncApiUrl = $this->_scopeConfig->getValue('csquare/settings/order_create', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            if ($orderSyncApiUrl != '') {
                $entity = 'order';
                $create = $this->manipulateEntity($entity, $status);
            }
        }
    }

    public function manipulateEntity($entity, $status = NULL) {

        $entityApiClass = $this->_data->getApiClass($entity);
        $entityApiClass = $entityApiClass->create();
        if (!$entityApiClass) {
            return;
        }
        try {
            $this->processPendingRec($entityApiClass, $entity, $status);
        } catch (\Exception $ex) {
            $this->_data->criticalLog(__METHOD__, $ex->getMessage(), "OrderSyncApiException");
        }
        return true;
    }

    public function processPendingRec($entityApiClass, $entity, $status = NULL) {

        try {
            if ($status == NULL) {
                $status = ['3','5'];
            } else {
                $status = ['1'];
            }
            $getRecordsO = $entityApiClass->getCollection()
                    ->addFieldtoFilter('status', ['in' => $status]);
                    //->setPageSize(50)->setCurPage(1);

            $getRecords = $getRecordsO->getData();
            if ($getRecordsO->getSize() > 0) {
                //call processlock till you get the list of ids
                $arrayOfIds = $this->processLockIds($getRecords, $entityApiClass, $entity);
            } else {
                return;
            }
        } catch (\Exception $ex) {
            $this->_data->criticalLog(__METHOD__, $ex->getMessage(), "OrderSyncApiException");
        }
    }

    public function processLockIds($getRecords, $entityApiClass, $entity) {

        try {
            $processedIds = array();
            foreach ($getRecords as $msgId) {
                try {
                    $processedIds[] = $msgId['id'];
                } catch (\Exception $ex) {
                    $this->_data->criticalLog(__METHOD__, $ex->getMessage(), "OrderSyncApiException");
                }
            }
            if (!empty($processedIds)) {
                $this->createObject($entity, $processedIds);
            }

            return true;
        } catch (\Exception $ex) {
            $this->_data->criticalLog(__METHOD__, $ex->getMessage(), "OrderSyncApiException");
        }
        return;
    }

    /**
     * function to operate each entity
     */
    public function createObject($entity, $arrayOfIds) {

        try {
            switch (trim($entity)) {
                case "order":
                    $action = '';
                    $object = $this->processOrder($entity, $arrayOfIds, $action , $post = 1);
                    break;
            }
            return $object;
        } catch (\Exception $ex) {
            $this->_data->criticalLog(__METHOD__, $ex->getMessage(), "OrderSyncApiException");
        }
    }

    /**
     * function to operate each entity
     */
    public function processOrder($entity, $arrayOfIds, $action, $post = 1) {
        try {

            $entityApiClass = $this->_data->getApiClass($entity);
            $entityApiClass = $entityApiClass->create();
            foreach ($arrayOfIds as $key => $id) {
                $entityApiClass = $entityApiClass->load($id);
                $orderId = $entityApiClass->getData()['source_order_id'];
                $order = $this->_salesOrderModel->create()->loadByIncrementId($orderId);
                $orderStatus = $order->getStatus();
                $createDate = $order->getCreatedAt();
                $customerName = $order->getCustomerName();
                $customerEmail = $order->getCustomerEmail();
                $customerId = $order->getCustomerId();
                $customerMobile = $order->getCustomerMobileNumber();
                if ($customerMobile == '') {
                    $customer = $this->_customerModel->create()->load($customerId);
                    $customerMobile = $customer->getCustomerNumber();
                    if ($customerMobile == '')
                        $customerMobile = $customer->getMobileNumber();
                }
                $discount_per = 0;
                $urgent = 0;
                $inter_sale = 0;
                $disc_rs = 0;
                $order_value = 0;
                $erpStatus = $entityApiClass->getData()['status'];
                if ($erpStatus == '5') {
                    $action = 'update';
                }elseif($action == '' || $erpStatus == 3){
                    $action = 'create';
                }
                
                $cIndex = $action;
                $action = '';
                $base_grand_total = number_format($order->getbase_grand_total(), 2, '.', '');
                $grand_total = number_format($order->getgrand_total(), 2, '.', '');
                $shipping_amount = number_format($order->getshipping_amount(), 2, '.', '');
                $discount_amount = number_format($order->getdiscount_amount(), 2, '.', '');
                $discount_canceled = number_format($order->getdiscount_canceled(), 2, '.', '');

                $billingAddress = $order->getBillingAddress();
                $billing_name = $billingAddress->getFirstname() . ' ' . $billingAddress->getLastname();
                $billStreet = $billingAddress->getStreet();
                $billing_add1 = '';
                $billing_add2 = '';
                $replaceSymbols =  str_split(self::SPECIAL_CHARACTERS, 1);
                foreach ($billStreet as $key => $value) {
                    if ($key == 0){
                        $billing_add1 = str_replace($replaceSymbols, ' ', $value);
                    }
                    if ($key == 1){
                        $billing_add2 = str_replace($replaceSymbols, ' ', $value);
                    }
                }
                $billing_city = $billingAddress->getCity();
                $billing_pin = $billingAddress->getPostcode();
                $billing_state = $billingAddress->getRegion();
                $billing_mobile_no = $billingAddress->getTelephone();

                $shippingAddress = $order->getShippingAddress();
                $shipping_name = $shippingAddress->getFirstname() . ' ' . $shippingAddress->getLastname();
                $shipStreet = $shippingAddress->getStreet();
                $shipping_add1 = '';
                $shipping_add2 = '';
                
                foreach ($shipStreet as $key => $value) {
                    if ($key == 0){
                        //$shipping_add1 = $value;
                        $shipping_add1 = str_replace($replaceSymbols, ' ', $value);
                    }
                    if ($key == 1){
                        //$shipping_add2 = $value;
                        $shipping_add2 = str_replace($replaceSymbols, ' ', $value);
                    }
                }
                $shipping_city = $shippingAddress->getCity();
                $shipping_pin = $shippingAddress->getPostcode();
                $shipping_state = $shippingAddress->getRegion();
                $shipping_mobile_no = $shippingAddress->getTelephone();
                $doctor_name = $order->getDoctorName();

                $payment = $order->getPayment();
                $paymentMethod = $payment->getMethod();
                if ($paymentMethod == 'cashondelivery' || $paymentMethod == 'free') {
                    $csPaymentMethod = 1;
                } elseif ($paymentMethod == 'paylater') {
                    $csPaymentMethod = 3;
                }
				
				if(!empty($billing_add1) || !empty($billing_add2) || !empty($shipping_add1) || !empty($shipping_add2)){
					setlocale(LC_CTYPE, 'cs_CZ');
					$billing_add1 = mb_strtolower($billing_add1);
					$billing_add2 = mb_strtolower($billing_add2);
					$shipping_add1 = mb_strtolower($shipping_add1);
					$shipping_add2 = mb_strtolower($shipping_add2);
					$billing_add1 = str_replace($replaceSymbols, ' ',ucwords(iconv('UTF-8', 'ASCII//TRANSLIT', $billing_add1)));
					$billing_add2 = str_replace($replaceSymbols, ' ',ucwords(iconv('UTF-8', 'ASCII//TRANSLIT', $billing_add2)));
					$shipping_add1 = str_replace($replaceSymbols, ' ',ucwords(iconv('UTF-8', 'ASCII//TRANSLIT', $shipping_add1)));
					$shipping_add2 = str_replace($replaceSymbols, ' ',ucwords(iconv('UTF-8', 'ASCII//TRANSLIT', $shipping_add2)));
				}
				
                $errorMsg = '';
                // Branch Code check
                $branchCollection = $this->_branch->create()->getCollection()
                        ->addFieldtoFilter('zip_code', $shipping_pin);
                if ($branchCollection->getSize() > 0) {
                    foreach ($branchCollection as $branch) {
                        $branchCode = $branch->getBranchCode();
                    }
                    if ($branchCode != '') {
                        $items = '';
                        $item_branch_id = $branchCode;
                        foreach ($order->getAllItems() as $item) {
                           // print_r($item->getData());
                            $sku = $item->getSku();
                            if($sku != 'Free'){
                                $productId = $item->getProductId();
                                $qtyOrdered = intval($item->getQtyOrdered());
                                $product = $this->_productCollectionFactory->create()->load($productId);
                                $item_id = $product->getUcodeId();
                                $discountprice = $item->getDiscountAmount();
                                $price = ($item->getRowTotal() - $discountprice);
                                $price = number_format($price, 2, '.', '');
                                $discountPercent = round($item->getDiscountPercent());
                                $discountedPricePerQty = $price / $qtyOrdered;
                                $discountedPricePerQty = number_format($discountedPricePerQty, 2, '.', '');
                                $items .= "&item[]['item_id']=" . $item_id . "&item[]['item_branch_id']=" . $item_branch_id . "&item[]['item_qty']=" . $qtyOrdered ."&item[]['item_price']=&item[]['item_discount']=0&item[]['disc_per']=".$discountPercent;
                            }
                        }
                        if ($shipping_amount > 0) { 
                            $items .= "&item[]['item_id']=0&item[]['item_branch_id']=" . $item_branch_id . "&item[]['item_qty']=1&item[]['item_price']=" . $shipping_amount . "&item[]['item_discount']=0&item[]['disc_per']=0";
                        }
                        $requestText = 'cIndex=' . $cIndex . '&order_no=' . $orderId . '&order_date=' . $createDate . '&customer_name=' . $customerName . '&email=' . $customerEmail . '&doctor_name=' . $doctor_name . '&customer_id=' . $customerId . '&customer_mobile=' . $customerMobile . '&shipping_name=' . $shipping_name . '&shipping_add1=' . $shipping_add1 . '&shipping_add2=' . $shipping_add2 . '&shipping_city=' . $shipping_city . '&shipping_pin=' . $shipping_pin . '&shipping_state=' . $shipping_state . '&shipping_mobile_no=' . $shipping_mobile_no . '&billing_name=' . $billing_name . '&billing_add1=' . $billing_add1 . '&billing_add2=' . $billing_add2 . '&billing_city=' . $billing_city . '&billing_pin=' . $billing_pin . '&billing_state=' . $billing_state . '&billing_mobile_no=' . $billing_mobile_no . '&discount_per=' . $discount_per . '&urgent=' . $urgent .'&conversion=0' . $items . '&inter_sale=' . $inter_sale . '&disc_rs=' . $disc_rs . '&order_value=' . $order_value . '&payment method=' . $csPaymentMethod . '&post='.$post;
                        //echo "\n\n".$requestText;
                        //API Call
                        $apiUrl = $this->_scopeConfig->getValue('csquare/settings/order_create', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                        try {
                            $ch = curl_init($apiUrl);
                            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $requestText);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                'Content-Type: application/x-www-form-urlencoded'
                                    //'Content-Length: ' . strlen($data_string)
                                    )
                            );
                            $response = curl_exec($ch);
                            $this->_data->infoLog(__METHOD__, "Ecogreen " . $entity . " has been created " . date('Y-m-d H:i:s'), 'OrderSync');
                            $result = json_decode($response, true);
                            //print_r($result);
                            // Updating records in Order Report Table
                            $ecoGreenStatus = '';
                            if (isset($result[0]['status'])) {
                                $ecoGreenStatus = $result[0]['status'];
                            }
                            $cmessage = '';
                            if (isset($result[0]['cmessage'])) {
                                $cmessage = $result[0]['cmessage'];
                            } else {
                                $otherError = true;
                            }
                            if ($ecoGreenStatus == "Error") {
                                $entityApiClass->setUpdatedAt($this->_dataTime->gmtDate());
                                $entityApiClass->setResponse($cmessage);
                                $entityApiClass->setFullfilmentStatus($ecoGreenStatus);
                                $entityApiClass->setFailureStatus(1);
                                $entityApiClass->setMagentoOrderStatus($orderStatus);
                                $entityApiClass->setStatus(3);
                                $entityApiClass->setUpdatedBy('CSQ');
                                $entityApiClass->save();
                                $this->_data->infoLog(__METHOD__, "Order Report Table has been updated with error value " . date('Y-m-d H:i:s'), 'OrderSync');

                                //Email Sending..
                                $templateId = 'order_sync_fails';
                                $errorMsg .= $ecoGreenStatus.'=>'.$cmessage;
                                //$this->sendEmail($orderId, $errorMsg, $templateId);
                                
                            } else {
                                if($post == 0){
                                    $ecoGreenStatus = 'Order Hold';
                                }else{
                                    $ecoGreenStatus = 'Order Pending';
                                }
                                $entityApiClass->setUpdatedAt($this->_dataTime->gmtDate());
                                $entityApiClass->setTargetOrderId($cmessage);
                                $entityApiClass->setResponse($cmessage);
                                $entityApiClass->setFullfilmentStatus($ecoGreenStatus);
                                $entityApiClass->setStatus(2);
                                $entityApiClass->setFailureStatus(0);
                                $entityApiClass->setMagentoOrderStatus($orderStatus);
                                $entityApiClass->save();
                                $this->_data->infoLog(__METHOD__, "Order Report Table has been updated with success value " . date('Y-m-d H:i:s'), 'OrderSync');
                            }
                        } catch (\Exception $ex) {
                            $this->_data->criticalLog(__METHOD__, $ex->getMessage(), "OrderSyncApiException");
                            return false;
                        }
                    } else {
                        $cmessage = "Branch value is empty for the rquested pincode (". $shipping_pin ."). Please update the branch id in Branchmanager.";
                        $ecoGreenStatus = 'Order Pending';
                        $entityApiClass->setUpdatedAt($this->_dataTime->gmtDate());
                        $entityApiClass->setResponse($cmessage);
                        $entityApiClass->setFullfilmentStatus($ecoGreenStatus);
                        $entityApiClass->setFailureStatus(1);
                        $entityApiClass->setMagentoOrderStatus($orderStatus);
                        $entityApiClass->setStatus(3);
                        $entityApiClass->setUpdatedBy('CSQ');
                        $entityApiClass->save();
                        $this->_data->criticalLog(__METHOD__, "Branch value is empty.Please update the branch id in Branchmanager. " . date('Y-m-d H:i:s'), "OrderSyncApiException");

						//Email Sending..
                        $templateId = 'order_sync_fails';
                        $errorMsg .= $ecoGreenStatus.'=>'.$cmessage;
                        //$this->sendEmail($orderId, $errorMsg, $templateId);
                    }
                } else {
                    $cmessage = "Requested zipcode(" . $shipping_pin . ") is not serviceable.Please add zipcode and branch id in Branchmanager.";
                    $ecoGreenStatus = 'Order Pending';
                    $entityApiClass->setUpdatedAt($this->_dataTime->gmtDate());
                    $entityApiClass->setResponse($cmessage);
                    $entityApiClass->setFullfilmentStatus($ecoGreenStatus);
                    $entityApiClass->setFailureStatus(1);
                    $entityApiClass->setMagentoOrderStatus($orderStatus);
                    $entityApiClass->setStatus(3);
                    $entityApiClass->setUpdatedBy('CSQ');
                    $entityApiClass->save();
                    $this->_data->criticalLog(__METHOD__, "Requested zipcode(" . $shipping_pin . ") is not serviceable.Please add zipcode and branch id in Branchmanager." . date('Y-m-d H:i:s'), "OrderSyncApiException");
                    
                    //Email Sending..
                    $templateId = 'order_sync_fails';
                    $errorMsg .= $ecoGreenStatus.'=>'.$cmessage;
                    //$this->sendEmail($orderId, $errorMsg, $templateId);

                }
            }
            return true;
        } catch (\Exception $ex) {
            $this->_data->criticalLog(__METHOD__, $ex->getMessage(), "OrderSyncApiException");
            return false;
        }
    }

    /**
     *
     * Order status cron
     *
     */
    public function checkStatus($invoiceIds, $manualSync = false) {

        $isEnabled = $this->_scopeConfig->getValue('csquare/settings/enabled', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if ($isEnabled) {
            $orderStatusApiUrl = $this->_scopeConfig->getValue('csquare/settings/order_status', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            if ($orderStatusApiUrl != '') {
                
                    $entity = 'order';
                    $entityApiClass = $this->_data->getApiClass($entity);
                    $entityApiClass = $entityApiClass->create();

                    // Order Pending Iteration Started
                    $getRecordsO = $entityApiClass->getCollection()
                        ->addFieldtoFilter('fullfilment_status', ['in' => ['Order Pending','Error']]);
					//$getRecordsO->addFieldtoFilter('source_order_id', ['nin' => ['000038399','000038494']]);
                    if($manualSync == true && count($invoiceIds) > 0){
                        $getRecordsO->addFieldtoFilter('id', ['in' => $invoiceIds]);
                    }
                    $getRecordsO->addFieldtoFilter('status', ['in' => [2]]);
                    //$getRecordsO->printlogquery(true);die;
                    //$getRecordsO->setPageSize(50)->setCurPage(1);

                    $getRecords = $getRecordsO->getData();
                    if ($getRecordsO->getSize() > 0) {
                        foreach ($getRecords as $msgId) {
                            try {
                                $processId = $msgId['id'];
                                $incrementId = $msgId['source_order_id'];
                                $orderId = $msgId['entity_id'];
                                $created_at = $msgId['created_at'];
                                $currentDate = date('Y-m-d H:i:s');
                                $timediff = strtotime($created_at) - strtotime($currentDate);
                                $hoursCheck = false;
                                if($timediff > 172800){
                                    $hoursCheck = true;
                                }
                                //API Call
                                $apiUrl = $orderStatusApiUrl . $incrementId;

                                $ch = curl_init($apiUrl);
                                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                $response = curl_exec($ch);
                                $this->_data->infoLog(__METHOD__, "Ecogreen " . $entity . " status check started " . date('Y-m-d H:i:s'), 'OrderSync');
                                $result = json_decode($response, true);
                                $items = $result['invoices'][0]['detail'];
                                echo $incrementId . '---';
                                if (isset($result['invoices'])) {
                                    
                                    $this->_data->infoLog(__METHOD__, $result['invoices'], 'invoicedetails');
                                    // Updating records in Order Report Table
                                    $ecoGreenOrderStatus = $result['invoices'][0]['docStatus'];
                                    $csquareInvoiceId = $result['invoices'][0]['docNo'];
                                    $items = $result['invoices'][0]['detail'];
                                    if (count($items) > 0) {

                                        $order = $this->_orderRepository->get($orderId);
                                        $payment = $order->getPayment();
                                        $paymentMethod = $payment->getMethod();

                                        $onHoldStatus = false;
                                        $qtyCheck = false;
                                        $priceCheck = false;
                                        $onholdReason = '';
                                        $onholdReason1 = '';
                                        $newStatus = '';
                                        $requestQtyArray = [];
                                        $requestPriceArray = [];
                                        $requestItemArray = [];
                                        $_SESSION['temp_mrp_greaterthan6per'] = [];
                                        $csquarLeessthan6perQtyArray = [];
                                        $csquarGreaterthan6perMrpArray = [];
                                        $csquareQtyArray = [];
										$onHoldReportArray = [];
										$odersArray = [];
										$fatalErrorStatus = false;
										$fatalErrorMsg = '';
										$fatalErrorMsg1 = "Fatal error occurred because of below ucode Ids: <br/>";
										$fatalErrorMsg2 = '';
                                        foreach ($order->getAllItems() as $magentoItem) {
                                            $sku = $magentoItem->getSku();
                                            if($sku != 'Free'){
                                                $productId = $magentoItem->getProductId();
                                                $qtyOrdered = intval($magentoItem->getQtyOrdered());
                                                $product = $this->_productCollectionFactory->create()->load($productId);
                                                $item_id = $product->getUcodeId();
                                                $discountprice = $magentoItem->getDiscountAmount();
                                                $price = ($magentoItem->getPrice());
                                                $price = number_format($price, 2, '.', '');
                                                $requestQtyArray[$item_id] = $qtyOrdered;
                                                $requestItemArray[$item_id] = $sku;
                                                $requestPriceArray[$item_id] = $price;
                                            }
                                        }

                                        foreach ($items as $data) {
               
                                            if ($data['productId'] != '0') {
                                                $qty = $data['BalQty'] / $data['qtyPerBox'];
                                                if(isset($_SESSION['temp_qty'])){
                                                if (array_key_exists($data['productId'], $_SESSION['temp_qty'])) {
                                                    $qty = $_SESSION['temp_qty'][$data['productId']] + $qty ;
                                                    $_SESSION['temp_qty'][$data['productId']] = $qty;
                                                 } else {
                                                    $_SESSION['temp_qty'][$data['productId']] = $qty;
                                                 }
                                                }else {
                                                    $_SESSION['temp_qty'][$data['productId']] = $qty;
                                                }
                                                $csquareQtyArray = $_SESSION['temp_qty'];
                                            }
                                        }
										$i = 1;
                                        //print_r($csquareQtyArray);
                                        foreach ($items as $key => $item) {
                                            $productId = $item['productId'];

                                            if ($productId != '0') {
                                                $qty = $item['qty'];
                                                $balQty = round($item['BalQty']);
                                                $mrp = $item['mrp'];
                                                $qtyPerBox = $item['qtyPerBox'];
                                                $csquarePrice = $mrp;

                                                // Qty Check
                                                //$responseQty = $balQty / $qtyPerBox;
                                                $requestQty = $requestQtyArray[$productId];
                                                $sku = $requestItemArray[$productId];
                                                $csquareQty = $csquareQtyArray[$productId];
                                                if ($requestQty > $csquareQty) {
                                                    $diffQty = $requestQty - $csquareQty;
                                                    $onHoldStatus = true;
                                                    $qtyCheck = true;
                                                    $procurementText = '';
                                                    if($hoursCheck == true){
                                                        $procurementText = 'Procurement in progress.';
                                                    }
                                                    $onholdReason1 .= '<br/>SKU '.$sku.' (Qty: '.$diffQty.') is not available.'.$procurementText;
													$onHoldReportArray[$incrementId][] = ['product_sku' => $sku, 'diff_qty' => $diffQty, 'diff_price' => '', 'reason' => 1];
                                                }

                                                // Price Check below 6%

                                                if($balQty > 0){
                                                    $magentoPrice = $requestPriceArray[$productId];
                                                    $qty = $item['BalQty'] / $item['qtyPerBox'];
                                                    if ( round($csquarePrice) <= round($magentoPrice * 1.06) ){
                                                        if(isset($_SESSION['temp_lessthan6per_qty'])){
                                                            if (array_key_exists($item['productId'], $_SESSION['temp_lessthan6per_qty'])) {
                                                                $qty = $_SESSION['temp_lessthan6per_qty'][$item['productId']] + $qty ;
                                                                $_SESSION['temp_lessthan6per_qty'][$item['productId']] = $qty;
                                                            }else {
                                                                $_SESSION['temp_lessthan6per_qty'][$item['productId']] = $qty;
                                                            }
                                                        }else {
                                                            $_SESSION['temp_lessthan6per_qty'][$item['productId']] = $qty;
                                                        }
                                                    }elseif ( round($csquarePrice) > round($magentoPrice * 1.06) ){

                                                        if(isset($_SESSION['temp_lessthan6per_qty'])){
                                                            if (array_key_exists($item['productId'], $_SESSION['temp_lessthan6per_qty'])) {
                                                                $_SESSION['temp_lessthan6per_qty'][$item['productId']] = $_SESSION['temp_lessthan6per_qty'][$item['productId']] + 0 ;
                                                            }else {
                                                                $_SESSION['temp_lessthan6per_qty'][$item['productId']] = 0;
                                                            }
                                                        }else {
                                                            $_SESSION['temp_lessthan6per_qty'][$item['productId']] = 0;
                                                        }

                                                        $mrp = $item['mrp'];
                                                        $_SESSION['temp_mrp_greaterthan6per'][$item['productId']] = $mrp;
                                                    }
                                                    $csquarLeessthan6perQtyArray = $_SESSION['temp_lessthan6per_qty'];
                                                    $csquarGreaterthan6perMrpArray = $_SESSION['temp_mrp_greaterthan6per'];
                                                }
												
												if(!array_key_exists($productId, $requestItemArray)){
													$fatalErrorStatus = true;
													$productName =  $item['productName'];
													$fatalErrorMsg2 .= "<br/>Ucode Id(".$productId.") of product(".$productName.").";
												}
                                            }
                                        }
                                        //print_r($csquarLeessthan6perQtyArray);
                                        //Price Check below 6% putting onhold
                                        foreach ($items as $key => $item) {
                                            $productId = $item['productId'];
                                            if ($productId != '0') {
                                                $balQty = round($item['BalQty']);
                                                if($balQty > 0){
                                                    $lessthan6per_qty = $csquarLeessthan6perQtyArray[$productId];
                                                    $requestQty = $requestQtyArray[$productId];
                                                    $mrp = '';
                                                    if(array_key_exists($productId, $csquarGreaterthan6perMrpArray)){
                                                        $mrp = $csquarGreaterthan6perMrpArray[$productId];
                                                    }
                                                    
                                                    if ($requestQty > $lessthan6per_qty && $mrp != ''){
                                                        $sku = $requestItemArray[$productId];
                                                        $priceCheck = true;
                                                        $onHoldStatus = true;
                                                        $onholdReason1 .= '<br/>More than 6% price difference occurred for SKU '.$sku.'. Csquare Price = Rs.'. $mrp;
														if(array_key_exists($incrementId, $onHoldReportArray)){
															$odersArray = $onHoldReportArray[$incrementId];
															foreach($odersArray as $key => $value){
																if(array_search($sku,$value) && $value['diff_qty'] != '' ){
																	$onHoldReportArray[$incrementId][] = ['product_sku' => $sku, 'diff_price' => $mrp,'reason' => 3];
																}
															}
															$onHoldReportArray[$incrementId][] = ['product_sku' => $sku, 'diff_qty' => '', 'diff_price' => $mrp,'reason' => 2];
														}else{
															$onHoldReportArray[$incrementId][] = ['product_sku' => $sku, 'diff_qty' => '', 'diff_price' => $mrp,'reason' => 2];
														}
                                                    }
                                                }
                                            }
                                        }
										
                                        unset($_SESSION['temp_mrp_greaterthan6per']);
                                        unset($_SESSION['temp_lessthan6per_qty']);
                                        unset($_SESSION['temp_qty']);

                                        $magentoItemsCount =  count($order->getAllItems());
                                        $csquareItemsCount = count($csquareQtyArray);
                                        
                                        if($magentoItemsCount != $csquareItemsCount){
                                            echo "\n magentoItemsCount : ".$magentoItemsCount." and csquareItemsCount :".$csquareItemsCount."\n";
											$fatalErrorStatus = true;
											$fatalErrorMsg = "Fatal error occurred because of magentoItemsCount (".$magentoItemsCount.") and csquareItemsCount (".$csquareItemsCount.") is not matching.";
											//continue;
                                        }
										
										//Skipping FatalError For Orders
										$quoteId = $order->getQuoteId();
										$quote = $this->quoteRepository->get($quoteId);
										foreach ($order->getAllItems() as $magentoItem) {
											$sku = $magentoItem->getSku();
											if($sku != 'Free'){
												$orderItemId = $magentoItem->getId();
												$orderItem = $order->getItemById($orderItemId);
												$quoteItem = $quote->getItemById($orderItem->getQuoteItemId());
												if(empty($quoteItem)){
													$fatalErrorStatus = true;
													$quoteItemId  = $orderItem->getQuoteItemId();
													$fatalErrorMsg .= "Fatal error occurred because of quote item (".$quoteItemId.") got deleted.";
												}
											}
										}
										if($fatalErrorMsg2 !=''){
											$fatalErrorMsg = $fatalErrorMsg1.$fatalErrorMsg2;
										}
										
										if($fatalErrorStatus == true){
											$entityApiClass->load($processId);
											$entityApiClass->setUpdatedAt($this->_dataTime->gmtDate());
											$entityApiClass->setFullfilmentStatus('Error');
											$entityApiClass->setResponse($fatalErrorMsg);
											$entityApiClass->setFailureStatus(1);
											$entityApiClass->save();
											continue;
										}
                                        //$status = array("Order Pending","Order Cancelled","Invoice Canceled","Invoice Created","Partial Return","Full Return");
                                        if ($ecoGreenOrderStatus == "Order Pending") {

                                            if ($onHoldStatus == true) {
                                                $newStatus = self::ON_HOLD_CS;
                                                if ($qtyCheck == true) {
                                                    $onholdReason .= '<br/>Order has been placed on Hold. Reasons:' . $onholdReason1;
                                                }elseif($priceCheck == true){
                                                    $onholdReason .= '<br/>Order has been placed on Hold. Reasons:' . $onholdReason1;
                                                }
                                            } else {
                                                $newStatus = self::ORDER_PROCESSING_CS;
                                            }

                                            if ($newStatus != '')
                                                $updateOrder = $this->updateStatus($order, $newStatus, $onholdReason, $processId, $qtyCheck, $priceCheck);

                                            $entityApiClass->load($processId);
                                            $entityApiClass->setUpdatedAt($this->_dataTime->gmtDate());

                                            if($newStatus == self::ORDER_PROCESSING_CS){
                                                $entityApiClass->setFullfilmentStatus($ecoGreenOrderStatus);
                                            }
                                            $entityApiClass->setResponse($onholdReason);
                                            $entityApiClass->setFailureStatus(0);
                                            $entityApiClass->setMagentoOrderStatus($newStatus);
                                            $entityApiClass->save();
                                            $this->_data->infoLog(__METHOD__, "Order Report Table has been updated with order status value " . date('Y-m-d H:i:s'), 'OrderSync');
											
											//onHoldReport Code
											if( count($onHoldReportArray) > 0){
												foreach($onHoldReportArray as $key => $value){
													foreach($value as $v){
														$reportIncrementId = $key;
														$reportSku = $v['product_sku'];
														$reportReason = $v['reason'];
														$reportDiffQty = $v['diff_qty'];
														$reportDiffPrice = $v['diff_price'];
														
														$onHoldReportRecords = $this->onHoldReportFactory->create()->getCollection()
															->addFieldtoFilter('product_sku', ['in' => [$reportSku]])
															->addFieldtoFilter('order_id', ['in' => [$reportIncrementId]]);
														if ($onHoldReportRecords->getSize() > 0) {
															foreach ($onHoldReportRecords as $onHoldReportRecord) {
																$reportId = $onHoldReportRecord->getId();
																$onHoldReportRecord = $this->onHoldReportFactory->create()->load($reportId);
																$onHoldReportRecord->setProductSku($reportSku)
																	->setOrderId($reportIncrementId)
																	->setReason($reportReason)
																	->setDiffQty($reportDiffQty)
																	->setDiffPrice($reportDiffPrice)
																	//->setCreatedDate($this->_dataTime->gmtDate())
																	->save();
															}
														}else{
															$onHoldReportRecord = $this->onHoldReportFactory->create()
																->setProductSku($reportSku)
																->setOrderId($reportIncrementId)
																->setReason($reportReason)
																->setDiffQty($reportDiffQty)
																->setDiffPrice($reportDiffPrice)
																->setCreatedDate($this->_dataTime->gmtDate())
																->save();
														}
													}
												}
												//print_r($onHoldReportArray);
											}											
                                        }elseif ($ecoGreenOrderStatus == "Invoice Created") {
                                            $this->_data->infoLog(__METHOD__, $processId, 'invoiceSync');
                                            $onholdReason = '';
                                            $newStatus = self::ORDER_PROCESSING_CS;
                                            if ($newStatus != '') {
                                                $updateOrder = $this->updateStatus($order, $newStatus, $onholdReason, $processId, $qtyCheck, $priceCheck);
                                            }
                                            $entityApiClass->load($processId);
                                            $entityApiClass->setUpdatedAt($this->_dataTime->gmtDate());
                                            //$entityApiClass->setFullfilmentStatus($ecoGreenOrderStatus);
                                            $entityApiClass->setStatus(2);
                                            $entityApiClass->setFailureStatus(0);
                                            $entityApiClass->setMagentoOrderStatus($newStatus);
                                            $entityApiClass->save();

                                            // Invoice Iteration Started
                                            $payment = $order->getPayment();
                                            $paymentMethod = $payment->getMethod();
                                            $newStatus = '';
                                            $onholdReason = '';

                                            if ($paymentMethod == 'cashondelivery' || $paymentMethod == 'free') {
                                                $newStatus = self::READY_FOR_PACKING_CS;
                                            } elseif ($paymentMethod == 'paylater') {
                                                $newStatus = self::CUSTOMER_PAYMENT_PENDING_CS;
                                            }
                                            $invoiceId = $this->createInvoice($order, $items);
                                            
                                            if ($invoiceId) {
                                                
                                                if ($newStatus != '')
                                                    $updateOrder = $this->updateStatus($order, $newStatus, $onholdReason, $processId, $qtyCheck, $priceCheck);
                                                $entityApiClass->load($processId);
                                                $entityApiClass->setUpdatedAt($this->_dataTime->gmtDate());
                                                $entityApiClass->setFullfilmentStatus($ecoGreenOrderStatus);
                                                $entityApiClass->setCsquareInvoiceId($csquareInvoiceId);
                                                $entityApiClass->setStatus(4);
                                                $entityApiClass->setResponse('');
                                                $entityApiClass->setFailureStatus(0);
                                                $entityApiClass->setMagentoOrderStatus($newStatus);
                                                $entityApiClass->save();
                                                $this->_data->infoLog(__METHOD__, "Order Report Table has been updated and invoice created " . date('Y-m-d H:i:s'), 'OrderSync');
                                           }
                                            // Invoice Iteration Ended
                                        }elseif ($ecoGreenOrderStatus == "Order Cancelled" || $ecoGreenOrderStatus == "Invoice Canceled") {
                                            $newStatus = self::ORDER_CANCELLED_CS;
                                            $entityApiClass->load($processId);
                                            $entityApiClass->setUpdatedAt($this->_dataTime->gmtDate());
                                            $entityApiClass->setFullfilmentStatus($ecoGreenOrderStatus);
                                            $entityApiClass->setStatus(4);
                                            $entityApiClass->setMagentoOrderStatus($newStatus);
                                            $entityApiClass->setFailureStatus(0);
                                            $entityApiClass->save();
                                            $this->_data->infoLog(__METHOD__, "Eco Green " . $ecoGreenOrderStatus . " and updated magento status also " . date('Y-m-d H:i:s'), 'OrderSync');
                                        }
                                    }
                                } else {
                                    $entityApiClass->load($processId);
                                    $entityApiClass->setUpdatedAt($this->_dataTime->gmtDate());
                                    $entityApiClass->setResponse($result['errorMessage']);
                                    $entityApiClass->setFailureStatus(1);
                                    $entityApiClass->setStatus(3);
                                    $entityApiClass->save();
                                    $this->_data->infoLog(__METHOD__, "Invoice Array is not created in Ecogreen for this order (" . $incrementId . ") " . date('Y-m-d H:i:s'), 'OrderSync');
                                }
                                 // Order Pending Iteration Ended
                                //return true;

                            } catch (\Exception $ex) {
								//echo "\n". $ex->getMessage();
                                $this->_data->criticalLog(__METHOD__, $ex->getMessage(), "OrderSyncApiException");
                                continue;
                                //return false;
                            }
                        }
                    }
            }
        }
    }

    /**
     *
     * Order Status Update
     *
     */
    public function updateStatus($order, $newStatus, $onholdReason, $processId, $qtyCheck, $priceCheck) {
        try {
            $updateAction = 1;
            //$orderHistroyComment = [];
            //$histories = $order->getStatusHistories();
            $statusHistory = '';
            $oldStatus = $order->getStatus();
            $statusHistory = "Order status has been changed from " . $oldStatus . " to " . $newStatus . ".";
            if ($onholdReason != '') {
                $statusHistory = $statusHistory . $onholdReason;
            }
            
            $statusHistoryItem = $order->getStatusHistoryCollection()->getFirstItem();
            $status = $statusHistoryItem->getStatusLabel();
            $comment = $statusHistoryItem->getComment();
            if ($comment == $statusHistory) {
                $updateAction = 0;
            }
            if($oldStatus == "customer_payment_pending_cs" || $oldStatus == "ready_for_packing_cs"){
				$updateAction = 0;
			}
            if ($updateAction == 1) {

                // Calling order Hold API in Csqure
                if ($newStatus == self::ON_HOLD_CS && $priceCheck == true){
                    $entity     = 'order';
                    $post       = 0;
                    $action     = 'update';
                    $arrayOfIds[] = $processId;
                    $holdApi = $this->processOrder($entity, $arrayOfIds, $action, $post);
                }

                $order->setStatus($newStatus);
                $order->addStatusHistoryComment($statusHistory);
                $order->save();

                $this->_data->infoLog(__METHOD__, "Order Status has been updated with the status (" . $newStatus . ") " . date('Y-m-d H:i:s'), 'OrderSync');

                
            }
            return true;
        } catch (\Exception $ex) {
            $this->_data->criticalLog(__METHOD__, $ex->getMessage(), "OrderSyncApiException");
            return false;
        }
    }

    /**
     *
     * Order Create Invoice
     *
     */
    public function createInvoice($order, $products) {
        try {
            if ($order->canInvoice()) {
                $quote = $this->updateQuote($order, $products);
                unset($_SESSION['temp']);
                $this->updateOrderItems($quote);
                $newGrandTotal = $quote->getGrandTotal();

                $discountAmount = -1 * abs($quote->getSubtotal() - $quote->getSubtotalWithDiscount());
                $baseDiscountAmount = -1 * abs($quote->getSubtotal() - $quote->getBaseSubtotalWithDiscount());

                 $onlyFreePrescription = TRUE;
                foreach ($order->getItems() as $eachItem) {
                    if ($eachItem->getSku() != 'Free') { // if any of the product is not a free product (prescription) then add shipping charges 
                        $onlyFreePrescription = FALSE;
                    }
                }
                $handlingCharges = FALSE;
				foreach ($products as $data) {
				$productName = trim($data['productName']);
				if($productName == 'SHIPPING CHARGES'){
						$handlingCharges = TRUE;
					}
				}
                $maximumPrice = $this->_scopeConfig->getValue("carriers/ecomexpress/shipping_subtotal");
                $shippingHandlingCharges = 0;
                $shippingHandlingChargesSet = 0;
                if ($order->getSubtotal() < $maximumPrice && $onlyFreePrescription == FALSE && $handlingCharges == TRUE) {
                    $shippingHandlingCharges = $this->_scopeConfig->getValue("carriers/ecomexpress/handling_charge");
                    $shippingHandlingChargesSet = $this->_scopeConfig->getValue("carriers/ecomexpress/handling_charge");
                }
                
                $order->setSubtotal($quote->getSubtotal());
                $order->setBaseSubtotal($quote->getBaseSubtotal());
                $order->setShippingAmount($shippingHandlingCharges);
                $order->setBaseShippingAmount($shippingHandlingCharges);
                $order->setGrandTotal($quote->getSubtotal() + $discountAmount + $shippingHandlingCharges);
                $order->setBaseGrandTotal($quote->getSubtotal() + $baseDiscountAmount + $shippingHandlingCharges);
                $order->setDiscountAmount($discountAmount);
                $order->setBaseDiscountAmount($baseDiscountAmount);

                $this->saveTransaction->addObject($order);
                $this->saveTransaction->save();
                $invoice = $this->_invoiceService->prepareInvoice($order);
                $invoice->register();
                $invoice->save();
                $transactionSave = $this->_transaction->addObject(
                                $invoice
                        )->addObject(
                        $invoice->getOrder()
                );
                $transactionSave->save();
                $order->addStatusHistoryComment(
                                __('Invoice has been created #%1.', $invoice->getId())
                        )
                        ->save();

                $payment = $order->getPayment();
                $paymentMethod = $payment->getMethod();
                if ($paymentMethod == 'paylater') {
                    $this->sendPaymentLink($order);
                }

                return $invoice->getId();
            }
        } catch (\Exception $ex) {
            $this->_data->criticalLog(__METHOD__, $ex->getMessage(), "OrderSyncApiException");
            return false;
        }
    }

    public function sendPaymentLink($order) {
        try {


            $jsonData = array(
                "appointmentId" => $order->getIncrementId(),
                "platform" => "ecom",
                "bookingType" => "ecom",
                "promotionId" => "",
                "promotionCode" => "",
            );
            $json = json_encode($jsonData, true);
            $paymentString = base64_encode($json);
            $storeManager = $this->objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            $store = $storeManager->getStore();
            $baseUrl = $store->getBaseUrl();
            $zoyloUrl = str_replace("medicines/", "", $baseUrl);
            $url = $zoyloUrl . "payment/" . $paymentString;
            $data = array("url" => $url);
            $data_string = json_encode($data);
            $apiUrl = $this->_scopeConfig->getValue('csquare/settings/url_shorten');
            $ch = curl_init($apiUrl);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json')
            );

            $result = curl_exec($ch);
            $resultData = json_decode($result, true);
            if (isset($resultData['success']) && $resultData['success'] == 1) {

                $order_id = $order->getId();
                $shortUrl = $resultData['data']['url'];
                //$order = $this->objectManager->create('Magento\Sales\Model\Order')->load($order_id);
                $customer = $this->objectManager->create('Magento\Customer\Model\Customer')->load($order->getCustomerId());
                $message = $this->_scopeConfig->getValue('csquare/settings/payment_notify');
                $sms = sprintf($message, $customer->getFirstname(), $order->getIncrementId(), $order->getBaseGrandTotal(), $shortUrl);
                $mobile = $customer->getCustomerNumber();
                $this->helperapi->callApiUrl($mobile, $sms);
                $order->setPaymentLink($shortUrl);
				$order->save();
                $order->addStatusHistoryComment(__('Payment link ( '.$shortUrl.' ) is sent to customer'))->save();
				$firstLineCont = "Please <a href='".$shortUrl."'>Click Here to Make Payment</a>";
				$sendPaymentLinkEmail = $this->sendPaymentLinkEmail($order,$firstLineCont);
                return true;
            }
        } catch (\Exception $ex) {
            $this->_data->criticalLog(__METHOD__, $ex->getMessage(), "Invoicesmsexception");
            return false;
        }
    }

    
    public function updateQuote($order, $products) {
        
        $quoteId = $order->getQuoteId();
        $quote = $this->quoteRepository->get($quoteId);
        $orderItems = $order->getAllItems();
        foreach ($order->getAllItems() as $magentoItem) {
            $sku = $magentoItem->getSku();
            if($sku != 'Free'){
                $productId = $magentoItem->getProductId();
                $qtyOrdered = intval($magentoItem->getQtyOrdered());
                $product = $this->_productCollectionFactory->create()->load($productId);
                $item_id = $product->getUcodeId();
                $price = number_format($magentoItem->getprice(), 2, '.', '');
                $quoteItemArray[$item_id] = $magentoItem->getId();
                $requestQtyArray[$item_id] = $qtyOrdered;
                $requestItemArray[$item_id] = $sku;
                $requestPriceArray[$item_id] = $price;
            }
        }
        $price = '';
        $qty = '';
        foreach ($products as $data) {
           $productId = trim($data['productId']);
            if ($productId != '0') {
                $discount = $data['itemTotal'] * ($data['discPer'] / 100) ;
                $qty = $data['qty'] / $data['qtyPerBox'];
                $price = $data['itemTotal'];
                $arrayCount[] = $data['productId'];
                if(isset($_SESSION['temp'])){
                if (array_key_exists($data['productId'], $_SESSION['temp'])) {
                    $price = $_SESSION['temp'][$data['productId']] + $data['itemTotal'] ;
                    $_SESSION['temp'][$data['productId']] = $price;
                 } else {
                    $_SESSION['temp'][$data['productId']] = $data['itemTotal'];
                 }
                }else {
                    $_SESSION['temp'][$data['productId']] = $data['itemTotal'];
                }
                $arrayProd = $_SESSION['temp'];
            }
        }
        $newPrice = '';
        $this->editedItems = array();
        foreach($arrayProd as $key => $value){
            $edited = false;
            $newPrice = $arrayProd[$key] / $requestQtyArray[$key];
            $orderItemId = trim($quoteItemArray[$key]);
            $orderItem = $order->getItemById($orderItemId);
            $quoteItem = $quote->getItemById($orderItem->getQuoteItemId());
            $quoteItem->setCustomPrice($newPrice);
            $quoteItem->setOriginalCustomPrice($newPrice);
            $quoteItem->setQty($requestQtyArray[$key]);
            $edited = true;
            if ($edited) {
                $this->editedItems[] = $orderItem;
                $quoteItem->save();
            }
        }
      

        // check if order is assigned to a customer and this customer exists
        if ($quote->getBillingAddress()
                // && $quote->getCustomerFirstname() // not registered customers do not have this value assigned
                && $quote->getCustomerGroupId() != 0 // default id for not logged in group is 0
                && !$quote->getCustomer()->getId()
        ) {
            $quote->getBillingAddress()->setCustomerAddressId(null);
            $quote->getBillingAddress()->setCustomerId(null);
        }

        $this->quoteRepository->save($quote);
        $quote->collectTotals();
        $quote->save();

        return $quote;
    }
  
    public function updateQuotebkp($order, $products) {
        $quoteId = $order->getQuoteId();
        $quote = $this->quoteRepository->get($quoteId);
        $orderItems = $order->getAllItems();
        foreach ($order->getAllItems() as $magentoItem) {
            $sku = $magentoItem->getSku();
            if($sku != 'Free'){
                $productId = $magentoItem->getProductId();
                $qtyOrdered = intval($magentoItem->getQtyOrdered());
                $product = $this->_productCollectionFactory->create()->load($productId);
                $item_id = $product->getUcodeId();
                $price = number_format($magentoItem->getprice(), 2, '.', '');
                $quoteItemArray[$item_id] = $magentoItem->getId();
                $requestQtyArray[$item_id] = $qtyOrdered;
                $requestItemArray[$item_id] = $sku;
                $requestPriceArray[$item_id] = $price;
            }
        }
        foreach ($products as $data) {
            $productId = trim($data['productId']);
            if ($productId != '0') {
                $responsePrice = $data['itemTotal'];
                $requestQty = $requestQtyArray[$productId];
                $sku = $requestItemArray[$productId];
                $edited = false;
                $orderItemId = trim($quoteItemArray[$productId]);
                $orderItem = $order->getItemById($orderItemId);
                $quoteItem = $quote->getItemById($orderItem->getQuoteItemId());
                $newprice = '';
                $newprice = $data['mrp'] * $data['qty'];
                $oldPrice = $requestPriceArray[$productId];

                $newQty = $data['qty'] / $data['qtyPerBox'];
                $oldQty = $requestQtyArray[$productId];

                // if ($newprice > $oldPrice) {
                $quoteItem->setCustomPrice($newprice);
                $quoteItem->setOriginalCustomPrice($newprice);
                $edited = true;
                // }

                if ($newQty < $oldQty) {
                    $productSku = $orderItem->getSku();
                    $oldQty = $quoteItem->getQty();
                    $newQty = $qty;
                    $this->updateProductStock($productSku, $oldQty, $newQty);

                    $quoteItem->setQty($qty);
                    $edited = true;
                }

                if ($edited) {
                    $this->editedItems[] = $orderItem;
                    $quoteItem->save();
                }
            }
        }

        // check if order is assigned to a customer and this customer exists
        if ($quote->getBillingAddress()
                // && $quote->getCustomerFirstname() // not registered customers do not have this value assigned
                && $quote->getCustomerGroupId() != 0 // default id for not logged in group is 0
                && !$quote->getCustomer()->getId()
        ) {
            $quote->getBillingAddress()->setCustomerAddressId(null);
            $quote->getBillingAddress()->setCustomerId(null);
        }

        $this->quoteRepository->save($quote);
        $quote->collectTotals();
        $quote->save();

        return $quote;
    }

    /**
     * For Update stock of product
     *
     * @param string $productSku which stock you want to update
     * @param int $productQty your updated data
     * @return void
     */
    protected function updateProductStock($productSku, $oldQty, $newQty) {
        $stockItem = $this->stockRegistry->getStockItemBySku($productSku);
        $qty = $stockItem->getQty() + $oldQty - $newQty;
        $stockItem->setQty($qty);
        $stockItem->setIsInStock((bool) $qty);
        $this->stockRegistry->updateStockItemBySku($productSku, $stockItem);
    }

    /**
     * Use updated quote to create a dummy order items and set required data fields for existed ones.
     *
     * @param $quote
     */
    public function updateOrderItems($quote) {
        
        foreach ($this->editedItems as $orderItem) {
            $quoteItem = $quote->getItemById($orderItem->getQuoteItemId());
//            $dummyOrderItem = $this->toOrderItem->convert($quoteItem, []);
             //   echo $quoteItem->getQty();die;
            $buyRequest = $orderItem->getBuyRequest()->setQty($quoteItem->getQty());
            $orderItem->setName($quoteItem->getName());
            $orderItem->setDescription($quoteItem->getDescription());
            $orderItem->setQtyOrdered($quoteItem->getQty());
            $orderItem->setBuyRequest($buyRequest);
            $orderItem->setPrice($quoteItem->getPrice());
            $orderItem->setBasePrice($quoteItem->getBasePrice());
            $orderItem->setRowTotal($quoteItem->getRowTotal());
            $orderItem->setBaseRowTotal($quoteItem->getBaseRowTotal());
            $orderItem->setRowTotalInclTax($quoteItem->getRowTotalInclTax());
            $orderItem->setBaseRowTotalInclTax($quoteItem->getBaseRowTotalInclTax());
            $orderItem->setTaxAmount($quoteItem->getTaxAmount());
            $orderItem->setProductOptions($quoteItem->getProductOptions());
            $orderItem->setDiscountAmount($quoteItem->getDiscountAmount());
            $this->saveTransaction->addObject($orderItem);
        }
    }

    /**
     *
     * Order Canceled api call
     *
     */
    public function cancelOrder($order, $getRecords) {
        $isEnabled = $this->_scopeConfig->getValue('csquare/settings/enabled', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if ($isEnabled) {
            $cancelApiUrl = $this->_scopeConfig->getValue('csquare/settings/order_create', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            if ($cancelApiUrl != '') {
                try {
                    foreach ($getRecords as $msgId) {
                        $target_order_id = $msgId['target_order_id'];
                        $processId = $msgId['id'];
                    }
                    $incrementId = $order->getIncrementId();
                    $orderStatus = $order->getStatus();
                    $requestText = "cIndex=cancel&so_no=" . $target_order_id . "";
                    $entity = 'order';
                    $entityApiClass = $this->_data->getApiClass($entity);
                    $entityApiClass = $entityApiClass->create();
                    $ch = curl_init($cancelApiUrl);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $requestText);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/x-www-form-urlencoded'
                            //'Content-Length: ' . strlen($data_string)
                            )
                    );
                    $response = curl_exec($ch);
                    $this->_data->infoLog(__METHOD__, "Ecogreen " . $entity . " has been created " . date('Y-m-d H:i:s'), 'OrderSync');
                    $result = json_decode($response, true);

                    // Updating records in Order Report Table
                    $ecoGreenStatus = $result[0]['status'];
                    $cmessage = $result[0]['cmessage'];
                    if ($ecoGreenStatus == "Error") {
                        $entityApiClass->load($processId);
                        $entityApiClass->setUpdatedAt($this->_dataTime->gmtDate());
                        $entityApiClass->setResponse($cmessage);
                        $entityApiClass->setStatus(3);
                        $entityApiClass->setFailureStatus(1);
                        $entityApiClass->setMagentoOrderStatus($orderStatus);
                        $entityApiClass->save();
                        $this->_data->infoLog(__METHOD__, "Order Report Table has been updated with error value " . date('Y-m-d H:i:s'), 'OrderSync');
                    } else {
                        $ecoGreenOrderStatus = "Order Cancelled";
                        $entityApiClass->load($processId);
                        $entityApiClass->setUpdatedAt($this->_dataTime->gmtDate());
                        $entityApiClass->setResponse($cmessage);
                        $entityApiClass->setStatus(4);
                        $entityApiClass->setFailureStatus(0);
                        $entityApiClass->setMagentoOrderStatus($orderStatus);
                        $entityApiClass->setFullfilmentStatus($ecoGreenOrderStatus);
                        $entityApiClass->save();
                        $this->_data->infoLog(__METHOD__, "Order Report Table has been updated with success value " . date('Y-m-d H:i:s'), 'OrderSync');
                    }
                    return true;
                } catch (\Exception $ex) {
                    $this->_data->criticalLog(__METHOD__, $ex->getMessage(), "OrderSyncApiException");
                    return false;
                }
            }
        }
    }

    public function sendEmail()
    {
        $templateId = 'order_sync_fails';
		$entity = 'order';
		$entityApiClass = $this->_data->getApiClass($entity);
		$entityApiClass = $entityApiClass->create();

		// Order Pending Iteration Started
		$getRecordsO = $entityApiClass->getCollection()
			->addFieldtoFilter('fullfilment_status', ['in' => ['Error']]);
		$getRecordsO->addFieldtoFilter('status', ['nin' => [4]]);
		//$getRecordsO->printlogquery(true);die;
		//$getRecordsO->setPageSize(50)->setCurPage(1);

		$getRecords = $getRecordsO->getData();
		if ($getRecordsO->getSize() > 0) {
			$errorMsg = '';
			$errorMsg .= '<table style="width:100%"  border="1" bordercolor="#000" cellpadding="10" cellspacing="0" >
					<tr><th>Order Id</th><th>Error</th></tr>';
			foreach ($getRecords as $msgId) {
				$processId = $msgId['id'];
				$orderId = $msgId['source_order_id'];
				$response = $msgId['response'];
				$errorMsg .= '<tr><td>'.$orderId.'</td><td>'.$response.'</td></tr>';
			}
			$errorMsg .= '</table>';
			$vars = [
				'errorMsg' => $errorMsg
			];
			$sender     = array('email' => 'noreply@zoylo.com','name' => 'Zoylo');
			$toEmailIds = $this->_scopeConfig->getValue('csquare/settings/order_sync_fails_emails', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$toEmailIds = explode(',', $toEmailIds);
			try {
				$transport      = $this->transportBuilder->setTemplateIdentifier($templateId)
					->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND,'store' => 1])
					->setTemplateVars($vars)
					->setFrom($sender)
					->addTo($toEmailIds);
				$transport = $transport->getTransport();
				$transport->sendMessage();
				//return true;
			} catch (\Exception $ex) {
				$this->_data->criticalLog(__METHOD__, $ex->getMessage(), "OrderSyncApiException");
				//return false;
			}
		}
    }
	
	public function sendPaymentLinkEmail($order,$firstLineCont)
    {
        $vars = [
            'order' => $order,
            'store' => $order->getStore(),
            'firstline'=>$firstLineCont
        ];
        
        $sender         = array('email' => 'noreply@zoylo.com','name' => 'Zoylo');
		try {
			$transport      = $this->transportBuilder->setTemplateIdentifier('customer_payment_pending_link')
				->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND,'store' => 1])
				->setTemplateVars($vars)
				->setFrom($sender)
				->addTo(array('To' => $order->getCustomerEmail()));
			$transport = $transport->getTransport();
			$transport->sendMessage();
			return true;
		} catch (\Exception $ex) {
			$this->_data->criticalLog(__METHOD__, $ex->getMessage(), "OrderSyncApiException");
			return false;
		}
    }
}