<?php
namespace Zoylo\Csquare\Model\Product\Attribute\Source;

class ZoyloProductGroup extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
    
		$this->_options=[ ['label'=>'-- Please Select --', 'value'=>''],
			['label'=>'MEDICINE', 'value'=>'medicine'],
			['label'=>'AYURVEDIC', 'value'=>'ayurvedic'],
                        ['label'=>'HOMEOPATHY', 'value'=>'Homeopathy'],
			['label'=>'CONSUMER', 'value'=>'consumer'],
			['label'=>'SURGICAL', 'value'=>'surgical'],
			['label'=>'GENERAL', 'value'=>'general'],
			['label'=>'VETERINARY PRODUCTS', 'value'=>'veterinary_products'],
			['label'=>'Healthcare Products', 'value'=>'healthcare_products']
		];
        return $this->_options;
    }
}
