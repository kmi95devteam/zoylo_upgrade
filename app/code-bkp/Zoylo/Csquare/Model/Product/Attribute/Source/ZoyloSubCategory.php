<?php
namespace Zoylo\Csquare\Model\Product\Attribute\Source;

class ZoyloSubCategory extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
    
		$this->_options=[ ['label'=>'-- Please Select --', 'value'=>'']
		];
        return $this->_options;
    }
}
