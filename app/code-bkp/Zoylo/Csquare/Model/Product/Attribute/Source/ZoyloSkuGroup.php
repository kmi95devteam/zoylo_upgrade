<?php
namespace Zoylo\Csquare\Model\Product\Attribute\Source;

class ZoyloSkuGroup extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
    
		$this->_options=[ ['label'=>'-- Please Select --', 'value'=>''],
			['label'=>'Medicine', 'value'=>'medicine'],
			['label'=>'OTC', 'value'=>'otc']
		];
        return $this->_options;
    }
	
	/**
     * Retrieve option array
     *
     * @return string[]
     */
    public static function getOptionArray()
    {
        return array("medicine" => "Medicine", "otc" => "OTC");
    }
}
