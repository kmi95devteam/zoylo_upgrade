<?php


namespace Zoylo\Csquare\Model\Product\Attribute\Source;

class StorageType extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        $this->_options = [
        ['label'=>'-- Please Select --', 'value'=>''],
        ['label'=>'Normal', 'value'=>'normal'],
        ['label'=>'Cold Storage', 'value'=>'cold_storage'],
        ['label'=>'Fragile', 'value'=>'fragile'],
        ['label'=>'Heavy/Bulky', 'value'=>'heavy_bulky']
        ];
        return $this->_options;
    }
}
