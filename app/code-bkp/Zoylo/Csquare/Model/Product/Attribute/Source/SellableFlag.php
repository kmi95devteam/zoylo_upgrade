<?php
namespace Zoylo\Csquare\Model\Product\Attribute\Source;

class SellableFlag extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
    
		$this->_options=[ ['label'=>'Yes', 'value'=> 1],
			['label'=>'Banned', 'value'=> 0],
                    ['label'=>'Discontinued', 'value'=> 2]
		];
        return $this->_options;
    }
}
