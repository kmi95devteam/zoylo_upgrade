<?php
namespace Zoylo\Csquare\Setup;
use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

class UpgradeData implements UpgradeDataInterface {

    private $eavSetupFactory;
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
		
        if (version_compare($context->getVersion(), '2.0.11', '<')) {		
	
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'top_sku_position');
		$eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'top_sku_position',
            [
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'Top Sku Position',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'General',
                'option' => ['values' => [""]]
            ]
        );
		
		}
    }

}
