<?php

namespace Zoylo\Csquare\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('csquare_product_report');
        // Check if the table already exists
        if ($installer->getConnection()->isTableExists($tableName) != true) {
            // Create csquare_product_report table
            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'sku',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Product Sku'
                )
                ->addColumn(
                    'ucode_id',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Ucode Id'
                )
                ->addColumn(
                    'product_data',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Product Data'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false],
                    'Created At'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false],
                    'Updated At'
                )
                ->addColumn(
                    'status',
                    Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false],
                    'Status'
                )
                ->setComment('CSquare Product Report')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
        }
		
		
		$ordertableName = $installer->getTable('csquare_order_report');
        // Check if the table already exists
        if ($installer->getConnection()->isTableExists($ordertableName) != true) {
           
            $table1 = $installer->getConnection()
                ->newTable($ordertableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'source_order_id',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Magento Order Id'
                )
                ->addColumn(
                    'target_order_id',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Csquare Order Id'
                )
                ->addColumn(
                    'fullfilment_status',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Fullfilment status'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false],
                    'Created At'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false],
                    'Updated At'
                )
                ->addColumn(
                    'status',
                    Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false],
                    'Status'
                )
                ->setComment('CSquare Order Report')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table1);
        }
        
        
        if ($installer->getConnection()->isTableExists('unicommerce_failed_sync_orders') != true) {
           
            $table2 = $installer->getConnection()
                ->newTable('unicommerce_failed_sync_orders')
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'order_id',
                    Table::TYPE_INTEGER,
                    11,
                    [
                        'nullable' => false
                    ],
                    'Magento Order Id'
                )
                ->addColumn(
                    'status',
                    Table::TYPE_INTEGER,
                    1,
                    [
                        'nullable' => false
                    ],
                    'Status 0 means failed, 1 means successfully synced'
                )
                ->addColumn(
                    'message',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Message'
                );
            $installer->getConnection()->createTable($table2);
        }
        
        if ($installer->getConnection()->isTableExists('facility_code_mapping') != true) {
           
            $table3 = $installer->getConnection()
                ->newTable('facility_code_mapping')
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )->addColumn(
                    'branch_id',
                    Table::TYPE_INTEGER,
                    11,
                    [
                        'nullable' => false
                    ],
                    'Branch ID'
                )->addColumn( 
                    'facility_code',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Facility Code'
                )->addColumn(
                    'unicommerce_store',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Unicommerce Store Code'
                );
            $installer->getConnection()->createTable($table3);
        }
        
        if ($installer->getConnection()->isTableExists('unicommerce_failed_sync_orders') != true) {
           
            $table4 = $installer->getConnection()
                ->newTable('unicommerce_failed_sync_orders')
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )->addColumn(
                    'order_id',
                    Table::TYPE_INTEGER,
                    11,
                    [
                        'nullable' => false
                    ],
                    'Order ID'
                )->addColumn( 
                   'status',
                    Table::TYPE_INTEGER,
                    1,
                    [
                        'nullable' => false
                    ],
                    'Status'
                );
            $installer->getConnection()->createTable($table4);
        }
        

        $installer->endSetup();
    }
}