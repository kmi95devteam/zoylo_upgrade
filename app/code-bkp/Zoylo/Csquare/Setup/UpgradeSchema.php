<?php

namespace Zoylo\Csquare\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();

        $product_report = 'csquare_product_report';
        $order_report = 'csquare_order_report';
        $sales_order = 'sales_order';
        $sales_order_grid = 'sales_order_grid';

        if (version_compare($context->getVersion(), '2.0.8', '<')) {
            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($product_report), 'response', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Response'
                            ]
            );
            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($product_report), 'failure_status', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                        'length' => 6,
                        'comment' => 'Failure Status'
                            ]
            );
            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($order_report), 'entity_id', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Order Entity Id'
                            ]
            );
            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($order_report), 'response', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Response'
                            ]
            );
            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($order_report), 'failure_status', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                        'length' => 6,
                        'comment' => 'Failure Status'
                            ]
            );

            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($sales_order), 'payment_status', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Payment Status'
                            ]
            );
        }
        if (version_compare($context->getVersion(), '2.0.9', '<')) {

            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($order_report), 'magento_order_status', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Magento Order Status'
                            ]
            );
        }

        if (version_compare($context->getVersion(), '2.0.10', '<')) {

            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($order_report), 'updated_by', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Updated By'
                            ]
            );
        }

        if (version_compare($context->getVersion(), '2.2.1', '<')) {

            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($sales_order), 'cancellation_reason', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Reason for order cancellation']
            );
            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($sales_order), 'order_cancelled_by', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Order cancelled by']
            );

            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($sales_order_grid), 'cancellation_reason', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Reason for order cancellation']
            );
            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($sales_order_grid), 'order_cancelled_by', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Order cancelled by']
            );
        }

        if (version_compare($context->getVersion(), '2.2.3', '<')) {

            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($product_report), 'price', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        'length' => '12,4',
                        'comment' => 'Price'
                            ]
            );
            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($product_report), 'special_price', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        'length' => '12,4',
                        'comment' => 'Special Price'
                            ]
            );
        }

        if (version_compare($context->getVersion(), '2.2.4', '<')) {

            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($sales_order), 'qc_status', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'QC Status'
                            ]
            );
            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($sales_order), 'qc_user', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Qc User'
                            ]
            );

            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($sales_order_grid), 'qc_status', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'QC Status'
                            ]
            );
            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($sales_order_grid), 'qc_user', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Qc User'
                            ]
            );

            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($order_report), 'csquare_invoice_id', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Csquare Invoice Id'
                            ]
            );
            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($sales_order), 'assigned_user', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Assigned User'
                            ]
            );

            $setup->getConnection()
                    ->addColumn(
                            $setup->getTable($sales_order_grid), 'assigned_user', [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'comment' => 'Assigned User'
                            ]
            );

            if (!$setup->tableExists('csquare_on_hold_report')) {
                $table = $setup->getConnection()->newTable(
                                        $setup->getTable('csquare_on_hold_report'))
                                ->addColumn(
                                        'id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, [
                                    'identity' => true,
                                    'nullable' => false,
                                    'primary' => true,
                                    'unsigned' => true,
                                        ], 'ID'
                                )
                                ->addColumn(
                                        'product_sku', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable => false'], 'SKU'
                                )
                                ->addColumn(
                                        'order_id', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Order ID'
                                )
                                ->addColumn(
                                        'reason', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Reason'
                                )
                                ->addColumn(
                                        'diff_qty', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Difference Qty'
                                )
                                ->addColumn(
                                        'diff_price', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Difference Price'
                                )
                                ->addColumn(
                                        'created_date', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT], 'Created Date'
                                )->setComment('Csquare On Hold Report Table');
                $setup->getConnection()->createTable($table);
            }
        }
		
		if (version_compare($context->getVersion(), '2.2.5', '<')) {
			$setup->getConnection()
               ->addColumn(
               $setup->getTable($sales_order),
               'assigned_user',
				[
                   'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                   'length' => 255,
                   'comment' =>'Assigned User'
				]
            );
		   
		   $setup->getConnection()
               ->addColumn(
               $setup->getTable($sales_order_grid),
               'assigned_user',
				[
                   'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                   'length' => 255,
                   'comment' =>'Assigned User'  
				]
            );
                   

		}
	
                if (version_compare($context->getVersion(), '2.2.8', '<')) {
                    $setup->getConnection()
                     ->addColumn(
                        $setup->getTable('unicommerce_failed_sync_orders'),
                        'increment_id',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 255,
                            'comment' =>'Increment ID'  
                        ]
                     );
                    $setup->getConnection()
                     ->addColumn(
                        $setup->getTable('unicommerce_failed_sync_orders'),
                        'sort_code',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 255,
                            'comment' =>'Sort Code'  
                        ]
                     );
                    $setup->getConnection()
                    ->addColumn(
                        $setup->getTable('unicommerce_failed_sync_orders'),
                        'label',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 255,
                            'comment' =>'Label'  
                        ]
                     );
                    $setup->getConnection()
                    ->addColumn(
                        $setup->getTable('unicommerce_failed_sync_orders'),
                        'security_key',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 255,
                            'comment' =>'Security Key'  
                        ]
                     );
                    $setup->getConnection()
                     ->addColumn(
                        $setup->getTable('unicommerce_failed_sync_orders'),
                        'reference_number',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 255,
                            'comment' =>'Reference Number'  
                        ]
                     );
                    $setup->getConnection()
                     ->addColumn(
                        $setup->getTable('unicommerce_failed_sync_orders'),
                        'waybill',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 255,
                            'comment' =>'Waybill'  
                        ]
                     );
                     $setup->getConnection()       
                     ->addColumn(
                        $setup->getTable('unicommerce_failed_sync_orders'),
                        'tracking_id',
                         [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 255,
                            'comment' =>'Tracking ID'  
                         ]
                     );
                     
                     $setup->getConnection()       
                     ->addColumn(
                        $setup->getTable('unicommerce_failed_sync_orders'),
                        'clickpost_order_id',
                         [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 255,
                            'comment' =>'Clickpost Order ID'  
                         ]
                     );
                     
                     $setup->getConnection()       
                     ->addColumn(
                        $setup->getTable('facility_code_mapping'),
                        'address',
                         [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 500,
                            'comment' =>'Complete address'  
                         ]
                     );
                     
                     $setup->getConnection()       
                     ->addColumn(
                        $setup->getTable('facility_code_mapping'),
                        'email',
                         [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 255,
                            'comment' =>'Facility Contact Email'  
                         ]
                     );
                     
                     $setup->getConnection()       
                     ->addColumn(
                        $setup->getTable('facility_code_mapping'),
                        'pincode',
                         [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            'length' => 11,
                            'comment' =>'Facility Pincode.'  
                         ]
                     );
                     
                     $setup->getConnection()       
                     ->addColumn(
                        $setup->getTable('facility_code_mapping'),
                        'city',
                         [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 50,
                            'comment' =>'Facility City'  
                         ]
                     );
                     
                     $setup->getConnection()       
                     ->addColumn(
                        $setup->getTable('facility_code_mapping'),
                        'country',
                         [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 50,
                            'comment' =>'Facility Country'  
                         ]
                     );
                     
                     $setup->getConnection()       
                     ->addColumn(
                        $setup->getTable('facility_code_mapping'),
                        'phone',
                         [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 50,
                            'comment' =>'Facility Phone'  
                         ]
                     );
                     
                     $setup->getConnection()       
                     ->addColumn(
                        $setup->getTable('facility_code_mapping'),
                        'state',
                         [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 50,
                            'comment' =>'Facility State'  
                         ]
                     );
                     
                     $setup->getConnection()       
                     ->addColumn(
                        $setup->getTable('facility_code_mapping'),
                        'name',
                         [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 60,
                            'comment' =>'Facility Name'  
                         ]
                     );
                     
                }
                
                if (version_compare($context->getVersion(), '2.2.11', '<')) {

                    $setup->getConnection()
                            ->addColumn(
                                $setup->getTable($sales_order), 'order_return_date_time', [
                                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                                [],
                                'comment' => 'Order return initiated date and time']
                    );
                    $setup->getConnection()
                            ->addColumn(
                                    $setup->getTable($sales_order), 'order_return_reason', [
                                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                                'length' => 500,
                                'comment' => 'Order Return Reason']
                    );

                    $setup->getConnection()
                            ->addColumn(
                                $setup->getTable($sales_order_grid), 'order_return_date_time', [
                                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                                [],
                                'comment' => 'Order return initiated date and time']
                    );
                    $setup->getConnection()
                            ->addColumn(
                                    $setup->getTable($sales_order_grid), 'order_return_reason', [
                                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                                'length' => 500,
                                'comment' => 'Order Return Reason']
                    );
                }
                
                if (version_compare($context->getVersion(), '2.2.12', '<')) {
                    $setup->getConnection()       
                     ->addColumn(
                        $setup->getTable('facility_code_mapping'),
                        'return_address',
                         [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'length' => 500,
                            'comment' =>'Return Complete address'  
                         ]
                     );
                }
        $setup->endSetup();
    }

}
