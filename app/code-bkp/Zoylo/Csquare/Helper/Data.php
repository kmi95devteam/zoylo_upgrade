<?php
namespace Zoylo\Csquare\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	/**
     * scopeConfig for system Congiguration
     *
     * @var string
     */
    protected $_scopeConfig;
	
	/**
      *@var \Zoylo\Csquare\Model\ProductsFactory
     */
    protected $_productsFactory;
	
    /**
     * @var \Zoylo\Csquare\Model\OrdersFactory
     */
    protected $_ordersFactory;
    
    protected $branchFactory;
    
    protected $resourceConnection;

    protected $unicommerceFailedSync;
    
    protected $timezoneDate;
    
    protected $orderConvert;
    
    protected $trackFactory;
    
    protected $smshelperdata;
    
    protected $customerFactory;
    
    protected $transportBuilder;
    
    protected $addressRenderer;
    
    protected $paymentHelper;
    
    protected $identityContainer;
    
    protected $smshelperapi;
    /**
     * 
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	 * @param \Zoylo\Csquare\Model\ProductsFactory $productsFactory
     */
	 
	public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Zoylo\Csquare\Model\OrdersFactory $ordersFactory,
	\Zoylo\Csquare\Model\ProductsFactory $productsFactory,
        \I95Dev\BranchInformation\Model\BranchFactory $branchFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Zoylo\Csquare\Model\UnicommerceFailedSyncFactory $unicommerceFailedSync,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneDate,
        \Magento\Sales\Model\Order $salesOrder,
        \Magento\Sales\Model\Convert\Order $orderConvert,
        \Magento\Sales\Model\Order\Shipment\TrackFactory $trackFactory,
        \Magecomp\Smsfree\Helper\Data $smshelperdata,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Payment\Helper\Data $paymentHelper,
        \Magento\Sales\Model\Order\Email\Container\OrderIdentity $identityContainer,
        \Magecomp\Smsfree\Helper\Apicall $smshelperapi
        ) 
        {
          $this->_scopeConfig = $scopeConfig;
          $this->_productsFactory = $productsFactory;
          $this->_ordersFactory = $ordersFactory;
          $this->branchFactory = $branchFactory;
          $this->resourceConnection = $resourceConnection;
          $this->unicommerceFailedSync = $unicommerceFailedSync;
          $this->timezoneDate = $timezoneDate;
          $this->orderConvert = $orderConvert;
          $this->trackFactory = $trackFactory;
          $this->smshelperdata = $smshelperdata;
          $this->customerFactory = $customerFactory;
          $this->transportBuilder = $transportBuilder;
          $this->addressRenderer = $addressRenderer;
          $this->paymentHelper= $paymentHelper;
          $this->identityContainer = $identityContainer;
          $this->smshelperapi = $smshelperapi;
	 } 
	 
	/**
     * check whether the Zoylo_Csquare is enabled or not
     * @return boolean
     */
    public function isEnabled()
    {
        $isEnabled = $this->_scopeConfig
                    ->getValue('csquare/settings/enabled', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $isEnabled;
    }
	
	 /*     * *
     * Info Log
     */

    public function infoLog($logArea, $message, $logName)
    {
        try {
            $writer = new \Zend\Log\Writer\Stream(BP . "/var/log/" . $logName . "_" . date('Y-m-d') . ".log");
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info($logArea);
            if (is_array($message) || is_object($message)) {
                $message = json_encode($message);
            }
            $logger->info($message);
        } catch (\Exception $exc) {
            echo $exc->getTraceAsString();
            $this->criticalLog(__METHOD__, $exc->getMessage(), 'infoLog');
        }
    }
	
	/* * *
     * Crtical Log
     */

    public function criticalLog($logArea, $message, $logName)
    {
        try {
            $writer = new \Zend\Log\Writer\Stream(BP . "/var/log/" . $logName . "_" . date('Y-m-d') . ".log");
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->crit($logArea);
            if (is_array($message) || is_object($message)) {
                $message = json_encode($message);
            }
            $logger->crit($message);
        } catch (\Exception $ex) {
            $this->criticalLog(__METHOD__, $ex->getMessage(), 'criticalLog');
        }
    }

    /* * *
     * debuglLog
     */
    public function debugLog($logArea, $message, $logName)
    {
        try {
            $writer = new \Zend\Log\Writer\Stream(BP . "/var/log/" . $logName . "_" . date('Y-m-d') . ".log");
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->debug($logArea);
            if (is_array($message) || is_object($message)) {
                $message = json_encode($message);
            }
            $logger->debug($message);
        } catch (\Exception $ex) {
            $this->criticalLog(__METHOD__, $ex->getMessage(), 'debugLog');
        }
    }

    /* * *
     * Error Log
     */
    public function errorLog($logArea, $message, $logName)
    {
        try {
            $writer = new \Zend\Log\Writer\Stream(BP . "/var/log/" . $logName . "_" . date('Y-m-d') . ".log");
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->err($logArea);
            if (is_array($message) || is_object($message)) {
                $message = json_encode($message);
            }
            $logger->err($message);
        } catch (\Exception $ex) {
            $this->criticalLog(__METHOD__, $ex->getMessage(), 'errorLog');
        }
    }
	
	public function getApiClass($entity)
    {
        try {
            switch ($entity) {
                case "product":
                    $classObject = $this->_productsFactory;
                    break;
                case "order":
                    $classObject = $this->_ordersFactory;
                    break;
                
                default:
                    return false;
            }
        } catch (\Exception $ex) {
            $this->helper->criticalLog(__METHOD__, $ex->getMessage(), 'Exception');
        }
        return $classObject;
    }
    
    public function syncToUniware($order)
    {
        try{
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $headerDetailsCh = curl_init("https://zdpl.unicommerce.com/oauth/token?grant_type=password&client_id=my-trusted-client&username=".$this->_scopeConfig->getValue('unicommerce_settings/zoylo/unicommerce_email', $storeScope)."&password=".$this->_scopeConfig->getValue('unicommerce_settings/zoylo/unicommerce_password', $storeScope));
            curl_setopt($headerDetailsCh, CURLOPT_CUSTOMREQUEST, "GET");   
            curl_setopt($headerDetailsCh,CURLOPT_RETURNTRANSFER,1);
            $tokenResult = curl_exec($headerDetailsCh);
            $tokenResult2 = explode(',',$tokenResult);
            $tokenResult3 = json_encode($tokenResult2[0]);
            $tokenFinal = explode(":",$tokenResult2[0])[1];
            $tokenFinal = str_replace('"', '', $tokenFinal);
            curl_close($headerDetailsCh);

            $cashOnDelivery = true;
            $condDataAll = $this->getConditionalData($order);
            $cashPayMethods = array('cashondelivery','free');
            if (isset($condDataAll['payment_method']) && !in_array($condDataAll['payment_method'],$cashPayMethods) )
            {
                $cashOnDelivery = false;
            }
            $allOrderItems = $order->getAllItems();
            $orderItemData = array();
            $shipPinCode = $order->getShippingAddress()->getPostCode();
            $branchCollection = $this->branchFactory->create()->getCollection();
            $branchCollection->getSelect()->joinLeft(array('facility_table'=>$this->resourceConnection->getTableName('facility_code_mapping')),'main_table.branch_code=facility_table.branch_id')->where("main_table.zip_code ='".$shipPinCode."'");
            $facilityCenter = '';
            if (isset($branchCollection->getData()[0]['facility_code']))
            {
                $facilityCenter = $branchCollection->getData()[0]['facility_code'];
            }
            /*foreach($allOrderItems as $eachROrderItem) //keep this code commented until testing is going on, uncomment when we get go ahead for making the code dynamic
            {
                $orderItemData[] = array("itemSku"=>$eachROrderItem->getSku(),"sellingPrice"=>$eachROrderItem->getPrice(),"shippingMethodCode"=>"STD","totalPrice"=>$eachROrderItem->getRowTotal(),"code"=>"90004443","facilityCode"=>"VIVA-MASSABTANK");
            }*/
            //10200375
            //$facilityCenter = 'Warehouse';
            //die('firstnmae:'.$order->getShippingAddress()->getFirstName());
            //die('discount:'.$order->getDiscountAmount());
            $streetArr = $order->getShippingAddress()->getStreet();
            $street1 = "";
            if (isset($streetArr[0]))
            {
                $street1 = $streetArr[0];
            }
            $street2 = "";
            if (isset($streetArr[1]))
            {
                $street2 = $streetArr[1];
            }
            $orderItemData[] = array("itemSku"=>'99999999',"sellingPrice"=>$order->getSubtotal()+$order->getDiscountAmount(),"shippingMethodCode"=>"STD","discount"=>abs($order->getDiscountAmount()),"shippingCharges"=>$order->getShippingAmount(),"totalPrice"=>$order->getGrandTotal(),"code"=>"90004443","facilityCode"=>$facilityCenter);
            $dataString = array("saleOrder"=>array("code"=>$order->getIncrementId(),"displayOrderCode"=>$order->getIncrementId(),"cashOnDelivery"=>$cashOnDelivery,"addresses"=>array(array("id"=>"0","name"=>$order->getCustomerFirstname()." ".$order->getCustomerLastname() ,"addressLine1"=>$street1,"addressLine2"=>$street2,"city"=>$order->getShippingAddress()->getCity(),"state"=>$order->getShippingAddress()->getRegion(),"country"=>"India","pincode"=>str_replace(' ','',$order->getShippingAddress()->getPostCode()),"phone"=>$order->getShippingAddress()->getTelephone())),"billingAddress"=>array("referenceId"=>0),"shippingAddress"=>array("referenceId"=>0),"saleOrderItems"=>$orderItemData));
            $jsonReq = json_encode($dataString);
            $ch = curl_init("https://zdpl.unicommerce.com/services/rest/v1/oms/saleOrder/create");
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonReq);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($jsonReq),
                'Authorization: Bearer '.$tokenFinal)
            );
            $result = curl_exec($ch);
            $responseString = str_replace('\n', '', $result);
            $responseString = rtrim($responseString, ',');
            $responseString = "[" . trim($responseString) . "]";
            $json = json_decode($responseString, true);

            if (curl_errno($ch)) { // if curl request failed
                $unicommerceFailedMdl = $this->unicommerceFailedSync->create()->load($order->getEntityId(),'order_id');
                if (!$unicommerceFailedMdl->getId())
                {
                    $unicommerceFailedMdl = $this->unicommerceFailedSync->create();
                }
                $unicommerceFailedMdl->setOrderId($order->getEntityId());
                $unicommerceFailedMdl->setStatus(0);
                $unicommerceFailedMdl->setMessage('Curl Connection Failed');
                $unicommerceFailedMdl->save();
                //die("error in curl");
            }
            else if ( isset($json[0]['successful']) && $json[0]['successful'] == true  ){ // everything correct
                $unicommerceFailedMdl = $this->unicommerceFailedSync->create()->load($order->getEntityId(),'order_id');
                if (!$unicommerceFailedMdl->getId())
                {
                    $unicommerceFailedMdl = $this->unicommerceFailedSync->create();
                }
                $unicommerceFailedMdl->setOrderId($order->getEntityId());
                $unicommerceFailedMdl->setStatus(1);
                $unicommerceFailedMdl->setMessage('Synced Successfully.');
                $unicommerceFailedMdl->save();
            }
            else{ // something wrong
                $unicommerceFailedMdl = $this->unicommerceFailedSync->create()->load($order->getEntityId(),'order_id');
                if (!$unicommerceFailedMdl->getId())
                {
                    $unicommerceFailedMdl = $this->unicommerceFailedSync->create();
                }
                $unicommerceFailedMdl->setOrderId($order->getEntityId());
                $unicommerceFailedMdl->setStatus(0);
                $unicommerceFailedMdl->setMessage('Failed To Sync To Unicommerce. Reason: '.$result);
                $unicommerceFailedMdl->save();                    
            }
            curl_close($ch);
        }catch(\Exception $e){
            echo "Exception:".$e->getMessage();
        }
    }
    
    public function syncToClickPost($order)
    {
        try
        {
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $pickupDate =  $this->timezoneDate->date()->format('Y-m-d'); // default date of today
            if ($this->timezoneDate->date()->format('H:i:s') < $this->timezoneDate->date('10:30:00')->format('H:i:s'))
            {
               $pickupDate = date('Y-m-d',strtotime($this->timezoneDate->date()->format('Y-m-d').' +1 Day'));
            }
            else
            {
                $pickupDate = date('Y-m-d',strtotime($this->timezoneDate->date()->format('Y-m-d').' +2 Day'));
            }
            $pickupDate = date ('Y-m-d H:i:s',strtotime($pickupDate." ".$this->_scopeConfig->getValue('clickpost_settings/zoylo/clickpost_pickup_time', $storeScope)));
            $unicommerceFailedMdl = $this->unicommerceFailedSync->create()->load($order->getIncrementId(),'increment_id');
            if (!$unicommerceFailedMdl->getId())
            {
                $unicommerceFailedMdl = $this->unicommerceFailedSync->create();
            }
            $paymentMethodDT = "COD";
            $codValue = $order->getGrandTotal();
            $condDataAll = $this->getConditionalData($order);
            $cashPayMethods = array('cashondelivery','free');
            if (isset($condDataAll['payment_method']) && !in_array($condDataAll['payment_method'],$cashPayMethods) )
            {
                $paymentMethodDT = "PREPAID";
                $codValue = 0;
            }
            $shipPinCode = $order->getShippingAddress()->getPostCode();
            $branchCollection = $this->branchFactory->create()->getCollection();
            $branchCollection->getSelect()->joinLeft(array('facility_table'=>$this->resourceConnection->getTableName('facility_code_mapping')),'main_table.branch_code=facility_table.branch_id')->where("main_table.zip_code ='".$shipPinCode."'");
            $facilityCenter = '';
            if (isset($branchCollection->getData()[0]['facility_code']))
            {
                $facilityCenter = $branchCollection->getData()[0]['facility_code'];
            }
            $customerToShipAddress = "";
            //$customerToShipAddress = $customerToShipAddress.$order->getShippingAddress()->getFirstname().", ".$order->getShippingAddress()->getLastname();
            $streetAddress = $order->getShippingAddress()->getStreet();
            if (isset($streetAddress[0]) && $streetAddress[0] != '')
            {
                $customerToShipAddress =  $customerToShipAddress.", ".$streetAddress[0];
            }
            if (isset($streetAddress[1]) && $streetAddress[1] != '')
            {
                $customerToShipAddress =  $customerToShipAddress.", ".$streetAddress[1];
            }
            if (isset($streetAddress[2]) && $streetAddress[2] != '')
            {
                $customerToShipAddress =  $customerToShipAddress.", ".$streetAddress[2];
            }
            $customerToShipAddress =  $customerToShipAddress.", ".$order->getShippingAddress()->getCity().", ".$order->getShippingAddress()->getRegion().", ".$order->getShippingAddress()->getPostcode().", INDIA ";
            $dataString = array(array('pickup_pincode'=>$branchCollection->getData()[0]['pincode'],'drop_pincode'=>$order->getShippingAddress()->getPostcode(),'order_type'=>$paymentMethodDT,'reference_number'=>$order->getIncrementId(),'item'=>'medicines','invoice_value'=>$order->getGrandTotal(),'delivery_type'=>'FORWARD','weight'=>'1','height'=>'1','length'=>'1','breadth'=>'1'));
            $jsonReq = json_encode($dataString);
            $ch = curl_init($this->_scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_base_url', $storeScope)."api/v1/recommendation_api/?key=".$this->_scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_key', $storeScope));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonReq);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($jsonReq)
            ));
            $result = curl_exec($ch);
            $result = json_decode($result);
            if ($result->meta->status == '200' && strtolower($result->meta->message) == 'success')
            {
                $dataStringCreateSalesOrder = new \stdClass();
                $dataStringCreateSalesOrder->pickup_info = new \stdClass();
                if (isset($branchCollection->getData()[0]['state']))
                {
                    $dataStringCreateSalesOrder->pickup_info->pickup_state = $branchCollection->getData()[0]['state'];
                } 
                if (isset($branchCollection->getData()[0]['address']))
                {
                    $dataStringCreateSalesOrder->pickup_info->pickup_address = $branchCollection->getData()[0]['address'];                    
                }
                if (isset($branchCollection->getData()[0]['email']))
                {
                    $dataStringCreateSalesOrder->pickup_info->email = $branchCollection->getData()[0]['email'];                    
                }
                if (isset($branchCollection->getData()[0]['pincode']))
                {
                    $dataStringCreateSalesOrder->pickup_info->pickup_pincode = $branchCollection->getData()[0]['pincode'];                    
                }
                if (isset($branchCollection->getData()[0]['city']))
                {
                    $dataStringCreateSalesOrder->pickup_info->pickup_city = $branchCollection->getData()[0]['city'];                    
                }
                if (isset($branchCollection->getData()[0]['name']))
                {
                    $dataStringCreateSalesOrder->pickup_info->pickup_name = $branchCollection->getData()[0]['name'];                    
                }
                if (isset($branchCollection->getData()[0]['phone']))
                {
                    $dataStringCreateSalesOrder->pickup_info->pickup_phone = $branchCollection->getData()[0]['phone'];                    
                }

                $dataStringCreateSalesOrder->pickup_info->pickup_time = $pickupDate;
                $dataStringCreateSalesOrder->pickup_info->tin = "tin";
                $dataStringCreateSalesOrder->pickup_info->pickup_country = "INDIA";
                $dataStringCreateSalesOrder->drop_info = new \stdClass();
                $dataStringCreateSalesOrder->drop_info->drop_address = $customerToShipAddress;
                $selfPincodes = explode(',',$this->_scopeConfig->getValue('clickpost_settings/zoylo/clickpost_self_pincode', $storeScope));
                $dataStringCreateSalesOrder->drop_info->drop_phone = "******".substr($order->getShippingAddress()->getTelephone(), -4);
                if (!empty($selfPincodes) && in_array($order->getShippingAddress()->getPostcode(),$selfPincodes)) // show the full phone number if the pincode of delivery is a self pincode
                {
                    $dataStringCreateSalesOrder->drop_info->drop_phone = $order->getShippingAddress()->getTelephone();
                }
                $dataStringCreateSalesOrder->drop_info->drop_country = "INDIA";
                $dataStringCreateSalesOrder->drop_info->drop_state = $order->getShippingAddress()->getRegion();
                $dataStringCreateSalesOrder->drop_info->drop_pincode = $order->getShippingAddress()->getPostcode();
                $dataStringCreateSalesOrder->drop_info->drop_city = $order->getShippingAddress()->getCity();
                $dataStringCreateSalesOrder->drop_info->drop_name = $order->getShippingAddress()->getFirstname()." ".$order->getShippingAddress()->getLastname();
                $dataStringCreateSalesOrder->shipment_details = new \stdClass();
                $dataStringCreateSalesOrder->shipment_details->height = "1";
                $dataStringCreateSalesOrder->shipment_details->order_type = $paymentMethodDT;
                $dataStringCreateSalesOrder->shipment_details->invoice_value = 1;
                $dataStringCreateSalesOrder->shipment_details->invoice_number = $order->getIncrementId();
                foreach ($order->getInvoiceCollection() as $invoice) //at present we are having only one invoice number
                {
                    $dataStringCreateSalesOrder->shipment_details->invoice_number = $invoice->getIncrementId();
                }
                if (isset($order->getInvoiceCollection()->getData()[0]['created_at']))
                {
                    $dataStringCreateSalesOrder->shipment_details->invoice_date = date('Y-m-d',  strtotime($order->getInvoiceCollection()->getData()[0]['created_at']));;                   
                }
                else // if invoice date is not set then we are sending the today's date 
                {
                    $dataStringCreateSalesOrder->shipment_details->invoice_date = $this->timezoneDate->date()->format('Y-m-d');       
                }
 
                $dataStringCreateSalesOrder->shipment_details->reference_number = $order->getIncrementId();
                $dataStringCreateSalesOrder->shipment_details->length = "1";
                $dataStringCreateSalesOrder->shipment_details->breadth = "1";
                $dataStringCreateSalesOrder->shipment_details->weight = "1";
                $itemsObjData = new \stdClass();
                $itemsObjData->product_url = "";
                $itemsObjData->price = 1;
                $itemsObjData->description = "Medicines";
                $itemsObjData->quantity = "1";
                $itemsObjData->sku = "zoylo-product";
                $itemsObjData->additional = new \stdClass();
                $itemsObjData->additional->images = "";
                $itemsObjData->additional->weight = '1';
                $itemsObjData->additional->return_days = "7";
                $dataStringCreateSalesOrder->order_date = $order->getCreatedAt();
                $dataStringCreateSalesOrder->shipment_details->items = array($itemsObjData);
                $dataStringCreateSalesOrder->shipment_details->cod_value = $codValue;
                
                if (isset($result->result[0]->preference_array[0]->cp_id))
                {
                    $dataStringCreateSalesOrder->shipment_details->courier_partner = $result->result[0]->preference_array[0]->cp_id;                    
                }
                else
                {
                    $dataStringCreateSalesOrder->shipment_details->courier_partner = 3;                    
                }

                if (isset($result->result[0]->preference_array[0]->account_code))
                {
                    $dataStringCreateSalesOrder->account_code = $result->result[0]->preference_array[0]->account_code;
                }
                else
                {
                    $dataStringCreateSalesOrder->account_code = 'ECOMM Express';                    
                }

                $jsonReqCreateSalesOrder = json_encode($dataStringCreateSalesOrder);
                
                $chCreateSalesOrder = curl_init($this->_scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_base_url', $storeScope)."api/v3/create-order/?username=".$this->_scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_username', $storeScope)."&key=".$this->_scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_key', $storeScope));
                curl_setopt($chCreateSalesOrder, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($chCreateSalesOrder, CURLOPT_POSTFIELDS, json_encode($dataStringCreateSalesOrder));                                                                  
                curl_setopt($chCreateSalesOrder, CURLOPT_RETURNTRANSFER, true);                                                                      
                curl_setopt($chCreateSalesOrder, CURLOPT_HTTPHEADER, array(                                                                          
                    'Content-Type: application/json',                                                                                
                    'Content-Length: ' . strlen(json_encode($dataStringCreateSalesOrder))
                ));
                $resultCreateSalesOrder = curl_exec($chCreateSalesOrder);
                $resultCreateSalesOrder = json_decode($resultCreateSalesOrder);
                if ($resultCreateSalesOrder->meta->status == '200') // order created successfully on clickpost
                {
                    $unicommerceFailedMdl->setOrderId($order->getEntityId());
                    $unicommerceFailedMdl->setStatus($resultCreateSalesOrder->meta->status);
                    $unicommerceFailedMdl->setMessage($resultCreateSalesOrder->meta->message);
                    $unicommerceFailedMdl->setSortCode($resultCreateSalesOrder->result->sort_code);
                    $unicommerceFailedMdl->setLabel($resultCreateSalesOrder->result->label);
                    $unicommerceFailedMdl->setSecurityKey($resultCreateSalesOrder->result->security_key);
                    $unicommerceFailedMdl->setReferenceNumber($resultCreateSalesOrder->result->reference_number);
                    $unicommerceFailedMdl->setWaybill($resultCreateSalesOrder->result->waybill);
                    $unicommerceFailedMdl->setTrackingId($resultCreateSalesOrder->tracking_id);
                    $unicommerceFailedMdl->setClickpostOrderId($resultCreateSalesOrder->order_id);
                    $unicommerceFailedMdl->setIncrementId($order->getIncrementId());
                    $unicommerceFailedMdl->save();  
                    
                    //code for create shippment starts
                    $order->setState('processing'); // temporary setting this state so that we can create shippment.
                    if ($order->canShip())
                    {
                        // Initialize the order shipment object
                        $convertOrder = $this->orderConvert;
                        $shipment = $convertOrder->toShipment($order);
                        // Loop through order items
                        foreach ($order->getAllItems() AS $orderItemEach) 
                        {
                            // Check if order item has qty to ship or is virtual
                            if (! $orderItemEach->getQtyToShip() || $orderItemEach->getIsVirtual()) 
                            {
                                continue;
                            }
                            $qtyShipped = $orderItemEach->getQtyToShip();
                            // Create shipment item with qty
                            $shipmentItem = $convertOrder->itemToShipmentItem($orderItemEach)->setQty($qtyShipped);
                            // Add shipment item to shipment
                            $shipment->addItem($shipmentItem);
                        }
                        // Register shipment
                        $shipment->register();
                        $shipment->getOrder()->setIsInProcess(true);
                        try {
                            $trackData = array(
                                'carrier_code' => 'ecomexpress',
                                'title' => 'Ecom Express',
                                'number' => $resultCreateSalesOrder->result->waybill, // Replace with your tracking number
                            );

                            $track = $this->trackFactory->create()->addData($trackData);
                            $shipment->addTrack($track)->save();
                            // Save created shipment and order
                            $shipment->save();
                            $shipment->getOrder()->save();

                            // Send email
                            //$objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
                            //    ->notify($shipment);
                            //$shipment->save();
                        } catch (\Exception $e) {
                           echo "Shipment Not Created". $e->getMessage(); exit;
                        }
                    }
                    $order->setState('payment_review'); // resetting the order state back to it's default state
                    //code for create shippment ends
                }
                else // order could not be created or was already created at clickpost
                {
                    $unicommerceFailedMdl->setOrderId($order->getEntityId());
                    $unicommerceFailedMdl->setIncrementId($order->getIncrementId());
                    $unicommerceFailedMdl->setStatus($resultCreateSalesOrder->meta->status);
                    $unicommerceFailedMdl->setMessage($resultCreateSalesOrder->meta->message);
                    $unicommerceFailedMdl->save();
                }
            }
            else
            {
                $unicommerceFailedMdl->setOrderId($order->getEntityId());
                $unicommerceFailedMdl->setIncrementId($order->getIncrementId());
                $unicommerceFailedMdl->setStatus($result->meta->status);
                $unicommerceFailedMdl->setMessage($result->meta->message);
                $unicommerceFailedMdl->save();
            }
        } catch (Exception $ex) {
            die('came till exception location.');
        }
    }
    
    public function syncReturnOrderToClickPost($order,$returnReason)
    {
        try
        {
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $pickupDate =  $this->timezoneDate->date()->format('Y-m-d'); // default date of today
            if ($this->timezoneDate->date()->format('H:i:s') < $this->timezoneDate->date('10:30:00')->format('H:i:s'))
            {
               $pickupDate = date('Y-m-d',strtotime($this->timezoneDate->date()->format('Y-m-d').' +1 Day'));
            }
            else
            {
                $pickupDate = date('Y-m-d',strtotime($this->timezoneDate->date()->format('Y-m-d').' +2 Day'));
            }
            $pickupDate = date ('Y-m-d H:i:s',strtotime($pickupDate." ".$this->_scopeConfig->getValue('clickpost_settings/zoylo/clickpost_pickup_time', $storeScope)));
            $unicommerceFailedMdl = $this->unicommerceFailedSync->create()->load("R-".$order->getIncrementId(),'increment_id');
            if (!$unicommerceFailedMdl->getId())
            {
                $unicommerceFailedMdl = $this->unicommerceFailedSync->create();
            }
            $paymentMethodDT = "COD";
            $codValue = $order->getGrandTotal();
            $condDataAll = $this->getConditionalData($order);
            $cashPayMethods = array('cashondelivery','free');
            if (isset($condDataAll['payment_method']) && !in_array($condDataAll['payment_method'],$cashPayMethods) )
            {
                $paymentMethodDT = "PREPAID";
                $codValue = 0;
            }
            $shipPinCode = $order->getShippingAddress()->getPostCode();
            $branchCollection = $this->branchFactory->create()->getCollection();
            $branchCollection->getSelect()->joinLeft(array('facility_table'=>$this->resourceConnection->getTableName('facility_code_mapping')),'main_table.branch_code=facility_table.branch_id')->where("main_table.zip_code ='".$shipPinCode."'");
            $facilityCenter = '';
            if (isset($branchCollection->getData()[0]['facility_code']))
            {
                $facilityCenter = $branchCollection->getData()[0]['facility_code'];
            }
            $customerToShipAddress = "";
            //$customerToShipAddress = $customerToShipAddress.$order->getShippingAddress()->getFirstname().", ".$order->getShippingAddress()->getLastname();
            $streetAddress = $order->getShippingAddress()->getStreet();
            if (isset($streetAddress[0]) && $streetAddress[0] != '')
            {
                $customerToShipAddress =  $customerToShipAddress.", ".$streetAddress[0];
            }
            if (isset($streetAddress[1]) && $streetAddress[1] != '')
            {
                $customerToShipAddress =  $customerToShipAddress.", ".$streetAddress[1];
            }
            if (isset($streetAddress[2]) && $streetAddress[2] != '')
            {
                $customerToShipAddress =  $customerToShipAddress.", ".$streetAddress[2];
            }
            $customerToShipAddress =  $customerToShipAddress.", ".$order->getShippingAddress()->getCity().", ".$order->getShippingAddress()->getRegion().", ".$order->getShippingAddress()->getPostcode().", INDIA ";
            $dataString = array(array('pickup_pincode'=>$branchCollection->getData()[0]['pincode'],'drop_pincode'=>$order->getShippingAddress()->getPostcode(),'order_type'=>$paymentMethodDT,'reference_number'=>$order->getIncrementId(),'item'=>'medicines','invoice_value'=>$order->getGrandTotal(),'delivery_type'=>'RVP','weight'=>'1','height'=>'1','length'=>'1','breadth'=>'1'));
            $jsonReq = json_encode($dataString);
            $ch = curl_init($this->_scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_base_url', $storeScope)."api/v1/recommendation_api/?key=".$this->_scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_key', $storeScope));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonReq);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($jsonReq)
            ));
            $result = curl_exec($ch);
            $result = json_decode($result);
            if ($result->meta->status == '200' && strtolower($result->meta->message) == 'success')
            {
                $dataStringCreateSalesOrder = new \stdClass();
                $dataStringCreateSalesOrder->pickup_info = new \stdClass();
                $dataStringCreateSalesOrder->pickup_info->pickup_time = $pickupDate;
                $dataStringCreateSalesOrder->pickup_info->rvp_reason = "Not Interested";
                $dataStringCreateSalesOrder->pickup_info->tin = "tin";
                $dataStringCreateSalesOrder->pickup_info->pickup_country = "INDIA";
                $dataStringCreateSalesOrder->drop_info = new \stdClass();
                if (isset($branchCollection->getData()[0]['state']))
                {
                    $dataStringCreateSalesOrder->drop_info->drop_state = $branchCollection->getData()[0]['state'];
                    $dataStringCreateSalesOrder->pickup_info->pickup_state = $branchCollection->getData()[0]['state'];
                } 
                if (isset($branchCollection->getData()[0]['address']))
                {
                    $dataStringCreateSalesOrder->drop_info->drop_address = $branchCollection->getData()[0]['return_address']; 
                    $dataStringCreateSalesOrder->pickup_info->pickup_address = $branchCollection->getData()[0]['address'];   
                }
                if (isset($branchCollection->getData()[0]['email']))
                {
                    $dataStringCreateSalesOrder->pickup_info->email = $branchCollection->getData()[0]['email'];  
                    //$dataStringCreateSalesOrder->pickup_info->email = $branchCollection->getData()[0]['email'];    
                }
                if (isset($branchCollection->getData()[0]['pincode']))
                {
                    $dataStringCreateSalesOrder->drop_info->drop_pincode = $branchCollection->getData()[0]['pincode'];    
                    $dataStringCreateSalesOrder->pickup_info->pickup_pincode = $branchCollection->getData()[0]['pincode'];         
                }
                if (isset($branchCollection->getData()[0]['city']))
                {
                    $dataStringCreateSalesOrder->drop_info->drop_city = $branchCollection->getData()[0]['city'];  
                    $dataStringCreateSalesOrder->pickup_info->pickup_city = $branchCollection->getData()[0]['city']; 
                }
                if (isset($branchCollection->getData()[0]['name']))
                {
                    $dataStringCreateSalesOrder->drop_info->drop_name = $branchCollection->getData()[0]['name'];  
                    $dataStringCreateSalesOrder->pickup_info->pickup_name = $branchCollection->getData()[0]['name'];
                }
                if (isset($branchCollection->getData()[0]['phone']))
                {
                    $dataStringCreateSalesOrder->drop_info->drop_phone = $branchCollection->getData()[0]['phone'];  
                    $dataStringCreateSalesOrder->pickup_info->pickup_phone = $branchCollection->getData()[0]['phone'];
                }
                $dataStringCreateSalesOrder->drop_info->drop_country = "INDIA";
                $dataStringCreateSalesOrder->shipment_details = new \stdClass();
                $dataStringCreateSalesOrder->shipment_details->height = "1";
                $dataStringCreateSalesOrder->shipment_details->order_type = $paymentMethodDT;
                $dataStringCreateSalesOrder->shipment_details->invoice_value = 1;
                $dataStringCreateSalesOrder->shipment_details->invoice_number = $order->getIncrementId();
                foreach ($order->getInvoiceCollection() as $invoice) //at present we are having only one invoice number
                {
                    $dataStringCreateSalesOrder->shipment_details->invoice_number = $invoice->getIncrementId();
                }
                if (isset($order->getInvoiceCollection()->getData()[0]['created_at']))
                {
                    $dataStringCreateSalesOrder->shipment_details->invoice_date = date('Y-m-d',  strtotime($order->getInvoiceCollection()->getData()[0]['created_at']));;                   
                }
                else // if invoice date is not set then we are sending the today's date 
                {
                    $dataStringCreateSalesOrder->shipment_details->invoice_date = $this->timezoneDate->date()->format('Y-m-d');       
                }
                $dataStringCreateSalesOrder->shipment_details->reference_number = "RETURN-".$order->getIncrementId();
                $dataStringCreateSalesOrder->shipment_details->length = "1";
                $dataStringCreateSalesOrder->shipment_details->breadth = "1";
                $dataStringCreateSalesOrder->shipment_details->weight = "1";
                $itemsObjData = new \stdClass();
                $itemsObjData->product_url = "";
                $itemsObjData->price = 1;
                $itemsObjData->description = "Medicines";
                $itemsObjData->quantity = "1";
                $itemsObjData->sku = "zoylo-product";
                $itemsObjData->additional = new \stdClass();
                $itemsObjData->additional->images = "";
                $itemsObjData->additional->weight = '1';
                $itemsObjData->additional->return_days = "7";
                $dataStringCreateSalesOrder->order_date = $order->getCreatedAt();
                $dataStringCreateSalesOrder->shipment_details->items = array($itemsObjData);
                $dataStringCreateSalesOrder->shipment_details->cod_value = $codValue;
                
                if (isset($result->result[0]->preference_array[0]->cp_id))
                {
                    $dataStringCreateSalesOrder->shipment_details->courier_partner = $result->result[0]->preference_array[0]->cp_id;                    
                }
                else
                {
                    $dataStringCreateSalesOrder->shipment_details->courier_partner = 3;                    
                }

                if (isset($result->result[0]->preference_array[0]->account_code))
                {
                    $dataStringCreateSalesOrder->account_code = $result->result[0]->preference_array[0]->account_code;
                }
                else
                {
                    $dataStringCreateSalesOrder->account_code = 'ECOMM Express';                    
                }

                $jsonReqCreateSalesOrder = json_encode($dataStringCreateSalesOrder);
                
                $chCreateSalesOrder = curl_init($this->_scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_base_url', $storeScope)."api/v3/create-order/?username=".$this->_scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_username', $storeScope)."&key=".$this->_scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_key', $storeScope));
                curl_setopt($chCreateSalesOrder, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($chCreateSalesOrder, CURLOPT_POSTFIELDS, json_encode($dataStringCreateSalesOrder));                                                                  
                curl_setopt($chCreateSalesOrder, CURLOPT_RETURNTRANSFER, true);                                                                      
                curl_setopt($chCreateSalesOrder, CURLOPT_HTTPHEADER, array(                                                                          
                    'Content-Type: application/json',                                                                                
                    'Content-Length: ' . strlen(json_encode($dataStringCreateSalesOrder))
                ));
                $resultCreateSalesOrder = curl_exec($chCreateSalesOrder);
                $resultCreateSalesOrder = json_decode($resultCreateSalesOrder);
                if ($resultCreateSalesOrder->meta->status == '200') // order created successfully on clickpost
                {
                    $unicommerceFailedMdl->setOrderId($order->getEntityId());
                    $unicommerceFailedMdl->setStatus($resultCreateSalesOrder->meta->status);
                    $unicommerceFailedMdl->setMessage($resultCreateSalesOrder->meta->message);
                    $unicommerceFailedMdl->setSortCode($resultCreateSalesOrder->result->sort_code);
                    $unicommerceFailedMdl->setLabel($resultCreateSalesOrder->result->label);
                    $unicommerceFailedMdl->setSecurityKey($resultCreateSalesOrder->result->security_key);
                    $unicommerceFailedMdl->setReferenceNumber($resultCreateSalesOrder->result->reference_number);
                    $unicommerceFailedMdl->setWaybill($resultCreateSalesOrder->result->waybill);
                    $unicommerceFailedMdl->setTrackingId($resultCreateSalesOrder->tracking_id);
                    $unicommerceFailedMdl->setClickpostOrderId($resultCreateSalesOrder->order_id);
                    $unicommerceFailedMdl->setIncrementId("R-".$order->getIncrementId());
                    $unicommerceFailedMdl->save();  
                    
                    //code for create shippment starts
                    /*$order->setState('processing'); // temporary setting this state so that we can create shippment.
                    if ($order->canShip())
                    {
                        // Initialize the order shipment object
                        $convertOrder = $this->orderConvert;
                        $shipment = $convertOrder->toShipment($order);
                        // Loop through order items
                        foreach ($order->getAllItems() AS $orderItemEach) 
                        {
                            // Check if order item has qty to ship or is virtual
                            if (! $orderItemEach->getQtyToShip() || $orderItemEach->getIsVirtual()) 
                            {
                                continue;
                            }
                            $qtyShipped = $orderItemEach->getQtyToShip();
                            // Create shipment item with qty
                            $shipmentItem = $convertOrder->itemToShipmentItem($orderItemEach)->setQty($qtyShipped);
                            // Add shipment item to shipment
                            $shipment->addItem($shipmentItem);
                        }
                        // Register shipment
                        $shipment->register();
                        $shipment->getOrder()->setIsInProcess(true);
                        try {
                            $trackData = array(
                                'carrier_code' => 'ecomexpress',
                                'title' => 'Ecom Express',
                                'number' => $resultCreateSalesOrder->result->waybill, // Replace with your tracking number
                            );

                            $track = $this->trackFactory->create()->addData($trackData);
                            $shipment->addTrack($track)->save();
                            // Save created shipment and order
                            $shipment->save();
                            $shipment->getOrder()->save();

                            // Send email
                            //$objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
                            //    ->notify($shipment);
                            //$shipment->save();
                        } catch (\Exception $e) {
                           echo "Shipment Not Created". $e->getMessage(); exit;
                        }
                    }
                    $order->setState('payment_review');*/ // resetting the order state back to it's default state
                    //code for create shippment ends
                }
                else if ($resultCreateSalesOrder->meta->status == '323')
                {
                    $unicommerceFailedMdl->setOrderId($order->getEntityId());
                    $unicommerceFailedMdl->setStatus($resultCreateSalesOrder->meta->status);
                    $unicommerceFailedMdl->setMessage($resultCreateSalesOrder->meta->message);
                    $unicommerceFailedMdl->setSortCode($resultCreateSalesOrder->result->sort_code);
                    $unicommerceFailedMdl->setLabel($resultCreateSalesOrder->result->label);
                    $unicommerceFailedMdl->setSecurityKey($resultCreateSalesOrder->result->security_key);
                    $unicommerceFailedMdl->setReferenceNumber($resultCreateSalesOrder->result->reference_number);
                    $unicommerceFailedMdl->setWaybill($resultCreateSalesOrder->result->waybill);
                    $unicommerceFailedMdl->setTrackingId($resultCreateSalesOrder->tracking_id);
                    $unicommerceFailedMdl->setClickpostOrderId($resultCreateSalesOrder->order_id);
                    $unicommerceFailedMdl->setIncrementId("R-".$order->getIncrementId());
                    $unicommerceFailedMdl->save();  
                }
                else // order could not be created or was already created at clickpost
                {
                    $unicommerceFailedMdl->setOrderId($order->getEntityId());
                    $unicommerceFailedMdl->setIncrementId("R-".$order->getIncrementId());
                    $unicommerceFailedMdl->setStatus($resultCreateSalesOrder->meta->status);
                    $unicommerceFailedMdl->setMessage($resultCreateSalesOrder->meta->message);
                    $unicommerceFailedMdl->save();
                }
            }
            else
            {
                $unicommerceFailedMdl->setOrderId($order->getEntityId());
                $unicommerceFailedMdl->setIncrementId("R-".$order->getIncrementId());
                $unicommerceFailedMdl->setStatus($result->meta->status);
                $unicommerceFailedMdl->setMessage($result->meta->message);
                $unicommerceFailedMdl->save();
            }
        } catch (Exception $ex) {
            die('came till exception location.');
        }
    }
    
    public function sendEmail($order,$frontendStatus,$statusSet,$firstLineCont)
    {
        $vars = [
            'order' => $order,
            'billing' => $order->getBillingAddress(),
            'payment_html' => $this->getPaymentHtml($order),
            'store' => $order->getStore(),
            'formattedShippingAddress' => $this->getFormattedShippingAddress($order),
            'formattedBillingAddress' => $this->getFormattedBillingAddress($order),
            'frontend_order_status' => $frontendStatus,
            'firstline'=>$firstLineCont
        ];
        
        $sender         = array('email' => 'noreply@zoylo.com','name' => 'Zoylo');
        $transport      = $this->transportBuilder->setTemplateIdentifier($statusSet)
            ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND,'store' => 1])
            ->setTemplateVars($vars)
            ->setFrom($sender)
            ->addTo(array('To' => $order->getCustomerEmail()));
        $transport = $transport->getTransport();
        $transport->sendMessage();
    }
    
    public function sendSMS($order,$statusFrontendLabel,$smsToSend)
    {
        if ($smsToSend != '')
        {
            if ($this->smshelperdata->isEnabled() && $this->smshelperdata->isOrderPlaceForUserEnabled()) {
            $status         = $order->getStatus();
            $customerId     = $order->getCustomerId();
            $customer       = $this->customerFactory->create()->load($customerId);
            
            $mobile         = $customer->getCustomerNumber();
            $smsToSend = sprintf($smsToSend, $order->getIncrementId());
            $this->smshelperapi->callApiUrl($mobile, $smsToSend);
            return true;
        }
        return false;
        }
        else
        {
            return false;
        }
    }
    
    
     /**
     * @param Order $order
     * @return string|null
     */
    public function getFormattedShippingAddress($order)
    {
        return $order->getIsVirtual()
            ? null
            : $this->addressRenderer->format($order->getShippingAddress(), 'html');
    }

    /**
     * @param Order $order
     * @return string|null
     */
    public function getFormattedBillingAddress($order)
    {
        return $this->addressRenderer->format($order->getBillingAddress(), 'html');
    }
    
    
    /**
     * Get payment info block as html
     *
     * @param Order $order
     * @return string
     */
    public function getPaymentHtml($order)
    {
        return $this->paymentHelper->getInfoBlockHtml(
            $order->getPayment(),
            $this->identityContainer->getStore()->getStoreId()
        );
    }
    
    public function getConditionalData($order){
        $orderCondComb = [];
        $containsPresciption = FALSE;
        $orderPayment = $order->getPayment();
        $paymentMethod = $orderPayment->getMethodInstance();
        $paymentMethodCode = $paymentMethod->getCode();
        $orderAllItems = $order->getAllItems(); 
        if ($orderAllItems) {
            foreach ($orderAllItems as $item) {
                if ( $item->getProduct() && $item->getProduct()->getData('prescription_required') && $item->getProduct()->getData('prescription_required') == 'required')
                {
                   $containsPresciption = TRUE;
                }
            }
        }
        
        $orderCondComb['payment_method'] = $paymentMethodCode;
        $orderCondComb['prescription_required'] = $containsPresciption;

        return $orderCondComb;
    }
}
