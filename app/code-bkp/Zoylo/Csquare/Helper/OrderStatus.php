<?php
namespace Zoylo\Csquare\Helper;

class OrderStatus extends \Magento\Framework\App\Helper\AbstractHelper
{
	/**
     * scopeConfig for system Congiguration
     *
     * @var string
     */
    protected $_scopeConfig;
	
	/**
      *@var \Zoylo\Csquare\Model\ProductsFactory
     */
    protected $_productsFactory;
	
    /**
     * @var \Zoylo\Csquare\Model\OrdersFactory
     */
    protected $_ordersFactory;

	/**
     * 
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	 * @param \Zoylo\Csquare\Model\ProductsFactory $productsFactory
     */
	protected $transportBuilder;
         
	public function __construct(
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Payment\Helper\Data $paymentHelper,
        \Magecomp\Smsfree\Helper\Data $smshelperdata,
        \Magecomp\Smsfree\Helper\Apicall $smshelperapi,
        \Magento\Sales\Model\Order\Email\Container\OrderIdentity $identityContainer,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Zoylo\Csquare\Model\UnicommerceFailedSyncFactory $unicommerceFailedSync,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \I95Dev\BranchInformation\Model\BranchFactory $branchFactory,
        \Magento\Sales\Model\Order $orderModel
        ) 
        {
            $this->addressRenderer         = $addressRenderer;
            $this->paymentHelper = $paymentHelper;
            $this->smshelperdata = $smshelperdata;
            $this->smshelperapi = $smshelperapi;
            $this->identityContainer = $identityContainer;
            $this->customerFactory = $customerFactory;
            $this->resourceConnection = $resourceConnection;
            $this->unicommerceFailedSync = $unicommerceFailedSync;
            $this->scopeConfig = $scopeConfig;
            $this->branchFactory = $branchFactory;
            $this->orderModel = $orderModel;
            $this->transportBuilder = $transportBuilder;
	}
        
    public function orderShippedCs($order)
    {
        if ($order->getStatus() == 'order_shipped_cs' ) // if order status is order_shipped_cs during the time of shipment creation
        {
            $statusFrontendLabel = str_replace(' CS', '', $order->getStatusLabel());
            $status         = $order->getStatus();
            $tracksCollection = $this->orderModel->load($order->getEntityId())->getTracksCollection();
            if (isset($tracksCollection->getData()[0]['title']))
            {
                $firstLineCont = " through ".$tracksCollection->getData()[0]['title'].". You can track your order using AWB #".$tracksCollection->getData()[0]['track_number']." ";
                //$smsContent = "Your zoylo order ".$order->getIncrementId()." has been shipped through ".$tracksCollection->getData()[0]['title'].". You can track your order using AWB ".$tracksCollection->getData()[0]['track_number'].". ";
                $courier_name = $tracksCollection->getData()[0]['title'];
                $awb_number = $tracksCollection->getData()[0]['track_number'];
                $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status);
                $smsContent = str_replace('$Courier_name', $courier_name, $smsContent);
                $smsContent = str_replace('$awb_number', $awb_number, $smsContent);
                $this->sendSMS($order,$statusFrontendLabel,$smsContent);
                $this->sendEmail($order,$statusFrontendLabel,$status,$firstLineCont);
            }
            else
            {
                $firstLineCont = '.';
                $smsContent = "Your Zoylo order ".$order->getIncrementId()." has been shipped.";
                $this->sendSMS($order,$statusFrontendLabel,$smsContent);
                $this->sendEmail($order,$statusFrontendLabel,$status,$firstLineCont);
            }
        }
        
    }
        /**
     * @param Order $order
     * @return string|null
     */
    public function getFormattedShippingAddress($order)
    {
        return $order->getIsVirtual()
            ? null
            : $this->addressRenderer->format($order->getShippingAddress(), 'html');
    }

    /**
     * @param Order $order
     * @return string|null
     */
    public function getFormattedBillingAddress($order)
    {
        return $this->addressRenderer->format($order->getBillingAddress(), 'html');
    }

    /**
     * Get payment info block as html
     *
     * @param Order $order
     * @return string
     */
    public function getPaymentHtml($order)
    {
        return $this->paymentHelper->getInfoBlockHtml(
            $order->getPayment(),
            $this->identityContainer->getStore()->getStoreId()
        );
    }
    
    public function sendEmail($order,$frontendStatus,$statusSet,$firstLineCont)
    {
        $vars = [
            'order' => $order,
            'billing' => $order->getBillingAddress(),
            'payment_html' => $this->getPaymentHtml($order),
            'store' => $order->getStore(),
            'formattedShippingAddress' => $this->getFormattedShippingAddress($order),
            'formattedBillingAddress' => $this->getFormattedBillingAddress($order),
            'frontend_order_status' => $frontendStatus,
            'firstline'=>$firstLineCont
        ];
        
        $sender         = array('email' => 'noreply@zoylo.com','name' => 'Zoylo');
        $transport      = $this->transportBuilder->setTemplateIdentifier($statusSet)
            ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND,'store' => 1])
            ->setTemplateVars($vars)
            ->setFrom($sender)
            ->addTo(array('To' => $order->getCustomerEmail()));
        $transport = $transport->getTransport();
        $transport->sendMessage();
    }
    
    public function sendSMS($order,$statusFrontendLabel,$smsToSend)
    {
        //echo "<pre>";
        //die(print_r($order->debug()));
        //echo "<pre>";
        //die(print_r($order->getStoreLabels()));
        //echo "<pre>";
        //die(print_r($this->statusCollection->getData()));
        //die("Status".$order->getStatusLabel());
        //die('frontend label:'.$statusFrontendLabel);
        if ($smsToSend != '')
        {
            if ($this->smshelperdata->isEnabled() && $this->smshelperdata->isOrderPlaceForUserEnabled()) {
            $status         = $order->getStatus();
            $customerId     = $order->getCustomerId();
            $customer       = $this->customerFactory->create()->load($customerId);
            //$message        = $this->smshelperdata->getOrderStatusTemplateForUser($smsTemplateId);
            $message = 'test';
            if($message != ''){
                $sms            =  sprintf($message, $order->getIncrementId());
                $mobile         = $customer->getCustomerNumber();
                $smsToSend = sprintf($smsToSend, $order->getIncrementId());
                $this->smshelperapi->callApiUrl($mobile, $smsToSend);
                return true;  
            }else{
                return false;
            }
        }
        }
        
        
    }

    public function cancelOrder($order){

        $incrementId = $order->getIncrementId();
        $entityApiClass = $this->_csquareOrdersFactory->create();
        $getRecordsO = $entityApiClass->getCollection()
            ->addFieldtoFilter('source_order_id', $incrementId);             
        $getRecords = $getRecordsO->getData();
        if ($getRecordsO->getSize() > 0) {
            // API Call
            $apiCallResponse = $this->_csquareOrderSyncFactory->create()->cancelOrder($order,$getRecords);
            return true;
        }else{
            return false;
        }
    }
    
    public function getConditionalRData($order){
        $orderCondComb = [];
        $containsPresciption = FALSE;
        $orderPayment = $order->getPayment();
        $paymentMethod = $orderPayment->getMethodInstance();
        $paymentMethodCode = $paymentMethod->getCode();
        $orderAllItems = $order->getAllItems(); 
        if ($orderAllItems) {
            foreach ($orderAllItems as $item) {
                if ( $item->getProduct() && $item->getProduct()->getData('prescription_required') && $item->getProduct()->getData('prescription_required') == 'required')
                {
                   $containsPresciption = TRUE;
                }
            }
        }
        
        $orderCondComb['payment_method'] = $paymentMethodCode;
        $orderCondComb['prescription_required'] = $containsPresciption;

        return $orderCondComb;
    }
}
