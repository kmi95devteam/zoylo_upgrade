<?php
namespace Zoylo\Csquare\Observer\Sales;
use Magento\Framework\Event\ObserverInterface;
use Zoylo\Csquare\Helper\Data;

class OrderCancelAfter implements ObserverInterface
{
    protected $_objectManager;

    /**
     * @var Data
    */
    protected $_data;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dataTime;

    /**
     * @var \Zoylo\Csquare\Model\OrdersFactory
     */
    protected $_csquareOrdersFactory;

    /**
     * @var \Zoylo\Csquare\Model\OrderSyncFactory
     */
    protected $_csquareOrderSyncFactory;

    protected $transportBuilder;
    
    protected $addressRenderer;
    
    protected $paymentHelper;
    
    protected $identityContainer;
    
    protected $resourceConnection;
    
    protected $unicommerceFailedSync;
    
    protected $scopeConfig;
    
    protected $branchFactory;
    /**
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dataTime
     * @param \Zoylo\Csquare\Model\OrdersFactory $csquareOrdersFactory
     * @param \Zoylo\Csquare\Model\OrderSyncFactory $csquareOrderSyncFactory
     * @param Data $data
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $dataTime,
        \Zoylo\Csquare\Model\OrdersFactory $csquareOrdersFactory,
        \Zoylo\Csquare\Model\OrderSyncFactory $csquareOrderSyncFactory,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Payment\Helper\Data $paymentHelper,
        \Magecomp\Smsfree\Helper\Data $smshelperdata,
        \Magecomp\Smsfree\Helper\Apicall $smshelperapi,
        \Magento\Sales\Model\Order\Email\Container\OrderIdentity $identityContainer,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Zoylo\Csquare\Model\UnicommerceFailedSyncFactory $unicommerceFailedSync,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \I95Dev\BranchInformation\Model\BranchFactory $branchFactory,
        \Magento\Sales\Model\Order $orderModel,
        Data $data
    ) {
        $this->_objectManager        = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_dataTime             = $dataTime;
        $this->_csquareOrdersFactory = $csquareOrdersFactory;
        $this->_csquareOrderSyncFactory = $csquareOrderSyncFactory;
        $this->transportBuilder     = $transportBuilder;
        $this->_data            = $data;
        $this->addressRenderer         = $addressRenderer;
        $this->paymentHelper = $paymentHelper;
        $this->smshelperdata = $smshelperdata;
        $this->smshelperapi = $smshelperapi;
        $this->identityContainer = $identityContainer;
        $this->customerFactory = $customerFactory;
        $this->resourceConnection = $resourceConnection;
        $this->unicommerceFailedSync = $unicommerceFailedSync;
        $this->scopeConfig = $scopeConfig;
        $this->branchFactory = $branchFactory;
        $this->orderModel = $orderModel;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order          = $observer->getOrder();
        $status         = $order->getStatus();
        $incrementId    = $order->getIncrementId();
        $orderId        = $order->getId();
        $statusFrontendLabel = str_replace(' CS', '', $order->getStatusLabel());
        $firstLineCont = '';
        $smsContent = '';
       
        if ($status == 'canceled')
        {
            $canceledApiResult = $this->cancelOrder($order);
            $cancellationReasonsArr = array('already placed another order','mrp Change','payment issue','shipping cost too high','found cheaper somewhere else','order taking time to delivery','need to change payment mode');
            $condDataAll = $this->getConditionalRData($order);
            $isCancelReasonOther = FALSE;
            if (!in_array(strtolower($order->getCancellationReason()), $cancellationReasonsArr)) // if no one the reasons, then the reason is others for which we need to send a generic message to the customer
            {
                $isCancelReasonOther = TRUE;
            }
            if (isset($condDataAll['payment_method']) && $condDataAll['payment_method'] == 'cashondelivery' )
            {
               //$smsContent = "Your zoylo order ".$order->getIncrementId()." has been cancelled upon your request.In case of any queries or assistance, please contact us.";
               $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status."_cod");
               $smsContent = str_replace('{reason}',strtolower($order->getCancellationReason()),$smsContent);
               $firstLineCont = "has been cancelled due to ".strtolower($order->getCancellationReason()).". Your refund, if applicable will be triggered to source in 4-5 working days";
            }
            else if(isset($condDataAll['payment_method']))
            {
               //$smsContent = "Your zoylo order ".$order->getIncrementId()." has been cancelled upon your request. Your refund will be triggered to source in 4-5 working days. In case of any queries or assistance, please contact us.";
               $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status."_prepaid");
               $smsContent = str_replace('{reason}',strtolower($order->getCancellationReason()),$smsContent);
               $firstLineCont = "has been cancelled due to ".strtolower($order->getCancellationReason()).". Your refund, if applicable will be triggered to source in 4-5 working days"; 
            }
            if ($isCancelReasonOther == TRUE)
            {
                $firstLineCont = "has been cancelled upon your request. Your refund, if applicable will be triggered to source in 4-5 working days";
                $smsContent = str_replace('cancelled due to {reason}','has been cancelled upon your request',$smsContent);
            }
            $this->sendSMS($order,$statusFrontendLabel,$smsContent);
            $this->sendEmail($order,$statusFrontendLabel,$status,$firstLineCont);
        }
    }
    
    /**
     * @param Order $order
     * @return string|null
     */
    public function getFormattedShippingAddress($order)
    {
        return $order->getIsVirtual()
            ? null
            : $this->addressRenderer->format($order->getShippingAddress(), 'html');
    }

    /**
     * @param Order $order
     * @return string|null
     */
    public function getFormattedBillingAddress($order)
    {
        return $this->addressRenderer->format($order->getBillingAddress(), 'html');
    }

    /**
     * Get payment info block as html
     *
     * @param Order $order
     * @return string
     */
    public function getPaymentHtml($order)
    {
        return $this->paymentHelper->getInfoBlockHtml(
            $order->getPayment(),
            $this->identityContainer->getStore()->getStoreId()
        );
    }
    
    public function sendEmail($order,$frontendStatus,$statusSet,$firstLineCont)
    {
        $vars = [
            'order' => $order,
            'billing' => $order->getBillingAddress(),
            'payment_html' => $this->getPaymentHtml($order),
            'store' => $order->getStore(),
            'formattedShippingAddress' => $this->getFormattedShippingAddress($order),
            'formattedBillingAddress' => $this->getFormattedBillingAddress($order),
            'frontend_order_status' => $frontendStatus,
            'firstline'=>$firstLineCont
        ];
        
        $sender         = array('email' => 'noreply@zoylo.com','name' => 'Zoylo');
        $transport      = $this->transportBuilder->setTemplateIdentifier($statusSet)
            ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND,'store' => 1])
            ->setTemplateVars($vars)
            ->setFrom($sender)
            ->addTo(array('To' => $order->getCustomerEmail()));
        $transport = $transport->getTransport();
        $transport->sendMessage();
    }
    
    public function sendSMS($order,$statusFrontendLabel,$smsToSend)
    {
        if ($smsToSend != '')
        {
            if ($this->smshelperdata->isEnabled() && $this->smshelperdata->isOrderPlaceForUserEnabled()) {
            $status         = $order->getStatus();
            $customerId     = $order->getCustomerId();
            $customer       = $this->customerFactory->create()->load($customerId);
            //$message        = $this->smshelperdata->getOrderStatusTemplateForUser($smsTemplateId);
            $message = 'test';
            if($message != ''){
                $sms            =  sprintf($message, $order->getIncrementId());
                $mobile         = $customer->getCustomerNumber();
                $smsToSend = sprintf($smsToSend, $order->getIncrementId());
                $this->smshelperapi->callApiUrl($mobile, $smsToSend);
                return true;  
            }else{
                return false;
            }
        }
        }
        
        
    }

    public function cancelOrder($order){

        $incrementId = $order->getIncrementId();
        $entityApiClass = $this->_csquareOrdersFactory->create();
        $getRecordsO = $entityApiClass->getCollection()
            ->addFieldtoFilter('source_order_id', $incrementId);             
        $getRecords = $getRecordsO->getData();
        if ($getRecordsO->getSize() > 0) {
            // API Call
            $apiCallResponse = $this->_csquareOrderSyncFactory->create()->cancelOrder($order,$getRecords);
            return true;
        }else{
            return false;
        }
    }
    
    public function getConditionalRData($order){
        $orderCondComb = [];
        $containsPresciption = FALSE;
        $orderPayment = $order->getPayment();
        $paymentMethod = $orderPayment->getMethodInstance();
        $paymentMethodCode = $paymentMethod->getCode();
        $orderAllItems = $order->getAllItems(); 
        if ($orderAllItems) {
            foreach ($orderAllItems as $item) {
                if ( $item->getProduct() && $item->getProduct()->getData('prescription_required') && $item->getProduct()->getData('prescription_required') == 'required')
                {
                   $containsPresciption = TRUE;
                }
            }
        }
        
        $orderCondComb['payment_method'] = $paymentMethodCode;
        $orderCondComb['prescription_required'] = $containsPresciption;

        return $orderCondComb;
    }
}