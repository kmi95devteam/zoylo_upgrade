<?php
namespace Zoylo\Csquare\Controller\Adminhtml\OnHoldOrders;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    /**
     * Authorization object.
     *
     * @var AuthorizationInterface
     */
    protected $_authorization;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        $this->_authorization = $context->getAuthorization();
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
    * {@inheritdoc}
    */
    protected function _isAllowed()
    {
        $aclResource = 'Zoylo_Csquare::onholdorder_report';

        if ($this->_authorization->isAllowed($aclResource) === false) {
            return '';
        }else{
            return $this;
        }
    }
    
    /**
     * Index action
     *
     * @return void
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Zoylo_Csquare::csquare');
        $resultPage->addBreadcrumb(__('Zoylo'), __('Zoylo'));
        $resultPage->addBreadcrumb(__("List of SKU's resulting in on-hold orders"), __("List of SKU's resulting in on-hold orders"));
        $resultPage->getConfig()->getTitle()->prepend(__("List of SKU's resulting in on-hold orders"));
        return $resultPage;
    }
}
