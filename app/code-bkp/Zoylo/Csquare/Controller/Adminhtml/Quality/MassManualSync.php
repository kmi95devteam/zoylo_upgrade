<?php
namespace Zoylo\Csquare\Controller\Adminhtml\Quality;

use Magento\Backend\App\Action;

class MassManualSync extends \Magento\Backend\App\Action
{
    
    /**
     * Authorization object.
     *
     * @var AuthorizationInterface
     */
    protected $_authorization;

    /**
    * @var \Zoylo\Csquare\Model\OrdersFactory
    */
    protected $_csquareOrdersSyncFactory;

    /**
    * 
    * @param Data $data
    */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Zoylo\Csquare\Model\OrderSyncFactory $csquareOrdersSyncFactory
    ){
        $this->_authorization = $context->getAuthorization();
        $this->_csquareOrdersSyncFactory = $csquareOrdersSyncFactory;
        parent::__construct($context);
    }

    /**
    * {@inheritdoc}
    */
    protected function _isAllowed()
    {
        $aclResource = 'Zoylo_Csquare::manual_sync';
        if ($this->_authorization->isAllowed($aclResource) === false) {
            return '';
        }else{
            return $this;
        }
    }

    /**
     * Update blog post(s) status action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $orderReportIds = $this->getRequest()->getParam('order_report_ids');
        if (!is_array($orderReportIds) || empty($orderReportIds)) {
            $this->messageManager->addError(__('Please select entry.'));
        } else {
            try {
                $createOrUpdateIds = [];
                $invoiceIds = [];
                foreach ($orderReportIds as $orderReportId) {
                    $orderReport = $this->_objectManager->get('Zoylo\Csquare\Model\Orders')->load($orderReportId);
                    $fullfilmentStatus = $orderReport->getFullfilmentStatus();
                    $orderReportStatus = $orderReport->getStatus();

                    if($fullfilmentStatus == 'Order Pending' && $orderReportStatus == 2){
                        $invoiceIds[] = $orderReportId;
                    }elseif( ($fullfilmentStatus == 'Error' && $orderReportStatus == 3) || ($orderReportStatus == 5) || ($fullfilmentStatus == '' && $orderReportStatus == 1) ) {
                       $createOrUpdateIds[] =  $orderReportId;
                    }
                }

                // Order Create/Update Call
                if(count($createOrUpdateIds) > 0){
                    $entity     = 'order';
                    $post       = 1;
                    $action     = '';
                    $orderSyncApi = $this->_csquareOrdersSyncFactory->create()->processOrder($entity, $createOrUpdateIds, $action, $post);
                }

                // Order Status API Call 
                if (count($invoiceIds) > 0) {
                    $manualSync = true;
                    $orderSyncStatusApi = $this->_csquareOrdersSyncFactory->create()->checkStatus($invoiceIds, $manualSync);
                }

                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been updated.', count($orderReportIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory->create()->setPath('csquare/*/index');
    }

}
