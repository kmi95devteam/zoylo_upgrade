<?php
namespace Zoylo\Csquare\Controller\Adminhtml\Quality;

use Magento\Backend\App\Action;

/**
 * Class MassDelete
 */
class MassDelete extends \Magento\Backend\App\Action
{
    
    /**
     * Authorization object.
     *
     * @var AuthorizationInterface
     */
    protected $_authorization;

    /**
    * 
    * @param Data $data
    */
    public function __construct(
        \Magento\Backend\App\Action\Context $context
    ){
        $this->_authorization = $context->getAuthorization();
        parent::__construct($context);
    }

    /**
    * {@inheritdoc}
    */
    protected function _isAllowed()
    {
        $aclResource = 'Zoylo_Csquare::order_report_delete';
        if ($this->_authorization->isAllowed($aclResource) === false) {
            return '';
        }else{
            return $this;
        }
        
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $orderReportIds = $this->getRequest()->getParam('order_report_ids');
        if (!is_array($orderReportIds) || empty($orderReportIds)) {
            $this->messageManager->addError(__('Please select entry.'));
        } else {
            try {
                foreach ($orderReportIds as $orderReportId) {
                    $orderReport = $this->_objectManager->get('Zoylo\Csquare\Model\Orders')->load($orderReportId);
                    $orderReport->delete();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been deleted.', count($orderReportIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory->create()->setPath('csquare/*/index');
    }
}
