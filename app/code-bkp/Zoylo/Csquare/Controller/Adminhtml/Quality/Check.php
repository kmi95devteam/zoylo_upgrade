<?php
namespace Zoylo\Csquare\Controller\Adminhtml\Quality;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Check extends \Magento\Backend\App\Action
{
    /**
     * Authorization object.
     *
     * @var AuthorizationInterface
     */
    protected $_authorization;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        $this->_authorization = $context->getAuthorization();
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
    * {@inheritdoc}
    */
    protected function _isAllowed()
    {
        $aclResource = 'Zoylo_Csquare::order_report';

        if ($this->_authorization->isAllowed($aclResource) === false) {
            return '';
        }else{
            return $this;
        }
    }
    
    /**
     * Index action
     *
     * @return void
     */
    public function execute()
    { 
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}
