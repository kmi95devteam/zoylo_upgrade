<?php
namespace Zoylo\Csquare\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Update extends \Magento\Backend\App\Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    protected $_productsFactory;
    /**
     * @param Context     $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Zoylo\Csquare\Model\ProductsFactory $productsFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_productsFactory = $productsFactory;
    }

    /**
     * Customer groups list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $dataId = $this->getRequest()->getParam('instanceId');
        $data = $this->getRequest()->getParam('dataUp');
        if ($dataId) {
            $this->ajaxHandle($dataId, $data);
        } else {
            return "no id found";
        }
    }
    public function ajaxHandle($dataId, $data)
    {
        $updatData =  $this->_productsFactory->create()->load($dataId, 'id');
        if ($updatData) {
		   $updatData->setProductData($data);
		   $updatData->save();
		   return "updated";
        }
    }
}
