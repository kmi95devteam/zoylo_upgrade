<?php

namespace Zoylo\Csquare\Controller\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Region extends \Magento\Framework\App\Action\Action {

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    protected $objectManager;
  
    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
    Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
            \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->objectManager = $objectManager;
    }

    /**
     * Index action
     *
     * @return void
     */
    public function execute() {
        try {

            $resultJson = $this->resultJsonFactory->create();
            $name = $this->getRequest()->getParam('name');
            $name = explode(" ",$name);
            if(isset($name[0])){
                $defaultName = $name[0];
            }else {
                $defaultName = $name;
            }
            $region = $this->objectManager->create('Magento\Directory\Model\ResourceModel\Region\Collection')
                        ->addFieldToFilter('default_name', ['like' => '%'.$defaultName.'%'])          
                       ->getFirstItem();
            if(!empty($region)){
                 return $resultJson->setData(['region' =>$region->getId()]);
            }else {
                return $resultJson->setData(['success' => 'false']);
            }
           
            
        } catch (\Exception $e) {
            return $resultJson->setData(['success' => $e->getMessage()]);
        }
    }

}
