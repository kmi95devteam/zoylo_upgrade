<?php
namespace Zoylo\Exportproductinfo\Controller\Adminhtml\Order;
use Magento\Framework\App\Filesystem\DirectoryList;
class ExportData extends \Magento\Backend\App\Action
    {
    /**
    * @var \Magento\Framework\View\Result\PageFactory
    */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    
    protected $resourceConnection;
    
    protected $fileFactory;
    
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Filesystem $filesystem
    ) {
            parent::__construct($context);
            $this->resultPageFactory = $resultPageFactory;
            $this->resourceConnection = $resourceConnection;
            $this->_fileFactory = $fileFactory;
            $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
    }

    /**
     * Load the page defined in view/adminhtml/layout/route_id_index_index.xml
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $connection = $this->resourceConnection->getConnection();
        $columnToAdd = array('increment_id'=>"Order Id",'customer_name'=>"Customer Name",'customer_id'=>"Customer ID",'customer_mobile_number'=>"Customer Number",'customer_email'=>"Customer Email",'customer_address'=>"Shipping Address",'shipping_city'=>'Shipping City','shipping_pincode'=>'Shipping Pincode','shipping_state'=>'Shipping State','prescription_values'=>"Prescription Attached",'created_at'=>"Purchase Date",'updated_at'=>"Order Updated At",'payment_method'=>"Payment Method",'coupon_code'=>"Coupon Code Used",'grand_total'=>"Order Value",'status'=>"Order Status",'sku'=>"Sku ID",'ucode_id'=>'ucode_id','name'=>'Name','ecomteck_order_comment'=>'Customer Note','admin_order_notes'=>'Admin Order Notes','first_on_hold'=>'Order First On Hold Date and Time','qty_ordered'=>'Quantity Ordered','display_pack_size'=>'Display Pack Size','cancellation_reason'=>'Cancellation Reason','order_cancelled_by'=>'Order Cancelled By');
        $columnToAddValues = array_values($columnToAdd);
        $columnHeader = [];
        foreach($columnToAddValues as $colHeader)
        {
            $columnHeader[] = $colHeader;
        }
        $name = date('m_d_Y_H_i_s');
        $filepath = 'export/custom' . $name . '.csv';
        $this->directory->create('export');
        /* Open file */
        $stream = $this->directory->openFile($filepath, 'w+');
        $stream->lock();
        
        /* Write Header */
        $stream->writeCsv($columnHeader);
        
        $selectQuery = "SELECT so.entity_id, so.increment_id,so.customer_firstname,so.customer_lastname,so.customer_id,so.customer_mobile_number,so.customer_email,so.prescription_values,AddTime(so.created_at,'05:30:00') as created_at,AddTime(so.updated_at,'05:30:00') as updated_at,so.grand_total,so.coupon_code,so.status,so.ecomteck_order_comment,sop.method as payment_method, soi.sku,soi.name,soi.product_id,soi.qty_ordered as qty_ordered,so.cancellation_reason,so.order_cancelled_by  FROM ".$this->resourceConnection->getTableName('sales_order_item')." soi INNER JOIN ".$this->resourceConnection->getTableName('sales_order')." so ON soi.order_id = so.entity_id LEFT JOIN ".$this->resourceConnection->getTableName('sales_order_payment'). " sop ON so.entity_id = sop.parent_id WHERE 1 ";
        
        if ($this->_request->getParam('from_date'))
        {
            $selectQuery =  $selectQuery." AND so.created_at >=  DATE_ADD('".$this->_request->getParam('from_date') ."',INTERVAL -1 DAY) ";  ;
        }
        if ($this->_request->getParam('to_date'))
        {
            $selectQuery =  $selectQuery." AND so.created_at <= DATE_ADD('".$this->_request->getParam('to_date') ."',INTERVAL +1 DAY) ";  ;             
        }
        if ($this->_request->getParam('order_status'))
        {
            $selectQuery =  $selectQuery." AND so.status = '".$this->_request->getParam('order_status') ."' ";            
        }
        if ($this->_request->getParam('payment_method'))
        {
            $selectQuery =  $selectQuery." AND sop.method = '".$this->_request->getParam('payment_method') ."' ";            
        }
        $columValues = [];
        $queryResult = $connection->fetchAll($selectQuery);
        $iCount = 0;
        foreach($queryResult as $resKey=>$resValue)
        {
            $orderAddressQuery = "SELECT firstname, middlename, lastname, street, city, region, postcode, telephone FROM ".$this->resourceConnection->getTableName('sales_order_address')." WHERE parent_id = '".$resValue['entity_id']."' AND address_type = 'shipping'";
            $orderAddressResult = $connection->fetchAll($orderAddressQuery);
            $columValues = [];
            foreach($columnToAdd as $colKey=>$colVal)
            {
                if (isset($resValue[$colKey]))
                {
                    if ($colKey == 'payment_method')
                    {
                        if ($resValue[$colKey] == 'cashondelivery' || $resValue[$colKey] == 'free')
                        {
                            $columValues[] = 'Cash On Delivery';
                        }
                        else
                        {
                            $columValues[] = 'Pay Later';
                        }
                    }
                    else if ($colKey == 'status')
                    {
                        if (isset($resValue[$colKey]) && $resValue[$colKey] != '')
                        {
                            $statusLblQry = "SELECT label FROM ".$this->resourceConnection->getTableName('sales_order_status')." WHERE status = '".$resValue[$colKey]."'";
                            $statusLblRes = $connection->fetchAll($statusLblQry);
                            if (isset($statusLblRes[0]['label']))
                            {
                                $columValues[] = $statusLblRes[0]['label'];
                            }
                        }
                        else
                        {
                            $columValues[] = '';
                        }
                    }
                    else if ($colKey == 'prescription_values')
                    {
                        if (isset($resValue[$colKey]) && $resValue[$colKey] != '')
                        {
                            $columValues[] = 'Yes';
                        }
                        else
                        {
                            $columValues[] = 'No';
                        }
                    }
                    
                    else
                    {
                       $columValues[] = $resValue[$colKey];                        
                    }
                }
                else
                {
                    if ($colKey == 'customer_name')
                    {
                        $columValues[] = $resValue['customer_firstname'].' '.$resValue['customer_lastname'];
                    }
                    else if ($colKey == 'customer_address')
                    {
                        $customerAddress = $orderAddressResult[0]['firstname'].' '.$orderAddressResult[0]['middlename'].' '.$orderAddressResult[0]['lastname'].','.$orderAddressResult[0]['street'].','.$orderAddressResult[0]['city'].','.$orderAddressResult[0]['city'].','.$orderAddressResult[0]['region'].','.$orderAddressResult[0]['postcode'].','.$orderAddressResult[0]['telephone'];
                        $columValues[] = $customerAddress;
                    }
                    else if ($colKey == 'shipping_pincode')
                    {
                        $columValues[] = $orderAddressResult[0]['postcode'];
                    }
                    else if ($colKey == 'shipping_city')
                    {
                        $columValues[] = ucfirst($orderAddressResult[0]['city']);                        
                    }
                    else if($colKey == 'shipping_state')
                    {
                        $columValues[] = $orderAddressResult[0]['region'];                        
                    }
                    else if ($colKey == 'admin_order_notes')
                    {
                        /*$salesHistoryQuery = "SELECT comment FROM ".$this->resourceConnection->getTableName('sales_order_status_history')." WHERE parent_id = '".$resValue['entity_id']."'";
                        $salesHistoryResult = $connection->fetchAll($salesHistoryQuery);
                        $commentArr = [];
                        $commentsStr = "";
                        foreach($salesHistoryResult as $eachCommentHis)
                        {
                            $commentArr[] = filter_var($eachCommentHis['comment'],FILTER_SANITIZE_STRING);
                            $commentsStr = $commentsStr.filter_var($eachCommentHis['comment'],FILTER_SANITIZE_STRING);
                        }*/
                        $columValues[] = '';
                    }
                    else if ($colKey == 'first_on_hold')
                    {
                        $salesHistOnHoldFirstQry = "SELECT AddTime(created_at,'05:30:00') as created_at FROM ".$this->resourceConnection->getTableName('sales_order_status_history')." WHERE parent_id = '".$resValue['entity_id']."' AND status = 'on_hold_cs' ORDER BY created_at ASC LIMIT 1 ";
                        $salesHisTonHoldFirstRes = $connection->fetchAll($salesHistOnHoldFirstQry);
                        
                        if (isset($salesHisTonHoldFirstRes[0]['created_at']))
                        {
                            $columValues[] = $salesHisTonHoldFirstRes[0]['created_at'];
                        }
                        else
                        {
                            $columValues[] = '';
                        }
                    }
                    else if ($colKey == 'prescription_values')
                    {
                        if (isset($resValue[$colKey]) && $resValue[$colKey] != '')
                        {
                            $columValues[] = 'Yes';
                        }
                        else
                        {
                            $columValues[] = 'No';
                        }
                    }
                    else if ($colKey == 'ucode_id')
                    {
                        $productQuery = "SELECT value FROM ".$this->resourceConnection->getTableName('catalog_product_entity_varchar')." WHERE attribute_id = (SELECT attribute_id FROM ".$this->resourceConnection->getTableName('eav_attribute')." WHERE attribute_code = 'ucode_id' AND entity_type_id='4'  ) AND entity_id = '".$resValue['product_id']."' ";
                        $productQueryResult = $connection->fetchAll($productQuery);
                        if (isset($productQueryResult[0]['value']))
                        {
                            $columValues[] = $productQueryResult[0]['value'];    
                        }
                        else
                        {
                            $columValues[] = '';
                        }
                    }
                    else if ($colKey == 'display_pack_size')
                    {
                        $productQuerySize = "SELECT value FROM ".$this->resourceConnection->getTableName('catalog_product_entity_varchar')." WHERE attribute_id = (SELECT attribute_id FROM ".$this->resourceConnection->getTableName('eav_attribute')." WHERE attribute_code = 'display_pack_size' AND entity_type_id='4'  ) AND entity_id = '".$resValue['product_id']."' ";
                        $productQuerySizeResult = $connection->fetchAll($productQuerySize);
                        if (isset($productQuerySizeResult[0]['value']))
                        {
                            $columValues[] = $productQuerySizeResult[0]['value'];    
                        }
                        else
                        {
                            $columValues[] = '';
                        }
                    }
                    else
                    {
                        $columValues[] = '';
                    }
                    //$columValues[] = 'Test please ignore';
                }
            }
            if ($resValue['created_at'] >= $this->_request->getParam('from_date')." 00:00:00 " && $resValue['created_at'] <= $this->_request->getParam('to_date')." 23:59:59 ")
            $stream->writeCsv($columValues);
        }
 
        $csvfilename = 'OrderExport.csv';
        $content = [];
        $content['type'] = 'filename'; // must keep filename
        $content['value'] = $filepath;
        $content['rm'] = '1'; //remove csv from var folder
        return $this->_fileFactory->create($csvfilename, $content, DirectoryList::VAR_DIR);
    }
    
    function clean($string) {
        //$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
     }
}
?>
  