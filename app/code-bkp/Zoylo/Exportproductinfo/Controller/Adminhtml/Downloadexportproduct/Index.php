<?php
namespace Zoylo\Exportproductinfo\Controller\Adminhtml\Downloadexportproduct;
use Magento\Framework\Controller\ResultFactory;
class Index extends \Magento\Backend\App\Action
{
    protected $fileFactory;
    protected $csvProcessor;
    protected $directoryList;
    protected $date;
    protected $productCollection;
    protected $productModel;
    protected $objectManager;

	public function __construct(
        \Magento\Backend\App\Action\Context $context,
    	\Magento\Framework\App\Response\Http\FileFactory $fileFactory,
    	\Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection,
        \Magento\Catalog\Model\Product $productModel
	)
	{
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$this->fileFactory = $fileFactory;
    	$this->csvProcessor = $csvProcessor;
    	$this->directoryList = $directoryList;
        $this->date = $date;
        $this->productCollection = $productCollection;
        $this->productModel = $productModel;
    	parent::__construct($context);
	}
    
    public function execute()
    {
        $date = $this->date->gmtDate();
        $postValues = $this->getRequest()->getPost();
        $minValue = $postValues['minprocount'];
        $maxValue = $postValues['maxprocount'];
        
        if((!empty($minValue) && $minValue>0) && (!empty($maxValue) && $maxValue>0)){
            $productFinalDataArray = $this->downloadExportProductCsvfile($minValue,$maxValue);
            if(!empty($productFinalDataArray)){
            $fileName = 'ProductsData-'.$date.'.csv';
            $filePath = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR). "/" . $fileName;
                    $this->csvProcessor
                    ->setDelimiter(',')
                    ->setEnclosure('"')
                    ->saveData(
                    $filePath,
                    $productFinalDataArray
                    );
                    $this->fileFactory->create(
                        $fileName,
                        [
                        'type' => "filename",
                        'value' => $fileName,
                        'rm' => true,
                        ],
                        \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR,
                        'application/octet-stream'
                    ); 
                   
            }else{
                
                $this->messageManager->addError(__('Empty data. No records found based on giving min and max values!!'));
                $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultRedirect->setPath('exportproductinfo/index/index');
                return $resultRedirect;
            }
        }else{
            $this->messageManager->addError(__('Please enter Min and Max input limit values.'));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('exportproductinfo/index/index');
            return $resultRedirect;
        }
	}
        
        
    public function downloadExportProductCsvfile($minValue,$maxValue){
        $productFinalData = $this->getProductData($minValue,$maxValue);
        return $productFinalData;
    }
        
    protected function getProductData($minValue,$maxValue)
	{                                   
        $result = [];
        $result = $this->getAllProductDataIntoArray($minValue,$maxValue);
        return $result;
	}

    public function getAllProductDataIntoArray($minValue,$maxValue){
        
        //$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
        $collection = $this->productCollection->addAttributeToSelect('*');
        if($minValue>0 && !empty($minValue)){
            $collection->addFieldToFilter('entity_id', ['gteq' => $minValue]);
        }
        if($maxValue>0 && !empty($maxValue)){
            $collection->addFieldToFilter('entity_id', ['lteq' => $maxValue]);
        }
        
        $count = count($collection->getData());
        $fcol = $collection->getData();
        $data = array();
        if($count>0){
            $i=0;
            $data[$i]['ucode_id'] = 'ucode_id';
            $data[$i]['product_id'] = 'product_id';
            $data[$i]['sku_id'] = 'sku_id';
            $data[$i]['sku_name'] = 'sku_name';
            $data[$i]['sku_display_name'] = 'sku_display_name';
            $data[$i]['sku_group_name'] = 'sku_group_name';
            $data[$i]['status'] = 'status';
            $data[$i]['price'] = 'price';
            $data[$i]['brand'] = 'brand';
            $data[$i]['zoylo_sku_group'] = 'zoylo_sku_group';
            $data[$i]['zoylo_product_group'] = 'zoylo_product_group';
            $data[$i]['zoylo_product_type'] = 'product_type';
            $data[$i]['zoylo_product_category'] = 'zoylo_product_category';
            $data[$i]['consume_group'] = 'consume_group';
            $data[$i]['consume_type'] = 'consume_type';
            $data[$i]['drug_type'] = 'drug_type';
            $data[$i]['pack_type'] = 'pack_type';
            $data[$i]['pack_size'] = 'pack_size';
            $data[$i]['display_pack_size'] = 'display_pack_size';
            $data[$i]['prescription_required'] = 'prescription_required';
            $data[$i]['sellable_flag'] = 'sellable_flag';
            $data[$i]['is_returnable'] = 'is_returnable';
            $data[$i]['top_sku'] = 'top_sku';
            $data[$i]['sku_speed'] = 'sku_speed';
            $data[$i]['quantity_limit'] = 'quantity_limit';
            $data[$i]['stock'] = 'stock';
            $i=1;
            foreach($fcol as $product){
                $productEntityId = $product['entity_id'];
                if(!empty($productEntityId)){
                    $productInfo = $this->objectManager->create('Magento\Catalog\Model\Product')->load($productEntityId);
                    
                    $data[$i]['ucode_id'] = $productInfo->getucode_id();
                    $data[$i]['product_id'] = $productEntityId;
                    $data[$i]['sku_id'] = $productInfo->getSku();
                    $data[$i]['sku_name'] = $productInfo->getName();
                    $data[$i]['sku_display_name'] = $productInfo->getsku_display_name();
        			$data[$i]['sku_group_name'] = $productInfo->getsku_group_name();
                    if($productInfo->getStatus() == 1) {
                        $status = "Enabled";
        			}elseif($productInfo->getStatus() == 2){
                        $status = "Disabled";
        			}
        			$data[$i]['status'] = $status;
                    $data[$i]['price'] = $productInfo->getPrice();
                    
                    $optionid = $productInfo->getProductBrand();
                    $product_brand  = '';
                    if($optionid){
                            $product_brand = $this->checkBrandOptionValue($optionid);   
                            $data[$i]['brand'] = $product_brand;
                    }else{
                            $data[$i]['brand'] = $product_brand;
                    }
                    $data[$i]['zoylo_sku_group'] = $productInfo->getzoylo_sku_group();
                    $data[$i]['zoylo_product_group'] = $productInfo->getzoylo_product_group();
                    $data[$i]['zoylo_product_type'] = $productInfo->getzoylo_product_type();

                    $catNames = '';
                    $categoryNames = [];
                    $categories = $productInfo->getCategoryIds(); /*will return category ids array*/
                    foreach($categories as $category){
                        $cat = $this->objectManager->create('Magento\Catalog\Model\Category')->load($category);
                        $categoryNames[] = $cat->getName();
                    }
                    if(count($categoryNames) > 0){
                        $catNames = implode("/", $categoryNames);
                    }
                    $data[$i]['zoylo_product_category'] = $catNames;
                    $data[$i]['consume_group'] = $productInfo->getconsume_group();
                    $data[$i]['consume_type'] = $productInfo->getconsume_type();
                    $data[$i]['drug_type'] = $productInfo->getdrug_type();
                    $data[$i]['pack_type'] = $productInfo->getpack_type();
                    $data[$i]['pack_size'] = $productInfo->getpack_size();
                    $data[$i]['display_pack_size'] = $productInfo->getdisplay_pack_size();
                    $prescription_required = '';
                    if($productInfo->getPrescriptionRequired() == 'required') {
                            $prescription_required = "Required";
                    }elseif($productInfo->getPrescriptionRequired() == 'not_required'){
                            $prescription_required = "Not Required";
                    }
                    $data[$i]['prescription_required'] = $prescription_required;
                    $sellable_flag = '';
                    if($productInfo->getsellable_flag() == 1){
                        $sellable_flag = 'Yes';
                    }elseif($productInfo->getsellable_flag() == 0){
                        $sellable_flag = 'Banned';
                    }else if($productInfo->getsellable_flag() == 2){
                        $sellable_flag = 'Discontinued';
                    }
                    $data[$i]['sellable_flag'] = $sellable_flag;
                    $returnable = '';
                    if($productInfo->getReturnable() == 0) {
                            $returnable = "No";
                    }elseif($productInfo->getReturnable() == 1){
                            $returnable = "Yes";
                    }
                    $data[$i]['is_returnable'] = $returnable;

                    $top_sku = '';
                    if($productInfo->gettop_sku() == 0) {
                            $top_sku = "No";
                    }elseif($productInfo->gettop_sku() == 1){
                            $top_sku = "Yes";
                    }
                    $data[$i]['top_sku'] = $top_sku;

                    $data[$i]['sku_speed'] = $productInfo->getsku_speed();
                    $stockItem = $productInfo->getExtensionAttributes()->getStockItem();
                    $data[$i]['quantity_limit'] = round($stockItem->getmax_sale_qty());
                    $data[$i]['stock'] = $stockItem->getqty();
                }
                $i++;
            }
        }
        return $data;
    }
    public function checkBrandOptionValue($optionid)
	{
		$resource = $this->objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$sql = "SELECT * FROM eav_attribute_option_value WHERE option_id = '$optionid'";
		$result = $connection->fetchAll($sql);
		if (!empty($result)) {
			$value = $result[0]['value'];
			return $value;
		}else{
			return false;
		}
	}
}

?>