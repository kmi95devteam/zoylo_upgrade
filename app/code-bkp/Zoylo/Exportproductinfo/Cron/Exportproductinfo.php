<?php
namespace Zoylo\Exportproductinfo\Cron;
use Magento\Store\Model\Store;
use Magento\Framework\App\Area;
class Exportproductinfo
{
	protected $logger;
	protected $fileFactory;
	protected $csvProcessor;
	protected $directoryList;
	protected $date;
	protected $productCollection;
	protected $productModel;
	protected $objectManager;

	public function __construct(
		\Psr\Log\LoggerInterface $loggerInterface,
		\Magento\Framework\App\Response\Http\FileFactory $fileFactory,
		\Magento\Framework\File\Csv $csvProcessor,
		\Magento\Framework\App\Filesystem\DirectoryList $directoryList,
		\Magento\Framework\Stdlib\DateTime\DateTime $date,
		\Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection,
		\Magento\Catalog\Model\Product $productModel
	) {
		$this->logger = $loggerInterface;
		$this->fileFactory = $fileFactory;
		$this->csvProcessor = $csvProcessor;
		$this->directoryList = $directoryList;
		$this->date = $date;
		$this->productCollection = $productCollection;
		$this->productModel = $productModel;
		$this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	}

	public function execute() {
                $scopeConfig = $this->objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');
                $cronEnableStatus = $scopeConfig->getValue('export_product_emails/email_configurations/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                if($cronEnableStatus==1){
                    echo "Zoylo Exportproductinfo cron starts";
                    $this->logger->debug('Zoylo\Exportproductinfo\Cron\Exportproductinfo starts');
                    $lastProductId = 1;
                    $this->exportProducts($lastProductId);
                    $this->logger->debug('Zoylo\Exportproduct\Cron\Exportproductonfo Ends');
                    echo "Zoylo Exportproductinfo cron ends";
                }else{
                    echo "Zoylo Exportproductinfo cron status is Disabled.Please check in admin configurations!!";
                }
	}

	 /*
	 * Get the product collection last 48 hours
	 * @params lastproductid
	 * send email to admins
	 **/
	 public function exportProducts($lastProductId){
		 	$directory = $this->objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
		 	$rootPath  =  $directory->getRoot();
		 	$path = $rootPath."/scripts/";
		 	$filename = 'productsexport.csv';
		 	$file = $path.$filename;
		 	$fp = fopen($file,"w+");
		 	$date = $this->date->gmtDate();
		 	$lastUpdateDate = date('Y-m-d H:i:s', strtotime('-48 hour'));
		 	$productCollection = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
		 	$collection = $productCollection->addAttributeToSelect('*');
		 	$collection->addAttributeToFilter('updated_at', array('from'=>$lastUpdateDate));
	        //echo "<br>select query:--".$collection->getSelect();

		 	if($lastProductId > 0){
		 		if($lastProductId == 1){
		 			$collection->addFieldToFilter('entity_id', ['gteq' => $lastProductId]);
		 		}else{
		 			$collection->addFieldToFilter('entity_id', ['gt' => $lastProductId]);
		 		}
		 	}
		 	$collection->setPageSize(30000);
			//->setStoreId(1)
			//->load();

		 	$count = count($collection->getData());
		 	$fcol = $collection->getData();

		 	$i = 1;
		 	if($count>0){
		 		$headers = ["Product Id","Sku", "Name", "Price","Status","Visibility","Zoylo Product Type","Top Sku","Otc Brands","Medicine Brands", "Product Format", "Pack Size","Composition","Pin code Zone","Shipping Type","Returnable","Refrigeratable","Prescption Required"];
		 		fputcsv($fp, $headers);
		 		foreach($fcol as $product){
		 			$productEntityId = $product['entity_id'];
		 			if(!empty($productEntityId)){
		 				$productInfo = $this->objectManager->create('Magento\Catalog\Model\Product')->load($productEntityId);
		 				$data = array();
		 				$data[] = $productInfo->getId();
		 				$data[] = $productInfo->getSku();
		 				$data[] = $productInfo->getName();
		 				$data[] = $productInfo->getPrice();
		 				if($productInfo->getStatus() == 1) {
		 					$status = "Enabled";
		 				}elseif($productInfo->getStatus() == 2){
		 					$status = "Disabled";
		 				}
		 				$data[] = $status;

		 				if($productInfo->getVisibility() == 4) {
		 					$data[] = "Catalog,Search";
		 				}

		 				$data[] = $productInfo->getZoyloProductType();
		 				$data[] = $productInfo->getTopSku();
		 				$optionid = $productInfo->getProductBrand();
		 				$optionids = $productInfo->getManufacturer();
		 				$product_brand	= '';
		 				if($optionid){
		 					$product_brand = $this->checkBrandOptionValue($optionid);	
		 					$data[] = $product_brand;
		 				}else{
		 					$data[] = $product_brand;
		 				}
		 				$manufacturer = '';
		 				if($optionids){	
		 					$manufacturer = $this->checkBrandOptionValue($optionids);
		 					$data[] = $manufacturer; 
		 				}else{
		 					$data[] = $manufacturer; 
		 				}

		 				$foption = $productInfo->getProductFormat();
		 				$format = '';
		 				if($foption){
		 					$format = $this->checkBrandOptionValue($foption);
		 					$data[] = $format;
		 				}else{
		 					$data[] = $format;
		 				}
		 				$data[] = $productInfo->getPackSize();
		 				$data[] = $productInfo->getSaltComposition();
		 				$data[] = $productInfo->getPincodeZone();
		 				$data[] = $productInfo->getShippingType();
		 				if($productInfo->getReturnable() == 0) {
		 					$returnable = "No";
		 				}elseif($productInfo->getReturnable() == 1){
		 					$returnable = "Yes";
		 				}
		 				$data[] = $returnable;

		 				$refrigeratable = $productInfo->getResource()->getAttribute('refrigeratable')->getFrontend()->getValue($productInfo);
						$data[] = ucfirst($refrigeratable);

		 				$value = '';
		 				if($productInfo->getPrescriptionRequired() == 'required') {
		 					$value = "Required";
		 				}elseif($productInfo->getPrescriptionRequired() == 'not_required'){
		 					$value = "Not Required";
		 				}
		 				$data[] = $value;
		 				//echo "<pre>";print_r($data);
		 				fputcsv($fp, $data); 	
		 			}
		 			if($i == $count){


		 				$directory = $this->objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
		 				$rootPath  =  $directory->getRoot();
		 				$path = $rootPath."/scripts/";
		 				$filename = 'productsexport.csv';
		 				$file = $path.$filename;
	               		//Send email to admins
		 				$this->sendEmail($file);
						/*$content = file_get_contents($file);
						$content = chunk_split(base64_encode($content));
						$uid = md5(uniqid(time()));
						$name = basename($file);
						$message = 'Please find the below attachment for the export Products List';
						$from_name = "Zoylo";
						$from_mail = "noreply@zoylo.com";
						$objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
		                                    $date = $objDate->gmtDate();
						//$mailto = "hiranmayi.p@zoylo.com";
						$mailto = "ajeshbabu.g@zoylo.com";
						$subject = "Products Export List of ". $date;
						//$bccEmails = "lakshmi.p@zoylo.com, srinivasrao.p@zoylo.com, ajeshbabu.g@zoylo.com, aditya.a@zoylo.com, sravanthi.n@zoylo.com, kanupriya.s@zoylo.com, Saranraju.B@VivaPharmacy.Com";
						$bccEmails = "ajeshbabug@gmail.com";
						

						// header
						$header = "From: ".$from_name." <".$from_mail.">\r\n";
						$header .= "Bcc: ".$bccEmails . "\r\n";
						$header .= "MIME-Version: 1.0\r\n";
						$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";

						// message & attachment
						$nmessage = "--".$uid."\r\n";
						$nmessage .= "Content-type:text/plain; charset=iso-8859-1\r\n";
						$nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
						$nmessage .= $message."\r\n\r\n";
						$nmessage .= "--".$uid."\r\n";
						$nmessage .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n";
						$nmessage .= "Content-Transfer-Encoding: base64\r\n";
						$nmessage .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
						$nmessage .= $content."\r\n\r\n";
						$nmessage .= "--".$uid."--";

						if (mail($mailto, $subject, $nmessage, $header)) {
							echo "\n Email has been send to Admins -".$mailto;
						    //return true; // Or do something here
						} else {
						  return false;
						}*/
					
					$lastProductId = $product['entity_id'];
					echo "<br>Last product ID:".$lastProductId;
					$this->exportProducts($lastProductId);
				}

				$i++;
			}

			fclose($fp);   
		}
	}

	
	/*
	* Check brand option value
	* @param option id
	* return value or boolean
	**/  
	public function checkBrandOptionValue($optionid)
	{
		$resource = $this->objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$sql = "SELECT * FROM eav_attribute_option_value WHERE option_id = '$optionid'";
		$result = $connection->fetchAll($sql);
		if (!empty($result)) {
			$value = $result[0]['value'];
			return $value;
		}else{
			return false;
		}
	}

	/*
	* Send email to Admins
	* @params csvfilepath
	* return boolean
	**/
	public function sendEmail($csvfile){
		if(!empty($csvfile)){
			$scopeConfig = $this->objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');
                        $smtpEnableStatus =$scopeConfig->getValue('smtp/general/enabled', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                        if($smtpEnableStatus==1){
                            $hostName =$scopeConfig->getValue('smtp/configuration_option/host', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $authentication =$scopeConfig->getValue('smtp/configuration_option/authentication', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $username =$scopeConfig->getValue('smtp/configuration_option/username', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $protocol =$scopeConfig->getValue('smtp/configuration_option/protocol', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $port =$scopeConfig->getValue('smtp/configuration_option/port', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $password =$scopeConfig->getValue('smtp/configuration_option/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $returnpath =$scopeConfig->getValue('smtp/configuration_option/return_path_email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $smtpDataHelper = $this->objectManager->get('Mageplaza\Smtp\Helper\Data');
                            $mailResource = $this->objectManager->get('Mageplaza\Smtp\Mail\Rse\Mail');
                            //$transportBuilder = $objectManager->get('Magento\Framework\Mail\Template\TransportBuilder');
                            $transportBuilder = $this->objectManager->get('Zoylo\Exportproductinfo\Model\Mail\TransportBuilder');
                            $storeManager = $this->objectManager->create('Magento\Store\Model\StoreManagerInterface');

                            $config = [
                            'type'       => 'smtp',
                            'host'       => $hostName,
                            'auth'       => $authentication,
                            'username'   => $username,
                            'ignore_log' => true,
                            'force_sent' => true
                            ];
                            $config['ssl'] = $protocol;
                            $config['port'] = $port;
                            $config['password'] = $smtpDataHelper->getPassword();
                            $config['return_path'] = $returnpath;

                            $sender = array('email' => 'noreply@zoylo.com','name' => 'Zoylo');
                            $toEmailIds =$scopeConfig->getValue('export_product_emails/email_configurations/toemailids', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            if($toEmailIds !=''){
                                $finalToEmails = explode(",",$toEmailIds);
                            }
                                        $bccEmailIds =$scopeConfig->getValue('export_product_emails/email_configurations/bccemailids', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            if($bccEmailIds !=''){
                                $bccemails = explode(",",$bccEmailIds);
                            }

                            $mailResource->setSmtpOptions(Store::DEFAULT_STORE_ID, $config);
                            //$bccemails = ['gajeshbabumca@gmail.com', 'srinivasrao.p@zoylo.com'];
                            $body = file_get_contents($csvfile); 
                            $mimeType    = \Zend_Mime::TYPE_OCTETSTREAM;
                                $disposition = \Zend_Mime::DISPOSITION_ATTACHMENT;
                                $encoding    = \Zend_Mime::ENCODING_BASE64;
                            $filename = 'exportproduct.csv';
                            $date = $this->date->gmtDate();
                            $vars = array('generateddate' => $date,	
                                     'store' => $storeManager->getStore()
                                 );

                            $transportBuilder->setTemplateIdentifier('zoylo_product_export_send_email')
                                ->setTemplateOptions(['area' => Area::AREA_FRONTEND, 'store' => Store::DEFAULT_STORE_ID])
                                ->setTemplateVars($vars)
                                ->setFrom($sender)
                                //->addTo(array('To'=>'ajeshbabu.g@zoylo.com'))
                                ->addTo($finalToEmails)
                                ->addBcc($bccemails)
                                ->addAttachment($body, $mimeType, $disposition, $encoding, $filename);
                            try{
                                $transportBuilder->getTransport()->sendMessage();
                                        echo "<br>Product export Email sent success!!";
                                return true;
                            }catch (\Exception $e) {
                             echo $e->getMessage(); die;
                            }
                        }else{
                            echo "SMTP Email service is disabled";
                            return false;
                        }
			
		}else{
			echo "<br>Csv file not found!!!";
			return false;
		}
	}
}