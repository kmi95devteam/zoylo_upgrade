<?php

namespace Zoylo\Exportproductinfo\Block\Adminhtml\Index;

class Index extends \Magento\Backend\Block\Widget\Container
{
    protected $storeManager;
    public function __construct(\Magento\Backend\Block\Widget\Context $context,
            \Magento\Store\Model\StoreManagerInterface $storeManager,
            \Magento\Framework\Data\Form\FormKey $formKey,
            array $data = [])
    {
        $this->storeManager = $storeManager;
        $this->formKey = $formKey;
        parent::__construct($context, $data);
    }

    public function getUrlDownloadExportProducts(){
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $frontUrl = $om->get('Magento\Store\Model\StoreManagerInterface')->getStore(1)->getUrl('exportproductinfo/downloadexportproduct/index');
        return $frontUrl;
        
    }
   
    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }
}
