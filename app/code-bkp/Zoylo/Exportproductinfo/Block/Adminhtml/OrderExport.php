<?php

namespace Zoylo\Exportproductinfo\Block\Adminhtml;


class OrderExport extends \Magento\Framework\View\Element\Template {

   
    protected $statusCollection;
    
    public function __construct(\Magento\Catalog\Block\Product\Context $context,
                                \Magento\Sales\Model\ResourceModel\Order\Status\Collection $statusCollection,
                                array $data = []) 
    {
        parent::__construct($context, $data);
        $this->statusCollection = $statusCollection;
    }
    
    public function getCustUrl($url)
    {
        return $this->getUrl($url);
    }
    
    public function getStatusStateList()
    {
        $statusStateCollection = $this->statusCollection->toOptionArray();
        return $statusStateCollection;
    }
}