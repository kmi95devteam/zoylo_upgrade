<?php
namespace Zoylo\Exportproductinfo\Block\Adminhtml\Downloadexportproduct;

class Index extends \Magento\Backend\Block\Widget\Container
{
    protected $storeManager;
    public function __construct(\Magento\Backend\Block\Widget\Context $context,
            \Magento\Store\Model\StoreManagerInterface $storeManager,
            array $data = [])
    {
        $this->storeManager = $storeManager;
        parent::__construct($context, $data);
    }

    public function getUrlDownloadExportProducts(){
        return $baseUrl = $this->storeManager->getStore()->getBaseUrl().'admin/exportproductinfo/downloadexportproduct/index';
    }
}
