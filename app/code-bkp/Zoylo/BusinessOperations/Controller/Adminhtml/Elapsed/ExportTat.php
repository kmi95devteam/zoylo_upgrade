<?php
namespace Zoylo\BusinessOperations\Controller\Adminhtml\Elapsed;

use Magento\Framework\App\Filesystem\DirectoryList;
class ExportTat extends \Magento\Backend\App\Action
    {
    /**
    * @var \Magento\Framework\View\Result\PageFactory
    */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    
    protected $resourceConnection;
    
    protected $fileFactory;
    
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Model\OrderFactory $orderModel
    ) {
            parent::__construct($context);
            $this->resultPageFactory = $resultPageFactory;
            $this->resourceConnection = $resourceConnection;
            $this->_fileFactory = $fileFactory;
            $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
            $this->orderCollectionFactory = $orderCollectionFactory;
            $this->orderModel = $orderModel;
    }

    public function execute()
    {
        $customerId = 0; // default value
        $orderItemsDetails = "";
        $allOrders = array();
        $ordersCol = [];
        $allTimeRangeArr = $this->getRequest()->getParam('timerrange');
        $addedColumHeader = FALSE;
        foreach($allTimeRangeArr as $eachTimeRange)
        {
            if ($this->getRequest()->getParam('orderstatus') && $this->getRequest()->getParam('orderstatus') != '' && $this->getRequest()->getParam('timerrange') && $this->getRequest()->getParam('timerrange') != '')
            {
                $fromTimeRange = explode('-', $eachTimeRange)[0];
                $toTimeRange = explode('-', $eachTimeRange)[1];
                $DateTime1 = new \DateTime();
                if ($toTimeRange != '+')
                {
                    $fromDateTimeRange = $DateTime1->modify('-'.$toTimeRange.' hours');
                }
                $DateTime2 = new \DateTime();
                $toDateTimeRange = $DateTime2->modify('-'.$fromTimeRange.' hours');
                if ($toTimeRange == '+')
                {
                    $ordersCol = $this->orderCollectionFactory->create()->addAttributeToFilter('created_at', array('lt'=>$toDateTimeRange->format("Y-m-d H:i:s")))->addAttributeToFilter('status', $this->getRequest()->getParam('orderstatus'));
                }
                else
                {
                    $ordersCol = $this->orderCollectionFactory->create()->addAttributeToFilter('created_at', array('from'=>$fromDateTimeRange->format("Y-m-d H:i:s"), 'to'=>$toDateTimeRange->format("Y-m-d H:i:s")))->addAttributeToFilter('status', $this->getRequest()->getParam('orderstatus'));
                }
            }
            $connection = $this->resourceConnection->getConnection();
            $columnToAdd = array('increment_id'=>"Order Id",'updated_at'=>"Last Updated At",'created_at'=>"Created At",'ordered_items'=>'Ordered Items','qty'=>'Qty','price'=>'Price','shippment_tracking_details'=>"Shipment Tracking Details",'shipping_charges'=>"Shipping Charges",'shipping_address'=>"Shipping Address",'payment_method'=>'Payment Method','coupon'=>'Coupon','order_total'=>'Order Total');
            $columnToAddValues = array_values($columnToAdd);
            $columnHeader = [];
            foreach($columnToAddValues as $colHeader)
            {
                $columnHeader[] = $colHeader;
            }
            $name = date('m_d_Y_H_i_s');
            $filepath = 'export/custom' . $name . '.csv';
            $this->directory->create('export');
            /* Open file */
            $stream = $this->directory->openFile($filepath, 'a+');
            $stream->lock();

            /* Write Header */
            if ($addedColumHeader == FALSE && !empty($columnHeader))
            {
                $stream->writeCsv($columnHeader);
                $addedColumHeader = TRUE;
            }



            $columValues = [];
            $iCount = 0;
            foreach($ordersCol as $resKey=>$resValue)
            {
                $updatedAt = '';
                $createdAt = '';
                $orderLoad = $this->orderModel->create()->load($resValue['entity_id']);
                $createdAt = date("Y-m-d H:i:s", strtotime('+5 hours',strtotime($orderLoad->getCreatedAt())));
                $createdAt = date("Y-m-d H:i:s", strtotime('+30 minutes',strtotime($createdAt)));
                $orderComments = $orderLoad->getAllStatusHistory();
                foreach ($orderComments as $comment) { // we are getting latest added comment first in the array index
                    if (strtolower($comment->getData('status')) == strtolower($this->getRequest()->getParam('orderstatus')))
                    {
                        $updatedAt = $comment->getData('created_at');
                        $updatedAt = date("Y-m-d H:i:s", strtotime('+5 hours',strtotime($updatedAt)));
                        $updatedAt = date("Y-m-d H:i:s", strtotime('+30 minutes',strtotime($updatedAt)));
                        break;
                    }
                }
                if ($orderLoad)
                {
                    foreach($orderLoad->getAllItems() as $eachOrderItem)
                    {
                        $columValues = [];
                        $shippingAddressData = [];
                        if ($orderLoad->getShippingAddress() && !empty($orderLoad->getShippingAddress()->getData()))
                        {
                            $shippingAddressData = $orderLoad->getShippingAddress()->getData();                    
                        }

                        $tracksCollection    = $orderLoad->getTracksCollection();
                        $payment = $orderLoad->getPayment();
                        $method = $payment->getMethodInstance();
                        $methodTitle = $method->getTitle();
                        $trackingAllData = $tracksCollection->getData();
                        $trackingAllStr = '';
                        foreach($trackingAllData as $trackData)
                        {
                            $trackingAllStr = $trackingAllStr."Carrier:".$trackData['carrier_code'].", Title:".$trackData['title'].", Tracking Number:".$trackData['track_number'].", Created At:".$createdAt.", Last Updated At:".$updatedAt;
                        }
                        $orderitemsnames = [];
                        foreach($orderLoad->getAllItems() as $eachItem)
                        {
                            $orderitemsnames[] = $eachItem->getName();
                        }
                        $columValues[]       = $orderLoad->getIncrementId();
                        $columValues[]       = $updatedAt;
                        $columValues[]       = $createdAt;
                        $columValues[]       = $eachOrderItem->getName();
                        $columValues[]       = $eachOrderItem->getQtyOrdered();
                        $columValues[]       = $eachOrderItem->getPrice();
                        $columValues[]       = $trackingAllStr;
                        $columValues[]       = "Shipping Cost:".$orderLoad->getShippingAmount().", Shipping Method:".$orderLoad->getShippingMethod();
                        if (isset($shippingAddressData['firstname']))
                        {
                            $columValues[]   = $shippingAddressData['firstname'].", ".$shippingAddressData['lastname'].", ".$shippingAddressData['street'].", ".$shippingAddressData['city'].", ".$shippingAddressData['region'].", ".$shippingAddressData['postcode'].", INDIA, T:".$shippingAddressData['telephone'];                    
                        }
                        else
                        {
                            $columValues[]   = '';
                        }

                        $columValues[]       = $methodTitle;
                        $columValues[]       = $orderLoad->getCouponCode();
                        $columValues[]       = $orderLoad->getGrandTotal();
                        $stream->writeCsv($columValues);
                    }
                }
            }
        }
        
 
        $csvfilename = 'ElapseTAT-'.implode('-',$allTimeRangeArr).'.csv';
        $content = [];
        $content['type'] = 'filename'; // must keep filename
        $content['value'] = $filepath;
        $content['rm'] = '1'; //remove csv from var folder
        return $this->_fileFactory->create($csvfilename, $content, DirectoryList::VAR_DIR);
    }
    
    function clean($string) {
        //$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
     }
}
?>
  