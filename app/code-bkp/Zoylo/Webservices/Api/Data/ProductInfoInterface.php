<?php
namespace Zoylo\Webservices\Api\Data;
/**
 * @api
 *
 */
interface ProductInfoInterface
{

    /**
     * Get Code
     *
     * @return int
     */
    public function getCode();

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess();

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success);

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage();

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * Get Product Data
     *
     * @return \Zoylo\Webservices\Api\Data\ProductInfoInterface[] $productData
     */
    public function getProductData();

     /**
     * Set Product Data
     *
     * @param \Zoylo\Webservices\Api\Data\ProductInfoInterface[] $productData
     * @return $this
     */
    public function setProductData($productData);
}
