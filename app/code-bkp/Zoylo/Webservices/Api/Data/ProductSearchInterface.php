<?php
namespace Zoylo\Webservices\Api\Data;
/**
 * @api
 *
 */
interface ProductSearchInterface
{
    
    /**
     * Get Code
     *
     * @return int
     */
    public function getCode();

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess();

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success);

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage();

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * Get Search Result Data
     *
     * @return \Zoylo\Webservices\Api\Data\ProductSearchInterface[]
     */
    public function getSearchResultData();

   /**
     * Set Search Result Data
     *
     * @param \Zoylo\Webservices\Api\Data\ProductSearchInterface[] $resultList
     * @return $this
     */
    public function setSearchResultData(array $resultList = null);
}