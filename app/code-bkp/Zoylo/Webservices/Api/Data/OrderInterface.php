<?php
namespace Zoylo\Webservices\Api\Data;
/**
 * @api
 *
 */
interface OrderInterface
{
    
    /**
     * Get Code
     *
     * @return int
     */
    public function getCode();

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess();

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success);

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage();

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * Get Order Id
     *
     * @return int
     */
    public function getOrderId();

    /**
     * Set Order Id
     *
     * @param int $orderId
     * @return $this
     */
    public function setOrderId($orderId);

    /**
     * Get Increment Id
     *
     * @return string
     */
    public function getIncrementId();

    /**
     * Set Increment Id
     *
     * @param string $incrementId
     * @return $this
     */
    public function setIncrementId($incrementId);
    
    /**
     * Get Grand Total
     *
     * @return float|null
     */
    public function getGrandTotal();

    /**
     * Set Grand Total
     *
     * @param float $grandTotal
     * @return $this
     */
    public function setGrandTotal($grandTotal);

    /**
     * Get PaymentMethod
     *
     * @return string
     */
    public function getPaymentMethod();

    /**
     * Set PaymentMethod
     *
     * @param string $payment_method
     * @return $this
     */
    public function setPaymentMethod($payment_method);

    /**
     * Get PaymentStatus
     *
     * @return string
     */
    public function getPaymentStatus();

    /**
     * Set PaymentStatus
     *
     * @param string $paymentStatus
     * @return $this
     */
    public function setPaymentStatus($paymentStatus);

    /**
     * Get Prescription_values
     *
     * @return string
     */
    public function getPrescriptionValues();

    /**
     * Set Prescription_values
     *
     * @param string $prescription_values
     * @return $this
     */
    public function setPrescriptionValues($prescription_values);

    /**
     * Get OrderInfo
     *
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function getOrderInfo();

    /**
     * Set OrderInfo
     *
     * @param \Magento\Sales\Api\Data\OrderInterface
     * @return $this
     */
    public function setOrderInfo(array $order = null);
}