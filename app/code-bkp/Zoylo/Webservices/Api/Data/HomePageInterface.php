<?php
namespace Zoylo\Webservices\Api\Data;
/**
 * @api
 *
 */
interface HomePageInterface
{
    
    /**
     * Get Code
     *
     * @return int
     */
    public function getCode();

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess();

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success);

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage();

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * @return \Zoylo\Webservices\Api\Data\HomePageInterface[]
     */
    public function getFeaturedCategories();

    /**
     * @param \Zoylo\Webservices\Api\Data\HomePageInterface[] $categoriesList
     * @return $this
     */
    public function setFeaturedCategories(array $categoriesList = null);

    /**
     * @return \Zoylo\Webservices\Api\Data\HomePageInterface[]
     */
    public function getBrandsData();

    /**
     * @param \Zoylo\Webservices\Api\Data\HomePageInterface[] $brandsData
     * @return $this
     */
    public function setBrandsData(array $brandsData = null);

    /**
     * @return \Zoylo\Webservices\Api\Data\HomePageInterface[]
     */
    public function getManufacturerData();

    /**
     * @param \Zoylo\Webservices\Api\Data\HomePageInterface[] $manufacturerData
     * @return $this
     */
    public function setManufacturerData(array $manufacturerData = null);

    /**
     * @return \Zoylo\Webservices\Api\Data\HomePageInterface[]
     */
    public function getSymtomsData();

    /**
     * @param \Zoylo\Webservices\Api\Data\HomePageInterface[] $symtomsData
     * @return $this
     */
    public function setSymtomsData(array $symtomsData = null);

    /**
     * @return \Zoylo\Webservices\Api\Data\HomePageInterface[]
     */
    public function getPopularProducts();

    /**
     * @param \Zoylo\Webservices\Api\Data\HomePageInterface[] $productsList
     * @return $this
     */
    public function setPopularProducts(array $productsList = null);

    /**
     * @return \Zoylo\Webservices\Api\Data\HomePageInterface[]
     */
    public function getBannersData();

    /**
     * @param \Zoylo\Webservices\Api\Data\HomePageInterface[] $bannersList
     * @return $this
     */
    public function setBannersData(array $bannersList = null);
}
