<?php
namespace Zoylo\Webservices\Api\Data;
/**
 * @api
 *
 */
interface OrdersListInterface
{
    
    /**
     * Get Code
     *
     * @return int
     */
    public function getCode();

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess();

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success);

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage();

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

	
	/**
     * get Orders count
     *
     * @return int
     */
    public function getOrdersCount();

    /**
     * set Orders count
     *
     * @param int $count
     * @return $this
     */
    public function setOrdersCount($count);
	
	
    /**
     * Get OrderInfo
     *
     * @return \Zoylo\Webservices\Api\Data\OrdersListInterface[]
     */
    public function getOrdersData();

    /**
     * Set OrderInfo
     *
     * @param \Zoylo\Webservices\Api\Data\OrdersListInterface[] $resultList
     * @return $this
     */
    public function setOrdersData(array $resultList = null);

    /**
     * get Orders total pages
     *
     * @return int
     */
    public function getOrdersTotalPages();

    /**
     * set Orders total pages
     *
     * @param int $orderTotalPages
     * @return $this
     */
    public function setOrdersTotalPages($orderTotalPages);
    
}