<?php
namespace Zoylo\Webservices\Api\Data;
/**
 * @api
 *
 */
interface CountriesInfoInterface
{
    
    /**
     * Get Code
     *
     * @return int
     */
    public function getCode();

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess();

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success);

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage();

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * @return \Zoylo\Webservices\Api\Data\CountriesInfoInterface[]
     */
    public function getCountriesData();

    /**
     * @param \Zoylo\Webservices\Api\Data\CountriesInfoInterface[] $countriesList
     * @return $this
     */
    public function setCountriesData(array $countriesList = null);
}