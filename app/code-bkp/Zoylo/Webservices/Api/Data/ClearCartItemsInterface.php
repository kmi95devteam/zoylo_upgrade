<?php
namespace Zoylo\Webservices\Api\Data;
/**
 * @api
 *
 */
interface ClearCartItemsInterface
{
    
    /**
     * Get Code
     *
     * @return int
     */
    public function getCode();

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess();

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success);

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage();

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * Get Customer Cart ID
     *
     * @return int
     */
    public function getCartId();

    /**
     * Set Customer Cart ID
     *
     * @param int $cartId
     * @return $this
     */
    public function setCartId($cartId);
}