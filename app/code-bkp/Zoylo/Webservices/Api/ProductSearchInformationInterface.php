<?php
namespace Zoylo\Webservices\Api;
 
/**
 * Interface for Searching the products.
 */
interface ProductSearchInformationInterface {
    /**
     * Gets the category_id.
     *
     * @api
     * @return string
     */
    public function getCategoryId();
 
    /**
     * Sets the Category Id.
     *
     * @api
     * @param int $category_id
     */
    public function setCategoryId($category_id);
 
}