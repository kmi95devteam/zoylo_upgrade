<?php

namespace Zoylo\Webservices\Model\Data;

class CustomerCreate extends \Magento\Framework\Model\AbstractModel implements
    \Zoylo\Webservices\Api\Data\CustomerCreateInterface
{
    const KEY_CODE = 'code';
    const KEY_Success = 'success';
    const KEY_Message       = 'message';
    const KEY_CustomerData = 'customer_data';
    const KEY_Token = 'token';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Get Code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->_getData(self::KEY_CODE);
    }

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code)
    {
        return $this->setData(self::KEY_CODE, $code);
    }

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess()
    {
        return $this->_getData(self::KEY_Success);
    }

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success)
    {
        return $this->setData(self::KEY_Success, $success);
    }

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->_getData(self::KEY_Message);
    }

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        return $this->setData(self::KEY_Message, $message);
    }

    /**
     * Get Customer Data
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    public function getCustomerData()
    {
        return $this->_getData(self::KEY_CustomerData);
    }

    /**
     * Set Customer Data
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $data
     * @return $this
     */
    public function setCustomerData(\Magento\Customer\Api\Data\CustomerInterface $data)
    {
        return $this->setData(self::KEY_CustomerData, $data);
    }

    /**
     * Get Token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->_getData(self::KEY_Token);
    }

    /**
     * Set Token
     *
     * @param string $token
     * @return $this
     */
    public function setToken($token)
    {
        return $this->setData(self::KEY_Token, $token);
    }
}