<?php

namespace Zoylo\Webservices\Model\Data;

class ClearCartItems extends \Magento\Framework\Model\AbstractModel implements
    \Zoylo\Webservices\Api\Data\ClearCartItemsInterface
{
    const KEY_CODE      = 'code';
    const KEY_Success   = 'success';
    const KEY_Message   = 'message';
    const KEY_CartId    = 'cart_id';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Get Code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->_getData(self::KEY_CODE);
    }

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code)
    {
        return $this->setData(self::KEY_CODE, $code);
    }

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess()
    {
        return $this->_getData(self::KEY_Success);
    }

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success)
    {
        return $this->setData(self::KEY_Success, $success);
    }

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->_getData(self::KEY_Message);
    }

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        return $this->setData(self::KEY_Message, $message);
    }

    /**
     * Get Guest Cart ID
     *
     * @return string
     */
    public function getCartId()
    {
        return $this->_getData(self::KEY_CartId);
    }

    /**
     * Set Guest Cart ID
     *
     * @param string $cartId
     * @return $this
     */
    public function setCartId($cartId)
    {
        return $this->setData(self::KEY_CartId, $cartId);
    }
}