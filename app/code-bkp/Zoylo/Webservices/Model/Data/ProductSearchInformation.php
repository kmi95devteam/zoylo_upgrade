<?php
namespace Zoylo\Webservices\Model\Data;
 
use \Zoylo\Webservices\Api\ProductSearchInformationInterface;
 
/**
 * Model that contains product search information.
 */
class ProductSearchInformation implements ProductSearchInformationInterface {
 
    /**
     * The Category Id for this search entry.
     * @var string
     */
    public $category_id;
 
    /**
     * Gets the category_id.
     *
     * @api
     * @return string
     */
    public function getCategoryId() {
        return $this->category_id;
    }
 
    /**
     * Sets the Category Id.
     *
     * @api
     * @param int $category_id
     */
    public function setCategoryId($category_id) {
        $this->category_id = $category_id;
    }
}