<?php

namespace Zoylo\Webservices\Model\Data;

class ProductInfo extends \Magento\Framework\Model\AbstractModel implements
    \Zoylo\Webservices\Api\Data\ProductInfoInterface
{
    const KEY_CODE = 'code';
    const KEY_Success = 'success';
    const KEY_Message = 'message';
    const KEY_ProductData = 'product_data';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Get Code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->_getData(self::KEY_CODE);
    }

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code)
    {
        return $this->setData(self::KEY_CODE, $code);
    }

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess()
    {
        return $this->_getData(self::KEY_Success);
    }

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success)
    {
        return $this->setData(self::KEY_Success, $success);
    }

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->_getData(self::KEY_Message);
    }

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        return $this->setData(self::KEY_Message, $message);
    }
    
    /**
     * Get Product Data
     *
     * @return \Zoylo\Webservices\Api\Data\ProductInfoInterface[] $productData
     */
    public function getProductData()
    {
        return $this->_getData(self::KEY_ProductData);
    }

    /**
     * Set Product Data
     *
     * @param \Zoylo\Webservices\Api\Data\ProductInfoInterface[] $productData
     * @return $this
     */
    public function setProductData($productData)
    {
        return $this->setData(self::KEY_ProductData, $productData);
    }

}