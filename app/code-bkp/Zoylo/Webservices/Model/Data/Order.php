<?php

namespace Zoylo\Webservices\Model\Data;

class Order extends \Magento\Framework\Model\AbstractModel implements
    \Zoylo\Webservices\Api\Data\OrderInterface
{
    const KEY_CODE          = 'code';
    const KEY_Success       = 'success';
    const KEY_Message       = 'message';
    const KEY_OrderId       = 'order_id';
    const KEY_IncrementId   = 'increment_id';
    const KEY_GrandTotal    = 'grand_total';
    const KEY_PaymentMethod = 'payment_method';
    const KEY_PaymentStatus = 'payment_status';
    const KEY_PrescriptionValues = 'prescription_values';
    const KEY_OrderInfo     = 'order_info';


    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Get Code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->_getData(self::KEY_CODE);
    }

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code)
    {
        return $this->setData(self::KEY_CODE, $code);
    }

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess()
    {
        return $this->_getData(self::KEY_Success);
    }

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success)
    {
        return $this->setData(self::KEY_Success, $success);
    }

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->_getData(self::KEY_Message);
    }

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        return $this->setData(self::KEY_Message, $message);
    }

    /**
     * Get Order Id
     *
     * @return int
     */
    public function getOrderId()
    {
        return $this->_getData(self::KEY_OrderId);
    }

    /**
     * Set Order Id
     *
     * @param int $orderId
     * @return $this
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::KEY_OrderId, $orderId);
    }

    /**
     * Get Increment Id
     *
     * @return string
     */
    public function getIncrementId()
    {
        return $this->_getData(self::KEY_IncrementId);
    }

    /**
     * Set Increment Id
     *
     * @param string $incrementId
     * @return $this
     */
    public function setIncrementId($incrementId)
    {
        return $this->setData(self::KEY_IncrementId, $incrementId);
    }

    /**
     * Get Grand Total
     *
     * @return float|null
     */
    public function getGrandTotal()
    {
        return $this->_getData(self::KEY_GrandTotal);
    }

    /**
     * Set Grand Total
     *
     * @param float $grandTotal
     * @return $this
     */
    public function setGrandTotal($grandTotal)
    {
        return $this->setData(self::KEY_GrandTotal, $grandTotal);
    }

    /**
     * Get PaymentMethod
     *
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->_getData(self::KEY_PaymentMethod);
    }

    /**
     * Set PaymentMethod
     *
     * @param string $payment_method
     * @return $this
     */
    public function setPaymentMethod($payment_method)
    {
        return $this->setData(self::KEY_PaymentMethod, $payment_method);
    }

    /**
     * Get PaymentStatus
     *
     * @return string
     */
    public function getPaymentStatus()
    {
        return $this->_getData(self::KEY_PaymentStatus);
    }

    /**
     * Set PaymentStatus
     *
     * @param string $paymentStatus
     * @return $this
     */
    public function setPaymentStatus($paymentStatus)
    {
        return $this->setData(self::KEY_PaymentStatus, $paymentStatus);
    }

    /**
     * Get Prescription_values
     *
     * @return string
     */
    public function getPrescriptionValues()
    {
        return $this->_getData(self::KEY_PrescriptionValues);
    }

    /**
     * Set Prescription_values
     *
     * @param string $prescription_values
     * @return $this
     */
    public function setPrescriptionValues($prescription_values)
    {
        return $this->setData(self::KEY_PrescriptionValues, $prescription_values);
    }

    /**
     * Get OrderInfo
     *
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function getOrderInfo()
    {
        return $this->_getData(self::KEY_OrderInfo);
    }

    /**
     * Set OrderInfo
     *
     * @param \Magento\Sales\Api\Data\OrderInterface
     * @return $this
     */
    public function setOrderInfo(array $order = null)
    {
        return $this->setData(self::KEY_OrderInfo, $order);
    }
}