<?php

namespace Zoylo\Webservices\Model\Data;

class Login extends \Magento\Framework\Model\AbstractModel implements
    \Zoylo\Webservices\Api\Data\LoginInterFace
{
    const KEY_CODE = 'code';
    const KEY_Success = 'success';
    const KEY_LoginData = 'login_data';
    const KEY_Token = 'token';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Get Code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->_getData(self::KEY_CODE);
    }

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code)
    {
        return $this->setData(self::KEY_CODE, $code);
    }

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess()
    {
        return $this->_getData(self::KEY_Success);
    }

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success)
    {
        return $this->setData(self::KEY_Success, $success);
    }

    /**
     * Get Login Data
     *
     * @return \Zoylo\Webservices\Api\Data\LoginInterFace[]
     */
    public function getLoginData()
    {
        return $this->_getData(self::KEY_LoginData);
    }

    /**
     * Set Login Data
     *
     * @param \Zoylo\Webservices\Api\Data\LoginInterFace[] $data
     * @return $this
     */
    public function setLoginData($data)
    {
        return $this->setData(self::KEY_LoginData, $data);
    }

    /**
     * Get Token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->_getData(self::KEY_Token);
    }

    /**
     * Set Token
     *
     * @param string $token
     * @return $this
     */
    public function setToken($token)
    {
        return $this->setData(self::KEY_Token, $token);
    }
}