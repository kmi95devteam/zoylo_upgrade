<?php

namespace Zoylo\Webservices\Model\Data;

class OrdersList extends \Magento\Framework\Model\AbstractModel implements
    \Zoylo\Webservices\Api\Data\OrdersListInterface
{
    const KEY_CODE          = 'code';
    const KEY_Success       = 'success';
    const KEY_Message       = 'message';
    const KEY_OrdersCount    = 'orders_count';
    const KEY_OrdersData    = 'orders_data';
    const KEY_OrdersTotalPages    = 'orders_total_pages';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Get Code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->_getData(self::KEY_CODE);
    }

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code)
    {
        return $this->setData(self::KEY_CODE, $code);
    }

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess()
    {
        return $this->_getData(self::KEY_Success);
    }

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success)
    {
        return $this->setData(self::KEY_Success, $success);
    }

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->_getData(self::KEY_Message);
    }

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        return $this->setData(self::KEY_Message, $message);
    }
    
	/**
     * get Orders count
     *
     * @return int
     */
    public function getOrdersCount()
	{
        return $this->_getData(self::KEY_OrdersCount);
    }

    /**
     * set Orders count
     *
     * @param int $count
     * @return $this
     */
    public function setOrdersCount($count)
	{
        return $this->setData(self::KEY_OrdersCount, $count);
    }
	
	
    /**
     * Get OrderInfo
     *
     * @return \Zoylo\Webservices\Api\Data\OrdersListInterface[]
     */
    public function getOrdersData()
    {
        return $this->_getData(self::KEY_OrdersData);
    }

    /**
     * Set OrderInfo
     *
     * @param \Zoylo\Webservices\Api\Data\OrdersListInterface[] $resultList
     * @return $this
     */
    public function setOrdersData(array $resultList = null)
    {
        return $this->setData(self::KEY_OrdersData, $resultList);
    }

    /**
     * get Orders total pages
     *
     * @return int
     */
    public function getOrdersTotalPages()
    {
        return $this->_getData(self::KEY_OrdersTotalPages);
    }

    /**
     * set Orders total pages
     *
     * @param int $orderTotalPages
     * @return $this
     */
    public function setOrdersTotalPages($orderTotalPages)
    {
        return $this->setData(self::KEY_OrdersTotalPages, $orderTotalPages);
    }
}