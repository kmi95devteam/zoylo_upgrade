<?php

namespace Zoylo\Webservices\Model;

use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Helper\Image;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\CartTotalRepositoryInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Model\Order\Payment\Transaction;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Encryption\EncryptorInterface as Encryptor;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\State\InputMismatchException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Math\Random;
use Magento\Framework\Intl\DateTimeFactory;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\Exception\MailException;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\AddressFactory;
use Magento\Customer\Model\AccountConfirmation;
use Magento\Customer\Model\AddressRegistry;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\Customer\CredentialsValidator;
use Magento\Customer\Model\Address as CustomerAddressModel;
use Magento\Customer\Model\Customer as CustomerModel;
use Magento\Customer\Model\EmailNotificationInterface;
use Magento\Integration\Model\Oauth\TokenFactory as TokenModelFactory;
use Magento\Store\Model\StoreManagerInterface;
use I95Dev\Customproductattributes\Helper\Data;
use Zoylo\Webservices\Api\Data\HomePageInterfaceFactory;
use Zoylo\Webservices\Api\Data\LoginInterFaceFactory;
use Zoylo\Webservices\Api\Data\ProductInfoInterfaceFactory;
use Zoylo\Webservices\Api\Data\CountriesInfoInterfaceFactory;
use Zoylo\Webservices\Api\Data\CustomerCreateInterfaceFactory;
use Zoylo\Webservices\Api\Data\ProductFiltersInterfaceFactory;
use Zoylo\Webservices\Api\Data\ProductSearchInterfaceFactory;
use Zoylo\Webservices\Api\Data\ZipcodeSearchInterfaceFactory;
use Zoylo\Webservices\Api\Data\MergeCartInterfaceFactory;
use Zoylo\Webservices\Api\Data\GuestCartIdInterfaceFactory;
use Zoylo\Webservices\Api\Data\CustomerCartIdInterfaceFactory;
use Zoylo\Webservices\Model\GuestcartFactory;
use Zoylo\Webservices\Model\Autocomplete\SearchDataProviderFactory;
use Zoylo\Webservices\Api\Data\CouponsListInterfaceFactory;
use Zoylo\Webservices\Api\Data\OrderInterfaceFactory;
use Zoylo\Webservices\Api\Data\UpdateOrderInterfaceFactory;
use Zoylo\Webservices\Api\Data\CancelOrderInterfaceFactory;
use Zoylo\Webservices\Api\Data\ClearCartItemsInterfaceFactory;
use Zoylo\Webservices\Api\Data\OrdersListInterfaceFactory;
use Zoylo\Webservices\Api\Data\ReorderInterfaceFactory;
use Zoylo\Webservices\Api\WebservicesInterface as ApiInterface;
use Ecom\Ecomexpress\Model\PincodeFactory;

class MobileApi implements ApiInterface {

    /**
     * Admin Username
     *
     * @deprecated
     */
    const ADMIN_API_USERNAME = 'mobile_app';

    /**
     * Admin Password
     *
     * @deprecated
     */
    const ADMIN_API_PASSWORD = 'Zoylo123$';

    /**
     * @param \Zoylo\Webservices\Logger\Logger $logger
     */
    protected $logger;

    /**
     * @param \Magento\Framework\App\ObjectManager
     */
    public $objectManager;

    /**
     * @param \Magento\Catalog\Api\CategoryManagementInterface $categoryManagement
     */
    protected $categoryManagement;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $stockState;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var \Magento\Catalog\Model\Category\Tree
     */
    protected $categoryTree;

    /**
     * @var \Magento\Framework\App\ScopeResolverInterface
     */
    private $scopeResolver;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    private $categoriesFactory;

    /**
     * Token Model
     *
     * @var TokenModelFactory
     */
    private $tokenModelFactory;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;

    /**
     * @var \Zoylo\Webservices\Api\Data\HomePageInterfaceFactory
     */
    protected $homePageInterfaceFactory;

    /**
     * @var \Zoylo\Webservices\Api\Data\LoginInterFaceFactory
     */
    protected $loginInterfaceFactory;

    /**
     * @var \Zoylo\Webservices\Api\Data\ProductInfoInterfaceFactory
     */
    protected $productInfoInterfaceFactory;

    /**
     * @var \Zoylo\Webservices\Api\Data\MergeCartInterfaceFactory
     */
    protected $mergeCartInterfaceFactory;

    /**
     * @var \Zoylo\Webservices\Api\Data\CustomerCartIdInterfaceFactory
     */
    protected $customerCartIdInterfaceFactory;

    /**
     * @var \Zoylo\Webservices\Api\Data\OrderInterfaceFactory
     */
    protected $orderInterfaceFactory;

    /**
     * @var \Zoylo\Webservices\Api\Data\OrdersListInterfaceFactory
     */
    protected $ordersListInterfaceFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    protected $resourceProductModel;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productFactory;

    /**
     * @var QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var \Magento\Customer\Model\AddressFactory
     */
    protected $addressFactory;

    /**
     * @var \Magento\Customer\Model\AddressRegistry
     */
    protected $addressRegistry;

    /**
     * @var \Magento\Customer\Model\CustomerRegistry
     */
    protected $customerRegistry;

    /**
     * @var \Zoylo\Webservices\Api\Data\CountriesInfoInterfaceFactory
     */
    protected $countriesInfoInterfaceFactory;

    /**
     * @var \Zoylo\Webservices\Api\Data\GuestCartIdInterfaceFactory
     */
    protected $guestCartIdInterfaceFactory;

    /**
     * @var \Zoylo\Webservices\Model\GuestcartFactory
     */
    protected $guestcartFactory;

    /**
     * @var \Magento\Directory\Api\CountryInformationAcquirerInterface
     */
    protected $countryInformationAcquirer;

    /**
     * @var \Zoylo\Webservices\Api\Data\CouponsListInterfaceFactory
     *
     */
    protected $couponsListInterfaceFactory;

    /**
     * @var \Zoylo\Webservices\Api\Data\UpdateOrderInterfaceFactory
     *
     */
    protected $updateOrderInterfaceFactory;
	
	/**
     * @var \Zoylo\Webservices\Api\Data\CancelOrderInterfaceFactory
     *
     */
    protected $cancelOrderInterfaceFactory;

    /**
     * @var \Zoylo\Webservices\Api\Data\ClearCartItemsInterfaceFactory
     *
     */
    protected $clearCartItemsInterfaceFactory;

    /**
     * @var CredentialsValidator
     */
    private $credentialsValidator;

    /**
     * @var Encryptor
     */
    private $encryptor;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var Random
     */
    private $mathRandom;

    /**
     * @var DateTimeFactory
     */
    private $dateTimeFactory;

    /**
     * Constants for the type of new account email to be sent
     *
     * @deprecated
     */
    const NEW_ACCOUNT_EMAIL_REGISTERED = 'registered';

    /**
     * Welcome email, when password setting is required
     *
     * @deprecated
     */
    const NEW_ACCOUNT_EMAIL_REGISTERED_NO_PASSWORD = 'registered_no_password';

    /**
     * Welcome email, when confirmation is enabled
     *
     * @deprecated
     */
    const NEW_ACCOUNT_EMAIL_CONFIRMATION = 'confirmation';

    /**
     * Confirmation email, when account is confirmed
     *
     * @deprecated
     */
    const NEW_ACCOUNT_EMAIL_CONFIRMED = 'confirmed';

    /**
     * @var EmailNotificationInterface
     */
    private $emailNotification;

    /**
     * @var DateTime
     */
    protected $dateTime;

    /**
     * @var AccountConfirmation
     */
    private $accountConfirmation;

    /**
     * @var \Zoylo\Webservices\Api\Data\CustomerCreateInterfaceFactory
     */
    protected $customerCreateInterfaceFactory;

    /**
     * @var \I95Dev\Customproductattributes\Helper\Data
     */
    protected $customHelperData;

    /**
     * @var \Zoylo\Webservices\Api\Data\ProductFiltersInterfaceFactory
     */
    protected $productFiltersInterfaceFactory;

    /**
     * @var \Zoylo\Webservices\Api\Data\ProductSearchInterfaceFactory
     */
    protected $productSearchInterfaceFactory;

    /**
     * @var \Zoylo\Webservices\Api\Data\ZipcodeSearchInterfaceFactory
     */
    protected $zipcodeSearchInterfaceFactory;

    /**
     * @var \Magento\Search\Model\Query
     */
    private $query;

    /**
     * @var \Ecom\Ecomexpress\Model\PincodeFactory
     */
    private $pincodeFactory;

    /**
     * @var CartManagementInterface
     */
    protected $quoteManagement;

    /**
     * @var CartTotalRepositoryInterface
     */
    protected $cartTotalRepository;

    /**
     * @var QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var SearchDataProviderFactory
     */
    protected $searchManagerFactory;

    /**
     * @var bool
     */
    private static $isLayerCreated = false;

    /**
     * @var \Magento\SalesRule\Model\ResourceModel\Coupon\CollectionFactory
     */
    protected $_salesRuleCoupon;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\Timezone $stdTimezone
     *
     */
    protected $stdTimezone;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /** @var \Magento\Catalog\Helper\Image */
    protected $imageHelper;
    protected $smshelperdata;
    protected $smshelperapi;
    protected $_productAttributeRepository;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Report\Bestsellers\CollectionFactory
     */
    protected $bestsellersCollectionFactory;

    /**
     * Catalog product visibility
     *
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $catalogProductVisibility;

    /**
     * @var \Magento\Catalog\Model\Product\Attribute\Source\Status
     */
    protected $productStatus;

    /**
     * @var \Zoylo\Webservices\Api\Data\ReorderInterfaceFactory
     */
    protected $reorderInterfaceFactory;

    
    protected $prstDateTime;
    
    protected $orderModel;
    
    protected $csquareHelper;

    protected $_stockItemRepository;
    /**
     * @param TokenModelFactory $tokenModelFactory
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param QuoteIdMaskFactory $quoteIdMaskFactory
     * @param \Magento\Customer\Model\AddressFactory $addressFactory
     * @param \Magento\Customer\Model\AddressRegistry $addressRegistry
     * @param \Magento\Customer\Model\CustomerRegistry $customerRegistry
     * @param \Zoylo\Webservices\Api\Data\HomePageInterfaceFactory $homePageInterfaceFactory
     */
    public function __construct(
    AccountManagementInterface $accountManagement, \I95Dev\Symptoms\Model\Symptoms $symptomsCollection, \Ves\Brand\Model\Brand $brandCollection, StockRegistryInterface $stockState, \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository, \Magento\Catalog\Model\Category\Tree $categoryTree, \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoriesFactory, HomePageInterfaceFactory $homePageInterfaceFactory, LoginInterFaceFactory $loginInterfaceFactory, ProductInfoInterfaceFactory $productInfoInterfaceFactory, CountriesInfoInterfaceFactory $countriesInfoInterfaceFactory, CustomerCreateInterfaceFactory $customerCreateInterfaceFactory, ProductFiltersInterfaceFactory $productFiltersInterfaceFactory, ProductSearchInterfaceFactory $productSearchInterfaceFactory, ZipcodeSearchInterfaceFactory $zipcodeSearchInterfaceFactory, MergeCartInterfaceFactory $mergeCartInterfaceFactory, GuestCartIdInterfaceFactory $guestCartIdInterfaceFactory, CustomerCartIdInterfaceFactory $customerCartIdInterfaceFactory, GuestcartFactory $guestcartFactory, CouponsListInterfaceFactory $couponsListInterfaceFactory, ClearCartItemsInterfaceFactory $clearCartItemsInterfaceFactory, OrdersListInterfaceFactory $ordersListInterfaceFactory, ReorderInterfaceFactory $reorderInterfaceFactory, \Zoylo\Webservices\Api\Data\OrderInterfaceFactory $orderInterfaceFactory, UpdateOrderInterfaceFactory $updateOrderInterfaceFactory, CancelOrderInterfaceFactory $cancelOrderInterfaceFactory, \I95Dev\Customproductattributes\Helper\Data $customHelperData, TokenModelFactory $tokenModelFactory, CustomerFactory $customerFactory, \Magento\Catalog\Model\ResourceModel\Product $resourceProductModel, \Magento\Catalog\Model\ProductRepository $productRepository, ProductCollectionFactory $productFactory, QuoteIdMaskFactory $quoteIdMaskFactory, \Magento\Quote\Api\CartRepositoryInterface $quoteRepository, \Magento\Quote\Model\QuoteFactory $quoteFactory, \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository, \Magento\Customer\Model\AddressFactory $addressFactory, \Magento\Customer\Model\AddressRegistry $addressRegistry, \Magento\Customer\Model\CustomerRegistry $customerRegistry, \Magento\Directory\Api\CountryInformationAcquirerInterface $countryInformationAcquirer, Encryptor $encryptor, \Magento\Catalog\Helper\Image $imageHelper, StoreManagerInterface $storeManager, AddressRepositoryInterface $addressRepository, Random $mathRandom, DateTime $dateTime, PincodeFactory $pincodeFactory, CartManagementInterface $quoteManagement, CartTotalRepositoryInterface $cartTotalRepository, SearchDataProviderFactory $searchManagerFactory, \Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory $salesRuleCoupon, \Magento\Framework\Stdlib\DateTime\Timezone $stdTimezone, \Magento\Sales\Api\OrderRepositoryInterface $orderRepository, \Magecomp\Smsfree\Helper\Data $smshelperdata, \Magecomp\Smsfree\Helper\Apicall $smshelperapi, \Magento\Catalog\Model\Product\Attribute\Repository $productAttributeRepository, \Magento\Sales\Model\ResourceModel\Report\Bestsellers\CollectionFactory $bestsellersCollectionFactory, \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility, \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus, AccountConfirmation $accountConfirmation = null, CredentialsValidator $credentialsValidator = null, DateTimeFactory $dateTimeFactory = null, \Zoylo\Webservices\Logger\Logger $logger, \Magento\Framework\Stdlib\DateTime\DateTime $prstDateTime, \Magento\Sales\Model\OrderFactory $orderModel,\Zoylo\Csquare\Helper\Data $csquareHelper, \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository 
    ) {
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->logger = $logger;
        $this->customerAccountManagement = $accountManagement;
        $this->_symptomsCollection = $symptomsCollection;
        $this->_brandCollection = $brandCollection;
        $this->stockState = $stockState;
        $this->categoryRepository = $categoryRepository;
        $this->categoryTree = $categoryTree;
        $this->categoriesFactory = $categoriesFactory;
        $this->tokenModelFactory = $tokenModelFactory;
        $this->customerFactory = $customerFactory;
        $this->homePageInterfaceFactory = $homePageInterfaceFactory;
        $this->loginInterfaceFactory = $loginInterfaceFactory;
        $this->countriesInfoInterfaceFactory = $countriesInfoInterfaceFactory;
        $this->productInfoInterfaceFactory = $productInfoInterfaceFactory;
        $this->resourceProductModel = $resourceProductModel;
        $this->_productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->quoteRepository = $quoteRepository;
        $this->customerRepository = $customerRepository;
        $this->addressFactory = $addressFactory;
        $this->addressRegistry = $addressRegistry;
        $this->customerRegistry = $customerRegistry;
        $this->countryInformationAcquirer = $countryInformationAcquirer;
        $this->credentialsValidator = $credentialsValidator ?: ObjectManager::getInstance()->get(CredentialsValidator::class);
        $this->dateTimeFactory = $dateTimeFactory ?: ObjectManager::getInstance()->get(DateTimeFactory::class);
        $this->accountConfirmation = $accountConfirmation ?: ObjectManager::getInstance()
                        ->get(AccountConfirmation::class);
        $this->encryptor = $encryptor;
        $this->storeManager = $storeManager;
        $this->addressRepository = $addressRepository;
        $this->mathRandom = $mathRandom;
        $this->dateTime = $dateTime;
        $this->customerCreateInterfaceFactory = $customerCreateInterfaceFactory;
        $this->customHelperData = $customHelperData;
        $this->productFiltersInterfaceFactory = $productFiltersInterfaceFactory;
        $this->productSearchInterfaceFactory = $productSearchInterfaceFactory;
        $this->zipcodeSearchInterfaceFactory = $zipcodeSearchInterfaceFactory;
        $this->mergeCartInterfaceFactory = $mergeCartInterfaceFactory;
        $this->guestCartIdInterfaceFactory = $guestCartIdInterfaceFactory;
        $this->customerCartIdInterfaceFactory = $customerCartIdInterfaceFactory;
        $this->couponsListInterfaceFactory = $couponsListInterfaceFactory;
        $this->orderInterfaceFactory = $orderInterfaceFactory;
        $this->updateOrderInterfaceFactory = $updateOrderInterfaceFactory;
        $this->cancelOrderInterfaceFactory = $cancelOrderInterfaceFactory;
        $this->clearCartItemsInterfaceFactory = $clearCartItemsInterfaceFactory;
        $this->ordersListInterfaceFactory = $ordersListInterfaceFactory;
        $this->guestcartFactory = $guestcartFactory;
        $this->pincodeFactory = $pincodeFactory;
        $this->quoteManagement = $quoteManagement;
        $this->quoteFactory = $quoteFactory;
        $this->cartTotalRepository = $cartTotalRepository;
        $this->searchManagerFactory = $searchManagerFactory;
        $this->_salesRuleCoupon = $salesRuleCoupon;
        $this->stdTimezone = $stdTimezone;
        $this->orderRepository = $orderRepository;
        $this->imageHelper = $imageHelper;
        $this->smshelperdata = $smshelperdata;
        $this->smshelperapi = $smshelperapi;
        $this->_productAttributeRepository = $productAttributeRepository;
        $this->bestsellersCollectionFactory = $bestsellersCollectionFactory;
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->productStatus = $productStatus;
        $this->reorderInterfaceFactory = $reorderInterfaceFactory;
        $this->prstDateTime = $prstDateTime;
        $this->orderModel = $orderModel;
        $this->csquareHelper = $csquareHelper;
        $this->_stockItemRepository = $stockItemRepository;
    }

    /**
     * Customer Login.
     *
     * @api
     * @param $mobile
     * @return boolean
     */
    public function login($username) {
        $login = array();

        if (!empty($username)) {
            $this->logger->info('Login Credentials');
            $this->logger->info('Username: ' . $username);
            try {
                $username = substr($username, -10);
                $customerCollection = $this->customerFactory->create()->getCollection()
                        ->addAttributeToSelect("*")
                        ->addAttributeToFilter("customer_number", array("eq" => $username));
                if (count($customerCollection) > 0) {
                    foreach ($customerCollection as $customer) {
                        
                    }

                    $code = 110;
                    $customerId = $customer->getId();
                    $customer1 = $this->customerRepository->getById($customerId);
                    $message[] = array('id' => $customerId, 'email' => $customer->getEmail(), 'name' => ucwords($customer1->getFirstName() . ' ' . $customer1->getLastName()));
                    $token = $this->tokenModelFactory->create()->createCustomerToken($customer->getId())->getToken();
                    $data = ['success' => 'true', 'login_data' => $message, 'code' => $code, 'token' => $token];
                    $result = $this->loginInterfaceFactory->create();
                    $result->setData($data);
                    return $result;
                } else {
                    $message[] = array('error' => __('Customer not found.'));
                    $code = 110;
                }
            } catch (EmailNotConfirmedException $e) {
                $message[] = array('error' => $e->getMessage());
                $code = 110;
            } catch (AuthenticationException $e) {
                $message[] = array('error' => $e->getMessage());
                $code = 110;
            } catch (\Exception $e) {
                $message[] = array('error' => $e->getMessage());
                $code = 110;
            }
        } else {
            $message[] = array('error' => $e->getMessage());
            $code = 110;
        }
        $token = '';
        $data = ['success' => 'false', 'login_data' => $message, 'code' => $code, 'token' => $token];
        $result = $this->loginInterfaceFactory->create();
        $result->setData($data);
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getTree($rootCategoryId = null, $depth = null) {
        $baseUrl = $this->storeManager->getStore()->getBaseUrl();
        $category = null;
        if ($rootCategoryId !== null) {
            /** @var \Magento\Catalog\Model\Category $category */
            $category = $this->categoryRepository->get($rootCategoryId);
        } elseif ($this->isAdminStore()) {
            $category = $this->getTopLevelCategory();
        }
        $result = $this->categoryTree->getTree($this->categoryTree->getRootNode($category), $depth);

        //Get Symptoms
        $symtomscollections = $this->getSymptomsCollection();
        $symtomsList = array();
        $symtomList = array();
        if (count($symtomscollections) > 0) {
            foreach ($symtomscollections as $symtoms) {
                $symtomname = $symtoms['name'];
                $symtomList['name'] = $symtomname;
                $symtomList['url'] = $baseUrl . 'catalogsearch/result/?q=' . $symtomname;
                $symtomsList[] = $symtomList;
            }
            $result->setSymtomsData($symtomsList);
        }

        //Get Shop By Brands List
        $brandcollections = $this->getBrandCollection();
        $brandsList = array();
        $brandList = array();
        if (count($brandcollections) > 0) {
            foreach ($brandcollections as $brands) {
                if($brands['image'] != ''){
                    $brandList['name'] = $brands['name'];
                    $brandList['url'] = '/brand/' . $brands['url_key'] . '.html';
                    $brandsList[] = $brandList;
                }
            }
            $result->setBrandsData($brandsList);
        }

        return $result;
    }

    /**
     * Get top level hidden root category
     *
     * @return \Magento\Catalog\Model\Category
     */
    private function getTopLevelCategory() {
        $categoriesCollection = $this->categoriesFactory->create();
        return $categoriesCollection->addFilter('level', ['eq' => 0])->getFirstItem();
    }

    /**
     * Check is request use default scope
     *
     * @return bool
     */
    private function isAdminStore() {
        return $this->getScopeResolver()->getScope()->getCode() == \Magento\Store\Model\Store::ADMIN_CODE;
    }

    /**
     * Get store manager for operations with admin code
     *
     * @return \Magento\Framework\App\ScopeResolverInterface
     */
    private function getScopeResolver() {
        if ($this->scopeResolver == null) {
            $this->scopeResolver = \Magento\Framework\App\ObjectManager::getInstance()
                    ->get(\Magento\Framework\App\ScopeResolverInterface::class);
        }

        return $this->scopeResolver;
    }

    /**
     * {@inheritdoc}
     */
    public function homepage() {
        try {
            $mediaUrl = $this->storeManager
                    ->getStore()
                    ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

            $baseUrl = $this->storeManager->getStore()->getBaseUrl();

            // get current store’s categories 
            $categoryHelper = $this->objectManager->get('\Magento\Catalog\Helper\Category');
            $categories = $categoryHelper->getStoreCategories();
            $categoriesList = array();
            $categorieList = array();

            if (count($categories) > 0) {
                foreach ($categories as $category) {
                    //print_r($category->getData());die;
                    if ($category->getIsActive() == 1) {
                        $categorieList['id'] = $category->getId();
                        $categorieList['parent_id'] = $category->getParentId();
                        $categorieList['name'] = $category->getName();
                        $categorieList['is_active'] = $category->getIsActive();
                        $categorieList['position'] = $category->getPosition();
                        $categorieList['level'] = $category->getLevel();
                        $categorieList['product_count'] = $category->getProductCount();
                        $categorieList['url'] = $category->getRequestPath();
                        $categorieList['children_data'] = $category->getChildren();
                    }
                    $categoriesList[] = $categorieList;
                }
            }

            //Get Symptoms
            $symtomscollections = $this->getSymptomsCollection();
            $symtomsList = array();
            $symtomList = array();
            if (count($symtomscollections) > 0) {
                foreach ($symtomscollections as $symtoms) {
                    $symtomname = $symtoms['name'];
                    $symtomList['name'] = $symtomname;
                    $imageurl = $mediaUrl . 'symptom/';
                    $symtomList['image'] = $imageurl . $symtomname . '.png';
                    $symtomList['url'] = $baseUrl . 'catalogsearch/result/?q=' . $symtomname;
                    $symtomsList[] = $symtomList;
                }
            }

            //Get Shop By Brands List
            $brandcollections = $this->getBrandCollection();
            $otcBrandCollection = $this->getOtcBrandCollection();
            $brandsList = array();
            $brandList = array();
            if (count($brandcollections) > 0) {
                foreach ($brandcollections as $brands) {
                    if($brands['image'] != ''){
                        $brandName = $brands['name'];
                        $option_id = '';
                        if (isset($otcBrandCollection[$brandName]))
                            $option_id = $otcBrandCollection[$brandName];
                        else
                            continue;

                        $brandList['image'] = $mediaUrl . $brands['image'];
                        $brandList['name'] = $brandName;
                        $brandList['url'] = '/brand/' . $brands['url_key'] . '.html';
                        $brandList['option_id'] = $option_id;
                        $brandList['filter_condition'] = 'eq';
                        $brandsList[] = $brandList;
                    }
                }
            }

            //Get Shop By Brands List
            /* $manufacturerCollections = $this->getManufacturerCollection();
              $manufacturerList = array();
              $manufacturersList = array();
              if( count($manufacturerCollections) >0){
              foreach ($manufacturerCollections as $key => $manufacturer) {
              $manufacturerList['attribute_code'] = 'manufacturer';
              $manufacturerList['option_id']      = $manufacturer->getOptionId();
              $manufacturerList['name']           = $manufacturer->getValue();
              $manufacturerList['attribute_id']   = $manufacturer->getAttributeId();
              $manufacturersList[] = $manufacturerList;
              }
              } */

            // Get Popular Products List
            $popularProductsCollection = $this->getPopularProductsCollection();
            $productsList = array();
            $productList = array();
            //$imageHelper = $this->objectManager->get(\Magento\Catalog\Helper\Image::class);
            if (count($popularProductsCollection) > 0) {
                foreach ($popularProductsCollection as $products) {
                    //print_r($products->getData());die;
                    $product = $this->_productRepository->get($products->getSku());
                    $productList['id'] = $product->getId();
                    $productList['sku'] = $product->getSku();
                    $productName        = '';
                    $name               = $product->getResource()->getAttribute('name')->getFrontend()->getValue($product);
                    $displayName        = $product->getResource()->getAttribute('sku_display_name')->getFrontend()->getValue($product);
                    if ($displayName == '') {
                        $productName = $name;
                    } else {
                        $productName = $displayName;
                    }
                    $productList['name'] = $productName;
                    $productList['status'] = $product->getStatus();
                    $stockItem = $this->stockState->getStockItem(
                            $product->getId(), $product->getStore()->getWebsiteId()
                    );
                    $productList['qty'] = $stockItem->getQty();
					
					$sale_status_msg = '';
					$sale_status = false;
					$saleProduct = $product->getSellableFlag();
					if (isset($saleProduct) && $saleProduct == 1) {
						if ($product->isSaleable()) {
							$sale_status = true;
							$sale_status_msg = __('Add To Cart');
						} else {
							$sale_status_msg = __('Out of stock');
						}
					} else {
						if (isset($saleProduct) && $saleProduct == 0) {
							$sale_status_msg = __('Not For Sale');
						} elseif (isset($saleProduct) && $saleProduct == 2) {
							$sale_status_msg = __('Discontinued');
						} else {
							if ($product->isSaleable()) {
								$sale_status = true;
								$sale_status_msg = __('Add To Cart');
							}
						}
					}
                    $productList['sale_status'] = $sale_status;
                    $productList['sale_status_msg'] = $sale_status_msg;

                    $productList['product_type'] = $product->getAttributeText('zoylo_sku_group');
                    $drug_type = $product->getResource()->getAttribute('drug_type')->getFrontend()->getValue($product);
                    $drugTypeUrl = '';
                    if (isset($drug_type) && !is_array($drug_type) && $drug_type != '' && $drug_type != 'no_selection' ) {
                        $drugTypeUrl = $mediaUrl . 'packtype/' .ucwords(strtolower($drug_type)) . '.png';
                    } elseif (strtolower($productList['product_type']) == "otc") {
                        $categoryIds = $product->getCategoryIds();
                        $_categoryImgUrl = '';
                        foreach ($categoryIds as $catId) {
                            if ($catId == 1 || $catId == 2 || $catId == 3 || $catId == 6 || $catId == 7)
                                continue;

                            $category = $this->categoryRepository->get($catId);
                            if ($category->getLevel() == 3) {
                                $_categoryImgUrl = $category->getImageUrl();
                            }
                        }
                        if ($_categoryImgUrl) {
                            $drugTypeUrl = $_categoryImgUrl;
                        } else {
                            $drugTypeUrl = $mediaUrl . 'packtype/OTC.png';
                        }
                    }


                    if ($product->getPrescriptionRequired() == "required")
                        $productList['prescription_status'] = true;
                    else
                        $productList['prescription_status'] = false;

                    $image = '';
                    if ($product->getImage() && $product->getImage() != "no_selection") {
                        $image = $mediaUrl . 'catalog/product' . $product->getImage();
                    }
                    if ($image == '' && $drugTypeUrl) {
                        $image = $drugTypeUrl;
                    }
                    if ($image == '') {
                        $image = $baseUrl . 'pub/static/frontend/Smartwave/porto/en_US/Magento_Catalog/images/product/placeholder/image.jpg';
                    }
                    $productList['image'] = $image;

                    /* if($product->getThumbnail())
                      $productList['image'] = $mediaUrl. 'catalog/product' . $product->getThumbnail();
                      elseif($productFormatUrl)
                      $productList['image'] = $productFormatUrl;
                      else
                      $productList['image'] = $baseUrl.'pub/static/frontend/Smartwave/porto/en_US/Magento_Catalog/images/product/placeholder/small_image.jpg'; */

                    $productList['price'] = $product->getPrice();
                    $productList['url'] = $product->getProductUrl();
                    $productList['pack'] = $product->getPackSize();
                    $productList['pack_type'] = $product->getPackType();

                    //Special Price
                    $orgprice = $product->getPrice();
                    $specialprice = $product->getSpecialPrice();
                    $finalPrice = $product->getFinalPrice();
            if($finalPrice != $orgprice){
                $specialprice = $finalPrice;
            }
                    $specialfromdate = $product->getSpecialFromDate();
                    $specialtodate = $product->getSpecialToDate();
                    $today = time();
                    $save_percent = 0;
                    if ($specialprice < $orgprice && ($specialprice)) {
                            $save_percent = 100 - round(($specialprice / $orgprice) * 100);
                        
                    }
					if($save_percent > 0){
						$productList['special_price'] = $specialprice;
					}else{
						$productList['special_price'] = null;
					}
                    $productList['save_percent'] = $save_percent;

                    $productsList[] = $productList;
                }
            }

            //Banner Block
            for ($i = 0; $i < 2; $i++) {
                if ($i == 0) {
                    $bannerList['image'] = $mediaUrl . 'wysiwyg/Banners/Group 3.png';
                    //$bannerList['parent_title'] = "India's First";
                    //$bannerList['title']        = 'DIGITAL HEALTHCARE ECOSYSTEM';
                    //$bannerList['sub_title']    = 'Touching Million + Lives';
                } elseif ($i == 1) {
                    $bannerList['image'] = $mediaUrl . 'wysiwyg/Banners/Group 4.png';
                    //$bannerList['parent_title'] = "Order medicines";
                    //$bannerList['title']        = "GET DOORSTEP DELIVERY";
                    //$bannerList['sub_title']    = "Serving 2000+ Locations in India";
                }
                $bannersList[] = $bannerList;
            }

            $this->logger->info('Testing');
            $data = [
                'code' => 100,
                'success' => 'true',
                'message' => __('Zoylo Home Page'),
                'featured_categories' => $categoriesList,
                'symtoms_data' => $symtomsList,
                'brands_data' => $brandsList,
                'manufacturer_data' => '',
                'popular_products' => $productsList,
                'banners_data' => $bannersList
            ];
            $result = $this->homePageInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 100,
                'success' => 'false',
                'message' => $message,
                'featured_categories' => '',
                'symtoms_data' => '',
                'brands_data' => '',
                'manufacturer_data' => '',
                'popular_products' => '',
                'banners_data' => ''
            ];
            $result = $this->homePageInterfaceFactory->create();
            $result->setData($data);
        }
        return $result;
    }

    /**
     * Return Guest Cart ID
     * @api
     * @param string $deviceId
     * @return \Zoylo\Webservices\Api\Data\GuestCartIdInterface containing Guest Cart ID objects
     */
    public function getGuestCartId($deviceId) {
        try {
            $this->logger->info('Guest deviceId: ' . $deviceId);
            $totalItems = '';
            $totalQuantity = '';
            $quote = '';
            $totals = '';
            $guestCartIdModel = $this->guestcartFactory->create();
            $deviceIdCollection = $guestCartIdModel->getCollection()
                    ->addFieldToFilter('device_id', array('eq' => $deviceId));
            if (sizeof($deviceIdCollection) > 0) {
                foreach ($deviceIdCollection as $deviceData) {
                    $cartId = $deviceData->getCartId();
                }
                $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
                $quote = $this->quoteRepository->get($quoteIdMask->getQuoteId());
                $totalItems = $quote->getItemsCount();
                $totalQuantity = $quote->getItemsQty();
                $catId = $quote->getId();
                $totals = $this->cartTotalRepository->get($catId);
            } else {

                /** @var $quoteIdMask \Magento\Quote\Model\QuoteIdMask */
                $quoteIdMask = $this->quoteIdMaskFactory->create();
                $catId = $this->quoteManagement->createEmptyCart();
                $quoteIdMask->setQuoteId($catId)->save();
                $cartId = $quoteIdMask->getMaskedId();

                //Saving the Cart ID to Perticulor Device Id
                $guestCartIdModel->setDeviceId($deviceId);
                $guestCartIdModel->setCartId($cartId);
                $guestCartIdModel->save();
            }

            $data = [
                'code' => 240,
                'success' => 'true',
                'message' => __('Guest Cart Id'),
                'cart_id' => $cartId,
                'items_count' => $totalItems,
                'items_qty' => $totalQuantity,
                'items' => $quote,
                'totals' => $totals
            ];
            $result = $this->guestCartIdInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 240,
                'success' => 'false',
                'message' => $message,
                'cart_id' => '',
                'items_count' => '',
                'items_qty' => '',
                'items' => '',
                'totals' => ''
            ];
            $result = $this->guestCartIdInterfaceFactory->create();
            $result->setData($data);
        }
        return $result;
    }

    /**
     * Return Login Customer Cart ID
     * @api
     * @param string $customerId
     * @return \Zoylo\Webservices\Api\Data\CustomerCartIdInterface containing Login Customer Cart ID objects
     */
    public function getCustomerCartId($customerId) {
        $storeId = $this->storeManager->getStore()->getStoreId();
        $quote = $this->createCustomerCart($customerId, $storeId);

        try {
            $itemsCount = $quote->getItemsCount();
            //Removing prescription item from cart
            $all_quote_items = $quote->getAllItems();
            foreach ($all_quote_items as $item) {
                $sku = $item->getSku();

                if ($sku == "Free") {
                    //$quote->removeItem($item->getId())->save();
                    $itemId = $item->getItemId(); //item id of particular item
                    $quoteItem = $this->getItemModel()->load($itemId); //load particular item which you want to delete by his item id
                    $quoteItem->delete(); //deletes the item
                    $this->quoteRepository->save($quote);
                    $quote = $this->createCustomerCart($customerId, $storeId);
                }
            }

            $this->quoteRepository->save($quote);
            $cartId = $quote->getId();
            $totals = $this->cartTotalRepository->get($cartId);
            $totalItems = $quote->getItemsCount();

            $totalQuantity = $quote->getItemsQty();
            $data = [
                'code' => 250,
                'success' => 'true',
                'message' => __('Customer Cart Id'),
                'cart_id' => $cartId,
                'items_count' => $totalItems,
                'items_qty' => $totalQuantity,
                'items' => $quote,
                'totals' => $totals
            ];
            $result = $this->customerCartIdInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 250,
                'success' => 'false',
                'message' => $message,
                'cart_id' => '',
                'items_count' => '',
                'items_qty' => '',
                'items' => '',
                'totals' => ''
            ];
            $result = $this->customerCartIdInterfaceFactory->create();
            $result->setData($data);
            //throw new CouldNotSaveException(__('Cannot create quote'));
        } catch (CouldNotSaveException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 250,
                'success' => 'false',
                'message' => $message,
                'cart_id' => '',
                'items_count' => '',
                'items_qty' => '',
                'items' => '',
                'totals' => ''
            ];
            $result = $this->customerCartIdInterfaceFactory->create();
            $result->setData($data);
        }

        return $result;
    }

    public function getItemModel() {
        $itemModel = $this->objectManager->create('Magento\Quote\Model\Quote\Item'); //Quote item model to load quote item
        return $itemModel;
    }

    /**
     * It will clear the cart Items
     * @api
     * @param int $customerId
     * @return \Zoylo\Webservices\Api\Data\ClearCartItemsInterface containing Cart objects
     */
    public function clearCartItems($customerId) {
        $storeId = $this->storeManager->getStore()->getStoreId();
        $quote = $this->createCustomerCart($customerId, $storeId);
        $cartId = $quote->getId();
        try {
            //Removing items from cart
            $itemsCount = $quote->getItemsCount();
            $quoteAllItems = $quote->getAllItems();
            if ($itemsCount > 0) {
                foreach ($quoteAllItems as $item) {
                    $sku = $item->getSku();
                    //$quote->removeItem($item->getId())->save();
                    $itemId = $item->getItemId(); //item id of particular item
                    $quoteItem = $this->getItemModel()->load($itemId); //load particular item which you want to delete by his item id
                    $quoteItem->delete(); //deletes the item
                }
                $this->quoteRepository->save($quote);
                $message = __('All cart items has been deleted');
            } else {
                $message = __('No items to delete in the cart');
            }

            $data = [
                'code' => 290,
                'success' => 'true',
                'message' => $message,
                'cart_id' => $cartId
            ];
            $result = $this->clearCartItemsInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 290,
                'success' => 'false',
                'message' => $message,
                'cart_id' => $cartId
            ];
            $result = $this->clearCartItemsInterfaceFactory->create();
            $result->setData($data);
            //throw new CouldNotSaveException(__('Cannot create quote'));
        } catch (CouldNotSaveException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 290,
                'success' => 'false',
                'message' => $message,
                'cart_id' => $cartId
            ];
            $result = $this->clearCartItemsInterfaceFactory->create();
            $result->setData($data);
        }

        return $result;
    }

    /**
     * Creates a cart for the currently logged-in customer.
     *
     * @param int $customerId
     * @param int $storeId
     * @return \Magento\Quote\Model\Quote Cart object.
     * @throws CouldNotSaveException The cart could not be created.
     */
    protected function createCustomerCart($customerId, $storeId) {
        try {
            $quote = $this->quoteRepository->getActiveForCustomer($customerId);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $customer = $this->customerRepository->getById($customerId);
            /** @var \Magento\Quote\Model\Quote $quote */
            $quote = $this->quoteFactory->create();
            $quote->setStoreId($storeId);
            $quote->setCustomer($customer);
            $quote->setCustomerIsGuest(0);
        }
        return $quote;
    }

    /**
     * Merge guest cart to logged in customer cart
     *
     * @param string $guestQuoteId
     * @param int $customerId
     * @param int $storeId
     * @return \Zoylo\Webservices\Api\Data\MergeCartInterface containing Cart information objects
     */
    public function mergeCart($cartId, $customerId, $storeId) {
        try {
            $quoteIdMask1 = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
            $guestQuote = $this->quoteRepository->get($quoteIdMask1->getQuoteId());
            $storeId = $this->storeManager->getStore()->getStoreId();
            $quote = $this->createCustomerCart($customerId, $storeId);
            $customer = $this->customerRepository->getById($customerId);
            $customerModel = $this->customerFactory->create();

            if (!in_array($storeId, $customerModel->load($customerId)->getSharedStoreIds())) {
                throw new StateException(
                __('Cannot assign customer to the given cart. The cart belongs to different store.')
                );
            }
            if ($guestQuote->getCustomerId()) {
                throw new StateException(
                __('Cannot assign customer to the given cart. The cart is not anonymous.')
                );
            }
            /* try {
              $this->quoteRepository->getForCustomer($customerId);
              throw new StateException(
              __('Cannot assign customer to the given cart. Customer already has active cart.')
              );
              } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
              } */
            $quote->merge($guestQuote);
            //$quote->setCustomer($customer);
            $guestQuote->setCustomerIsGuest(0);
            /** @var \Magento\Quote\Model\QuoteIdMask $quoteIdMask */
            $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'quote_id');
            if ($quoteIdMask->getId()) {
                $quoteIdMask->delete();
            }
            $this->quoteRepository->save($quote);

            //Deleting Cart Id from the Device table
            $guestCartIdModel = $this->guestcartFactory->create();
            $deviceIdCollection = $guestCartIdModel->getCollection()
                    ->addFieldToFilter('cart_id', array('eq' => $cartId));
            if (sizeof($deviceIdCollection) > 0) {
                foreach ($deviceIdCollection as $deviceData) {
                    $deviceData->delete();
                }
            }

            $quote = $this->quoteRepository->getForCustomer($customerId);
            $totalItems = $quote->getItemsCount();
            $totalQuantity = $quote->getItemsQty();

            $message = __('Cart Information');
            $data = [
                'code' => 230,
                'success' => 'true',
                'message' => $message,
                'items_count' => $totalItems,
                'items_qty' => $totalQuantity
            ];
            $result = $this->mergeCartInterfaceFactory->create();
            $result->setData($data);
        } catch (StateException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 230,
                'success' => 'false',
                'message' => $message,
                'items_count' => '',
                'items_qty' => ''
            ];
            $result = $this->mergeCartInterfaceFactory->create();
            $result->setData($data);
        }

        return $result;
    }

    /**
     * Reorder API
     *
     * @param int $customerId
     * @param string $orderId
     * @return \Zoylo\Webservices\Api\Data\ReorderInterface containing Reorder information objects
     */
    public function reorder($customerId, $orderId) {
        $baseUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $redirectUrl = $baseUrl . "checkout/cart";
        $message = "";
        try {

            $cartId = $this->quoteManagement->createEmptyCartForCustomer($customerId);
            $quote = $this->quoteRepository->getActive($cartId);
            $order = $this->objectManager->create('\Magento\Sales\Model\Order')->loadByIncrementId($orderId);

            $reorderItemsArray = $disableFlagItems= array();
            //add items in quote
            $cnt=0;
            foreach ($order->getAllItems() as $item) {
                /*$product_id = $item->getProductId();
                $_productStock = $this->_stockItemRepository->get($product_id);
                $stock_status = $_productStock->getIsInStock();

                $sku = $item->getSku();
                $product = $this->_productRepository->get($sku);
                $status = $product->getStatus();
                $sellable_flag = $product->getSellableFlag();

                  if($status == 1 && $sellable_flag == 1 && $stock_status == 1){ // for active items only reorder
                      $quote->addProduct($product, intval($item->getQtyOrdered()));
                      $reorderItemsArray[] = $product->getId();
                  }*/
                
                $stockStatus= "";
                $itemName = $item->getName();
                $sku = $item->getSku();
                $product = $this->_productRepository->get($sku);
                $status = $product->getStatus();
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $StockItemState = $objectManager->get('\Magento\CatalogInventory\Model\Stock\Item');
                $stockItem = $StockItemState->load($product->getId(), 'product_id');
                $stockItemId = $stockItem->getItemId();
                if($stockItemId!=''){
                    $StockState = $objectManager->get('\Magento\CatalogInventory\Model\Stock\StockItemRepository');
                    $proStock = $StockState->get($stockItemId);
                    $stockStatus = $proStock->getIsInStock();
                }
                $sellableFlag = $product->getSellableFlag();
                if($status == 1 && $sellableFlag == 1 && $stockStatus == 1){ // for active items only reorder
                  $quote->addProduct($product, intval($item->getQtyOrdered()));
                  $reorderItemsArray[] = $product->getId();
                }else{
                  $disableFlagItems[] = $itemName;
                }
                $cnt++;
            }
            $quote->getBillingAddress()->setCollectShippingRates(true);
            $quote->getShippingAddress()->setCollectShippingRates(true);
            $this->quoteRepository->save($quote);


             /* Code written by Ajesh Starts */
            $itemsCount = $quote->getItemsCount();
            $quoteAllItems = $quote->getAllItems();
            if($itemsCount>0){
                foreach ($quoteAllItems as $item) {
                    $skuNew = $item->getSku();
                    $productObj = $this->_productRepository->get($skuNew);
                    $productId  = $productObj->getId();
                    if($productId!=''){
                        if (!in_array($productId, $reorderItemsArray)){
                            try {
                                $product = $this->_productRepository->getById($productId);
                            } catch (NoSuchEntityException $e) {
                                $product = null;
                            }
                            $this->_wishlistRepository = $this->objectManager->create('\Magento\Wishlist\Model\WishlistFactory');
                            $wishlist = $this->_wishlistRepository->create()->loadByCustomerId($customerId, true);
                            $wishlist->addNewItem($product);
                            $wishlist->save(); //Wishlist Save
                            $itemId = $item->getItemId(); //item id of particular item
                            $quoteItem = $this->getItemModel()->load($itemId); //load particular item which you want to delete by his item id
                            $quoteItem->delete(); //Quote item delete
                        }
                    }
                    
                }
            }
            /* Code written by Ajesh ends */
            $baseUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
            $redirectUrl = $baseUrl . "checkout/cart";
            if(count($disableFlagItems)>0){
                if(count($disableFlagItems)!=$cnt){
                    $message .= __('Re-order few items are not available');
                }else{
                    $message .= __('Re-order all items are not available');
                }
            }else{
                $message .= __('Reorder Information');
            }
            $data = [
                'code' => 320,
                'success' => 'true',
                'message' => $message,
                'redirect_url' => $redirectUrl
            ];
            $result = $this->reorderInterfaceFactory->create();
            $result->setData($data);
        } catch (NoSuchEntityException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 320,
                'success' => 'false',
                'message' => $message,
                'redirect_url' => ''
            ];
            $result = $this->reorderInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 320,
                'success' => 'false',
                'message' => $message,
                'redirect_url' => ''
            ];
            $result = $this->reorderInterfaceFactory->create();
            $result->setData($data);
        } catch (LocalizedException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 320,
                'success' => 'false',
                'message' => $message,
                'redirect_url' => ''
            ];
            $result = $this->reorderInterfaceFactory->create();
            $result->setData($data);
        }

        return $result;
    }

    /**
     * Admin Token function
     *
     * @api
     * @return array
     */
    public function adminToken() {
        $baseUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        define('BASEURL', $baseUrl . 'rest');

        $tokenApi = "/V1/integration/admin/token";
        $api_username = self::ADMIN_API_USERNAME;
        $api_pd = self::ADMIN_API_PASSWORD;
        $data = array("username" => $api_username, "password" => $api_pd);
        $apiUrl = BASEURL . $tokenApi;
        $data_string = json_encode($data);
        try {
            $ch = curl_init($apiUrl);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json'
                    //'Content-Length: ' . strlen($data_string)
                    )
            );
            $token = curl_exec($ch);
            $token = json_decode($token);
            if (isset($token->message)) {
                echo $token->message;
            } else {
                $key = $token;
            }
        } catch (\Exception $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $key;
    }

    public function getBrandCollection() {
        $collection = $this->_brandCollection->getCollection()->addFieldToFilter('position', 2)->addFieldToFilter('status', 1);
               // ->setOrder('position', 'ASC')
                //->addFieldToFilter('status', 1);
        $collection->setPageSize(215);
        return $collection;
    }

    public function getManufacturerCollection() {
        /**
         * Get attribute info by attribute code and entity type
         */
        $attributeCode = 'manufacturer';
        $entityType = 'catalog_product';
        $attributeInfo = $this->objectManager->get(\Magento\Eav\Model\Entity\Attribute::class)->loadByCode($entityType, $attributeCode);

        /**
         * Get all options name and value of the attribute
         */
        $attributeId = $attributeInfo->getAttributeId();
        $attributeOptionAll = $this->objectManager->get(\Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection::class)
                ->setPositionOrder('asc')
                ->setAttributeFilter($attributeId)
                ->setStoreFilter()
                ->load();
        return $attributeOptionAll;
    }

    public function getOtcBrandCollection() {
        /**
         * Get attribute info by attribute code and entity type
         */
        $attributeCode = 'product_brand';
        $entityType = 'catalog_product';
        $attributeInfo = $this->objectManager->get(\Magento\Eav\Model\Entity\Attribute::class)->loadByCode($entityType, $attributeCode);

        /**
         * Get all options name and value of the attribute
         */
        $attributeId = $attributeInfo->getAttributeId();
        $attributeOptionAll = $this->objectManager->get(\Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection::class)
                ->setPositionOrder('asc')
                ->setAttributeFilter($attributeId)
                ->setStoreFilter()
                ->load();
        $manufacturerList = [];
        if (count($attributeOptionAll) > 0) {
            foreach ($attributeOptionAll as $key => $manufacturer) {
                $manufacturerList[$manufacturer->getValue()] = $manufacturer->getOptionId();
            }
        }
        return $manufacturerList;
    }

    /**
     * Return home collection
     * @api
     * @param string $sku
     * @return \Zoylo\Webservices\Api\Data\ProductInfoInterface containing Products objects
     * @throws NoSuchEntityException
     */
    public function getProductInfo($sku) {
        try {
            $this->logger->info('Product Details');
            $this->logger->info('Product SKU: ' . $sku);
            $message = 'Product info Of (' . $sku . ') Sku.';
            $productId = $this->resourceProductModel->getIdBySku($sku);
            if (!$productId) {
                throw new NoSuchEntityException(__('Requested product doesn\'t exist'));
            }
            $mediaUrl = $this->storeManager
                    ->getStore()
                    ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

            $baseUrl = $this->storeManager->getStore()->getBaseUrl();
            $product = $this->_productRepository->getById($productId);
            $stockItem = $this->stockState->getStockItem(
                    $product->getId(), $product->getStore()->getWebsiteId()
            );

            $productList['id'] = $product->getId();
            $productList['sku'] = $product->getSku();
            $productName        = '';
            $name               = $product->getResource()->getAttribute('name')->getFrontend()->getValue($product);
            $displayName        = $product->getResource()->getAttribute('sku_display_name')->getFrontend()->getValue($product);
            if ($displayName == '') {
                $productName = $name;
            } else {
                $productName = $displayName;
            }
            $productList['name'] = $productName;
            $productList['status'] = $product->getStatus();
            $stockItem = $this->stockState->getStockItem(
                    $product->getId(), $product->getStore()->getWebsiteId()
            );
            $productList['qty'] = $stockItem->getQty();

            $sale_status_msg = '';
			$sale_status = false;
			$saleProduct = $product->getSellableFlag();
			if (isset($saleProduct) && $saleProduct == 1) {
				if ($product->isSaleable()) {
					$sale_status = true;
					$sale_status_msg = __('Add To Cart');
				} else {
					$sale_status_msg = __('Out of stock');
				}
			} else {
				if (isset($saleProduct) && $saleProduct == 0) {
					$sale_status_msg = __('Not For Sale');
				} elseif (isset($saleProduct) && $saleProduct == 2) {
					$sale_status_msg = __('Discontinued');
				} else {
					if ($product->isSaleable()) {
						$sale_status = true;
						$sale_status_msg = __('Add To Cart');
					}
				}
			}
			
			$productList['sale_status'] = $sale_status;
			$productList['sale_status_msg'] = $sale_status_msg;

            $productList['product_type'] = $product->getAttributeText('zoylo_sku_group');
            $drug_type = $product->getResource()->getAttribute('drug_type')->getFrontend()->getValue($product);
            $drugTypeUrl = '';
            if (isset($drug_type) && !is_array($drug_type) && $drug_type != '' && $drug_type != 'no_selection' ) {
                $drugTypeUrl = $mediaUrl . 'packtype/' . ucwords(strtolower($drug_type)) . '.png';
            } elseif (strtolower($productList['product_type']) == "otc") {
                $categoryIds = $product->getCategoryIds();
                $_categoryImgUrl = '';
                foreach ($categoryIds as $catId) {
                    if ($catId == 1 || $catId == 2 || $catId == 3 || $catId == 6 || $catId == 7)
                        continue;

                    $category = $this->categoryRepository->get($catId);
                    if ($category->getLevel() == 3) {
                        $_categoryImgUrl = $category->getImageUrl();
                    }
                }
                if ($_categoryImgUrl) {
                    $drugTypeUrl = $_categoryImgUrl;
                } else {
                    $drugTypeUrl = $mediaUrl . 'packtype/OTC.png';
                }
            }

            $products = $this->_productRepository->get($product->getSku());
            if ($products->getPrescriptionRequired() == "required")
                $productList['prescription_status'] = true;
            else
                $productList['prescription_status'] = false;

            $image = '';
            if ($product->getImage() && $product->getImage() != "no_selection") {
                $image = $mediaUrl . 'catalog/product' . $product->getImage();
            }
            if ($image == '' && $drugTypeUrl) {
                $image = $drugTypeUrl;
            }
            if ($image == '') {
                $image = $baseUrl . 'pub/static/frontend/Smartwave/porto/en_US/Magento_Catalog/images/product/placeholder/image.jpg';
            }
            $productList['image'] = $image;

            /* if($product->getThumbnail()){
              $productList['image'] = $mediaUrl. 'catalog/product' . $product->getThumbnail();
              }elseif($drugTypeUrl) {
              $productList['image'] = $drugTypeUrl;
              }else{
              $productList['image'] = $baseUrl.'pub/static/frontend/Smartwave/porto/en_US/Magento_Catalog/images/product/placeholder/small_image.jpg';
              } */

            $productList['price'] = $product->getPrice();
            $productList['url'] = $product->getProductUrl();
            $productList['pack'] = $product->getPackSize();
            $productList['pack_type'] = $product->getPackType();

            //Special Price
            $orgprice = $product->getPrice();
            $specialprice = $product->getSpecialPrice();
            $finalPrice = $product->getFinalPrice();
            if($finalPrice != $orgprice){
                $specialprice = $finalPrice;
            }
            $specialfromdate = $product->getSpecialFromDate();
            $specialtodate = $product->getSpecialToDate();
            $today = time();
            $save_percent = 0;
            if ($specialprice < $orgprice && ($specialprice)) {
                    $save_percent = 100 - round(($specialprice / $orgprice) * 100);

            }
			if($save_percent > 0){
				$productList['special_price'] = $specialprice;
			}else{
				$productList['special_price'] = null;
			}
					
            $productList['save_percent'] = $save_percent;
            $productList['manufacture'] = $product->getAttributeText('product_brand');
            

            $composition = $product->getResource()->getAttribute('salt_composition')->getFrontend()->getValue($product);
            $productList['composition'] = $composition;
            $productList['salt'] = $composition;
            if (isset($drug_type) && !is_array($drug_type)){
                $productList['drug_type'] = $drug_type;
                //substitutes
                $substitutesCollection = $this->getProductsByComposition($composition, $productId, $drug_type);
                if (count($substitutesCollection) > 0) {
                    foreach ($substitutesCollection as $substitutes) {
                        $manufacture = $substitutes->getAttributeText('product_brand');
                        $productType = $substitutes->getAttributeText('zoylo_sku_group');
                        //$productList['manufacture'] = $manufacture;
                        $substitutesName = $substitutes->getResource()->getAttribute('name')->getFrontend()->getValue($substitutes);
                        $substitutesDisplayName = $substitutes->getResource()->getAttribute('sku_display_name')->getFrontend()->getValue($substitutes);
                          
                        if ($substitutesDisplayName == '') {
                            $substitutesProductName = $substitutesName;
                        } else {
                            $substitutesProductName = $substitutesDisplayName;
                        }
						
						$substitutes_sale_status_msg = '';
						$substitutes_sale_status = false;
						$substitutesSaleProduct = $substitutes->getSellableFlag();
						if (isset($substitutesSaleProduct) && $substitutesSaleProduct == 1) {
							if ($substitutes->isSaleable()) {
								$substitutes_sale_status = true;
								$substitutes_sale_status_msg = __('Add To Cart');
							} else {
								$substitutes_sale_status_msg = __('Out of stock');
							}
						} else {
							if (isset($substitutesSaleProduct) && $substitutesSaleProduct == 0) {
								$substitutes_sale_status_msg = __('Not For Sale');
							} elseif (isset($substitutesSaleProduct) && $substitutesSaleProduct == 2) {
								$substitutes_sale_status_msg = __('Discontinued');
							} else {
								if ($substitutes->isSaleable()) {
									$substitutes_sale_status = true;
									$substitutes_sale_status_msg = __('Add To Cart');
								}
							}
						}
			
                        $substitutesData[] = [
                            'title' => __('Substitutes for Ibugesic tablet'),
                            'name' => $substitutesProductName,
                            'price' => $substitutes->getPrice(),
                            'sku' => $substitutes->getSku(),
                            'id' => $substitutes->getId(),
                            'pack' => $substitutes->getPackSize(),
                            'pack_type' => $substitutes->getPackType(),
                            'manufacture' => $manufacture,
                            'sale_status' => $substitutes_sale_status,
                            'sale_status_msg' => $substitutes_sale_status_msg
                        ];
                    }
                    $productList['substitutes_data'] = $substitutesData;
                }
            }
            

            $alcohol = '';
            $driving = '';
            $pregnancy = '';
            $lactation = '';
            $kidney = '';
            $smoking = '';
            $tolerance = '';
            $vision = '';
            $overdose = '';
            $driving = '';
            $exercise = '';
            $operating_heavy_machinery = '';
            $antibiotics = '';
            $anti_inflammatory = '';
            $chemotherapy_drugs = '';
            $anticonvulsants = '';
            $sulfa_drugs = '';
            $high_blood_pressure = '';
            $children = '';
            $seniors = '';
            $usesof = '';
            $expertadvice = '';
            $frequentQuestions = '';
            $low_blood_pressure = '';
            $diabetes = '';
            $liver = '';
            $oral_contraceptives = '';
            $caffeine = '';
            $pain_killers = '';
            $anticoagulants = '';
            $heart = '';
            $salt = '';
            if (!empty($composition)) {
                $pos = strpos($composition, " + ");
                if ($pos == true) {
                    $salt = trim($composition);

                } else {
                    $salt = preg_replace('/[0-9]+/', '', $composition);
                    $salt = str_replace('MG', '', $salt);
                    $salt = trim($salt);
                }
            
				$aliasType = '';
				if ($drug_type == 'TABLETS' || $drug_type == 'CAPSULES') {
					$aliasType = 'Tablet';
				} else if ($drug_type == 'DRY SYP') {
					$aliasType = 'Dry Syrup';
				} else if ($drug_type == 'SYRUP' || $drug_type == 'SUSPENSION' || $drug_type == 'GEL(SYRUP)' || $drug_type == 'EXPECTORANT') {
					$aliasType = 'Syrup';
				} else if ($drug_type == 'BALM' || $drug_type == 'CREAM' || $drug_type == 'OINTMENT' || $drug_type == 'OIL' || $drug_type == 'LOTION' || $drug_type == 'GEL(PASTE)' || $drug_type == 'GEL(TOPICAL)' || $drug_type == 'GEL' || $drug_type == 'LINIMENT' || $drug_type == 'SERUM') {
					$aliasType = 'Gel';
				} else if ($drug_type == 'FACE WASH' || $drug_type == 'FOAM' || $drug_type == 'SOAP') {
					$aliasType = 'Soap';
				} else if ($drug_type == 'ABDOMINAL BINDER' || $drug_type == 'ANTISEPTIC POWDER' || $drug_type == 'DUSTING POWDER' || $drug_type == 'ENEMA' || $drug_type == 'GRANULES' || $drug_type == 'POWDER') {
					$aliasType = 'Powder';
				} else if ($drug_type == 'SUPPOSITORIES') {
					$aliasType = 'Suppositories';
				} else if ($drug_type == 'INHALER') {
					$aliasType = 'Inhaler';
				} else if ($drug_type == 'INJECTIBLE' || $drug_type == 'INJECTION' || $drug_type == 'INJECTION') {
					$aliasType = 'Injection';
				} else if ($drug_type == 'EYE EAR DROPS' || $drug_type == 'EYE DROPS' || $drug_type == 'EAR DROPS' || $drug_type == 'NASAL DROPS' || $drug_type == 'ORAL DROPS' || $drug_type == 'GEL(PASTE)' || $drug_type == 'GEL(TOPICAL)' || $drug_type == 'GEL' || $drug_type == 'LINIMENT' || $drug_type == 'SERUM') {
					$aliasType = 'Drops';
				} else if ($drug_type == 'LIQUID' || $drug_type == 'RESPULES' || $drug_type == 'MOUTH WASH' || $drug_type == 'SOLUTION') {
					$aliasType = 'Liquid';
				} else if ($drug_type == 'SPRAY' || $drug_type == 'NASAL SPRAY') {
					$aliasType = 'Spray';
				}

				$saltInformation = $this->objectManager->create('I95Dev\Salts\Model\Salt')->getCollection()->addFieldToFilter('name',  array('like' => $salt))
									->addFieldToFilter('drug_type', array('like' => '%'.$aliasType.'%'));
				$saltInformation->getSelect()->limit(1);
				foreach ($saltInformation as $information) {
					$alcohol = $information->getKidney();
					$driving = $information->getDriving();
					$pregnancy = $information->getPregnancy();
					$lactation = $information->getLactation();
					$kidney = $information->getKidney();
					$smoking = $information->getSmoking();
					$tolerance = $information->getTolerance();
					$vision = $information->getVision();
					$overdose = $information->getOverdose();
					$driving = $information->getDriving();
					$exercise = $information->getExercise();
					$operating_heavy_machinery = $information->getOperatingHeavyAchinery();
					$antibiotics = $information->getAntibiotics();
					$anti_inflammatory = $information->getAntiInflammatory();
					$chemotherapy_drugs = $information->getChemotherapyDrugs();
					$anticonvulsants = $information->getAnticonvulsants();
					$sulfa_drugs = $information->getSulfaDrugs();
					$high_blood_pressure = $information->getHighBloodPressure();
					$children = $information->getChildrenSalt();
					$seniors = $information->getSeniors();
					$heart = $information->getHeart();
					$usesof = $information->getUses();
					$caffeine = $information->getCaffeine();
					$expertadvice = $information->getExpertAdvice();
					$frequentQuestions = $information->getFaqs();
					$frequentQuestions = str_replace('name1', $productName, $frequentQuestions);
					$frequentQuestions = str_replace('name', $productName, $frequentQuestions);
					$frequentQuestions = str_replace('$', '', $frequentQuestions);
					$antibiotics = $information->getAntibiotics();
					$pain_killers = $information->getPainKillers();
					$anticoagulants = $information->getAnticoagulants();

					$kidney1 = str_replace(" ","_",$kidney);
					$pregnancy1 = str_replace(" ","_",$pregnancy);
					$heart1 = str_replace(" ","_",$heart);
					$liver1 = str_replace(" ","_",$liver);
					$lactation1 = str_replace(" ","_",$lactation);
					$children1 = str_replace(" ","_",$children);
					$seniors1 = str_replace(" ","_",$seniors);

					$smoking1 = str_replace(" ","_",$smoking);
					$alcohol1 = str_replace(" ","_",$alcohol);
					$caffeine1 = str_replace(" ","_",$caffeine);
					$vision1 = str_replace(" ","_",$vision);
					$overdose1 = str_replace(" ","_",$overdose);
					$tolerance1 = str_replace(" ","_",$tolerance);
					$driving1 = str_replace(" ","_",$driving);
					$exercise1 = str_replace(" ","_",$exercise);
					$operating_heavy_machinery1 = str_replace(" ","_",$operating_heavy_machinery);

					$antibiotics1 = str_replace(" ","_",$antibiotics);
					$pain_killers1 = str_replace(" ","_",$pain_killers);
					$chemotherapy_drugs1 = str_replace(" ","_",$chemotherapy_drugs);
					$anticoagulants1 = str_replace(" ","_",$anticoagulants);
					$anticonvulsants1 = str_replace(" ","_",$anticonvulsants);
					$sulfa_drugs1 = str_replace(" ","_",$sulfa_drugs);

					$antibioticstext        = $this->customHelperData->getConfig('allergy_interaction/antibiotics/' . strtolower($antibiotics1));
					$antibioticstext        = str_replace('name1', $productName, $antibioticstext);
					$antibioticstext        = str_replace('name', $productName, $antibioticstext);
					$antibioticstext        = str_replace('$', '', $antibioticstext);
					$anti_inflammatorytext  = $this->customHelperData->getConfig('allergy_interaction/anti_inflammatory/' . strtolower($anti_inflammatory));
					$anti_inflammatorytext  = str_replace('name1', $productName, $anti_inflammatorytext);
					$anti_inflammatorytext  = str_replace('name', $productName, $anti_inflammatorytext);
					$anti_inflammatorytext  = str_replace('$', '', $anti_inflammatorytext);
					$chemotherapy_drugstext = $this->customHelperData->getConfig('allergy_interaction/chemotherapy_drugs/' . strtolower($chemotherapy_drugs1));
					$chemotherapy_drugstext = str_replace('name1', $productName, $chemotherapy_drugstext);
					$chemotherapy_drugstext = str_replace('name', $productName, $chemotherapy_drugstext);
					$chemotherapy_drugstext = str_replace('$', '', $chemotherapy_drugstext);
					$anticonvulsantstext    = $this->customHelperData->getConfig('allergy_interaction/anticonvulsants/' . strtolower($anticonvulsants1));
					$anticonvulsantstext    = str_replace('name1', $productName, $anticonvulsantstext);
					$anticonvulsantstext    = str_replace('name', $productName, $anticonvulsantstext);
					$anticonvulsantstext    = str_replace('$', '', $anticonvulsantstext);
					$sulfa_drugstext        = $this->customHelperData->getConfig('allergy_interaction/sulfa_drugs/' . strtolower($sulfa_drugs1));
					$sulfa_drugstext        = str_replace('name1', $productName, $sulfa_drugstext);
					$sulfa_drugstext        = str_replace('name', $productName, $sulfa_drugstext);
					$sulfa_drugstext        = str_replace('$', '', $sulfa_drugstext);
					$pain_killerstext       = $this->customHelperData->getConfig('allergy_interaction/pain_killers/' . strtolower($pain_killers1));
					$pain_killerstext        = str_replace('name1', $productName, $pain_killerstext);
					$pain_killerstext        = str_replace('name', $productName, $pain_killerstext);
					$pain_killerstext        = str_replace('$', '', $pain_killerstext);
					$anticoagulantstext     = $this->customHelperData->getConfig('allergy_interaction/anticoagulants/' . strtolower($anticoagulants1));
					$anticoagulantstext        = str_replace('name1', $productName, $anticoagulantstext);
					$anticoagulantstext        = str_replace('name', $productName, $anticoagulantstext);
					$anticoagulantstext        = str_replace('$', '', $anticoagulantstext);

					$kidneytext = $this->customHelperData->getConfig('interaction_with_common_conditions/kidney/' . strtolower($kidney1));
					$kidneytext        = str_replace('name1', $productName, $kidneytext);
					$kidneytext        = str_replace('name', $productName, $kidneytext);
					$kidneytext        = str_replace('$', '', $kidneytext);
					$pregnancytext = $this->customHelperData->getConfig('interaction_with_common_conditions/pregnancy/' . strtolower($pregnancy1));
					$pregnancytext        = str_replace('name1', $productName, $pregnancytext);
					$pregnancytext        = str_replace('name', $productName, $pregnancytext);
					$pregnancytext        = str_replace('$', '', $pregnancytext);
					$high_blood_pressuretext = $this->customHelperData->getConfig('interaction_with_common_conditions/high_blood_pressure/' . strtolower($high_blood_pressure));
					$high_blood_pressuretext        = str_replace('name1', $productName, $high_blood_pressuretext);
					$high_blood_pressuretext        = str_replace('name', $productName, $high_blood_pressuretext);
					$high_blood_pressuretext        = str_replace('$', '', $high_blood_pressuretext);
					//$low_blood_pressuretext = $this->customHelperData->getConfig('interaction_with_common_conditions/low_blood_pressure/'.strtolower($low_blood_pressure));
					//$diabetestext = $this->customHelperData->getConfig('interaction_with_common_conditions/diabetes/'.strtolower($diabetes));
					//$livertext = $this->customHelperData->getConfig('interaction_with_common_conditions/liver/'.strtolower($liver));
					$lactationtext = $this->customHelperData->getConfig('interaction_with_common_conditions/lactation/' . strtolower($lactation1));
					$lactationtext        = str_replace('name1', $productName, $lactationtext);
					$lactationtext        = str_replace('name', $productName, $lactationtext);
					$lactationtext        = str_replace('$', '', $lactationtext);
					//$oral_contraceptivestext = $this->customHelperData->getConfig('interaction_with_common_conditions/oral_contraceptives/'.strtolower($oral_contraceptives));
					$childrentext = $this->customHelperData->getConfig('interaction_with_common_conditions/children/' . strtolower($children1));
					$childrentext        = str_replace('name1', $productName, $childrentext);
					$childrentext        = str_replace('name', $productName, $childrentext);
					$childrentext        = str_replace('$', '', $childrentext);
					$seniorstext = $this->customHelperData->getConfig('interaction_with_common_conditions/seniors/' . strtolower($seniors1));
					$seniorstext        = str_replace('name1', $productName, $seniorstext);
					$seniorstext        = str_replace('name', $productName, $seniorstext);
					$seniorstext        = str_replace('$', '', $seniorstext);
					$hearttext          = $this->customHelperData->getConfig('interaction_with_common_conditions/heart/' . strtolower($heart1));
					$hearttext        = str_replace('name1', $productName, $hearttext);
					$hearttext        = str_replace('name', $productName, $hearttext);
					$hearttext        = str_replace('$', '', $hearttext);

					$smokingtext = $this->customHelperData->getConfig('precautions/smoking/' . strtolower($smoking1));
					$smokingtext        = str_replace('name1', $productName, $smokingtext);
					$smokingtext        = str_replace('name', $productName, $smokingtext);
					$smokingtext        = str_replace('$', '', $smokingtext);
					$alcoholtext = $this->customHelperData->getConfig('precautions/alcohol/' . strtolower($alcohol1));
					$alcoholtext        = str_replace('name1', $productName, $alcoholtext);
					$alcoholtext        = str_replace('name', $productName, $alcoholtext);
					$alcoholtext        = str_replace('$', '', $alcoholtext);
					$tolerancetext = $this->customHelperData->getConfig('precautions/tolerance/' . strtolower($tolerance1));
					$tolerancetext        = str_replace('name1', $productName, $tolerancetext);
					$tolerancetext        = str_replace('name', $productName, $tolerancetext);
					$tolerancetext        = str_replace('$', '', $tolerancetext);
					$visiontext = $this->customHelperData->getConfig('precautions/vision/' . strtolower($vision1));
					$visiontext        = str_replace('name1', $productName, $visiontext);
					$visiontext        = str_replace('name', $productName, $visiontext);
					$visiontext        = str_replace('$', '', $visiontext);
					$overdosetext = $this->customHelperData->getConfig('precautions/overdose/' . strtolower($overdose1));
					$overdosetext        = str_replace('name1', $productName, $overdosetext);
					$overdosetext        = str_replace('name', $productName, $overdosetext);
					$overdosetext        = str_replace('$', '', $overdosetext);
					$drivingtext = $this->customHelperData->getConfig('precautions/driving/' . strtolower($driving1));
					$drivingtext        = str_replace('name1', $productName, $drivingtext);
					$drivingtext        = str_replace('name', $productName, $drivingtext);
					$drivingtext        = str_replace('$', '', $drivingtext);
					$exercisetext = $this->customHelperData->getConfig('precautions/exercise/' . strtolower($exercise1));
					$exercisetext        = str_replace('name1', $productName, $exercisetext);
					$exercisetext        = str_replace('name', $productName, $exercisetext);
					$exercisetext        = str_replace('$', '', $exercisetext);
					$operating_heavy_machinerytext = $this->customHelperData->getConfig('precautions/operating_heavy_machinery/' . strtolower($operating_heavy_machinery1));
					$operating_heavy_machinerytext        = str_replace('name1', $productName, $operating_heavy_machinerytext);
					$operating_heavy_machinerytext        = str_replace('name', $productName, $operating_heavy_machinerytext);
					$operating_heavy_machinerytext        = str_replace('$', '', $operating_heavy_machinerytext);
					$caffeinetext = $this->customHelperData->getConfig('precautions/driving/' . strtolower($caffeine1));
					$caffeinetext        = str_replace('name1', $productName, $caffeinetext);
					$caffeinetext        = str_replace('name', $productName, $caffeinetext);
					$caffeinetext        = str_replace('$', '', $caffeinetext);
				}
				//MEDICAL OVERVIEW
				//$usesof = $product->getResource()->getAttribute('primary_symptoms')->getFrontend()->getValue($product);
				$dosagesideeffects = $product->getResource()->getAttribute('dosage')->getFrontend()->getValue($product);
				//$expertadvice = $product->getResource()->getAttribute('expert_advice')->getFrontend()->getValue($product);
				$description = $product->getResource()->getAttribute('description')->getFrontend()->getValue($product);
				$special_instructions = $product->getResource()->getAttribute('special_instructions')->getFrontend()->getValue($product);
				$overviewItems = null;
				for ($i = 0; $i < 6; $i++) {
					if ($i == 0) {
						if ($composition != '' && $composition != null) {
							$overviewItems[$i]['sub_title'] = __('Product Composition');
							$overviewItems[$i]['description'] = $composition;
						}
					} elseif ($i == 1) {
						if ($usesof != '' && $usesof != null) {
							$overviewItems[$i]['sub_title'] = __('Uses of ') . $productName;
							$usesof = str_replace('name', $productName, $usesof);
							$usesof = str_replace('$', '', $usesof);
							$overviewItems[$i]['description'] = $usesof;
						}
					} elseif ($i == 2) {
						if ($dosagesideeffects != '' && $dosagesideeffects != null) {
							$overviewItems[$i]['sub_title'] = __('Side effects of salts in ') . $productName;
							$overviewItems[$i]['description'] = $dosagesideeffects;
						}
					} elseif ($i == 3) {
						if ($expertadvice != '' && $expertadvice != null) {
							$overviewItems[$i]['sub_title'] = __('Expert Advice For ') . $productName;
							$expertadvice = str_replace('name', $productName, $expertadvice);
							$expertadvice = str_replace('$', '', $expertadvice);
							$overviewItems[$i]['description'] = $expertadvice;
						}
					} elseif ($i == 4) {
						if ($description != '' && $description != null) {
							$overviewItems[$i]['sub_title'] = __('Information About of ') . $productName;
							$overviewItems[$i]['description'] = $description;
						}
					} elseif ($i == 5) {
						if ($special_instructions != '' && $special_instructions != null) {
							$overviewItems[$i]['sub_title'] = __('Special Instructions');
							$overviewItems[$i]['description'] = $special_instructions;
						}
					}
				}

				if (count($overviewItems) > 0) {
					array_splice($overviewItems, 0, 0);
					$medicalOverviewData = [
						'title' => __('Medical Overview'),
						'items' => $overviewItems
					];
					$productList['medical_overview_data'] = $medicalOverviewData;
				}

				//ALLERGY INTERACTIONS
				/* $antibiotics = $product->getResource()->getAttribute('antibiotics')->getFrontend()->getValue($product);
				  $anti_inflammatory = $product->getResource()->getAttribute('anti_inflammatory')->getFrontend()->getValue($product);
				  $chemotherapy_drugs = $product->getResource()->getAttribute('chemotherapy_drugs')->getFrontend()->getValue($product);
				  $anticonvulsants = $product->getResource()->getAttribute('anticonvulsants')->getFrontend()->getValue($product);
				  $sulfa_drugs = $product->getResource()->getAttribute('sulfa_drugs')->getFrontend()->getValue($product); */


				$allergyInteractionsItems = null;
				for ($i = 0; $i < 7; $i++) {
					if ($i == 0) {
						if ($antibiotics != 'NA' && $antibiotics != '') {
							$allergyInteractionsItems[$i]['sub_title'] = __('Penicilin ');
							if ($antibiotics == 'Unknown') {
								$allergyInteractionsItems[$i]['description'] = sprintf($antibioticstext, $productName, $productName);
								$allergyInteractionsItems[$i]['status'] = $antibiotics;
								$allergyInteractionsItems[$i]['attribute_code'] = 'antibiotics';
							} else {
								$allergyInteractionsItems[$i]['description'] = sprintf($antibioticstext, $productName);
								$allergyInteractionsItems[$i]['status'] = $antibiotics;
								$allergyInteractionsItems[$i]['attribute_code'] = 'antibiotics';
							}
						}
					} elseif ($i == 1) {
						if ($anti_inflammatory != 'NA' && $anti_inflammatory != '') {
							$allergyInteractionsItems[$i]['sub_title'] = __('Aspirin ');
							if ($anti_inflammatory == 'Unknown') {
								$allergyInteractionsItems[$i]['description'] = sprintf($anti_inflammatorytext, $productName, $productName);
								$allergyInteractionsItems[$i]['status'] = $anti_inflammatory;
								$allergyInteractionsItems[$i]['attribute_code'] = 'anti_inflammatory';
							} else {
								$allergyInteractionsItems[$i]['description'] = sprintf($anti_inflammatorytext, $productName);
								$allergyInteractionsItems[$i]['status'] = $anti_inflammatory;
								$allergyInteractionsItems[$i]['attribute_code'] = 'anti_inflammatory';
							}
						}
					} elseif ($i == 2) {
						if ($chemotherapy_drugs != 'NA' && $chemotherapy_drugs != '') {
							$allergyInteractionsItems[$i]['sub_title'] = __('Chemotherapy drugs ');
							if ($chemotherapy_drugs == 'Unknown') {
								$allergyInteractionsItems[$i]['description'] = sprintf($chemotherapy_drugstext, $productName, $productName);
								$allergyInteractionsItems[$i]['status'] = $chemotherapy_drugs;
								$allergyInteractionsItems[$i]['attribute_code'] = 'chemotherapy_drugs';
							} else {
								$allergyInteractionsItems[$i]['description'] = sprintf($chemotherapy_drugstext, $productName);
								$allergyInteractionsItems[$i]['status'] = $chemotherapy_drugs;
								$allergyInteractionsItems[$i]['attribute_code'] = 'chemotherapy_drugs';
							}
						}
					} elseif ($i == 3) {
						if ($anticonvulsants != 'NA' && $anticonvulsants != '') {
							$allergyInteractionsItems[$i]['sub_title'] = __('Anticonvulsants ');
							if ($anticonvulsants == 'Unknown') {
								$allergyInteractionsItems[$i]['description'] = sprintf($anticonvulsantstext, $productName, $productName);
								$allergyInteractionsItems[$i]['status'] = $anticonvulsants;
								$allergyInteractionsItems[$i]['attribute_code'] = 'anticonvulsants';
							} else {
								$allergyInteractionsItems[$i]['description'] = sprintf($anticonvulsantstext, $productName);
								$allergyInteractionsItems[$i]['status'] = $anticonvulsants;
								$allergyInteractionsItems[$i]['attribute_code'] = 'anticonvulsants';
							}
						}
					} elseif ($i == 4) {
						if ($sulfa_drugs != 'NA' && $sulfa_drugs != '') {
							$allergyInteractionsItems[$i]['sub_title'] = __('Sulfa Drugs ');
							if ($sulfa_drugs == 'Unknown') {
								$allergyInteractionsItems[$i]['description'] = sprintf($sulfa_drugstext, $productName, $productName);
								$allergyInteractionsItems[$i]['status'] = $sulfa_drugs;
								$allergyInteractionsItems[$i]['attribute_code'] = 'sulfa_drugs';
							} else {
								$allergyInteractionsItems[$i]['description'] = sprintf($sulfa_drugstext, $productName);
								$allergyInteractionsItems[$i]['status'] = $sulfa_drugs;
								$allergyInteractionsItems[$i]['attribute_code'] = 'sulfa_drugs';
							}
						}
					} elseif ($i == 5) {
						if ($pain_killers != 'NA' && $pain_killers != '') {
							$allergyInteractionsItems[$i]['sub_title'] = __('Pain Killers ');
							if ($pain_killers == 'Unknown') {
								$allergyInteractionsItems[$i]['description'] = sprintf($pain_killerstext, $productName, $productName);
								$allergyInteractionsItems[$i]['status'] = $pain_killers;
								$allergyInteractionsItems[$i]['attribute_code'] = 'pain_killers';
							} else {
								$allergyInteractionsItems[$i]['description'] = sprintf($pain_killerstext, $productName);
								$allergyInteractionsItems[$i]['status'] = $pain_killers;
								$allergyInteractionsItems[$i]['attribute_code'] = 'pain_killers';
							}
						}
					} elseif ($i == 6) {
						if ($anticoagulants != 'NA' && $anticoagulants != '') {
							$allergyInteractionsItems[$i]['sub_title'] = __('Anticoagulants ');
							if ($anticoagulants == 'Unknown') {
								$allergyInteractionsItems[$i]['description'] = sprintf($anticoagulantstext, $productName, $productName);
								$allergyInteractionsItems[$i]['status'] = $anticoagulants;
								$allergyInteractionsItems[$i]['attribute_code'] = 'anticoagulants';
							} else {
								$allergyInteractionsItems[$i]['description'] = sprintf($anticoagulantstext, $productName);
								$allergyInteractionsItems[$i]['status'] = $anticoagulants;
								$allergyInteractionsItems[$i]['attribute_code'] = 'anticoagulants';
							}
						}
					}
				}

				if (count($allergyInteractionsItems) > 0) {
					array_splice($allergyInteractionsItems, 0, 0);
					$allergyInteractionsData = [
						'title' => __('Allergy Interactions'),
						'items' => $allergyInteractionsItems
					];
					$productList['allergy_interactions_data'] = $allergyInteractionsData;
				}

				//INTERACTION WITH COMMON CONDITIONS
				/* $kidney = $product->getResource()->getAttribute('kidney')->getFrontend()->getValue($product); 
				  $pregnancy = $product->getResource()->getAttribute('pregnancy')->getFrontend()->getValue($product);
				  $high_blood_pressure = $product->getResource()->getAttribute('high_blood_pressure')->getFrontend()->getValue($product);
				  $low_blood_pressure = $product->getResource()->getAttribute('low_blood_pressure')->getFrontend()->getValue($product);
				  $diabetes = $product->getResource()->getAttribute('diabetes')->getFrontend()->getValue($product);
				  $liver = $product->getResource()->getAttribute('liver')->getFrontend()->getValue($product);
				  $lactation = $product->getResource()->getAttribute('lactation')->getFrontend()->getValue($product);
				  $oral_contraceptives = $product->getResource()->getAttribute('oral_contraceptives')->getFrontend()->getValue($product);
				  $children = $product->getResource()->getAttribute('children_safe')->getFrontend()->getValue($product);
				  $seniors = $product->getResource()->getAttribute('seniors')->getFrontend()->getValue($product); */


				$commonConditionsItems = null;
				for ($i = 0; $i < 11; $i++) {
					if ($i == 0) {
						if ($kidney != 'NA' && $kidney != '') {
							$commonConditionsItems[$i]['sub_title'] = $kidney . __(' for kidney');
							if ($kidney == 'Unknown') {
								$commonConditionsItems[$i]['description'] = sprintf($kidneytext, $productName, $productName);
								$commonConditionsItems[$i]['status'] = $kidney;
								$commonConditionsItems[$i]['attribute_code'] = 'kidney';
							} else {
								$commonConditionsItems[$i]['description'] = sprintf($kidneytext, $productName);
								$commonConditionsItems[$i]['status'] = $kidney;
								$commonConditionsItems[$i]['attribute_code'] = 'kidney';
							}
						}
					} elseif ($i == 1) {
						if ($pregnancy != 'NA' && $pregnancy != '') {
							$commonConditionsItems[$i]['sub_title'] = $pregnancy . __(' for pregnancy');
							if ($pregnancy == 'Unknown') {
								$commonConditionsItems[$i]['description'] = sprintf($pregnancytext, $productName, $productName);
								$commonConditionsItems[$i]['status'] = $pregnancy;
								$commonConditionsItems[$i]['attribute_code'] = 'pregnancy';
							} else {
								$commonConditionsItems[$i]['description'] = sprintf($pregnancytext, $productName);
								$commonConditionsItems[$i]['status'] = $pregnancy;
								$commonConditionsItems[$i]['attribute_code'] = 'pregnancy';
							}
						}
					} elseif ($i == 2) {
						if ($high_blood_pressure != 'NA' && $high_blood_pressure != '') {
							$commonConditionsItems[$i]['sub_title'] = $high_blood_pressure . __(' for high blood pressure');
							if ($high_blood_pressure == 'Unknown') {
								$commonConditionsItems[$i]['description'] = sprintf($high_blood_pressuretext, $productName, $productName);
								$commonConditionsItems[$i]['status'] = $high_blood_pressure;
								$commonConditionsItems[$i]['attribute_code'] = 'high_blood_pressure';
							} else {
								$commonConditionsItems[$i]['description'] = sprintf($high_blood_pressuretext, $productName);
								$commonConditionsItems[$i]['status'] = $high_blood_pressure;
								$commonConditionsItems[$i]['attribute_code'] = 'high_blood_pressure';
							}
						}
					} elseif ($i == 3) {
						if ($low_blood_pressure != 'NA' && $low_blood_pressure != '') {
							$commonConditionsItems[$i]['sub_title'] = $low_blood_pressure . __(' for low blood pressure');
							if ($low_blood_pressure == 'Unknown') {
								$commonConditionsItems[$i]['description'] = sprintf($low_blood_pressuretext, $productName, $productName);
								$commonConditionsItems[$i]['status'] = $low_blood_pressure;
								$commonConditionsItems[$i]['attribute_code'] = 'low_blood_pressure';
							} else {
								$commonConditionsItems[$i]['description'] = sprintf($low_blood_pressuretext, $productName);
								$commonConditionsItems[$i]['status'] = $low_blood_pressure;
								$commonConditionsItems[$i]['attribute_code'] = 'low_blood_pressure';
							}
						}
					} elseif ($i == 4) {
						if ($diabetes != 'NA' && $diabetes != '') {
							$commonConditionsItems[$i]['sub_title'] = $diabetes . __(' for diabetes');
							if ($diabetes == 'Unknown') {
								$commonConditionsItems[$i]['description'] = sprintf($diabetestext, $productName, $productName);
								$commonConditionsItems[$i]['status'] = $diabetes;
								$commonConditionsItems[$i]['attribute_code'] = 'diabetes';
							} else {
								$commonConditionsItems[$i]['description'] = sprintf($diabetestext, $productName);
								$commonConditionsItems[$i]['status'] = $diabetes;
								$commonConditionsItems[$i]['attribute_code'] = 'diabetes';
							}
						}
					} elseif ($i == 5) {
						if ($liver != 'NA' && $liver != '') {
							$commonConditionsItems[$i]['sub_title'] = $liver . __(' for liver');
							if ($liver == 'Unknown') {
								$commonConditionsItems[$i]['description'] = sprintf($livertext, $productName, $productName);
								$commonConditionsItems[$i]['status'] = $liver;
								$commonConditionsItems[$i]['attribute_code'] = 'liver';
							} else {
								$commonConditionsItems[$i]['description'] = sprintf($livertext, $productName);
								$commonConditionsItems[$i]['status'] = $liver;
								$commonConditionsItems[$i]['attribute_code'] = 'liver';
							}
						}
					} elseif ($i == 6) {
						if ($lactation != 'NA' && $lactation != '') {
							$commonConditionsItems[$i]['sub_title'] = $lactation . __(' for lactation');
							if ($lactation == 'Unknown') {
								$commonConditionsItems[$i]['description'] = sprintf($lactationtext, $productName, $productName);
								$commonConditionsItems[$i]['status'] = $lactation;
								$commonConditionsItems[$i]['attribute_code'] = 'lactation';
							} else {
								$commonConditionsItems[$i]['description'] = sprintf($lactationtext, $productName);
								$commonConditionsItems[$i]['status'] = $lactation;
								$commonConditionsItems[$i]['attribute_code'] = 'lactation';
							}
						}
					} elseif ($i == 7) {
						if ($oral_contraceptives != 'NA' && $oral_contraceptives != '') {
							$commonConditionsItems[$i]['sub_title'] = $oral_contraceptives . __(' for oral contraceptives');
							if ($oral_contraceptives == 'Unknown') {
								$commonConditionsItems[$i]['description'] = sprintf($oral_contraceptivestext, $productName, $productName);
								$commonConditionsItems[$i]['status'] = $oral_contraceptives;
								$commonConditionsItems[$i]['attribute_code'] = 'oral_contraceptives';
							} else {
								$commonConditionsItems[$i]['description'] = sprintf($oral_contraceptivestext, $productName);
								$commonConditionsItems[$i]['status'] = $oral_contraceptives;
								$commonConditionsItems[$i]['attribute_code'] = 'oral_contraceptives';
							}
						}
					} elseif ($i == 8) {
						if ($children != 'NA' && $children != '') {
							$commonConditionsItems[$i]['sub_title'] = $children . __(' for children');
							if ($children == 'Unknown') {
								$commonConditionsItems[$i]['description'] = sprintf($childrentext, $productName, $productName);
								$commonConditionsItems[$i]['status'] = $children;
								$commonConditionsItems[$i]['attribute_code'] = 'children_safe';
							} else {
								$commonConditionsItems[$i]['description'] = sprintf($childrentext, $productName);
								$commonConditionsItems[$i]['status'] = $children;
								$commonConditionsItems[$i]['attribute_code'] = 'children_safe';
							}
						}
					} elseif ($i == 9) {
						if ($seniors != 'NA' && $seniors != '') {
							$commonConditionsItems[$i]['sub_title'] = $seniors . __(' for seniors');
							if ($seniors == 'Unknown') {
								$commonConditionsItems[$i]['description'] = sprintf($seniorstext, $productName, $productName);
								$commonConditionsItems[$i]['status'] = $seniors;
								$commonConditionsItems[$i]['attribute_code'] = 'seniors';
							} else {
								$commonConditionsItems[$i]['description'] = sprintf($seniorstext, $productName);
								$commonConditionsItems[$i]['status'] = $seniors;
								$commonConditionsItems[$i]['attribute_code'] = 'seniors';
							}
						}
					} elseif ($i == 10) {
						if ($heart != 'NA' && $heart != '') {
							$commonConditionsItems[$i]['sub_title'] = $heart . __(' for heart');
							if ($seniors == 'Unknown') {
								$commonConditionsItems[$i]['description'] = sprintf($hearttext, $productName, $productName);
								$commonConditionsItems[$i]['status'] = $heart;
								$commonConditionsItems[$i]['attribute_code'] = 'heart';
							} else {
								$commonConditionsItems[$i]['description'] = sprintf($hearttext, $productName);
								$commonConditionsItems[$i]['status'] = $heart;
								$commonConditionsItems[$i]['attribute_code'] = 'heart';
							}
						}
					}
				}

				if (count($commonConditionsItems) > 0) {
					array_splice($commonConditionsItems, 0, 0);
					$commonConditionsData = [
						'title' => __('Interaction With Common Conditions'),
						'items' => $commonConditionsItems
					];
					$productList['common_conditions_data'] = $commonConditionsData;
				}

				//WARNING
				/* $smoking = $product->getResource()->getAttribute('smoking')->getFrontend()->getValue($product); 
				  $alcohol = $product->getResource()->getAttribute('alcohol')->getFrontend()->getValue($product);
				  $tolerance = $product->getResource()->getAttribute('tolerance')->getFrontend()->getValue($product);
				  $vision = $product->getResource()->getAttribute('vision')->getFrontend()->getValue($product);
				  $overdose = $product->getResource()->getAttribute('overdose')->getFrontend()->getValue($product);
				  $driving = $product->getResource()->getAttribute('driving')->getFrontend()->getValue($product);
				  $exercise = $product->getResource()->getAttribute('exercise')->getFrontend()->getValue($product);
				  $operating_heavy_machinery = $product->getResource()->getAttribute('operating_heavy_machinery')->getFrontend()->getValue($product); */


				$warningItems = null;
				for ($i = 0; $i < 9; $i++) {
					if ($i == 0) {
						if ($smoking != 'NA' && $smoking != '') {
							$warningItems[$i]['sub_title'] = __('Smoking');
							if ($smoking == 'Unknown') {
								$warningItems[$i]['description'] = sprintf($smokingtext, $productName, $productName);
								$warningItems[$i]['status'] = $smoking;
								$warningItems[$i]['attribute_code'] = 'smoking';
							} else {
								$warningItems[$i]['description'] = sprintf($smokingtext, $productName);
								$warningItems[$i]['status'] = $smoking;
								$warningItems[$i]['attribute_code'] = 'smoking';
							}
						}
					} elseif ($i == 1) {
						if ($alcohol != 'NA' && $alcohol != '') {
							$warningItems[$i]['sub_title'] = __('Alchol');
							if ($alcohol == 'Unknown') {
								$warningItems[$i]['description'] = sprintf($alcoholtext, $productName, $productName);
								$warningItems[$i]['status'] = $alcohol;
								$warningItems[$i]['attribute_code'] = 'alcohol';
							} else {
								$warningItems[$i]['description'] = sprintf($alcoholtext, $productName);
								$warningItems[$i]['status'] = $alcohol;
								$warningItems[$i]['attribute_code'] = 'alcohol';
							}
						}
					} elseif ($i == 2) {
						if ($tolerance != 'NA' && $tolerance != '') {
							$warningItems[$i]['sub_title'] = __('Tolerance');
							if ($tolerance == 'Unknown') {
								$warningItems[$i]['description'] = sprintf($tolerancetext, $productName, $productName);
								$warningItems[$i]['status'] = $tolerance;
								$warningItems[$i]['attribute_code'] = 'tolerance';
							} else {
								$warningItems[$i]['description'] = sprintf($tolerancetext, $productName);
								$warningItems[$i]['status'] = $tolerance;
								$warningItems[$i]['attribute_code'] = 'tolerance';
							}
						}
					} elseif ($i == 3) {
						if ($vision != 'NA' && $vision != '') {
							$warningItems[$i]['sub_title'] = __('Vision');
							if ($vision == 'Unknown') {
								$warningItems[$i]['description'] = sprintf($visiontext, $productName, $productName);
								$warningItems[$i]['status'] = $vision;
								$warningItems[$i]['attribute_code'] = 'vision';
							} else {
								$warningItems[$i]['description'] = sprintf($visiontext, $productName);
								$warningItems[$i]['status'] = $vision;
								$warningItems[$i]['attribute_code'] = 'vision';
							}
						}
					} elseif ($i == 4) {
						if ($overdose != 'NA' && $overdose != '') {
							$warningItems[$i]['sub_title'] = __('Overdose');
							if ($overdose == 'Unknown') {
								$warningItems[$i]['description'] = sprintf($overdosetext, $productName, $productName);
								$warningItems[$i]['status'] = $overdose;
								$warningItems[$i]['attribute_code'] = 'overdose';
							} else {
								$warningItems[$i]['description'] = sprintf($overdosetext, $productName);
								$warningItems[$i]['status'] = $overdose;
								$warningItems[$i]['attribute_code'] = 'overdose';
							}
						}
					} elseif ($i == 5) {
						if ($driving != 'NA' && $driving != '') {
							$warningItems[$i]['sub_title'] = __('Driving');
							if ($driving == 'Unknown') {
								$warningItems[$i]['description'] = sprintf($drivingtext, $productName, $productName);
								$warningItems[$i]['status'] = $driving;
								$warningItems[$i]['attribute_code'] = 'driving';
							} else {
								$warningItems[$i]['description'] = sprintf($drivingtext, $productName);
								$warningItems[$i]['status'] = $driving;
								$warningItems[$i]['attribute_code'] = 'driving';
							}
						}
					} elseif ($i == 6) {
						if ($exercise != 'NA' && $exercise != '') {
							$warningItems[$i]['sub_title'] = __('Exercise');
							if ($exercise == 'Unknown') {
								$warningItems[$i]['description'] = sprintf($exercisetext, $productName, $productName);
								$warningItems[$i]['status'] = $exercise;
								$warningItems[$i]['attribute_code'] = 'exercise';
							} else {
								$warningItems[$i]['description'] = sprintf($exercisetext, $productName);
								$warningItems[$i]['status'] = $exercise;
								$warningItems[$i]['attribute_code'] = 'exercise';
							}
						}
					} elseif ($i == 7) {
						if ($operating_heavy_machinery != 'NA' && $operating_heavy_machinery != '') {
							$warningItems[$i]['sub_title'] = __('Operating Heavy Machinery');
							if ($operating_heavy_machinery == 'Unknown') {
								$warningItems[$i]['description'] = sprintf($operating_heavy_machinerytext, $productName, $productName);
								$warningItems[$i]['status'] = $operating_heavy_machinery;
								$warningItems[$i]['attribute_code'] = 'operating_heavy_machinery';
							} else {
								$warningItems[$i]['description'] = sprintf($operating_heavy_machinerytext, $productName);
								$warningItems[$i]['status'] = $operating_heavy_machinery;
								$warningItems[$i]['attribute_code'] = 'operating_heavy_machinery';
							}
						}
					} elseif ($i == 8) {
						if ($caffeine != 'NA' && $caffeine != '') {
							$warningItems[$i]['sub_title'] = __('Caffeine');
							if ($caffeine == 'Unknown') {
								$warningItems[$i]['description'] = sprintf($caffeinetext, $productName, $productName);
								$warningItems[$i]['status'] = $caffeine;
								$warningItems[$i]['attribute_code'] = 'caffeine';
							} else {
								$warningItems[$i]['description'] = sprintf($caffeinetext, $productName);
								$warningItems[$i]['status'] = $caffeinetext;
								$warningItems[$i]['attribute_code'] = 'caffeine';
							}
						}
					}
				}

				if (count($warningItems) > 0) {
					array_splice($warningItems, 0, 0);
					$warningsData = [
						'title' => __('Warnings'),
						'items' => $warningItems
					];
					$productList['warnings_data'] = $warningsData;
				}
			}
			
            //FREQUENTLY ASKED QUESTIONS
            //$frequentQuestionsItems = $product->getResource()->getAttribute('zoylo_faq')->getFrontend()->getValue($product);
            $frequentQuestionsData = [
                'title' => __('Frequently Asked Questions'),
                'items' => $frequentQuestions
            ];
            //if(count($frequentQuestionsItems) > 0){
            $productList['frequent_questions_data'] = $frequentQuestionsData;
            //}
            //Doctor's Advice
            $doctorAdviceItems = null;
            $doctorAdviceData = [
                'title' => __("Doctor's Advice"),
                'items' => $doctorAdviceItems
            ];
            //if(count($doctorAdviceItems) > 0){
            $productList['doctor_advice_data'] = $doctorAdviceData;
            //}

            $productData[] = $productList;

            $data = [
                'code' => 120,
                'success' => 'true',
                'message' => $message,
                'product_data' => $productData
            ];
            $result = $this->productInfoInterfaceFactory->create();
            $result->setData($data);
        } catch (NoSuchEntityException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 120,
                'success' => 'false',
                'message' => $message,
                'product_data' => ''
            ];
            $result = $this->productInfoInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 120,
                'success' => 'false',
                'message' => $message,
                'product_data' => ''
            ];
            $result = $this->productInfoInterfaceFactory->create();
            $result->setData($data);
        }
        return $result;
    }

    /**
     * Return Product Filter collection
     * @api
     * @param int $cartId
     * @param string $query
     * @return \Zoylo\Webservices\Api\Data\ProductFiltersInterface containing Filter objects
     * @throws NoSuchEntityException
     */
    public function getFilters($catId = null, $query = null) {
        if ($catId == null && $query == null) {
            $message = __('Please search with either category Id or quesry string');
            $data = [
                'code' => 200,
                'success' => 'false',
                'message' => $message,
                'filters_data' => ''
            ];
            $result = $this->productFiltersInterfaceFactory->create();
            $result->setData($data);
        } else {
            try {
                //Filter By Cat Id
                $categoryId = intval($catId);
                $message = __('Product Filter Collection');
                if ($categoryId && $query == null) {
                    $filterableAttributes = $this->objectManager->get(\Magento\Catalog\Model\Layer\Category\FilterableAttributeList::class);
                    $layerResolver = $this->objectManager->get(\Magento\Catalog\Model\Layer\Resolver::class);
                    $filterList = $this->objectManager->create(
                            \Magento\Catalog\Model\Layer\FilterList::class, [
                        'filterableAttributes' => $filterableAttributes
                            ]
                    );
                    $layer = $layerResolver->get();
                    $layer->setCurrentCategory($categoryId);
                    $filterAttributes = $filterList->getFilters($layer);

                    $category = $this->categoryRepository->get($categoryId);
                    $filterArray = [];
                    $filterArray['category_id'] = $categoryId;
                    $i = 0;

                    foreach ($filterAttributes as $filter) {
                        $attributeLabel = (string) $filter->getName();
                        $attributeCode = (string) $filter->getRequestVar();
                        if ($attributeCode == "price") {
                            $filterCondition = 'from';
                            $filterName = $attributeCode;
                        } else {
                            $filterCondition = 'in';
                            $filterName = $attributeCode;
                        }

                        $items = $filter->getItems();
                        $filterValues = [];

                        $j = 0;

                        foreach ($items as $item) {
                            if ($attributeCode == 'cat') {
                                $attributeCode = 'category_id';
                                $filterName = $attributeCode;
                                $filterValues[$j]['display'] = strip_tags($item->getLabel());
                                $filterValues[$j]['value'] = $item->getValue();
                                $filterValues[$j]['count'] = $item->getCount();
                            } elseif ($category->getIsAnchor()) {
                                /* if ($filter->getAttributeModel()
                                  && $filter->getAttributeModel()->getFrontendInput() == 'price') {
                                  $filterValues[$j]['min_price'] = $filter->getLayer()->getProductCollection()->getMinPrice();
                                  $filterValues[$j]['max_price'] = $filter->getLayer()->getProductCollection()->getMaxPrice();
                                  break;
                                  } */

                                if ($item->getValue() == "-1000")
                                    $optionvalue = "0-1000";
                                elseif (strpos(strip_tags($item->getLabel()), 'and above') !== false)
                                    $optionvalue = $item->getValue() . "10000000000";
                                else
                                    $optionvalue = $item->getValue();

                                $filterValues[$j]['display'] = strip_tags($item->getLabel());
                                $filterValues[$j]['value'] = $optionvalue;
                                $filterValues[$j]['count'] = $item->getCount();
                            }
                            $j++;
                        }

                        if (!empty($filterValues)) {
                            $object['attribute_code'] = $attributeCode;
                            $object['label'] = $attributeLabel;
                            $object['filter_condition'] = $filterCondition;
                            $object['filter_name'] = $filterName;
                            $object['values'] = $filterValues;
                            $filterArray['available_filter'][] = $object;
                        }
                        $i++;
                    }
                    $success = 1;
                    $filtersData[] = $filterArray;
                } elseif ($query && $categoryId == null) {

                    //Filter By Query
                    $pageSize = '';
                    $pageLimit = '';
                    $searchDataModel = $this->searchManagerFactory->create();
                    $productsIDs = $searchDataModel->searchProductsFullText($query, $pageSize, $pageLimit);
                    $filterArray = [];
                    $filterArray['query'] = $query;

                    if (count($productsIDs) > 0) {

                        $productCollection = $this->productFactory->create()
                                ->addAttributeToSelect('*')
                                ->addAttributeToFilter('entity_id', array('in' => $productsIDs))
                                ->load();

                        $filterableAttributes = $this->objectManager->get(\Magento\Catalog\Model\Layer\Category\FilterableAttributeList::class);
                        $layerResolver = $this->objectManager->get(\Magento\Catalog\Model\Layer\Resolver::class);
                        $filterList = $this->objectManager->create(
                                \Magento\Catalog\Model\Layer\FilterList::class, [
                            'filterableAttributes' => $filterableAttributes
                                ]
                        );
                        $layer = $layerResolver->get();
                        $layer->setProductCollection($productCollection);
                        $filterAttributes = $filterList->getFilters($layer);

                        $i = 0;

                        foreach ($filterAttributes as $filter) {
                            $attributeLabel = (string) $filter->getName();
                            $attributeCode = (string) $filter->getRequestVar();
                            if ($attributeCode == "price") {
                                $filterCondition = 'from';
                                $filterName = $attributeCode;
                            } else {
                                $filterCondition = 'in';
                                $filterName = $attributeCode;
                            }

                            $items = $filter->getItems();
                            $filterValues = [];

                            $j = 0;

                            foreach ($items as $item) {
                                if ($attributeCode == 'cat') {
                                    $attributeCode = 'category_id';
                                    $filterName = $attributeCode;
                                    $filterValues[$j]['display'] = strip_tags($item->getLabel());
                                    $filterValues[$j]['value'] = $item->getValue();
                                    $filterValues[$j]['count'] = $item->getCount();
                                } else {
                                    /* if ($filter->getAttributeModel()
                                      && $filter->getAttributeModel()->getFrontendInput() == 'price') {
                                      $filterValues[$j]['min_price'] = $filter->getLayer()->getProductCollection()->getMinPrice();
                                      $filterValues[$j]['max_price'] = $filter->getLayer()->getProductCollection()->getMaxPrice();
                                      break;
                                      } */

                                    if ($item->getValue() == "-1000")
                                        $optionvalue = "0-1000";
                                    elseif (strpos(strip_tags($item->getLabel()), 'and above') !== false)
                                        $optionvalue = $item->getValue() . "10000000000";
                                    else
                                        $optionvalue = $item->getValue();

                                    $filterValues[$j]['display'] = strip_tags($item->getLabel());
                                    $filterValues[$j]['value'] = $optionvalue;
                                    $filterValues[$j]['count'] = $item->getCount();
                                }
                                $j++;
                            }

                            if (!empty($filterValues)) {
                                $object['attribute_code'] = $attributeCode;
                                $object['label'] = $attributeLabel;
                                $object['filter_condition'] = $filterCondition;
                                $object['filter_name'] = $filterName;
                                $object['values'] = $filterValues;
                                $filterArray['available_filter'][] = $object;
                            }
                            $i++;
                        }
                    } else {
                        $message = __('Please Search with different keyword');
                        $filterArray['items_count'] = 0;
                    }
                    $success = 1;
                    $filtersData[] = $filterArray;
                } else {
                    $success = 0;
                }

                if ($success == 1) {

                    $data = [
                        'code' => 200,
                        'success' => 'true',
                        'message' => $message,
                        'filters_data' => $filtersData
                    ];
                } else {
                    $message = __('Please search with only category Id or quesry string');
                    $data = [
                        'code' => 200,
                        'success' => 'false',
                        'message' => $message,
                        'filters_data' => ''
                    ];
                }

                $result = $this->productFiltersInterfaceFactory->create();
                $result->setData($data);
            } catch (NoSuchEntityException $e) {
                $message = $e->getMessage();
                $data = [
                    'code' => 200,
                    'success' => 'false',
                    'message' => $message,
                    'filters_data' => ''
                ];
                $result = $this->productFiltersInterfaceFactory->create();
                $result->setData($data);
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $data = [
                    'code' => 200,
                    'success' => 'false',
                    'message' => $message,
                    'filters_data' => ''
                ];
                $result = $this->productFiltersInterfaceFactory->create();
                $result->setData($data);
            }
        }
        return $result;
    }

    /**
     * Return Product Search Result
     * @api
     * @param mixed $searchData
     * @return \Zoylo\Webservices\Api\Data\ProductSearchInterface containing Filter objects
     * @throws NoSuchEntityException
     */
    public function getSearchList($searchData) {
        $catId = $searchData['category_id'];
        $query = $searchData['query'];
        $productBrand = $searchData['product_brand'];

        if ($catId == null && $query == null && $productBrand == null) {
            $message = __('Please search with either category Id or quesry string or OTC Brand');
            $data = [
                'code' => 210,
                'success' => 'false',
                'message' => $message,
                'search_result_data' => ''
            ];
            $result = $this->productSearchInterfaceFactory->create();
            $result->setData($data);
        } else {
            try {

                $categoryId = intval($catId);
                $productBrand = intval($productBrand);
                if ($categoryId && $query == null && $productBrand == null) {
                    //Search By Cat Id
                    $searchDataModel = $this->searchManagerFactory->create();
                    $resultList = $searchDataModel->getSearchRelatedItems($searchData);
                } elseif ($query && $categoryId == null && $productBrand == null) {
                    //Search By Query
                    $searchDataModel = $this->searchManagerFactory->create();
                    $resultList = $searchDataModel->getSearchRelatedItems($searchData);
                } elseif ($productBrand && $query == null && $categoryId == null) {
                    //Search By Query
                    $searchDataModel = $this->searchManagerFactory->create();
                    $resultList = $searchDataModel->getSearchRelatedItems($searchData);
                }

                $message = __('Products for searched category id or query or OTC Brand');
                $data = [
                    'code' => 210,
                    'success' => 'true',
                    'message' => $message,
                    'search_result_data' => $resultList
                ];
                $result = $this->productSearchInterfaceFactory->create();
                $result->setData($data);
            } catch (NoSuchEntityException $e) {
                $message = $e->getMessage();
                $data = [
                    'code' => 210,
                    'success' => 'false',
                    'message' => $message,
                    'search_result_data' => ''
                ];
                $result = $this->productSearchInterfaceFactory->create();
                $result->setData($data);
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $data = [
                    'code' => 210,
                    'success' => 'false',
                    'message' => $message,
                    'search_result_data' => ''
                ];
                $result = $this->productSearchInterfaceFactory->create();
                $result->setData($data);
            }
        }
        return $result;
    }

    /**
     * Return Zipcode Availability
     * @api
     * @param int $zipcode
     * @return \Zoylo\Webservices\Api\Data\ZipcodeSearchInterface containing Availability objects
     * @throws NoSuchEntityException
     */
    public function zipcodeChecker($zipcode = null) {
        if ($zipcode == null) {
            $message = __('Please search with zipcode');
            $data = [
                'code' => 220,
                'success' => 'false',
                'message' => $message
            ];
            $result = $this->zipcodeSearchInterfaceFactory->create();
            $result->setData($data);
        } else {
            try {
                $zipcode = intval($zipcode);
                if ($zipcode) {
                    //Search By Zipcode
                    if ($this->customHelperData->getConfig('carriers/ecomexpress/active') != "0") {

                        $to_country = "IN";
                        $allowed = false;
                        if (!$this->customHelperData->getConfig('carriers/ecomexpress/sallowspecific')) {
                            $allowed = true;
                        } else {
                            $allowed_country = explode(',', $this->customHelperData->getConfig('carriers/ecomexpress/specificcountry'));
                            $allowed = in_array($to_country, $allowed_country) == true ? true : false;
                        }
                        $picodeModel = $this->pincodeFactory->create();
                        $picodeCollection = $picodeModel->getCollection()
                                ->addFieldToFilter('pincode', array('eq' => $zipcode));
                        if ($allowed && sizeof($picodeCollection) > 0) {
                            $message = $this->customHelperData->getConfig('checkDelivery/configvalues/success_message');
                            $data = [
                                'code' => 220,
                                'success' => 'true',
                                'message' => $message,
                            ];
                            $result = $this->zipcodeSearchInterfaceFactory->create();
                            $result->setData($data);
                        } else {
                            throw new NoSuchEntityException(__('Requested zipcode doesn\'t exist'));
                        }
                    } else {
                        throw new NoSuchEntityException(__('Requested zipcode doesn\'t exist'));
                    }
                }
            } catch (NoSuchEntityException $e) {
                $message = $e->getMessage();
                $data = [
                    'code' => 220,
                    'success' => 'false',
                    'message' => $message
                ];
                $result = $this->zipcodeSearchInterfaceFactory->create();
                $result->setData($data);
            } catch (\Exception $e) {
                $message = $e->getMessage();
                $data = [
                    'code' => 220,
                    'success' => 'false',
                    'message' => $message
                ];
                $result = $this->zipcodeSearchInterfaceFactory->create();
                $result->setData($data);
            }
        }
        return $result;
    }

    /**
     * Return Coupons List
     * @api
     * @return \Zoylo\Webservices\Api\Data\CouponsListInterface containing Coupons List objects
     * @throws NoSuchEntityException
     */
    public function getCouponsList() {

        try {

            $currentTime = $this->stdTimezone->date()->format('Y-m-d');
            $couponsCollection = $this->_salesRuleCoupon->create()
                            ->addFieldToFilter('is_active', 1)
                            ->addFieldToFilter('show_in_frontend', 1)
                            ->addFieldToFilter('to_date', ['gteq' => $currentTime])->setPageSize(2);
            $couponsCollection->getSelect()->group('code');
            /* $product = $this->_coreRegistry->registry('product');

              foreach ($rules as $rule) {
              try {
              if ($rule->getActions()->validate($product)) {
              $activeRules[] = $rule;
              }
              } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
              continue;
              }
              }
              $rulesApplied = "<h3>Applicable Offers:</h3>";
              foreach ($rules as $rule) {
              $rulesApplied .= $rule->getName()."</br>";
              }
              return $rulesApplied; */

            if (count($couponsCollection) > 0) {
                foreach ($couponsCollection as $coupon) {
                    $couponsList[] = [
                        'name' => $coupon->getName(),
                        'code' => $coupon->getCode(),
                        'description' => $coupon->getDescription(),
                        'from_date' => $coupon->getFromDate(),
                        'to_date' => $coupon->getToDate()
                    ];
                }

                $message = __('List of Coupons');
                $data = [
                    'code' => 260,
                    'success' => 'false',
                    'message' => $message,
                    'coupons_data' => $couponsList
                ];
                $result = $this->couponsListInterfaceFactory->create();
                $result->setData($data);
            } else {
                throw new NoSuchEntityException(__('No coupons are available'));
            }
        } catch (NoSuchEntityException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 260,
                'success' => 'false',
                'message' => $message,
                'coupons_data' => ''
            ];
            $result = $this->couponsListInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 260,
                'success' => 'false',
                'message' => $message,
                'coupons_data' => ''
            ];
            $result = $this->couponsListInterfaceFactory->create();
            $result->setData($data);
        }
        return $result;
    }

    /**
     * Save/Update customer address.
     *
     * @param \Magento\Customer\Api\Data\AddressInterface $address
     * @return \Magento\Customer\Api\Data\AddressInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function updateAddresses(\Magento\Customer\Api\Data\AddressInterface $address) {
        $addressModel = null;
        $customerModel = $this->customerRegistry->retrieve($address->getCustomerId());
        if ($address->getId()) {
            $addressModel = $this->addressRegistry->retrieve($address->getId());
        }

        if ($addressModel === null) {
            /** @var \Magento\Customer\Model\Address $addressModel */
            $addressModel = $this->addressFactory->create();
            $addressModel->updateData($address);
            $addressModel->setCustomer($customerModel);
        } else {
            $addressModel->updateData($address);
        }
        $addressModel->setStoreId($customerModel->getStoreId());

        $errors = $addressModel->validate();
        if ($errors !== true) {
            $inputException = new InputException();
            foreach ($errors as $error) {
                $inputException->addError($error);
            }
            throw $inputException;
        }
        $addressModel->save();
        $address->setId($addressModel->getId());
        // Clean up the customer registry since the Address save has a
        // side effect on customer : \Magento\Customer\Model\ResourceModel\Address::_afterSave
        $this->addressRegistry->push($addressModel);
        $this->updateAddressCollection($customerModel, $addressModel);

        return $addressModel->getDataModel();
    }

    /**
     * @param Customer $customer
     * @param Address $address
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    private function updateAddressCollection(CustomerModel $customer, CustomerAddressModel $address) {
        $customer->getAddressesCollection()->removeItemByKey($address->getId());
        $customer->getAddressesCollection()->addItem($address);
    }

    /**
     * Return Country collection
     * @api
     * @return \Zoylo\Webservices\Api\Data\CountriesInfoInterface containing homePage objects
     */
    public function getCountriesInfo() {
        try {
            $message = "Countries Information";
            $countries = $this->countryInformationAcquirer->getCountriesInfo();

            foreach ($countries as $country) {
                // Get regions for this country:
                $regions = [];

                if ($availableRegions = $country->getAvailableRegions()) {
                    foreach ($availableRegions as $region) {
                        $regions[] = [
                            'id' => $region->getId(),
                            'code' => $region->getCode(),
                            'name' => $region->getName()
                        ];
                    }
                }

                // Add to countriesData:
                $countriesData[] = [
                    'value' => $country->getTwoLetterAbbreviation(),
                    'label' => __($country->getFullNameLocale()),
                    'regions' => $regions
                ];
            }
            $data = [
                'code' => 130,
                'success' => 'true',
                'message' => $message,
                'countries_data' => $countriesData
            ];
            $result = $this->countriesInfoInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 130,
                'success' => 'false',
                'message' => $message,
                'countries_data' => ''
            ];
            $result = $this->countriesInfoInterfaceFactory->create();
            $result->setData($data);
        }
        return $result;
    }

    /**
     * Return Regions collection
     * @api
     * @return \Zoylo\Webservices\Api\Data\CountriesInfoInterface containing homePage objects
     */
    public function getRegionsInfo() {
        try {
            $message = "Regions Information";
            $countries = $this->countryInformationAcquirer->getCountriesInfo();

            foreach ($countries as $country) {
                // Get regions for this country:
                $regions = [];

                if ($availableRegions = $country->getAvailableRegions()) {
                    foreach ($availableRegions as $region) {
                        $regions[] = [
                            'region_code' => $region->getCode(),
                            'region' => $region->getName(),
                            'region_id' => $region->getId()
                        ];
                    }
                }

                // Add to countriesData:
                $countriesData[] = [
                    'value' => $country->getTwoLetterAbbreviation(),
                    'label' => __($country->getFullNameLocale()),
                    'regions' => $regions
                ];
            }
            $data = [
                'code' => 310,
                'success' => 'true',
                'message' => $message,
                'countries_data' => $countriesData
            ];
            $result = $this->countriesInfoInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 310,
                'success' => 'false',
                'message' => $message,
                'countries_data' => ''
            ];
            $result = $this->countriesInfoInterfaceFactory->create();
            $result->setData($data);
        }
        return $result;
    }

    /**
     * Create customer account. Perform necessary business operations like sending email.
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @param string $password
     * @param string $redirectUrl
     * @return \Zoylo\Webservices\Api\Data\CustomerCreateInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createAccount(CustomerInterface $customer, $password = null, $redirectUrl = '') {
        $this->logger->info("Create Customer:");
        $mobileNumber = $customer->getCustomAttributes()['mobile_number']->getValue();
        $mobileNumber = substr($mobileNumber, -10);
        $customerCollection = $this->customerFactory->create()->getCollection()
                ->addAttributeToSelect("*")
                ->addAttributeToFilter("customer_number", array("eq" => $mobileNumber));
        if (count($customerCollection) > 0) {
            foreach ($customerCollection as $customer) {
                
            }
            $message = __('Customer Already Exists');
            $token = $this->tokenModelFactory->create()->createCustomerToken($customer->getId())->getToken();
            $data = [
                'code' => 140,
                'success' => 'false',
                'message' => $message,
                'token' => $token,
                'customer_data' => $customer
            ];
            $result = $this->customerCreateInterfaceFactory->create();
            $result->setData($data);
            return $result;
        }

        $this->logger->info("Email: " . $customer->getEmail());
        $this->logger->info("Firstname: " . $customer->getFirstname());
        $this->logger->info("Lastname: " . $customer->getLastname());
        $this->logger->info("Password: " . $password);
        $this->logger->info("Mobile Number: " . $mobileNumber);
        if ($password !== null) {
            //$this->checkPasswordStrength($passmword);
            $customerEmail = $customer->getEmail();
            try {
                $this->credentialsValidator->checkPasswordDifferentFromEmail($customerEmail, $password);
            } catch (InputException $e) {
                throw new LocalizedException(__('Password cannot be the same as email address.'));
            }
            $hash = $this->createPasswordHash($password);
        } else {
            $hash = null;
        }
        return $this->createAccountWithPasswordHash($customer, $hash, $redirectUrl);
    }

    /**
     * Create a hash for the given password
     *
     * @param string $password
     * @return string
     */
    protected function createPasswordHash($password) {
        return $this->encryptor->getHash($password, true);
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function createAccountWithPasswordHash(CustomerInterface $customer, $hash, $redirectUrl = '') {
        $mobileNumber = $customer->getCustomAttributes()['mobile_number']->getValue();
        $mobileNumber = substr($mobileNumber, -10);
        // This logic allows an existing customer to be added to a different store.  No new account is created.
        // The plan is to move this logic into a new method called something like 'registerAccountWithStore'
        if ($customer->getId()) {
            $customer = $this->customerRepository->get($customer->getEmail());
            $websiteId = $customer->getWebsiteId();

            if ($this->isCustomerInStore($websiteId, $customer->getStoreId())) {
                throw new InputException(__('This customer already exists in this store.'));
            }
            // Existing password hash will be used from secured customer data registry when saving customer
        }

        // Make sure we have a storeId to associate this customer with.
        if (!$customer->getStoreId()) {
            if ($customer->getWebsiteId()) {
                $storeId = $this->storeManager->getWebsite($customer->getWebsiteId())->getDefaultStore()->getId();
            } else {
                $storeId = $this->storeManager->getStore()->getId();
            }
            $customer->setStoreId($storeId);
        }

        // Associate website_id with customer
        if (!$customer->getWebsiteId()) {
            $websiteId = $this->storeManager->getStore($customer->getStoreId())->getWebsiteId();
            $customer->setWebsiteId($websiteId);
        }

        // Update 'created_in' value with actual store name
        if ($customer->getId() === null) {
            $storeName = $this->storeManager->getStore($customer->getStoreId())->getName();
            $customer->setCreatedIn($storeName);
        }

        $customerAddresses = $customer->getAddresses() ?: [];
        $customer->setAddresses(null);
        try {
            // If customer exists existing hash will be used by Repository
            $customer = $this->customerRepository->save($customer, $hash);
        } catch (AlreadyExistsException $e) {
            throw new InputMismatchException(
            __('A customer with the same email already exists in an associated website.')
            );
        } catch (LocalizedException $e) {
            throw $e;
        }
        try {
            foreach ($customerAddresses as $address) {
                if ($address->getId()) {
                    $newAddress = clone $address;
                    $newAddress->setId(null);
                    $newAddress->setCustomerId($customer->getId());
                    $this->addressRepository->save($newAddress);
                } else {
                    $address->setCustomerId($customer->getId());
                    $this->addressRepository->save($address);
                }
            }
            $this->customerRegistry->remove($customer->getId());
        } catch (InputException $e) {
            $this->customerRepository->delete($customer);
            throw $e;
        }
        $customer = $this->customerRepository->getById($customer->getId());
        $customerId = $customer->getId();
        $resource = $this->objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('customer_entity_varchar');

        $sql = "INSERT IGNORE INTO " . $tableName . " (attribute_id,value,entity_id) VALUES ( 182 , " . $mobileNumber . " , " . $customerId . " )";
        $connection->query($sql);

        $newLinkToken = $this->mathRandom->getUniqueHash();
        $this->changeResetPasswordLinkToken($customer, $newLinkToken);
        $this->sendEmailConfirmation($customer, $redirectUrl);

        $token = $this->tokenModelFactory->create()->createCustomerToken($customer->getId())->getToken();
        $message = __('Customer Create');
        $data = [
            'code' => 140,
            'success' => 'true',
            'message' => $message,
            'token' => $token,
            'customer_data' => $customer
        ];
        $result = $this->customerCreateInterfaceFactory->create();
        $result->setData($data);

        return $result;
    }

    /**
     * Change reset password link token
     *
     * Stores new reset password link token
     *
     * @param CustomerInterface $customer
     * @param string $passwordLinkToken
     * @return bool
     * @throws InputException
     */
    public function changeResetPasswordLinkToken($customer, $passwordLinkToken) {
        if (!is_string($passwordLinkToken) || empty($passwordLinkToken)) {
            throw new InputException(
            __(
                    'Invalid value of "%value" provided for the %fieldName field.', ['value' => $passwordLinkToken, 'fieldName' => 'password reset token']
            )
            );
        }
        if (is_string($passwordLinkToken) && !empty($passwordLinkToken)) {
            $customerSecure = $this->customerRegistry->retrieveSecureData($customer->getId());
            $customerSecure->setRpToken($passwordLinkToken);
            $customerSecure->setRpTokenCreatedAt(
                    $this->dateTimeFactory->create()->format(DateTime::DATETIME_PHP_FORMAT)
            );
            $this->customerRepository->save($customer);
        }
        return true;
    }

    /**
     * Send either confirmation or welcome email after an account creation
     *
     * @param CustomerInterface $customer
     * @param string $redirectUrl
     * @return void
     */
    protected function sendEmailConfirmation(CustomerInterface $customer, $redirectUrl) {
        try {
            $hash = $this->customerRegistry->retrieveSecureData($customer->getId())->getPasswordHash();
            $templateType = self::NEW_ACCOUNT_EMAIL_REGISTERED;
            if ($this->isConfirmationRequired($customer) && $hash != '') {
                $templateType = self::NEW_ACCOUNT_EMAIL_CONFIRMATION;
            } elseif ($hash == '') {
                $templateType = self::NEW_ACCOUNT_EMAIL_REGISTERED_NO_PASSWORD;
            }
            $this->getEmailNotification()->newAccount($customer, $templateType, $redirectUrl, $customer->getStoreId());
        } catch (MailException $e) {
            // If we are not able to send a new account email, this should be ignored
            $this->logger->info($e);
        } catch (\UnexpectedValueException $e) {
            $this->logger->info($e);
        }
    }

    /**
     * Get email notification
     *
     * @return EmailNotificationInterface
     * @deprecated 100.1.0
     */
    private function getEmailNotification() {
        if (!($this->emailNotification instanceof EmailNotificationInterface)) {
            return \Magento\Framework\App\ObjectManager::getInstance()->get(
                            EmailNotificationInterface::class
            );
        } else {
            return $this->emailNotification;
        }
    }

    /**
     * Check if accounts confirmation is required in config
     *
     * @param CustomerInterface $customer
     * @return bool
     * @deprecated
     * @see AccountConfirmation::isConfirmationRequired
     */
    protected function isConfirmationRequired($customer) {
        return $this->accountConfirmation->isConfirmationRequired(
                        $customer->getWebsiteId(), $customer->getId(), $customer->getEmail()
        );
    }

    /**
     * Set payment information for a specified cart.
     *
     * @param int $prescription_comment
     * @param int $payment_method
     * @param int $cartId
     * @param string $order_comment
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return \Zoylo\Webservices\Api\Data\OrderInterface containing Order objects
     */
    public function createOrder($cartId, $prescription_comment = null, $payment_method = null ,$order_comment) {
        try {
            $quote = $this->quoteRepository->getActive($cartId);
			 //written by achyut start
			  $items = $quote->getAllItems();
			  $storagetypeCheck = 0;
			  foreach ($items  as $item) 
			{		
				$sku = $item->getSku();
				$product = $this->_productRepository->get($sku);
				$storagetype = $product->getResource()->getAttribute('storage_type')->getFrontend()->getValue($product);
				
				if($storagetype == 'Cold Storage'){
					$storagetypeCheck = 1;
					$name = $product->getName();
				}
			  
			} 

			    $shippingAddress = $quote->getShippingAddress();
				$to_zipcode = $shippingAddress->getPostcode();
				if($storagetypeCheck == 1){	
				$serviceblemodel = $this->objectManager->get('I95Dev\Serviceablepincodes\Model\DataPincodes')->getCollection()->addFieldToFilter('pincode', array('eq' => $to_zipcode));
				if($serviceblemodel->count() <= 0){
					$message = 'Your Pincode is not serviceble for the item '.$name;
                    throw new CouldNotSaveException(__($message));
				}
				
				}
             //written by achyut end  
            if ($payment_method == 0)
                $quote->getPayment()->setMethod('cashondelivery');
            elseif($payment_method == 1)
                $quote->getPayment()->setMethod('paylater');
            $quote->setOrigin('MobileApp');
            $this->quoteRepository->save($quote);

            $order = $this->quoteManagement->submit($quote);
            if (null == $order) {
                throw new CouldNotSaveException(
                __('An error occurred on the server. Please try to place the order again.')
                );
            }
            $addComment = 0;
            if ($prescription_comment == 1) {
                $comment = __('I will submit at the time of delivery');
                $order->addStatusHistoryComment($comment);
                $addComment = 1;
            }

            if($order_comment != '' && $order_comment != null){
                $order->addStatusHistoryComment($order_comment);
                $addComment = 1;
            }
                
            if($addComment ==1){
                $order->setIsCustomerNotified(false)->setEntityName('order')->save();
            }
            
            $orderId = $order->getId();
            if ($payment_method == 0 || $payment_method == 1) {
                $prescriptionValues = $order->getData('prescription_values');
                $orderData = $order;
            } else {
                $orderData = '';
                $prescriptionValues = '';
            }

            $customerId = $order->getCustomerId();
            if($customerId!=''){
                $orderpageHelper = $this->objectManager->get('I95Dev\Orderpage\Helper\Data');
                $orderpageHelper->moveWishlistItemsToCart($customerId);
            }

            $incrementId = $order->getIncrementId();
            $grandTotal = $order->getGrandTotal();
            $message = __('Order Info');
            $data = [
                'code' => 270,
                'success' => 'true',
                'message' => $message,
                'order_id' => $orderId,
                'increment_id' => $incrementId,
                'grand_total' => $grandTotal,
                'payment_method' => $payment_method,
                'payment_status' => '',
                'prescription_values' => $prescriptionValues,
                'order_info' => $orderData
            ];
            $result = $this->orderInterfaceFactory->create();
            $result->setData($data);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            throw new CouldNotSaveException(
            __($e->getMessage()), $e
            );
        } catch (\Exception $e) {
            $this->logger->info($e);
            throw new CouldNotSaveException(
            __('An error occurred on the server. Please try to place the order again.'), $e
            );
        } catch (CouldNotSaveException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 270,
                'success' => 'false',
                'message' => $message,
                'order_id' => '',
                'increment_id' => '',
                'grand_total' => '',
                'payment_method' => '',
                'payment_status' => '',
                'prescription_values' => '',
                'order_info' => ''
            ];
            $result = $this->orderInterfaceFactory->create();
            $result->setData($data);
        }
        return $result;
    }

    /**
     * Update order information.
     *
     * @param mixed $orderData
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return \Zoylo\Webservices\Api\Data\UpdateOrderInterface containing Update Order objects
     */
    public function updateOrder($orderData) {
        try {
            $id             = $orderData['id'];
            $order          = $this->objectManager->create('\Magento\Sales\Model\Order')->loadByIncrementId($id);
            $paymentMethod  = $orderData['payment_method'];
            $transactionId  = $orderData['transaction_id'];
            $statusText     = $orderData['status'];
            $paymentComment = $orderData['payment_commet'];
            $status         = 0;
            if ($paymentMethod == "paytm") {
                $status = 1;
                $paymentData = "Paytm";
                if($paymentComment != '')
                    $paymentComment = $paymentComment.' '. $paymentData;
                else
                    $paymentComment = $paymentData;
            } elseif ($paymentMethod == "razorpay") {
                $paymentData = "Razorpay";
            } elseif ($paymentMethod == "zoylopaymentmethod") {
                $paymentData = "Zoylo Wallet";
            }elseif ($paymentMethod == "paypal") {
                $paymentMethod = "custompaypal";
                $paymentData   = "Paypal";
            }
            $scopeConfig        = $this->objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');
            $accessToken        = $scopeConfig->getValue('juspay_settings/zoylo/access_token', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $apiUrl             = $scopeConfig->getValue('juspay_settings/zoylo/url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE).$id;
            if($accessToken != '' && $apiUrl != ''){
                try{
                    if ($paymentMethod != "paytm") {
                        $ch = curl_init($apiUrl); 
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                        curl_setopt($ch, CURLOPT_USERPWD, $accessToken);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                        $data = curl_exec($ch);
                        $result = json_decode($data,true);
                        //print_r($result);die;
                        if($result['status_id'] ==21 && $result['status'] == "CHARGED")
                            $status = 1;
                        $transactionId      = $result['txn_id'];
                        $paymentData = $paymentData.' '.$result['payment_method'];
                        if($paymentComment != '')
                            $paymentComment = $paymentComment.' '. $paymentData;
                        else
                            $paymentComment = $paymentData;
                    }
                    if ($status == 0 && $id != null && $id != '') {
                        $order->setStatus('payment_failed_revise_payment_cs');
                        $statusText = 'cancel';
                        $order->setPaymentStatus('failed');
                    } elseif ($status == 1 && $id != null && $id != '') {
                        $order->setStatus('ready_for_packing_cs');
                        $statusText = 'success';
                        $order->setPaymentStatus('paid');
                    } else {
                        //$this->logger->info($orderData);
                        throw new CouldNotSaveException(
                        __('An error occurred on the server. Please try to place the order again.'), $e
                        );
                    }

                    $order->setOrigin('MobileApp');
                    // Prepare payment object
                    $payment = $order->getPayment();
                    $payment->setMethod($paymentMethod);
                    $payment->setLastTransId($transactionId);
                    $payment->setTransactionId($transactionId);
                    $payment->setAdditionalInformation([Transaction::RAW_DETAILS => $paymentData]);

                    // Formatted price
                    $formatedPrice = $order->getBaseCurrency()->formatTxt($order->getGrandTotal());
                    // Add transaction to payment
                    $payment->addTransactionCommentsToOrder($transactionId, __('The authorized amount is %1.', $formatedPrice));
                    $payment->setParentTransactionId(null);
                    // Save payment, transaction and order
                    $payment->save();

                    $order->addStatusHistoryComment($paymentComment);
                    $this->orderRepository->save($order);

                    if($statusText == 'success'){
                        if ($this->smshelperdata->isEnabled() && $this->smshelperdata->isOrderPlaceForUserEnabled()) {
                            $customer = $this->objectManager->create('Magento\Customer\Model\Customer')->load($order->getCustomerId());
                            $message = $this->smshelperdata->getOrderPlaceTemplateForUser();
                            $sms = sprintf($message, $order->getIncrementId());
                            $mobile = $customer->getCustomerNumber();
                            $this->smshelperapi->callApiUrl($mobile, $sms);
                        }
                    }
                    
                    $prescriptionValues = $order->getData('prescription_values');
                    $message = __('Order Info');
                    $data = [
                        'code' => 280,
                        'success' => 'true',
                        'message' => $message,
                        'status' => $statusText,
                        'prescription_values' => $prescriptionValues,
                        'order_info' => $order
                    ];
                    $result = $this->updateOrderInterfaceFactory->create();
                    $result->setData($data);

                }catch(\Exception $e){
                    $message = $e->getMessage();
                    $data = [
                        'code' => 280,
                        'success' => 'false',
                        'message' => $message,
                        'status' => '',
                        'prescription_values' => '',
                        'order_info' => ''
                    ];
                    $result = $this->updateOrderInterfaceFactory->create();
                    $result->setData($data);
                }
            }
            
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            throw new CouldNotSaveException(
            __($e->getMessage()), $e
            );
        } catch (\Exception $e) {
            $this->logger->info($e);
            throw new CouldNotSaveException(
            __('An error occurred on the server. Please try to place the order again.'), $e
            );
        } catch (CouldNotSaveException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 280,
                'success' => 'false',
                'message' => $message,
                'status' => '',
                'prescription_values' => '',
                'order_info' => ''
            ];
            $result = $this->updateOrderInterfaceFactory->create();
            $result->setData($data);
        }
        return $result;
    }
	
	/**
	* Cancel order.
     *
	 * @param int $customerId
     * @param mixed $orderData
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return \Zoylo\Webservices\Api\Data\CancelOrderInterface containing Update Order objects
     */
    public function cancelOrder($customerId, $orderData){
		try {
			if (!$customerId) {
                throw new Exception(__('Requested Customer doesn\'t exist'));
            }
			$incrementId = $orderData['id'];
			$cancelled_by = 'Customer';
			$cancellation_reason = $orderData['cancellation_reason'];
			$comment = 'Order cancelled by customer and the reason is '. $cancellation_reason;
            $order = $this->objectManager->create('\Magento\Sales\Model\Order')->loadByIncrementId($incrementId);
			$order->setCancellationReason($cancellation_reason);
			$order->setOrderCancelledBy($cancelled_by);
            $order->addStatusHistoryComment($comment);
			$order->cancel()->save();
			
		   //Code starts by Ajesh/
           $currentOrderId = $order->getId();
           $orderHistory = $this->objectManager->create('\Magento\Sales\Model\ResourceModel\Order\Status\History\Collection');
           $orderHistory->addFieldToFilter('parent_id', array('eq' => $currentOrderId));
           $orderHistory->addFieldToFilter('comment', array('like' => '%'.$comment.'%'));
           $orderSize = $orderHistory->getSize();
           $finalStatusHistoryId='';
           if($orderSize>0){
               $orderHistoryData = $orderHistory->getData();
                   foreach($orderHistoryData as $orderHistoryValue){
                       $finalStatusHistoryId = $orderHistoryValue['entity_id'];
                   }
                   if($finalStatusHistoryId!=''){
                       $orderStatus = "canceled"; //Canceled status
                       $orderHistoryObj = $this->objectManager->create('\Magento\Sales\Model\Order\Status\History')->load($finalStatusHistoryId);
                       $orderHistoryObj->setStatus($orderStatus);
                       $orderHistoryObj->save();
                   }
           }
           //Code ends by Ajesh/
		   
            //$this->orderRepository->save($order);
            $message = __('Order has been cancelled successfully');
            $data = [
                'code' => 340,
                'success' => 'true',
                'message' => $message
            ];
            $result = $this->cancelOrderInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 340,
                'success' => 'false',
                'message' => $message
            ];
            $result = $this->cancelOrderInterfaceFactory->create();
            $result->setData($data);
        } catch (NoSuchEntityException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 340,
                'success' => 'false',
                'message' => $message
            ];
            $result = $this->cancelOrderInterfaceFactory->create();
            $result->setData($data);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 340,
                'success' => 'false',
                'message' => $message
            ];
            $result = $this->cancelOrderInterfaceFactory->create();
            $result->setData($data);
        }
        return $result;
	}
        
        
    /**
    * Order Status Update.
    *
    * @param int $customerId
    * @param mixed $orderData
    * @throws \Magento\Framework\Exception\CouldNotSaveException
    * @return \Zoylo\Webservices\Api\Data\CancelOrderInterface containing Update Order objects
    */

    public function orderStatusUpdate($customerId, $orderData)
    {
        $allowedStatusNotifcations = array('return_approved_cs','return_declined_cs');
        
        $data = [];
        try{
            if (!$customerId) {
                throw new \Exception(__('Requested Customer doesn\'t exist'));
            }
            if (!isset($orderData['id']))
            {
                throw new \Exception(__('Order id is missing'));
            }
            if(!isset($orderData['order_status']))
            {
                throw new \Exception(__('order_status is missing'));
            }
            if(!isset($orderData['reason']))
            {
                throw new \Exception(__('Return reason is missing'));
            }
            if (isset($orderData['id']))
            {
                $orderLoad = $this->orderModel->create()->loadByIncrementId($orderData['id']);
                $orderLoad->setStatus($orderData['order_status']);
                $orderLoad->setOrderReturnReason($orderData['reason']);
                if ($orderLoad->getOrderReturnDateTime() == '')
                $orderLoad->setOrderReturnDateTime($this->prstDateTime->gmtDate());
                $orderLoad->save();
                $data = [
                    'code' => '200',
                    'success' => 'true',
                    'message' => 'Order Status Updated Successfully',
                    'orders_data' => ''
                ];
                if (isset($orderData['order_status']) && in_array($orderData['order_status'],$allowedStatusNotifcations) )
                {
                    $firstLineCont = "";
                    $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($orderLoad->getStatus());
                    $statusFrontendLabel = str_replace(' CS', '', $orderLoad->getStatusLabel());
                    $this->csquareHelper->sendEmail($orderLoad, $statusFrontendLabel, $orderLoad->getStatus(), $firstLineCont);
                    $this->csquareHelper->sendSMS($orderLoad, $statusFrontendLabel, $smsContent);                    
                }

            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 300,
                'success' => 'false',
                'message' => $message,
                'orders_data' => ''
            ];
        }
        $result = $this->cancelOrderInterfaceFactory->create();
        $result->setData($data);
        return $result; 
    }
    
     /**
     * Clickpost Status Update
     * 
     */
    
    public function clickpostStatusUpdate()
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/clickpost.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(print_r(file_get_contents('php://input'),true));
        
        $webHookResponse = file_get_contents('php://input');
        $webHookResponse = json_decode($webHookResponse);
        $logger->info("Way bill data::".$webHookResponse->waybill);
        if ($webHookResponse->additional->latest_status->clickpost_status_description && strtolower($webHookResponse->additional->latest_status->clickpost_status_description) == 'delivered' )
        {
            $clickPOrderLoad = $this->orderModel->create()->loadByIncrementId($webHookResponse->additional->latest_status->reference_number);
            $clickPOrderLoad->setStatus('order_delivered_cs');
            $clickPOrderLoad->save();
            $logger->info("Order with reference number:".$webHookResponse->additional->latest_status->reference_number." delivered.");
            $statusFrontendLabel = str_replace(' CS', '', $clickPOrderLoad->getStatusLabel());
            $firstLineCont = "";
            $this->csquareHelper->sendEmail($clickPOrderLoad, $statusFrontendLabel, $clickPOrderLoad->getStatus(), $firstLineCont);
            $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($clickPOrderLoad->getStatus());
            $this->csquareHelper->sendSMS($clickPOrderLoad, $statusFrontendLabel, $smsContent);        
        }
    }
	
    /**
     * Return the Customer orders collection.
     *
     * @param int $customerId
     * @param mixed $searchData
     * @return \Zoylo\Webservices\Api\Data\OrdersListInterface containing Orders objects
     */
    public function getOrdersList($customerId, $searchData) {
        try {
            if (!$customerId) {
                throw new Exception(__('Requested Customer doesn\'t exist'));
            }
            $searchDataModel = $this->searchManagerFactory->create();
            $resultList = $searchDataModel->getOrdersList($customerId, $searchData);
            $orderCount = $this->orderModel->create()->getCollection()->addFieldToSelect(
            '*'
        )->addFieldToFilter(
            'customer_id',
            $customerId
        )->setOrder(
            'created_at',
            'desc'
        );
$count = count($orderCount);
			//$count = count($resultList);
            $message = __('Orders Information');
            $data = [
                'code' => 300,
                'success' => 'true',
                'message' => $message,
				'orders_count' => $count,
                'orders_data' => $resultList
            ];
            $result = $this->ordersListInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 300,
                'success' => 'false',
                'message' => $message,
				'orders_count' => '',
                'orders_data' => ''
            ];
            $result = $this->ordersListInterfaceFactory->create();
            $result->setData($data);
        } catch (NoSuchEntityException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 300,
                'success' => 'false',
                'message' => $message,
				'orders_count' => '',
                'orders_data' => ''
            ];
            $result = $this->ordersListInterfaceFactory->create();
            $result->setData($data);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 300,
                'success' => 'false',
                'message' => $message,
				'orders_count' => '',
                'orders_data' => ''
            ];
            $result = $this->ordersListInterfaceFactory->create();
            $result->setData($data);
        }
        return $result;
    }

    /**
     * Return the order information.
     *
     * @param string $incrementId
     * @throws \Magento\Framework\Exception\NoSuchEntityException If ID is not found
     * @return \Zoylo\Webservices\Api\Data\OrderInterface containing Order objects
     */
    public function getOrderInfo($incrementId) {
        try {
            $order = $this->objectManager->create('\Magento\Sales\Model\Order')->loadByIncrementId($incrementId);
            $orderId = $order->getId();
            $paymentStatus = $order->getPaymentStatus();
            $prescriptionValues = $order->getData('prescription_values');
            $incrementId = $order->getIncrementId();
            $grandTotal = $order->getGrandTotal();
            $payment_method = $order->getPayment()->getMethod();
            $message = __('Order Info');
            $data = [
                'code' => 330,
                'success' => 'true',
                'message' => $message,
                'order_id' => $orderId,
                'increment_id' => $incrementId,
                'grand_total' => $grandTotal,
                'payment_method' => $payment_method,
                'payment_status' => $paymentStatus,
                'prescription_values' => $prescriptionValues,
                'order_info' => $order
            ];
            $result = $this->orderInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 330,
                'success' => 'false',
                'message' => $message,
                'order_id' => '',
                'increment_id' => '',
                'grand_total' => '',
                'payment_method' => '',
                'payment_status' => '',
                'prescription_values' => '',
                'order_info' => ''
            ];
            $result = $this->orderInterfaceFactory->create();
            $result->setData($data);
        } catch (NoSuchEntityException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 330,
                'success' => 'false',
                'message' => $message,
                'order_id' => '',
                'increment_id' => '',
                'grand_total' => '',
                'payment_method' => '',
                'payment_status' => '',
                'prescription_values' => '',
                'order_info' => ''
            ];
            $result = $this->orderInterfaceFactory->create();
            $result->setData($data);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 330,
                'success' => 'false',
                'message' => $message,
                'order_id' => '',
                'increment_id' => '',
                'grand_total' => '',
                'payment_method' => '',
                'payment_status' => '',
                'prescription_values' => '',
                'order_info' => ''
            ];
            $result = $this->orderInterfaceFactory->create();
            $result->setData($data);
        }
        return $result;
    }

    /**
     * Product List function
     * @api
     * @param \Zoylo\Webservices\Api\ProductSearchInformationInterface[] $search
     * @return array
     */
    public function getList($search = null) {
        if (!empty($search)) {
            $searchCriteria = '';
            $i = 0;
            //print_r($search);die;
            foreach ($search as $key => $value) {
                $categoryId = $value->getCategoryId();
                if (!empty($categoryId)) {
                    $conditionType = "eq";
                    $field = 'category_id';
                    $searchCriteria = 'searchCriteria[filter_groups][0][filters][' . $i . '][field]=' . $field;
                    $searchCriteria .= '&searchCriteria[filter_groups][0][filters][' . $i . '][value]=' . $categoryId;
                    $searchCriteria .= '&searchCriteria[filter_groups][0][filters][' . $i . '][conditionType]=' . $conditionType;
                }
                $i++;
            }
            //echo ltrim($searchCriteria,'[]');
            $baseUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);

            $tokenApi = $baseUrl . "rest/V1/products?";
            $adminToken = $this->adminToken();
            $headers = array(
                'Authorization: Bearer ' . $adminToken,
                'Content-Type: application/json',
            );
            $apiUrl = $tokenApi . $searchCriteria;
            try {
                $ch = curl_init($apiUrl);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                $data = curl_exec($ch);
                $result = json_decode($data, true);
                if (isset($result->message)) {
                    echo $result->message;
                } else {
                    
                }
            } catch (Exception $e) {
                echo 'Error: ' . $e->getMessage();
            }
            //print_r($result);die;
            return $result;
        }
    }

    public function getProductsByComposition($composition, $productId, $drug_type) {
        $productCollection = $this->productFactory->create()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('salt_composition', $composition)
                ->addAttributeToFilter('drug_type',array('like' => '%'.$drug_type.'%'))
                ->addAttributeToFilter('entity_id', array('neq' => $productId))
                ->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
                ->addAttributeToSort('price', 'asc')
                ->setPageSize(3)
                ->load();
        return $productCollection;
    }

    public function getSymptomsCollection() {
        $symptomscollection = $this->_symptomsCollection->getCollection();
        $symptomscollection->addFieldToFilter('popular', 1);
        $symptomscollection->setOrder('name', 'ASC');
        $symptomscollection->setPageSize(15);
        return $symptomscollection;
    }

    public function getPopularProductsCollection() {
        $collection = $this->productFactory->create();
        $collection->addIdFilter($this->getBestsellerProductIds());
        $collection->addAttributeToFilter('status', ['in' => $this->productStatus->getVisibleStatusIds()]);
        $collection->joinField('stock_status', 'cataloginventory_stock_status', 'stock_status', 'product_id=entity_id', '{{table}}.stock_id=1', 'left')->addFieldToFilter('stock_status', array('eq' => \Magento\CatalogInventory\Model\Stock\Status::STATUS_IN_STOCK));
        $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());
        $collection->setPageSize(15);
        return $collection;
    }

    /**
     * Getting the best seller product ids
     *
     * @param array $ids
     */
    protected function getBestsellerProductIds() {
        $storeId = $this->storeManager->getStore()->getId();
        $items = $this->bestsellersCollectionFactory->create()
                        ->setPeriod('month')
                        ->setModel(
                                'Magento\Catalog\Model\Product'
                        )->addStoreFilter(
                $storeId
        );
        $ids = [];
        foreach ($items as $item) {
            $ids[] = $item->getProductId();
        }

        return $ids;
    }

}
