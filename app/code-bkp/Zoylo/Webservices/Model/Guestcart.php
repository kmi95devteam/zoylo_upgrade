<?php
namespace Zoylo\Webservices\Model;

class Guestcart extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Zoylo\Webservices\Model\ResourceModel\Guestcart');
    }
}
?>