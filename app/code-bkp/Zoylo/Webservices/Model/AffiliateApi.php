<?php
namespace Zoylo\Webservices\Model;

use Magento\Integration\Model\IntegrationFactory;
use Magento\Integration\Api\OauthServiceInterface as IntegrationOauthService;
use Magento\Customer\Model\CustomerFactory;
use Magento\Integration\Model\Oauth\TokenFactory as TokenModelFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Zoylo\Webservices\Model\Autocomplete\SearchDataProviderFactory;
use Zoylo\Webservices\Api\Data\AffiliateUsersInterFaceFactory;
use Zoylo\Webservices\Api\Data\AffiliateLoginInterFaceFactory;
use Zoylo\Webservices\Api\Data\CouponsListInterfaceFactory;
use Zoylo\Webservices\Api\Data\AffiliateOrderInterFaceFactory;
use Zoylo\Webservices\Api\Data\CountriesInfoInterfaceFactory;
use Zoylo\Webservices\Api\Data\OrdersListInterfaceFactory;
use Zoylo\Webservices\Api\AffiliateInterface as ApiInterface;

class AffiliateApi implements ApiInterface {

	/**
     * @param \Zoylo\Webservices\Logger\Logger $logger
     */
    protected $logger;

    /**
     * @param \Magento\Framework\App\ObjectManager
     */
    public $objectManager;
	
	/**
     * @var \Zoylo\Webservices\Api\Data\AffiliateUsersInterFaceFactory
     */
    protected $affiliateUsersInterFaceFactory;
	
	/**
     * @var IntegrationFactory
     */
    protected $_integrationFactory;

    /**
     * @var IntegrationOauthService
     */
    protected $_oauthService;
	
	 /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;
	
	 /**
     * Token Model
     *
     * @var TokenModelFactory
     */
    private $tokenModelFactory;
	
	/**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;
	
	/**
     * @var \Zoylo\Webservices\Api\Data\AffiliateLoginInterFaceFactory
     */
    protected $affiliateLoginInterFaceFactory;
	
	/**
     * @var \Zoylo\Webservices\Api\Data\CouponsListInterfaceFactory
     *
     */
    protected $couponsListInterfaceFactory;
	
	/**
     * @var \Magento\Framework\Stdlib\DateTime\Timezone $stdTimezone
     *
     */
    protected $stdTimezone;
	
	/**
     * @var \Magento\SalesRule\Model\ResourceModel\Coupon\CollectionFactory
     */
    protected $_salesRuleCoupon;
	
	private $storeManager;
	
	protected $quoteFactory;
	
	protected $productRepository;
	
	protected $paymentInterface;
	
	protected $cartManagementInterface;
	
	/**
     * @var \Zoylo\Webservices\Api\Data\AffiliateOrderInterFaceFactory
     */
    protected $affiliateOrderInterFaceFactory;
	
	/**
     * @var \Magento\Directory\Api\CountryInformationAcquirerInterface
     */
    protected $countryInformationAcquirer;
	
	/**
     * @var \Zoylo\Webservices\Api\Data\CountriesInfoInterfaceFactory
     */
    protected $countriesInfoInterfaceFactory;
	
	protected $directoryList;
	
	protected $_prescription;
	
	/**
     * @var SearchDataProviderFactory
     */
    protected $searchManagerFactory;
	
	/**
     * @var \Zoylo\Webservices\Api\Data\OrdersListInterfaceFactory
     */
    protected $ordersListInterfaceFactory;
	
	public function __construct( AffiliateUsersInterFaceFactory $affiliateUsersInterFaceFactory,\Zoylo\Webservices\Logger\Logger $logger, IntegrationFactory $integrationFactory, IntegrationOauthService $oauthService, CustomerFactory $customerFactory, TokenModelFactory $tokenModelFactory, \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository, AffiliateLoginInterFaceFactory $affiliateLoginInterFaceFactory, CouponsListInterfaceFactory $couponsListInterfaceFactory, \Magento\Framework\Stdlib\DateTime\Timezone $stdTimezone, \Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory $salesRuleCoupon, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Quote\Model\QuoteFactory $quoteFactory, \Magento\Catalog\Model\ProductRepository $productRepository, \Magento\Quote\Api\Data\PaymentInterface $paymentInterface, \Magento\Quote\Api\CartManagementInterface $cartManagementInterface, AffiliateOrderInterFaceFactory $affiliateOrderInterFaceFactory, \Magento\Directory\Api\CountryInformationAcquirerInterface $countryInformationAcquirer, CountriesInfoInterfaceFactory $countriesInfoInterfaceFactory, \Magento\Framework\Filesystem\DirectoryList $directoryList, \I95Dev\UploadPrescription\Model\PrescriptionFactory  $prescription, SearchDataProviderFactory $searchManagerFactory, OrdersListInterfaceFactory $ordersListInterfaceFactory
    ) {
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->logger = $logger;
		$this->affiliateUsersInterFaceFactory = $affiliateUsersInterFaceFactory;
		$this->_integrationFactory = $integrationFactory;
        $this->_oauthService = $oauthService;
		$this->tokenModelFactory = $tokenModelFactory;
        $this->customerFactory = $customerFactory;
		$this->customerRepository = $customerRepository;
		$this->affiliateLoginInterFaceFactory = $affiliateLoginInterFaceFactory;
		$this->couponsListInterfaceFactory = $couponsListInterfaceFactory;
		$this->stdTimezone = $stdTimezone;
		$this->_salesRuleCoupon = $salesRuleCoupon;
		$this->storeManager = $storeManager;
		$this->quoteFactory = $quoteFactory;
		$this->productRepository = $productRepository;
		$this->paymentInterface = $paymentInterface;
		$this->affiliateOrderInterFaceFactory = $affiliateOrderInterFaceFactory;
		$this->cartManagementInterface = $cartManagementInterface;
		$this->countryInformationAcquirer = $countryInformationAcquirer;
		$this->countriesInfoInterfaceFactory = $countriesInfoInterfaceFactory;
		$this->directoryList = $directoryList;
		$this->_prescription = $prescription;
		$this->searchManagerFactory = $searchManagerFactory;
		$this->ordersListInterfaceFactory = $ordersListInterfaceFactory;
	}

	/**
     * Return access token for admin given the customer credentials.
     *
     * @param string $username
     * @param string $email
     * @return \Zoylo\Webservices\Api\Data\AffiliateUsersInterFace containing Users objects
     * @throws \Magento\Framework\Exception\AuthenticationException
     */
    public function checkAffiliateUsers($username,$email)
	{
        if (!empty($username) && !empty($email)) {
            $this->logger->info('Admin Login Credentials');
            $this->logger->info('Username: ' . $username);
            $this->logger->info('Email: ' . $email);
            try {
				$usernames = [];
				$useremails = [];
				$rolesConfig = $this->objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface');
				$roleName =  $rolesConfig->getValue('affiliate_role/role_user/users_rold');
				$roleModels = $this->objectManager->get('Magento\Authorization\Model\Role');
				$userModel = $this->objectManager->get('Magento\User\Model\User');
				$roleModel = $roleModels->load($roleName, 'role_name');
				if ($roleModel->getId()) {
					$userIds = $roleModel->getRoleUsers();
					foreach ($userIds as $userId) {
						$user = $userModel->load($userId);
						$usernames[] = $user->getUsername();
						$useremails[] = $user->getEmail();
					}
				}
				$username = substr($username, -10);
				if (in_array($username,$usernames) && in_array($email,$useremails)) {
					$integration = $this->_integrationFactory->create()->load($roleName, 'name');
					if ($integration->getId()) {
						$accessToken = $this->_oauthService->getAccessToken($integration->getConsumerId());
						$token = $accessToken->getToken();
					}
					$code = 110;
                    $data = ['success' => 'true', 'code' => $code, 'token' => $token]; 
                    $result = $this->affiliateUsersInterFaceFactory->create();
                    $result->setData($data);
                    return $result;
                } else {
                    $message[] = array('error' => __('Customer not found.'));
                    $code = 110;
                }
            } catch (EmailNotConfirmedException $e) {
                $message[] = array('error' => $e->getMessage());
                $code = 110;
            } catch (AuthenticationException $e) {
                $message[] = array('error' => $e->getMessage());
                $code = 110;
            } catch (\Exception $e) {
                $message[] = array('error' => $e->getMessage());
                $code = 110;
            }
        } else {
            $message[] = array('error' => $e->getMessage());
            $code = 110;
        }
        $token = '';
        $data = ['success' => 'false', 'code' => $code, 'token' => $token];
        $result = $this->affiliateUsersInterFaceFactory->create();
        $result->setData($data);
        return $result;
    }
	
	/**
     * Retun the customer information.
     *
     * @param string $username
     * @return \Zoylo\Webservices\Api\Data\AffiliateLoginInterFace containing Login objects
     * @throws \Magento\Framework\Exception\AuthenticationException
     */
    public function getCustomerInfo($username){
		if (!empty($username)) {
            $this->logger->info('Customer Login Credentials');
            $this->logger->info('Username: ' . $username);
            try {
                $username = substr($username, -10);
                $customerCollection = $this->customerFactory->create()->getCollection()
                        ->addAttributeToSelect("*")
                        ->addAttributeToFilter("customer_number", array("eq" => $username));
                if (count($customerCollection) > 0) {
                    foreach ($customerCollection as $customer) {
                        
                    }

                    $code = 120;
                    $customerId = $customer->getId();
                    $customerObj = $this->objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
					$customerAddress = $regionCodeArray = array();   
					foreach ($customerObj->getAddresses() as $address)
					{
						//$customerAddress[] = $address->toArray();
                                            $addressObj =$address->toArray();
							if (array_key_exists("region_id",$addressObj)){
								$regionId = $addressObj['region_id'];
								if($regionId!=''){
									$region = $this->objectManager->create('Magento\Directory\Model\Region')->load($regionId);
							  		$regionData = $region->getData();
								  	if(isset($regionData['code']) && $regionData['code']!=''){
								  		$regionCode = $regionData['code'];
								  		$regionCodeArray['region_code'] = $regionCode;
								  	}else{
								  		$regionCodeArray['region_code'] = '';
								  	}
								}
						  	}
                                                        $regionCodeArray['email_id'] = $customerObj->getEmail();
						$customerAddress[] = array_merge_recursive($addressObj,$regionCodeArray);
					}
                    $token = $this->tokenModelFactory->create()->createCustomerToken($customer->getId())->getToken();
                    $data = ['success' => 'true', 'login_data' => $customerAddress, 'code' => $code, 'token' => $token];
                    $result = $this->affiliateLoginInterFaceFactory->create();
                    $result->setData($data);
                    return $result;
                } else {
                    $message[] = array('error' => __('Customer not found.'));
                    $code = 120;
                }
            } catch (EmailNotConfirmedException $e) {
                $message[] = array('error' => $e->getMessage());
                $code = 120;
            } catch (AuthenticationException $e) {
                $message[] = array('error' => $e->getMessage());
                $code = 120;
            } catch (\Exception $e) {
                $message[] = array('error' => $e->getMessage());
                $code = 120;
            }
        } else {
            $message[] = array('error' => $e->getMessage());
            $code = 120;
        }
        $token = '';
        $data = ['success' => 'false', 'login_data' => $message, 'code' => $code, 'token' => $token];
        $result = $this->affiliateLoginInterFaceFactory->create();
        $result->setData($data);
        return $result;
	}
	
	/**
     * Return Coupons List
     * @api
     * @return \Zoylo\Webservices\Api\Data\CouponsListInterface containing Coupons List objects
     * @throws NoSuchEntityException
     */
    public function getCouponsList() {

        try {

            $currentTime = $this->stdTimezone->date()->format('Y-m-d');
            $couponsCollection = $this->_salesRuleCoupon->create()
                            ->addFieldToFilter('is_active', 1)
                            //->addFieldToFilter('show_in_frontend', 1)
                            ->addFieldToFilter('to_date', ['gteq' => $currentTime]);
							//->setPageSize(2);
            $couponsCollection->getSelect()->group('code');
            if (count($couponsCollection) > 0) {
                foreach ($couponsCollection as $coupon) {
                    $couponsList[] = [
                        'name' => $coupon->getName(),
                        'code' => $coupon->getCode(),
                        'description' => $coupon->getDescription(),
                        'from_date' => $coupon->getFromDate(),
                        'to_date' => $coupon->getToDate(),
						'discount_percent' => $coupon->getDiscountAmount()
                    ];
                }

                $message = __('List of Coupons');
                $data = [
                    'code' => 130,
                    'success' => 'false',
                    'message' => $message,
                    'coupons_data' => $couponsList
                ];
                $result = $this->couponsListInterfaceFactory->create();
                $result->setData($data);
            } else {
                throw new NoSuchEntityException(__('No coupons are available'));
            }
        } catch (NoSuchEntityException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 130,
                'success' => 'false',
                'message' => $message,
                'coupons_data' => ''
            ];
            $result = $this->couponsListInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 130,
                'success' => 'false',
                'message' => $message,
                'coupons_data' => ''
            ];
            $result = $this->couponsListInterfaceFactory->create();
            $result->setData($data);
        }
        return $result;
    }
	 /**
     * Create order information.
     *
     * @param mixed $orderData
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return \Zoylo\Webservices\Api\Data\AffiliateOrderInterFace containing Create Order objects
     */
	public function createOrder($orderData){		
		try{
			error_reporting(0);
			$email = $orderData['email'];
			$firstname = $orderData['firstname'];
			$lastname = $orderData['lastname'];
			$password = "Zoylo@321#";
			$telephone = $orderData['telephone'];
			$telephone = substr($telephone,-10);
			$company = $orderData['company'];
			$street = $orderData['street'];
			$city = $orderData['city'];
			$region = $orderData['region'];
			$region_id = $region['region_id'];
			$country_id = $orderData['country_id'];
			$postcode = $orderData['postcode'];
			$gender = $orderData['gender'];
			$order_by = $orderData['order_by'];
			$products = $orderData['items'];
			$payment_method = $orderData['payment_method'];
			$couponCode = $orderData['coupon_code'];
			$order_comment = $orderData['order_comment'];
			$customerData = [
				"addresses" => [
			        "region" => $region,
			        "region_id" => $region_id,
			        "country_id" => $country_id,
			        "street" => $street,
			        "company" => $company,
			        "telephone" => $telephone,
			        "postcode" => $postcode,
			        "city" => $city,
			        "firstname" => $firstname,
			        "lastname" => $lastname,
			        "default_shipping" => 1,
			        "default_billing" => 1
				]
			];
			
			if($telephone == ''){
				throw new \Exception("Please provide the mobilenumber..");
			}elseif(!\Zend_Validate::is(trim($email), 'EmailAddress')){
				throw new \Exception("Please provide the email..");
			}elseif(!\Zend_Validate::is(trim($firstname), 'NotEmpty')){
				throw new \Exception("Please provide the firstname..");
			}elseif(!\Zend_Validate::is(trim($lastname), 'NotEmpty')){
				throw new \Exception("Please provide the lastname..");
			}elseif(!\Zend_Validate::is(trim($company), 'NotEmpty')){
				throw new \Exception("Please provide the company..");
			}elseif(!\Zend_Validate::is($street, 'NotEmpty')){
				throw new \Exception("Please provide the street..");
			}elseif(!\Zend_Validate::is(trim($city), 'NotEmpty')){
				throw new \Exception("Please provide the city..");
			}elseif(!\Zend_Validate::is($region, 'NotEmpty')){
				throw new \Exception("Please provide the region..");
			}elseif(!\Zend_Validate::is(trim($region_id), 'NotEmpty')){
				throw new \Exception("Please provide the region_id..");
			}elseif(!\Zend_Validate::is(trim($country_id), 'NotEmpty')){
				throw new \Exception("Please provide the country_id..");
			}elseif(!\Zend_Validate::is(trim($postcode), 'NotEmpty')){
				throw new \Exception("Please provide the postcode..");
			}elseif(!\Zend_Validate::is(trim($payment_method), 'NotEmpty')){
				throw new \Exception("Please provide the Payment Method..");
			}elseif( count($products) <= 0 ){
				throw new \Exception("Please provide the Items ..");
			}elseif(!\Zend_Validate::is(trim($order_by), 'NotEmpty')){
				throw new \Exception("Please provide the order create user..");
			}

			$customerId = '';
			$origin 	= $orderData['origin'];
			$source 	= $orderData['source'];
			$store=$this->storeManager->getStore();
	        $websiteId = $this->storeManager->getStore()->getWebsiteId();

	        //Save Customer
	        $customerCollection = $this->customerFactory->create()
	        	->getCollection()
	        	->addAttributeToSelect("*")
           		->addAttributeToFilter("customer_number", array("eq" => $telephone));
	        //$customer->loadByEmail($email);// load customet by email address
           	if (count($customerCollection) > 0) {
	            foreach ($customerCollection as $customer) {
	            }
	        }else{
	        	//If not avilable then create this customer 
	        	$customer = $this->customerFactory->create();
	            $customer->setWebsiteId($websiteId)
	                    ->setStore($store)
	                    ->setFirstname($firstname)
	                    ->setLastname($lastname)
	                    ->setEmail($email) 
	                    ->setPassword($password)
	                    ->setCustomerNumber($telephone)
	                    ->setMobileNumber($telephone);
	            $customer->save();

	            $addresss = $this->objectManager->get('\Magento\Customer\Model\AddressFactory');
	            $address = $addresss->create();
	            $address->setCustomerId($customer->getId())
	                    ->setFirstname($firstname)
	                    ->setLastname($lastname)
	                    ->setCountryId($country_id)
	                    ->setPostcode($postcode)
	                    ->setCity($city)
	                    ->setRegion($region)
	                    ->setRegionId($region_id)
	                    ->setTelephone($telephone)
	                    ->setCompany($company)
	                    ->setStreet($street)
	                    ->setCustomerAddressType('home')
	                    ->setIsDefaultBilling('1')
	                    ->setIsDefaultShipping('1')
	                    ->setSaveInAddressBook('1');
	            $address->save();

	        }
	        $customerId = $customer->getId();

            if($orderData['prescription_name']!=''){
    			//Save Prescription Image
    			$media_type = explode('/', $orderData['media_type']);
    			$data = $orderData['content']['base64_encoded_data'];
    			$imageType = $media_type[1];
    			if($imageType == 'jpeg')
    				$imageType = 'jpg';
    			$label 		= $orderData['prescription_name'];
    			$label 		= $this->seo_friendly_url($label);
    			$fileName 	= $orderData['prescription_name'];
    			$fileName 	= preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileName);
    			$fileName 	= $this->seo_friendly_url($fileName);

    			$this->logger->info('Customer Id: '.$customerId);
    			$this->logger->info('Media Type: '.$imageType);
    			$this->logger->info('Label: '.$label);
    			$this->logger->info('File Name: '.$fileName);

    			$rootPath  =  $this->directoryList->getRoot();
    			$folderpath = $rootPath.'/pub/media/prescriptions/'.$customerId;
    			if(!is_dir($folderpath)){
    				if (!mkdir($folderpath, 0777, true))
    				{
    		            throw new \Exception("Unable to create directory '{$folderpath}'.");
    		        }
    	   		}
    			$prescriptionId = $this->saveFileToDb($customerId, $fileName, $label, $imageType);
    			
    			$data = str_replace(' ', '+', $data);
    			$data = base64_decode($data);
    			$file = $rootPath.'/pub/media/prescriptions/'.$customerId.'/'.$fileName.'.'.$imageType;
    			$success = file_put_contents($file, $data);
    			//$data = base64_decode($data); 
    			$source_img = imagecreatefromstring($data);
    			$file = $rootPath.'/pub/media/prescriptions/'.$customerId.'/'.$fileName.'.'.$imageType;
    			$imageSave = imagejpeg($source_img, $file, 10);
    			$uploaddFilePath = 'prescriptions/'.$customerId.'/'.$fileName.'.'.$imageType;
    			$uploader = $this->objectManager->create('I95Dev\S3\Model\MediaStorage\File\Storage\S3');
    			$ddddd = $uploader->saveFile($uploaddFilePath);
    			imagedestroy($source_img);
			}else{
                            $prescriptionId = "";
                        }


			//Create Quote
	        $quote = $this->quoteFactory->create();
	        $quote->setStore($store);
	        $customer= $this->customerRepository->getById($customer->getEntityId()); // if you have allready buyer id then you can load customer directly 
	        $quote->setCurrency();
	        $quote->assignCustomer($customer); //Assign quote to customer
			$storagetypeCheck = 0;
	 		foreach($products as $items){
				$productRepository = $this->productRepository;
				$sku = $items['sku'];
				$qty = $items['qty'];
				$product = $productRepository->get($sku);
                                //storage type code starts				
				$storagetype = $product->getResource()->getAttribute('storage_type')->getFrontend()->getValue($product);
				if($storagetype == 'Cold Storage'){
					$storagetypeCheck = 1;
					$name = $product->getName();
				}
				 //storage type code end
				$quote->addProduct($product, $qty);
			}
				$to_zipcode = $postcode;
				if($storagetypeCheck == 1){	
				$serviceblemodel = $this->objectManager->get('I95Dev\Serviceablepincodes\Model\DataPincodes')->getCollection()->addFieldToFilter('pincode', array('eq' => $to_zipcode));
    				if($serviceblemodel->count() <= 0){
    					$message = 'Your Pincode is not serviceble for the item '.$name;
                                        throw new CouldNotSaveException(__($message));
    				}
				}
				
                        //Set Address to quote
                        $quote->getBillingAddress()->addData($customerData['addresses']);
                        $quote->getShippingAddress()->addData($customerData['addresses']);
			$quote->setCustomerEmail($email);
			$quote->setOrigin($origin);
			$quote->setSource($source);
			$quote->setOrderBy($order_by);
			$quote->setPrescriptionValues($prescriptionId);
			$quote->setCouponCode($couponCode);
			
			//Collect Rates and Set Shipping & Payment Method
			$shippingAddress=$quote->getShippingAddress(); 
                        $shippingAddress->setShippingMethod('ecomexpress_ecomexpress')->setCollectShippingRates(true);
			$quote->setPaymentMethod($payment_method); //payment method
			$quote->setInventoryProcessed(false); //not effetc inventory
			$quote->save(); //Now Save quote and your quote is ready
			$quote->getPayment()->importData(['method' => $payment_method]); // Set Sales Order Payment
			$quote->collectTotals()->save(); // Collect Totals & Save Quote
			$quote->getShippingAddress()->collectShippingRates();
			$quote->save();
			
			//Create Order
			$order = $this->cartManagementInterface->submit($quote);

			if ($order == null) {
				throw new CouldNotSaveException(
                    __('An error occurred on the server. Please try to place the order again.')
                );
			}
			
			if(isset($order_comment)){
				$order->addStatusHistoryComment($order_comment);
				$order->setIsCustomerNotified(false)->setEntityName('order')->save();
			}
 			$status = $order->getStatus();
 			$incrementId = $order->getIncrementId();
 			$message = __('Order Info');
            $data = [
				'code' 			=> 140,
                'success'       => 'true',
                'message'       => $message,
                'status'        => $status,
                'order_info'    => $order
            ];
            $result = $this->affiliateOrderInterFaceFactory->create();
            $result->setData($data);

		  
		 } catch (\Magento\Framework\Exception\LocalizedException $e) {
		 
			$message 		= $e->getMessage();
			$success 		= 'false';
			
            $data = [
				'code' 			=> 140,
                'success'       => $success,
                'message'       => $message,
                'status'        => '',
                'order_info'    => ''
            ];
            $result = $this->affiliateOrderInterFaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
        	
            $status = '';
            $message = $e->getMessage();
            $data = [
				'code' 			=> 140,
                'success'       => 'false',
                'message'       => $message,
                'status'        => $status,
                'order_info'    => ''
            ];
            $result = $this->affiliateOrderInterFaceFactory->create();
            $result->setData($data);
        }catch (CouldNotSaveException $e) {
        	$status = '';
            $message = $e->getMessage();
            $data = [
				'code' 			=> 140,
                'success'       => 'false',
                'message'       => $message,
                'status'        => $status,
                'order_info'    => ''
            ];
            $result = $this->affiliateOrderInterFaceFactory->create();
            $result->setData($data);
        }
        return $result;
	}
	
	/**
     * Return Regions collection
     * @api
     * @return \Zoylo\Webservices\Api\Data\CountriesInfoInterface containing homePage objects
     */
    public function getRegionsInfo() {
        try {
            $message = "Regions Information";
            $countries = $this->countryInformationAcquirer->getCountriesInfo();

            foreach ($countries as $country) {
                // Get regions for this country:
                $regions = [];

                if ($availableRegions = $country->getAvailableRegions()) {
                    foreach ($availableRegions as $region) {
                        $regions[] = [
                            'region_code' => $region->getCode(),
                            'region' => $region->getName(),
                            'region_id' => $region->getId()
                        ];
                    }
                }

                // Add to countriesData:
                $countriesData[] = [
                    'value' => $country->getTwoLetterAbbreviation(),
                    'label' => __($country->getFullNameLocale()),
                    'regions' => $regions
                ];
            }
            $data = [
                'code' => 150,
                'success' => 'true',
                'message' => $message,
                'countries_data' => $countriesData
            ];
            $result = $this->countriesInfoInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 150,
                'success' => 'false',
                'message' => $message,
                'countries_data' => ''
            ];
            $result = $this->countriesInfoInterfaceFactory->create();
            $result->setData($data);
        }
        return $result;
    }
	
	public function saveFileToDb($customerId, $fileName, $label, $imageType){
		try{
			$fileName 	= $this->seo_friendly_url($fileName);
			$mediaUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
			$baseTmpPath = "prescriptions/$customerId/";
			$filepullpath = $mediaUrl.$baseTmpPath.$fileName.'.'.$imageType;
			$prescriptionmodel = $this->_prescription->create();
			$prescriptionmodel->setCustomerId($customerId);
			$prescriptionmodel->setPrescriptionName($fileName);
			$prescriptionmodel->setLabel($label);
			$prescriptionmodel->setValue($filepullpath);
			$prescriptionmodel->setCreatedDate(date('Y-m-d H:i:s', time()));
			$prescriptionmodel->setStatus(1);
			$prescriptionmodel->save();
			$prescription_id = $prescriptionmodel->getPrescriptionId();
				
		} catch(\Exception $e) {
			die($e->getMessage());
		}
		return $prescription_id;		
	}
	
	public function seo_friendly_url($string){
		$string = str_replace(array('[\', \']'), '', $string);
		$string = preg_replace('/\[.*\]/U', '', $string);
		$string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
		$string = htmlentities($string, ENT_COMPAT, 'utf-8');
		$string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
		$string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
		return strtolower(trim($string, '-'));
	}
	
	/**
     * Return the Affiliate Admin orders collection.
     *
     * @param mixed $searchData
     * @return \Zoylo\Webservices\Api\Data\OrdersListInterface containing Orders objects
     */
    public function getOrdersList($searchData) {
        try {
            
            $searchDataModel = $this->searchManagerFactory->create();
            $resultList = $searchDataModel->getAffiliateOrdersList($searchData);
            if(isset($resultList['TotalOrderCount'])){
                $totalOrderCount = $resultList['TotalOrderCount'];
            }else{
                $totalOrderCount = 0;
            }

            if(isset($resultList['page_size'])){
                $pageSize = $resultList['page_size'];
            }else{
                $pageSize = 0;
            }

            //print_r($resultList);
            array_shift($resultList);
            array_shift($resultList);

			$count = count($resultList);
            if(($totalOrderCount>0) && ($pageSize>0)){
                $totalPages = ceil($totalOrderCount/$pageSize);
            }else{
                $totalPages = 0;
            }
            $message = __('Orders Information');
            $data = [
                'code' => 160,
                'success' => 'true',
                'message' => $message,
				'orders_count' => $totalOrderCount,
                'orders_data' => $resultList,
                'orders_total_pages' => $totalPages
            ];
            $result = $this->ordersListInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 160,
                'success' => 'false',
                'message' => $message,
				'orders_count' => '',
                'orders_data' => '',
                'orders_total_pages' => ''
            ];
            $result = $this->ordersListInterfaceFactory->create();
            $result->setData($data);
        } catch (NoSuchEntityException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 160,
                'success' => 'false',
                'message' => $message,
				'orders_count' => '',
                'orders_data' => '',
                'orders_total_pages' => ''
            ];
            $result = $this->ordersListInterfaceFactory->create();
            $result->setData($data);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $message = $e->getMessage();
            $data = [
                'code' => 160,
                'success' => 'false',
                'message' => $message,
				'orders_count' => '',
                'orders_data' => '',
                'orders_total_pages' => ''
            ];
            $result = $this->ordersListInterfaceFactory->create();
            $result->setData($data);
        }
        return $result;
    }
}