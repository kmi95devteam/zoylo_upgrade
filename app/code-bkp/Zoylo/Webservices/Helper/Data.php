<?php
/**
 * Copyright © 2015 Zoylo . All rights reserved.
 */
namespace Zoylo\Webservices\Helper;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

	/**
     * @param \Magento\Framework\App\Helper\Context $context
     */
	public function __construct(\Magento\Framework\App\Helper\Context $context
	) {
		parent::__construct($context);
	}
}