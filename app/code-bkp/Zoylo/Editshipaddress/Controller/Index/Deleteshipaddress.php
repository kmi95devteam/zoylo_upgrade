<?php

namespace Zoylo\Editshipaddress\Controller\Index;

class Deleteshipaddress extends \Magento\Framework\App\Action\Action
{
    /**
 * @var \Magento\Framework\Controller\Result\JsonFactory
 */
 protected $resultJsonFactory;
 
 protected $date;
 
 protected $objectManager;

/**
* $_addressRepository
* @var \Magento\Customer\Api\AddressRepository
*/
protected $_addressRepository;
protected $_customerSession;

 /**
 * @param \Magento\Framework\App\Action\Context $context
 * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
 */
 public function __construct(
     \Magento\Framework\App\Action\Context $context,
     \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
     \Magento\Directory\Model\Country $country,
     \Magento\Framework\Stdlib\DateTime\DateTime $date,
     \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
     \Magento\Customer\Model\Session $customerSession
 ) {
     $this->resultJsonFactory = $resultJsonFactory;
     $this->country = $country;
     $this->date = $date;
     $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
     $this->_addressRepository = $addressRepository;
     $this->_customerSession = $customerSession;
     parent::__construct($context);
 }
 
    public function execute()
    {   
        $resultJson = $this->resultJsonFactory->create();
        $deleteShipAddressId = $this->getRequest()->getParam('ship_address_id');
        $address = $this->_addressRepository->getById($deleteShipAddressId);
        $customerId = $address->getCustomerId();
        $sessionCustomerId = $this->_customerSession->getCustomerId();
        
        if ($customerId === $sessionCustomerId) {
            if($deleteShipAddressId!=''){
                try{
                    $this->_addressRepository->deleteById($deleteShipAddressId);
                    $deleteMsgHtml = '';
                    $deleteMsgHtml .= '<div data-role="checkout-messages" class="messages" data-bind="visible: isVisible(), click: removeAll" style="display: block;"><!-- ko foreach: messageContainer.getErrorMessages() -->
    <div role="alert" class="message message-success success">
        <div data-ui-id="checkout-cart-validationmessages-message-success" data-bind="text: $data">You have deleted address successfully.!!</div>
    </div><!--/ko-->
<!-- ko foreach: messageContainer.getSuccessMessages() --><!--/ko-->
</div>';
                    $resultJson->setData(['success' => 'true','delete_message'=> $deleteMsgHtml]);
                }catch(\Exception $e) {
                    $deleteErrorMsgHtml = '';
                    $deleteErrorMsgHtml .= '<div data-role="checkout-messages" class="messages" data-bind="visible: isVisible(), click: removeAll" style="display: block;"><!-- ko foreach: messageContainer.getErrorMessages() -->
    <div role="alert" class="message message-error error">
        <div data-ui-id="checkout-cart-validationmessages-message-error" data-bind="text: $data">Unfortunately, we do not delete the address!</div>
    </div><!--/ko-->
<!-- ko foreach: messageContainer.getSuccessMessages() --><!--/ko-->
</div>';
                    $resultJson->setData(['success' => 'false','delete_message_fail'=> $deleteErrorMsgHtml]);
                    //$this->messageManager->addError($e->getMessage());
                }
            }
        }
        return $resultJson;
    }
}