<?php

namespace Zoylo\Editshipaddress\Controller\Index;

class Customshipaddressupdate extends \Magento\Framework\App\Action\Action
{
    /**
 * @var \Magento\Framework\Controller\Result\JsonFactory
 */
 protected $resultJsonFactory;
 
 protected $date;
 
 protected $objectManager;

 /**
 * @param \Magento\Framework\App\Action\Context $context
 * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
 */
 public function __construct(
     \Magento\Framework\App\Action\Context $context,
     \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
     \Magento\Directory\Model\Country $country,
     \Magento\Framework\Stdlib\DateTime\DateTime $date
 ) {
     $this->resultJsonFactory = $resultJsonFactory;
     $this->country = $country;
     $this->date = $date;
     $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
     parent::__construct($context);
 }
 
    public function execute()
    {   
        $addressType = $this->getRequest()->getParam('address_type');
        if($addressType=="newcustomer_newaddress"){
            $resultJson = $this->resultJsonFactory->create();
            $shipAddressId = $this->getRequest()->getParam('ship_address_id');
            $shipFormAddressData = $this->getRequest()->getParam('formdata');
            $params = array();
            parse_str($shipFormAddressData, $params);
            //echo "<pre>";print_r($params);
            $cartId = $this->getRequest()->getParam('quote_id');
            $regionName = $this->getRegionName($params['region_id']);
            $postalCode = $params['postcode'];
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $pincodeErrorFlag = false;
            if($postalCode!=''){
                $pincodeModel = $objectManager->get('Ecom\Ecomexpress\Model\Pincode')->getCollection()->addFieldToFilter('pincode', array('eq' => $postalCode));
                if(sizeof($pincodeModel)==0){
                    $pincodeErrorFlag = true;
                }else{
                    $pincodeErrorFlag = false;
                }
            }
            if($pincodeErrorFlag!=true){
                    $pincodeValidFlag = true;
                    $customerSession = $objectManager->create('Magento\Customer\Model\Session');
                    $customerId = $customerEmail = '';
                    if ($customerSession->isLoggedIn()) {
                        $customerEmail = $customerSession->getCustomer()->getEmail();
                        $customerId = $customerSession->getCustomer()->getId();
                    }
                        if(isset($params['custom_attributes']['customer_address_type'])){
                                $customer_address_type = $params['custom_attributes']['customer_address_type'];
                        }else{
                                $customer_address_type = '';
                        }
                    $shippingAddressData=[
                        'email' => $customerEmail,
                        'shipping_address' =>[
                                            'customer_address_id' => '',
                                            'prefix' => '',
                                            'firstname'    => $params['firstname'],
                                            'middlename' => '',
                                            'lastname'     => $params['lastname'],
                                            'suffix' => '',
                                            'company' => '', 
                                            'street' => $params['street'][0].','.$params['street'][1],
                                            'city' => $params['city'],
                                            'country_id' => $params['country_id'],
                                            'region' => $regionName,
                                            'region_id' => $params['region_id'],
                                            'postcode' => $params['postcode'],
                                            'telephone' => $params['telephone'],
                                            'fax' => $params['telephone'],
                                            'save_in_address_book' => 1,
                                            'customer_address_type' => $customer_address_type
                                        ]
                    ];
                    $email = $shippingAddressData['email'];
                    $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
                    $store = $storeManager->getStore();
                    $websiteId = $storeManager->getStore()->getWebsiteId();
                    $customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();
                    $customer = $objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface')->getById($customerId);
                    try {
                        $quoteFactory = $objectManager->get('\Magento\Quote\Api\CartRepositoryInterface')->get($cartId);
                        $quoteFactory->setStore($store);
                        $quoteFactory->setCurrency();
                        $quoteFactory->assignCustomer($customer);
                        $quoteFactory->getBillingAddress()->addData($shippingAddressData['shipping_address']);
                        $quoteFactory->getShippingAddress()->addData($shippingAddressData['shipping_address']);
                        $shippingAddress = $quoteFactory->getShippingAddress();
                        $shippingAddress->setCollectShippingRates(true)
                                    ->collectShippingRates()
                                    ->setShippingMethod('ecomexpress_ecomexpress');
                        $quoteFactory->save();
                        $quoteFactory->collectTotals()->save();
                        //$shippingAddressId = $quoteFactory->getShippingAddress()->getId();
                        /*Save customer new address in customer address table */
                        $customAddress = $objectManager->get('\Magento\Customer\Model\AddressFactory')->create();
                        $customAddress->setData($shippingAddressData['shipping_address'])
                              ->setCustomerId($customerId)
                              ->setIsDefaultBilling('1')
                              ->setIsDefaultShipping('1')
                              ->setSaveInAddressBook('1');
                        $customAddress->save();
                        $shippingAddressId = $customAddress->getId();
                        if($shippingAddressId!=''){
                            $successhtml = '';
                            $successhtml .='<div data-role="checkout-messages" class="messages" data-bind="visible: isVisible(), click: removeAll" style="display: block;"><!-- ko foreach: messageContainer.getErrorMessages() -->
    <div role="alert" class="message message-success success">
        <div data-ui-id="checkout-cart-validationmessages-message-success" data-bind="text: $data">New Address data added successfully!!</div>
    </div><!--/ko-->
<!-- ko foreach: messageContainer.getSuccessMessages() --><!--/ko-->
</div>';
                        }
                        $resultJson->setData(['success' => 'true','pincode_valid' => $pincodeValidFlag,'ship_address_new_add'=> $successhtml]);
                    } catch (Exception $e) {
                        $html = '';
                        $html .= 'New address saved fail.'.$e->getMessage();
                        $resultJson->setData(['success' => 'false','ship_address_new_addfail'=> $html]);
                    }
            }else{
                $unvalidhtml = '';
                $unvalidhtml .='<div data-role="checkout-messages" class="messages" data-bind="visible: isVisible(), click: removeAll" style="display: block;"><!-- ko foreach: messageContainer.getErrorMessages() -->
    <div role="alert" class="message message-error error">
        <div data-ui-id="checkout-cart-validationmessages-message-error" data-bind="text: $data">Unfortunately, we do not ship to your PIN Code!</div>
    </div><!--/ko-->
<!-- ko foreach: messageContainer.getSuccessMessages() --><!--/ko-->
</div>';
                $pincodeValidFlag = false;
                $resultJson->setData(['success' => 'false','ship_address_new_addfail'=> $unvalidhtml,'pincode_valid' => $pincodeValidFlag]);
            }
            return $resultJson;
            
        }else{ //Edit address below functionality
            $pincodeErrorFlag = false;  
            $resultJson = $this->resultJsonFactory->create();
            $shipAddressId = $this->getRequest()->getParam('ship_address_id');
            $shipFormAddressData = $this->getRequest()->getParam('formdata');
            $params = array();
            parse_str($shipFormAddressData, $params);

            //echo "<pre>";print_r($params);exit;

            $date = $this->date->gmtDate();
            $params['updated_at'] = $date;
            
            try {

                if(isset($params['postcode']) && $params['postcode']!=''){
                    $pincode = $params['postcode'];
                    $objectmanager = \Magento\Framework\App\ObjectManager::getInstance();
                    $model = $objectmanager->get('Ecom\Ecomexpress\Model\Pincode')->getCollection()->addFieldToFilter('pincode', array('eq' => $pincode));
                    if(sizeof($model)==0 && !empty($pincode)){
                        $pincodeErrorFlag = true; 
                    }else{
                        $pincodeErrorFlag = false; 
                    }
                }
                if($pincodeErrorFlag!=true){
                    //$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
                    $addressShipping = $this->objectManager->get('Magento\Customer\Model\AddressFactory')->create()->load($shipAddressId);
                    $addressShipping->addData($params);
                    $addressShipping->save();
                    
                    
                    $regionName = $this->getRegionName($params['region_id']);
                    $countryCode = $params['country_id'];
                    $countryName = $this->getCountryname($countryCode);
                    
                    
                    //echo "<pre>";print_r($params);exit;
                    $html = '';
                    $html .='<p class="customer-title"><!-- ko text: address().prefix --><!-- /ko --> <!-- ko text: address().firstname -->'.$params['firstname'].'<!-- /ko --> <!-- ko text: address().middlename --><!-- /ko -->
                    <!-- ko text: address().lastname -->'.$params['lastname'].'<!-- /ko --> <!-- ko text: address().suffix --><!-- /ko --></p>';
                    $html .='<!-- ko text: _.values(address().street).join(", ") -->'.$params['street'].'<!-- /ko --><br>';
                    $html .='<!-- ko text: address().city -->'.$params['city'].'<!-- /ko -->, <span data-bind="html: address().region">'.$regionName.'</span> <!-- ko text: address().postcode -->'.$params['postcode'].'<!-- /ko --><br>';
                    $html .='<!-- ko text: getCountryName(address().countryId) -->'.$countryName.'<!-- /ko --><br>';
                    $html .='<!-- ko if: (address().telephone) --><a data-bind="text: address().telephone, attr: {"href": "tel:" + address().telephone}" href="tel:'.$params['telephone'].'">'.$params['telephone'].'</a><!-- /ko --><br>';
                    if($params['customer_address_type']!=''){
                        $html .='<span class="address-type"> <!-- ko text: element[attribute].value -->'.$params['customer_address_type'].'<!-- /ko --> </span>';
                    }
                    
                    
                    $html .='<!-- ko foreach: { data: address().customAttributes, as: "element" } --><!-- /ko -->
                    <!-- ko if: (address().isEditable()) --><!-- /ko -->
                    <!-- ko if: (!address().isEditable()) -->
                    <button type="button" class="edit-ship-address-link" data-bind="attr: { id: address().customerAddressId }" id="1">
                        <span data-bind="i18n: \'Edit\'">Edit</span>
                    </button>
                    <!-- /ko -->';
                    $resultJson->setData(['success' => 'true','ship_address_data'=> $html]);
                }else{
                    $html = '';
                    $html .= 'This Pincode is not serviceable.';
                    $resultJson->setData(['success' => 'false','ship_address_data'=> $html]);
                }
                
               
                //$resultJson->setData(['success' => 'true','ship_address_data'=> $html]);
                return $resultJson;
            } catch (\Exception $e) {
                    $this->messageManager->addError($e->getMessage());
            }
        }
    }
    public function getRegionName($regionId){
        if($regionId!=''){
            $region = $this->objectManager->create('Magento\Directory\Model\Region')
                        ->load($regionId);
            $regionData = $region->getData();
            $regionName = $regionData['name'];
        }else{
            $regionName = '';
        }
        
        return $regionName;
    }
    public function getCountryname($countryCode){    
        $countryName = $this->objectManager->create('\Magento\Directory\Model\Country')->load($countryCode)->getName();
        return $countryName;
    }
    public function showShippingAddress($shippingAddress,$shippingAddressId){
    				$html = '';
    				$html .='<div class="shipping-address-item">';
    				$html .='<input type="radio" class="radio selected-item" data-bind="click: selectAddress, css: isSelected() ? \'selected-item\' : \'not-selected-item\',attr: { id: \'zoylo-cust-ship-\'+address().customerAddressId }" id="zoylo-cust-ship-'.$shippingAddressId.'">';

	    				$html .='<div class="custom-ship-add-chk" data-bind="attr: { id: \'zoylo-checkout-ship-addr-cust-id-\'+address().customerAddressId }" id="zoylo-checkout-ship-addr-cust-id-'.$shippingAddressId.'">';

	                    $html .='<p class="customer-title"><!-- ko text: address().prefix --><!-- /ko --> <!-- ko text: address().firstname -->'.$shippingAddress['firstname'].'<!-- /ko --> <!-- ko text: address().middlename --><!-- /ko -->
	                    <!-- ko text: address().lastname -->'.$shippingAddress['lastname'].'<!-- /ko --> <!-- ko text: address().suffix --><!-- /ko --></p>';
	                    $html .='<!-- ko text: _.values(address().street).join(", ") -->'.$shippingAddress['street'].'<!-- /ko --><br>';
	                    $html .='<!-- ko text: address().city -->'.$shippingAddress['city'].'<!-- /ko -->, <span data-bind="html: address().region">'.$shippingAddress['region'].'</span> <!-- ko text: address().postcode -->'.$shippingAddress['postcode'].'<!-- /ko --><br>';
	                    $html .='<!-- ko text: getCountryName(address().countryId) -->'.$shippingAddress['country_id'].'<!-- /ko --><br>';
	                    $html .='<!-- ko if: (address().telephone) --><a data-bind="text: address().telephone, attr: {"href": "tel:" + address().telephone}" href="tel:'.$shippingAddress['telephone'].'">'.$shippingAddress['telephone'].'</a><!-- /ko --><br>';
	                    /*if($params['customer_address_type']!=''){
	                        $html .='<span class="address-type"> <!-- ko text: element[attribute].value -->'.$shippingAddress['telephone'].'<!-- /ko --> </span>';
	                    }*/
	                    
	                    $html .='</div';
                    $html .='<!-- ko foreach: { data: address().customAttributes, as: "element" } --><!-- /ko -->
                    <!-- ko if: (address().isEditable()) --><!-- /ko -->
                    <!-- ko if: (!address().isEditable()) -->
                    <button type="button" class="edit-ship-address-link" data-bind="attr: { id: address().customerAddressId }" id="'.$shippingAddressId.'">
                        <span data-bind="i18n: \'Edit\'">Edit</span>
                    </button>
                    <!-- /ko -->';
                    $html .='</div';
                    return $html;
    }
}