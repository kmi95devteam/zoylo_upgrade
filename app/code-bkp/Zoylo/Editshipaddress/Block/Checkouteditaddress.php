<?php
namespace Zoylo\Editshipaddress\Block;
class Checkouteditaddress extends \Magento\Framework\View\Element\Template {
    
    protected $_registry;
    protected $objectManager;
    protected $customerSession;
    protected $customerModel;
    protected $checkoutSession;
    public function __construct
    (
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Customer $customerModel,
        \Magento\Checkout\Model\Session $checkoutSession,
        array $data = []
    ) 
    {
        $this->_registry = $registry;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->customerSession = $customerSession;
        $this->customerModel = $customerModel;
        $this->checkoutSession = $checkoutSession;
        parent::__construct($context, $data);

    }
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
}