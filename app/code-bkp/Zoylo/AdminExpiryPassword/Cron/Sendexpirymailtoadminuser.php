<?php

namespace Zoylo\AdminExpiryPassword\Cron;
use Magento\Store\Model\Store;
use Magento\Framework\App\Area;
class Sendexpirymailtoadminuser
{
	protected $logger;

	protected $userCollectionFactory;

	protected $userFactory;

	protected $objectManager;

	protected $mathRandom;

	protected $_backendUrl;

	protected $date;
 
	public function __construct(
		\Psr\Log\LoggerInterface $loggerInterface,
		\Magento\User\Model\ResourceModel\User\CollectionFactory $userCollectionFactory,
		\Magento\User\Model\UserFactory $userFactory,
		\Magento\Framework\Math\Random $mathRandom,
		\Magento\Backend\Model\UrlInterface $backendUrl,
		\Magento\Framework\Stdlib\DateTime\DateTime $date
	) {
		$this->logger = $loggerInterface;
		$this->userCollectionFactory = $userCollectionFactory;
		$this->userFactory = $userFactory;
		$this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$this->mathRandom = $mathRandom;
		$this->_backendUrl = $backendUrl;
		$this->date = $date;
	}
 	/*
	* @Ajeshbabu G
	* @params No
	* Calculate exact differnece days and send email to user if diff<=45
	* Return all adminUsers data
	**/
	public function execute() {
		$updatedPasswordDate = '';
		//$maxDays = 45;
		$maxDays = $this->getPasswordLifeTimeDays(); //45
		if($maxDays>0){
			$minDiff = 7; //Diff days
			$adminUsersList = $this->getAdminUsers();
			foreach($adminUsersList as $userData){
					echo "<br>User Id: ".$userId = $userData['value'];
					$updatedPasswordDate = $this->getUserData($userId);
					$currentDateTime = date("Y-m-d H:i:s"); //Current Date
					$start_date = strtotime($updatedPasswordDate); //Table date
					$end_date = strtotime($currentDateTime); //Current date conversion
					$exactDifferneceDays = round(($end_date - $start_date)/60/60/24); //Calculate Diff bw end data and start date
					$maxMinDiff = $maxDays - $minDiff;
					//if(($maxMinDiff < $exactDifferneceDays) && ($exactDifferneceDays >=$maxDays)){
					if($maxMinDiff < $exactDifferneceDays && $maxMinDiff>0){
							echo "-<b>Diff-</b>".$exactDifferneceDays;
							$this->sendEmail($userId,$exactDifferneceDays); //Send email here
					}
			}
		}
	}

	/*
	* @Ajeshbabu G
	* @params No
	* Get all admin userdata
	* Return all adminUsers data
	**/
	private function getAdminUsers()
	{
	    $adminUsers = [];
	    foreach ($this->userCollectionFactory->create() as $user) {
	    	if($user->getIsActive()==1){
	    		$adminUsers[] = [
		            'value' => $user->getId(),
		            'label' => $user->getName()
	        	];
	    	}
	    }
	    return $adminUsers;
	}

	/*
	* @Ajeshbabu G
	* @params $userid
	* Get userdata based on Id
	* Return $lastUpdatedDate of $userPasswordData 
	**/
	public function getUserData($userId)
	{
		$userPasswordData = '';
	    $user = $this->userFactory->create()->load($userId);
	    $data = $user->getData();
	    if($data['is_active']==1){
	    	$currentPassword = $data['password'];
	    	$userPasswordData = $this->getAdminUserPasswordData($userId, $currentPassword);
	    }
	    return $userPasswordData;
	}

	/*
	* @Ajeshbabu G
	* @params $userid,$currentPassword
	* Get lastupdated date from admin_passwords table
	* Return $lastUpdatedDate
	**/
	public function getAdminUserPasswordData($userId,$currentPassword){
		$lastUpdated = $lastUpdatedDate = "";
		$isActive=1;
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$select = $connection->select()
		    ->from(
		        ['ap' => 'admin_passwords'],
		        ['password_id', 'user_id','password_hash','last_updated']
		    );
		$select->where('user_id = ?', (int)$userId);
		$userPasswordData = $connection->fetchAll($select);
		$passwordIdArray = array();
		
		if(isset($userPasswordData) && count($userPasswordData)>0){
			foreach($userPasswordData as $userPasswordInfo){
				if($userPasswordInfo['password_hash']==$currentPassword){
					$lastUpdated = $userPasswordInfo['last_updated'];
				}
			}
		}
		if($lastUpdated!=''){
			$lastUpdatedDate= date("Y-m-d H:i:s", $lastUpdated);
		}
		return $lastUpdatedDate;
	}

	/*
	* @Ajeshbabu G
	* @params userId
	* return userData array
	**/
	public function getUserNamebyUserId($userId){
		$userData = array();
		if($userId!=''){
			$user = $this->userFactory->create()->load($userId);
	    	$data = $user->getData();
	    	if(isset($data) && $data['firstname']!='' && $data['lastname']!='' && $data['email']!=''){
	    		$userData['username'] = $data['firstname'].' '.$data['lastname'];
	    		$userData['email'] = $data['email'];
	    	}
		}
		return $userData;
	}

	/*
	* @Ajeshbabu G
	* Send email to Admins
	* @params $userid,$exactDifferneceDays
	* return boolean true or false
	**/
	public function sendEmailBkp($userId,$exactDifferneceDays){
		if(!empty($userId)){
			$userName = $finalToEmails = '';
			$userData = $this->getUserNamebyUserId($userId);
			if(isset($userData) && $userData['username'] && $userData['email']){
				$userName = $userData['username'];
				$finalToEmails = $userData['email'];
			}
			$tokenKey = $this->mathRandom->getUniqueHash();
			$finalUrl = $this->_backendUrl->getUrl("admin/auth/resetpassword")."?id=".$userId."&token=".$tokenKey."&session_expiry=true";
			$scopeConfig = $this->objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');
                        $smtpEnableStatus =$scopeConfig->getValue('smtp/general/enabled', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                        if($smtpEnableStatus==1 && $finalToEmails!=''){
                            $hostName =$scopeConfig->getValue('smtp/configuration_option/host', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $authentication =$scopeConfig->getValue('smtp/configuration_option/authentication', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $username =$scopeConfig->getValue('smtp/configuration_option/username', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $protocol =$scopeConfig->getValue('smtp/configuration_option/protocol', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $port =$scopeConfig->getValue('smtp/configuration_option/port', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $password =$scopeConfig->getValue('smtp/configuration_option/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $returnpath =$scopeConfig->getValue('smtp/configuration_option/return_path_email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $smtpDataHelper = $this->objectManager->get('Mageplaza\Smtp\Helper\Data');
                            $mailResource = $this->objectManager->get('Mageplaza\Smtp\Mail\Rse\Mail');
                            $transportBuilder = $this->objectManager->get('Magento\Framework\Mail\Template\TransportBuilder');
                            $storeManager = $this->objectManager->create('Magento\Store\Model\StoreManagerInterface');
                            $config = [
                            'type'       => 'smtp',
                            'host'       => $hostName,
                            'auth'       => $authentication,
                            'username'   => $username,
                            'ignore_log' => true,
                            'force_sent' => true
                            ];
                            $config['ssl'] = $protocol;
                            $config['port'] = $port;
                            $config['password'] = $smtpDataHelper->getPassword();
                            $config['return_path'] = $returnpath;
                            $sender = array('email' => 'noreply@zoylo.com','name' => 'Zoylo');
                            $mailResource->setSmtpOptions(Store::DEFAULT_STORE_ID, $config);
                            $vars = array('userid' => $userId,
                            			'username' => $userName,
                            			'usertoken' => $tokenKey,
                            			'adminurl' => $finalUrl,
                            			'zoyloadmin' => 'admin',
                            			'reseturl' => $finalUrl,
                            			'diffdays' => $exactDifferneceDays,
                                     'store' => Store::DEFAULT_STORE_ID
                                 );
                            $transportBuilder->setTemplateIdentifier('zoylo_admin_expiry_password_send_email')
                                ->setTemplateOptions(['area' => Area::AREA_ADMINHTML, 'store' => Store::DEFAULT_STORE_ID])
                                ->setTemplateVars($vars)
                                ->setFrom($sender)
                                ->addTo($finalToEmails);
                            try{
                            	if($tokenKey!='' && $userId!=''){
                            		$result = $this->setAdminUserToken($tokenKey,$userId);
                            		if($result!=''){
                            			$transportBuilder->getTransport()->sendMessage();
                                        echo "Expiry Email sent success!!";
                            		}else{
                            			echo "Expiry Email not sent due to token data not stored in table!!";
                            		}
                            	}
                                return true;
                            }catch (\Exception $e) {
                             echo $e->getMessage(); die;
                            }
                        }else{
                            echo "SMTP Email service is disabled";
                            return false;
                        }
		}else{
			echo "<br>User ID not found!!!";
			return false;
		}
	}
	/*
	* @Ajeshbabu G
	* @params userId and tokenKey
	* return boolean true or false
	**/
	public function setAdminUserToken($tokenKey,$userId){
		$user = $this->userFactory->create()->load($userId);
        $user->setRpToken($tokenKey);
        $date = $this->date->gmtDate();
        $user->setRpTokenCreatedAt($date);
        if($user->save()){
        	return true;
        }else{
        	return false;
        }
	}

	public function getPasswordLifeTimeDays(){
		$scopeConfig = $this->objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');
		$passwordLifetime=0;
		$passwordLifetime =$scopeConfig->getValue('admin/security/password_lifetime', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		if($passwordLifetime!=''){
			return $passwordLifetime;
		}
		return $passwordLifetime;
	}

	/*
	* @Ajeshbabu G
	* Send email to Admins
	* @params $userid,$exactDifferneceDays
	* return boolean true or false
	**/
	public function sendEmail($userId,$exactDifferneceDays){
		if(!empty($userId)){
			$userName = $finalToEmails = '';
			$userData = $this->getUserNamebyUserId($userId);
			if(isset($userData) && $userData['username'] && $userData['email']){
				$userName = $userData['username'];
				$finalToEmails = $userData['email'];
			}
			//$tokenKey = $this->mathRandom->getUniqueHash();
			//$finalUrl = $this->_backendUrl->getUrl("admin/auth/resetpassword")."?id=".$userId."&token=".$tokenKey."&session_expiry=true";
			$scopeConfig = $this->objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');
                        $smtpEnableStatus =$scopeConfig->getValue('smtp/general/enabled', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                        if($smtpEnableStatus==1 && $finalToEmails!=''){
                            $hostName =$scopeConfig->getValue('smtp/configuration_option/host', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $authentication =$scopeConfig->getValue('smtp/configuration_option/authentication', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $username =$scopeConfig->getValue('smtp/configuration_option/username', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $protocol =$scopeConfig->getValue('smtp/configuration_option/protocol', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $port =$scopeConfig->getValue('smtp/configuration_option/port', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $password =$scopeConfig->getValue('smtp/configuration_option/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $returnpath =$scopeConfig->getValue('smtp/configuration_option/return_path_email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $smtpDataHelper = $this->objectManager->get('Mageplaza\Smtp\Helper\Data');
                            $mailResource = $this->objectManager->get('Mageplaza\Smtp\Mail\Rse\Mail');
                            $transportBuilder = $this->objectManager->get('Magento\Framework\Mail\Template\TransportBuilder');
                            $storeManager = $this->objectManager->create('Magento\Store\Model\StoreManagerInterface');
                            $config = [
                            'type'       => 'smtp',
                            'host'       => $hostName,
                            'auth'       => $authentication,
                            'username'   => $username,
                            'ignore_log' => true,
                            'force_sent' => true
                            ];
                            $config['ssl'] = $protocol;
                            $config['port'] = $port;
                            $config['password'] = $smtpDataHelper->getPassword();
                            $config['return_path'] = $returnpath;
                            $sender = array('email' => 'noreply@zoylo.com','name' => 'Zoylo');
                            $mailResource->setSmtpOptions(Store::DEFAULT_STORE_ID, $config);
                            $vars = array('userid' => $userId,
                            			'username' => $userName,
                            			'zoyloadmin' => 'admin',
                            			'diffdays' => $exactDifferneceDays,
                                     	'store' => Store::DEFAULT_STORE_ID
                                 );
                            $transportBuilder->setTemplateIdentifier('zoylo_admin_expiry_password_send_email')
                                ->setTemplateOptions(['area' => Area::AREA_ADMINHTML, 'store' => Store::DEFAULT_STORE_ID])
                                ->setTemplateVars($vars)
                                ->setFrom($sender)
                                ->addTo($finalToEmails);
                            try{
                            	if($userId!=''){
                            			$transportBuilder->getTransport()->sendMessage();
                                        echo "Expiry Email sent success!!";
                            	}else{
                            		echo "Expiry Email not sent due to userid is not exists!!";
                            	}
                                return true;
                            }catch (\Exception $e) {
                             echo $e->getMessage(); die;
                            }
                        }else{
                            echo "SMTP Email service is disabled";
                            return false;
                        }
		}else{
			echo "<br>User ID not found!!!";
			return false;
		}
	}
}