<?php
namespace Zoylo\UrlkeyRewrite\Helper;
class Catalogdata extends \Magento\Catalog\Helper\Data
{
	/**
     * Return current category path or get it from current category
     * and creating array of categories|product paths for breadcrumbs
     *
     * @return array
     */
    public function getBreadcrumbPath()
    {
        if (!$this->_categoryPath) {
            $path = [];
            $category = $this->getCategory();
            if ($category) {
                $pathInStore = $category->getPathInStore();
                $pathIds = array_reverse(explode(',', $pathInStore));

                $categories = $category->getParentCategories();

                // add category path breadcrumb
                foreach ($pathIds as $categoryId) {
                    if (isset($categories[$categoryId]) && $categories[$categoryId]->getName()) {
                        $path['category' . $categoryId] = [
                            'label' => $categories[$categoryId]->getName(),
                            'link' => $this->_isCategoryLink($categoryId) ? $categories[$categoryId]->getUrl() : ''
                        ];
                    }
                }
            }else{
                $categories = $this->getProduct()->getCategoryIds(); /*will return category ids array*/
                if(count($categories) > 0){
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    foreach($categories as $cat){
                        $category = $objectManager->create('Magento\Catalog\Model\Category')->load($cat);
                    }
                    $pathInStore = $category->getPathInStore();
                    $pathIds = array_reverse(explode(',', $pathInStore));

                    $categories = $category->getParentCategories();

                    // add category path breadcrumb
                    foreach ($pathIds as $categoryId) {
                        if (isset($categories[$categoryId]) && $categories[$categoryId]->getName()) {
                            $path['category' . $categoryId] = [
                                'label' => $categories[$categoryId]->getName(),
                                'link' => $this->_isCategoryLink($categoryId) ? $categories[$categoryId]->getUrl() : ''
                            ];
                        }
                    }
                }
            }

            if ($this->getProduct()) {
                if($this->getProduct()->getSkuDisplayName() != ''){
                    $path['product'] = ['label' => $this->getProduct()->getSkuDisplayName()];
                }else{
                    $path['product'] = ['label' => $this->getProduct()->getName()];
                }
            }

            $this->_categoryPath = $path;
        }
        return $this->_categoryPath;
    }
}