<?php
namespace Zoylo\UrlkeyRewrite\Model\Config\Source;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

/**
* Custom Attribute Renderer
*/
class Options extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
	/**
	* @var OptionFactory
	*/

	protected $optionFactory;

	/**
	* @param OptionFactory $optionFactory
	*/
	/**
	* Get all options
	*
	* @return array
	*/

	public function getAllOptions()

	{

		/* your Attribute options list*/
		//$categoryTypes = [];
    	$categoryTypes = [ ['value' => '0', 'label' => 'Please select type'], ['value' => '1', 'label' => 'Medicine'], ['value' => '2', 'label' => 'OTCMedicine'], ['value' => '3', 'label' => 'OTC'] ];
    	return $categoryTypes;

	}

}