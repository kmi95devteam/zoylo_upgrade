<?php
    namespace Zoylo\UrlkeyRewrite\Model\CatalogUrlRewrite;

    class ProductUrlPathGenerator extends \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator
    {

        // CHANGE THESE FOR CUSTOM STATIC PREFIX ROUTE of PRODUCT and PRODUCT CATEGORY
        const OTC_PREFIX_ROUTE           = 'otc';
        const MEDICINES_PREFIX_ROUTE     = 'medicines';
        const OTC_MEDICINES_PREFIX_ROUTE = 'otc_medicines';

        /**
         * @param \Magento\Store\Model\StoreManagerInterface $storeManager
         * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
         * @param \Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator $categoryUrlPathGenerator
         * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
         */
        public function __construct(
            \Magento\Store\Model\StoreManagerInterface $storeManager,
            \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
            \Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator $categoryUrlPathGenerator,
            \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
        ) {
            parent::__construct($storeManager, $scopeConfig, $categoryUrlPathGenerator, $productRepository);
        }

        /**
         * Retrieve Product Url path (with category if exists)
         *
         * @param \Magento\Catalog\Model\Product $product
         * @param \Magento\Catalog\Model\Category $category
         *
         * @return string
         */
        public function getUrlPath($product, $category = null)
        {
            $path = $product->getData('url_path');
            if ($path === null) {
                $path = $product->getUrlKey()
                    ? $this->prepareProductUrlKey($product)
                    : $this->prepareProductDefaultUrlKey($product);
            }

            $storedProduct = $this->productRepository->getById($product->getId());
            $productType = $storedProduct->getAttributeText('zoylo_product_type');
            $brand = $storedProduct->getAttributeText('brand');
            $brand = $storedProduct->formatUrlKey($brand);

            if($productType == "Medicine"){
                $path = self::MEDICINES_PREFIX_ROUTE."/".$brand."/".$path;
            }elseif ($productType == "OTCMedicine") {
               $path = self::OTC_MEDICINES_PREFIX_ROUTE."/".$brand."/".$path;
            }elseif ($productType == "OTC") {
                if ($category !== null) {
                    $categoryUrl = $this->categoryUrlPathGenerator->getUrlPath($category);
                }else{
                    $categories = $storedProduct->getCategoryIds();/*will return category ids array*/
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    foreach($categories as $category){
                        $cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category);
                        $categoryNames[] = $storedProduct->formatUrlKey($cat->getName());
                    }
                    $categoryUrl = implode("/", $categoryNames);
                }
               $path = self::OTC_PREFIX_ROUTE."/".$categoryUrl."/".$brand."/".$path;
            }else{
                $path = $product->getUrlKey();
            }
            
            return $path;
        }
    }