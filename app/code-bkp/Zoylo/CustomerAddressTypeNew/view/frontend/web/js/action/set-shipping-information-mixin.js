/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, quote) {
    'use strict';

    return function (setShippingInformationAction) {

        return wrapper.wrap(setShippingInformationAction, function (originalAction) {
            var shippingAddress = quote.shippingAddress();
            if (shippingAddress['extension_attributes'] === undefined) {
                shippingAddress['extension_attributes'] = {};
            }
            //console.log("test--"+shippingAddress.customAttributes['customer_address_type']);
            var shipAdd = shippingAddress.customAttributes['customer_address_type'];
            if(typeof shipAdd === 'object' && shipAdd.constructor === Object){
                //console.log("object");
            }else{
                //console.log("Its a string");
                if(shipAdd!==undefined){
                    if (typeof shipAdd === 'string' || shipAdd instanceof String){ //string
                        //console.log("Address Type: "+shipAdd);
                        shippingAddress['extension_attributes']['customer_address_type'] = shipAdd;
                    }
                }
            }
            //shippingAddress['extension_attributes']['customer_address_type'] = shippingAddress.customAttributes['customer_address_type'];
            // pass execution to original action ('Magento_Checkout/js/action/set-shipping-information')
            return originalAction();
        });
    };
});
