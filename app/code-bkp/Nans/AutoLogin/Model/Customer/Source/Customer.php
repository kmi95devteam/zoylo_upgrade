<?php

namespace Nans\AutoLogin\Model\Customer\Source;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\ResourceModel\Customer\Collection;
use Magento\Framework\Data\OptionSourceInterface;

class Customer implements OptionSourceInterface
{
    /**
     * @var Collection
     */
    private $collection;

    /**
     * @param Collection $collection
     */
    public function __construct(
        Collection $collection
    ) {
        $this->collection = $collection;
    }

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        $customers = [];

        /** @var CustomerInterface $customer */
        /* foreach ($this->collection->getItems() as $customer) {
            $customers[] = [
                'label' => $customer->getEmail(),
                'value' => $customer->getId(),
            ];
        } */
        $customers = [
             [
                 'label' => 'Krishnamohan@i95dev.com',
                 'value' => 264691,
             ],
             [
                 'label' => 'test@test123.com',
                 'value' => 315235,
             ],
            [
                 'label' => 'saikiran.b@zoylo.com',
                 'value' => 315037,
             ],
            
            [
                'label' => 'ramesh.dodla@jivainfotech.com',
                'value' => 313464
            ]
         ];

        return $customers;
    }
}
