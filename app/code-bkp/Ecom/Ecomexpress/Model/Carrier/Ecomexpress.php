<?php

namespace Ecom\Ecomexpress\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;

class Ecomexpress extends \Magento\Shipping\Model\Carrier\AbstractCarrier implements \Magento\Shipping\Model\Carrier\CarrierInterface 
{
	protected $_code = 'ecomexpress';
	protected $price = '0';
	protected $_logger;
	protected $_isFixed = true;
	protected $_rateResultFactory;
	protected $_rateMethodFactory;
	protected $_rateResultErrorFactory;
	protected $request;
        protected $urlInterface;
        protected $oderFactory;
        protected $quoteRepository;
        protected $messageManager;
		
        public function __construct(
	\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
	\Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
	\Psr\Log\LoggerInterface $logger,
	\Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
	\Magento\Quote\Model\QuoteFactory $quoteFactory,
	\Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\UrlInterface $urlInterface,  
        \Magento\Sales\Model\OrderFactory $oderFactory,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Framework\Message\ManagerInterface $messageManager,
	array $data = [])
	{
		$this->_rateResultFactory = $rateResultFactory;	
		$this->_rateMethodFactory = $rateMethodFactory;
		$this->_rateResultErrorFactory= $rateErrorFactory;
		$this->_logger = $logger;
		$this->scopeconfig=$scopeConfig;
                $this->request = $request;
                $this->urlInterface = $urlInterface;
                $this->oderFactory = $oderFactory;
                $this->quoteRepository = $quoteRepository;
		parent::__construct ( $scopeConfig, $rateErrorFactory, $logger, $data );
		$this->quoteFactory = $quoteFactory;
		$this->messageManager = $messageManager;
	}


	public function collectRates(RateRequest $request) 
	{
		
		try {
			if ((strpos($this->urlInterface->getCurrentUrl(), 'trueeditorder/order_shippingMethod/edit/order_id') == true) || (strpos($this->urlInterface->getCurrentUrl(), 'trueeditorder/order_shippingMethod/savePost/order_id') == true)) {
			$adminFlag = 1;
			}elseif ((strpos($this->urlInterface->getCurrentUrl(), 'admin/sales/order_create/loadBlock') == true) || (strpos($this->urlInterface->getCurrentUrl(), 'admin/sales/order_create/index') == true) || (strpos($this->urlInterface->getCurrentUrl(), 'admin/sales/order_create/reorder/order_id') == true) || (strpos($this->urlInterface->getCurrentUrl(), 'admin/quickorder/index/createorder') == true)) {
				$adminFlag = 3;
			}else{
				$adminFlag = 2;
			}
			

                $orderID = 0; 
		if($this->scopeconfig->getValue('carriers/ecomexpress/active')!="0"){
			$price = 0;
			$maximumPrice = 0;
			$prescriptionItems = false;
		
			$objectmanager = \Magento\Framework\App\ObjectManager::getInstance();
			if (! $this->getConfigFlag ( 'active' )) {
					
				return false;
			}
			$result = $this->_rateResultFactory->create ();
			//$quote = $objectmanager->create('Magento\Checkout\Model\Cart')->getQuote();
			$items = $request->getAllItems();
            $quoteId = false;
			$storagetypeCheck = 0;
			foreach ($items  as $item) 
			{
				$quoteId = $item->getQuoteId();
				$sku = $item->getSku();
				//written by achyut
				$productRepository = $objectmanager->create('Magento\Catalog\Api\ProductRepositoryInterface')->get($sku);
				$storagetype = $productRepository->getResource()->getAttribute('storage_type')->getFrontend()->getValue($productRepository);
				
				if($storagetype == 'Cold Storage'){
					$storagetypeCheck = 1;
					$name = $productRepository->getName();
				}
				
				//written by achyut
                if($sku == "Free"){
                    $prescriptionItems = true;
                }
				break;
			}
			
            //$quote = $this->getCurrentQuote($quoteId);

            if($adminFlag==2){
				$quote = $this->quoteRepository->getActive($quoteId); //Custom code for frontend
			}elseif($adminFlag==3){
				$quote = $this->getCurrentQuote($quoteId);
			}else{
				$quote = $this->getCurrentQuote($quoteId); //Commented code by Ajesh for Admin
			}
                        
			$shippingAddress = $quote->getShippingAddress();
			$to_country = $shippingAddress->getCountryId();
			$to_city = $shippingAddress->getCity();
			$to_zipcode = $shippingAddress->getPostcode();
			if($to_zipcode){
			//written by achyut start
				if($storagetypeCheck == 1){	
				$serviceblemodel = $objectmanager->get('I95Dev\Serviceablepincodes\Model\DataPincodes')->getCollection()->addFieldToFilter('pincode', array('eq' => $to_zipcode));
				if($serviceblemodel->count() <= 0){
					
					$message = 'Your Pincode is not serviceble for the item '.$name;
                    $this->messageManager->addError($message);//For Error Message
					return $message;
				}
				
				}
                         //written by achyut end  
				$allowed = false;
				if(!$this->scopeconfig->getValue('carriers/ecomexpress/sallowspecific')){
					$allowed = true;
				}else{
					$allowed_country = explode(',',$this->scopeconfig->getValue('carriers/ecomexpress/specificcountry'));
					$allowed = in_array($to_country,$allowed_country)==true ? true : false;
				}
				//$model = $objectmanager->get('Ecom\Ecomexpress\Model\Pincode')->load($to_zipcode,'pincode');
				$model = $objectmanager->get('Ecom\Ecomexpress\Model\Pincode')->getCollection()
	                                ->addFieldToFilter('pincode', array('eq' => $to_zipcode));
				//echo $to_country.'<br>'; 
				//var_dump($allowed);die('---');
				if(sizeof($model)>0){
					if($this->scopeconfig->getValue('carriers/ecomexpress/handling')){
						if(!$prescriptionItems){
							$subtotal = $quote->getSubtotal();
                            if (strpos($this->urlInterface->getCurrentUrl(), 'admin/sales/order_create/reorder/order_id') !== false && empty($quote->getItems())) { // checking this url patter so this condition executes at the admin side reorder page only and also quote is still not create if subtotal is zero
                                if ($this->request->getParam('order_id')) // if contains order id in the parameter
                                {
                                    $orderID = $this->request->getParam('order_id');
                                    $orderCollection = $this->oderFactory->create()->getCollection()->addFieldToSelect('subtotal')->addFieldToSelect('entity_id')->addFilter('entity_id', $orderID);
                                    $orderCollectionData = $orderCollection->getData();
                                    if (isset($orderCollectionData[0]['subtotal']))
                                    {
                                        $subtotal = $orderCollectionData[0]['subtotal'];
                                    }
                                }
                            }
							$maximumPrice = $this->scopeconfig->getValue("carriers/ecomexpress/shipping_subtotal");
							if($subtotal < $maximumPrice)
								$price = $this->scopeconfig->getValue("carriers/ecomexpress/handling_charge");
						}
					}
					$method = $this->_rateMethodFactory->create();
					$method->setCarrier($this->_code);
					$method->setCarrierTitle($this->scopeconfig->getValue('carriers/ecomexpress/title'));
					$method->setMethod('ecomexpress');
					$method->setMethodTitle($this->scopeconfig->getValue('carriers/ecomexpress/name'));
					$method->setPrice($price);
					$method->setCost($price);
					$result->append($method);
				}elseif($prescriptionItems){
					if(sizeof($model) > 0){
						$method = $this->_rateMethodFactory->create();
						$method->setCarrier($this->_code);
						$method->setCarrierTitle($this->scopeconfig->getValue('carriers/ecomexpress/title'));
						$method->setMethod('ecomexpress');
						$method->setMethodTitle($this->scopeconfig->getValue('carriers/ecomexpress/name'));
						$method->setPrice($price);
						$method->setCost($price);
						$result->append($method);
					}else{
						$error = $this->_rateResultErrorFactory->create();
			            $error->setCarrier($this->_code);
			            $error->setCarrierTitle($this->scopeconfig->getValue('carriers/ecomexpress/title'));
			            $error->setErrorMessage($this->scopeconfig->getValue('carriers/ecomexpress/specificerrmsg'));
			            $result->append($error);
					}
				}
				/*else if(in_array($to_country,$allowed_country)== true && sizeof($model)>0 )
				{ die('in elseif');
					if($this->scopeconfig->getValue('carriers/ecomexpress/handling') == true){
						$price = $this->scopeconfig->getValue("carriers/ecomexpress/handling_charge");
					}
					$method = $this->_rateMethodFactory->create();
					$method->setCarrier($this->_code);
					$method->setCarrierTitle($this->_code);
					$method->setMethod($this->_code);
					$method->setMethodTitle($this->scopeconfig->getValue('carriers/ecomexpress/name'));
					$method->setPrice($price);
					$method->setCost($price);
					$result->append($method);
				}*/
				else{
					if($this->scopeconfig->getValue('carriers/ecomexpress/showmethods')!="0"){

						$this->_logger->info("quoteId: ".$quoteId);
						$this->_logger->info("Country: ".$to_country);
						$this->_logger->info("to_city: ".$to_city);
						$this->_logger->info("to_zipcode: ".$to_zipcode);
						$this->_logger->info("prescriptionItems: ".$prescriptionItems);
						
						$error =$this->_rateResultErrorFactory->create();
						$error->setCarrier($this->_code);
						$error->setCarrierTitle($this->scopeconfig->getValue('carriers/ecomexpress/title'));
						$error->setMethod('ecomexpress');
						$error->setMethodTitle($this->scopeconfig->getValue('carriers/ecomexpress/name'));
						$error->setErrorMessage($this->scopeconfig->getValue('carriers/ecomexpress/specificerrmsg'));
						$result->append($error);
					}
				}
			}else{
				$method = $this->_rateMethodFactory->create();
				$method->setCarrier($this->_code);
				$method->setCarrierTitle($this->scopeconfig->getValue('carriers/ecomexpress/title'));
				$method->setMethod('ecomexpress');
				$method->setMethodTitle($this->scopeconfig->getValue('carriers/ecomexpress/name'));
				$method->setPrice($price);
				$method->setCost($price);
				$result->append($method);
			}
			//echo "tets";exit;
			return $result;
		}
		} catch (\Exception $e){
			if(strpos($e->getMessage(), "No such entity with cartId") !== false){
			}else{
				$this->messageManager->addException($e, __('Issue exists.'.$e->getMessage()));
			}
        }
	}
	public function collectRatesTestbkp(RateRequest $request) 
	{ 
		
		if ((strpos($this->urlInterface->getCurrentUrl(), 'trueeditorder/order_shippingMethod/edit/order_id') == true) || (strpos($this->urlInterface->getCurrentUrl(), 'trueeditorder/order_shippingMethod/savePost/order_id') == true)) {
			$adminFlag = 1;
		}elseif ((strpos($this->urlInterface->getCurrentUrl(), 'sales/order_create/reorder/order_id') == true)) {
			$adminFlag = 3;
		}else{
			$adminFlag = 2;
		}
		
		try {
        $orderID = 0; 
		if($this->scopeconfig->getValue('carriers/ecomexpress/active')!="0"){
			$price = 0;
			$maximumPrice = 0;
			$prescriptionItems = false;
			$objectmanager = \Magento\Framework\App\ObjectManager::getInstance();
			if (! $this->getConfigFlag ( 'active' )) {
					
				return false;
			}

			$result = $this->_rateResultFactory->create ();
			//$quote = $objectmanager->create('Magento\Checkout\Model\Cart')->getQuote();
			$items = $request->getAllItems();
            $quoteId = false;
            $to_zipcode=''; //custom
		
			foreach ($items  as $item) 
			{
				$quoteId = $item->getQuoteId();
				$sku = $item->getSku();
				
				
                if($sku == "Free"){
                      $prescriptionItems = true;
                }               
				break;
			}
			//echo $quoteId."---".$adminFlag;exit;
			if($adminFlag==2){
				$quote = $this->quoteRepository->getActive($quoteId); //Custom code for frontend
			}elseif($adminFlag==3){
				$quote = $this->getCurrentQuote($quoteId);
			}else{
				$quote = $this->getCurrentQuote($quoteId); //Commented code by Ajesh for Admin
			}

			//$shippingAddress = $quote->getShippingAddress();
    		//$quote = $this->getCurrentQuote($quoteId); //Commented code by Ajesh
                        
			$shippingAddress = $quote->getShippingAddress();
			$to_country = $shippingAddress->getCountryId();
			$to_city = $shippingAddress->getCity();
			$to_zipcode = $shippingAddress->getPostcode();
			//echo $quoteId."Testshipmethod - ".$to_zipcode;exit;
			if($to_zipcode){
				
				$allowed = false;
				if(!$this->scopeconfig->getValue('carriers/ecomexpress/sallowspecific')){
					$allowed = true;
				}else{
					$allowed_country = explode(',',$this->scopeconfig->getValue('carriers/ecomexpress/specificcountry'));
					$allowed = in_array($to_country,$allowed_country)==true ? true : false;
				}
				//$model = $objectmanager->get('Ecom\Ecomexpress\Model\Pincode')->load($to_zipcode,'pincode');
				$model = $objectmanager->get('Ecom\Ecomexpress\Model\Pincode')->getCollection()
	                                ->addFieldToFilter('pincode', array('eq' => $to_zipcode));
									
								
									
				//echo $to_country.'<br>'; 
				//var_dump($allowed);die('---');
				if(sizeof($model)>0){
					if($this->scopeconfig->getValue('carriers/ecomexpress/handling')){
						if(!$prescriptionItems){
							
							$subtotal = $quote->getSubtotal();
                            if (strpos($this->urlInterface->getCurrentUrl(), 'admin/sales/order_create/reorder/order_id') !== false && empty($quote->getItems())) { // checking this url patter so this condition executes at the admin side reorder page only and also quote is still not create if subtotal is zero

                                if ($this->request->getParam('order_id')) // if contains order id in the parameter
                                {
                                    $orderID = $this->request->getParam('order_id');
                                    $orderCollection = $this->oderFactory->create()->getCollection()->addFieldToSelect('subtotal')->addFieldToSelect('entity_id')->addFilter('entity_id', $orderID);
                                    $orderCollectionData = $orderCollection->getData();
                                    if (isset($orderCollectionData[0]['subtotal']))
                                    {
                                        $subtotal = $orderCollectionData[0]['subtotal'];
                                    }
                                }
                            }
							$maximumPrice = $this->scopeconfig->getValue("carriers/ecomexpress/shipping_subtotal");
							if($subtotal < $maximumPrice)
								$price = $this->scopeconfig->getValue("carriers/ecomexpress/handling_charge");
						}
					}
					$method = $this->_rateMethodFactory->create();
					$method->setCarrier($this->_code);
					$method->setCarrierTitle($this->scopeconfig->getValue('carriers/ecomexpress/title'));
					$method->setMethod('ecomexpress');
					$method->setMethodTitle($this->scopeconfig->getValue('carriers/ecomexpress/name'));
					$method->setPrice($price);
					$method->setCost($price);
					$result->append($method);
				}elseif($prescriptionItems){
					if(sizeof($model) > 0){
						$method = $this->_rateMethodFactory->create();
						$method->setCarrier($this->_code);
						$method->setCarrierTitle($this->scopeconfig->getValue('carriers/ecomexpress/title'));
						$method->setMethod('ecomexpress');
						$method->setMethodTitle($this->scopeconfig->getValue('carriers/ecomexpress/name'));
						$method->setPrice($price);
						$method->setCost($price);
						$result->append($method);
					}else{
						$error = $this->_rateResultErrorFactory->create();
			            $error->setCarrier('ecomexpress_ecomexpress');
			            $error->setCarrierTitle($this->scopeconfig->getValue('carriers/ecomexpress/title'));
			            $error->setErrorMessage($this->scopeconfig->getValue('carriers/ecomexpress/specificerrmsg'));
			            $result->append($error);
					}
				}else{
					if($this->scopeconfig->getValue('carriers/ecomexpress/showmethods')!="0"){
						
						$this->_logger->info("quoteId: ".$quoteId);
						$this->_logger->info("Country: ".$to_country);
						$this->_logger->info("to_city: ".$to_city);
						$this->_logger->info("to_zipcode: ".$to_zipcode);
						$this->_logger->info("prescriptionItems: ".$prescriptionItems);
						
						$error2 = $this->_rateResultErrorFactory->create();
						$error2->setCarrier($this->_code);
						$error2->setCarrierTitle($this->scopeconfig->getValue('carriers/ecomexpress/title'));
						$error2->setMethod('ecomexpress');
						$error2->setMethodTitle($this->scopeconfig->getValue('carriers/ecomexpress/name'));
						$error2->setErrorMessage($this->scopeconfig->getValue('carriers/ecomexpress/specificerrmsg'));
						$result->append($error2);
					}
				}
		}else{
				$method = $this->_rateMethodFactory->create();
				$method->setCarrier($this->_code);
				$method->setCarrierTitle($this->scopeconfig->getValue('carriers/ecomexpress/title'));
				$method->setMethod('ecomexpress');
				$method->setMethodTitle($this->scopeconfig->getValue('carriers/ecomexpress/name'));
				$method->setPrice($price);
				$method->setCost($price);
				$result->append($method);
			}
			return $result;
		}
	} catch (\Exception $e){
		$this->messageManager->addException($e, __('Issue exists.'.$e->getMessage()));
	}
	}
	
	protected function getCurrentQuote($quoteId){
	 	return $this->quoteFactory->create()->load($quoteId);
	}
	
	/**
     * @return array
     */
    public function getAllowedMethods()
    {
        return ['ecomexpress' => $this->getConfigData('name')];
    }

	public function isTrackingAvailable(){
	
		return true;
	}

}