/**
 * Copyright © 2017 Ecomteck. All rights reserved.
 * See LICENSE.txt for license details.
 */
var config = {
    map: {
        '*': {
            'Magento_Checkout/js/model/default-post-code-resolver': 'Ecomteck_OneStepCheckout/js/model/default-post-code-resolver'
        }
    }
};