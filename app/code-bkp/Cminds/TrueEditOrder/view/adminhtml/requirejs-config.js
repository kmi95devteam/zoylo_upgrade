/**
 * Cminds TrueEditOrder admin requirejs config.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */

var config = {
    map: {
        '*': {
            trueeditorder: 'Cminds_TrueEditOrder/js/order/view/trueeditorder',
            trueeditorderdelete: 'Cminds_TrueEditOrder/js/order/view/delete-order'
        }
    }
};