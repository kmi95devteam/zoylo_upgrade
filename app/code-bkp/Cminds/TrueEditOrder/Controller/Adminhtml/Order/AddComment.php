<?php

/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order;

use Magento\Backend\App\Action;
use Magento\Sales\Model\Order\Email\Sender\OrderCommentSender;

class AddComment extends \Magento\Sales\Controller\Adminhtml\Order {

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Sales::comment';

    /**
     * Add order comment action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
		
        $order = $this->_initOrder();
        if ($order) {
            try {
                $data = $this->getRequest()->getPost('history');
                $orderId = $this->getRequest()->getParam('order_id');
                
                $oldOrderStatus = $order->getStatus();
                $newStatusCode = $data['status'];
                
                if ($this->getRequest()->getPost('cancel_reason') && $this->getRequest()->getPost('cancel_reason') == 'select')
                {
                    throw new \Magento\Framework\Exception\LocalizedException(__('Please select a reason for cancellation.'));
                }
                
                if (isset($_POST['cancel_reason_text']) && $_POST['cancel_reason_text'] == '')
                {
                    throw new \Magento\Framework\Exception\LocalizedException(__('Please enter a reason for cancellation.'));
                }
                
                if ($this->getRequest()->getPost('order_return_reason') && $this->getRequest()->getPost('order_return_reason') == 'select')
                {
                    throw new \Magento\Framework\Exception\LocalizedException(__('Please select a reason for order return.'));
                }
                
                if (isset($_POST['order_return_reason_text']) && $_POST['order_return_reason_text'] == '')
                {
                    throw new \Magento\Framework\Exception\LocalizedException(__('Please enter a reason for order return.'));
                }
                
                //if ($oldOrderStatus != $newStatusCode) {
                    
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $userName = $objectManager->get('\Magento\Backend\Model\Auth\Session')->getUser()->getUserName();
                    $comment = __(
                            'User %1 has updated order status.', $userName
                    );
                    $comment .= '<br><br>';

                    $comment .= __(
                            'Order status has been changed from %1 to %2.', $oldOrderStatus, $newStatusCode
                    );
                //}
                if (empty($data['comment']) && $data['status'] == $order->getDataByKey('status')) {
                    throw new \Magento\Framework\Exception\LocalizedException(__('Please enter a comment.'));
                }
                else if (empty($data['comment']))
                {
                    throw new \Magento\Framework\Exception\LocalizedException(__('Please enter a comment.'));
                }

                $notify = isset($data['is_customer_notified']) ? $data['is_customer_notified'] : false;
                $visible = isset($data['is_visible_on_front']) ? $data['is_visible_on_front'] : false;

                




                $comment .= $data['comment'];

                $history = $order->addStatusHistoryComment($comment, $data['status']);
                $history->setIsVisibleOnFront($visible);
                $history->setIsCustomerNotified($notify);
                $history->save();
                $defaultOrderStatusStateMap = array('order_validated_cs'=>'processing','customer_payment_pending_cs'=>'pending_payment','ready_for_delivery_cs'=>'payment_review','order_placed'=>'new','order_delivered_cs'=>'complete','refund_processed_cs'=>'closed','canceled'=>'canceled');
                
                if (isset($defaultOrderStatusStateMap[$newStatusCode]))
                {
                    $order->setState($defaultOrderStatusStateMap[$newStatusCode]);
                } 
                $order->setStatus($newStatusCode);
                $order->save();

                $comment = trim(strip_tags($data['comment']));

                $order->save();
                /** @var OrderCommentSender $orderCommentSender */
                $orderCommentSender = $this->_objectManager
                        ->create(\Magento\Sales\Model\Order\Email\Sender\OrderCommentSender::class);

                $orderCommentSender->send($order, $notify, $comment);

                return $this->resultPageFactory->create();
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $response = ['error' => true, 'message' => $e->getMessage()];
            } catch (\Exception $e) {
                $response = ['error' => true, 'message' => __('We cannot add order history.')];
            }
            if (is_array($response)) {
                $resultJson = $this->resultJsonFactory->create();
                $resultJson->setData($response);
                return $resultJson;
            }
        }
        return $this->resultRedirectFactory->create()->setPath('sales/*/');
    }

}
