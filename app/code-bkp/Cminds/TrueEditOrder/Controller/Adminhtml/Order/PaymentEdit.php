<?php

namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order;

use Cminds\TrueEditOrder\Model\Config;
use Cminds\TrueEditOrder\Model\Order\EditValidator;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class PaymentEdit extends Action
{
    /**
     * @var Config
     */
    protected $moduleConfig;

    /**
     * @var EditValidator
     */
    protected $editValidator;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @param Context $context
     * @param OrderFactory $orderFactory
     * @param Config $moduleConfig
     * @param EditValidator $editValidator
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        OrderFactory $orderFactory,
        Config $moduleConfig,
        EditValidator $editValidator,
        JsonFactory $resultJsonFactory
    ) {
        $this->orderFactory = $orderFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->moduleConfig = $moduleConfig;
        $this->editValidator = $editValidator;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $response = [];
        try {
            if ($this->moduleConfig->isActive()
                && $this->moduleConfig->isOrderShippingMethodEditionEnabled()
            ) {
                $request = $this->getRequest();
                /** @var \Magento\Sales\Model\Order $order */
                $order = $this->orderFactory->create()
                    ->load($request->getParam('order_id'));

                if ($request->getParam('isAjax') &&
                    $this->editValidator->validateStatus($order) &&
                    ($method = $request->getParam('method'))
                ) {
                    $order
                        ->getPayment()
                        ->setMethod($method)
                        ->save();

                    $response = ['status' => true];
                }
            }
        } catch (\Exception $e) {
            $resultJson->setStatusHeader(
                \Zend\Http\Response::STATUS_CODE_400,
                \Zend\Http\AbstractMessage::VERSION_11,
                'Bad Request'
            );
            $response = ['status' => false, 'message' => $e->getMessage()];
        }

        return $resultJson->setData($response);
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Cminds_TrueEditOrder::edit_order_payment_method');
    }
}
