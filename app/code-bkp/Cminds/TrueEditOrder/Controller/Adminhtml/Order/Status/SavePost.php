<?php

namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order\Status;

use Cminds\TrueEditOrder\Model\Config;
use Cminds\TrueEditOrder\Model\Order\EditValidator;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Sales\Model\OrderFactory;

class SavePost extends Action
{
    /**
     * Order factory object.
     *
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * Module config object.
     *
     * @var Config
     */
    protected $moduleConfig;

    /**
     * Order edit validator.
     *
     * @var EditValidator
     */
    protected $editValidator;

    /**
     * Object constructor.
     *
     * @param Context              $context          Context object.
     * @param OrderFactory         $orderFactory     Order factory object.
     * @param Config               $moduleConfig     Module config object.
     * @param EditValidator        $editValidator    Order edit validator.
     */
    public function __construct(
        Context $context,
        OrderFactory $orderFactory,
        Config $moduleConfig,
        EditValidator $editValidator
    ) {
        $this->orderFactory = $orderFactory;
        $this->moduleConfig = $moduleConfig;
        $this->editValidator = $editValidator;

        parent::__construct($context);
    }

    /**
     * Dispatch request.
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $newStatusCode = $this->getRequest()->getParam('status');

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('sales/order/view', ['order_id' => $orderId]);

        if ($this->getRequest()->isPost() === false) {
            return $resultRedirect;
        }

        if ($this->moduleConfig->isActive() === false
            || $this->moduleConfig->isOrderStatusEditionEnabled() === false
        ) {
            return $resultRedirect;
        }

        $aclResource = 'Cminds_TrueEditOrder::edit_order_status';
        if ($this->_isAllowed($aclResource) === false) {
            return $resultRedirect;
        }

        if (empty($newStatusCode)) {
            $this->messageManager->addErrorMessage(
                __('Order status has been not selected.')
            );

            return $resultRedirect;
        }

        $order = $this->orderFactory->create()->load($orderId);
        $oldOrderStatus = $order->getStatus();

        if (!$this->editValidator->validateStatus($order)) {
            $this->messageManager->addErrorMessage(
                __('Modyfying orders with %1 status is not allowed.', $order->getStatus())
            );

            return $resultRedirect;
        }

        if ($oldOrderStatus === $newStatusCode) {
            $this->messageManager->addErrorMessage(
                __('No new status has been chosen.')
            );

            return $resultRedirect;
        }

        $order->setStatus($newStatusCode);
        $order->save();

        $this->_eventManager->dispatch(
            'cminds_trueeditorder_order_edit_status_update',
            [
                'order' => $order,
                'old_order_status' => $oldOrderStatus,
                'new_order_status' => $order->getStatus()
            ]
        );

        $this->messageManager->addSuccessMessage(
            __('Order status has been updated.')
        );

        return $resultRedirect;
    }
}
