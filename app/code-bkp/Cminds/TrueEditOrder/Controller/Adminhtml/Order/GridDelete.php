<?php

namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Cminds\TrueEditOrder\Model\Config as ModuleConfig;
use Cminds\TrueEditOrder\Model\Order\Delete as OrderDeleteModel;

/**
 *  Cminds TrueEditOrder admin order grid delete mass action controller.
 * 
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 */
class GridDelete extends AbstractMassAction
{
    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var ModuleConfig
     */
    protected $config;
    
    /**
     * @var \Cminds\TrueEditOrder\Model\Order\Delete
     */
    protected $deleteModel;

    /**
     * GridDelete constructor.
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param OrderRepository $orderRepository
     * @param ModuleConfig $moduleConfig
     * @param OrderDeleteModel $orderDeleteModel
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        ModuleConfig $moduleConfig,        
        OrderRepository $orderRepository,
        OrderDeleteModel $orderDeleteModel
    )
    {
        parent::__construct($context, $filter);

        $this->collectionFactory = $collectionFactory;
        $this->orderRepository = $orderRepository;
        $this->config = $moduleConfig;
        $this->deleteModel = $orderDeleteModel;
    }

    /**
     * @param AbstractCollection $collection
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    protected function massAction(AbstractCollection $collection)
    {
        if ( $this->config->isActive() && $this->config->isOrderDeletionEnabled() ) {
            $deleted = 0;

            /** @var \Magento\Sales\Api\Data\OrderInterface $order */
            foreach ($collection->getItems() as $order) {
                try {                
                    
                    $this->deleteModel->deleteOrder($order);
                    $deleted++;

                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage(__('Cannot delete order #%1. Please try again later.', $order->getIncrementId()));
                }
            }
            if ($deleted) {
                $this->messageManager->addSuccessMessage(__('A total of %1 order(s) have been deleted.', $deleted));
            }
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath($this->getComponentRefererUrl());

        return $resultRedirect;
    }
}
