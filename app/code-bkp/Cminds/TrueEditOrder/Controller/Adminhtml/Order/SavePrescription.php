<?php
namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;

class SavePrescription extends Action
{
    protected $logger;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
		\Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
		\Magento\Sales\Model\ResourceModel\Order $orderResourceModel
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
		$this->orderResourceModel = $orderResourceModel;
		$this->orderRepository = $orderRepository;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute() {
		
        $data = $this->getRequest()->getParams();
		$order_entity_id = $data['order_entity_id'];
		$order = $this->orderRepository->get($order_entity_id);
		$customer_id = $order->getCustomerId();
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		 $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('sales/order/view', ['order_id' => $order_entity_id]);
		
		//echo "<pre>"; print_r($data); exit;
		  if (isset($_FILES["pic"]["name"]) && $_FILES["pic"]["name"] != "") {
			  
					$total1 = $_FILES["pic"]["name"][0];
					 $total = count($_FILES['pic']['name']);
					 if($total1 != ''){
						if($total > 0){
							for( $i=0 ; $i < $total ; $i++ ) {
								if (isset($_FILES["pic"]["name"]) && $_FILES["pic"]["name"] != "") {
									$fileName = $_FILES["pic"]["name"][$i];
									$fileName = str_replace(' ', '-', $fileName);
									$fileName = str_replace("'", "_", $fileName);
									$fileName = str_replace("-", "_", $fileName);
									$fileName = preg_replace('/[^a-zA-Z0-9,.\s]/', '_', $fileName);
									
									$fileId = "pic[$i]";	
									if (isset($customer_id)&& ($customer_id != 0)) {
										$baseTmpPath = "prescriptions/$customer_id";
									} else {
										$baseTmpPath = "prescriptions";
									}
									$uploader = $objectManager->create('\Magento\MediaStorage\Model\File\UploaderFactory')->create(['fileId' => $fileId]);
									$uploader->setAllowCreateFolders(true);
									$uploader->setAllowRenameFiles(true);
									$uploader->setFilesDispersion(false);
									// $uploader->getFileExtension();
									$result = $uploader->save($objectManager->get('\Magento\Framework\Filesystem')->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath($baseTmpPath),$fileName);
									 $prescription_id[] = $this->saveFileToDb($customer_id, $fileName);
								
								}
							}
					 
						}
					 }
		  }
				if(!empty($prescription_id)){
					$prescription_ids = implode(',',  $prescription_id);
					$order->setPrescriptionValues($prescription_ids);
					$this->messageManager->addSuccessMessage(__('Prescription has been added'));
				}
				$this->orderResourceModel->save($order);
				return $resultRedirect;

    }
	
	public function saveFileToDb($customerId, $fileName){
		try{
			$fileName = str_replace("(", "_", $fileName);
			$fileName = str_replace(")", "_", $fileName);
			$fileName = preg_replace('/[^a-zA-Z0-9,.\s]/', '_', $fileName);

			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$mediaUrl = $objectManager->get('Magento\Store\Model\StoreManagerInterface')
                    ->getStore()
                    ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
			$baseTmpPath = "prescriptions/$customerId/";
			$filepullpath = $mediaUrl.$baseTmpPath.$fileName;
			$prescriptionmodel = $objectManager->create('\I95Dev\UploadPrescription\Model\Prescription');
			$prescriptionmodel->setCustomerId($customerId);
			$prescriptionmodel->setPrescriptionName($fileName);
			$prescriptionmodel->setLabel($fileName);
			$prescriptionmodel->setValue($filepullpath);
			$prescriptionmodel->setCreatedDate(date('Y-m-d H:i:s', time()));
			$prescriptionmodel->save();
			$prescription_id = $prescriptionmodel->getPrescriptionId();
				
		} catch(\Exception $e) {
			die($e->getMessage());
		}
		return $prescription_id;		
	}
	
	
	
	
	
}

