<?php

namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order\ShippingMethod;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Registry;
use Magento\Framework\Translate\InlineInterface;
use Magento\Framework\View\Result\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Controller\Adminhtml\Order;
use Magento\Sales\Model\OrderFactory;
use Psr\Log\LoggerInterface;

/**
 * Cminds TrueEditOrder admin order shipping method edit controller.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class Edit extends Order
{
    /**
     * Authorization level of a basic admin session.
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Cminds_TrueEditOrder::edit_order_shipping_method';

    /**
     * Order factory object.
     *
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * Object constructor.
     *
     * @param Context                  $context             Context object.
     * @param Registry                 $coreRegistry        Core registry object.
     * @param FileFactory              $fileFactory         File factory object.
     * @param InlineInterface          $translateInline     Inline object.
     * @param PageFactory              $resultPageFactory   Page factory object.
     * @param JsonFactory              $resultJsonFactory   Json factory object.
     * @param LayoutFactory            $resultLayoutFactory Layout factory object.
     * @param RawFactory               $resultRawFactory    Raw factory object.
     * @param OrderManagementInterface $orderManagement     Order management object.
     * @param OrderRepositoryInterface $orderRepository     Order repository object.
     * @param LoggerInterface          $logger              Logger object.
     * @param OrderFactory             $orderFactory        Order factory object.
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        FileFactory $fileFactory,
        InlineInterface $translateInline,
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        LayoutFactory $resultLayoutFactory,
        RawFactory $resultRawFactory,
        OrderManagementInterface $orderManagement,
        OrderRepositoryInterface $orderRepository,
        LoggerInterface $logger,
        OrderFactory $orderFactory
    ) {
        parent::__construct(
            $context,
            $coreRegistry,
            $fileFactory,
            $translateInline,
            $resultPageFactory,
            $resultJsonFactory,
            $resultLayoutFactory,
            $resultRawFactory,
            $orderManagement,
            $orderRepository,
            $logger
        );

        $this->orderFactory = $orderFactory;
    }

    /**
     * Edit order address form
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $order = $this->orderFactory->create()->load($orderId);

        if ($order->getId()) {
            $this->_coreRegistry->register('trueditorder_order', $order);
            $resultPage = $this->resultPageFactory->create();

            return $resultPage;
        } else {
            return $this->resultRedirectFactory->create()->setPath('sales/*/');
        }
    }
}
