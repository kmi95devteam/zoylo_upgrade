<?php

namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order\Items;

use Cminds\TrueEditOrder\Model\Config;
use Cminds\TrueEditOrder\Model\Order\EditValidator;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\ProductFactory;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Item\ToOrderItem as ToOrderItemConverter;
use Magento\Quote\Model\Quote\ItemFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Catalog\Model\Product\Type\AbstractType;
use Magento\Framework\DataObjectFactory;
use Magento\Catalog\Model\Product;
use Magento\Quote\Model\Quote\Item\Processor;
use Cminds\TrueEditOrder\Model\Order\Tax as TaxProcessor;
use Magento\Framework\DB\TransactionFactory;
use Magento\Quote\Model\QuoteRepository;
use Magento\CatalogInventory\Api\StockRegistryInterface as StockRegistry;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface as ScopeConfig;

/**
 * Cminds TrueEditOrder admin order items add controller.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class AddPost extends Action {

    /**
     * Order factory object.
     *
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * Product factory object.
     *
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * Quote item factory.
     *
     * @var ItemFactory
     */
    protected $quoteItemFactory;

    /**
     * To order item converter object.
     *
     * @var ToOrderItemConverter
     */
    protected $toOrderItem;

    /**
     * Module config object.
     *
     * @var Config
     */
    protected $moduleConfig;

    /**
     * Order edit validator.
     *
     * @var EditValidator
     */
    protected $editValidator;

    /**
     * @var \Magento\Quote\Model\Quote\Item\Processor
     */
    protected $itemProcessor;

    /**
     * @var DataObjectFactory
     */
    protected $dataObjectFactory;

    /**
     * @var TaxProcessor
     */
    private $taxProcessor;

    /**
     * @var \Magento\Framework\DB\Transaction
     */
    private $saveTransaction;

    /**
     * @var QuoteRepository
     */
    private $quoteRepository;

    /**
     * @var StockRegistry
     */
    protected $stockRegistry;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Object constructor.
     *
     * @param Context              $context             Context object.
     * @param OrderFactory         $orderFactory        Order factory object.
     * @param ProductFactory       $productFactory      Product factory object.
     * @param ItemFactory          $quoteItemFactory    Quote item factory object.
     * @param ToOrderItemConverter $toOrderItem         To order item converter object.
     * @param Config               $moduleConfig        Module config object.
     * @param EditValidator        $editValidator       Order edit validator.
     * @param Processor            $itemProcessor       Quote item processor.
     * @param DataObjectFactory    $dataObjectFactory   DataObject factory.
     * @param TaxProcessor         $taxProcessor        TaxProcessor object.
     * @param TransactionFactory   $transactionFactory  TransactionFactory object.
     * @param QuoteRepository      $quoteRepository     QuoteRepository object.
     * @param StockRegistry        $stockRegistry
     */
    public function __construct(
    Context $context, OrderFactory $orderFactory, ProductFactory $productFactory, ItemFactory $quoteItemFactory, ToOrderItemConverter $toOrderItem, Config $moduleConfig, EditValidator $editValidator, Processor $itemProcessor, DataObjectFactory $dataObjectFactory, TaxProcessor $taxProcessor, TransactionFactory $transactionFactory, QuoteRepository $quoteRepository, StoreManagerInterface $storeManager, StockRegistry $stockRegistry, ScopeConfig $scopeConfig
    ) {
        $this->orderFactory = $orderFactory;
        $this->productFactory = $productFactory;
        $this->quoteItemFactory = $quoteItemFactory;
        $this->toOrderItem = $toOrderItem;
        $this->moduleConfig = $moduleConfig;
        $this->editValidator = $editValidator;
        $this->itemProcessor = $itemProcessor;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->taxProcessor = $taxProcessor;
        $this->saveTransaction = $transactionFactory->create();
        $this->quoteRepository = $quoteRepository;
        $this->stockRegistry = $stockRegistry;
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * Dispatch request.
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute() {
        $products = $this->getRequest()->getPost('item');
        $orderId = $this->getRequest()->getParam('order_id');

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('sales/order/view', ['order_id' => $orderId]);

        if ($this->getRequest()->isPost() === false) {
            return $resultRedirect;
        }

        if ($this->moduleConfig->isActive() === false || $this->moduleConfig->isOrderItemsEditionEnabled() === false
        ) {
            return $resultRedirect;
        }

        $aclResource = 'Cminds_TrueEditOrder::edit_order_items';
        if ($this->_isAllowed($aclResource) === false) {
            return $resultRedirect;
        }

        if (empty($products)) {
            $this->messageManager->addErrorMessage(
                    __('No products has been selected.')
            );

            return $resultRedirect;
        }

        $order = $this->orderFactory->create()->load($orderId);

        if (!$this->editValidator->validateStatus($order)) {
            $this->messageManager->addErrorMessage(
                    __('Modyfying orders with %1 status is not allowed.', $order->getStatus())
            );

            return $resultRedirect;
        }

        $quoteId = $order->getQuoteId();
        $order->getStoreId();

        $this->storeManager->setCurrentStore($order->getStoreId());
        $quote = $this->quoteRepository->get($quoteId);

        $quoteItems = [];
		$exist = array();
        foreach ($products as $productId => $config) {

            $config['qty'] = isset($config['qty']) ? (double) $config['qty'] : 1;
            if (isset($config['qty']) && $config['qty'] == 'NaN') {
                $config['qty'] = 1;
            }
            $config = $this->dataObjectFactory->create()->setData($config);
            
            foreach ($order->getAllItems() as $items) {
                $product_id = $items->getProductId();
                $qouteItemId = $items->getQuoteItemId();
                $exist[$product_id] = $qouteItemId;
            }
            if (array_key_exists($productId, $exist)) {

                $qouteItemId = $exist[$productId];
                $quoteItem = $quote->getItemById($qouteItemId);
                $oldQty = $quoteItem->getQty();
                $newQty = $config['qty'];
                $totalQty = $oldQty + $newQty;
                $quoteItem->setQty($totalQty);
                $this->quoteRepository->save($quote);
                $this->updateOrderItems($quote, $order);
				$edited = true;
				if ($edited) {
					$orderItems[] = $quoteItem;
				}
            } else {
                $product = $this->productFactory->create()->load($productId);
                $product->setCartQty($config->getQty());
                $quoteItem = $this->addProductToQuote(
                        $product, $quote, $config, AbstractType::PROCESS_MODE_FULL
                );

                if (is_string($quoteItem)) {
                    $this->messageManager->addErrorMessage(__($quoteItem));

                    return $resultRedirect;
                }

                $productSku = $product->getSku();
                $productQty = $config['qty'];
                // update product Quantity
                $this->updateProductStock($productSku, $productQty);

                $quoteItems[] = $quoteItem;
                $this->quoteRepository->save($quote);
                $orderItems = [];
				foreach ($quoteItems as $quoteItem) {
					$orderItem = $this->toOrderItem->convert($quoteItem, []);
					$orderItem->setQuoteItemId($quoteItem->getId());
					$order->addItem($orderItem);
					$orderItems[] = $orderItem;
					$this->saveTransaction->addObject($orderItem);
				}
            }
				
            unset(
                    $aclResource, $quoteItems, $quoteItem, $orderItem, $products, $product, $productId, $orderId, $quoteId, $qty
            );

            $discountAmount = -1 * abs($quote->getSubtotal() - $quote->getSubtotalWithDiscount());
            $baseDiscountAmount = -1 * abs($quote->getSubtotal() - $quote->getBaseSubtotalWithDiscount());

            $order
                    ->setGrandTotal($quote->getGrandTotal())
                    ->setBaseGrandTotal($quote->getBaseGrandTotal())
                    ->setSubtotal($quote->getSubtotal())
                    ->setBaseSubtotal($quote->getBaseSubtotal())
                    ->setDiscountAmount($discountAmount)
                    ->setBaseDiscountAmount($baseDiscountAmount);
            $updateAction = 0;
            $checkStatus = 'order_validated_cs';
            $histories = $order->getStatusHistories();
            if (count($histories) > 0) {
                foreach ($histories as $key => $value) {
                    $orderHistroyStatus[] = $value->getStatus();
                }
                if (in_array($checkStatus, $orderHistroyStatus)) {
                    $updateAction = 1;
                }
            }
            if ($updateAction == 1) {
                $order->setStatus('order_modified_cs');
            }

            $order_status = $order->getStatus();
            if ($order_status == 'order_placed' || $order_status == 'validation_on_hold_cs' || $order_status == 'generate_rx_cs') {
                $onlyFreePrescription = TRUE;
                foreach ($order->getItems() as $eachItem) {
                    if ($eachItem->getSku() != 'Free') { // if any of the product is not a free product (prescription) then add shipping charges 
                        $onlyFreePrescription = FALSE;
                    }
                }
                $maximumPrice = $this->scopeConfig->getValue("carriers/ecomexpress/shipping_subtotal");
                $shippingHandlingCharges = 0;
                if ($order->getSubtotal() < $maximumPrice && $onlyFreePrescription == FALSE) {
                    $shippingHandlingCharges = $this->scopeConfig->getValue("carriers/ecomexpress/handling_charge");
                }
                $order->setShippingAmount($shippingHandlingCharges);
                $order->setBaseShippingAmount($shippingHandlingCharges);
                $order->setGrandTotal($order->getSubtotal() + $order->getDiscountAmount() + $shippingHandlingCharges);
                $order->setBaseGrandTotal($order->getSubtotal() + $order->getDiscountAmount() + $shippingHandlingCharges);
            }
            $this->saveTransaction->addObject($order);
            $this->saveTransaction->save();

            $this->taxProcessor->updateTax($quote, $order);

            $this->_eventManager->dispatch(
                    'cminds_trueeditorder_order_edit_add_items_after', [
                'order' => $order,
                'items' => $orderItems,
                    ]
            );
			$this->_eventManager->dispatch(
                'sales_order_edit',
                [
                    'order' => $order
                ]
            );
            unset(
                    $orderItems
            );

            $this->messageManager->addSuccessMessage(
                    __('Products has been added to order.')
            );
        }
        return $resultRedirect;
    }

    /**
     * Add product to quote.
     *
     * Method is similar to addProduct in Quote Model
     * but in case of already existed product in quote
     * we are adding new quote item instead of changing qty.
     *
     * @param Product $product
     * @param Quote $quote
     * @param null $request
     * @param string $processMode
     *
     * @return Quote\Item|null|string
     */
    public function addProductToQuote(
    Product $product, Quote $quote, $request = null, $processMode = AbstractType::PROCESS_MODE_FULL
    ) {
        $cartCandidates = $product->getTypeInstance()->prepareForCartAdvanced(
                $request, $product, $processMode
        );

        /**
         * Error message
         */
        if (is_string($cartCandidates) || $cartCandidates instanceof \Magento\Framework\Phrase) {
            return strval($cartCandidates);
        }

        /**
         * If prepare process return one object
         */
        if (!is_array($cartCandidates)) {
            $cartCandidates = [$cartCandidates];
        }

        $parentItem = null;
        $item = null;
        foreach ($cartCandidates as $candidate) {
            // Child items can be sticked together only within their parent
            $stickWithinParent = $candidate->getParentProductId() ? $parentItem : null;
            $candidate->setStickWithinParent($stickWithinParent);

            $item = $this->getItemByProduct($candidate, $quote);
            if (!$item) {
                $item = $this->itemProcessor->init($candidate, $request);
                $item->setQuote($quote);
                $item->setOptions($candidate->getCustomOptions());
                $item->setProduct($candidate);
                $quote->addItem($item);
            }
            $items[] = $item;
            if (!$parentItem) {
                $parentItem = $item;
            }

            if ($parentItem && $candidate->getParentProductId() && !$item->getParentItem()) {
                $item->setParentItem($parentItem);
            }

            $item->addQty($candidate->getCartQty());
        }

        return $parentItem;
    }

    /**
     * For Update stock of product
     *
     * @param string $productSku which stock you want to update
     * @param int $productQty your updated data
     * @return void
     */
    protected function updateProductStock($productSku, $productQty) {
        $stockItem = $this->stockRegistry->getStockItemBySku($productSku);
        $qty = $stockItem->getQty() - $productQty;
        if ($qty < 0) {
            $qty = 0;
        }
        $stockItem->setQty($qty);
        $stockItem->setIsInStock((bool) $qty);
        $this->stockRegistry->updateStockItemBySku($productSku, $stockItem);
    }

    /**
     * Retrieve quote item by product id
     *
     * @param   \Magento\Catalog\Model\Product $product
     * @return  \Magento\Quote\Model\Quote\Item|bool
     */
    public function getItemByProduct($product, $quote) {
        foreach ($quote->getAllItems() as $item) {
            if ($item->representProduct($product)) {
                return $item;
            }
        }
        return false;
    }

    public function updateOrderItems($quote, $order) {

        foreach ($order->getAllItems() as $orderItem) {
            $quoteItem = $quote->getItemById($orderItem->getQuoteItemId());
//            $dummyOrderItem = $this->toOrderItem->convert($quoteItem, []);
            //  echo $orderItem->getQuoteItemId();echo $quoteItem->getQty();die;
            $buyRequest = $orderItem->getBuyRequest()->setQty($quoteItem->getQty());
            $orderItem->setName($quoteItem->getName());
            $orderItem->setDescription($quoteItem->getDescription());
            $orderItem->setQtyOrdered($quoteItem->getQty());
            $orderItem->setBuyRequest($buyRequest);
            $orderItem->setPrice($quoteItem->getPrice());
            $orderItem->setBasePrice($quoteItem->getBasePrice());
            $orderItem->setRowTotal($quoteItem->getRowTotal());
            $orderItem->setBaseRowTotal($quoteItem->getBaseRowTotal());
            $orderItem->setRowTotalInclTax($quoteItem->getRowTotalInclTax());
            $orderItem->setBaseRowTotalInclTax($quoteItem->getBaseRowTotalInclTax());
            $orderItem->setTaxAmount($quoteItem->getTaxAmount());
            $orderItem->setProductOptions($quoteItem->getProductOptions());
            $orderItem->setDiscountAmount($quoteItem->getDiscountAmount());
            $this->saveTransaction->addObject($orderItem);
        }
    }

}
