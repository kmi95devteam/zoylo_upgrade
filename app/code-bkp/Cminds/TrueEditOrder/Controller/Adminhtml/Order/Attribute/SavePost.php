<?php

namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order\Attribute;

use Cminds\TrueEditOrder\Model\Config;
use Cminds\TrueEditOrder\Model\Order\EditValidator;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Shipping\Model\Config as ShippingConfig;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\OrderFactory;

/**
 * Cminds TrueEditOrder admin order shipping method save controller.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class SavePost extends Action
{
    /**
     * Order factory object.
     *
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * Module config object.
     *
     * @var Config
     */
    protected $moduleConfig;

    /**
     * Order edit validator.
     *
     * @var EditValidator
     */
    protected $editValidator;

    /**
     * Scope Config.
     *
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * SavePost constructor.
     *
     * @param Context $context
     * @param OrderFactory $orderFactory
     * @param Config $moduleConfig
     * @param EditValidator $editValidator
     */
    public function __construct(
        Context $context,
        OrderFactory $orderFactory,
        Config $moduleConfig,
        EditValidator $editValidator,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->orderFactory = $orderFactory;
        $this->moduleConfig = $moduleConfig;
        $this->editValidator = $editValidator;
        $this->scopeConfig = $scopeConfig;

        parent::__construct($context);
    }

    /**
     * Dispatch request.
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $orderId = $this
            ->getRequest()
            ->getParam('order_id');

        $doctorName = $this
            ->getRequest()
            ->getParam('doctor_name');

        $age = $this
            ->getRequest()
            ->getParam('age');

        $gender = $this
            ->getRequest()
            ->getParam('customer_gender');

        $resultRedirect = $this->resultRedirectFactory->create()
            ->setPath('sales/order/view', ['order_id' => $orderId]);

        $isPost = $this
            ->getRequest()
            ->isPost();
        if ($isPost === false) {
            return $resultRedirect;
        }

        if ($this->moduleConfig->isActive() === false
            || $this->moduleConfig->isOrderAttributeEditionEnabled() === false
        ) {
            return $resultRedirect;
        }

        $aclResource = 'Cminds_TrueEditOrder::edit_order_attributes';
        if ($this->_isAllowed($aclResource) === false) {
            return $resultRedirect;
        }

        
        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderFactory->create()
            ->load($orderId);

        $order->setDoctorName($doctorName);
        $order->setAge($age);
        $order->setCustomerGender($gender);
        $order->save();

        $this->_eventManager->dispatch(
            'cminds_trueeditorder_order_edit_attribute_update',
            [
                'order' => $order,
                'doctor_name' => $doctorName,
                'age' => $age,
                'customer_gender' => $gender
            ]
        );

        $this->messageManager->addSuccessMessage(
            __('Details has been updated.')
        );

        return $resultRedirect;
    }
}
