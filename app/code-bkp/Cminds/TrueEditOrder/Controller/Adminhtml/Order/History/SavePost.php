<?php

namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order\History;

use Cminds\TrueEditOrder\Model\Config;
use Cminds\TrueEditOrder\Model\Order\EditValidator;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Sales\Model\Order\Status\HistoryFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Backend\Model\View\Result\Redirect;

/**
 * Cminds TrueEditOrder admin order history save controller.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class SavePost extends Action
{
    /**
     * History factory object.
     *
     * @var HistoryFactory
     */
    protected $historyFactory;

    /**
     * Module config object.
     *
     * @var Config
     */
    protected $moduleConfig;

    /**
     * Order factory object.
     *
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * Order edit validator.
     *
     * @var EditValidator
     */
    protected $editValidator;

    /**
     * Object constructor.
     *
     * @param Context        $context        Context object.
     * @param HistoryFactory $historyFactory History factory object.
     * @param Config         $moduleConfig   Module config object.
     * @param OrderFactory   $orderFactory   Order factory object.
     * @param EditValidator  $editValidator   Order edit validator.
     */
    public function __construct(
        Context $context,
        HistoryFactory $historyFactory,
        Config $moduleConfig,
        OrderFactory $orderFactory,
        EditValidator $editValidator
    ) {
        $this->historyFactory = $historyFactory;
        $this->moduleConfig = $moduleConfig;
        $this->orderFactory = $orderFactory;
        $this->editValidator = $editValidator;

        parent::__construct($context);
    }

    /**
     * Dispatch request.
     *
     * @return Redirect
     */
    public function execute()
    {
        $noteId = $this->getRequest()->getParam('history_id');
        $comment = $this->getRequest()->getParam('comment');

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());

        if ($this->getRequest()->isPost() === false) {
            return $resultRedirect;
        }

        if ($this->moduleConfig->isActive() === false
            || $this->moduleConfig->isOrderNotesEditionEnabled() === false
        ) {
            return $resultRedirect;
        }

        $aclResource = 'Cminds_TrueEditOrder::edit_order_notes';
        if ($this->_isAllowed($aclResource) === false) {
            return $resultRedirect;
        }

        if (empty($comment)) {
            $this->messageManager->addErrorMessage(
                __('Order note can not be empty.')
            );

            return $resultRedirect;
        }

        $comment = str_replace("\n", '<br>', $comment);

        $history = $this->historyFactory->create()->load($noteId);

        $order = $this->orderFactory->create()->load($history->getParentId());
        if (!$this->editValidator->validateStatus($order)) {
            $this->messageManager->addErrorMessage(
                __('Modyfying orders with %1 status is not allowed.', $order->getStatus())
            );

            return $resultRedirect;
        }

        $history
            ->setComment($comment)
            ->save();

        $resultRedirect->setPath(
            'sales/order/view',
            ['order_id' => $history->getParentId()]
        );

        $this->messageManager->addSuccessMessage(
            __('Order note has been updated.')
        );

        return $resultRedirect;
    }
}
