<?php

namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order\Edit;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\Model\Session as BackendSession;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Backend\Model\View\Result\Redirect as RedirectResult;
use Magento\Catalog\Helper\Product as ProductHelper;
use Magento\Framework\Controller\Result\Raw as RawResult;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Controller\Adminhtml\Order\Create;

/**
 * Cminds TrueEditOrder admin order edit load bloack controller.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class LoadBlock extends Create
{
    /**
     * Raw result factory object.
     *
     * @var RawFactory
     */
    protected $resultRawFactory;

    /**
     * Backend session object.
     *
     * @var BackendSession
     */
    protected $backendSession;

    /**
     * @param Action\Context $context              Context object.
     * @param ProductHelper  $productHelper        Product helper object.
     * @param Escaper        $escaper              Escaper object.
     * @param PageFactory    $resultPageFactory    Page result factory object.
     * @param ForwardFactory $resultForwardFactory Forward result factory object.
     * @param RawFactory     $resultRawFactory     Raw result factory object.
     */
    public function __construct(
        Action\Context $context,
        ProductHelper $productHelper,
        Escaper $escaper,
        PageFactory $resultPageFactory,
        ForwardFactory $resultForwardFactory,
        RawFactory $resultRawFactory
    ) {
        $this->resultRawFactory = $resultRawFactory;
        $this->backendSession = $context->getSession();

        parent::__construct(
            $context,
            $productHelper,
            $escaper,
            $resultPageFactory,
            $resultForwardFactory
        );
    }

    /**
     * Loading page block.
     *
     * @return RedirectResult|RawResult
     */
    public function execute()
    {
        $request = $this->getRequest();
        try {
            $this->_initSession()->_processData();
        } catch (LocalizedException $e) {
            $this->_reloadQuote();
            $this->messageManager->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_reloadQuote();
            $this->messageManager->addException($e, $e->getMessage());
        }

        $asJson = $request->getParam('json');
        $block = $request->getParam('block');

        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        if ($asJson) {
            $resultPage->addHandle('sales_order_create_load_block_json');
        } else {
            $resultPage->addHandle('sales_order_create_load_block_plain');
        }

        if ($block) {
            $blocks = explode(',', $block);
            if ($asJson && !in_array('message', $blocks)) {
                $blocks[] = 'message';
            }

            foreach ($blocks as $block) {
                $resultPage->addHandle('trueeditorder_create_load_block_' . $block);
            }
        }

        $result = $resultPage->getLayout()->renderElement('content');
        if ($request->getParam('as_js_varname')) {
            $this->_objectManager->get('Magento\Backend\Model\Session')->setUpdateResult($result);

            return $this->resultRedirectFactory->create()
                ->setPath('sales/*/showUpdateResult');
        }

        return $this->resultRawFactory->create()
            ->setContents($result);
    }
}
