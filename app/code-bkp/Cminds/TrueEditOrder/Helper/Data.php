<?php 
namespace Cminds\TrueEditOrder\Helper;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
   protected $context;

   public function __construct(Context $context)
   {
       $this->context = $context;
       parent::__construct($context);
   }

   public function isEnable()
   {	
		return $this->scopeConfig->getValue('cminds_trueeditorder/order_edit/allow_uploading_prescriptions', ScopeInterface::SCOPE_STORE);
   }
   
   public function isActive()
    {
		return $this->scopeConfig->getValue('cminds_trueeditorder/general/enable', ScopeInterface::SCOPE_STORE);
    }
}
