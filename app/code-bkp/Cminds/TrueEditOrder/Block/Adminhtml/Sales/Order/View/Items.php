<?php
namespace Cminds\TrueEditOrder\Block\Adminhtml\Sales\Order\View;


class Items extends \Magento\Sales\Block\Adminhtml\Order\View\Items
{
    /**
     * @var \Cminds\TrueEditOrder\Model\Config
     */
    protected $moduleConfig;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration
     * @param \Magento\Framework\Registry $registry
     * @param \Cminds\TrueEditOrder\Model\Config $moduleConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration,
        \Magento\Framework\Registry $registry,
        \Cminds\TrueEditOrder\Model\Config $moduleConfig,
        array $data = []
    ) {
        $this->moduleConfig = $moduleConfig;
        parent::__construct(
            $context,
            $stockRegistry,
            $stockConfiguration,
            $registry,
            $data
        );
    }

    /**
     * @return bool
     */
    public function actionColumnEnabled()
    {
        return $this->moduleConfig->isActive() && $this->moduleConfig->isOrderItemsEditionEnabled();
    }
}
