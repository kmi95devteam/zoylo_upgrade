<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\Edit\ShippingMethod;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Model\Session\Quote;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Phrase;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;
use Magento\Quote\Model\Quote\Address\RateRequestFactory;
use Magento\Sales\Block\Adminhtml\Order\Create\Form\AbstractForm;
use Magento\Sales\Model\AdminOrder\Create;
use Magento\Shipping\Model\Config as ShippingConfig;
use Magento\Sales\Model\Order;
use Magento\Shipping\Model\Shipping;
use Magento\Quote\Model\QuoteFactory;

/**
 * Cminds TrueEditOrder admin order shipping method edit form block.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class Form extends AbstractForm
{
    /**
     * Form template.
     *
     * @var string
     */
    protected $_template = 'order/shippingmethod/form.phtml';

    /**
     * Core registry object.
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * Shipping object.
     *
     * @var Shipping
     */
    protected $shipping;

    /**
     * Rate request factory object.
     *
     * @var RateRequestFactory
     */
    protected $rateRequestFactory;

    /**
     * Pricing helper object.
     *
     * @var PricingHelper
     */
    protected $pricingHelper;

    /**
     * Quote create object.
     *
     * @var QuoteFactory
     */
    protected $quoteFactory;

    /**
     * Shipping Config.
     *
     * @var ShippingConfig
     */
    protected $shippingConfig;

    /**
     * Form constructor.
     *
     * @param Context $context
     * @param Quote $sessionQuote
     * @param Create $orderCreate
     * @param PriceCurrencyInterface $priceCurrency
     * @param FormFactory $formFactory
     * @param DataObjectProcessor $dataObjectProcessor
     * @param Registry $registry
     * @param Shipping $shipping
     * @param RateRequestFactory $rateRequestFactory
     * @param PricingHelper $pricingHelper
     * @param QuoteFactory $quoteFactory
     * @param ShippingConfig $shippingConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        Quote $sessionQuote,
        Create $orderCreate,
        PriceCurrencyInterface $priceCurrency,
        FormFactory $formFactory,
        DataObjectProcessor $dataObjectProcessor,
        Registry $registry,
        Shipping $shipping,
        RateRequestFactory $rateRequestFactory,
        PricingHelper $pricingHelper,
        QuoteFactory $quoteFactory,
        ShippingConfig $shippingConfig,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $sessionQuote,
            $orderCreate,
            $priceCurrency,
            $formFactory,
            $dataObjectProcessor,
            $data
        );

        $this->coreRegistry = $registry;
        $this->shipping = $shipping;
        $this->rateRequestFactory = $rateRequestFactory;
        $this->pricingHelper = $pricingHelper;
        $this->quoteFactory = $quoteFactory;
        $this->shippingConfig = $shippingConfig;
    }

    /**
     * Order getter.
     *
     * @return Order
     */
    protected function getOrder()
    {
        return $this->coreRegistry->registry('trueditorder_order');
    }

    /**
     * Define form attributes (id, method, action).
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $this->_form->setId('edit_form');
        $this->_form->setMethod('post');
        $this->_form->setAction(
            $this->getUrl(
                'trueeditorder/*/savePost',
                ['order_id' => $this->getOrder()->getId()]
            )
        );
        $this->_form->setUseContainer(true);

        $fieldset = $this->_form->addFieldset('main', ['no_container' => true]);

        $fieldset->addField(
            'order_id',
            'hidden',
            [
                'name' => 'order_id',
                'value' => $this->getOrder()->getId(),
            ]
        );

        $shippingMethods = $this->getShippingMethodsOptionArray();
        $currentShippingMethod = $this->getOrder()->getShippingMethod(false);
        $shippingAmount = floatval($this->getOrder()->getShippingAmount());
        $baseShippingAmount = floatval($this->getOrder()->getBaseShippingAmount());
        
        $fieldset->addField(
            'shipping_method',
            'select',
            [
                'name' => 'shipping_method',
                'label' => __('Shipping Method'),
                'title' => __('Shipping Method'),
                'values' => $shippingMethods,
                'required' => true,
                'class' => 'required-entry',
                'value' => $currentShippingMethod,
            ]
        );
        // add use custom price checkbox        
        $fieldset->addField(
            'use_custom_shipping_price',
            'checkbox',
            [
                'name' => 'use_custom_shipping_price',
                'label' => __('Use Custom Shipping Price'),
                'title' => __('Use Custom Shipping Price'),
                'onchange' => 'this.value = this.checked;',
                'checked' => $shippingAmount != $baseShippingAmount,
                'value' => $shippingAmount != $baseShippingAmount ? 1 : ''                
            ]
        );

        // if price isn't custom, than do not output anything here
        $fieldset->addField(
            'custom_shipping_price',
            'text',
            [
                'name' => 'custom_shipping_price',
                'label' => __('Custom Shipping Price'),
                'title' => __('Custom Shipping Price'),
                'required' => false,
                'value' => $shippingAmount != $baseShippingAmount ? $shippingAmount : ''
            ]
        );

        return $this;
    }

    /**
     * Form header text getter.
     *
     * @return Phrase
     */
    public function getHeaderText()
    {
        return __('Order Shipping Method');
    }

    /**
     * Return Form Elements values.
     *
     * @return array
     */
    public function getFormValues()
    {
        return $this->getOrder()->getData();
    }

    /**
     * Retrieve shipping methods available for particular address.
     *
     * @return \Magento\Shipping\Model\Rate\Result
     */
    protected function getShippingMethods()
    {
        $order = $this->getOrder();
        $store = $this->_storeManager->getStore();
        $quote = $this->quoteFactory->create()->load($order->getQuoteId());
        $address = $quote->getShippingAddress();
        $orderAddress = $order->getShippingAddress();

        //
        // Collect quote totals fills address data with free_method_weight data
        // which is required to make table rate method available
        // Logic is done in Magento\Quote\Model\Quote\Address\Total\Shipping collect() method
        //
        $quote->collectTotals();

        $request = $this->rateRequestFactory->create();
        $request->setAllItems($address->getAllItems());
        $request->setDestCountryId($orderAddress->getCountryId());
        $request->setDestRegionId($orderAddress->getRegionId());
        $request->setDestPostcode($orderAddress->getPostcode());
        $request->setPackageValue($address->getBaseSubtotal());
        $request->setPackageValueWithDiscount($address->getBaseSubtotalWithDiscount());
        $request->setPackageWeight($address->getWeight());
        $request->setFreeMethodWeight($address->getFreeMethodWeight());
        $request->setPackageQty($address->getItemQty());
        $request->setStoreId($store->getId());
        $request->setWebsiteId($store->getWebsiteId());
        $request->setBaseCurrency($store->getBaseCurrency());
        $request->setPackageCurrency($store->getCurrentCurrency());

        $request->setBaseSubtotalInclTax($order->getBaseSubtotalInclTax());

        $this->shipping->collectRates($request);
        $shippingMethods = $this->shipping->getResult();

        return $shippingMethods;
    }

    /**
     * Retrieve shipping methods.
     *
     * @return array
     */
    protected function getShippingMethodsOptionArray()
    {
        $shippingMethods = $this->getShippingMethods();
        $optionArray = [];

        foreach ($shippingMethods->getAllRates() as $shippingMethod) {
            $optionArray[] = [
                'value' => sprintf(
                    '%s_%s',
                    $shippingMethod->getCarrier(),
                    $shippingMethod->getMethod()
                ),
                'label' => sprintf(
                    '%s - %s (default %s)',
                    $shippingMethod->getCarrierTitle(),
                    $shippingMethod->getMethodTitle(),
                    $this->pricingHelper->currency(
                        $shippingMethod->getPrice(),
                        true,
                        false
                    )
                ),
            ];
        }

        return $optionArray;
    }
}
