<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\Edit\Coupon;

use Cminds\TrueEditOrder\Block\Adminhtml\Order\Edit\ShippingMethod\Form as FormParent;

class Form extends FormParent
{
    /**
     * @var string
     */
    protected $_template = 'order/coupon/form.phtml';

    /**
     * {@inheritdoc}
     */
    protected function _prepareForm()
    {
        $this->_form->setId('edit_form');
        $this->_form->setMethod('post');
        $this->_form->setAction(
            $this->getUrl(
                'trueeditorder/*/savePost',
                ['order_id' => $this->getOrder()->getId()]
            )
        );
        $this->_form->setUseContainer(true);

        $fieldset = $this->_form->addFieldset('main', ['no_container' => true]);

        $fieldset->addField(
            'order_id',
            'hidden',
            [
                'name' => 'order_id',
                'value' => $this->getOrder()->getId(),
            ]
        );

        $fieldset->addField(
            'coupon_code',
            'text',
            [
                'name' => 'coupon_code',
                'label' => __('Coupon Code'),
                'title' => __('Coupon Code'),
                'required' => false,
                'note' => __('Leave empty to delete coupon from order'),
                'value' => $this->getOrder()->getCouponCode() ?: ''
            ]
        );

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getHeaderText()
    {
        return __('Order Coupon');
    }
}
