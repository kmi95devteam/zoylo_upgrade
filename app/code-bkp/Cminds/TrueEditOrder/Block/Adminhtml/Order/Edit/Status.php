<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\Edit;

use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Block\Widget\Form\Container;
use Magento\Framework\Registry;
use Magento\Sales\Model\Order;

class Status extends Container
{
    /**
     * Core registry object.
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * Object initialization.
     *
     * @param Context  $context  Context object.
     * @param Registry $registry Registry object.
     * @param array    $data     Data array.
     */
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->coreRegistry = $registry;

        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Internal constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_order';
        $this->_mode = 'edit_status';
        $this->_blockGroup = 'Cminds_TrueEditOrder';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Order Status'));
        $this->buttonList->remove('delete');
    }

    /**
     * Retrieve text for header element depending on loaded page.
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        return __('Edit Order Status');
    }

    /**
     * Order getter.
     *
     * @return Order
     */
    protected function getOrder()
    {
        return $this->coreRegistry->registry('trueditorder_order');
    }

    /**
     * Back button url getter.
     *
     * @return string
     */
    public function getBackUrl()
    {
        $order = $this->getOrder();

        return $this->getUrl(
            'sales/order/view',
            ['order_id' => $order ? $order->getId() : null]
        );
    }
}
