<?php
namespace Cminds\TrueEditOrder\Block\Adminhtml\Order;

use Cminds\TrueEditOrder\Model\Config;
use Magento\Framework\AuthorizationInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Block\Adminhtml\Order\Totals as TotalsParent;
use Magento\Sales\Helper\Admin;

class Totals extends TotalsParent
{
    /**
     * @var Config
     */
    protected $moduleConfig;

    /**
     * @var AuthorizationInterface
     */
    protected $authorization;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param Admin $adminHelper
     * @param Config $moduleConfig
     * @param AuthorizationInterface $authorization
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Admin $adminHelper,
        Config $moduleConfig,
        AuthorizationInterface $authorization,
        array $data = []
    ) {
        $this->moduleConfig = $moduleConfig;
        $this->authorization = $authorization;
        parent::__construct(
            $context,
            $registry,
            $adminHelper,
            $data
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function _initTotals()
    {
        parent::_initTotals();
        if (
            $this->moduleConfig->isActive() &&
            $this->moduleConfig->orderCouponEditingEnabled() &&
            $this->authorization->isAllowed('Cminds_TrueEditOrder::edit_order_coupon')
        ) {
            if ($couponDesc = $this->getSource()->getDiscountDescription()) {
                $discountInfo = __('Discount (%1)', $couponDesc);
            } else {
                $discountInfo = __('Discount');
            }
            $discountEditUrl = $this->getUrl(
                'trueeditorder/order_coupon/edit',
                ['order_id' => $this->getRequest()->getParam('order_id')]
            );
            $discountLabel = __(
                $discountInfo .
                '<a href="' .
                $discountEditUrl .
                '" id="discount_edit"> ' .
                __('Edit') .
                '</a>'
            );
            if (isset($this->_totals['discount'])) {
                $this->_totals['discount']['label'] = $discountLabel;
            } else {
                $this->_totals['discount'] = new \Magento\Framework\DataObject(
                    [
                        'code' => 'discount',
                        'value' => 0,
                        'base_value' => 0,
                        'label' => $discountLabel
                    ]
                );
            }
        }
        return $this;
    }
}