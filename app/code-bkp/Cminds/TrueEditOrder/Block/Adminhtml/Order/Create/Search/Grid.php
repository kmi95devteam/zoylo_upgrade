<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\Create\Search;

use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Sales\Block\Adminhtml\Order\Create\Search\Grid as OrderGrid;

/**
 * Cminds TrueEditOrder admin order create search grid block.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class Grid extends OrderGrid
{
    /**
     * Object internal initialization.
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setId('sales_order_create_search_grid');
        $this->setRowClickCallback('order.productGridRowClick.bind(order)');
        $this->setCheckboxCheckCallback('');
        $this->setRowInitCallback('');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
        if ($this->getRequest()->getParam('collapse')) {
            $this->setIsCollapsed(true);
        }
    }

    /**
     * Get grid url.
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl(
            'trueeditorder/order_edit/loadBlock',
            ['block' => 'search_grid', '_current' => true, 'collapse' => null]
        );
    }

    /**
     * Prepare the collection of the products to list in the search grid while editing the order.
     *
     * @return Grid
     */
    protected function _prepareCollection()
    {
        $attributes = $this->_catalogConfig->getProductAttributes();
        /* @var $collection Collection */
        $collection = $this->_productFactory->create()->getCollection()
            ->addAttributeToSelect(
                $attributes
            )->addAttributeToSelect(
                'sku'
            )->addAttributeToFilter(
                'type_id',
                $this->_salesConfig->getAvailableProductTypes()
            )->addAttributeToSelect(
                'gift_message_available'
            );

        $this->setCollection($collection);

        return Extended::_prepareCollection();
    }
}
