<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\View\Tab\Info\Buttons;

use Magento\Backend\Block\Template;

/**
 * Cminds TrueEditOrder adminhtml order view tab info add items button block.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class AddItems extends AbstractButton
{
    /**
     * Prepare html output.
     *
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->moduleConfig->isActive() === false
            || $this->moduleConfig->isOrderItemsEditionEnabled() === false
        ) {
            return '';
        }

        $aclResource = 'Cminds_TrueEditOrder::edit_order_items';
        if ($this->authorization->isAllowed($aclResource) === false) {
            return '';
        }
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderId = $this->getRequest()->getParam('order_id');
        $order = $objectManager->create('\Magento\Sales\Model\Order')->load($orderId);
        $status = $order->getStatus();
        if($status == 'order_placed' || $status == 'validation_on_hold_cs' || $status == 'generate_rx_cs' || $status == 'on_hold_cs' || $status == 'order_modified_cs'){
            return parent::_toHtml();
        }else{
            return '';
        }

        
    }
}
