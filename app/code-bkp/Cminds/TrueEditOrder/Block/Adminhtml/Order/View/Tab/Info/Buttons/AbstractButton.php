<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\View\Tab\Info\Buttons;

use Cminds\TrueEditOrder\Model\Config;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\AuthorizationInterface;

/**
 * Cminds TrueEditOrder adminhtml order view tab info abstract button block.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
abstract class AbstractButton extends Template
{
    /**
     * Module config object.
     *
     * @var Config
     */
    protected $moduleConfig;

    /**
     * Authorization object.
     *
     * @var AuthorizationInterface
     */
    protected $authorization;

    /**
     * Object constructor.
     *
     * @param Context $context      Context object.
     * @param Config  $moduleConfig Module config object.
     * @param array   $data         Data array.
     */
    public function __construct(
        Context $context,
        Config $moduleConfig,
        array $data = []
    ) {
        $this->moduleConfig = $moduleConfig;
        $this->authorization = $context->getAuthorization();

        parent::__construct(
            $context,
            $data
        );
    }
}
