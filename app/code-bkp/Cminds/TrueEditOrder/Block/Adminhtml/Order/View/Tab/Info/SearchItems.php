<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\View\Tab\Info;

use Cminds\TrueEditOrder\Model\Config;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\AuthorizationInterface;

/**
 * Cminds TrueEditOrder adminhtml order view tab info search items block.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class SearchItems extends Template
{
    /**
     * Module config object.
     *
     * @var Config
     */
    protected $moduleConfig;

    /**
     * Authorization object.
     *
     * @var AuthorizationInterface
     */
    protected $authorization;

    /**
     * Object constructor.
     *
     * @param Context $context Context object.
     * @param Config $moduleConfig Module config object.
     * @param array $data Data array.
     */
    public function __construct(
        Context $context,
        Config $moduleConfig,
        array $data = []
    ) {
        $this->moduleConfig = $moduleConfig;
        $this->authorization = $context->getAuthorization();

        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Prepare html output.
     *
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->moduleConfig->isActive() === false
            || $this->moduleConfig->isOrderItemsEditionEnabled() === false
        ) {
            return '';
        }

        $aclResource = 'Cminds_TrueEditOrder::edit_order_items';
        if ($this->authorization->isAllowed($aclResource) === false) {
            return '';
        }

        return parent::_toHtml();
    }

    /**
     * Retrieve order id.
     *
     * @return int
     */
    public function getOrderId()
    {
        return $this->getRequest()->getParam('order_id');
    }

    /**
     * Retrieve add order items url.
     *
     * @return string
     */
    public function getAddItemsUrl()
    {
        return $this->getUrl('trueeditorder/order_items/addPost');
    }

    /**
     * Retrieve edit order items url.
     *
     * @return string
     */
    public function getEditItemsUrl()
    {
        return $this->getUrl('trueeditorder/order_items/editPost');
    }

    /**
     * @return int
     */
    public function needToAskConfirmation()
    {
        return intval($this->moduleConfig->needToSendRefundMessage());
    }
}
