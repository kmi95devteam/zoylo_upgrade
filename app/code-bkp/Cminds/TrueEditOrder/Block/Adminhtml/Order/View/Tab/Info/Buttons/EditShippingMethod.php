<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\View\Tab\Info\Buttons;

use Magento\Backend\Block\Template;

/**
 * Cminds TrueEditOrder adminhtml order view tab info edit shipping method
 * button block.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class EditShippingMethod extends AbstractButton
{
    /**
     * Prepare html output.
     *
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->moduleConfig->isActive() === false
            || $this->moduleConfig->isOrderShippingMethodEditionEnabled() === false
        ) {
            return '';
        }

        $aclResource = 'Cminds_TrueEditOrder::edit_order_shipping_method';
        if ($this->authorization->isAllowed($aclResource) === false) {
            return '';
        }

        return parent::_toHtml();
    }

    /**
     * Retrieve shipping method edit url.
     *
     * @return string
     */
    public function getEditUrl()
    {
        return $this->getUrl(
            'trueeditorder/order_shippingMethod/edit',
            ['order_id' => $this->getRequest()->getParam('order_id')]
        );
    }
}
