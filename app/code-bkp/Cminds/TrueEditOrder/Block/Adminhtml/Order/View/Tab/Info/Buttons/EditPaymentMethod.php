<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\View\Tab\Info\Buttons;

class EditPaymentMethod extends AbstractButton
{
    /**
     * {@inheritdoc}
     */
    protected function _toHtml()
    {
        if ($this->moduleConfig->paymentEditAllowed() &&
            $this->authorization->isAllowed('Cminds_TrueEditOrder::edit_order_payment_method')
        ) {
            return parent::_toHtml();
        }
        return '';
    }

    /**
     * @return string
     */
    public function getEditUrl()
    {
        return $this->getUrl(
            'trueeditorder/order/paymentedit',
            ['order_id' => $this->getRequest()->getParam('order_id')]
        );
    }
}
