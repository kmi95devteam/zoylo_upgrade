<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\View\Tab\Info\Buttons;

use Magento\Backend\Block\Template;

/**
 * Cminds TrueEditOrder adminhtml order view tab info remove order note
 * button block.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class RemoveOrderNote extends AbstractButton
{
    /**
     * Template path.
     *
     * @var string
     */
    protected $_template = 'Cminds_TrueEditOrder::order/view/tab/info/buttons/removeordernote.phtml';

    /**
     * Item id.
     *
     * @var int|null
     */
    protected $itemId;

    /**
     * Item id setter.
     *
     * @param int $itemId Item id.
     *
     * @return EditOrderNote
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * Item id getter.
     *
     * @return int|null
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Prepare html output.
     *
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->moduleConfig->isActive() === false
            || $this->moduleConfig->isOrderNotesRemovingEnabled() === false
        ) {
            return '';
        }

        $aclResource = 'Cminds_TrueEditOrder::remove_order_notes';
        if ($this->authorization->isAllowed($aclResource) === false) {
            return '';
        }

        return parent::_toHtml();
    }

    /**
     * Retrieve order note remove url.
     *
     * @return string
     */
    public function getRemoveUrl()
    {
        return $this->getUrl(
            'trueeditorder/order_history/removePost',
            ['note_id' => $this->getItemId()]
        );
    }
}
