<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\View\Tab\Info\Buttons;

use Magento\Backend\Block\Template;

/**
 * Cminds TrueEditOrder adminhtml order view tab info edit order note
 * button block.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class EditOrderNote extends AbstractButton
{
    /**
     * Template path.
     *
     * @var string
     */
    protected $_template = 'Cminds_TrueEditOrder::order/view/tab/info/buttons/editordernote.phtml';

    /**
     * Item id.
     *
     * @var int|null
     */
    protected $itemId;

    /**
     * Item id setter.
     *
     * @param int $itemId Item id.
     *
     * @return EditOrderNote
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * Item id getter.
     *
     * @return int|null
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Prepare html output.
     *
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->moduleConfig->isActive() === false
            || $this->moduleConfig->isOrderNotesEditionEnabled() === false
        ) {
            return '';
        }

        $aclResource = 'Cminds_TrueEditOrder::edit_order_notes';
        if ($this->authorization->isAllowed($aclResource) === false) {
            return '';
        }

        return parent::_toHtml();
    }

    /**
     * Retrieve order note edit url.
     *
     * @return string
     */
    public function getEditUrl()
    {
        return $this->getUrl(
            'trueeditorder/order_history/edit',
            ['note_id' => $this->getItemId()]
        );
    }
}
