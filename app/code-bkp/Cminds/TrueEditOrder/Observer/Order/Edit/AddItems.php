<?php

namespace Cminds\TrueEditOrder\Observer\Order\Edit;

use Magento\Framework\Event\Observer;

/**
 * Cminds TrueEditOrder order edit add items event observer.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class AddItems extends AbstractObserver
{
    /**
     * Handle order edit add items event.
     * Add proper order comment which will describe changes.
     *
     * @param Observer $observer Observer object.
     *
     * @return AddItems
     */
    public function execute(Observer $observer)
    {
        $flag = $this->moduleConfig
            ->isCreateOrderNoteAfterOrderItemsEditionEnabled();
        if ($flag === false) {
            return $this;
        }

        $order = $observer->getOrder();
        $items = $observer->getItems();

        $comment = __(
            'User %1 has added item(s) to the order.',
            $this->getUser()->getUserName()
        );
        $comment .= '<br><br>';

        $comment .= __('Added items:');
        $comment .= '<br>';

        foreach ($items as $item) {
            $comment .= sprintf(
                '- %sx %s (%s).',
                (int)$item->getQtyOrdered(),
                $item->getName(),
                $item->getSku()
            );
            $comment .= '<br>';
        }

        $this->orderHistory->addComment(
            $order->getId(),
            $comment
        );

        return $this;
    }
}
