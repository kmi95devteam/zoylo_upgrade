<?php

namespace Cminds\TrueEditOrder\Plugin\Sales\Block\Order\View;

use Magento\Framework\AuthorizationInterface;
use Magento\Framework\View\LayoutInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Sales\Block\Adminhtml\Order\View;
use Cminds\TrueEditOrder\Model\Config as ModuleConfig;

/**
 * Class DeleteButton - add delete button and it's logic to order view page
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 */
class DeleteButton
{
    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @var ModuleConfig
     */
    protected $config;

    /**
     * @var AuthorizationInterface
     */
    protected $_authorization;

    /**
     * AddDeleteButton constructor.
     * @param JsonHelper $jsonHelper
     * @param ModuleConfig $moduleConfig
     * @param AuthorizationInterface $authorization
     */
    public function __construct(
        JsonHelper $jsonHelper,
        ModuleConfig $moduleConfig,
        AuthorizationInterface $authorization
    )
    {
        $this->jsonHelper = $jsonHelper;
        $this->config = $moduleConfig;
        $this->_authorization = $authorization;
    }

    /**
     * @param View $object
     * @param LayoutInterface $layout
     * @return array
     */
    public function beforeSetLayout(View $object, LayoutInterface $layout)
    {
        if ( $this->config->isActive() && $this->config->isOrderDeletionEnabled() ) {
            $object->addButton(
                'order_delete',
                [
                    'label' => __('Delete'),
                    'class' => 'delete',
                    'id' => 'cminds-trueeditorder-order-delete-button',
                    'data_attribute' => [
                        'url' => $object->getUrl(
                            'trueeditorder/*/delete',
                            ['order_id' => $object->getOrder()->getId()]
                        )
                    ]
                ]);
        }

        return [$layout];
    }

    /**
     * Require js scripts an add button click event listner script
     * @param View $object
     * @param $result
     * @return string
     */
     public function afterGetFormScripts(View $object, $result)
    {
        if ( $this->config->isActive() && $this->config->isOrderDeletionEnabled() ) {
            $result .= '<script type="text/x-magento-init">{ "*": { "trueeditorderdelete": {} } }</script>';
        }
        return $result;
    }
}