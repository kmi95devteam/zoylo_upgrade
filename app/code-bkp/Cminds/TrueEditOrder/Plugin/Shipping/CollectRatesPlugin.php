<?php

namespace Cminds\TrueEditOrder\Plugin\Shipping;

use Temando\Shipping\Plugin\Shipping\CollectRatesPlugin as TemandoCollectRatesPlugin;

class CollectRatesPlugin
{
    public function aroundBeforeCollectRates(TemandoCollectRatesPlugin $subject, callable $proceed, ...$args)
    {
        try {
            $result = $proceed(...$args);

            return $result;
        } catch (\Exception $exception) {
            return null;
        }
    }
}
