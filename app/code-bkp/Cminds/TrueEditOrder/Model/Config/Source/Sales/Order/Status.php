<?php

namespace Cminds\TrueEditOrder\Model\Config\Source\Sales\Order;

use Magento\Sales\Model\Order\Config;

class Status implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var Config
     */
    protected $orderConfig;

    /**
     * @param Config $orderConfig
     */
    public function __construct(Config $orderConfig)
    {
        $this->orderConfig = $orderConfig;
    }

    /**
     * Get all statuses as array.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $statuses = $this->orderConfig->getStatuses();

        $options = [];
        foreach ($statuses as $code => $label) {
            $options[] = ['value' => $code, 'label' => $label];
        }

        return $options;
    }
}
