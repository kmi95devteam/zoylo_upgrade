<?php

namespace Cminds\TrueEditOrder\Model\Order;

use Magento\Quote\Model\Quote\Address\ToOrder as ToOrderConverter;
use Magento\Sales\Api\Data\OrderInterfaceFactory as OrderFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Sales\Api\Data\OrderInterface;

class Dummy
{
    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var ToOrderConverter
     */
    protected $quoteAddressToOrder;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * Object constructor.
     *
     * @param OrderFactory   $orderFactory
     * @param ToOrderConverter $quoteAddressToOrder
     * @param DataObjectHelper $dataObjectHelper
     */
    public function __construct(
        OrderFactory $orderFactory,
        ToOrderConverter $quoteAddressToOrder,
        DataObjectHelper $dataObjectHelper
    ) {
        $this->orderFactory = $orderFactory;
        $this->quoteAddressToOrder = $quoteAddressToOrder;
        $this->dataObjectHelper = $dataObjectHelper;
    }

    /**
     * Generate dummy order based on existing quote.
     * Logic based on submitQuote method in \Magento\Quote\Model\QuoteManagement but without order placing.
     *
     * @param \Magento\Quote\Model\Quote $quote
     *
     * @return OrderInterface
     */
    public function generateDummyOrder($quote)
    {
        $dummyOrder = $this->orderFactory->create();
        if ($quote->isVirtual()) {
            $this->dataObjectHelper->mergeDataObjects(
                OrderInterface::class,
                $dummyOrder,
                $this->quoteAddressToOrder->convert($quote->getBillingAddress())
            );
        } else {
            $this->dataObjectHelper->mergeDataObjects(
                OrderInterface::class,
                $dummyOrder,
                $this->quoteAddressToOrder->convert($quote->getShippingAddress())
            );
        }

        return $dummyOrder;
    }
}
