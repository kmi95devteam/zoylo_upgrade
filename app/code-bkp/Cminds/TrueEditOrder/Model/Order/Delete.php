<?php
namespace Cminds\TrueEditOrder\Model\Order;

use Magento\Sales\Api\OrderRepositoryInterface;


/*
 * Implement order delete logic
 **/
class Delete {
    
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository
    ) {
        $this->orderRepository = $orderRepository;
    }
    
    /**
     *  iterate through order associated objects an delete them ( invoices, shipments order memos, etc )
     *  @param \Magento\Sales\Model\Order\Interceptor $orderObject
     */
    public function deleteOrder( \Magento\Sales\Model\Order $orderObject ) {
        // delete order items
        $items = $orderObject->getAllItems();
        foreach ($items as $item) {
            $item->delete();
        }
        
        // delete invocies and associated items
        $invoices = $orderObject->getInvoiceCollection();
        foreach ($invoices as $invoice){
            $items = $invoice->getAllItems();
            foreach ($items as $item) {
                $item->delete();
            }
            //delete invoice
            $invoice->delete();
        }
        
        //delete all creditnotes and associated items
        $creditnotes = $orderObject->getCreditmemosCollection();
        foreach ($creditnotes as $creditnote){
            $items = $creditnote->getAllItems();
            foreach ($items as $item) {
                $item->delete();
            }
            //delete credit note
            $creditnote->delete();
        }

        //delete all shipments and associated items
        $shipments = $orderObject->getShipmentsCollection();
        foreach ($shipments as $shipment){
            $items = $shipment->getAllItems();
            foreach ($items as $item) {
                $item->delete();
            }
            //delete shipment
            $shipment->delete();
        }
        
        // delete order itself
        $this->orderRepository->delete( $orderObject );
    }
}
