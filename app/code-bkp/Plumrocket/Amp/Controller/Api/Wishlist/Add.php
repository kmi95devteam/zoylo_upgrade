<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket Amp v2.x.x
 * @copyright   Copyright (c) 2018 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

namespace Plumrocket\Amp\Controller\Api\Wishlist;

use \Magento\Framework\Message\MessageInterface;
use Magento\Framework\Controller\ResultFactory;

class Add extends \Magento\Wishlist\Controller\Index\Add
{
    /**
     * @var null|\Plumrocket\Amp\Helper\Data
     */
    private $dataHelper = null;

    /**
     * @var \Magento\Framework\Escaper|null
     */
    private $escaper = null;

    /**
     * Add constructor.
     *
     * @param \Magento\Framework\App\Action\Context                  $context
     * @param \Magento\Customer\Model\Session                        $customerSession
     * @param \Magento\Wishlist\Controller\WishlistProviderInterface $wishlistProvider
     * @param \Magento\Catalog\Api\ProductRepositoryInterface        $productRepository
     * @param \Magento\Framework\Data\Form\FormKey\Validator         $formKeyValidator
     * @param \Plumrocket\Amp\Helper\Data                            $dataHelper
     * @param \Plumrocket\Amp\Model\Validator                        $validator
     * @param \Magento\Framework\Escaper                             $escaper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Wishlist\Controller\WishlistProviderInterface $wishlistProvider,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Plumrocket\Amp\Helper\Data $dataHelper,
        \Plumrocket\Amp\Model\Validator $validator,
        \Magento\Framework\Escaper $escaper
    ) {
        parent::__construct(
            $context,
            $customerSession,
            $wishlistProvider,
            $productRepository,
            $formKeyValidator
        );
        $this->dataHelper = $dataHelper;
        $this->escaper = $escaper;
        $this->formKeyValidator = $validator;
    }

    /**
     * Refactor response to json
     * Ignore form key validation
     *
     * @return \Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\Result\Redirect
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $errorCount = $this->messageManager->getMessages()->getCountByType(MessageInterface::TYPE_ERROR);
        $successCount = $this->messageManager->getMessages()->getCountByType(MessageInterface::TYPE_SUCCESS);
        if (!$this->_request->isAjax()) {
            // For forwards, example: customer/account/loginPost/
            /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

            parent::execute();

            if ($successCount < $this->messageManager->getMessages()->getCountByType(MessageInterface::TYPE_SUCCESS)) {
                $wishlist = $this->wishlistProvider->getWishlist();
                return $resultRedirect->setPath('wishlist', ['wishlist_id' => $wishlist->getId()]);
            } else {
                return $resultRedirect->setPath('wishlist/');
            }
        } else {
            // For amp xhr request
            $this->_request->setParam('ajax', null);
            /** @var \Magento\Framework\Controller\Result\Json $result */
            $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $result->setData([]);

            // If customer not logged in - redirect to login
            if (!$this->_customerSession->isLoggedIn()) {
                $this->dataHelper->sanitizeHttpHeaders();
                $this->prepareReferer();
                $result->setHttpResponseCode(400);
                $result->setData(['errors' => __('You will be redirected to the login page')]);
                $this->dataHelper->setFormRedirectHeaders(
                    $this->_url->getUrl(\Magento\Customer\Model\Url::ROUTE_ACCOUNT_LOGIN)
                );
                return $result;
            }

            // Magento add to wishlist
            parent::execute();

            // Handle Error messages
            if ($errorCount < $this->messageManager->getMessages()->getCountByType(MessageInterface::TYPE_ERROR)) {
                $items = $this->messageManager->getMessages(true)->getItemsByType(MessageInterface::TYPE_ERROR);
                $errors = [];
                foreach ($items as $message) {
                    $errors[] = $message->getText();
                }

                $result->setData(['errors' => implode(', ', $errors)]);
                $result->setHttpResponseCode(400);
                $this->dataHelper->sanitizeHttpHeaders();
                return $result;
            }

            // Handle Success messages
            if ($successCount < $this->messageManager->getMessages()->getCountByType(MessageInterface::TYPE_SUCCESS)) {
                $items = $this->messageManager->getMessages(false)->getItemsByType(MessageInterface::TYPE_SUCCESS);
                $messageText = null;
                foreach ($items as $message) {
                    if (null === $message && isset($message->getData()['product_name'])) {
                        $messageText = $this->escaper->escapeHtml(__(
                            '%1 has been added to your Wish List.',
                            $this->escaper->escapeHtml($message->getData()['product_name']) ?: null
                        ));
                    }
                }

                $result->setData(['message' => $messageText]);
                $wishlist = $this->wishlistProvider->getWishlist();
                $this->dataHelper->sanitizeHttpHeaders();
                $this->dataHelper->setFormRedirectHeaders(
                    $this->_url->getUrl('wishlist/index/index', ['wishlist_id' => $wishlist->getId()])
                );
            }

            return $result;
        }
    }

    /**
     * Set referer and before data
     *
     * @return $this
     */
    private function prepareReferer()
    {
        if (!$this->_customerSession->getBeforeWishlistUrl()) {
            $this->_customerSession->setBeforeWishlistUrl($this->_redirect->getRefererUrl());
        }
        $this->_customerSession->setBeforeWishlistRequest($this->_request->getParams());
        $this->_customerSession->setBeforeRequestParams($this->_customerSession->getBeforeWishlistRequest());
        $this->_customerSession->setBeforeModuleName('pramp');
        $this->_customerSession->setBeforeControllerName('api_wishlist');
        $this->_customerSession->setBeforeAction('add');

        return $this;
    }
}
