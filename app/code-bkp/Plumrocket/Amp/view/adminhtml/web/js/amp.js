/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_Amp
 * @copyright   Copyright (c) 2017 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

require([
    'jquery',
    'domReady!'
], function ($) {
    'use strict';

    function decodeDeclaration(declaration)
    {
        return Base64.decode(decodeURIComponent(declaration).replace(/,/g, '='));
    }

    window.PRAMP = {
        parseLink: function (modal) {
            var input = modal.focussedElement.parentElement.querySelector('input');
            var image = modal.focussedElement.parentElement.querySelector('img');
            var label = modal.focussedElement.parentElement.querySelector('.control-value');
            if (typeof input !== 'undefined' && input.value && input.value.indexOf('/___directive/')) {
                var result = /___directive\/(.*?),*\//ig.exec(input.value);
                if (result) {
                    var declaration = decodeDeclaration(result[1]);
                    var imagePath = /url="(.*?)"/ig.exec(declaration)[1];
                    input.value = imagePath;
                    image.src = image.dataset.mediaUrl + imagePath;
                    label.innerText = imagePath;
                }
            }
        }
    };

    if (location.pathname.indexOf('amp_image_slider')) {
        $('select[name*="show_slide"]').each(function (index, select) {
            var regex = /[+-]?\d+(?:\.\d+)?/g;
            var match = regex.exec(select.name);
            if (match && match[0]) {
                var div = document.querySelector('div[class*="image' + match[0] + '"]');
                $(select).on('change', function () {
                    $(div).toggle();
                    div.querySelector('input').classList.toggle('required-entry');
                });
                if ('0' === select.value) {
                    $(div).hide();
                    div.querySelector('input').classList.toggle('required-entry');
                }
            }
        });
    }
});
