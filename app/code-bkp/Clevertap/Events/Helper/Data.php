<?php
namespace Clevertap\Events\Helper;
use Magento\Store\Model\Store;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $scopeConfigInterface;
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface
            //array $data = []
    )
    {
               $this->scopeConfigInterface = $scopeConfigInterface;
               parent::__construct($context);
    }
    
    
                        
    /**
     * Retrieve config value
     *
     * @return string
     */
    public function getClevertapEnableStatus()
    {
        $clevertapEnableStatus =$this->scopeConfigInterface->getValue('clevertap/events_configurations/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if($clevertapEnableStatus==1){
            return true;
        }else{
            return false;
        }
    }
    
    public function getClevertapAccountId(){
        $clevertapStatus = $this->getClevertapEnableStatus();
        if($clevertapStatus==1){
            return $this->scopeConfigInterface->getValue('clevertap/events_configurations/accountid', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        }else{
            return '';
        }
    }
    
    public function getHomePageStatus(){
        $clevertapEnableStatus =$this->scopeConfigInterface->getValue('clevertap/events_configurations/home_page', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $cleverTopStatus = $this->getClevertapEnableStatus();
        if($clevertapEnableStatus==1 && $cleverTopStatus==1){
            return true;
        }else{
            return false;
        }
    }
    
    public function getSearchListStatus(){
        $clevertapEnableStatus =$this->scopeConfigInterface->getValue('clevertap/events_configurations/search_list', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $cleverTopStatus = $this->getClevertapEnableStatus();
        if($clevertapEnableStatus==1 && $cleverTopStatus==1 ){
            return true;
        }else{
            return false;
        }
    }
    
    public function getCategoryListStatus(){
        $clevertapEnableStatus =$this->scopeConfigInterface->getValue('clevertap/events_configurations/category_list', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $cleverTopStatus = $this->getClevertapEnableStatus();
        if($clevertapEnableStatus==1 && $cleverTopStatus==1 ){
            return true;
        }else{
            return false;
        }
    }
    
    public function getProductViewStatus(){
        $clevertapEnableStatus =$this->scopeConfigInterface->getValue('clevertap/events_configurations/product_view', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $cleverTopStatus = $this->getClevertapEnableStatus();
        if($clevertapEnableStatus==1 && $cleverTopStatus==1 ){
            return true;
        }else{
            return false;
        }
    }
    
    public function getUploadPrescriptionStatus(){
        $clevertapEnableStatus =$this->scopeConfigInterface->getValue('clevertap/events_configurations/upload_prescription', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $cleverTopStatus = $this->getClevertapEnableStatus();
        if($clevertapEnableStatus==1 && $cleverTopStatus==1 ){
            return true;
        }else{
            return false;
        }
    }
    
    public function getUploadPrescInitializeStatus(){
        $clevertapEnableStatus =$this->scopeConfigInterface->getValue('clevertap/events_configurations/upload_prescription_initialize', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $cleverTopStatus = $this->getClevertapEnableStatus();
        if($clevertapEnableStatus==1 && $cleverTopStatus==1 ){
            return true;
        }else{
            return false;
        }
    }
    
    
    
    public function getAddtocartStatus(){
        $clevertapEnableStatus =$this->scopeConfigInterface->getValue('clevertap/events_configurations/addtocart', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $cleverTopStatus = $this->getClevertapEnableStatus();
        if($clevertapEnableStatus==1 && $cleverTopStatus==1 ){
            return true;
        }else{
            return false;
        }
    }
    
    public function getCartViewStatus(){
        $clevertapEnableStatus =$this->scopeConfigInterface->getValue('clevertap/events_configurations/cart_view', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $cleverTopStatus = $this->getClevertapEnableStatus();
        if($clevertapEnableStatus==1 && $cleverTopStatus==1 ){
            return true;
        }else{
            return false;
        }
    }
    
    public function getCheckoutStatus(){
        $clevertapEnableStatus =$this->scopeConfigInterface->getValue('clevertap/events_configurations/checkout', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $cleverTopStatus = $this->getClevertapEnableStatus();
        if($clevertapEnableStatus==1 && $cleverTopStatus==1 ){
            return true;
        }else{
            return false;
        }
    }
    
    public function getOrderSuccessPaylaterStatus(){
        $clevertapEnableStatus =$this->scopeConfigInterface->getValue('clevertap/events_configurations/order_success_paylater', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $cleverTopStatus = $this->getClevertapEnableStatus();
        if($clevertapEnableStatus==1 && $cleverTopStatus==1 ){
            return true;
        }else{
            return false;
        }
    }
    
    public function getOrderSuccessPayonlineStatus(){
        $clevertapEnableStatus =$this->scopeConfigInterface->getValue('clevertap/events_configurations/order_success_payonline', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $cleverTopStatus = $this->getClevertapEnableStatus();
        if($clevertapEnableStatus==1 && $cleverTopStatus==1 ){
            return true;
        }else{
            return false;
        }
    }
    
    public function getRxInitiateCheckoutStatus(){
        $clevertapEnableStatus =$this->scopeConfigInterface->getValue('clevertap/events_configurations/rx_initiate_checkout', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $cleverTopStatus = $this->getClevertapEnableStatus();
        if($clevertapEnableStatus==1 && $cleverTopStatus==1 ){
            return true;
        }else{
            return false;
        }
    }
    
    public function getRxOrderSuccessOnlyStatus(){
        $clevertapEnableStatus =$this->scopeConfigInterface->getValue('clevertap/events_configurations/rx_order_place', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $cleverTopStatus = $this->getClevertapEnableStatus();
        if($clevertapEnableStatus==1 && $cleverTopStatus==1 ){
            return true;
        }else{
            return false;
        }
    }

    public function getDefaultHeaderScriptStatus(){
        $clevertapHeaderScripEnableStatus =$this->scopeConfigInterface->getValue('clevertap/events_configurations/default_header_script', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $cleverTopStatus = $this->getClevertapEnableStatus();
        $getClevertapAccountId = $this->getClevertapAccountId();
        if($clevertapHeaderScripEnableStatus==1 && $cleverTopStatus==1 && $getClevertapAccountId!='' ){
            return true;
        }else{
            return false;
        }
    }
    
}
