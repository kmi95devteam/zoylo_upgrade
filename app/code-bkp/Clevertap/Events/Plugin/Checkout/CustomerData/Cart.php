<?php
namespace Clevertap\Events\Plugin\Checkout\CustomerData;
class Cart {

    protected $clevertapHelper;
    protected $_registry;
    protected $_categoryCollectionFactory;
    protected $salt;
    protected $objectManager;
    public function __construct
    (
        \Clevertap\Events\Helper\Data $clevertapHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \I95Dev\Salts\Model\SaltFactory $salt,
        array $data = []
    ) 
    {
        $this->clevertapHelper = $clevertapHelper;
        $this->_registry = $registry;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->salt = $salt;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //parent::__construct($data);

    }

    public function afterGetSectionData(\Magento\Checkout\CustomerData\Cart $subject, array $result)
    {
        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //$clevertapHelperObj = $objectManager->get('Clevertap\Events\Helper\Data');
        //$addtoCartStatus = $clevertapHelperObj->getAddtocartStatus();
        $addtoCartStatus = $this->clevertapHelper->getAddtocartStatus();
        $result['extra_data'] = '';
        if($addtoCartStatus!=''){
            $clevertapArray = $this->getClevertapInfo($result);
            $getCurrentClevertapPage = $this->getCurrentClevertapPage();
            if((!empty($clevertapArray)) && ($getCurrentClevertapPage==1)){
                $htmlscript='';
                $htmlscript .='<div id="clevertap-add2cart"><script type="text/javascript">';
                $htmlscript .= 'var currentSku = "clevertap-'.$clevertapArray['id'].'";';
                $resultvalue = "clevertaptrue".$clevertapArray['id'];
                $htmlscript .='if(localStorage.getItem(currentSku) =="'.$resultvalue.'"){ }else{';
                    $htmlscript .='clevertap.event.push("Medicine_add2cart",{';
                    $htmlscript .= '"Type" :"'.$clevertapArray['sku_type'].'",';
                    $htmlscript .= '"SKU_Name" :"'.$clevertapArray['sku'].'",';
                    $htmlscript .= '"Salt" :"'.$clevertapArray['salt'].'",';
                    $htmlscript .= '"Symptom" :"NA",';
                    $htmlscript .= '"SKU_Class" :"NA",';
                    $htmlscript .= '"SKU_Price" :"'.$clevertapArray['price'].'",';
                    $htmlscript .= '"SKU_Qty" :"'.$clevertapArray['qty'].'"';
                    $htmlscript .='});';                    
                $htmlscript .='}';
                $htmlscript .='if (!localStorage.currentSku){';
                $htmlscript .='localStorage.setItem(currentSku, "'.$resultvalue.'");';
                $htmlscript .='}';
                $htmlscript .='</script></div>';
                $result['extra_data'] = $htmlscript;
            }else{
                $result['extra_data'] = "";
            }
        }
        return $result;
    }
    
    /*
     * @Params $quoteResult
     * Return Latest added quote item data array
     */
    public function getClevertapInfo($quoteResult){
        $qItemsCreatedAt = $qItemsUpdatedAt = array();
        $clevertapArray =array();
        if($quoteResult!=''){
            //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $quoteItemObj = $this->objectManager->get('Magento\Quote\Model\Quote\Item');
            foreach($quoteResult['items'] as $quoteItemData){
                $quoteItemId = $quoteItemData['item_id'];
                
                $quoteItem = $quoteItemObj->load($quoteItemId);
                $quoteItemData = $quoteItem->getData();
                $qItemsCreatedAt[$quoteItemId] = strtotime($quoteItemData['created_at']);
                $qItemsUpdatedAt[$quoteItemId] = strtotime($quoteItemData['updated_at']);
            }
            //if($qItemsCreatedAt!='' && $qItemsUpdatedAt!=''){
            if(!empty($qItemsCreatedAt) && !empty($qItemsUpdatedAt)){
                $createdMaxDate = max($qItemsCreatedAt);
                $updatedMaxDate = max($qItemsUpdatedAt);
                if($updatedMaxDate > $createdMaxDate){
                    $updatedKeyLatest = array_keys($qItemsUpdatedAt,max($qItemsUpdatedAt));
                    $latestQuoteItemId = $updatedKeyLatest[0];
                }else{
                    $createdKeyLatest = array_keys($qItemsCreatedAt,max($qItemsCreatedAt));
                    $latestQuoteItemId = $createdKeyLatest[0];
                }
                if($latestQuoteItemId!=''){
                    $latestQuoteItem = $quoteItemObj->load($latestQuoteItemId);
                    $latestQuoteItemData = $latestQuoteItem->getData();
                    /*$latestQuoteItemid = $latestQuoteItemData['item_id'];
                    $latestQuoteItemSku = $latestQuoteItemData['name'];
                    $latestQuoteItemPrice = round($latestQuoteItemData['price']);
                    $latestQuoteItemQty = $latestQuoteItemData['qty'];

                    $clevertapArray['id'] = $latestQuoteItemid;
                    $clevertapArray['sku'] = $latestQuoteItemSku;
                    $clevertapArray['price'] = $latestQuoteItemPrice;
                    $clevertapArray['qty'] = $latestQuoteItemQty;*/
                    $latestQuoteItemid = $latestQuoteItemData['item_id'];
                    $latestQuoteItemSku = $latestQuoteItemData['sku'];
                    $latestQuoteItemName = $latestQuoteItemData['name'];
                    $latestQuoteItemPrice = round($latestQuoteItemData['price']);
                    $latestQuoteItemQty = round($latestQuoteItemData['qty']);
                    
                    $productCollectionFactory = $this->objectManager->get('\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
                    $collection = $productCollectionFactory->create();
                    $collection->addAttributeToSelect('*');
                    $collection->addAttributeToFilter('sku', array('eq' => $latestQuoteItemSku));
                    $count = count($collection->getData());
                    if($count>0){
                        $fcol = $collection->getData();
                        foreach($fcol as $product){
                        $productEntityId = $product['entity_id'];
                            if(!empty($productEntityId)){
                                $productInfo = $this->objectManager->create('Magento\Catalog\Model\Product')->load($productEntityId);
                                $clevertapArray['id'] = $latestQuoteItemid;
                                $clevertapArray['sku'] = $latestQuoteItemName;
                                $clevertapArray['price'] = $latestQuoteItemPrice;
                                $clevertapArray['qty'] = $latestQuoteItemQty;
                                $rxValue = '';
                                if($productInfo->getPrescriptionRequired() == 'required') {
                                   $rxValue = "Rx"; 
                                }elseif($productInfo->getPrescriptionRequired() == 'not_required'){
                                   $rxValue = "OTC";
                                }

                                if($rxValue!=''){
                                    $clevertapArray['sku_type'] = $rxValue;
                                }else{
                                    $clevertapArray['sku_type'] = 'NA';
                                }
                                
                                $skuSaltValue = $productInfo->getSaltComposition();
                                if($skuSaltValue!=''){
                                    $clevertapArray['salt'] = $skuSaltValue;
                                }else{
                                    $clevertapArray['salt'] = 'NA';
                                }
                                
                            }
                        }
                    }
                }
            }
        }
        return $clevertapArray;
    }

    public function getCurrentProductSaltValue($productEntityId)
    {        
        $productSalt = "";
        $pId = $productEntityId;
        if($pId!=''){
            //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productObjData = $this->objectManager->create('Magento\Catalog\Model\Product')->load($pId);
            $zoyloProductSalt =  $productObjData->getSaltName();
            if($zoyloProductSalt!=''){
                $productSalt = $zoyloProductSalt;
            }
        }
        return $productSalt;
    }

    public function getSaltValue($productEntityId){
        $saltNamesArray =array();
        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //$quoteItemObj = $objectManager->get('Magento\Quote\Model\Quote\Item');
         $saltObj = $this->salt->create();
         $saltId = $this->getCurrentProductSaltValue($productEntityId);
         if($saltId!=''){
            $explodeSaltIds = explode(",",$saltId);
                $count = count($explodeSaltIds);
                if($count>0){
                    foreach ($explodeSaltIds as $value) {
                        $saltData = $saltObj->load($value);
                        $saltNamesArray[] = $saltData->getName();
                    }
                }
            $implodeSaltNames = implode(",", $saltNamesArray);

            if($implodeSaltNames!=''){
                return $implodeSaltNames;
            }else{
                return 'NA';
            }
        }else{
            return 'NA';
        }
                
    }


    /**
    * Get customer mobile no
    * Return string
    */
    public function getCustomerMobileNo(){
        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $this->objectManager->get('Magento\Customer\Helper\Session\CurrentCustomer');
        $customerId = $customerObj->getCustomerId();
        if($customerId!=''){
            $customer = $this->objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
            $customerInfo = $customer->getData();
            $customerNumber = $customerInfo['customer_number'];
            return $customerNumber;
        }else{
            return '';
        }
    }

    public function getCurrentClevertapPage(){
        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $appRequest = $this->objectManager->get('Magento\Framework\App\Request\Http');
        $currentPage = $appRequest->getFullActionName();
        if($currentPage=="customer_section_load" || $currentPage=="cms_index_index" || $currentPage=="catalog_category_view" || $currentPage=="catalog_product_view" || $currentPage=="checkout_cart_index"){
            return true;
        }else{
            return false;
        }
    }
    
}

                    
                