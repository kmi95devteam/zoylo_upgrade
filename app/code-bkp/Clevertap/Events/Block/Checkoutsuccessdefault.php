<?php
namespace Clevertap\Events\Block;
class Checkoutsuccessdefault extends \Magento\Framework\View\Element\Template {
    protected $clevertapHelper;
    protected $_registry;
    protected $objectManager;
    protected $customerSession;
    protected $customerModel;
    protected $checkoutSession;
    protected $orderFactory;
    protected $salt;
    public function __construct
    (
        \Magento\Catalog\Block\Product\Context $context,
        \Clevertap\Events\Helper\Data $clevertapHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Customer $customerModel,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \I95Dev\Salts\Model\SaltFactory $salt,
        array $data = []
    ) 
    {
        $this->clevertapHelper = $clevertapHelper;
        $this->_registry = $registry;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->customerSession = $customerSession;
        $this->customerModel = $customerModel;
        $this->checkoutSession = $checkoutSession;
        $this->orderFactory = $orderFactory;
        $this->salt = $salt;
        parent::__construct($context, $data);

    }
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    
    public function getClevertapStatus(){
        return $this->clevertapHelper->getRxOrderSuccessOnlyStatus();
    }

    public function getClevertapPaylaterStatus(){
        return $this->clevertapHelper->getOrderSuccessPaylaterStatus();
    }

    public function getCustomerMobileNo(){
        $customerNumber = "";
       $clevertapStatus = $this->getClevertapStatus();
       if($clevertapStatus!=''){
            $customerId = $this->customerSession->getCustomer()->getId();
            if($customerId!=''){
                $customer = $this->customerModel->load($customerId);
                $customerInfo = $customer->getData();
                $customerNumber = $customerInfo['customer_number'];  
            }else{
                $customerNumber = "";
            }
       }
       return $customerNumber;
    }

    public function getCheckoutSession() 
    {
        return $this->checkoutSession;
    }

    public function checkRxItemExistsOrNot()
    {
        $clevertapStatus = $this->getClevertapStatus();
        if($clevertapStatus!=''){
            $quoteId = $this->getCheckoutSession()->getQuoteId();
            $itemsIds = array();
            if(!empty($quoteId)){
                $cartItems = $this->getCheckoutSession()->getQuote()->getAllVisibleItems();
                foreach ($cartItems as $cartItem) {
                    $itemsIds[] = $cartItem->getProduct()->getSku();
                }
                if(!empty($itemsIds)){
                    if (in_array("Free", $itemsIds)) {
                        $rxItemExists = true;
                    }else{
                        $rxItemExists = false;
                    }
                }
            }
        }else{
            $rxItemExists = false;
        }
        return $rxItemExists;
    }
    
    /*
     *  Get order object from checkout session using order factory
     *  return object or false
     */
    public function getOrder()
    {
       $clevertapStatus = $this->getClevertapStatus();
       if($clevertapStatus!=''){
           return $orderData = $this->orderFactory->create()->loadByIncrementId($this->checkoutSession->getLastRealOrderId());
       }else{
           return '';
       }
    }

    public function getOrderId(){
        $order = $this->getOrder();
        if(!empty($order)){
            $orderId = $order->getId();
        }else{
            $orderId = '';
        }
        return $orderId;
    }
    
    
    /*
     *  Check Rx order item or not
     *  return rx item flag true or false
     */
    public function checkRxOrderItem(){
        $rxItemFlag = false;
        //$skuArray = array();
        $order = $this->getOrder();
        if(!empty($order)){
            $items = $order->getAllItems();
            $total_items = count($items);
            if($total_items==1){
                foreach ($items as $item) {
                    //$skuArray[] = $item->getSku(); 
                    if ($item->getSku() == 'Free') {
                        $rxItemFlag = true;
                    }
                }
            }
        }
        return $rxItemFlag;
    }

    /*
     *  Check order items
     *  return rx item flag true or false
     */
    public function checkDefaultOrderItems(){
        $order = $this->getOrder();
        $finalOrderArray = $itemNamesArray = $itemPriceArray = $itemSaltArray = array();
        $chargeArray = $finalSaltValueArray = array();
        $rxArray = $nonRxArray = array();
        if(!empty($order)){
            $finalOrderArray['orderTotal'] = $order->getGrandTotal();
            $finalOrderArray['incrementId'] = $order->getIncrementId();
            $couponCode = $order->getCouponCode();
            $items = $order->getAllItems();
            $total_items = count($items);
            if($total_items>0){
                //$chargeArray['orderTotal'] = $order->getGrandTotal();
                //$chargeArray['incrementId'] = $order->getIncrementId();
                $i=0;  
                foreach ($items as $item) {
                   
                   $itemNamesArray[] = $item->getName();
                   $itemPriceArray[] = round($item->getPrice());
                   $sku = $item->getSku();
                   $productId = $this->objectManager->get('Magento\Catalog\Model\Product')->getIdBySku($sku);
                   //$finalOrderArray['finalSkuType'] = $this->getSkuType($sku);
                   $productSkuType = $this->getSkuType($sku);


                   if($productSkuType == 'required') { 
                        $rxArray[] = "RX";
                        $skuItemtype = "RX";
                    }elseif($productSkuType == 'not_required'){   
                        $nonRxArray[] = "Non-Rx";
                        $skuItemtype = "Non-Rx";
                    }
                    $itemSaltArray[] = $this->getSaltValue($productId);
                    $skuSaltValue = $this->getSaltValue($productId);
                    if($skuSaltValue!=''){
                        $finalSaltValueArray = explode("+",$skuSaltValue);
                    }else{
                        $finalSaltValueArray = "NA";
                    }
                    
                    /*Ajesh code 10th July */
                    if($skuItemtype == "RX" && $skuItemtype != "Non-Rx"){
                        $finalSkuType = "Rx";
                    }
                    if($skuItemtype != "RX" && $skuItemtype == "Non-Rx"){
                        $finalSkuType = "OTC";
                    }
                    if($skuItemtype == "RX" && $skuItemtype == "Non-Rx"){
                        $finalSkuType = "Rx/OTC";
                    }

                    $chargeArray[$i]['name'] = $item->getName();
                    $chargeArray[$i]['skutype'] = $finalSkuType;
                    $chargeArray[$i]['salt'] = $finalSaltValueArray;
                    $chargeArray[$i]['price'] = round($item->getPrice());
                    $i++;
                }
            }
            if(!empty($itemNamesArray)){
                $finalOrderArray['implodeItemNames'] = implode(", ", $itemNamesArray);
            }else{
                $finalOrderArray['implodeItemNames'] = 'NA';
            }
            
            if(!empty($itemPriceArray)){
        	$finalOrderArray['implodeItemPrices'] = implode(", ", $itemPriceArray);
            }else{
                $finalOrderArray['implodeItemPrices'] = 'NA';
            }

            if(!empty($itemSaltArray)){
                $finalOrderArray['implodeSaltValues'] = implode(", ", $itemSaltArray);
            }else{
                $finalOrderArray['implodeSaltValues'] = 'NA';
            }
            
            if($couponCode!=''){
                $finalOrderArray['couponCode'] = $couponCode;
            }else{
                $finalOrderArray['couponCode'] = 'NA';
            }

            $skuType = '';
            if(!empty($rxArray) && empty($nonRxArray)){
                $skuType = "Rx";
            }
            if(empty($rxArray) && !empty($nonRxArray)){
                $skuType = "OTC";
            }
            if(!empty($rxArray) && !empty($nonRxArray)){
                $skuType = "Rx/OTC";
            }
            $finalOrderArray['finalSkuType'] = $skuType;

            if(isset($chargeArray) && !empty($chargeArray)){
                $finalOrderArray['charged'] = $chargeArray;
            }
            
        }
        return $finalOrderArray;
    }


    public function getSkuType($sku){
        $rxArray = $nonRxArray = array();
        $productSkuType="";
        if($sku !=''){
                $productCollectionFactory = $this->objectManager->get('\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
                    $collection = $productCollectionFactory->create();
                    $collection->addAttributeToSelect('*');
                    $collection->addAttributeToFilter('sku', array('eq' => $sku));
                    $count = count($collection->getData());
                    if($count>0){
                        $fcol = $collection->getData();
                        foreach($fcol as $product){
                            $productEntityId = $product['entity_id'];
                            if(!empty($productEntityId)){
                                $productInfo = $this->objectManager->create('Magento\Catalog\Model\Product')->load($productEntityId);
                                $productSkuType = $productInfo->getPrescriptionRequired();
                               
                        }
                    }
                }
            }
        /*$skuType = '';
        if(!empty($rxArray) && empty($nonRxArray)){
            $skuType = "RX";
        }
        if(empty($rxArray) && !empty($nonRxArray)){
            $skuType = "Non-RX";
        }
        if(!empty($rxArray) && !empty($nonRxArray)){
            $skuType = "Rx AND Non-RX";
        }*/
        return $productSkuType;
    }
    /*
     * Get IncrementID by using checkout session
     */
    public function getIncrementId(){
        $incrementId = $this->checkoutSession->getLastRealOrderId();
        if($incrementId!=''){
           return $incrementId;
        }else{
            return '';
        }
    }

    public function getCurrentProductSaltValue($proId)
    {        
        $productSalt = "";
        $pId = $proId; //$this->getCurrentProductId();
        if($pId!=''){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productObjData = $this->objectManager->create('Magento\Catalog\Model\Product')->load($pId);
            $zoyloProductSalt =  $productObjData->getSaltName();
            if($zoyloProductSalt!=''){
                $productSalt = $zoyloProductSalt;
            }
        }
        return $productSalt;
    }

    public function getSaltValue($proId){
        $productSalt = "NA";
        if($proId!=''){
            $productObjData = $this->objectManager->create('Magento\Catalog\Model\Product')->load($proId);
            $zoyloProductSalt =  $productObjData->getSaltComposition();
            if($zoyloProductSalt!=''){
                $productSalt = $zoyloProductSalt;
            }
        }
        return $productSalt;    
    }

    public function getSaltValueBkp($proId){
         $saltNamesArray =array();
         $saltObj = $this->salt->create();
         $saltId = $this->getCurrentProductSaltValue($proId);
         if($saltId!=''){
            $explodeSaltIds = explode(",",$saltId);
                $count = count($explodeSaltIds);
                if($count>0){
                    foreach ($explodeSaltIds as $value) {
                        $saltData = $saltObj->load($value);
                        $saltNamesArray[] = $saltData->getName();
                    }
                }
            $implodeSaltNames = implode(",", $saltNamesArray);

            if($implodeSaltNames!=''){
                return $implodeSaltNames;
            }else{
                return 'NA';
            }
        }else{
            return 'NA';
        }
                
    }
}