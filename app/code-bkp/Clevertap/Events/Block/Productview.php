<?php
namespace Clevertap\Events\Block;
class Productview extends \Magento\Framework\View\Element\Template {
    protected $clevertapHelper;
    protected $_registry;
    protected $_categoryCollectionFactory;
    //protected $productFactory;
    protected $salt;
    protected $objectManager;
    public function __construct
    (
        \Magento\Catalog\Block\Product\Context $context,
        \Clevertap\Events\Helper\Data $clevertapHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        //\Magento\Catalog\Model\ProductFactory $productFactory,
        \I95Dev\Salts\Model\SaltFactory $salt,
        array $data = []
    ) 
    {
        $this->clevertapHelper = $clevertapHelper;
        $this->_registry = $registry;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        //$this->productFactory = $productFactory;
        $this->salt = $salt;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        parent::__construct($context, $data);

    }
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    
    public function getClevertapStatus(){
        return $this->clevertapHelper->getProductViewStatus();
    }
    
    public function getClevertapAccountId(){
        return $this->clevertapHelper->getClevertapAccountId();
    }
    
    public function getCurrentProduct()
    {        
        $clevertapStatus = $this->getClevertapStatus();
        if($clevertapStatus==1){
            return $this->_registry->registry('current_product');
        }else{
            return '';
        }
    }

    public function getCurrentProductId()
    {        
        $currentProduct = $this->getCurrentProduct();
        $pId = $currentProduct->getId();
        if($pId!=''){
            return $pId;
        }else{
            return '';
        }
    }
    public function getCurrentProductType()
    {        
        $productType = "NA";
        $zoylo_product_type = "";
        $pId = $this->getCurrentProductId();
        if($pId!=''){
            //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productObjData = $this->objectManager->create('Magento\Catalog\Model\Product')->load($pId);
            $sku = $productObjData->getSku();
                  
        //  $productSkuType = $this->getSkuType($sku);
            $productSkuType = $productObjData->getPrescriptionRequired();
            if($productSkuType == 'required') { 
                $zoylo_product_type = "Rx";
            }elseif($productSkuType == 'not_required'){   
                $zoylo_product_type = "OTC";
            }
            //$zoylo_product_type =  $productObjData->getZoyloProductType();
            if($zoylo_product_type!=''){
                $productType = $zoylo_product_type;
            }
        }
        return $productType;
    }


    public function getCurrentProductSaltValue()
    {        
        $productSalt = "";
        $pId = $this->getCurrentProductId();
        if($pId!=''){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productObjData = $this->objectManager->create('Magento\Catalog\Model\Product')->load($pId);
            $zoyloProductSalt =  $productObjData->getSaltName();
            if($zoyloProductSalt!=''){
                $productSalt = $zoyloProductSalt;
            }
        }
        return $productSalt;
    }

    public function getSaltValueBkp(){
         $saltNamesArray =array();
         $saltObj = $this->salt->create();
         $saltId = $this->getCurrentProductSaltValue();
         if($saltId!=''){
            $explodeSaltIds = explode(",",$saltId);
                $count = count($explodeSaltIds);
                if($count>0){
                    foreach ($explodeSaltIds as $value) {
                        $saltData = $saltObj->load($value);
                        $saltNamesArray[] = $saltData->getName();
                    }
                }
            $implodeSaltNames = implode(",", $saltNamesArray);

            if($implodeSaltNames!=''){
                return $implodeSaltNames;
            }else{
                return 'NA';
            }
        }else{
            return 'NA';
        }
                
    }
    
    public function getSaltValue(){
        $productSalt = "NA";
        $pId = $this->getCurrentProductId();
        if($pId!=''){
            $productObjData = $this->objectManager->create('Magento\Catalog\Model\Product')->load($pId);
            $zoyloProductSalt =  $productObjData->getSaltComposition();
            if($zoyloProductSalt!=''){
                $productSalt = $zoyloProductSalt;
            }
        }
        return $productSalt;
                
    }
    
    /**
     * Get category collection
     *
     * @param bool $isActive
     * @param bool|int $level
     * @param bool|string $sortBy
     * @param bool|int $pageSize
     * @return \Magento\Catalog\Model\ResourceModel\Category\Collection or array
     */
    public function getCategoryCollection($isActive = true, $level = false, $sortBy = false, $pageSize = false)
    {
        $collection = $this->_categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');        
        
        // select only active categories
        if ($isActive) {
            $collection->addIsActiveFilter();
        }
                
        // select categories of certain level
        if ($level) {
            $collection->addLevelFilter($level);
        }
        
        // sort categories by some value
        if ($sortBy) {
            $collection->addOrderField($sortBy);
        }
        
        // select certain number of categories
        if ($pageSize) {
            $collection->setPageSize($pageSize); 
        }    
        
        return $collection;
    }
    public function getCategoryName(){
        $product = $this->getCurrentProduct();
        $final_cat="";
        if($product!=''){
            $categoryIds = $product->getCategoryIds();
            $categories = $this->getCategoryCollection()->addAttributeToFilter('entity_id', $categoryIds);
            foreach($categories as $category){
                $final_cat.=(string)($category->getName());
                $final_cat.=",";
            }
        }
        if(!empty($final_cat)){
            return $finalCatNames = rtrim($final_cat,',');
        }else{
            return '';
        }
    }

    public function getSkuType($sku){
        $rxArray = $nonRxArray = array();
        $productSkuType="";
        if($sku !=''){
                $productCollectionFactory = $this->objectManager->get('\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
                    $collection = $productCollectionFactory->create();
                    $collection->addAttributeToSelect('*');
                    $collection->addAttributeToFilter('sku', array('eq' => $sku));
                    $count = count($collection->getData());
                    if($count>0){
                        $fcol = $collection->getData();
                        foreach($fcol as $product){
                            $productEntityId = $product['entity_id'];
                            if(!empty($productEntityId)){
                                $productInfo = $this->objectManager->create('Magento\Catalog\Model\Product')->load($productEntityId);
                                $productSkuType = $productInfo->getPrescriptionRequired();
                               
                        }
                    }
                }
            }
        return $productSkuType;
    }
    
    public function getCurrentProductCategoryId(){
        $catArray = array();
        $product = $this->getCurrentProduct();
        if($product!=''){
            $categoryIds = $product->getCategoryIds();
            $categories = $this->getCategoryCollection()->addAttributeToFilter('entity_id', $categoryIds);
            foreach($categories as $category){
                if($category->hasChildren()) {
                    $parentCategories = $category->getParentCategory();
                    $parentCatName = $parentCategories->getName();
                    if($parentCatName=="Default Category"){
                        $catArray['parentcat_id'] = $category->getName();
                    }
                    if($parentCatName!="Default Category"){
                        $catArray['subcat_id'] = $category->getName();
                    }
                }
            }
        }
        return $catArray;
    }
}