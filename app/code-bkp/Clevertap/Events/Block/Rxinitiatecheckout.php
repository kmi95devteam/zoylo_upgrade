<?php
namespace Clevertap\Events\Block;
class Rxinitiatecheckout extends \Magento\Framework\View\Element\Template {
    protected $clevertapHelper;
    protected $_registry;
    protected $objectManager;
    protected $customerSession;
    protected $customerModel;
    protected $checkoutSession;
    protected $salt;
    public function __construct
    (
        \Magento\Catalog\Block\Product\Context $context,
        \Clevertap\Events\Helper\Data $clevertapHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Customer $customerModel,
        \Magento\Checkout\Model\Session $checkoutSession,
        \I95Dev\Salts\Model\SaltFactory $salt,
        array $data = []
    ) 
    {
        $this->clevertapHelper = $clevertapHelper;
        $this->_registry = $registry;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->customerSession = $customerSession;
        $this->customerModel = $customerModel;
        $this->checkoutSession = $checkoutSession;
        $this->salt = $salt;
        parent::__construct($context, $data);

    }
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    
    public function getClevertapStatus(){
        return $this->clevertapHelper->getRxInitiateCheckoutStatus();
    }
    
    public function getClevertapAccountId(){
        return $this->clevertapHelper->getClevertapAccountId();
    }

    public function getCustomerMobileNo(){
        $customerNumber = "";
       $clevertapStatus = $this->getClevertapStatus();
       if($clevertapStatus!=''){
            $customerId = $this->customerSession->getCustomer()->getId();
            if($customerId!=''){
                $customer = $this->customerModel->load($customerId);
                $customerInfo = $customer->getData();
                $customerNumber = $customerInfo['customer_number'];  
            }else{
                $customerNumber = "";
            }
       }
       return $customerNumber;
    }

    public function getCheckoutSession() 
    {
        return $this->checkoutSession;
    }

    public function checkRxItemExistsOrNot()
    {
        $clevertapStatus = $this->getClevertapStatus();
        if($clevertapStatus!=''){
            $quoteId = $this->getCheckoutSession()->getQuoteId();
            $itemsIds = array();
            if(!empty($quoteId)){
                $cartItems = $this->getCheckoutSession()->getQuote()->getAllVisibleItems();
                foreach ($cartItems as $cartItem) {
                    $itemsIds[] = $cartItem->getProduct()->getSku();
                }
                if(!empty($itemsIds)){
                    if (in_array("Free", $itemsIds)) {
                        $rxItemExists = true;
                    }else{
                        $rxItemExists = false;
                    }
                }
            }
        }else{
            $rxItemExists = false;
        }
        return $rxItemExists;
    }

    public function getCurrentProductSaltValue($proId)
    {        
        $productSalt = "";
        $pId = $proId; //$this->getCurrentProductId();
        if($pId!=''){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productObjData = $this->objectManager->create('Magento\Catalog\Model\Product')->load($pId);
            $zoyloProductSalt =  $productObjData->getSaltName();
            if($zoyloProductSalt!=''){
                $productSalt = $zoyloProductSalt;
            }
        }
        return $productSalt;
    }

    public function getSaltValue($proId){
         $saltNamesArray =array();
         $saltObj = $this->salt->create();
         $saltId = $this->getCurrentProductSaltValue($proId);
         if($saltId!=''){
            $explodeSaltIds = explode(",",$saltId);
                $count = count($explodeSaltIds);
                if($count>0){
                    foreach ($explodeSaltIds as $value) {
                        $saltData = $saltObj->load($value);
                        $saltNamesArray[] = $saltData->getName();
                    }
                }
            $implodeSaltNames = implode(",", $saltNamesArray);

            if($implodeSaltNames!=''){
                return $implodeSaltNames;
            }else{
                return 'NA';
            }
        }else{
            return 'NA';
        }
                
    }
}