<?php
namespace Clevertap\Events\Block;
class Checkoutsuccesspayonline extends \Magento\Framework\View\Element\Template {
    protected $clevertapHelper;
    protected $_registry;
    protected $objectManager;
    protected $customerSession;
    protected $customerModel;
    protected $checkoutSession;
    protected $orderFactory;
    public function __construct
    (
        \Magento\Catalog\Block\Product\Context $context,
        \Clevertap\Events\Helper\Data $clevertapHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Customer $customerModel,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        array $data = []
    ) 
    {
        $this->clevertapHelper = $clevertapHelper;
        $this->_registry = $registry;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->customerSession = $customerSession;
        $this->customerModel = $customerModel;
        $this->checkoutSession = $checkoutSession;
        $this->orderFactory = $orderFactory;
        parent::__construct($context, $data);

    }
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    
    public function getClevertapStatus(){
        return $this->clevertapHelper->getOrderSuccessPayonlineStatus();
    }

    public function getCustomerMobileNo(){
        $customerNumber = "";
       $clevertapStatus = $this->getClevertapStatus();
       if($clevertapStatus!=''){
            $customerId = $this->customerSession->getCustomer()->getId();
            if($customerId!=''){
                $customer = $this->customerModel->load($customerId);
                $customerInfo = $customer->getData();
                $customerNumber = $customerInfo['customer_number'];  
            }else{
                $customerNumber = "";
            }
       }
       return $customerNumber;
    }
}