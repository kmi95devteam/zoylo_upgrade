<?php
namespace Clevertap\Events\Block;
class Home extends \Magento\Framework\View\Element\Template {
    protected $clevertapHelper;
    public function __construct(\Magento\Catalog\Block\Product\Context $context,
            \Clevertap\Events\Helper\Data $clevertapHelper,
            array $data = []) {
        $this->clevertapHelper = $clevertapHelper;
        parent::__construct($context, $data);

    }
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    
    public function getClevertapStatus(){
        return $this->clevertapHelper->getHomePageStatus();
    }
    
    public function getClevertapAccountId(){
        return $this->clevertapHelper->getClevertapAccountId();
    }
    

}