<?php
namespace Clevertap\Events\Block;
class Catalogsearch extends \Magento\Framework\View\Element\Template {
    protected $clevertapHelper;
    protected $request;
    public function __construct(\Magento\Catalog\Block\Product\Context $context,
            \Clevertap\Events\Helper\Data $clevertapHelper,
            \Magento\Framework\App\Request\Http $request,
            array $data = []) {
        $this->clevertapHelper = $clevertapHelper;
        $this->request = $request;
        parent::__construct($context, $data);

    }
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    
    public function getClevertapStatus(){
        return $this->clevertapHelper->getClevertapEnableStatus();
    }
    
    public function getClevertapAccountId(){
        return $this->clevertapHelper->getClevertapAccountId();
    }
    
    public function getSearchKeywords(){
        $clevertapStatus = $this->getClevertapStatus();
        if($clevertapStatus==1){
            return $searchKeyWord = $this->request->getParam('q');
        }else{
            return false;
        }
    }
    

}