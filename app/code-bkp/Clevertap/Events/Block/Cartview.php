<?php
namespace Clevertap\Events\Block;
class Cartview extends \Magento\Framework\View\Element\Template {
    protected $clevertapHelper;
    protected $_registry;
    protected $_categoryCollectionFactory;
    //protected $productFactory;
    protected $salt;
    protected $objectManager;
    public function __construct
    (
        \Magento\Catalog\Block\Product\Context $context,
        \Clevertap\Events\Helper\Data $clevertapHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        //\Magento\Catalog\Model\ProductFactory $productFactory,
        \I95Dev\Salts\Model\SaltFactory $salt,
        array $data = []
    ) 
    {
        $this->clevertapHelper = $clevertapHelper;
        $this->_registry = $registry;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        //$this->productFactory = $productFactory;
        $this->salt = $salt;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        parent::__construct($context, $data);

    }
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    
    public function getClevertapStatus(){
        return $this->clevertapHelper->getCartViewStatus();
    }

    public function getCurrentProductSaltValue($proId)
    {        
        $productSalt = "";
        $pId = $proId; //$this->getCurrentProductId();
        if($pId!=''){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productObjData = $this->objectManager->create('Magento\Catalog\Model\Product')->load($pId);
            $zoyloProductSalt =  $productObjData->getSaltName();
            if($zoyloProductSalt!=''){
                $productSalt = $zoyloProductSalt;
            }
        }
        return $productSalt;
    }

    public function getSaltValue($proId){
         $saltNamesArray =array();
         $saltObj = $this->salt->create();
         $saltId = $this->getCurrentProductSaltValue($proId);
         if($saltId!=''){
            $explodeSaltIds = explode(",",$saltId);
                $count = count($explodeSaltIds);
                if($count>0){
                    foreach ($explodeSaltIds as $value) {
                        $saltData = $saltObj->load($value);
                        $saltNamesArray[] = $saltData->getName();
                    }
                }
            $implodeSaltNames = implode(",", $saltNamesArray);

            if($implodeSaltNames!=''){
                return $implodeSaltNames;
            }else{
                return 'NA';
            }
        }else{
            return 'NA';
        }
                
    }

    public function getCustomerMobileNo(){
        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $this->objectManager->get('Magento\Customer\Helper\Session\CurrentCustomer');
        $customerId = $customerObj->getCustomerId();
        if($customerId!=''){
            $customer = $this->objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
            $customerInfo = $customer->getData();
            $customerNumber = $customerInfo['customer_number'];
            return $customerNumber;
        }else{
            return '';
        }
    }
}