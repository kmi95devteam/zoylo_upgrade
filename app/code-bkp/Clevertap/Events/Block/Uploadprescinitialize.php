<?php
namespace Clevertap\Events\Block;
class Uploadprescinitialize extends \Magento\Framework\View\Element\Template {
    protected $clevertapHelper;
    protected $_registry;
    protected $objectManager;
    public function __construct
    (
        \Magento\Catalog\Block\Product\Context $context,
        \Clevertap\Events\Helper\Data $clevertapHelper,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) 
    {
        $this->clevertapHelper = $clevertapHelper;
        $this->_registry = $registry;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        parent::__construct($context, $data);

    }
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    
    public function getClevertapStatus(){
        return $this->clevertapHelper->getUploadPrescInitializeStatus();
    }
}