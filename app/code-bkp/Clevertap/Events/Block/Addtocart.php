<?php
namespace Clevertap\Events\Block;
class Addtocart extends \Magento\Framework\View\Element\Template {
    protected $clevertapHelper;
    protected $_registry;
    protected $_categoryCollectionFactory;
    public function __construct
    (
        \Magento\Catalog\Block\Product\Context $context,
        \Clevertap\Events\Helper\Data $clevertapHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        array $data = []
    ) 
    {
        $this->clevertapHelper = $clevertapHelper;
        $this->_registry = $registry;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        parent::__construct($context, $data);

    }
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    
    public function getClevertapStatus(){
        return $this->clevertapHelper->getProductViewStatus();
    }
    
    public function getClevertapAccountId(){
        return $this->clevertapHelper->getClevertapAccountId();
    }
    
    public function getCurrentProduct()
    {        
        $clevertapStatus = $this->getClevertapStatus();
        if($clevertapStatus==1){
            return $this->_registry->registry('current_product');
        }else{
            return '';
        }
    }
    
    /**
     * Get category collection
     *
     * @param bool $isActive
     * @param bool|int $level
     * @param bool|string $sortBy
     * @param bool|int $pageSize
     * @return \Magento\Catalog\Model\ResourceModel\Category\Collection or array
     */
    public function getCategoryCollection($isActive = true, $level = false, $sortBy = false, $pageSize = false)
    {
        $collection = $this->_categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');        
        
        // select only active categories
        if ($isActive) {
            $collection->addIsActiveFilter();
        }
                
        // select categories of certain level
        if ($level) {
            $collection->addLevelFilter($level);
        }
        
        // sort categories by some value
        if ($sortBy) {
            $collection->addOrderField($sortBy);
        }
        
        // select certain number of categories
        if ($pageSize) {
            $collection->setPageSize($pageSize); 
        }    
        
        return $collection;
    }
    public function getCategoryName(){
        $product = $this->getCurrentProduct();
        $final_cat="";
        if($product!=''){
            $categoryIds = $product->getCategoryIds();
            $categories = $this->getCategoryCollection()->addAttributeToFilter('entity_id', $categoryIds);
            foreach($categories as $category){
                $final_cat.=(string)($category->getName());
                $final_cat.=",";
            }
        }
        if(!empty($final_cat)){
            return $finalCatNames = rtrim($final_cat,',');
        }else{
            return '';
        }
    }
}