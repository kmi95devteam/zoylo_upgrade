<?php
namespace I95Dev\Categorieslist\Plugin\Catalog\Block;

class Toolbar
{
    /**
    * Plugin
    *
    * @param \Magento\Catalog\Block\Product\ProductList\Toolbar $subject
    * @param \Closure $proceed
    * @param \Magento\Framework\Data\Collection $collection
    * @return \Magento\Catalog\Block\Product\ProductList\Toolbar
    */
    public function aroundSetCollection(
        \Magento\Catalog\Block\Product\ProductList\Toolbar $toolbar,
        \Closure $proceed,
        $collection
    ) {
        $this->_collection = $collection;
        $currentOrder = $toolbar->getCurrentOrder();
        $currentDirection = $toolbar->getCurrentDirection();
        $result = $proceed($collection);
        $this->_collection->getSelect()->order('is_salable DESC');
        if ($currentOrder) {
            switch ($currentOrder) {
			
            case 'name_az':
            $this->_collection->setOrder('name', 'ASC');
            break;

            case 'name_za':
            $this->_collection->setOrder('name', 'DESC');
            break;

			case 'price_desc':
            $toolbar->getCollection()->setOrder('price', 'DESC');
            break;

            case 'price_asc':
            $toolbar->getCollection()->setOrder('price', 'ASC');
            break;

	        case 'prescription_yes':
            $this->_collection->addAttributeToFilter('prescription_required', array('eq' => 'required'));
            $this->_collection->setOrder('prescription_required', 'DESC');
            break;

            case 'prescription_no':
            $this->_collection->addAttributeToFilter('prescription_required', array('eq' => 'not_required'));
            $this->_collection->setOrder('prescription_required', 'ASC');
            break;

            default:        
            $this->_collection->setOrder($currentOrder, $currentDirection);
            break;

            }
        }
        
        //var_dump((string) $this->_collection->getSelect()); 
		//die;
        return $result;
    }
}