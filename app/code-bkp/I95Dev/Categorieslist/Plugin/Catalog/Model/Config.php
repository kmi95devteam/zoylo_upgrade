<?php
namespace I95Dev\Categorieslist\Plugin\Catalog\Model;

class Config
{
    /**
     * Adding custom options and changing labels
     *
     * @param \Magento\Catalog\Model\Config $catalogConfig
     * @param [] $options
     * @return []
     */
    public function afterGetAttributeUsedForSortByArray(\Magento\Catalog\Model\Config $catalogConfig, $options)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $requestInterface = $objectManager->get('Magento\Framework\App\RequestInterface');
        $requestName = $requestInterface->getFullActionName();

        if($requestName != 'vesbrand_index_index' && $requestName != 'vesbrand_group_view' && $requestName != 'vesbrand_search_result'){
            //Remove default sorting options
            unset($options['position']);
            unset($options['name']);
            unset($options['price']);
            unset($options['manufacturer']);
            unset($options['Prescription Required']);
            unset($options['Salt Name ']);
            
            //Change label of default sorting options if needed
            //$options['position'] = __('Relevance');

            //New sorting options
            $options['name_az'] = __('Product Name A - Z');
            $options['name_za'] = __('Product Name Z - A');
            $options['price_desc'] = __('Price High - Low');
            $options['price_asc'] = __('Price Low - High');    
            $options['prescription_yes'] = __('Prescription Required - Yes');
            $options['prescription_no'] = __('Prescription Required - No');
        }
        return $options;
    }
}