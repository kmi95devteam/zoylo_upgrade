<?php

namespace I95Dev\PaymentRedirect\Controller\Index;

class Success extends \Magento\Framework\App\Action\Action {

    protected $helperdata;
    protected $helperapi;
    protected $objectManager;
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\ObjectManagerInterface $objectManager, \Magecomp\Smsfree\Helper\Data $helperdata, \Magecomp\Smsfree\Helper\Apicall $helperapi
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->objectManager = $objectManager;
        $this->helperdata = $helperdata;
        $this->helperapi = $helperapi;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        try {
            if ($this->helperdata->isEnabled() && $this->helperdata->isOrderPlaceForUserEnabled()) {
                $params     = array_keys($this->getRequest()->getParams());
                $data       = base64_decode($params[0]);
                $data       = json_decode($data, true);
                $order_id   = $data[0]['orderData']['id'];
                $order      = $this->objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($order_id);
                $status     = $order->getStatus();
                if($status == "order_placed"){
                    $customer   = $this->objectManager->create('Magento\Customer\Model\Customer')->load($order->getCustomerId());
                    $message    = $this->helperdata->getOrderPlaceTemplateForUser();
                    $sms        =  sprintf($message, $order->getIncrementId());
                    $mobile     = $customer->getCustomerNumber();
                    $this->helperapi->callApiUrl($mobile, $sms);
                }
                return $this->resultPageFactory->create();
            }
            return true;
        } catch (\Exception $e) {
            return true;
        }
        return $this->resultPageFactory->create();
    }

}
