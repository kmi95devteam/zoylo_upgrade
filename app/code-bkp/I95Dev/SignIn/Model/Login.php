<?php

namespace I95Dev\SignIn\Model;

use I95Dev\SignIn\Api\LoginInterface;

class Login implements LoginInterface {

    public $_session;
    public $_customer;

    public function __construct(
    \Magento\Customer\Model\CustomerFactory $customer, \Magento\Customer\Model\Session $session
    ) {

        $this->_customer = $customer;
        $this->_session = $session;
    }

    /**
     * Returns greeting message to user
     *
     * @api
     * @param string $name Users name.
     * @return string Greeting message with users name.
     */
    public function weblogin($mobile) {

        $customerCollection = $this->_customer->create()->getCollection()
                ->addAttributeToSelect("*")
                ->addAttributeToFilter("customer_number", array("eq" => $mobile));
        if (count($customerCollection) > 0) {

            foreach ($customerCollection as $customer) {

                $customerData = $customer;
            }
            if ($customerData->getId()) {
                $customeSession = $this->_session;
                $customeSession->setCustomerAsLoggedIn($customer);
                return true;
            } else {
                return "Customer not found";
            }
        } else {
            return "Customer not found";
        }
    }
}
