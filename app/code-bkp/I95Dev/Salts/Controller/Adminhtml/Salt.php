<?php
namespace I95Dev\Salts\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\RedirectFactory;
use I95Dev\Salts\Model\SaltFactory;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\Filter\Date;

abstract class Salt extends Action
{
    /**
     * author factory
     *
     * @var SaltFactory
     */
    protected $saltFactory;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * date filter
     *
     * @var \Magento\Framework\Stdlib\DateTime\Filter\Date
     */
    protected $dateFilter;

    /**
     * @param Registry $registry
     * @param SaltFactory $saltFactory
     * @param RedirectFactory $resultRedirectFactory
     * @param Context $context
     */
    public function __construct(
        Registry $registry,
        SaltFactory $saltFactory,
        RedirectFactory $resultRedirectFactory,
        Date $dateFilter,
        Context $context

    )
    {
        $this->coreRegistry = $registry;
        $this->saltFactory = $saltFactory;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->dateFilter = $dateFilter;
        parent::__construct($context);
    }

    /**
     * @return \I95Dev\Salts\Model\Author
     */
    protected function initAuthor()
    {
        $saltId  = (int) $this->getRequest()->getParam('salt_id');
        /** @var \I95Dev\Salts\Model\Salt $salt */
        $salt    = $this->saltFactory->create();
        if ($saltId) {
            $salt->load($saltId);
        }
        $this->coreRegistry->register('i95dev_salts', $salt);
        return $salt;
    }

    /**
     * filter dates
     *
     * @param array $data
     * @return array
     */
    public function filterData($data)
    {
        $inputFilter = new \Zend_Filter_Input(
            ['dob' => $this->dateFilter],
            [],
            $data
        );
        $data = $inputFilter->getUnescaped();
        if (isset($data['awards'])) {
            if (is_array($data['awards'])) {
                $data['awards'] = implode(',', $data['awards']);
            }
        }
        return $data;
    }

}
