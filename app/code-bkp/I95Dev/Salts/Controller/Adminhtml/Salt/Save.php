<?php
namespace I95Dev\Salts\Controller\Adminhtml\Salt;

use Magento\Framework\Registry;
use I95Dev\Salts\Controller\Adminhtml\Salt;
use Magento\Framework\Stdlib\DateTime\Filter\Date;
use I95Dev\Salts\Model\SaltFactory;
use Magento\Backend\Model\Session;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\Exception\LocalizedException;
use I95Dev\Salts\Model\Salt\Image as ImageModel;
use I95Dev\Salts\Model\Salt\File as FileModel;
use I95Dev\Salts\Model\Upload;
use Magento\Backend\Helper\Js as JsHelper;

class Save extends Salt
{
    /**
     * Salt factory
     * @var \I95Dev\Salts\Model\SaltFactory
     */
    protected $saltFactory;

    /**
     * image model
     *
     * @var \I95Dev\Salts\Model\Salt\Image
     */
    protected $imageModel;

    /**
     * file model
     *
     * @var \I95Dev\Salts\Model\Salt\File
     */
    protected $fileModel;

    /**
     * upload model
     *
     * @var \I95Dev\Salts\Model\Upload
     */
    protected $uploadModel;

    /**
     * @var \Magento\Backend\Helper\Js
     */
    protected $jsHelper;


    /**
     * @param JsHelper $jsHelper
     * @param Session $backendSession
     * @param Date $dateFilter
     * @param ImageModel $imageModel
     * @param FileModel $fileModel
     * @param Upload $uploadModel
     * @param Registry $registry
     * @param SaltFactory $saltFactory
     * @param RedirectFactory $resultRedirectFactory
     * @param Context $context
     */
    public function __construct(
        JsHelper $jsHelper,

        ImageModel $imageModel,
        FileModel $fileModel,
        Upload $uploadModel,
        Registry $registry,
        SaltFactory $saltFactory,
        RedirectFactory $resultRedirectFactory,
        Date $dateFilter,
        Context $context
    )
    {
        $this->jsHelper = $jsHelper;
        $this->imageModel = $imageModel;
        $this->fileModel = $fileModel;
        $this->saltFactory = $saltFactory;
        parent::__construct($registry, $saltFactory, $resultRedirectFactory, $dateFilter, $context);
    }

    /**
     * run the action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getPost('salt');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {

            $data = $this->filterData($data);
            $salt = $this->initAuthor();
            $salt->setKidney($data['kidney']);
            $salt->setPregnancy($data['pregnancy']);
            $salt->setLiver($data['liver']);
            $salt->setLactation($data['lactation']);
            $salt->setHeart($data['heart']);
            $salt->setChildrenSalt($data['children_salt']);
            $salt->setSeniors($data['seniors']);
            $salt->setVision($data['vision']);
            $salt->setSmoking($data['smoking']);
            $salt->setAlcohol($data['alcohol']);
            $salt->setCaffeine($data['caffeine']);
            $salt->setOverdose($data['overdose']);
            $salt->setDriving($data['driving']);
            $salt->setExercise($data['exercise']);
            $salt->setOperatingHeavyAchinery($data['operating_heavy_achinery']);
            $salt->setAntibiotics($data['antibiotics']);
            $salt->setChemotherapyDrugs($data['chemotherapy_drugs']);
            $salt->setAnticonvulsants($data['anticonvulsants']);
            $salt->setAnticoagulants($data['anticoagulants']);
            $salt->setSulfaDrugs($data['sulfa_drugs']);
            $salt->setData($data);
            if(isset($data['symptoms'])){
                $symptoms = implode(',', $data['symptoms']);
                $salt->setAilments($symptoms);
            }
            $salt->setSideEffects($data['side_effects']);
            $saltFaq = '';
            if(isset($data['salt_id'])){
                $id = $data['salt_id'];
                
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $saltObj = $objectManager->create('I95Dev\Salts\Model\Salt')->load($id);
                $saltFaq = $saltObj->getFaqs();
                
            }
            if(isset($data['faq'])){
            $faqs = $data['faq'];
            $insertFaqs = '';
            $i=1;
            foreach($faqs as $key=>$value){
                if(isset($faqs['question'.$i])){
                   $insertFaqs .= "<p><strong>".$faqs['question'.$i] .'</strong><br>' . $faqs['input'.$i]."<br></p>";
                }
                $i++;
            }
            $faqs = $saltFaq . $insertFaqs;
            $salt->setFaqs($faqs);
            
            }
            
           
            try {
                 $salt->save();
                $this->messageManager->addSuccess(__('The salt has been saved.'));
                $this->_getSession()->setSampleNewsAuthorData(false);
                if ($this->getRequest()->getParam('back')) {
                    $resultRedirect->setPath(
                        'i95dev_salts/*/edit',
                        [
                            'salt_id' => $salt->getId(),
                            '_current' => true
                        ]
                    );
                    return $resultRedirect;
                }
                $resultRedirect->setPath('i95dev_salts/*/');
                return $resultRedirect;
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the salt.'));
            }

            $this->_getSession()->setSampleNewsAuthorData($data);
            $resultRedirect->setPath(
                'i95dev_salts/*/edit',
                [
                    'salt_id' => $salt->getId(),
                    '_current' => true
                ]
            );
            return $resultRedirect;
        }
        $resultRedirect->setPath('i95dev_salts/*/');
        return $resultRedirect;
    }
}
