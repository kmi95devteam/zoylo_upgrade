<?php
namespace I95Dev\Salts\Controller\Adminhtml\Salt;

use I95Dev\Salts\Controller\Adminhtml\Salt as SaltController;
use Magento\Framework\Registry;
use I95Dev\Salts\Model\SaltFactory;
use Magento\Backend\Model\Session as BackendSession;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Stdlib\DateTime\Filter\Date;

class Edit extends SaltController
{
    /**
     * backend session
     *
     * @var BackendSession
     */
    protected $backendSession;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * constructor
     *
     * @param Registry $registry
     * @param SaltFactory $saltFactory
     * @param BackendSession $backendSession
     * @param PageFactory $resultPageFactory
     * @param Context $context
     * @param RedirectFactory $resultRedirectFactory
     */
    public function __construct(
        Registry $registry,
        PageFactory $resultPageFactory,
        SaltFactory $saltFactory,
        BackendSession $backendSession,
        RedirectFactory $resultRedirectFactory,
        Date $dateFilter,
        Context $context

    )
    {
        $this->backendSession = $backendSession;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($registry, $saltFactory, $resultRedirectFactory, $dateFilter, $context);
    }

    /**
     * is action allowed
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('I95Dev_Salts::salt');
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('salt_id');
        /** @var \I95Dev\Salts\Model\Salt $salt */
        $salt = $this->initAuthor();
        /** @var \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('I95Dev_Salts::salt');
        $resultPage->getConfig()->getTitle()->set((__('Salts')));
        if ($id) {
            $salt->load($id);
            if (!$salt->getId()) {
                $this->messageManager->addError(__('This salt no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath(
                    'i95dev_salts/*/edit',
                    [
                        'salt_id' => $salt->getId(),
                        '_current' => true
                    ]
                );
                return $resultRedirect;
            }
        }
        $title = $salt->getId() ? $salt->getName() : __('New Salt');
        $resultPage->getConfig()->getTitle()->append($title);
        $data = $this->backendSession->getData('i95dev_salts_slat_data', true);
        if (!empty($data)) {
            $salt->setData($data);
        }
        return $resultPage;
    }
}
