<?php
namespace I95Dev\Salts\Controller\Adminhtml\Salt;

use Magento\Framework\Exception\LocalizedException;
use I95Dev\Salts\Controller\Adminhtml\Salt;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\RedirectFactory;
use I95Dev\Salts\Model\SaltFactory;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\Filter\Date;
use Magento\Ui\Component\MassAction\Filter;
use I95Dev\Salts\Model\ResourceModel\Salt\CollectionFactory;
use I95Dev\Salts\Model\Salt as SaltModel;

abstract class MassAction extends Salt
{
    protected $filter;
    protected $collectionFactory;
    /**
     * @var string
     */
    protected $successMessage = 'Mass Action successful on %1 records';
    /**
     * @var string
     */
    protected $errorMessage = 'Mass Action failed';

    public function __construct(
        Filter $filter,
        CollectionFactory $collectionFactory,
        Registry $registry,
        SaltFactory $saltFactory,
        RedirectFactory $resultRedirectFactory,
        Date $dateFilter,
        Context $context
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($registry, $saltFactory, $resultRedirectFactory, $dateFilter, $context);
    }

    /**
     * @param SaltModel $salt
     * @return mixed
     */
    protected abstract function doTheAction(SaltModel $salt);

    /**
     * execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $collectionSize = $collection->getSize();
            foreach ($collection as $salt) {
                $this->doTheAction($salt);
            }
            $this->messageManager->addSuccess(__($this->successMessage, $collectionSize));
        } catch (LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __($this->errorMessage));
        }
        $redirectResult = $this->resultRedirectFactory->create();
        $redirectResult->setPath('i95dev_salts/*/index');
        return $redirectResult;
    }
}
