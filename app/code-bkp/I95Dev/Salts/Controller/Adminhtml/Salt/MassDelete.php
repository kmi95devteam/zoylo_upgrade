<?php
namespace I95Dev\Salts\Controller\Adminhtml\Salt;

use I95Dev\Salts\Model\Salt;
class MassDelete extends MassAction
{
    /**
     * @var string
     */
    protected $successMessage = 'A total of %1 record(s) have been deleted';
    /**
     * @var string
     */
    protected $errorMessage = 'An error occurred while deleting record(s).';

    /**
     * @param $salt
     * @return $this
     */
    protected function doTheAction(Salt $salt)
    {
        $salt->delete();
        return $this;
    }
}
