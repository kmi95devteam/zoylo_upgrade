<?php
namespace I95Dev\Salts\Controller\Adminhtml\Salt;

use Magento\Backend\App\Action\Context;
use I95Dev\Salts\Model\AuthorFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use I95Dev\Salts\Controller\Adminhtml\Salt as SaltController;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\Registry;
use I95Dev\Salts\Model\SaltFactory;
use Magento\Framework\Stdlib\DateTime\Filter\Date;


class InlineEdit extends SaltController
{
    /**
     * @var JsonFactory
     */
    protected $jsonFactory;

    /**
     * @param JsonFactory $jsonFactory
     * @param Registry $registry
     * @param SaltFactory $saltFactory
     * @param RedirectFactory $resultRedirectFactory
     * @param Date $dateFilter
     * @param Context $context
     */
    public function __construct(
        JsonFactory $jsonFactory,
        Registry $registry,
        SaltFactory $saltFactory,
        RedirectFactory $resultRedirectFactory,
        Date $dateFilter,
        Context $context

    ) {
        $this->jsonFactory = $jsonFactory;
        parent::__construct($registry, $saltFactory, $resultRedirectFactory, $dateFilter, $context);

    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }

        foreach (array_keys($postItems) as $saltId) {
            /** @var \I95Dev\Salts\Model\Salt $salt */
            $salt = $this->saltFactory->create()->load($saltId);
            try {
                $saltData = $this->filterData($postItems[$saltId]);
                $salt->addData($saltData);

                $salt->save();
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $messages[] = $this->getErrorWithAuthorId($salt, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithAuthorId($salt, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithAuthorId(
                    $salt,
                    __('Something went wrong while saving the page.')
                );
                $error = true;
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Add salt id to error message
     *
     * @param Salt $salt
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithAuthorId(Salt $salt, $errorText)
    {
        return '[Salt ID: ' . $salt->getId() . '] ' . $errorText;
    }
}
