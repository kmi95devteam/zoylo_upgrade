<?php
namespace I95Dev\Salts\Controller\Salt;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use I95Dev\Salts\Model\SaltFactory;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Registry;
use I95Dev\Salts\Model\Salt\Url as UrlModel;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class View extends Action
{
    //const BREADCRUMBS_CONFIG_PATH = 'i95dev_salts/salt/breadcrumbs';
    /**
     * @var \I95Dev\Salts\Model\SaltFactory
     */
    protected $saltFactory;

    /**
     * @var \Magento\Framework\Controller\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \I95Dev\Salts\Model\Salt\Url
     */
    protected $urlModel;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param Context $context
     * @param SaltFactory $saltFactory
     * @param ForwardFactory $resultForwardFactory
     * @param PageFactory $resultPageFactory
     * @param Registry $coreRegistry
     * @param UrlModel $urlModel
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Context $context,
        SaltFactory $saltFactory,
        ForwardFactory $resultForwardFactory,
        PageFactory $resultPageFactory,
        Registry $coreRegistry,
        UrlModel $urlModel,
        ScopeConfigInterface $scopeConfig
    )
    {
        
        $this->resultForwardFactory = $resultForwardFactory;
        $this->saltFactory = $saltFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $coreRegistry;
        $this->urlModel = $urlModel;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Forward|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        
        $saltId = (int) $this->getRequest()->getParam('id');
        
        $salt = $this->saltFactory->create();
        $salt->load($saltId);
        
        if (!$salt->isActive()) {
            $resultForward = $this->resultForwardFactory->create();
            $resultForward->forward('noroute');
            return $resultForward;
        }

        $this->coreRegistry->register('current_salt', $salt);

        $resultPage = $this->resultPageFactory->create();
        $title = ($salt->getMetaTitle()) ?: $salt->getName();
        
        

        return $resultPage;
    }
}
