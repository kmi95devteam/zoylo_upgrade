<?php

namespace I95Dev\Salts\Controller;

use Magento\Framework\App\RouterInterface;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\DataObject;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\App\State;
use I95Dev\Salts\Model\SaltFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Url;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Router implements RouterInterface {

    /**
     * @var \Magento\Framework\App\ActionFactory
     */
    protected $actionFactory;

    /**
     * Event manager
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager;

    /**
     * Store manager
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Salt factory
     * @var \I95Dev\Salts\Model\SaltFactory
     */
    protected $saltFactory;

    /**
     * Config primary
     * @var \Magento\Framework\App\State
     */
    protected $appState;

    /**
     * Url
     * @var \Magento\Framework\UrlInterface
     */
    protected $url;

    /**
     * Response
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $response;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var bool
     */
    protected $dispatched;

    /**
     * @param ActionFactory $actionFactory
     * @param ManagerInterface $eventManager
     * @param UrlInterface $url
     * @param State $appState
     * @param SaltFactory $saltFactory
     * @param StoreManagerInterface $storeManager
     * @param ResponseInterface $response
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
    ActionFactory $actionFactory, ManagerInterface $eventManager, UrlInterface $url, State $appState, SaltFactory $saltFactory, StoreManagerInterface $storeManager, ResponseInterface $response, ScopeConfigInterface $scopeConfig
    ) {
        $this->actionFactory = $actionFactory;
        $this->eventManager = $eventManager;
        $this->url = $url;
        $this->appState = $appState;
        $this->saltFactory = $saltFactory;
        $this->storeManager = $storeManager;
        $this->response = $response;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Validate and Match News Author and modify request
     * @param \Magento\Framework\App\RequestInterface $request
     * @return bool
     * //TODO: maybe remove this and use the url rewrite table.
     */
    public function match(RequestInterface $request) {
     
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        $customerId = $customerSession->getCustomerId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        //get base url 
        $baseurl = $objectManager->get('I95Dev\UploadPrescription\Helper\Data');
        $url = $baseurl->getBaseUrl();
        $urlstring = str_replace('medicines/', '', $url);
        $urlKey = trim($request->getPathInfo(), '/');
        
            if (strpos($urlKey, "checkout") !== false){
                    $exists = "yes";
            }else {
                $exists =  "no";
            }
            if (strpos($urlKey, "buynow") !== false) {
                $buyExists = "yes";
            } else {
                $buyExists = "no";
            }
            
            if (strpos($urlKey, "adminorder") !== false) {
                $adminExists = "yes";
            } else {
                $adminExists = "no";
            }
            
            if (strpos($urlKey, "quickorder") !== false) {
                $quickorder = "yes";
            } else {
                $quickorder = "no";
            }
            
            
            
            
            
            if(isset($_SERVER['HTTP_REFERER'])){
                $referUrl = $_SERVER['HTTP_REFERER'];
            }else {
                $referUrl = '';
            }
            $token = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('singin/single_sign/sign_token');
            $amp = $request->getParam('amp');
            if ($customerId == '' && $urlKey != 'searchautocomplete/ajax/suggest' && $amp != 1 &&  $quickorder == 'no' && $adminExists ==  'no' && $exists == 'no' && $urlKey != 'customer/section/load' && $buyExists == 'no' && $urlKey != 'adminorder'
                    && $urlKey != 'csquare/index/payment'&& $urlKey != 'signin/index/index' && $urlKey != 'ajaxshoppingcartpage/cart/couponpost') {
                $cSession = 0;
                ?>

                    <form class="form form-login" action="<?php echo $url; ?>signin/index/index/" method="post" id="zoylo-login-form" novalidate="novalidate">
                        <input name="form_key" value="LKhgY51FyHL9CnDr" type="hidden">
                        <input name="zoylotoken" id="zoylotoken" value="" type="hidden">
                        <input name="login[zoylotoken]" type="hidden" id="zoylotokenlogin" value="0">
                        <input name="refer" type="hidden" id="refer" value="<?php echo $referUrl;?>">
                        <input name="zoyloflag" type="hidden" id="zoyloflag" value="<?php echo $cSession; ?>">
                    </form> 

                    <script type="text/javascript">
                        var user_token = localStorage.getItem("<?php echo $token?>");
                        var isLogging = 0;
                        isLogging = document.getElementById("zoyloflag").value;
                        document.getElementById("zoylotoken").value = user_token;
                        if ((user_token != null) && (isLogging == 0)) {
                            var data = document.getElementById("zoylo-login-form").submit();
                        }
                    </script> 
                    <?php
                }
         


        if (!$this->dispatched) {

            $urlKey = trim($request->getPathInfo(), '/');
            $origUrlKey = $urlKey;

            /** @var Object $condition */
            $condition = new DataObject(['url_key' => $urlKey, 'continue' => true]);
            $this->eventManager->dispatch(
                    'i95dev_salts_controller_router_match_before', ['router' => $this, 'condition' => $condition]
            );
            $urlKey = $condition->getUrlKey();

            if ($condition->getRedirectUrl()) {
                $this->response->setRedirect($condition->getRedirectUrl());
                $request->setDispatched(true);
                return $this->actionFactory->create(
                                'Magento\Framework\App\Action\Redirect', ['request' => $request]
                );
            }
            if (!$condition->getContinue()) {
                return null;
            }
            $entities = [
                'salt' => [
                    'prefix' => '',
                    'suffix' => '',
                    'list_key' => '',
                    'list_action' => 'index',
                    'factory' => $this->saltFactory,
                    'controller' => 'salt',
                    'action' => 'view',
                    'param' => 'id',
                ]
            ];

            foreach ($entities as $entity => $settings) {

                if ($settings['prefix']) {

                    $parts = explode('/', $urlKey);

                    if ($parts[0] != $settings['prefix'] || count($parts) != 2) {
                        continue;
                    }
                    $urlKey = $parts[1];
                }
                if ($settings['suffix']) {
                    $suffix = substr($urlKey, -strlen($settings['suffix']) - 1);
                    if ($suffix != '.' . $settings['suffix']) {
                        continue;
                    }
                    $urlKey = substr($urlKey, 0, -strlen($settings['suffix']) - 1);
                }

                $instance = $settings['factory']->create();
                $id = $instance->checkUrlKey($urlKey, $this->storeManager->getStore()->getId());

                if (!$id) {
                    return null;
                }


                $request->setModuleName('salts')
                        ->setControllerName('salt')
                        ->setActionName('view')
                        ->setParam('id', $id);
                $request->setAlias(Url::REWRITE_REQUEST_PATH_ALIAS, $origUrlKey);
                $request->setDispatched(true);
                $this->dispatched = true;
                return $this->actionFactory->create(
                                'Magento\Framework\App\Action\Forward', ['request' => $request]
                );
            }
        }
        return null;
    }

}
