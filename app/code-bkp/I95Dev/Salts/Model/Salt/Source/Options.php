<?php
namespace I95Dev\Salts\Model\Salt\Source;

use Magento\Framework\Option\ArrayInterface;
use I95Dev\Salts\Model\Salt;

class Options implements ArrayInterface
{
    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => '',
                'label' => __('')
            ],
            [
                'value' => 'Safe',
                'label' => __('Safe')
            ],[
                'value' => 'UnSafe',
                'label' => __('UnSafe')
            ],
            [
                'value' => 'Unknown',
                'label' => __('Unknown')
            ],
            [
                'value' => 'Supervision Required',
                'label' => __('Supervision Required')
            ],
            [
                'value' => 'NA',
                'label' => __('NA')
            ],
        ];
    }

    /**
     * get options as key value pair
     *
     * @return array
     */
    public function getOptions()
    {
        $_tmpOptions = $this->toOptionArray();
        $_options = [];
        foreach ($_tmpOptions as $option) {
            $_options[$option['value']] = $option['label'];
        }
        return $_options;
    }
    
    public function getAllOptions() {
        $this->_options = [];
      //  $datas = $this->getFormatCollection();
        $this->_options[] = [
                'label' => 'Select',
                'value' => 'Select',
            ];
        $this->_options[] = [
                'label' => 'Tablet',
                'value' => 'Tablet',
            ];
        $this->_options[] = [
                'label' => 'Drops',
                'value' => 'Drops',
            ];
        $this->_options[] = [
                'label' => 'Gel',
                'value' => 'Gel',
            ];
        $this->_options[] = [
                'label' => 'Injection',
                'value' => 'Injection',
            ];
        $this->_options[] = [
                'label' => 'Syrup',
                'value' => 'Syrup',
            ];
         $this->_options[] = [
                'label' => 'Dry Syrup',
                'value' => 'Dry Syrup',
            ];
          $this->_options[] = [
                'label' => 'Soap',
                'value' => 'Soap',
            ]; $this->_options[] = [
                'label' => 'Powder',
                'value' => 'Powder',
            ];
             $this->_options[] = [
                'label' => 'Suppositories',
                'value' => 'Suppositories',
            ];
              $this->_options[] = [
                'label' => 'Inhaler',
                'value' => 'Inhaler',
            ];
               $this->_options[] = [
                'label' => 'Liquid',
                'value' => 'Liquid',
            ];
                $this->_options[] = [
                'label' => 'Spray',
                'value' => 'Spray',
            ];
                           
        
//        foreach ($datas as $data) {
//            
//            $this->_options[] = [
//                'label' => trim($data['label']),
//                'value' => trim($data['label']),
//            ];
//        }

        return $this->_options;
    }

   public function getFormatCollection() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $eavConfig = $objectManager->get('\Magento\Eav\Model\Config');
        $attribute = $eavConfig->getAttribute('catalog_product', 'product_format');
        $alloptions = $attribute->getSource()->getAllOptions();
        return $alloptions;
    }
}
