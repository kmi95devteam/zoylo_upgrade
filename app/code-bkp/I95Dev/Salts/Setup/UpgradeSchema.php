<?php
namespace I95Dev\Salts\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();
       
    
          $installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'rx',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'Rx'
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'not_for_sale',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'Not for Sale'
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'kidney',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'kidney'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'pregnancy',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'pregnancy'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'high_blood_pressure',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'high_blood_pressure'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'low_blood_pressure',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'low_blood_pressure'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'diabetes',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'diabetes'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'liver',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'liver'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'lactation',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'lactation'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'oral_contraceptives',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'oral_contraceptives'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'children_salt',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'children_salt'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'seniors',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'seniors'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'smoking',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'smoking'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'alcohol',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'alcohol'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'tolerance',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'tolerance'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'vision',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'vision'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'overdose',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'overdose'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'driving',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'driving'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'exercise',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'exercise'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'oral_contraceptives',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'oral_contraceptives'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'operating_heavy_achinery',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'operating_heavy_achinery'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'sulfa_drugs',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'sulfa_drugs'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'anticonvulsants',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'anticonvulsants'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'chemotherapy_drugs',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'chemotherapy_drugs'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'anti-inflammatory',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'anti-inflammatory'
                ]
            );$installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'antibiotics',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'antibiotics'
                ]
            ); 
             if (version_compare($context->getVersion(), '2.0.8', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('i95dev_salts'),
                'drug_type',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => 10,
                    'nullable' => false,
                    
                    'comment' => 'Drug Type'
                ]
            );
             }
        
        $installer->endSetup();
    }
}