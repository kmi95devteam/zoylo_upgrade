<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace I95Dev\Salts\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (!$installer->tableExists('i95dev_salts')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('i95dev_salts'));
            $table->addColumn(
                    'salt_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Salt ID'
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable'  => false,],
                    'Salt Name'
                )
                ->addColumn(
                    'url_key',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable'  => false,],
                    'Salt Url Key'
                )
                ->addColumn(
                    'short_name',
                    Table::TYPE_TEXT,
                    '2M',
                    [],
                    'Short Name'
                )
                ->addColumn(
                    'ailments',
                    Table::TYPE_TEXT,
					'2M',
                    [],
                    'Ailments'
                )
                ->addColumn(
                    'uses',
                    Table::TYPE_TEXT,
                    '2M',
                    [],
                    'Uses'
                )
		->addColumn(
                    'side_effects',
                    Table::TYPE_TEXT,
                    '2M',
                    [],
                    'side_effects'
                )		
                ->addColumn(
                    'expert_advice',
                    Table::TYPE_TEXT,
                    '2M',
                    [],
                    'Expert Advice'
                )
             ->addColumn(
                    'meta_title',
                    Table::TYPE_TEXT,
                    '2M',
                    [],
                    'Salt Meta Title'
                )
				 ->addColumn(
                    'meta_keywords',
                    Table::TYPE_TEXT,
                    '2M',
                    [],
                    'Salt Meta Keywords'
                )
                ->addColumn(
                    'meta_description',
                    Table::TYPE_TEXT,
                    '2M',
                    [],
                    'Salt Meta Description'
                )
               ->addColumn(
                    'faqs',
                    Table::TYPE_TEXT,
                    '2M',
                    [],
                    'FAQs'
                )
                ->addColumn(
                    'is_active',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'nullable'  => false,
                        'default'   => '1',
                    ],
                    'Is Salt Active'
                )
               ->setComment('Salts');
            $installer->getConnection()->createTable($table);

            $installer->getConnection()->addIndex(
                $installer->getTable('i95dev_salts'),
                $setup->getIdxName(
                    $installer->getTable('i95dev_salts'),
                    ['name','photo'],
                    AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                [
                    'name',
                    'short_name',
                    'url_key',
					'uses',
					'ailments',
                    'meta_title',
                    'meta_keywords',
                    'meta_description',
					'faqs'
                ],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }

       

       
    }
}
