<?php
namespace I95Dev\Salts\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

class Salt extends Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_order';
        $this->_blockGroup = 'I95Dev_Salts';
        $this->_headerText = __('Salts');
        $this->_addButtonLabel = __('Create New Salt');
        parent::_construct();
    }
}
