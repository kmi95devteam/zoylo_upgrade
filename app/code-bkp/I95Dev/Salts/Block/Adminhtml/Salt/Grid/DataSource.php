<?php
namespace I95Dev\Salts\Block\Adminhtml\Salt\Grid;

use Magento\Framework\Data\CollectionDataSourceInterface;
use I95Dev\Salts\Model\ResourceModel\Salt\Collection;
use Magento\Backend\Block\Widget\Grid\Column;

class DataSource implements CollectionDataSourceInterface
{
    /**
     * filter by store
     *
     * @param Collection $collection
     * @param Column $column
     * @return $this
     */
    public function filterStoreCondition(Collection $collection, Column $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        $collection->addStoreFilter($value);
        return $this;
    }
}
