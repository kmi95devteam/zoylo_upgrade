<?php
namespace I95Dev\Salts\Block\Adminhtml\Salt\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic as GenericForm;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config as WysiwygConfig;

class Faq extends GenericForm implements TabInterface
{
	/**
     * @var WysiwygConfig
     */
//    protected $wysiwygConfig;
    
      //const SALT_TEMPLATE = 'salt/faq.phtml';
   //   protected $_template = 'I95Dev_Salts::salt/faq.phtml';
	
    /**
     * constructor
     *
	 * @param WysiwygConfig $wysiwygConfig
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param array $data
     */
    
    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array                                 $data
     */
//    public function __construct(
//        \Magento\Backend\Block\Widget\Context $context,
//        array $data = []
//    ) {
//        parent::__construct($context, $data);
//    }

    public function __construct(
		WysiwygConfig $wysiwygConfig,
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        array $data = []
    )
    {
		$this->wysiwygConfig    = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare the form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \I95Dev\Salts\Model\Salt $author */
        $author = $this->_coreRegistry->registry('i95dev_salts');
        $form   = $this->_formFactory->create();
        $form->setHtmlIdPrefix('salt_');
        $form->setFieldNameSuffix('salt');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend'    =>__('Faqs'),
                'class'     => 'fieldset-wide'
            ]
        );
        $fieldset->addField(
            'faqs',
            'editor',
            [
                'name'      => 'faqs',
                'label'     => __('FAQs'),
                'title'     => __('FAQs'),
                'required'  => true,
                'style'     => 'height:36em',
				'config'    => $this->wysiwygConfig->getConfig()
            ]
        );
		
        $form->addValues($author->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Faqs');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return !$this->_storeManager->isSingleStoreMode();
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
    
  

    /**
     * Set JS template to itself.
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate(static::SALT_TEMPLATE);
        }

        return $this;
    }
    
    protected function getFaqData()
    {
        $salt = 3;
        echo $salt;die;
        return $this;
    }
}
