<?php
namespace I95Dev\Salts\Block\Adminhtml\Salt\Edit;

use Magento\Backend\Block\Widget\Tabs as WidgetTabs;

/**
 * @method Tabs setTitle(\string $title)
 */
class Tabs extends WidgetTabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('salt_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Salts Information'));
    }
}
