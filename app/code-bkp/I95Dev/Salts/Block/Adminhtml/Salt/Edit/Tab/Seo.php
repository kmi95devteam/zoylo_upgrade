<?php
namespace I95Dev\Salts\Block\Adminhtml\Salt\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic as GenericForm;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config as WysiwygConfig;

class Seo extends GenericForm implements TabInterface
{
    
	 /**
     * @var WysiwygConfig
     */
    protected $wysiwygConfig;

    /**
     * @param WysiwygConfig $wysiwygConfig
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        WysiwygConfig $wysiwygConfig,
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        array $data = []
    )
    {
        $this->wysiwygConfig    = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }
	
	/**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \I95Dev\Salts\Model\Salt $salt */
        $salt = $this->_coreRegistry->registry('i95dev_salts');
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('salt_');
        $form->setFieldNameSuffix('salt');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend'    =>__('SEO Information'),
                'class'     => 'fieldset-wide'
            ]
        );
        $fieldset->addField(
            'meta_title',
            'text',
            [
                'name'      => 'meta_title',
                'label'     => __('Meta Title'),
                'title'     => __('Meta Title'),
                'required'  => false,
            ]
        );
        $fieldset->addField(
            'meta_keywords',
            'textarea',
            [
                'name'      => 'meta_keywords',
                'label'     => __('Meta Keywords'),
                'title'     => __('Meta Keywords'),
                'required'  => false,
                'rows'      => 5
            ]
        );
        $fieldset->addField(
            'meta_description',
            'editor',
            [
                'name'      => 'meta_description',
                'label'     => __('Meta Description'),
                'title'     => __('Meta Description'),
                'style'     => 'height:36em',
                'required'  => true,
                'config'    => $this->wysiwygConfig->getConfig()
            ]
        );
        $form->setValues($salt->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Seo');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
}
