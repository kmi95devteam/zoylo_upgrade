<?php
namespace I95Dev\Salts\Block\Adminhtml\Salt\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic as GenericForm;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config as WysiwygConfig;

class Interactions extends GenericForm implements TabInterface
{
    
	 /**
     * @var WysiwygConfig
     */
    protected $wysiwygConfig;

    /**
     * @param WysiwygConfig $wysiwygConfig
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        WysiwygConfig $wysiwygConfig,
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
            \I95Dev\Salts\Model\Salt\Source\Options $options,
        array $data = []
            
    )
    {
         $this->options = $options;
        $this->wysiwygConfig    = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }
	
	/**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \I95Dev\Salts\Model\Salt $salt */
        $salt = $this->_coreRegistry->registry('i95dev_salts');
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('salt_');
        $form->setFieldNameSuffix('salt');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend'    =>__('Interactions'),
                'class'     => 'fieldset-wide'
            ]
        );
        $fieldset->addField(
            'kidney',
            'select',
            [
                'label'     => __('Kidney'),
                'title'     => __('Kidney'),
                'name'      => 'kidney',
                
                'options'   => $this->options->getOptions(),
            ]
        );
        $fieldset->addField(
            'heart',
            'select',
            [
                'label'     => __('Heart'),
                'title'     => __('Heart'),
                'name'      => 'heart',
                
                'options'   => $this->options->getOptions(),
            ]
        );
        $fieldset->addField(
            'pregnancy',
            'select',
            [
                'label'     => __('Pregnancy'),
                'title'     => __('Pregnancy'),
                'name'      => 'pregnancy',
                
                'options'   => $this->options->getOptions(),
            ]
        );
        $fieldset->addField(
            'liver',
            'select',
            [
                'label'     => __('Liver'),
                'title'     => __('Liver'),
                'name'      => 'liver',
                
                'options'   => $this->options->getOptions(),
            ]
        );
        $fieldset->addField(
            'lactation',
            'select',
            [
                'label'     => __('Lactation'),
                'title'     => __('Lactation'),
                'name'      => 'lactation',
                
                'options'   => $this->options->getOptions(),
            ]
        );
        
        $fieldset->addField(
            'children_salt',
            'select',
            [
                'label'     => __('Children'),
                'title'     => __('Children'),
                'name'      => 'children_salt',
                
                'options'   => $this->options->getOptions(),
            ]
        );
        $fieldset->addField(
            'seniors',
            'select',
            [
                'label'     => __('Seniors'),
                'title'     => __('Seniors'),
                'name'      => 'seniors',
                'options'   => $this->options->getOptions(),
            ]
        );
        $form->setValues($salt->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Interactions');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
}
