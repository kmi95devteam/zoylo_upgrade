<?php
namespace I95Dev\QuickOrder\Controller\Index;
use Magento\Framework\App\Action\Action;

class CheckStatus extends Action
{
 /**
 * @var \Magento\Framework\Controller\Result\JsonFactory
 */
 protected $resultJsonFactory;

 /**
 * @var \Magento\Customer\Model\Customer 
 */
 protected $_orderFactory;

 /**
 * @param \Magento\Framework\App\Action\Context $context
 * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
 */
 public function __construct(
     \Magento\Framework\App\Action\Context $context,
     \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
     \Magento\Sales\Model\OrderFactory  $orderFactory
 ) {
     $this->resultJsonFactory = $resultJsonFactory;
     $this->_orderFactory = $orderFactory;
     parent::__construct($context);
 }

 public function execute()
 {
     $resultJson = $this->resultJsonFactory->create();
     $orderIncrementId = $this->getRequest()->getParam('id');
     
     $order = $this->_orderFactory->create()->loadByIncrementId($orderIncrementId);
     $orderData = $order->getData();
     if(!empty($orderData)){
         $status = $order->getStatus();
         if($status != ''){
            return $resultJson->setData(['success' => 'true','status' => $status]);           
         } else {
             $status = 'Order does not exists';
        return $resultJson->setData(['success' => 'false','status' => $status]);           
         }
        
    }else {
        $status = 'Order does not exists';
        return $resultJson->setData(['success' => 'false','status' => $status]);           
    }
    
 }
}