<?php
namespace I95Dev\QuickOrder\Controller\Index;
use Magento\Framework\App\Action\Action;

class MobileValidate extends Action
{
 /**
 * @var \Magento\Framework\Controller\Result\JsonFactory
 */
 protected $resultJsonFactory;

 /**
 * @var \Magento\Customer\Model\Customer 
 */
 protected $_customerModel;

 /**
 * @param \Magento\Framework\App\Action\Context $context
 * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
 */
 public function __construct(
     \Magento\Framework\App\Action\Context $context,
     \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
     \Magento\Customer\Model\Customer $customerModel
 ) {
     $this->resultJsonFactory = $resultJsonFactory;
     $this->_customerModel = $customerModel;
     parent::__construct($context);
 }

 public function execute()
 {
     $resultJson = $this->resultJsonFactory->create();
     $mobile = $this->getRequest()->getParam('mobile');
     //$mobiledata = '+91'.$mobile;
     $customerData = $this->_customerModel->getCollection()->addFieldToFilter('customer_number', $mobile);
    $shippingAddressId = '';
    if(count($customerData) > 0) {
        foreach ($customerData as $customer){
            $shippingAddressId = $customer->getDefaultShipping() ? $customer->getDefaultShipping() : '';
        }
        if(isset($shippingAddressId) && $shippingAddressId !=''){
            $message = "true";
        } else {
            $message = "false";
        }
     } else {
       $message = "false";
     }
     $resultJson->setData(['success' => $message]);
     return $resultJson;
 }
}