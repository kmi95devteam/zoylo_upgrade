<?php

namespace I95Dev\QuickOrder\Controller\Index;

class Productdata extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    public function __construct(
    \Magento\Framework\App\Action\Context $context, 
            \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute() {
        $post = $this->getRequest()->getPostValue();
        $coll = $this->getProductData($post['sku']);
        
        echo json_encode($coll, JSON_NUMERIC_CHECK);
        //return $this->_pageFactory->create();
    }
    public function getProductData($sku) {
		
		$_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$productobject = $_objectManager->create('\Magento\Catalog\Model\ProductRepository');
		$product = $productobject->get($sku);
		$price = $product->getPrice();
		$pricHelper = $_objectManager->create('Magento\Framework\Pricing\Helper\Data');
		$formattedPrice = $pricHelper->currency($price, true, false);
		//$currencysymbol = $_objectManager->get('Magento\Store\Model\StoreManagerInterface');
		//$currency = $currencysymbol->getStore()->getCurrentCurrencyCode();
		return $price;
	}
}