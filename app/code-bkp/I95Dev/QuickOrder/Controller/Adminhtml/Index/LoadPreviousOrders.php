<?php
namespace I95Dev\QuickOrder\Controller\Adminhtml\Index;

class LoadPreviousOrders extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;

        protected $resourceConnection;
        
        protected $orderFactory;
        public function __construct(
            \Magento\Framework\App\Action\Context $context,
            \Magento\Framework\App\ResourceConnection $resourceConnection,
            \Magento\Sales\Model\OrderFactory $orderFactory
        )
	{
                $this->resourceConnection = $resourceConnection;
                $this->orderFactory = $orderFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
            $connection = $this->resourceConnection->getConnection();
            $customerId = 0; // default value
            $orderItemsDetails = "";
            $allOrders = array();
            if ($this->getRequest()->getParam('phone_no') && $this->getRequest()->getParam('phone_no') != '')
            {
                $customerQuery = "SELECT entity_id FROM ".$this->resourceConnection->getTableName('customer_entity_varchar')." WHERE attribute_id = (SELECT attribute_id FROM ".$this->resourceConnection->getTableName('eav_attribute')." WHERE attribute_code = 'customer_number' AND entity_type_id='1'  ) AND value LIKE '%".$this->getRequest()->getParam('phone_no')."%'";
                $customerQueryResult = $connection->fetchAll($customerQuery);
                if (isset($customerQueryResult[0]['entity_id']))
                {
                    $customerId = $customerQueryResult[0]['entity_id'];    
                }
            }
            if ($customerId == 0)
            {
                echo "no customer";
                exit;
            }
            /*Query for getting the customer details starts*/
            $customerDetailsQuery = "SELECT firstname, lastname, email, entity_id FROM ".$this->resourceConnection->getTableName('customer_entity')." WHERE entity_id = '".$customerId."'";
            $customerDetailsResult = $connection->fetchAll($customerDetailsQuery);
            /*Query for getting the customer details ends*/           
            $customerDetailsDiv = "<div><p><b>Name:</b>".$customerDetailsResult[0]['firstname'].' '.$customerDetailsResult[0]['lastname']."</p><p><b>Email:</b>".$customerDetailsResult[0]['email']."</p><p><a target='_blank' href='".$this->_url->getUrl("customer/index/edit/id/$customerId")."'>View Customer</a></p></div>";
            $table = $customerDetailsDiv."<table border='1' style='width:100%;font-size:11px;'><thead><th>Order ID</th><th>Order Creation Date</th><th>Order Confirm Date</th><th>Order Status</th><th>Order Items</th><th>Shipping Charges</th><th>Shipping Address</th><th>Payment Method</th><th>Coupon</th><th>Order Total</th><th>Action</th></thead>";
            $tableTR = "";
            $ordersQuery = "SELECT so.entity_id, so.increment_id , so.grand_total,so.status as order_status ,AddTime(so.created_at,'05:30:00') as order_created_at, sop.method as payment_method, so.shipping_description, so.shipping_method, so.shipping_amount,so.coupon_code,so.coupon_code,so.discount_amount,so.subtotal_incl_tax FROM ".$this->resourceConnection->getTableName('sales_order')." so LEFT JOIN ".$this->resourceConnection->getTableName('sales_order_payment')." sop ON so.entity_id = sop.parent_id WHERE so.customer_id = ".$customerId." ORDER BY so.increment_id DESC";
            $allOrders = $connection->fetchAll($ordersQuery);
            foreach($allOrders as $eachOrder) // loop through all the orders to get the order items
            {
                $allItems = []; // array to store all the order items
                $orderItemsQuery = "SELECT name,sku,price_incl_tax,row_total_incl_tax,qty_ordered FROM ".$this->resourceConnection->getTableName('sales_order_item')." WHERE order_id = '".$eachOrder['entity_id']."' ";
                $orderItemsResult = $connection->fetchAll($orderItemsQuery);
                $itemCount = 0;
                $orderItemsTable = "<table border='1' style='width:100%;' ><th>Name</th><th>Qty</th><th>Price</th>";
                $containsItems = 0;
                foreach($orderItemsResult as $eachItem)
                {
                    if ($eachItem['name'] != '') // if the order items table contains any item
                    {
                        $containsItems = 1;
                    }
                    $orderItemsTable .= "<tr><td style='width:70%;' >".$eachItem['name']."</td><td style='width:15%;'>".$eachItem['qty_ordered']."</td><td style='width:15%;'>".$eachItem['row_total_incl_tax']."</td></tr>";
                    $allItems[$itemCount]['name'] = $eachItem['name']; 
                    $allItems[$itemCount]['sku']  = $eachItem['sku']; 
                    $itemCount++;
                }  
                $orderItemsTable .= "<tr><td colspan='3'><span style='float:right;' ><b>Subtotal:</b>".$eachOrder['subtotal_incl_tax']."</span></td></tr>";
                $orderItemsTable .= "</table>";
                if ($containsItems == 0) // if there where no items in the order then do not create the order items table
                {
                    $orderItemsTable = '';
                }
                /*Query for the order status starts*/
                $orderConfirmQuery = "SELECT AddTime(created_at,'05:30:00') as created_at FROM ".$this->resourceConnection->getTableName('sales_order_status_history')." WHERE parent_id = '".$eachOrder['entity_id']."' AND status = 'order_confirmed' ";
                $orderConfirmResult = $connection->fetchAll($orderConfirmQuery);
                /*Query for the order status ends*/
                /*Query for getting the customer order shipping address starts*/
                $orderAddressQuery = "SELECT firstname, middlename, lastname, street, city, region, postcode, telephone FROM ".$this->resourceConnection->getTableName('sales_order_address')." WHERE parent_id = '".$eachOrder['entity_id']."' AND address_type = 'shipping'";
                $orderAddressResult = $connection->fetchAll($orderAddressQuery);
                $customerShippingAddress = "<p><b>".$orderAddressResult[0]['firstname']." ".$orderAddressResult[0]['lastname']." </b></p><p>".$orderAddressResult[0]['street']."</p><p>".$orderAddressResult[0]['city'].", ".$orderAddressResult[0]['region'].", ".$orderAddressResult[0]['postcode']."</p><p>India</p><p>T:".$orderAddressResult[0]['telephone']."</p>";
                /*Query for getting the customer order shipping address ends*/
                $orderConfirmDate = "";
                if (!empty($orderConfirmResult))
                {
                    if (isset($orderConfirmResult[0]['created_at']))
                    {
                        $orderConfirmDate = $orderConfirmResult[0]['created_at'];   
                    }
                }
                $couponData = "";
                if ($eachOrder['coupon_code'] != '')
                {
                    $couponData = "<p>Coupon Code:".$eachOrder['coupon_code']."</p><p>Discount Amount:".$eachOrder['discount_amount']."</p>";
                }
                if ($eachOrder['payment_method'] == 'free')
                {
                    $eachOrder['payment_method'] = 'cashondelivery';
                }
				$entityId = $eachOrder['entity_id'];
                $tableTR = $tableTR."<tr><td>".$eachOrder['increment_id']."</td><td>".$eachOrder['order_created_at']."</td><td>".$orderConfirmDate."</td><td>".$eachOrder['order_status']."</td><td>".$orderItemsTable."</td><td><p>Shipping Cost:".$eachOrder['shipping_amount']."</p><p>Shipping Method:</p>".$eachOrder['shipping_method']."</td><td>".$customerShippingAddress."</td><td>".$eachOrder['payment_method']."</td><td>".$couponData."</td><td>".$eachOrder['grand_total']."</td><td> <a target = '_blank' 
				
				href='".$this->_url->getUrl("sales/order/view/order_id/$entityId")."'> View </a> |
				
				<a target='_blank' 
				href='".$this->_url->getUrl("sales/order_create/reorder/order_id/$entityId")."'>Re-Order</a></td></tr>"; 
            }
            echo $table.$tableTR."</table>";
	}
}