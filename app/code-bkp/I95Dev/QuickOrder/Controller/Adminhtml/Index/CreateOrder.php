<?php

namespace I95Dev\QuickOrder\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Contact\Model\ConfigInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\ObjectManager;

class CreateOrder extends \Magento\Backend\App\Action {

    protected $_pageFactory;
    protected $_storeManager;
    protected $prescription;
    protected $_messageManager;
    protected $logger;
    protected $resourceConnection;
    protected $pincode;
    protected $backendSession;
    
    public function __construct(\Psr\Log\LoggerInterface $logger, \Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Catalog\Model\Product $product, \Magento\Framework\Data\Form\FormKey $formkey, \Magento\Quote\Model\QuoteFactory $quote, \Magento\Quote\Model\QuoteManagement $quoteManagement, \Magento\Customer\Model\CustomerFactory $customerFactory, \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository, \Magento\Sales\Model\Service\OrderService $orderService, \Magento\Customer\Model\AddressFactory $addressFactory, \Magento\Framework\Message\ManagerInterface $messageManager, \I95Dev\UploadPrescription\Model\PrescriptionFactory $prescription,    \Magento\Backend\Model\Auth\Session $authSession,\Magento\Framework\App\ResourceConnection $resourceConnection,\Ecom\Ecomexpress\Model\Pincode $pincode,\Magento\Backend\Model\Session $backendSession
    ) {
        $this->logger = $logger;
        $this->_pageFactory = $pageFactory;
        $this->_storeManager = $storeManager;
        $this->_product = $product;
        $this->_formkey = $formkey;
        $this->quote = $quote;
        $this->quoteManagement = $quoteManagement;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->orderService = $orderService;
        $this->_addressFactory = $addressFactory;
        $this->_prescription = $prescription;
        $this->_messageManager = $messageManager;
	$this->_authSession = $authSession;
        $this->resourceConnection = $resourceConnection;
        $this->pincode = $pincode;
        $this->backendSession = $backendSession;
        return parent::__construct($context);
    }

    public function execute() {
        $this->backendSession->setQuickOrderValues(json_encode($_REQUEST));
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
        $resultpagefactory = $this->_pageFactory->create();
        $post = $this->getRequest()->getPostValue();
        $mobile = $post['mobile'];
     $quote = $this->quote->create();

        $customerObj = $objectManager->create('Magento\Customer\Model\ResourceModel\Customer\Collection');
        $collection = $customerObj->addAttributeToSelect('*')
                ->addAttributeToFilter('customer_number', $mobile)
                ->load();
        /*Code is written by Ajesh for HURR-412 */
        $custGData = $collection->getData();
       if (isset($custGData[0]['group_id']) && $custGData[0]['group_id'] == 4 )
       {
           $this->_messageManager->addError(__("Account already exists and has been blocked. Order cannot be created."));
           $resultRedirect = $this->resultRedirectFactory->create();
           $resultRedirect->setUrl($_SERVER["HTTP_REFERER"]);
           return $resultRedirect;
       }
       /*Code is written by Ajesh for HURR-412 */
        $saveInAddressBook = 0;
        $dataToCheck = [];
        $couponMessage = "";
        $paymentMethod = 'cashondelivery';
        if (isset($_POST['customer_payment_method']))
        {
            $paymentMethod = $_POST['customer_payment_method'];            
        }
        $dataToCheck = array('name'=>'','emailnew'=>'','city'=>'','region'=>'','pincode'=>'');  // intialize the array indexes with the empty values to avoid undefined index errors if it is used anywhere.
        if (isset($_POST['name']))
        {
            $dataToCheck['name'] = $_POST['name'];
        }
        if (isset($_POST['emailnew']))
        {
            $dataToCheck['emailnew'] = $_POST['emailnew'];
        }
        if (isset($_POST['city']))
        {
            $dataToCheck['city'] = $_POST['city'];
        }
        if (isset($_POST['region']))
        {
            $dataToCheck['region'] = $_POST['region'];
        }
        if (isset($_POST['pincode']))
        {
            $dataToCheck['pincode'] = $_POST['pincode'];
        }
        if (isset($_POST['pincode']) && $_POST['pincode'] != '')
        {
            $pinCodeCollection = $this->pincode->getCollection()->addFieldToFilter('pincode', array('eq' => $_POST['pincode']));
            if (empty($pinCodeCollection->getData())) // if there is no pincode then this array will be an empty array
            {
                $this->_messageManager->addError(__('Pincode: ' . $_POST['pincode']." is not serviceable."));
                $resultRedirect = $this->resultRedirectFactory->create();
                $url = $_SERVER['HTTP_REFERER'];
                $resultRedirect->setUrl($url);
                return $resultRedirect;
            }
        }
        
        if (isset($_POST['emailnew']) && $_POST['emailnew'] != '') // if the value in the $_POST['emailnew'] is not empty then it means create new address.
        {
            $saveInAddressBook = 1;
        }
        if (count($collection) > 0) {
			$shippingAddressId = '';
            $cus_data = $collection->getData();
            $email = $cus_data[0]['email'];
            $customer_id = $cus_data[0]['entity_id'];
			foreach ($collection as $customerData){
				$shippingAddressId = $customerData->getDefaultShipping() ? $customerData->getDefaultShipping() : '';
			}

			if(!isset($shippingAddressId) || $shippingAddressId == '' || $_POST['customer_address'] == 'new' ){
			
				$address = $this->_addressFactory->create();
				$address->setCustomerId($customer_id)
						->setFirstname($_POST['first_name'])
						->setLastname($_POST['last_name'])
						->setCountryId("IN")
						->setRegionId($_POST['region'])
						->setPostcode($_POST['pincode'])
						->setCity($_POST['city'])
						->setTelephone($_POST['mobile'])
						->setStreet($_POST['street'])
						->setIsDefaultBilling('1')
						->setIsDefaultShipping('1')
						->setSaveInAddressBook('1');
			}	
		
        } else {
           try {
               $websiteId = $this->_storeManager->getStore()->getWebsiteId();
               $customer = $this->customerFactory->create();
               $customer->setWebsiteId($websiteId);
               $customer->setEmail($_POST['emailnew']);
               $customer->setFirstname($_POST['first_name']);
               $customer->setLastname($_POST['last_name']);
               $customer->setCustomerNumber($_POST['mobile']);
               $customer->setPassword("123456789");
               $customer->save();
           }
           catch(\Exception $e)
           {
               $this->_messageManager->addError(__("Error: ".$e->getMessage()));
               $resultRedirect = $this->resultRedirectFactory->create();
               if (isset($_SERVER['HTTP_REFERER']))
               {
                    $resultRedirect->setUrl($_SERVER['HTTP_REFERER']);
                    return $resultRedirect;                     
               }
               else
               {
                   echo $e->getMessage();
               }
           }
            $customer_id = $customer->getId();

            $address = $this->_addressFactory->create();
            $address->setCustomerId($customer_id)
                    ->setFirstname($_POST['first_name'])
                    ->setLastname($_POST['last_name'])
                    ->setCountryId("IN")
                    ->setRegionId($_POST['region'])
                    ->setPostcode($_POST['pincode'])
                    ->setCity($_POST['city'])
                    ->setTelephone($_POST['mobile'])
                    ->setStreet($_POST['street'])
                    ->setIsDefaultBilling('1')
                    ->setIsDefaultShipping('1')
                    ->setSaveInAddressBook('1');
            
            $email = $_POST['emailnew'];
        }
			
		if (isset($_FILES)) {
			$total1 = $_FILES["pic"]["name"][0];
			$total = count($_FILES['pic']['name']);
			if($total1 != ''){
				if ($total > 0) {
					for ($i = 0; $i < $total; $i++) {
						$fileName = $_FILES["pic"]["name"][$i];
                        $fileSize = $_FILES["pic"]["size"][$i];
                        $maxUploadFileSz = ini_get('upload_max_filesize');
                        $maxUploadFileSz = str_replace('M','',$maxUploadFileSz);
                        $maxUploadFileSz = 10;
                        $maxMBSize = $maxUploadFileSz*1024*1024;
                        if ($fileSize > $maxMBSize)
                        {
                            $this->_messageManager->addError(__('File: ' .$fileName." size is greater than allowed 10MB file size limit."));
                            $resultRedirect = $this->resultRedirectFactory->create();
                            $url = $_SERVER['HTTP_REFERER'];
                            $resultRedirect->setUrl($url);
                            return $resultRedirect;
                        }
						
                        $fileName = str_replace(' ', '-', $fileName);
						$fileName = str_replace("'", "_", $fileName);
						$fileName = str_replace("-", "_", $fileName);
						$fileName = preg_replace('/[^a-zA-Z0-9,.\s]/', '_', $fileName);
						$fileId = "pic[$i]";
						if (isset($customer_id) && $customer_id != 0) {
							$baseTmpPath = "prescriptions/$customer_id";
						} else {
							$baseTmpPath = "prescriptions";
						}
						$uploader = $objectManager->create('\Magento\MediaStorage\Model\File\UploaderFactory')->create(['fileId' => $fileId]);
						$uploader->setAllowCreateFolders(true);
						$uploader->setAllowRenameFiles(true);
						$uploader->setFilesDispersion(false);
						$result = $uploader->save($objectManager->get('\Magento\Framework\Filesystem')->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath($baseTmpPath), $fileName);
						$prescription_id[] = $this->saveFileToDb($customer_id, $fileName);
					}

					$prescription_ids = implode(',', $prescription_id);
				} 
			}
		}
			
        $store = $this->_storeManager->getStore();
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        

        $quote->setStore($store);
        
        $quote->setCurrency();
       


        // Add Product
        $productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
        
        if (isset($_POST['sku'])) {
            $i = 0;
            foreach ($_POST['sku'] as $itemname) {
                $itemnameSkuFull = $itemname;
                if (isset(explode('--',$itemname)[0]))
                {
                    $itemname = explode('--',$itemname)[0];
                }
                $collection = $productCollection->create()
                        ->addAttributeToSelect('*')
                        ->addAttributeToFilter('name', $itemname)
                        ->load();
                if (empty($collection->getData()))
                {
                    $this->_messageManager->addError(__('Product: ' .$itemnameSkuFull." does not exists."));
                    $resultRedirect = $this->resultRedirectFactory->create();
                    $url = $_SERVER['HTTP_REFERER'];
                    $resultRedirect->setUrl($url);
                    return $resultRedirect;  
                }
                
                                        
                $storagetypeCheck = 0;
                foreach ($collection as $product) {
                    $qty = $_POST['qty'][$i];
                    $prod_id = $product->getEntityId();
                    $product = $objectManager->create('Magento\Catalog\Model\Product')->load($prod_id);
					$storagetype = $product->getResource()->getAttribute('storage_type')->getFrontend()->getValue($product);
				
				if($storagetype == 'Cold Storage'){
					$storagetypeCheck = 1;
					$name = $product->getName();
				}
					
                    $storeId = 0; // default store id
                    $productStatQry = "SELECT value FROM ".$this->resourceConnection->getTableName('catalog_product_entity_int')."  WHERE attribute_id = (SELECT attribute_id FROM ".$this->resourceConnection->getTableName('eav_attribute')." WHERE attribute_code = 'status' AND entity_type_id = '4' ) AND entity_id = ".$product->getEntityId()."  AND store_id = '".$storeId."' ";
                    $productStatRes = $this->resourceConnection->getConnection()->fetchAll($productStatQry);
                    if (isset($productStatRes[0]['value']) && $productStatRes[0]['value'] == 2 ) // if product status value is 2 then it is a disabled product
                    {
                      $this->_messageManager->addError(__('Product: ' .$product->getName()." is disabled."));
                      $resultRedirect = $this->resultRedirectFactory->create();
                      $url = $_SERVER['HTTP_REFERER'];
                      $resultRedirect->setUrl($url);
                      return $resultRedirect;  
                    }
                    try 
                    {
                        $quote->addProduct(
                            $product, intval($qty)
                        );
                    } catch (\Exception $e) {
                        $this->_messageManager->addError(__('Error While Saving the order : ' . $e->getMessage().' (Please check if product '.$product->getName().' is in stock)'));
                        $resultRedirect = $this->resultRedirectFactory->create();
                        $url = $_SERVER['HTTP_REFERER'];
                        $resultRedirect->setUrl($url);
                        return $resultRedirect;  
                    }
                    
                }
                
                $i++;
            }
        }


    //  $billingAddressId = $customer->getDefaultBilling();
                $customer = $this->customerFactory->create();
                if (isset($address))
                $address->save();
                $customer->setWebsiteId($websiteId);
                $customer->loadByEmail($email);
                $customer = $this->customerRepository->getById($customer->getEntityId());
                $quote->assignCustomer($customer);
		$shippingAddressId = $customer->getDefaultShipping();
                if (isset($_POST['customer_address']) && ($_POST['customer_address'] != 0 || $_POST['customer_address'] != 'new'))
                {
                    $shippingAddressId = $_POST['customer_address'];
                }
                $shippingAddress = $this->_addressFactory->create()->load($shippingAddressId);
		$firstname = $shippingAddress->getFirstname();
		$lastname = $shippingAddress->getLastname();
		$region_id = $shippingAddress->getRegionId();
		$streets = $shippingAddress->getStreet();
		$street = $streets[0];
		$city = $shippingAddress->getCity();
		$country_id = $shippingAddress->getCountryId();
		$region = $shippingAddress->getRegion();
		$postcode = $shippingAddress->getPostcode();
		$telephone = $shippingAddress->getTelephone();
		
                if (isset($postcode) && $postcode != '')
                {
				//written by achyut start
				if($storagetypeCheck == 1){	
				$serviceblemodel = $objectManager->get('I95Dev\Serviceablepincodes\Model\DataPincodes')->getCollection()->addFieldToFilter('pincode', array('eq' => $postcode));
				if($serviceblemodel->count() <= 0){
					 
					$message = 'Your Pincode is not serviceble for the item '.$name;
                    $this->_messageManager->addError($message);//For Error Message
					$resultRedirect = $this->resultRedirectFactory->create();
                        $url = $_SERVER['HTTP_REFERER'];
                        $resultRedirect->setUrl($url);
                        return $resultRedirect;
				}
				
				}
               //written by achyut end  
					
					
                    $pinCodeCollectionT = $this->pincode->getCollection()->addFieldToFilter('pincode', array('eq' => $postcode));
                    if (empty($pinCodeCollectionT->getData())) // if there is no pincode then this array will be an empty array
                    {
                        $this->_messageManager->addError(__('Pincode: ' . $postcode." is not serviceable."));
                        $resultRedirect = $this->resultRedirectFactory->create();
                        $url = $_SERVER['HTTP_REFERER'];
                        $resultRedirect->setUrl($url);
                        return $resultRedirect;
                    }
                }
                    $quote->getShippingAddress()->addData(array(
			 'customer_address_id' => '',
			 'prefix' => '',
			 'firstname' => $firstname,
			 'middlename' => '',
			 'lastname' => $lastname,
			 'suffix' => '',
			 'company' =>'', 
			 'street' => array(
					 '0' => $street
				 ),
			 'city' => $city,
			 'country_id' => $country_id,
			 'region' => $region,
			 'region_id' => $region_id,
			 'postcode' => $postcode,
			 'telephone' => $telephone,
			 'fax' => '',
			 'vat_id' => '',
			 'save_in_address_book' => $saveInAddressBook
		 ));
        $shippingAddress = $quote->getShippingAddress();
        $shippingAddress->setCollectShippingRates(true)
                ->collectShippingRates()
                ->setShippingMethod('ecomexpress_ecomexpress');
        $quote->setPaymentMethod($paymentMethod);
        if (isset($prescription_ids)) {
            $quote->setPrescriptionValues($prescription_ids);
        }
		if(isset($post['store_credit_check']) && $post['store_credit_check'] == 1){
			$quote->setStorecredit($post['store_credit']);
			$quote->setBaseStorecredit($post['store_credit']);
		}
		$quote->setSource($post['source']);
		$quote->setOrderBy($post['current_user']);
		$quote->setOrigin('CCTeam');
        $couponCode = $post['coupon_code'];
        $quote->setCouponCode($couponCode)->collectTotals()->save();
        $quote->setInventoryProcessed(false);
        $quote->save();
        $quote->getPayment()->importData(['method' => $paymentMethod]);
        $quote->collectTotals()->save();
        if ($quote->getCouponCode() == '' && isset($_POST['coupon_code']) && $_POST['coupon_code'] != '') // if invalid or no coupon is applied.
        {
            $couponMessage = '<span style ="color:red;" ><b>Coupon Code: '.$post['coupon_code'].' not a valid coupon code.</b></span>';
            $this->_messageManager->addError(__($couponMessage));
            $resultRedirect = $this->resultRedirectFactory->create();
            $url = $_SERVER['HTTP_REFERER'];
            $resultRedirect->setUrl($url);
            return $resultRedirect;  
        }
        else if (isset($_POST['coupon_code']) && $_POST['coupon_code'] != '')
        {
            $couponMessage = '<span><b>Coupon Code: '.$post['coupon_code'].' applied.</b></span>';
        }
		
		
	
        try {
            $order = $this->quoteManagement->submit($quote);
			if(isset($post['comments'])){
				$comment = $post['comments'];
				$order->addStatusHistoryComment($comment)->setIsCustomerNotified(false)->setEntityName('order')->save();
			}
			$increment_id = $order->getRealOrderId();
            $order->setEmailSent(0);
//			$customer_entity_id = $post['customerId'];
//			// $customer_entity_id = 264691;
//			if(isset($customer_entity_id)){
//				$customerData = $this->customerRepository->getById($customer_entity_id);
//				if($customerStoreCredit = $customerData->getCustomAttribute('store_credit')){ 
//					$customerStoreCredit = $customerStoreCredit->getValue();
//					$base_storecredit = $order->getBaseStorecredit();
//					
//					if($base_storecredit > $customerStoreCredit){
//						$customerData->setCustomAttribute('store_credit', 0);
//					}else{
//						$finalStoreCredit = $customerStoreCredit - $base_storecredit;
//						$customerData->setCustomAttribute('store_credit', $finalStoreCredit);
//					}
//					
//					$this->customerRepository->save($customerData);
//				}
//			}
		
		
            if ($increment_id) {
                $result['order_id'] = $order->getRealOrderId();
                $orderObj = $objectManager->create('Magento\Sales\Model\Order')->load($order->getId());
                
                $subtotal = $orderObj->getSubTotal();
                $maximumPrice = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface')->getValue("carriers/ecomexpress/shipping_subtotal");
                $price = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface')->getValue("carriers/ecomexpress/handling_charge");
                if($subtotal > $maximumPrice){
                    $shippingprice = 0.00;
                }else {
                    $shippingprice = $price;
                }
                $orderObj->setShippingAmount($shippingprice);
                $orderObj->setBaseShippingAmount($shippingprice);
                $orderObj->setGrandTotal($orderObj->getGrandTotal() + $shippingprice); //adding shipping price to grand total
                $orderObj->save();
            } else {
                $result = ['error' => 1, 'msg' => 'Error While Saving the order'];
            }
        } catch (\Exception $e) {
			 $this->_messageManager->addError(__('Error While Saving the order :' . $e->getMessage()));
        }

		if(isset($increment_id)){
                        if ($post['coupon_code'] == '') // if there was no coupon code was passed.
                        {
                            $this->_messageManager->addSuccess(__('Your order has been saved with order Id : ' . $increment_id));   
                        }
                        else
                        {
                            $this->_messageManager->addSuccess(__('Your order has been saved with order Id : ' . $increment_id.'  '.$couponMessage));
                        }
                        
                        $this->backendSession->unsQuickOrderValues();  
		}
        $resultRedirect = $this->resultRedirectFactory->create();
        $url = $_SERVER['HTTP_REFERER'];
        $resultRedirect->setUrl($url);
        return $resultRedirect;
    }

    public function saveFileToDb($customerId, $fileName) {
        try {
            $fileName = str_replace("(", "_", $fileName);
			$fileName = str_replace(")", "_", $fileName);
			$fileName = preg_replace('/[^a-zA-Z0-9,.\s]/', '_', $fileName);
            $mediaUrl = $this->_storeManager->getStore()
                    ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            $baseTmpPath = "prescriptions/$customerId/";
            $filepullpath = $mediaUrl . $baseTmpPath . $fileName;
            $prescriptionmodel = $this->_prescription->create();
            $prescriptionmodel->setCustomerId($customerId);
            $prescriptionmodel->setPrescriptionName($fileName);
            $prescriptionmodel->setLabel($fileName);
            $prescriptionmodel->setValue($filepullpath);
            $prescriptionmodel->setCreatedDate(date('Y-m-d H:i:s', time()));
            $prescriptionmodel->save();
            $prescription_id = $prescriptionmodel->getPrescriptionId();
        } catch (\Exception $e) {
            $this->logger->info($e->getMessage());
        }

        return $prescription_id;
    }

}
