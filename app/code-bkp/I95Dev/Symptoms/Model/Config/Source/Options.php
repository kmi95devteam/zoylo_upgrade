<?php
namespace I95Dev\Symptoms\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;
use I95Dev\Symptoms\Model\ResourceModel\Symptoms\CollectionFactory;
/**

* Custom Attribute Renderer

*/

class Options extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

	/**
    * @var OptionFactory
    */
    public $optionFactory;

    /**
     * @var symptomsCollectionFactory
     */
    public $symptomsCollectionFactory;

    public function __construct(
        OptionFactory $optionFactory,
        CollectionFactory $symptomsCollectionFactory
    ) {
        
        $this->optionFactory = $optionFactory;
        $this->symptomsCollectionFactory = $symptomsCollectionFactory;
    }

	/**
	* Get all options
	*
	* @return array
	*/
	public function getAllOptions()

	{

        $this->_options = [];
        $datas = $this->getSymptomsCollection();
        
        foreach ($datas as $data) {
            $this->_options[] = [
                'label' => trim($data->getName()),
                'value' => trim($data->getName()),
            ];
        }

        return $this->_options;

	}
	
	
    public function getSymptomsCollection()
    {
        
        /** @var $collection \I95Dev\Symptoms\Model\ResourceModel\Symptoms\CollectionFactory */
        $collection = $this->symptomsCollectionFactory->create();
        return $collection;
    }
}

