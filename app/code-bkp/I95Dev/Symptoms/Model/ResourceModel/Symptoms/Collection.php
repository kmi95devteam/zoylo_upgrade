<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace I95Dev\Symptoms\Model\ResourceModel\Symptoms;

/**
 * Symptomss Collection
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('I95Dev\Symptoms\Model\Symptoms', 'I95Dev\Symptoms\Model\ResourceModel\Symptoms');
    }
}
