<?php
/**
 * Copyright © 2015 I95Dev. All rights reserved.
 */

namespace I95Dev\Symptoms\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();
 
        if (version_compare($context->getVersion(), '2.0.1', '<')) {
          $installer->getConnection()->addColumn(
                $installer->getTable('i95dev_symptoms'),
                'popular',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    'nullable' => true,
                    'comment' => 'Popular'
                ]
            );
        }
        $installer->endSetup();
    }
}
