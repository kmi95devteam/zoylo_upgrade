<?php
namespace I95Dev\Symptoms\Block\Adminhtml\Symptoms\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected function _construct()
    {
		
        parent::_construct();
        $this->setId('checkmodule_symptoms_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Symptoms Information'));
    }
}