<?php
namespace I95Dev\Symptoms\Block\Adminhtml;
class Symptoms extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
		
        $this->_controller = 'adminhtml_symptoms';/*block grid.php directory*/
        $this->_blockGroup = 'I95Dev_Symptoms';
        $this->_headerText = __('Symptoms');
        $this->_addButtonLabel = __('Add New'); 
        parent::_construct();
		
    }
}
