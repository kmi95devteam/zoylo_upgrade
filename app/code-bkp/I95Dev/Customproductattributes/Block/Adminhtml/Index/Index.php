<?php

namespace I95Dev\Customproductattributes\Block\Adminhtml\Index;


class Index extends \Magento\Framework\View\Element\Template {

    /**
     * @var \I95Dev\Salts\Model\SaltFactory
     */
    public $saltFactory;

    /**
     *
     * @param \I95Dev\Salts\Model\SaltFactory $saltFactory
	 */
    public function __construct(\Magento\Catalog\Block\Product\Context $context,
								\I95Dev\Salts\Model\SaltFactory $saltFactory, 
								array $data = []) 
	{
		$this->saltFactory = $saltFactory;
        parent::__construct($context, $data);

    }


    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
	
	public function getAllSalts()
    {
		$saltCollection = $this->saltFactory
								->create()
								->getCollection();
		return $saltCollection;
    }

}