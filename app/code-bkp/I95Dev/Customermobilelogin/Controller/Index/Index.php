<?php

namespace I95Dev\Customermobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Model\Customer;

class Index extends \Magento\Framework\App\Action\Action {

    protected $resultPageFactory;
    private $customerFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Customer\Model\CustomerFactory $customer
     */
    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Customer\Model\CustomerFactory $customerFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->customerFactory = $customerFactory;
        parent::__construct($context);
    }

    public function execute() {

        $mobile = $this->getRequest()->getParam('mobile');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $baseurl = $storeManager->getStore()->getBaseUrl();
        $messageinterface = $objectManager->create('\Magento\Framework\Message\ManagerInterface');
        $CustomerModelcollection = $objectManager->create('\Magento\Customer\Model\ResourceModel\Customer\Collection');
        $CustomerModelcollection->addAttributeToSelect("*")
                ->addAttributeToFilter("mobile_number", array("eq" => $mobile));
        if (count($CustomerModelcollection) > 0) {
            foreach ($CustomerModelcollection as $customer) {
            }
            $Customesession = $objectManager->create('Magento\Customer\Model\Session');
            if ($customer->getId()) {
                $Customesession->setCustomerAsLoggedIn($customer);

                if ($Customesession->isLoggedIn()) {
                    return $this->_redirect('customer/account/');
                } else {

                    return $this->_redirect($baseurl);
                }
            } else {

                $messageinterface->addError('tesat');
                return $this->_redirect($baseurl);
            }
        } else {
            echo 'customer dosnot exist';
             return $this->_redirect($baseurl);
        }
    }

}
