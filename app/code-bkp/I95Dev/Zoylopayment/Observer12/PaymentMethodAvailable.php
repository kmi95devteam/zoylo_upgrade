<?php

namespace I95Dev\Zoylopayment\Observer;

use Magento\Framework\Event\ObserverInterface;

class PaymentMethodAvailable implements ObserverInterface {

    protected $_checkoutSession;

    public function __construct(
    \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * payment_method_is_active event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $quote = $observer->getEvent()->getQuote();
        $quoteItems = $this->_checkoutSession->getQuote()->getAllVisibleItems();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        foreach ($quoteItems as $item) {

            $product = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface')->getById($item->getProductId());
            $checkObpr[] = $product->getObpr();
        }

        if (count(array_filter($checkObpr)) == count($checkObpr)) {
            if (in_array(0, $checkObpr)) {
                if ($observer->getEvent()->getMethodInstance()->getCode() == "zoylopaymentmethod") {
                    $checkResult = $observer->getEvent()->getResult();
                    $checkResult->setData('is_available', false); //this is disabling the payment method at checkout page
                }
            }
        } else {
            if ($observer->getEvent()->getMethodInstance()->getCode() == "zoylopaymentmethod") {
                $checkResult = $observer->getEvent()->getResult();
                $checkResult->setData('is_available', false); //this is disabling the payment method at checkout page
            }
        }
    }

}
