<?php

namespace I95Dev\Zoylocategoryattributes\Setup;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

class UpgradeData implements UpgradeDataInterface {

    private $eavSetupFactory;
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
		if (version_compare($context->getVersion(), '1.0.5', '<=')) {
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'top_sku');
		    
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'top_sku', 
					[					 
					'type' => 'varchar',					 
					'backend' => '',					 
					'frontend' => '',					 
					'label' => 'TOP SKU',					 
					'input' => 'boolean',					 
					'class' => '',
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable', 
			]	
		);
	 }
    }

}
