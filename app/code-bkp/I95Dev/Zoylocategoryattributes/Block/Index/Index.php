<?php

namespace I95Dev\Zoylocategoryattributes\Block\Index;

use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\View\Element\Template\Context;

class Index extends \Magento\Framework\View\Element\Template {

	protected $_productFactory;

	public function __construct(
	   Context $context, 
	   ProductFactory $productFactory,
	   array $data = array()       
	) {
	   $this->_productFactory   = $productFactory;
	   parent::__construct($context, $data);
	}

	protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
	
	public function getProductsByComposition($composition, $product_id, $drug_type) {
		
	   $productCollection = $this->_productFactory->create()
												->getCollection()
												->addAttributeToSelect('*')							
												->addAttributeToFilter('salt_composition',array('eq' => $composition))
												->addAttributeToFilter('drug_type',array('like' => '%'.$drug_type.'%'))
												->addFieldToFilter('entity_id', array('neq' => $product_id))
												->addAttributeToSort('price', 'asc')
												->setPageSize(3);	   
	   return $productCollection;
		
	}


    

}