<?php
namespace I95Dev\UploadPrescription\Api\Data;
/**
 * @api
 *
 */
interface PartnerPrescriptionsOrderInterface
{

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess();

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success);

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage();

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * Get Status
     *
     * @return string
     */
    public function getStatus();

    /**
     * Set Status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * Get OrderId
     *
     * @return string
     */
    public function getOrderId();

    /**
     * Set Order Id
     *
     * @param string $status
     * @return $this
     */
    public function setOrderId($incrementId);
}