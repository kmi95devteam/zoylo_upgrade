<?php
namespace I95Dev\UploadPrescription\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	
	protected $_storeManager;
	
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Session $customerSession,
		\Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->customerSession = $customerSession;
		$this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    public function isLoggedIn()
    {
        return $this->customerSession->isLoggedIn();
    }
	
	 public function getLoggedInCustomerId()
    {
        return $this->customerSession->getCustomerId();
    }
	
	 public function getLoggedinCustomerIds()
    {	
        return $this->customerSession->getId();
    }
	
	public function getBaseUrl(){
		return $this->_storeManager->getStore()->getBaseUrl();
	}
	
	public function getMediaurl(){
		return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
	}
	
	
}