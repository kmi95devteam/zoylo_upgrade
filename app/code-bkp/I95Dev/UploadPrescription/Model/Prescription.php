<?php

namespace I95Dev\UploadPrescription\Model;
use Magento\Framework\Model\AbstractModel;

class Prescription extends AbstractModel
{

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init("I95Dev\UploadPrescription\Model\ResourceModel\Prescription");
    }
    
}