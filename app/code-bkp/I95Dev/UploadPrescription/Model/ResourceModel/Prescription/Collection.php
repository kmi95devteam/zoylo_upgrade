<?php

namespace I95Dev\UploadPrescription\Model\ResourceModel\Prescription;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('I95Dev\UploadPrescription\Model\Prescription', 'I95Dev\UploadPrescription\Model\ResourceModel\Prescription');
    }
}