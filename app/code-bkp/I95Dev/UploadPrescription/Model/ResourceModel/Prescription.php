<?php

namespace I95Dev\UploadPrescription\Model\ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Prescription extends AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('i95dev_prescription_details', 'prescription_id');
    }
}