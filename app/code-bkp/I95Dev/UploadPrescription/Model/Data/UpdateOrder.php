<?php

namespace I95Dev\UploadPrescription\Model\Data;

class UpdateOrder extends \Magento\Framework\Model\AbstractModel implements
    \I95Dev\UploadPrescription\Api\Data\UpdateOrderInterface
{
    const KEY_CODE          = 'code';
    const KEY_Success       = 'success';
    const KEY_Message       = 'message';
    const KEY_Status        = 'status';
    const KEY_PrescriptionValues = 'prescription_values';
    const KEY_OrderInfo     = 'order_info';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Get Code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->_getData(self::KEY_CODE);
    }

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code)
    {
        return $this->setData(self::KEY_CODE, $code);
    }

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess()
    {
        return $this->_getData(self::KEY_Success);
    }

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success)
    {
        return $this->setData(self::KEY_Success, $success);
    }

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->_getData(self::KEY_Message);
    }

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        return $this->setData(self::KEY_Message, $message);
    }

    /**
     * Get Status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->_getData(self::KEY_Status);
    }

    /**
     * Set Status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        return $this->setData(self::KEY_Status, $status);
    }

    /**
     * Get Prescription_values
     *
     * @return string
     */
    public function getPrescriptionValues()
    {
        return $this->_getData(self::KEY_PrescriptionValues);
    }

    /**
     * Set Prescription_values
     *
     * @param string $prescription_values
     * @return $this
     */
    public function setPrescriptionValues($prescription_values)
    {
        return $this->setData(self::KEY_PrescriptionValues, $prescription_values);
    }

    /**
     * Get OrderInfo
     *
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function getOrderInfo()
    {
        return $this->_getData(self::KEY_OrderInfo);
    }

    /**
     * Set OrderInfo
     *
     * @param \Magento\Sales\Api\Data\OrderInterface
     * @return $this
     */
    public function setOrderInfo(array $order = null)
    {
        return $this->setData(self::KEY_OrderInfo, $order);
    }
}