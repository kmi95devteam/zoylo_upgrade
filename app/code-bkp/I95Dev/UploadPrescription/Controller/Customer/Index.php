<?php 
namespace I95Dev\UploadPrescription\Controller\Customer;  

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Customer;

class Index extends \Magento\Framework\App\Action\Action { 
  
	protected $customerSession;

    protected $customers;

    public function __construct(
        Context $context,
        Session $customerSession,
        Customer $customers
    )
    {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->customers = $customers;       
    }

	
 public function execute() { 
  
		if ($this->customerSession->isLoggedIn()) 
		{
			$this->_view->loadLayout(); 
			$this->_view->renderLayout(); 
		}
		else{
			$resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('customer/account/login/');
			return $resultRedirect; 
		}
   
  } 
  
} 
?> 