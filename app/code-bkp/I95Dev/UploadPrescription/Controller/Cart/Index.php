<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace I95Dev\UploadPrescription\Controller\Cart;

class Index extends \Magento\Checkout\Controller\Cart
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\I95Dev\UploadPrescription\Helper\Data $helper
    ) {
        parent::__construct(
            $context,
            $scopeConfig,
            $checkoutSession,
            $storeManager,
            $formKeyValidator,
            $cart,
			$helper
        );
        $this->resultPageFactory = $resultPageFactory;
        $this->helper = $helper;
    }

    /**
     * Shopping cart display action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        echo '<script>    window.onload = function () { if (! localStorage.cartOnce) { localStorage.setItem("cartOnce", "true"); window.location.reload(); } }</script>';
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Shopping Cart'));
		
		$baseurl = $this->helper->getBaseUrl();
		$customerId = $this->helper->getLoggedInCustomerId();
        $token = $this->_objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('singin/single_sign/sign_token');
		$referUrl = 'checkout/cart/';
		if($customerId == '') { 
		$cSession = 0;
                
		?>
		<form class="form form-login" action="<?php echo $baseurl; ?>signin/index/index/" method="post" id="zoylo-login-form" novalidate="novalidate">
                    <input name="form_key" value="LKhgY51FyHL9CnDr" type="hidden">
                    <input name="zoylotoken" id="zoylotoken" value="" type="hidden">
                    <input name="login[zoylotoken]" type="hidden" id="zoylotoken" value="0">
                    <input name="refer" type="hidden" id="refer" value="<?php echo $referUrl;?>">
                    <input name="zoyloflag" type="hidden" id="zoyloflag" value="<?php echo $cSession; ?>">
                </form> 

                <script type="text/javascript">
                    var user_token = localStorage.getItem("<?php echo $token?>");
                    var isLogging = 0;
                    isLogging = document.getElementById("zoyloflag").value;
                    document.getElementById("zoylotoken").value = user_token;
                    if ((user_token != null) && (isLogging == 0)) {
                        var data = document.getElementById("zoylo-login-form").submit();
                    }
                </script> 
	<?php		
			}	
        return $resultPage;
    }
}
