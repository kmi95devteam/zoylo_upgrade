<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace I95Dev\UploadPrescription\Controller\Checkout;

use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Session;
use Magento\Checkout\Model\Cart;
use Magento\Catalog\Model\ProductRepository;
class Remove extends \Magento\Framework\App\Action\Action
{
	
	protected $checkoutsession;
	protected $cart;

    public function __construct(
		Context $context,
        Session $checkoutsession,
        Cart $cart,
        ProductRepository $productrepository
    )
    {
		parent::__construct($context);
        $this->checkoutsession = $checkoutsession;       
        $this->cart = $cart;   			
        $this->productrepository = $productrepository;   			
    }
	
	
    /**
     * Delete shopping cart item action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
     public function execute() { 

		$checkoutSession = $this->checkoutsession;
		$allItems = $checkoutSession->getQuote()->getAllVisibleItems();
		$cart =  $this->cart;
		 
		if(count($allItems) > 0){
			foreach ($allItems as $item) {
				$sku = $item->getSku();
				$itemId = $item->getItemId(); //item id of particular item
				$productRepository = $this->productrepository;
				$product = $productRepository->get(trim($sku));
				$prescriptionAction = $product->getPrescriptionRequired(); 
				if($prescriptionAction == 'required')
					{
						$cart->removeItem($itemId)->save();
					}
				
			}
		}
		$resultRedirect = $this->resultRedirectFactory->create();
		$resultRedirect->setPath('checkout/cart/index');
		return $resultRedirect;		
	}
}
