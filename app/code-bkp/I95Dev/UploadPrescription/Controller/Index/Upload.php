<?php

namespace I95Dev\UploadPrescription\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Model\Session;

class Upload extends \Magento\Framework\App\Action\Action {

    protected $resultPageFactory;
    protected $jsonHelper;
    private $coreFileStorageDatabase;
    private $mediaDirectory;
    private $uploaderFactory;
    private $storeManager;
    private $logger;
    public $basePath;
    public $allowedExtensions;
    protected $customerSession;
    protected $_prescription;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
    \Magento\Framework\App\Action\Context $context, Session $customerSession, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\Json\Helper\Data $jsonHelper, \Magento\MediaStorage\Helper\File\Storage\Database $coreFileStorageDatabase, \Magento\Framework\Filesystem $filesystem, \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Psr\Log\LoggerInterface $logger, \I95Dev\UploadPrescription\Model\PrescriptionFactory $prescription, \I95Dev\S3\Helper\Data $helper
    ) {
        $this->customerSession = $customerSession;
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->coreFileStorageDatabase = $coreFileStorageDatabase;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->uploaderFactory = $uploaderFactory;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
        $this->basePath = "prescriptions";
        $this->allowedExtensions = ['jpg', 'jpeg', 'gif', 'png', 'jfif', 'pdf'];
        $this->_prescription = $prescription;
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function execute() {
        try {

            //  $data = $this->getRequest()->getPost();
            //	$files = $this->getRequest()->getFiles();

            if (!empty($_FILES)) {
                $newName = $_FILES['file']['name'];
            $newName = str_replace(' ', '_', $newName);
            $newName = str_replace("'", "_", $newName);
            $newName = str_replace("-", "_", $newName);
			$newName = preg_replace('/[^a-zA-Z0-9,.\s]/', '_', $newName);
                $result = $this->saveFileToDir($newName);
                $saveData = $this->saveFileToDb($newName);
                $result['url'] = $this->storeManager
                                ->getStore()
                                ->getBaseUrl(
                                        \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                                ) . $this->getFilePath($this->basePath, $result['file']);

                if ($result['file']) {
                    $result['success'] = 'true';
                    $resultPage = $this->resultPageFactory->create();
                    $resultPage->getConfig()->getTitle()->prepend(__(' heading '));
                    //	$this->messageManager->addSuccess(__('Prescription has been successfully uploaded')); 
                }
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }

        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }

    public function setBasePath($basePath) {
        $this->basePath = $basePath;
    }

    public function setAllowedExtensions($allowedExtensions) {
        $this->allowedExtensions = $allowedExtensions;
    }

    public function getBasePath() {
        return $this->basePath;
    }

    public function getAllowedExtensions() {
        return $this->allowedExtensions;
    }

    public function getFilePath($path, $imageName) {
        return rtrim($path, '/') . '/' . ltrim($imageName, '/');
    }

    public function saveFileToDir($newName) {

        try {

            if ($this->customerSession->isLoggedIn()) {
                $customerId = $this->customerSession->getCustomerData()->getId();
                $baseTmpPath = $this->getBasePath() . "/$customerId";
            } else {
                $baseTmpPath = $this->getBasePath();
            }

            $uploader = $this->uploaderFactory->create(['fileId' => 'file']);
            $uploader->setAllowCreateFolders(true);
            $uploader->setAllowedExtensions($this->getAllowedExtensions());
            $uploader->setAllowRenameFiles(false);
            $uploader->setFilesDispersion(false);
            //$uploader->getFileExtension();

            $result = $uploader->save($this->mediaDirectory->getAbsolutePath($baseTmpPath), $newName);

            if (!$result) {
                throw new \Magento\Framework\Exception\LocalizedException(
                __('File can not be saved to the destination folder.')
                );
            }

            $result['tmp_name'] = str_replace('\\', '/', $result['tmp_name']);
            $result['path'] = str_replace('\\', '/', $result['path']);
            $result['url'] = $this->storeManager
                            ->getStore()
                            ->getBaseUrl(
                                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                            ) . $this->getFilePath($baseTmpPath, $result['file']);
            $result['name'] = $result['file'];
            if (isset($result['file'])) {
                try {
                    $relativePath = rtrim($baseTmpPath, '/') . '/' . ltrim($result['file'], '/');
                    $this->coreFileStorageDatabase->saveFile($relativePath);
                } catch (\Exception $e) {
                    $this->logger->critical($e);
                    throw new \Magento\Framework\Exception\LocalizedException(
                    __('Something went wrong while saving the file(s).')
                    );
                }
            }
        } catch (Exception $e) {
            $result = array(
                'error' => $e->getMessage(),
                'errorcode' => $e->getCode());
        }
        return $result;
    }

    public function saveFileToDb($newName) {
        $newName = str_replace("(", "_", $newName);
        $newName = str_replace(")", "_", $newName);
        $newName = preg_replace('/[^a-zA-Z0-9,.\s]/', '_', $newName);
        //$newName = preg_replace('/_+/', '_', $newName);
        $mediaUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $customerId = $this->customerSession->getCustomerData()->getId();
        $baseTmpPath = $this->getBasePath() . "/$customerId/";
        $filepullpath = $mediaUrl . $baseTmpPath . $newName;
        $prescriptionmodel = $this->_prescription->create();
        $prescriptionmodel->setCustomerId($customerId);
        $prescriptionmodel->setPrescriptionName($newName);
        $prescriptionmodel->setEmail();
        $prescriptionmodel->setValue($filepullpath);
        $prescriptionmodel->setCreatedDate(date('Y-m-d H:i:s', time()));
        $prescriptionmodel->setStatus(1);
        $prescriptionmodel->save();
    }

}
