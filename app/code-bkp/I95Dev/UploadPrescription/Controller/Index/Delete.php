<?php
namespace I95Dev\UploadPrescription\Controller\Index;

class Delete extends \Magento\Framework\App\Action\Action
{
/**
 * @var \Magento\Framework\Controller\Result\JsonFactory
 */
 protected $resultJsonFactory;

 /**
 * @param \Magento\Framework\App\Action\Context $context
 * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
 */
 public function __construct(
     \Magento\Framework\App\Action\Context $context,
     \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
 ) {
     $this->resultJsonFactory = $resultJsonFactory;
     parent::__construct($context);
 }
 
 
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
		 $resultJson = $this->resultJsonFactory->create();
		$prescription_id = $this->getRequest()->getParam('prescription_id');
		try {
				$prescriptions = $this->_objectManager->get('I95Dev\UploadPrescription\Model\Prescription')->load($prescription_id);
				//$path = $prescriptions->getValue();
				if($prescription_id){
					$prescriptions->setStatus(0);
					$prescriptions->save();
					//$prescriptions->delete();
					$message = "true";
				}else{
					$message = "false";
				} 
                //$this->messageManager->addSuccess(__('Prescription Deleted successfully !'));
				$resultJson->setData(['success' => $message]);
				return $resultJson;
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
	    //$this->_redirect('*/customer/index/');
    }
}
