<?php

namespace I95Dev\UploadPrescription\Block\Index;

class Save extends \Magento\Framework\View\Element\Template {

    protected $_prescription;
    protected $_checkoutSession;
    protected $productInterface;

    public function __construct(\I95Dev\UploadPrescription\Model\PrescriptionFactory $prescription, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Catalog\Api\Data\ProductInterface $productInterface, \Magento\Framework\View\Element\Template\Context $context, array $data = []) {

        
        parent::__construct($context, $data);
        $this->_prescription = $prescription;
        $this->_checkoutSession = $checkoutSession;
        $this->productInterface = $productInterface;
    }

    protected function _prepareLayout() {
        return parent::_prepareLayout();
    }

    public function getPrescriptions($customerid) {

        $collection = $this->_prescription->create()
                ->getCollection()
                ->addFieldToFilter('customer_id', array('eq' => $customerid))
                ->setOrder('prescription_id', 'DESC');
        return $collection;
    }

    public function getSelectedPrescriptions($customerid, $resultstr) {

        $collection = $this->_prescription->create()
                ->getCollection()
                ->addFieldToFilter('customer_id', array('eq' => $customerid))
                ->addFieldToFilter('prescription_id', array('in' => $resultstr))
                ->setOrder('prescription_id', 'DESC');
        return $collection;
    }

    public function getprescriptionItemsCount() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $checkoutsession = $objectManager->create('Magento\Checkout\Model\Session');
        //$checkoutsession = $this->_checkoutSession;
        $quote = $checkoutsession->getQuote();
        $quoteId = $quote->getId();
        $allItems = $quote->getAllItems();
        $required = array();
        if (count($allItems) > 0) {
            foreach ($allItems as $item) {
                $productId = $item->getProduct()->getId();
                $sku = $item->getProduct()->getSku();
                $product = $this->productInterface->load($productId);
                $product->getPrescriptionRequired();
                if ($sku != 'Free') {
                    if ($product->getPrescriptionRequired() == 'required') {
                        $required[] = $productId;
                    }
                }
            }
        }
        return $required;
    }

    public function getnonprescriptionItemsCount() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $checkoutsession = $objectManager->create('Magento\Checkout\Model\Session');
        //$checkoutsession = $this->_checkoutSession;
        $quote = $checkoutsession->getQuote();
        $quoteId = $quote->getId();
        $allItems = $quote->getAllItems();
        $notrequired = array();
        if (count($allItems) > 0) {
            foreach ($allItems as $item) {
                $productId = $item->getProduct()->getId();
                $sku = $item->getProduct()->getSku();
                $product = $this->productInterface->load($productId);
                $product->getPrescriptionRequired();
                if ($sku != 'Free') {
                    if ($product->getPrescriptionRequired() != 'required') {
                        $notrequired[] = $productId;
                    }
                }
            }
        }
        return $notrequired;
    }

}
