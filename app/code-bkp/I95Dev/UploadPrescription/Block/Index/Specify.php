<?php
namespace I95Dev\UploadPrescription\Block\Index;

class Specify extends \Magento\Framework\View\Element\Template {

	protected $_prescription;
	
	protected $customerSession;
 
    public function __construct(\I95Dev\UploadPrescription\Model\PrescriptionFactory  $prescription,
	\Magento\Customer\Model\Session $customerSession, 
	\Magento\Framework\View\Element\Template\Context $context, array $data = []) {

        parent::__construct($context, $data);
		$this->_prescription = $prescription;
	    $this->customerSession = $customerSession;
	 }

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
	public function setMyValue($myValue)
	{
		return $this->customerSession->setMyValue($myValue); //set value in customer session
	}

	public function getMyValue()
	{
		return $this->customerSession->getMyValue(); //Get value from customer session
	}
	
}