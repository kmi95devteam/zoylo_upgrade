<?php
/**
 * Copyright © 2015 I95Dev. All rights reserved.
 */
namespace I95Dev\BranchInformation\Model\ResourceModel;

/**
 * Branch resource
 */
class Branch extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('branchinformation_branch', 'id');
    }

  
}
