<?php 
namespace I95Dev\Serviceablepincodes\Model\ResourceModel\DataPincodes;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection{
	public function _construct(){
		$this->_init("I95Dev\Serviceablepincodes\Model\DataPincodes","I95Dev\Serviceablepincodes\Model\ResourceModel\DataPincodes");
	}
}
 ?>