<?php
namespace I95Dev\SideEffects\Controller\Adminhtml\Sideeffects;
use Magento\Backend\App\Action;
class NewAction extends \Magento\Backend\App\Action
{
     public function execute()
    {
		$this->_forward('edit');
    }
}
