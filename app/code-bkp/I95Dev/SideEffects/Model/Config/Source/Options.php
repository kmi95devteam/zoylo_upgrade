<?php
namespace I95Dev\SideEffects\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;
use I95Dev\SideEffects\Model\ResourceModel\Sideeffects\CollectionFactory;
/**

* Custom Attribute Renderer

*/

class Options extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

	/**
    * @var OptionFactory
    */
    public $optionFactory;

    /**
     * @var sideeffectsCollectionFactory
     */
    public $sideeffectsCollectionFactory;

    public function __construct(
        OptionFactory $optionFactory,
        CollectionFactory $sideeffectsCollectionFactory
    ) {
        $this->optionFactory = $optionFactory;
        $this->sideeffectsCollectionFactory = $sideeffectsCollectionFactory;
    }

	/**
	* Get all options
	*
	* @return array
	*/
	public function getAllOptions()

	{

        $this->_options = [];
        $datas = $this->getSideeffectsCollection();

        foreach ($datas as $data) {
            $this->_options[] = [
                'label' => trim($data->getName()),
                'value' => trim($data->getName()),
            ];
        }

        return $this->_options;

	}
	
	
    public function getSideeffectsCollection()
    {
        /** @var $collection \I95Dev\SideEffects\Model\ResourceModel\Sideeffects\CollectionFactory */
        $collection = $this->sideeffectsCollectionFactory->create();
        return $collection;
    }
}

