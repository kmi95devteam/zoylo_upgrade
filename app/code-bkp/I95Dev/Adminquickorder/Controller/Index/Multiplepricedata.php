<?php
namespace I95Dev\Adminquickorder\Controller\Index;

use Magento\Framework\App\Action\Action;

class Multiplepricedata extends Action {
 /**
 * @var \Magento\Framework\Controller\Result\JsonFactory
 */
 protected $resultJsonFactory;
 /**
 * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
 */
 protected $_productCollectionFactory;

    public function __construct(
    \Magento\Framework\App\Action\Context $context,
    \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,       
    \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory       
   
)
{    
    $this->resultJsonFactory = $resultJsonFactory;    
    $this->_productCollectionFactory = $productCollectionFactory;    
    parent::__construct($context);
}
public function execute()
{
    $price = '';
    $result = $this->resultJsonFactory->create();
    $skuname = $this->getRequest()->getParam('skuname'); 
    $product = $this->_productCollectionFactory->create()->addAttributeToSelect('*');
    $productPriceBySku = $product->addFieldToFilter('name', $skuname);
    $priceData = $productPriceBySku->getData();
    if(!empty($priceData)){
        foreach($productPriceBySku as $data){
            $price = $data->getPrice();    
        }
        
    }
     return $result->setData(['multipleprice' => $price]);
}
}