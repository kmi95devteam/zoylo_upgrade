<?php
namespace I95Dev\Adminquickorder\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Contact\Model\ConfigInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\ObjectManager;

class Createorder extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
	protected $_storeManager;
	protected $prescription;
	protected $_messageManager;
	protected $logger;
	
	public function __construct(\Psr\Log\LoggerInterface $logger,
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $product,
        \Magento\Framework\Data\Form\FormKey $formkey,
        \Magento\Quote\Model\QuoteFactory $quote,
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Sales\Model\Service\OrderService $orderService,
		\Magento\Customer\Model\AddressFactory $addressFactory,
		\Magento\Framework\Message\ManagerInterface $messageManager,
		\I95Dev\UploadPrescription\Model\PrescriptionFactory $prescription
                )
	{
		$this->logger = $logger;
		$this->_pageFactory = $pageFactory;
        $this->_storeManager = $storeManager;
        $this->_product = $product;
        $this->_formkey = $formkey;
        $this->quote = $quote;
        $this->quoteManagement = $quoteManagement;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->orderService = $orderService;
        $this->_addressFactory = $addressFactory;
        $this->_prescription = $prescription;
        $this->_messageManager = $messageManager;
		return parent::__construct($context);
	}

	public function execute()
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resultpagefactory =  $this->_pageFactory->create();
        $post = $this->getRequest()->getPostValue();
         //echo '<pre>'; print_r($post);print_r($_FILES);die;
		$mobile = $post['mobile'];
		$mobile = "+91".$mobile;
		
		$quote = $this->quote->create();
		
		$customerObj = $objectManager->create('Magento\Customer\Model\ResourceModel\Customer\Collection');
		$collection  = $customerObj->addAttributeToSelect('*')
								  ->addAttributeToFilter('mobile_number',$mobile)
								  ->load();
								 
		if(count($collection)>0){  
				$cus_data = $collection->getData();
				$email = $cus_data[0]['email']; 
				$customer_id = $cus_data[0]['entity_id']; 
				
				if (isset($_FILES)) {
						 $total = count($_FILES['pic']['name']);
						 if($total > 1){
							for($i=0;$i<$total;$i++) {
						
									$fileName = $_FILES["pic"]["name"][$i];
									$fileName = str_replace(' ', '-', $fileName); 
									
									$fileId = "pic[$i]";	
									if (isset($customer_id) && ($customer_id != 0)) {
										$baseTmpPath = "prescriptions/$customer_id";
									} else {
										$baseTmpPath = "prescriptions";
									}
									$uploader = $objectManager->create('\Magento\MediaStorage\Model\File\UploaderFactory')->create(['fileId' => $fileId]);
									$uploader->setAllowCreateFolders(true);
									$uploader->setAllowRenameFiles(true);
									$uploader->setFilesDispersion(false);
									$result = $uploader->save($objectManager->get('\Magento\Framework\Filesystem')->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath($baseTmpPath),$fileName);
									 $prescription_id[] = $this->saveFileToDb($customer_id, $fileName);

							}
						
							$prescription_ids = implode(',',  $prescription_id);
						
						 }	
					}
			}else{
				$customer_Id = $this->createNewCustomer($post);
				$email = $_POST['emailnew'];
			}
        $store=$this->_storeManager->getStore();
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        $customer=$this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        $customer->loadByEmail($email);

        $quote->setStore($store); 
        $customer= $this->customerRepository->getById($customer->getEntityId());
        $quote->setCurrency();
        $quote->assignCustomer($customer);
		
		// Add Product
		$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
		if (isset($_POST['sku'])) {
			$i= 0;		
			   foreach($_POST['sku'] as $itemname){
					$collection = $productCollection->create()
													->addAttributeToSelect('*')
													->addAttributeToFilter('name',$itemname)
													->load();
						foreach ($collection as $product){
							 $qty = $_POST['qty'][$i];
							 $prod_id  = $product->getEntityId();
							 $product = $objectManager->create('Magento\Catalog\Model\Product')->load($prod_id);
							 $quote->addProduct(
									$product,
									intval($qty)
								); 
							
						}
					$i++;	
			   }
		}
		
      
		$billingAddressId = $customer->getDefaultBilling();
		$shippingAddressId = $customer->getDefaultShipping();
		$billingAddress = $this->_addressFactory->create()->load($billingAddressId);
		$shippingAddress = $this->_addressFactory->create()->load($shippingAddressId);
        $quote->getBillingAddress()->addData((array)$billingAddress);
        $quote->getShippingAddress()->addData((array)$shippingAddress);
        $shippingAddress=$quote->getShippingAddress();
        $shippingAddress->setCollectShippingRates(true)
                        ->collectShippingRates()
                        ->setShippingMethod('ecomexpress_ecomexpress');
        $quote->setPaymentMethod('cashondelivery');
		if(isset($prescription_ids)){
			$quote->setPrescriptionValues($prescription_ids);	
		}
			$quote->setInventoryProcessed(false); 
			$quote->save(); 
			$quote->getPayment()->importData(['method' => 'cashondelivery']);
			$quote->collectTotals()->save();
		try{
			$order = $this->quoteManagement->submit($quote);
			$increment_id = $order->getRealOrderId();
			if($increment_id){
				$result['order_id']= $order->getRealOrderId();
			}else{
				$result=['error'=>1,'msg'=>'Error While Saving the order'];
			}
		}catch(\Exception $e) {
			$this->logger->info($e->getMessage());
		}
		
		
        $order->setEmailSent(0);
        
        $this->_messageManager->addSuccess(__('Your order has been saved with order Id :'.$increment_id));
		$resultRedirect = $this->resultRedirectFactory->create();
        $url = $this->_redirect->getRefererUrl();
        $resultRedirect->setUrl($url);
        return $resultRedirect;

	}

	public function saveFileToDb($customerId, $fileName){
		try{
			$mediaUrl = $this->_storeManager->getStore()
								->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
			$baseTmpPath = "prescriptions/$customerId/";
			$filepullpath = $mediaUrl.$baseTmpPath.$fileName;
			$prescriptionmodel = $this->_prescription->create();
			$prescriptionmodel->setCustomerId($customerId);
			$prescriptionmodel->setPrescriptionName($fileName);
			$prescriptionmodel->setLabel($fileName);
			$prescriptionmodel->setValue($filepullpath);
			$prescriptionmodel->setCreatedDate(date('Y-m-d H:i:s', time()));
			$prescriptionmodel->save();
			$prescription_id = $prescriptionmodel->getPrescriptionId();
				
		} catch(\Exception $e) {
			$this->logger->info($e->getMessage());
		}
								
		return $prescription_id;		
	}
	
	
	public function createNewCustomer($post){
		try{
	
			$websiteId = $this->_storeManager->getStore()->getWebsiteId();
			$customer = $this->customerFactory->create();
			$customer->setWebsiteId($websiteId);
			$customer->setEmail($_POST['emailnew']);
			$customer->setFirstname($_POST['name']);
			$customer->setLastname($_POST['name']);
			$customer->setMobileNumber($_POST['mobile']);
			$customer->setPassword("123456789");
			$customer->save();
			$customer_id =$customer->getId();

			$address = $this->_addressFactory->create();
			$address->setCustomerId($customer_id)
					->setFirstname($_POST['name'])
					->setLastname($_POST['name'])
					->setCountryId("IN")
					->setRegionId($_POST['region'])
					->setPostcode($_POST['pincode'])
					->setCity($_POST['city'])
					->setTelephone($_POST['mobile'])
					->setStreet("Street")
					->setIsDefaultBilling('1')
					->setIsDefaultShipping('1')
					->setSaveInAddressBook('1');
			$address->save();
			return $customer_id;
			
		} catch(\Exception $e) {
			$this->logger->info($e->getMessage());
		}
	}
	
	




	
	
}