<?php

namespace I95Dev\Adminquickorder\Controller\Index;

class ajaxcall extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    public function __construct(
    \Magento\Framework\App\Action\Context $context, 
            \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute() {
        $post = $this->getRequest()->getPostValue();
        $coll = $this->getProductCollection($post['sku']);
        echo json_encode($coll);
        //return $this->_pageFactory->create();
    }

    public function getProductCollection($skutext) {
$_objectManager = \Magento\Framework\App\ObjectManager::getInstance();

$productCollection = $_objectManager->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');

if($skutext != null){
    $sku = array();
$collection = $productCollection->create();

        $collection ->addAttributeToSelect('name');

        $collection ->addAttributeToFilter('name', array('like' => '%'.$skutext.'%'));
        foreach($collection as $key=>$product){
            
            $sku = $product->getSku();
            if($sku != null){
                
            ?>
<ul id='myid'>  
    <li class="searchlist" id="<?php echo $key; ?>" value="<?php echo $sku; ?>"><a href="#"><?php echo $sku; ?></a></li>
</ul>  
       <?php
       }
        }
        if(empty($sku)){return 'no results found';}
        
}else{
   return 'no Key word found';
}
    }

}
