var config = {
    map: {
        '*': {
            minicart_open: 'I95Dev_MinicartOpen/js/view/minicartopen'
        }
    },
    shim: {
        minicart_open: {
            deps: [
                'jquery'
            ]
        }
    }
};