define([
    'jquery',
    'Magento_Checkout/js/action/get-totals',
    'Magento_Customer/js/customer-data',
	'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-rate-processor/new-address',
    'Magento_Checkout/js/model/shipping-rate-processor/customer-address',
	 'Magento_Checkout/js/model/new-customer-address',
    'Magento_Checkout/js/model/shipping-rate-registry',
	'Magento_Checkout/js/model/url-builder',
    'mage/url'
	
	
], function ($, getTotalsAction, customerData, quote, defaultProcessor, customerAddressProcessor, newCustomerAddress, rateRegistry, urlBuilder, urlFormatter) {

    $(document).ready(function(){
	    var urdirect = $('#coupon_code').val();
							if (urdirect != ''){
								$("#couponcode_data").hide();
								$("#couponcode_cancel").show();
							}else{
							$("#couponcode_data").show();	
							}
		$("#couponcode_data").click(function(){
if ($(window).width() <= 1024) {
	//alert('hi');
$("#couponcode_data").hide();
}			
						$("#coupon-sucesss-msg").hide();$("#coupon-invalid-msg").hide();
						var couponValue = $("input[name='coupon_code']").val();				
						if(couponValue){
						var url = urlFormatter.build('ajaxshoppingcartpage/cart/couponpost');
							$.ajax({
							url: url,
							type: "POST",
							data: {coupon_code:couponValue},
							showLoader: true,
							cache: false,
							dataType: 'json',
							success: function(response){
								//console.log(response.output);
								if(response.success == 'true'){
									$("#coupon-sucesss-msg").show();
									$("#coupon-sucesss-msg").html(response.coupon_message_success);
									$("#shopping-cart-table").load(" #shopping-cart-table");
									$("#updatediv").load(" #updatediv");
									$("#couponcode_cancel").show();
									
									 setTimeout(function(){
                                    $("#coupon-sucesss-msg").hide();
                                },5000);
								
							
									
									var sections = ['cart'];
									// The mini cart reloading
									customerData.reload(sections, true);

									// The totals summary block reloading
									var deferred = $.Deferred();
									getTotalsAction([], deferred);
									
									//reload shipping-rate-registry
									  var processors = [];
									var customerdata = quote.shippingAddress();
                                  if(customerdata != null){
								   rateRegistry.set(quote.shippingAddress().getCacheKey(), null);

								   processors.default =  defaultProcessor;
								   processors['customer-address'] = customerAddressProcessor;

								   var type = quote.shippingAddress().getType();

								   if (processors[type]) {
									  processors[type].getRates(quote.shippingAddress());
								   } else {
									  processors.default.getRates(quote.shippingAddress());
								   }
								
								}
								}
								if(response.success == 'false'){
									$("#coupon-invalid-msg").show();									
									$("#coupon-invalid-msg").html(response.coupon_message_invalid);
								setTimeout(function(){
                                    $("#coupon-invalid-msg").hide();
                                },5000);
								}
								
							} //Success close
						});
					}
				});
				      	  	
				
                        $("#couponcode_cancel").click(function(){
					    $("#couponcode_cancel").hide();
						$("#couponcode_data").show();	
						$("#coupon-cancel-msg").hide();	
						$('#remove-coupon').val(1);
						var removeValue = $('#remove-coupon').val();
						if(removeValue){
							var url = urlFormatter.build('ajaxshoppingcartpage/cart/couponpost');
							$.ajax({
							url: url,
							type: "POST",
							data: {remove:removeValue},
							showLoader: true,
							cache: false,
							dataType: 'json',
							success: function(response){
								if(response.success == 'false'){
									//alert('Hello world');
									$("#coupon-cancel-msg").show();
									$("#coupon-cancel-msg").html(response.coupon_message_cancel);
                                    $("#shopping-cart-table").load(" #shopping-cart-table");
									$("#updatediv").load(" #updatediv");
                                    $("input[name='coupon_code']").val('');	
									 
								setTimeout(function(){
                                    $("#coupon-cancel-msg").hide();
                                },5000);
									var sections = ['cart'];
									// The mini cart reloading
									customerData.reload(sections, true);

									// The totals summary block reloading
									var deferred = $.Deferred();
									getTotalsAction([], deferred);
									
									//reload shipping-rate-registry
									  var processors = [];
									var customerdata = quote.shippingAddress();
                                   if(customerdata != null){
								   rateRegistry.set(quote.shippingAddress().getCacheKey(), null);

								   processors.default =  defaultProcessor;
								   processors['customer-address'] = customerAddressProcessor;

								   var type = quote.shippingAddress().getType();

								   if (processors[type]) {
									  processors[type].getRates(quote.shippingAddress());
								   } else {
									  processors.default.getRates(quote.shippingAddress());
								   }
								   }
								}
								}
								
							//} //Success close
						});
					}
				});
		
		
    });
});