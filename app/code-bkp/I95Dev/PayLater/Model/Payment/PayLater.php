<?php


namespace I95Dev\PayLater\Model\Payment;

class PayLater extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = "paylater";
    protected $_isOffline = true;

    public function isAvailable(
        \Magento\Quote\Api\Data\CartInterface $quote = null
    ) {
        return parent::isAvailable($quote);
    }
}
