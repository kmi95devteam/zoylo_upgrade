define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'paylater',
                component: 'I95Dev_PayLater/js/view/payment/method-renderer/paylater-method'
            }
        );
        return Component.extend({});
    }
);