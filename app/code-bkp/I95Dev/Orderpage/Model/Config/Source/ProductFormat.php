<?php

namespace I95Dev\Orderpage\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

		/**
		* Custom Attribute Renderer
		*/

class ProductFormat extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

		/**
		* @var OptionFactory
		*/

		protected $optionFactory;

		/**
		* @param OptionFactory $optionFactory
		*/
		/**
		* Get all options
		*
		* @return array
		*/

		public function getAllOptions()

		{
			/* your Attribute options list*/

			$this->_options=[ ['label'=>'', 'value'=>''],
			['label'=>'Tablet', 'value'=>'Tablet'],
			['label'=>'Capsule', 'value'=>'Capsule'],
			['label'=>'Powder', 'value'=>'Powder'],
			['label'=>'Gel', 'value'=>'Gel'],
			['label'=>'Syrup', 'value'=>'Syrup'],
			['label'=>'Injection', 'value'=>'Injection'],
			['label'=>'Ointment', 'value'=>'Ointment'],
			['label'=>'Patches', 'value'=>'Patches']
			];

			return $this->_options;

		}

}