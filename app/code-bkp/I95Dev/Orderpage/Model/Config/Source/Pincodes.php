<?php

namespace I95Dev\Orderpage\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

		/**
		* Custom Attribute Renderer
		*/

class Pincodes extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

		/**
		* @var OptionFactory
		*/

		protected $optionFactory;

		/**
		* @param OptionFactory $optionFactory
		*/
		/**
		* Get all options
		*
		* @return array
		*/

		public function getAllOptions()

		{
			/* your Attribute options list*/

			$this->_options=[ ['label'=>'', 'value'=>''],	
			['label'=>'North', 'value'=>'north'],
			['label'=>'South', 'value'=>'south'],
			['label'=>'East', 'value'=>'east'],
			['label'=>'West', 'value'=>'west']
			];

			return $this->_options;

		}

}