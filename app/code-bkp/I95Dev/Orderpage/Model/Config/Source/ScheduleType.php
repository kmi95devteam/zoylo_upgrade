<?php

namespace I95Dev\Orderpage\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

		/**
		* Custom Attribute Renderer
		*/

class ScheduleType extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

		/**
		* @var OptionFactory
		*/

		protected $optionFactory;

		/**
		* @param OptionFactory $optionFactory
		*/
		/**
		* Get all options
		*
		* @return array
		*/

		public function getAllOptions()

		{
			/* your Attribute options list*/

			$this->_options=[ ['label'=>'', 'value'=>''],
			['label'=>'G', 'value'=>'G'],
			['label'=>'H', 'value'=>'H'],
			['label'=>'X', 'value'=>'X'],
			['label'=>'J', 'value'=>'J'],
			['label'=>'W', 'value'=>'W']
			];

			return $this->_options;

		}

}