<?php

namespace I95Dev\Orderpage\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

		/**
		* Custom Attribute Renderer
		*/

class ScheduleCategory extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

		/**
		* @var OptionFactory
		*/

		protected $optionFactory;

		/**
		* @param OptionFactory $optionFactory
		*/
		/**
		* Get all options
		*
		* @return array
		*/

		public function getAllOptions()

		{
			/* your Attribute options list*/

			$this->_options=[ ['label'=>'', 'value'=>''],
			['label'=>'Dangerous', 'value'=>'Dangerous'],
			['label'=>'Potentially Dangerous', 'value'=>'Potentially Dangerous'],
			['label'=>'Mildly Unsafe', 'value'=>'Mildly Unsafe'],
			['label'=>'Safe', 'value'=>'Safe']
			];

			return $this->_options;

		}

}