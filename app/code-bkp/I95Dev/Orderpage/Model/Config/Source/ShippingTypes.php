<?php

namespace I95Dev\Orderpage\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

		/**
		* Custom Attribute Renderer
		*/

class ShippingTypes extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

		/**
		* @var OptionFactory
		*/

		protected $optionFactory;

		/**
		* @param OptionFactory $optionFactory
		*/
		/**
		* Get all options
		*
		* @return array
		*/

		public function getAllOptions()

		{
			/* your Attribute options list*/

			$this->_options=[ ['label'=>'', 'value'=>''],
			['label'=>'Express', 'value'=>'express'],
			['label'=>'Regular', 'value'=>'regular'],
			['label'=>'Out of station', 'value'=>'out_of_station'],
			//['label'=>'Not for Sale', 'value'=>'not_for_sale']
			];

			return $this->_options;

		}

}