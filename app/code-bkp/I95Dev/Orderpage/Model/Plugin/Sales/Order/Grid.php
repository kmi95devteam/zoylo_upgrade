<?php
namespace I95Dev\Orderpage\Model\Plugin\Sales\Order;
class Grid
{

    public static $table = 'sales_order_grid';
    public static $leftJoinTable = 'sales_order_address';
    public static $leftJoinTableNotes = 'sales_order_status_history';
    public function afterSearch($intercepter, $collection)
    {
        if ($collection->getMainTable() === $collection->getConnection()->getTableName(self::$table)) {
            $leftJoinTableName = $collection->getConnection()->getTableName(self::$leftJoinTable);
            $leftJoinTableNotesName = $collection->getConnection()->getTableName(self::$leftJoinTableNotes);
            
            $collection->getSelect()->joinLeft(['sod1'=>$leftJoinTableName],"sod1.parent_id = main_table.entity_id AND sod1.address_type = 'shipping'", ['sod1.telephone','sod1.postcode','sod1.region']);
            
            $collection->getSelect()->joinLeft(['sosh'=>$leftJoinTableNotesName],"sosh.parent_id = main_table.entity_id", ['comment' => 'GROUP_CONCAT(sosh.comment)']);
            $collection->getSelect()->group("main_table.entity_id");

            $where = $collection->getSelect()->getPart(\Magento\Framework\DB\Select::WHERE);
            $searchword1 = 'status';
            $searchword2 = 'created_at';
            $matches = array();
            foreach($where as $k=>$v) {
                if(preg_match("/\b$searchword1\b/i", $v)) {
                    $matches[$k] = str_replace("`status`","`main_table`.`status`",$v);
                }elseif(preg_match("/\b$searchword2\b/i", $v)) {
                    $matches[$k] = str_replace("`created_at`","`main_table`.`created_at`",$v);
                }else{
                    $matches[$k] = $v;
                }
            }
            $collection->getSelect()->setPart(\Magento\Framework\DB\Select::WHERE, $matches);
            //echo $collection->getSelect()->__toString();die;
        }
        return $collection;
    }
}