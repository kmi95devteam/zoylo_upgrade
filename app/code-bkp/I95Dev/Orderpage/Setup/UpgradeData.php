<?php

namespace I95Dev\Orderpage\Setup;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

class UpgradeData implements UpgradeDataInterface {

    private $eavSetupFactory;
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        if (version_compare($context->getVersion(), '2.0.0', '=')) {		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'quantity_limit');

		
			$eavSetup->addAttribute(\Magento\Catalog\Model\Product :: ENTITY, 'quantity_limit', [
                        'type' => 'int',
						'group' => 'Zoylo Custom Attributes',
                        'label' => 'Quantity Limit',
                        'input' => 'text',
						'required' => false,
                        'sort_order' => 110,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
						"default" => "",
						"class"    => "",
						"note"       => ""
			]
			);
					
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'pincode_zone');

		
			$eavSetup->addAttribute(\Magento\Catalog\Model\Product :: ENTITY, 'pincode_zone', [
                        'type' => 'text',
                        'label' => 'Pincode Zone',
						'group' => 'Zoylo Custom Attributes',
                        'input' => 'multiselect',
						'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                        'source' => 'I95Dev\Orderpage\Model\Config\Source\Pincodes',
						'required' => false,
                        'sort_order' => 120,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
						"default" => "",
						"class"    => "",
						"note"       => ""
			]
			);
					
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'shipping_type');

		
			$eavSetup->addAttribute(\Magento\Catalog\Model\Product :: ENTITY, 'shipping_type', [
                        'type' => 'text',
                        'label' => 'Shipping Type',
						'group' => 'Zoylo Custom Attributes',
                        'input' => 'multiselect',
						'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                        'source' => 'I95Dev\Orderpage\Model\Config\Source\ShippingTypes',
						'required' => false,
                        'sort_order' => 130,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
						"default" => "",
						"class"    => "",
						"note"       => ""
			]
			);
					
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'returnable');

		
			$eavSetup->addAttribute(\Magento\Catalog\Model\Product :: ENTITY, 'returnable', [
                        'type' => 'int',
                        'label' => 'Returnable',
						'group' => 'Zoylo Custom Attributes',
                        'input' => 'select',
						'required' => false,
                        'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                        'sort_order' => 140,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
						"default" => "",
						"class"    => "",
						"note"       => ""
			]
			);
		}
    }

}
