<?php
namespace I95Dev\S3\Block\System\Config\Bucket;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Validation extends \Magento\Config\Block\System\Config\Form\Field {

    /**
     * @var string
     */
    protected $_template = 'I95Dev_S3::system/config/bucket/validation.phtml';

    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(
    Context $context, array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * Render button
     *
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element) {
        // Remove scope label
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * Return element html
     *
     * @param  AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element) {
        return $this->_toHtml();
    }

    /**
     * Return ajax url for collect button
     *
     * @return string
     */
    public function getAjaxUrl() {
        return $this->getUrl('i95devs3/system_config/s3validation');
    }

    /**
     * Generate collect button html
     *
     * @return string
     */
    public function getButtonHtml() {
        $button = $this->getLayout()->createBlock(
                        'Magento\Backend\Block\Widget\Button'
                )->setData(
                [
                    'id' => 'validation',
                    'label' => __('Check'),
                ]
        );

        return $button->toHtml();
    }

}

?>
