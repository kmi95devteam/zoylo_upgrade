<?php
namespace I95Dev\S3\Controller\Adminhtml\System\Config;
include_once 'vendor/Aws/aws-autoloader.php';
//$rootPath = str_replace('/medicines//pub','',$_SERVER["DOCUMENT_ROOT"]);
//if($rootPath == ''){
//    include_once $rootPath.'/medicines/vendor/Aws/aws-autoloader.php';
//} else {
//    include_once $_SERVER["DOCUMENT_ROOT"].'/medicines/vendor/Aws/aws-autoloader.php';
//}
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class S3Validation extends \Magento\Framework\App\Action\Action {

    /**
     * @var PageFactory
     */
    protected $resultJsonFactory;
    protected $messageManager;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
    Context $context
    , JsonFactory $resultJsonFactory
    , \Magento\Framework\Message\ManagerInterface $messageManager
	, \I95Dev\S3\Helper\Data $helper
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->messageManager = $messageManager;
		$this->helper = $helper;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute() {

        try {
		
			$this->client = new \Aws\S3\S3Client([
                'version' => 'latest',
                'region' => $this->helper->getRegion(),
                'credentials' => [
                    'key' => $this->helper->getAccessKey(),
                    'secret' => $this->helper->getSecretKey()
                ]
            ]);
			$bucketName = $this->helper->getBucket();
			$doesBucketExist = $this->client->doesBucketExist($bucketName);

            if ($doesBucketExist != "") {
                echo json_encode(['status' => "success", 'token' => $doesBucketExist]);
            } else {
                echo json_encode(['status' => "invalid", 'token' => $doesBucketExist]);
            }
        } catch (Exception $ex) {
            $this->messageManager->addError("Invalid Data" . $ex);
        }
    }

}
