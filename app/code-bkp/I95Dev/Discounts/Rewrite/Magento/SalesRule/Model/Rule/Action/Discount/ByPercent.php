<?php

namespace I95Dev\Discounts\Rewrite\Magento\SalesRule\Model\Rule\Action\Discount;

class ByPercent extends \Magento\SalesRule\Model\Rule\Action\Discount\ByPercent {

    /**
     * @param \Magento\SalesRule\Model\Rule $rule
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $item
     * @param float $qty
     * @param float $rulePercent
     * @return Data
     */
    protected function _calculate($rule, $item, $qty, $rulePercent) {
        /** @var \Magento\SalesRule\Model\Rule\Action\Discount\Data $discountData */
        $discountData = $this->discountFactory->create();

        $itemPrice = $this->validator->getItemPrice($item);
        $baseItemPrice = $this->validator->getItemBasePrice($item);
        $itemOriginalPrice = $this->validator->getItemOriginalPrice($item);
        $baseItemOriginalPrice = $this->validator->getItemBaseOriginalPrice($item);
        $productId = $item->getProductId();
        $_rulePct = $rulePercent / 100;

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
        $applyRules = $product->getApplyCatalogRules();

        /* If catalog rule is applied */
        if (isset($applyRules) & $applyRules == 1) {


            $percent = (1 - ($itemPrice / $itemOriginalPrice)) * 100;

            if ($percent < $rulePercent) {

                $newPercent = $_rulePct;
                $discountData->setAmount(($qty * $itemOriginalPrice - $item->getDiscountAmount()) * $newPercent);
                $discountData->setBaseAmount(($qty * $baseItemOriginalPrice - $item->getBaseDiscountAmount()) * $newPercent);
                $discountData->setOriginalAmount(($qty * $itemOriginalPrice - $item->getDiscountAmount()) * $newPercent);
                $discountData->setBaseOriginalAmount(
                        ($qty * $baseItemOriginalPrice - $item->getBaseDiscountAmount()) * $_rulePct
                );
                if (!$rule->getDiscountQty() || $rule->getDiscountQty() > $qty) {
                    $discountPercent = min(100, $item->getDiscountPercent() + $newPercent * 100);
                    $item->setDiscountPercent($discountPercent);
                }
            }else {
                $newPercent = $percent / 100;;
                $discountData->setAmount(($qty * $itemOriginalPrice - $item->getDiscountAmount()) * $newPercent);
                $discountData->setBaseAmount(($qty * $baseItemOriginalPrice - $item->getBaseDiscountAmount()) * $newPercent);
                $discountData->setOriginalAmount(($qty * $itemOriginalPrice - $item->getDiscountAmount()) * $newPercent);
                $discountData->setBaseOriginalAmount(
                        ($qty * $baseItemOriginalPrice - $item->getBaseDiscountAmount()) * $_rulePct
                );
                if (!$rule->getDiscountQty() || $rule->getDiscountQty() > $qty) {
                    $discountPercent = min(100, $item->getDiscountPercent() + $newPercent * 100);
                    $item->setDiscountPercent($discountPercent);
                }
            }
        } else {

            if (isset($applyRules) & $applyRules == 0) {
                $percent = $product->getPercentage();
                if(isset($percent)&& $percent!=''){
				$percent = $percent / 100;
				}else{
					$percent = $_rulePct;
					}
                
                $discountData->setAmount(($qty * $itemOriginalPrice - $item->getDiscountAmount()) * $percent);
                $discountData->setBaseAmount(($qty * $baseItemOriginalPrice - $item->getBaseDiscountAmount()) * $percent);
                $discountData->setOriginalAmount(($qty * $itemOriginalPrice - $item->getDiscountAmount()) * $percent);
                $discountData->setBaseOriginalAmount(($qty * $baseItemOriginalPrice - $item->getBaseDiscountAmount()) * $percent);
                if (!$rule->getDiscountQty() || $rule->getDiscountQty() > $qty) {
                    $discountPercent =  $percent;
                    $item->setDiscountPercent($discountPercent);
                }
                
            }else {
                $newPercent = $_rulePct;
                $discountData->setAmount(($qty * $itemOriginalPrice - $item->getDiscountAmount()) * $newPercent);
                $discountData->setBaseAmount(($qty * $baseItemOriginalPrice - $item->getBaseDiscountAmount()) * $newPercent);
                $discountData->setOriginalAmount(($qty * $itemOriginalPrice - $item->getDiscountAmount()) * $newPercent);
                $discountData->setBaseOriginalAmount(
                        ($qty * $baseItemOriginalPrice - $item->getBaseDiscountAmount()) * $newPercent
                );
                if (!$rule->getDiscountQty() || $rule->getDiscountQty() > $qty) {
                    $discountPercent = min(100, $item->getDiscountPercent() + $newPercent * 100);
                    $item->setDiscountPercent($discountPercent);
                }
            }
        }
        return $discountData;
    }

}
