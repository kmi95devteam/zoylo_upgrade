/* var config = {
    paths: {
        jquery.infinite-scroller: 'Smartwave_Porto/js/view/jquery.infinite-scroller',
		jquery.lazyload: 'Smartwave_Porto/js/view/jquery.lazyload'
    }
}; */
var config = {
    map: {
        '*': {
           'jqueryinfinitescroller': 'Smartwave_Porto/js/jquery.infinite-scroller',
		   'jquerylazyload': 'js/jquery.lazyload'
        }
    }
};