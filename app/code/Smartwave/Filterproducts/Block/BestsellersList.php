<?php

namespace Smartwave\Filterproducts\Block;

class BestsellersList extends \Magento\Catalog\Block\Product\AbstractProduct {

    protected $_catalogProductVisibility;

    protected $_productCollectionFactory;

    protected $_categoryFactory;

    protected $urlHelper;

    protected $_resource;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        \Magento\Framework\App\ResourceConnection $resource,
        array $data = []
    ) {
        $this->_categoryFactory = $categoryFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_catalogProductVisibility = $catalogProductVisibility;
        $this->urlHelper = $urlHelper;
        $this->_resource = $resource;

        parent::__construct($context, $data);
    }

    public function getProducts() {
        
      $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
   $productCollectionFactory = $objectManager->create('\Magento\Catalog\Model\ResourceModel\Product\Collection');
   $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
   $storeId = $storeManager->getStore()->getId();
   $collection = $productCollectionFactory->addAttributeToSelect('*');
   $collection->getSelect()->joinLeft( 
                           'sales_order_item', 
                           'e.entity_id = sales_order_item.product_id', 
                           array('qty_ordered'=>'SUM(sales_order_item.qty_ordered)')) 
                           ->group('e.entity_id')->limit(8);

        //$collection->printlogquery(true);die;
        return $collection;
    }

    public function getAddToCartPostParams(\Magento\Catalog\Model\Product $product)
    {
        $url = $this->getAddToCartUrl($product);
        return [
            'action' => $url,
            'data' => [
                'product' => $product->getEntityId(),
                \Magento\Framework\App\Action\Action::PARAM_NAME_URL_ENCODED =>
                    $this->urlHelper->getEncodedUrl($url),
            ]
        ];
    }

    public function getLoadedProductCollection() {
        return $this->getProducts();
    }

    public function getProductCount() {
        $limit = $this->getData("product_count");
        if(!$limit)
            $limit = 10;
        return $limit;
    }
}
