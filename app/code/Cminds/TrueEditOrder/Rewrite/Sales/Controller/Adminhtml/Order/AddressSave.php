<?php
namespace Cminds\TrueEditOrder\Rewrite\Sales\Controller\Adminhtml\Order;

use Cminds\TrueEditOrder\Model\Config;
use Magento\Backend\App\Action\Context;
use Magento\Directory\Model\RegionFactory;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\Data\OrderAddressInterface;
use Magento\Sales\Controller\Adminhtml\Order;
use Magento\Sales\Controller\Adminhtml\Order\AddressSave as AddressSaveParent;
use Magento\Sales\Model\Order\Address as AddressModel;
use Psr\Log\LoggerInterface;
use Magento\Framework\Registry;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Translate\InlineInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\View\Result\LayoutFactory;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\ObjectManager;

class AddressSave extends AddressSaveParent
{
    /**
     * @var RegionFactory
     */
    private $regionFactory;

    /**
     * @var Config
     */
    protected $moduleConfig;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param FileFactory $fileFactory
     * @param InlineInterface $translateInline
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $resultJsonFactory
     * @param LayoutFactory $resultLayoutFactory
     * @param RawFactory $resultRawFactory
     * @param OrderManagementInterface $orderManagement
     * @param OrderRepositoryInterface $orderRepository
     * @param LoggerInterface $logger
     * @param RegionFactory|null $regionFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        FileFactory $fileFactory,
        InlineInterface $translateInline,
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        LayoutFactory $resultLayoutFactory,
        RawFactory $resultRawFactory,
        OrderManagementInterface $orderManagement,
        OrderRepositoryInterface $orderRepository,
        LoggerInterface $logger,
        RegionFactory $regionFactory = null,
        Config $moduleConfig
    ) {
        $this->regionFactory = $regionFactory ?: ObjectManager::getInstance()->get(RegionFactory::class);
        $this->moduleConfig = $moduleConfig;
        parent::__construct(
            $context,
            $coreRegistry,
            $fileFactory,
            $translateInline,
            $resultPageFactory,
            $resultJsonFactory,
            $resultLayoutFactory,
            $resultRawFactory,
            $orderManagement,
            $orderRepository,
            $logger
        );
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pincodeValid = $telephoneValid = $pincodeNonServiceFlag = false;
        $addressId = $this->getRequest()->getParam('address_id');
        
        /** @var $address OrderAddressInterface|AddressModel */
        $address = $this->_objectManager->create(
            OrderAddressInterface::class
        )->load($addressId);
        $data = $this->getRequest()->getPostValue();
        
        if(isset($data['postcode']) && $data['postcode']!=''){
            $pincode = $data['postcode'];
            $pincodeValid = $this->validatePincode($pincode);
            
            if($pincodeValid==1){
                $chkPincodeValid = $this->chkPincode($pincode);
                if($chkPincodeValid==1){
                    $pincodeNonServiceFlag = true; //valid
                }else{
                    $pincodeNonServiceFlag = false; //pincode not exists in pincode shippig table
                }
            }


            //$pincodeValid = $this->validatePincode($pincode);
        }
        if(isset($data['telephone']) && $data['telephone']!=''){
            $telephone = $data['telephone'];
            $telephoneValid = $this->validateTelephone($telephone);
        }
        
                $data = $this->updateRegionData($data);
                $resultRedirect = $this->resultRedirectFactory->create();
                if ($data && $address->getId()) {
                    $address->addData($data);
                    try {
                        $address->save();
                        if($pincodeValid==1){
                            if($telephoneValid!=1){
                                $errorMessage = __('Please enter a valid Mobile Number.');
                            $this->messageManager->addError($errorMessage);
                            $resultRedirect = $this->resultRedirectFactory->create();
                            return $resultRedirect->setPath('sales/*/address', ['address_id' => $address->getId()]);
                            }
                            if($pincodeNonServiceFlag!=1){
                                $serviceableErrorMessage = __('This pincode is not serviceable.');
                                $this->messageManager->addError($serviceableErrorMessage);
                                $resultRedirect = $this->resultRedirectFactory->create();
                                return $resultRedirect->setPath('sales/*/address', ['address_id' => $address->getId()]);
                            }


                        }else{
                            $errorMessage = __('Please enter a valid Pincode.');
                            $this->messageManager->addError($errorMessage);
                            $resultRedirect = $this->resultRedirectFactory->create();
                            return $resultRedirect->setPath('sales/*/address', ['address_id' => $address->getId()]);
                        } 

                        $this->_eventManager->dispatch(
                            'admin_sales_order_address_update',
                            [
                                'order_id' => $address->getParentId()
                            ]
                        );
                        if ($this->moduleConfig->isOrderShippingMethodEditionEnabled()) {
                            $url = 'trueeditorder/order_shippingMethod/edit';
                            $message = __('You updated the order address. Please, choose shipping method.');
                        } else {
                            $url = 'sales/*/view';
                            $message = __('You updated the order address.');
                        }
                        $this->messageManager->addSuccess($message);
                        return $resultRedirect->setPath($url, ['order_id' => $address->getParentId()]);
                    } catch (LocalizedException $e) {
                        $this->messageManager->addError($e->getMessage());
                    } catch (\Exception $e) {
                        $this->messageManager->addException($e, __('We can\'t update the order address right now.'));
                    }
                    return $resultRedirect->setPath('sales/*/address', ['address_id' => $address->getId()]);
                } else {
                    return $resultRedirect->setPath('sales/*/');
                }
    }

    /**
     * {@inheritdoc}
     */
    private function updateRegionData($attributeValues)
    {
        if (!empty($attributeValues['region_id'])) {
            $newRegion = $this->regionFactory->create()->load($attributeValues['region_id']);
            $attributeValues['region_code'] = $newRegion->getCode();
            $attributeValues['region'] = $newRegion->getDefaultName();
        }
        return $attributeValues;
    }

    public function chkPincode($pincode){
        $objectmanager = \Magento\Framework\App\ObjectManager::getInstance();
        $model = $objectmanager->get('Ecom\Ecomexpress\Model\Pincode')->getCollection()->addFieldToFilter('pincode', array('eq' => $pincode));
        if(sizeof($model)>0){
            return true;
        }else{
            return false;
        }
    }

    public function validatePincode($pincode){
        if ( preg_match( '/^[1-9]{1}[0-9]+/', $pincode) && strlen($pincode)==6 ) {
            $errorFlag = true;
            /*$chkPincodeValid = $this->chkPincode($pincode);
            if($chkPincodeValid==1){
                $errorFlag = true; //valid
            }else{
                $errorFlag = false; //pincode not exists in pincode shippig table
            }*/
        } else {
            $errorFlag = false; //invalid
        }
        return $errorFlag;
    }
    public function validateTelephone($telephone){
        if ( preg_match( '/^[1-9]{1}[0-9]+/', $telephone) && strlen($telephone)==10 ) {
            $mobileerrorFlag = true; //valid
        } else {
            $mobileerrorFlag = false; //invalid
        }
        return $mobileerrorFlag;
    }
}
