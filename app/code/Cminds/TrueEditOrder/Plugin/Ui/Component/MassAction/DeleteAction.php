<?php

namespace Cminds\TrueEditOrder\Plugin\Ui\Component\MassAction;

use Magento\Ui\Component\MassAction;
use Cminds\TrueEditOrder\Model\Config as ModuleConfig;

/**
 * Class DeleteAction - remove delete action if config disabled
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 */
class DeleteAction
{
    /**
     * @var ModuleConfig
     */
    protected $config;

    /**
     * DeleteAction constructor.
     * @param ModuleConfig $moduleConfig
     */
    public function __construct(
        ModuleConfig $moduleConfig
    )
    {
        $this->config = $moduleConfig;        
    }

    /**
     * @param MassAction $object
     * @param $result
     * @return mixed
     */
    public function afterGetChildComponents(MassAction $object, $result)
    {
        if ( isset($result['cm_truedit_massdelete']) && ( 
            !$this->config->isActive() 
            || !$this->config->isOrderDeletionEnabled() 
            ) 
        ) {
            unset($result['cm_truedit_massdelete']);            
        }

        return $result;
    }
}