<?php
namespace Cminds\TrueEditOrder\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Pricing\Helper\Data;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class OrderEdit extends AbstractHelper
{
    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var int
     */
    protected $storeId;

    /**
     * @var Data
     */
    protected $priceHelper;

    /**
     * @param TransportBuilder $transportBuilder
     * @param StoreManagerInterface $storeManager
     * @param Data $priceHelper
     * @param Context $context
     */
    public function __construct(
        TransportBuilder $transportBuilder,
        StoreManagerInterface $storeManager,
        Data $priceHelper,
        Context $context
    ) {
        $this->transportBuilder = $transportBuilder;
        $this->storeManager = $storeManager;
        $this->priceHelper = $priceHelper;
        $this->storeId = $this->storeManager->getStore()->getId();
        parent::__construct($context);
    }

    /**
     * @param string $template
     * @param string $email
     * @param string $orderId
     * @param string $oldValue
     * @param string $newValue
     */
    public function sendRefundMessage($template, $email, $orderId, $oldValue, $newValue)
    {
        $emailData = [
            'oldValue' => $this->priceHelper->currency($oldValue, true, false),
            'newValue' => $this->priceHelper->currency($newValue, true, false),
            'orderId' => $orderId
        ];
        $senderInfo = [
            'name' => $this->scopeConfig->getValue(
                'trans_email/ident_support/name',
                ScopeInterface::SCOPE_STORE,
                $this->storeId

            ),
            'email' => $this->scopeConfig->getValue(
                'trans_email/ident_support/email',
                ScopeInterface::SCOPE_STORE,
                $this->storeId
            ),
        ];
        $this->transportBuilder->setTemplateIdentifier($template)
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => $this->storeManager->getStore()->getId(),
                ]
            )
            ->setTemplateVars($emailData)
            ->setFrom($senderInfo)
            ->addTo($email);
        $transport = $this->transportBuilder->getTransport();
        $transport->sendMessage();
    }
}