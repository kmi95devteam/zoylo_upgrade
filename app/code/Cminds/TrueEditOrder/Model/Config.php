<?php

namespace Cminds\TrueEditOrder\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Cminds TrueEditOrder config model.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class Config
{
    /**
     * Scope config object.
     *
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Store manager object.
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Store id.
     *
     * @var int
     */
    protected $storeId;

    /**
     * Already fetched config values.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Object initialization.
     *
     * @param ScopeConfigInterface $scopeConfig Scope config object.
     * @param StoreManagerInterface $storeManager Store manager object.
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;

        $this->storeId = $this->getStoreId();
    }

    /**
     * Return store id.
     *
     * @return int
     */
    protected function getStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }

    /**
     * Return config field value.
     *
     * @param string $fieldKey Field key.
     *
     * @return mixed
     */
    protected function getConfigValue($fieldKey)
    {
        if (isset($this->config[$fieldKey]) === false) {
            $this->config[$fieldKey] = $this->scopeConfig->getValue(
                'cminds_trueeditorder/' . $fieldKey,
                ScopeInterface::SCOPE_STORE,
                $this->storeId
            );
        }

        return $this->config[$fieldKey];
    }

    /**
     * Return bool value depends of that if module is active or not.
     *
     * @return bool
     */
    public function isActive()
    {
        return (bool)$this->getConfigValue('general/enable');
    }

    /**
     * Return bool value depends of that if order items edition feature
     * is active or not.
     *
     * @return bool
     */
    public function isOrderItemsEditionEnabled()
    {
        return (bool)$this->getConfigValue(
            'order_edit/allow_editing_order_items'
        );
    }

    /**
     * Return bool value depends of that if create order note after order
     * items edition feature is active or not.
     *
     * @return bool
     */
    public function isCreateOrderNoteAfterOrderItemsEditionEnabled()
    {
        return (bool)$this->getConfigValue(
            'order_edit/create_order_note_after_editing_order_items'
        );
    }

    /**
     * Return bool value depends of that if order shipping method edition
     * feature is active or not.
     *
     * @return bool
     */
    public function isOrderShippingMethodEditionEnabled()
    {
        return (bool)$this->getConfigValue(
            'order_edit/allow_editing_order_shipping_method'
        );
    }

    /**
     * Return bool value depends of that if order attribute edition
     * feature is active or not.
     *
     * @return bool
     */
    public function isOrderAttributeEditionEnabled()
    {
        return (bool)$this->getConfigValue(
            'order_edit/allow_editing_order_attributes'
        );
    }

    /**
     * @return int
     */
    public function paymentEditAllowed()
    {
        return $this->getConfigValue(
            'order_edit/allow_editing_order_payment_method'
        );
    }

    /**
     * Return bool value depends of that if create order note after
     * shipping method edition feature is active or not.
     *
     * @return bool
     */
    public function isCreateOrderNoteAfterShippingMethodEditionEnabled()
    {
        return (bool)$this->getConfigValue(
            'order_edit/create_order_note_after_shipping_method_update'
        );
    }

    /**
     * Return bool value depends of that if order notes edition feature
     * is active or not.
     *
     * @return bool
     */
    public function isOrderNotesEditionEnabled()
    {
        return (bool)$this->getConfigValue(
            'order_edit/allow_editing_order_notes'
        );
    }

    /**
     * Return bool value depends of that if order notes removing feature
     * is active or not.
     *
     * @return bool
     */
    public function isOrderNotesRemovingEnabled()
    {
        return (bool)$this->getConfigValue(
            'order_edit/allow_removing_order_notes'
        );
    }

    /**
     * Return array of allowed order statuses to modify.
     *
     * @return array|false
     */
    public function getAllowedOrderStatuses()
    {
        $data = $this->getConfigValue('order_edit/allowed_order_statuses');
        if ($data !== null) {
            return explode(',', $data);
        }

        return false;
    }

    /**
     * Return bool value depends of that if order status edition feature
     * is active or not.
     *
     * @return bool
     */
    public function isOrderStatusEditionEnabled()
    {
        return (bool)$this->getConfigValue(
            'order_edit/allow_editing_order_status'
        );
    }

    /**
     * Return bool value depends of that if create order note after
     * order status edition feature is active or not.
     *
     * @return bool
     */
    public function isCreateOrderNoteAfterOrderStatusEditionEnabled()
    {
        return (bool)$this->getConfigValue(
            'order_edit/create_order_note_after_order_status_update'
        );
    }

    /**
     * Check if admin can edit coupons.
     *
     * @return bool
     */
    public function orderCouponEditingEnabled()
    {
        return (bool)$this->getConfigValue(
            'order_edit/allow_editing_order_coupon');
    }


    /**
     * Check if the refund message should be sent.
     *
     * @return bool
     */
    public function needToSendRefundMessage()
    {
        return (bool)$this->getConfigValue(
            'order_edit/send_refund_message'
        );
    }

    /**
     * Get Refund Message Template.
     *
     * @return string
     */
    public function getRefundMessageTemplate()
    {
        return $this->getConfigValue(
            'order_edit/refund_email_template'
        );
    }

    /**
     * Return bool value depends of that if create order note after
     * order status edition feature is active or not.
     *
     * @return bool
     */
    public function isOrderDeletionEnabled()
    {
        return (bool)$this->getConfigValue(
            'order_delete/allow_deleting_orders'
        );
    }
}
