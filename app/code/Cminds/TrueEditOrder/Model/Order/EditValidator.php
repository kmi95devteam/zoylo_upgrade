<?php

namespace Cminds\TrueEditOrder\Model\Order;

use Cminds\TrueEditOrder\Model\Config;
use Magento\Sales\Model\Order;

class EditValidator
{
    /**
     * Module config model.
     *
     * @var Config
     */
    protected $config;

    /**
     * Object constructor.
     *
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Get allowed order statuses from module configuration.
     *
     * @return array|false
     */
    public function getAllowedOrderStatuses()
    {
        return $this->config->getAllowedOrderStatuses();
    }

    /**
     * Validate if order can be edited along with current status.
     *
     * @param Order $order
     *
     * @return bool
     */
    public function validateStatus(Order $order)
    {
        $allowedStatuses = $this->getAllowedOrderStatuses();

        if ($allowedStatuses === false
            || !is_array($allowedStatuses)
        ) {
            return false;
        }

        if (!in_array($order->getStatus(), $allowedStatuses, true)) {
            return false;
        }

        return true;
    }
}
