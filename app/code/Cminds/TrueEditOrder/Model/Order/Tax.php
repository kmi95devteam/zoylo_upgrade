<?php

namespace Cminds\TrueEditOrder\Model\Order;

use Magento\Tax\Model\Sales\Order\TaxFactory;
use Magento\Sales\Model\Order\Tax\ItemFactory;
use Cminds\TrueEditOrder\Model\Order\DummyFactory as DummyOrderFactory;
use Magento\Tax\Model\Sales\Order\TaxManagement;

class Tax
{
    /**
     * @var \Magento\Tax\Model\Sales\Order\TaxFactory
     */
    protected $orderTaxFactory;

    /**
     * @var \Magento\Sales\Model\Order\Tax\ItemFactory
     */
    protected $taxItemFactory;

    /**
     * @var DummyOrderFactory
     */
    protected $dummyOrderFactory;

    /**
     * @var TaxManagement
     */
    protected $taxManagement;

    /**
     * Object constructor.
     *
     * @param TaxFactory $orderTaxFactory
     * @param ItemFactory $taxItemFactory
     * @param DummyOrderFactory $dummyOrderFactory
     * @param TaxManagement $taxManagement
     */
    public function __construct(
        TaxFactory $orderTaxFactory,
        ItemFactory $taxItemFactory,
        DummyOrderFactory $dummyOrderFactory,
        TaxManagement $taxManagement
    ) {
        $this->orderTaxFactory = $orderTaxFactory;
        $this->taxItemFactory = $taxItemFactory;
        $this->dummyOrderFactory = $dummyOrderFactory;
        $this->taxManagement = $taxManagement;
    }

    /**
     * Update order taxes, inlcuding sales_order_tax and sales_order_tax_item tables.
     * Logic based on \Magento\Tax\Model\Plugin\OrderSave and
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Sales\Model\Order $order
     *
     * @return Tax
     */
    public function updateTax($quote, $order)
    {
        /** @var \Cminds\TrueEditOrder\Model\Order\Dummy $dummyOrder */
        $dummyOrderGenerator = $this->dummyOrderFactory->create();
        $dummyOrder = $dummyOrderGenerator->generateDummyOrder($quote);

        $extensionAttribute = $dummyOrder->getExtensionAttributes();

        /** @var \Magento\Tax\Api\Data\OrderTaxDetailsAppliedTaxInterface[]|null $taxes */
        $taxes = null;
        if ($extensionAttribute){
            $taxes = $extensionAttribute->getAppliedTaxes();            
        }
        if ($taxes == null) {
            $taxes = [];
        }

        /** @var \Magento\Tax\Api\Data\OrderTaxDetailsItemInterface[]|null $taxesForItems */
        $taxesForItems = null;
        if ($extensionAttribute){
            $taxesForItems = $extensionAttribute->getItemAppliedTaxes();
        }
        if ($taxesForItems == null) {
            $taxesForItems = [];
        }

        $ratesIdQuoteItemId = [];
        foreach ($taxesForItems as $taxesArray) {
            foreach ($taxesArray['applied_taxes'] as $rates) {
                if (isset($rates['extension_attributes'])) {
                    /** @var \Magento\Tax\Api\Data\AppliedTaxRateInterface[] $taxRates */
                    $taxRates = $rates['extension_attributes']->getRates();
                    if (is_array($taxRates)) {
                        if (count($taxRates) == 1) {
                            $ratesIdQuoteItemId[$rates['id']][] = [
                                'id' => $taxesArray['item_id'],
                                'percent' => $rates['percent'],
                                'code' => $taxRates[0]['code'],
                                'associated_item_id' => $taxesArray['associated_item_id'],
                                'item_type' => $taxesArray['type'],
                                'amount' => $rates['amount'],
                                'base_amount' => $rates['base_amount'],
                                'real_amount' => $rates['amount'],
                                'real_base_amount' => $rates['base_amount'],
                            ];
                        } else {
                            $percentSum = 0;
                            foreach ($taxRates as $rate) {
                                $realAmount = $rates['amount'] * $rate['percent'] / $rates['percent'];
                                $realBaseAmount = $rates['base_amount'] * $rate['percent'] / $rates['percent'];
                                $ratesIdQuoteItemId[$rates['id']][] = [
                                    'id' => $taxesArray['item_id'],
                                    'percent' => $rate['percent'],
                                    'code' => $rate['code'],
                                    'associated_item_id' => $taxesArray['associated_item_id'],
                                    'item_type' => $taxesArray['type'],
                                    'amount' => $rates['amount'],
                                    'base_amount' => $rates['base_amount'],
                                    'real_amount' => $realAmount,
                                    'real_base_amount' => $realBaseAmount,
                                ];
                                $percentSum += $rate['percent'];
                            }
                        }
                    }
                }
            }
        }

        foreach ($taxes as $row) {
            $id = $row['id'];
            if (isset($row['extension_attributes'])) {
                /** @var \Magento\Tax\Api\Data\AppliedTaxRateInterface[] $taxRates */
                $taxRates = $row['extension_attributes']->getRates();
                if (is_array($taxRates)) {
                    foreach ($taxRates as $tax) {
                        if ($row['percent'] == null) {
                            $baseRealAmount = $row['base_amount'];
                        } else {
                            if ($row['percent'] == 0 || $tax['percent'] == 0) {
                                continue;
                            }
                            $baseRealAmount = $row['base_amount'] / $row['percent'] * $tax['percent'];
                        }
                        $hidden = isset($row['hidden']) ? $row['hidden'] : 0;
                        $priority = isset($tax['priority']) ? $tax['priority'] : 0;
                        $position = isset($tax['position']) ? $tax['position'] : 0;
                        $process = isset($row['process']) ? $row['process'] : 0;
                        $data = [
                            'order_id' => $order->getEntityId(),
                            'code' => $tax['code'],
                            'title' => $tax['title'],
                            'hidden' => $hidden,
                            'percent' => $tax['percent'],
                            'priority' => $priority,
                            'position' => $position,
                            'amount' => $row['amount'],
                            'base_amount' => $row['base_amount'],
                            'process' => $process,
                            'base_real_amount' => $baseRealAmount,
                        ];

                        /** @var $orderTax \Magento\Tax\Model\Sales\Order\Tax */
                        $orderTax = $this->orderTaxFactory->create()->load($order->getId(), 'order_id');
                        if ($orderTax->getTaxId()) {
                            $data['tax_id'] = $orderTax->getTaxId();
                        }
                        $result = $orderTax->setData($data)->save();

                        if (isset($ratesIdQuoteItemId[$id])) {
                            foreach ($ratesIdQuoteItemId[$id] as $quoteItemId) {
                                if ($quoteItemId['code'] === $tax['code']) {
                                    $itemId = null;
                                    $associatedItemId = null;
                                    if (isset($quoteItemId['id'])) {
                                        //This is a product item
                                        $item = $order->getItemByQuoteItemId($quoteItemId['id']);
                                        if ($item !== null && $item->getId()) {
                                            $itemId = $item->getId();
                                        }
                                    } elseif (isset($quoteItemId['associated_item_id'])) {
                                        //This item is associated with a product item
                                        $item = $order->getItemByQuoteItemId($quoteItemId['associated_item_id']);
                                        $associatedItemId = $item->getId();
                                    }

                                    $data = [
                                        'item_id' => $itemId,
                                        'tax_id' => $result->getTaxId(),
                                        'tax_percent' => $quoteItemId['percent'],
                                        'associated_item_id' => $associatedItemId,
                                        'amount' => $quoteItemId['amount'],
                                        'base_amount' => $quoteItemId['base_amount'],
                                        'real_amount' => $quoteItemId['real_amount'],
                                        'real_base_amount' => $quoteItemId['real_base_amount'],
                                        'taxable_item_type' => $quoteItemId['item_type'],
                                    ];

                                    /** @var $taxItem \Magento\Sales\Model\Order\Tax\Item */
                                    $taxItem = $this->taxItemFactory->create()->load($itemId, 'item_id');
                                    if ($taxItem->getTaxItemId()) {
                                        $data['tax_item_id'] = $taxItem->getTaxItemId();
                                    }
                                    $taxItem->setData($data)->save();
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->updateOrder($order);

        return $this;
    }

    /**
     * Update
     *
     * @param $order
     */
    public function updateOrder($order)
    {
        $taxAmount = 0;
        $baseTaxAmount = 0;
        $taxDetails = $this->taxManagement->getOrderTaxDetails($order->getId());

        $appliedTaxes = $taxDetails->getAppliedTaxes();
        foreach ($appliedTaxes as $appliedTax) {
            $taxAmount += $appliedTax->getAmount();
            $baseTaxAmount += $appliedTax->getBaseAmount();
        }

        $order->setTaxAmount($taxAmount);
        $order->setBaseTaxAmount($baseTaxAmount);
        $order->save();
    }
}
