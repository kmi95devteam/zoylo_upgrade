<?php

namespace Cminds\TrueEditOrder\Model\Order;

use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Order\Status\HistoryFactory;
use Magento\Sales\Model\Order;

/**
 * Cminds TrueEditOrder order history model.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class History
{
    /**
     * Order factory object.
     *
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * History factory object.
     *
     * @var HistoryFactory
     */
    protected $historyFactory;

    /**
     * Object constructor.
     *
     * @param OrderFactory   $orderFactory   Order factory object.
     * @param HistoryFactory $historyFactory History factory object.
     */
    public function __construct(
        OrderFactory $orderFactory,
        HistoryFactory $historyFactory
    ) {
        $this->orderFactory = $orderFactory;
        $this->historyFactory = $historyFactory;
    }

    /**
     * Return order.
     *
     * @param int $orderId Order id.
     *
     * @return Order
     */
    protected function getOrder($orderId)
    {
        return $this->orderFactory
            ->create()
            ->load($orderId);
    }

    /**
     * Add comment to an order.
     *
     * @param int    $orderId Order id.
     * @param string $comment Comment message.
     *
     * @return History
     */
    public function addComment($orderId, $comment)
    {
        $order = $this->getOrder($orderId);

        $history = $this->historyFactory->create();

        $history
            ->setData('comment', $comment)
            ->setData('parent_id', $orderId)
            ->setData('is_visible_on_front', 1)
            ->setData('is_customer_notified', 0)
            ->setData('entity_name', 'order')
            ->setData('status', $order->getStatus())
            ->save();

        return $this;
    }
}