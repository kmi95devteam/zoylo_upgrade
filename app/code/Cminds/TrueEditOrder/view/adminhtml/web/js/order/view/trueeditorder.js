/**
 * Cminds TrueEditOrder admin order view script.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */

define([
    'jquery',
    'Magento_Ui/js/modal/confirm',
    'mage/translate'
], function ($, confirm) {
    'use strict';

    var orderId,
        addItemsButtonSelector = '#cminds-trueeditorder-additems-button',
        editItemsButtonSelector = '#cminds-trueeditorder-edititems-button',
        cancelEditItemsButtonSelector = '#cminds-trueeditorder-cancel-edititems-button',
        saveEditItemsButtonSelector = '#cminds-trueeditorder-save-edititems-button',
        fieldsToEditSelector = '.edited-fields',
        originalFieldsSelector = '.original-fields',
        addItemsFormSelector = '#cminds-trueeditorder-additems-form',
        editItemsFormSelector = '#order-items_grid',
        addItemsUrl,
        editItemsUrl,
        itemsContainerSelector = '#cminds-trueeditorder-items-container',
        askConfirmation;

    function init(config) {
        orderId = config.orderId;
        addItemsUrl = config.addItemsUrl;
        editItemsUrl = config.editItemsUrl;
        askConfirmation = config.askConfirmation;
        $(document)
            .ready(initAddItemsButtonOnClickHook)
            .ready(initEditItemsButtonOnClickHook)
            .ready(initCancelEditItemsButtonOnClickHook)
            .ready(initSaveEditItemsButtonOnClickHook);
    }

    function initAddItemsButtonOnClickHook() {
        $(addItemsButtonSelector).on('click', addItemsButtonOnClickHandler);
    }

    function initEditItemsButtonOnClickHook() {
        $(editItemsButtonSelector).on('click', editItemsButtonOnClickHandler);
    }

    function initCancelEditItemsButtonOnClickHook() {
        $(cancelEditItemsButtonSelector).on('click', cancelEditItemsButtonOnClickHandler);
    }

    function initSaveEditItemsButtonOnClickHook() {
        $(saveEditItemsButtonSelector).on('click', saveEditItemsButtonOnClickHandler);
    }

    function addItemsButtonOnClickHandler() {
        $(itemsContainerSelector).show();
        $(addItemsButtonSelector).hide();
        productConfigure.clean('current');
    }

    function editItemsButtonOnClickHandler() {
        $(addItemsButtonSelector).hide();
        $(editItemsButtonSelector).hide();
        $(originalFieldsSelector).hide();
        $(cancelEditItemsButtonSelector).show();
        $(saveEditItemsButtonSelector).show();
        $(fieldsToEditSelector).show();
        productConfigure.clean('current');
    }

    function cancelEditItemsButtonOnClickHandler() {
        $(addItemsButtonSelector).show();
        $(editItemsButtonSelector).show();
        $(originalFieldsSelector).show();
        $(cancelEditItemsButtonSelector).hide();
        $(saveEditItemsButtonSelector).hide();
        $(fieldsToEditSelector).hide();
        productConfigure.clean('current');
    }

    function appendProductConfigure(form) {
        var blockConfirmed = productConfigure.blockConfirmed;
        var childElements = blockConfirmed.childElements();

        childElements.each(function (blockItem) {
            var scopeArr = blockItem.id.match(/.*\[\w+\]\[([^\]]+)\]$/);
            var itemId = scopeArr[1];
            var pattern = new RegExp('(\\w+)(\\[?)');
            var replacement = 'item[' + itemId + '][$1]$2';

            var renameAndAppend = function (elms) {
                for (var i = 0; i < elms.length; i++) {
                    if (elms[i].name) {
                        if (elms[i].type == 'radio') {
                            if (elms[i].checked) {
                                elms[i].name = elms[i].name.replace(pattern, replacement);
                                $('<input>').attr({
                                    type: 'hidden',
                                    name: elms[i].name,
                                    value: elms[i].getValue()
                                }).appendTo(form);
                            }
                        } else {
                            elms[i].name = elms[i].name.replace(pattern, replacement);
                            $('<input>').attr({
                                type: 'hidden',
                                name: elms[i].name,
                                value: elms[i].getValue()
                            }).appendTo(form);
                        }

                    }
                }
            };
            renameAndAppend(blockItem.getElementsByTagName('input'));
            renameAndAppend(blockItem.getElementsByTagName('select'));
            renameAndAppend(blockItem.getElementsByTagName('textarea'));
        });
    }

    function saveEditItemsButtonOnClickHandler() {
        if (askConfirmation) {
            confirm({
                title: 'Confirmation',
                content: 'Customer will be notified in case of decreased order\'s total cost. Save changes?',
                actions: {
                    confirm: saveEditItems
                }
            });
        } else {
            saveEditItems();
        }
    }

    function saveEditItems() {
        $('<input>').attr({
            type: 'hidden',
            name: 'order_id',
            value: orderId
        }).appendTo(editItemsFormSelector);

        appendProductConfigure(editItemsFormSelector);

        var form = $(editItemsFormSelector);
        form.attr('action', editItemsUrl);
        form.attr('method', 'post');
        form.submit();
    }

    function addSelected() {
        var selectedItems = $(itemsContainerSelector)
                .find('.data-grid')
                .find('input:checked'),
            form = $(addItemsFormSelector);

        selectedItems.each(function () {
            var qtyField = $(this)
                    .parents('tr')
                    .find('input[name=qty]'),
                qty = parseInt(qtyField.val());

            $('<input>').attr({
                type: 'hidden',
                name: 'item[' + $(this).val() + '][qty]',
                value: qty
            }).appendTo(addItemsFormSelector);
        });

        appendProductConfigure(addItemsFormSelector);

        $('<input>').attr({
            type: 'hidden',
            name: 'order_id',
            value: orderId
        }).appendTo(addItemsFormSelector);

        form.attr('action', addItemsUrl);
        form.attr('method', 'post');
        form.submit();
    }

    return {
        init: function (config) {
            init(config);
        },
        addSelected: function () {
            addSelected();
        }
    };
});