<?php

namespace Cminds\TrueEditOrder\Observer\Order\Address;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Quote\Model\QuoteFactory;

class Update implements ObserverInterface
{
    /**
     * Order Factory.
     *
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * Quote Factory.
     *
     * @var QuoteFactory
     */
    protected $quoteFactory;

    /**
     * Update constructor.
     *
     * @param OrderFactory $orderFactory
     * @param QuoteFactory $quoteFactory
     */
    public function __construct(
        OrderFactory $orderFactory,
        QuoteFactory $quoteFactory
    ) {
        $this->orderFactory = $orderFactory;
        $this->quoteFactory = $quoteFactory;
    }

    /**
     * Update quote shipping address.
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $orderId = $observer->getOrderId();
        $order = $this->orderFactory->create()
            ->load($orderId);

        $shippingAddress = $order->getShippingAddress();

        $quoteId = $order->getQuoteId();
        $quote = $this->quoteFactory->create()
            ->load($quoteId);

        if (!$quote->getId()) {
            return;
        }

        $quoteShippingAddress = $quote->getShippingAddress();
        $quoteShippingAddressId = $quoteShippingAddress->getId();

        $quoteShippingAddress = $quote
            ->getShippingAddress()
            ->setData($shippingAddress->getData())
            ->setId($quoteShippingAddressId);

        $quote
            ->setShippingAddress($quoteShippingAddress)
            ->save();
    }
}
