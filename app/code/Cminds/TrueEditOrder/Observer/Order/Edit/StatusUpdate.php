<?php

namespace Cminds\TrueEditOrder\Observer\Order\Edit;

use Magento\Framework\Event\Observer;

class StatusUpdate extends AbstractObserver
{
    /**
     * Handle order status edit event.
     * Add proper order comment which will describe changes.
     *
     * @param Observer $observer Observer object.
     *
     * @return StatusUpdate
     */
    public function execute(Observer $observer)
    {
        $flag = $this->moduleConfig
            ->isCreateOrderNoteAfterOrderStatusEditionEnabled();
        if ($flag === false) {
            return $this;
        }

        $order = $observer->getOrder();
        $oldOrderStatus = $observer->getOldOrderStatus();
        $newOrderStatus = $observer->getNewOrderStatus();

        $comment = __(
            'User %1 has updated order status.',
            $this->getUser()->getUserName()
        );
        $comment .= '<br><br>';

        $comment .= __(
            'Order status has been changed from %1 to %2.',
            $oldOrderStatus,
            $newOrderStatus
        );

        $this->orderHistory->addComment(
            $order->getId(),
            $comment
        );

        return $this;
    }
}
