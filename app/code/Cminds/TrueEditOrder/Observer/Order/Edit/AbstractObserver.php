<?php

namespace Cminds\TrueEditOrder\Observer\Order\Edit;

use Cminds\TrueEditOrder\Model\Order\History as OrderHistory;
use Cminds\TrueEditOrder\Model\Config as ModuleConfig;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\User\Model\User;

/**
 * Cminds TrueEditOrder order edit abstract observer.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
abstract class AbstractObserver implements ObserverInterface
{
    /**
     * Order history object.
     *
     * @var OrderHistory
     */
    protected $orderHistory;

    /**
     * Auth session object.
     *
     * @var Session
     */
    protected $authSession;

    /**
     * Module config object.
     *
     * @var ModuleConfig
     */
    protected $moduleConfig;

    /**
     * Object constructor.
     *
     * @param OrderHistory $orderHistory Order history object.
     * @param Session      $authSession  Auth session object.
     * @param ModuleConfig $moduleConfig Module config object.
     */
    public function __construct(
        OrderHistory $orderHistory,
        Session $authSession,
        ModuleConfig $moduleConfig
    ) {
        $this->orderHistory = $orderHistory;
        $this->authSession = $authSession;
        $this->moduleConfig = $moduleConfig;
    }

    /**
     * Return logged in user.
     *
     * @return User
     */
    protected function getUser()
    {
        return $this->authSession->getUser();
    }

    /**
     * Handle order edit add items event.
     *
     * @param Observer $observer Observer object.
     *
     * @return AbstractObserver
     */
    abstract public function execute(Observer $observer);
}
