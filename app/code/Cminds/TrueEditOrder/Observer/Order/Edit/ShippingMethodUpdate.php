<?php

namespace Cminds\TrueEditOrder\Observer\Order\Edit;

use Magento\Framework\Event\Observer;

/**
 * Cminds TrueEditOrder order edit shipping method update event observer.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class ShippingMethodUpdate extends AbstractObserver
{
    /**
     * Handle order edit add items event.
     * Add proper order comment which will describe changes.
     *
     * @param Observer $observer Observer object.
     *
     * @return AddItems
     */
    public function execute(Observer $observer)
    {
        $flag = $this->moduleConfig
            ->isCreateOrderNoteAfterShippingMethodEditionEnabled();
        if ($flag === false) {
            return $this;
        }

        $order = $observer->getOrder();
        $oldShippingMethod = $observer->getOldShippingMethod();
        $newShippingMethod = $observer->getNewShippingMethod();

        $comment = __(
            'User %1 has updated order shipping method.',
            $this->getUser()->getUserName()
        );
        $comment .= '<br><br>';

        $comment .= __(
            'Shipping method has been changed from %1 to %2.',
            $oldShippingMethod,
            $newShippingMethod
        );

        $this->orderHistory->addComment(
            $order->getId(),
            $comment
        );

        return $this;
    }
}
