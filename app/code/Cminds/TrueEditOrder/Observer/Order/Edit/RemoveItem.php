<?php

namespace Cminds\TrueEditOrder\Observer\Order\Edit;

use Magento\Framework\Event\Observer;

/**
 * Cminds TrueEditOrder order edit remove item event observer.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class RemoveItem extends AbstractObserver
{
    /**
     * Handle order edit remove items event.
     * Add proper order comment which will describe changes.
     *
     * @param Observer $observer Observer object.
     *
     * @return AddItems
     */
    public function execute(Observer $observer)
    {
        $flag = $this->moduleConfig
            ->isCreateOrderNoteAfterOrderItemsEditionEnabled();
        if ($flag === false) {
            return $this;
        }

        $order = $observer->getOrder();
        $item = $observer->getItem();

        $comment = __(
            'User %1 has removed item from the order.',
            $this->getUser()->getUserName()
        );
        $comment .= '<br><br>';

        $comment .= __('Removed item:');
        $comment .= '<br>';
        $comment .= sprintf(
            '- %sx %s (%s).',
            (int)$item->getQtyOrdered(),
            $item->getName(),
            $item->getSku()
        );
        $comment .= '<br>';

        $this->orderHistory->addComment(
            $order->getId(),
            $comment
        );

        return $this;
    }
}
