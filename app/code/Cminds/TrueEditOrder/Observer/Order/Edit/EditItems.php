<?php

namespace Cminds\TrueEditOrder\Observer\Order\Edit;

use Cminds\TrueEditOrder\Model\Config as ModuleConfig;
use Cminds\TrueEditOrder\Model\Order\History as OrderHistory;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\Event\Observer;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;

class EditItems extends AbstractObserver
{
    /**
     * @var PriceHelper
     */
    protected $priceHelper;

    /**
     * EditItems constructor.
     *
     * @param OrderHistory $orderHistory
     * @param Session $authSession
     * @param ModuleConfig $moduleConfig
     * @param PriceHelper $priceHelper
     */
    public function __construct(
        OrderHistory $orderHistory,
        Session $authSession,
        ModuleConfig $moduleConfig,
        PriceHelper $priceHelper
    ) {
        $this->priceHelper = $priceHelper;

        parent::__construct($orderHistory, $authSession, $moduleConfig);
    }

    /**
     * Handle order edit items event.
     * Add proper order comment which will describe changes.
     *
     * @param Observer $observer Observer object.
     *
     * @return EditItems
     */
    public function execute(Observer $observer)
    {
        $flag = $this->moduleConfig
            ->isCreateOrderNoteAfterOrderItemsEditionEnabled();
        if ($flag === false) {
            return $this;
        }

        $order = $observer->getOrder();
        $items = $observer->getEditedItems();

        $comment = __(
            'User %1 has edited item(s) in the order.',
            $this->getUser()->getUserName()
        );
        $comment .= '<br><br>';

        $comment .= __('Edited items:');
        $comment .= '<br>';

        foreach ($items as $item) {
            $origName = $item->getOrigData('name') ?: '';
            $name = $item->getData('name') ?: '';
            $origDescription = $item->getOrigData('description') ?: '';
            $description = $item->getData('description') ?: '';
            $origQty = (int)$item->getOrigData('qty_ordered');
            $qty = (int)$item->getData('qty_ordered');
            $origPrice = $item->getOrigData('price');
            $price = $item->getData('price');

            $comment .= __(
                '%1 Name: %2 -> %3. Description: %4 -> %5. Sku: %6. Qty: %7 -> %8. Price: %9 -> %10',
                $item->getName(),
                $origName,
                $name,
                $origDescription,
                $description,
                $item->getSku(),
                $origQty,
                $qty,
                $this->priceHelper->currency($origPrice, true, false),
                $this->priceHelper->currency($price, true, false)
            );

            $comment .= '<br>';
        }

        $this->orderHistory->addComment(
            $order->getId(),
            $comment
        );

        return $this;
    }
}
