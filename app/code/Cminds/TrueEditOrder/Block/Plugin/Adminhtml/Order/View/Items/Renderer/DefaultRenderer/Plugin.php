<?php

namespace Cminds\TrueEditOrder\Block\Plugin\Adminhtml\Order\View\Items\Renderer\DefaultRenderer;

use Cminds\TrueEditOrder\Model\Config as ModuleConfig;
use Magento\Sales\Block\Adminhtml\Order\View\Items\Renderer\DefaultRenderer\Interceptor;
use Magento\Framework\AuthorizationInterface;

/**
 * Cminds TrueEditOrder adminhtml default items renderer plugin.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class Plugin
{
    /**
     * Module config object.
     *
     * @var ModuleConfig
     */
    protected $moduleConfig;

    /**
     * Authorization object.
     *
     * @var AuthorizationInterface
     */
    protected $authorization;

    /**
     * Object constructor.
     *
     * @param ModuleConfig           $moduleConfig  Module config object.
     * @param AuthorizationInterface $authorization Authorization object.
     */
    public function __construct(
        ModuleConfig $moduleConfig,
        AuthorizationInterface $authorization
    ) {
        $this->moduleConfig = $moduleConfig;
        $this->authorization = $authorization;
    }

    /**
     * Add remove column to order items.
     *
     * @param Interceptor $subject Subject object.
     * @param array       $columns Columns array.
     *
     * @return array
     */
    public function afterGetColumns(
        Interceptor $subject,
        array $columns
    ) {
        if ($this->moduleConfig->isActive() === false
            || $this->moduleConfig->isOrderItemsEditionEnabled() === false
        ) {
            return $columns;
        }

        $aclResource = 'Cminds_TrueEditOrder::edit_order_items';
        if ($this->authorization->isAllowed($aclResource) === false) {
            return $columns;
        }

        $columns['remove'] = 'column_remove';

        return $columns;
    }
}
