<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\Edit\Assign;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Model\Session\Quote;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Phrase;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;
use Magento\Quote\Model\Quote\Address\RateRequestFactory;
use Magento\Sales\Block\Adminhtml\Order\Create\Form\AbstractForm;
use Magento\Sales\Model\AdminOrder\Create;
use Magento\Shipping\Model\Config as ShippingConfig;
use Magento\Sales\Model\Order;
use Magento\Shipping\Model\Shipping;
use Magento\Quote\Model\QuoteFactory;

/**
 * Cminds TrueEditOrder admin order shipping method edit form block.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class Form extends AbstractForm {

    /**
     * Form template.
     *
     * @var string
     */
    protected $_template = 'order/attributes/form.phtml';

    /**
     * Core registry object.
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * Shipping object.
     *
     * @var Shipping
     */
    protected $shipping;

    /**
     * Rate request factory object.
     *
     * @var RateRequestFactory
     */
    protected $rateRequestFactory;

    /**
     * Pricing helper object.
     *
     * @var PricingHelper
     */
    protected $pricingHelper;

    /**
     * Quote create object.
     *
     * @var QuoteFactory
     */
    protected $quoteFactory;

    /**
     * Shipping Config.
     *
     * @var ShippingConfig
     */
    protected $shippingConfig;
    protected $objectManager;

    /**
     * Form constructor.
     *
     * @param Context $context
     * @param Quote $sessionQuote
     * @param Create $orderCreate
     * @param PriceCurrencyInterface $priceCurrency
     * @param FormFactory $formFactory
     * @param DataObjectProcessor $dataObjectProcessor
     * @param Registry $registry
     * @param Shipping $shipping
     * @param RateRequestFactory $rateRequestFactory
     * @param PricingHelper $pricingHelper
     * @param QuoteFactory $quoteFactory
     * @param ShippingConfig $shippingConfig
     * @param array $data
     */
    public function __construct(
    Context $context, Quote $sessionQuote, Create $orderCreate, PriceCurrencyInterface $priceCurrency, FormFactory $formFactory, DataObjectProcessor $dataObjectProcessor, Registry $registry, Shipping $shipping, RateRequestFactory $rateRequestFactory, PricingHelper $pricingHelper, QuoteFactory $quoteFactory, ShippingConfig $shippingConfig, array $data = []
    ) {
        parent::__construct(
                $context, $sessionQuote, $orderCreate, $priceCurrency, $formFactory, $dataObjectProcessor, $data
        );

        $this->coreRegistry = $registry;
        $this->shipping = $shipping;
        $this->rateRequestFactory = $rateRequestFactory;
        $this->pricingHelper = $pricingHelper;
        $this->quoteFactory = $quoteFactory;
        $this->shippingConfig = $shippingConfig;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    /**
     * Order getter.
     *
     * @return Order
     */
    protected function getOrder() {
        return $this->coreRegistry->registry('trueditorder_order');
    }

    /**
     * Define form attributes (id, method, action).
     *
     * @return $this
     */
    protected function _prepareForm() {
        $this->_form->setId('edit_form');
        $this->_form->setMethod('post');
        $this->_form->setAction(
                $this->getUrl(
                        'trueeditorder/*/savePost', ['order_id' => $this->getOrder()->getId()]
                )
        );
        $this->_form->setUseContainer(true);

        $fieldset = $this->_form->addFieldset('main', ['no_container' => true]);

        $fieldset->addField(
                'order_id', 'hidden', [
            'name' => 'order_id',
            'value' => $this->getOrder()->getId(),
                ]
        );

        $doctorName = $this->getOrder()->getDoctorName();
        $age = $this->getOrder()->getAge();
        $user = $this->getOrder()->getAssignedUser();
        $users = $this->getUsersOptionArray();
        $fieldset->addField(
                'customer_gender', 'select', [
            'name' => 'assign_user',
            'label' => __('Users'),
            'title' => __('Users'),
            'values' => $users,
            'required' => false,
            'value' => $user
                ]
        );




        return $this;
    }

    /**
     * Form header text getter.
     *
     * @return Phrase
     */
    public function getHeaderText() {
        return __('Order User Assignment');
    }

    /**
     * Retrieve genders.
     *
     * @return array
     */
    protected function getUsersOptionArray() {
        
        //$roleName = "Administrators";
        $rolesConfig = $this->objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
        $roleName =  $rolesConfig->getValue('user_assignment_role/role_user/users_rold', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
         
        $roleModel = $this->objectManager->create('Magento\Authorization\Model\Role');
        $userModel = $this->objectManager->create('Magento\User\Model\User');
        $roleModel = $roleModel->load($roleName, 'role_name');
        $optionArray[] = [
                    'value' => 'Select',
                    'label' => 'Select'
                ];
        if ($roleModel->getId()) {
            $userIds = $roleModel->getRoleUsers();
            foreach ($userIds as $userId) {
                $user = $userModel->load($userId);
                $name = $user->getUsername();
                $optionArray[] = [
                    'value' => $name,
                    'label' => $name,
                ];
            }
        }
        return $optionArray;
    }

}
