<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\Edit;

use Magento\Sales\Model\Order;

class Coupon extends ShippingMethod
{
    /**
     * @var string
     */
    protected $buttonLabel = 'Save Coupon';

    /**
     * @var string
     */
    protected $mode = 'edit_coupon';

    /**
     * {@inheritdoc}
     */
    public function getHeaderText()
    {
        $order = $this->getOrder();
        $orderId = $order->getIncrementId();

        return __('Edit Order %1 Coupon', $orderId);
    }
}
