<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\Edit\History;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Model\Session\Quote;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Phrase;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;
use Magento\Quote\Model\Quote\Address\RateRequestFactory;
use Magento\Sales\Block\Adminhtml\Order\Create\Form\AbstractForm;
use Magento\Sales\Model\AdminOrder\Create;
use Magento\Sales\Model\Order\Status\History;
use Magento\Shipping\Model\Shipping;

/**
 * Cminds TrueEditOrder admin order history edit form block.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class Form extends AbstractForm
{
    /**
     * Form template.
     *
     * @var string
     */
    protected $_template = 'order/history/form.phtml';

    /**
     * Core registry object.
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * Shipping object.
     *
     * @var Shipping
     */
    protected $shipping;

    /**
     * Rate request factory object.
     *
     * @var RateRequestFactory
     */
    protected $rateRequestFactory;

    /**
     * Pricing helper object.
     *
     * @var PricingHelper
     */
    protected $pricingHelper;

    /**
     * Object constructor.
     *
     * @param Context                $context             Context object.
     * @param Quote                  $sessionQuote        Session quote object.
     * @param Create                 $orderCreate         Order create object.
     * @param PriceCurrencyInterface $priceCurrency       Price currency object.
     * @param FormFactory            $formFactory         Form factory object.
     * @param DataObjectProcessor    $dataObjectProcessor Data object processor object.
     * @param Registry               $registry            Registry object.
     * @param Shipping               $shipping            Shipping object.
     * @param RateRequestFactory     $rateRequestFactory  Rate request factory object.
     * @param PricingHelper          $pricingHelper       Pricing helper object.
     * @param array                  $data                Data array.
     */
    public function __construct(
        Context $context,
        Quote $sessionQuote,
        Create $orderCreate,
        PriceCurrencyInterface $priceCurrency,
        FormFactory $formFactory,
        DataObjectProcessor $dataObjectProcessor,
        Registry $registry,
        Shipping $shipping,
        RateRequestFactory $rateRequestFactory,
        PricingHelper $pricingHelper,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $sessionQuote,
            $orderCreate,
            $priceCurrency,
            $formFactory,
            $dataObjectProcessor,
            $data
        );

        $this->coreRegistry = $registry;
        $this->shipping = $shipping;
        $this->rateRequestFactory = $rateRequestFactory;
        $this->pricingHelper = $pricingHelper;
    }

    /**
     * Order note getter.
     *
     * @return History
     */
    protected function getOrderNote()
    {
        return $this->coreRegistry->registry('trueditorder_order_note');
    }

    /**
     * Define form attributes (id, method, action).
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $this->_form->setId('edit_form');
        $this->_form->setMethod('post');
        $this->_form->setAction(
            $this->getUrl('trueeditorder/*/savePost')
        );
        $this->_form->setUseContainer(true);

        $fieldset = $this->_form->addFieldset('main', ['no_container' => true]);

        $fieldset->addField(
            'history_id',
            'hidden',
            [
                'name' => 'history_id',
                'value' => $this->getOrderNote()->getId(),
            ]
        );

        $comment = $this->getOrderNote()->getComment();
        $comment = str_replace('<br>', "\n", $comment);

        $fieldset->addField(
            'comment',
            'textarea',
            [
                'name' => 'comment',
                'label' => __('Comment'),
                'title' => __('Comment'),
                'required' => true,
                'class' => 'required-entry',
                'value' => $comment,
            ]
        );

        return $this;
    }

    /**
     * Form header text getter.
     *
     * @return Phrase
     */
    public function getHeaderText()
    {
        return __('Order Note');
    }
}
