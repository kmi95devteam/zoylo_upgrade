<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\Edit;

use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Block\Widget\Form\Container;
use Magento\Framework\Registry;
use Magento\Sales\Model\Order\Status\History as OrderNote;
use Magento\Framework\App\Response\RedirectInterface;

/**
 * Cminds TrueEditOrder admin order history edit block.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class History extends Container
{
    /**
     * Core registry object.
     *
     * @var Registry
     */
    protected $coreRegistry = null;

    /**
     * Redirect object.
     *
     * @var RedirectInterface
     */
    protected $redirect;

    /**
     * Object initialization.
     *
     * @param Context           $context  Context object.
     * @param Registry          $registry Registry object.
     * @param RedirectInterface $redirect Redirect object.
     * @param array             $data     Data array.
     */
    public function __construct(
        Context $context,
        Registry $registry,
        RedirectInterface $redirect,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        $this->redirect = $redirect;

        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Internal constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_order';
        $this->_mode = 'edit_history';
        $this->_blockGroup = 'Cminds_TrueEditOrder';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Order Note'));
        $this->buttonList->remove('delete');
    }

    /**
     * Order note getter.
     *
     * @return OrderNote
     */
    protected function getOrderNote()
    {
        return $this->coreRegistry->registry('trueditorder_order_note');
    }

    /**
     * Retrieve text for header element depending on loaded page.
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        return __('Edit Order %1 Note');
    }

    /**
     * Back button url getter.
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl($this->redirect->getRefererUrl());
    }
}
