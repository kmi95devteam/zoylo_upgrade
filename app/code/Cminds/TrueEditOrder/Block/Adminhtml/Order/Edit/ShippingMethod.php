<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\Edit;

use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Block\Widget\Form\Container;
use Magento\Framework\Registry;
use Magento\Sales\Model\Order;

/**
 * Cminds TrueEditOrder admin order shipping method edit block.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class ShippingMethod extends Container
{
    /**
     * Core registry object.
     *
     * @var Registry
     */
    protected $coreRegistry = null;

    /**
     * @var string
     */
    protected $buttonLabel = 'Save Shipping Method';

    /**
     * @var string
     */
    protected $mode = 'edit_shippingMethod';

    /**
     * Object initialization.
     *
     * @param Context  $context  Context object.
     * @param Registry $registry Registry object.
     * @param array    $data     Data array.
     */
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->coreRegistry = $registry;

        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Internal constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_order';
        $this->_blockGroup = 'Cminds_TrueEditOrder';
        $this->_mode = $this->mode;

        parent::_construct();

        $this->buttonList->update('save', 'label', __($this->buttonLabel));
        $this->buttonList->remove('delete');
    }

    /**
     * Order getter.
     *
     * @return Order
     */
    protected function getOrder()
    {
        return $this->coreRegistry->registry('trueditorder_order');
    }

    /**
     * Retrieve text for header element depending on loaded page.
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        $order = $this->getOrder();
        $orderId = $order->getIncrementId();

        return __('Edit Order %1 Shipping Method', $orderId);
    }

    /**
     * Back button url getter.
     *
     * @return string
     */
    public function getBackUrl()
    {
        $order = $this->getOrder();

        return $this->getUrl(
            'sales/order/view',
            [   'order_id' => $order ? $order->getId() : null]
        );
    }
}
