<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\Edit\Status;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Model\Session\Quote;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Phrase;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;
use Magento\Quote\Model\Quote\Address\RateRequestFactory;
use Magento\Sales\Block\Adminhtml\Order\Create\Form\AbstractForm;
use Magento\Sales\Model\AdminOrder\Create;
use Magento\Sales\Model\Order;
use Magento\Shipping\Model\Shipping;
use Cminds\TrueEditOrder\Model\Config\Source\Sales\Order\Status as StatusConfig;

class Form extends AbstractForm
{
    /**
     * Core registry object.
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * Form template.
     *
     * @var string
     */
    protected $_template = 'order/status/form.phtml';

    /**
     * Order statuses array.
     *
     * @var StatusConfig
     */
    protected $statusConfig;

    /**
     * Object constructor.
     *
     * @param Context                $context             Context object.
     * @param Quote                  $sessionQuote        Session quote object.
     * @param Create                 $orderCreate         Order create object.
     * @param PriceCurrencyInterface $priceCurrency       Price currency object.
     * @param FormFactory            $formFactory         Form factory object.
     * @param DataObjectProcessor    $dataObjectProcessor Data object processor object.
     * @param Registry               $registry            Registry object.
     * @param Shipping               $shipping            Shipping object.
     * @param RateRequestFactory     $rateRequestFactory  Rate request factory object.
     * @param PricingHelper          $pricingHelper       Pricing helper object.
     * @param array                  $data                Data array.
     */
    public function __construct(
        Context $context,
        Quote $sessionQuote,
        Create $orderCreate,
        PriceCurrencyInterface $priceCurrency,
        FormFactory $formFactory,
        DataObjectProcessor $dataObjectProcessor,
        Registry $registry,
        Shipping $shipping,
        RateRequestFactory $rateRequestFactory,
        PricingHelper $pricingHelper,
        StatusConfig $statusConfig,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $sessionQuote,
            $orderCreate,
            $priceCurrency,
            $formFactory,
            $dataObjectProcessor,
            $data
        );

        $this->coreRegistry = $registry;
        $this->statusConfig = $statusConfig;
    }

    /**
     * Define form attributes (id, method, action).
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $this->_form->setId('edit_form');
        $this->_form->setMethod('post');
        $this->_form->setAction(
            $this->getUrl('trueeditorder/*/savePost')
        );
        $this->_form->setUseContainer(true);

        $fieldset = $this->_form->addFieldset('main', ['no_container' => true]);

        $currentOrderStatus = $this->getOrder()->getStatus();

        $fieldset->addField(
            'order_id',
            'hidden',
            [
                'name' => 'order_id',
                'value' => $this->getOrder()->getId(),
            ]
        );

        $fieldset->addField(
            'status',
            'select',
            [
                'label' => __('Status'),
                'name' => 'status',
                'required' => true,
                'values' => $this->statusConfig->toOptionArray(),
                'value' => $currentOrderStatus
            ]
        );
        return $this;
    }

    /**
     * Form header text getter.
     *
     * @return Phrase
     */
    public function getHeaderText()
    {
        return __('Order Status');
    }

    /**
     * Order getter.
     *
     * @return Order
     */
    protected function getOrder()
    {
        return $this->coreRegistry->registry('trueditorder_order');
    }
}
