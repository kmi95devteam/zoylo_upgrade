<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\Edit\Attribute;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Model\Session\Quote;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Phrase;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;
use Magento\Quote\Model\Quote\Address\RateRequestFactory;
use Magento\Sales\Block\Adminhtml\Order\Create\Form\AbstractForm;
use Magento\Sales\Model\AdminOrder\Create;
use Magento\Shipping\Model\Config as ShippingConfig;
use Magento\Sales\Model\Order;
use Magento\Shipping\Model\Shipping;
use Magento\Quote\Model\QuoteFactory;

/**
 * Cminds TrueEditOrder admin order shipping method edit form block.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class Form extends AbstractForm
{
    /**
     * Form template.
     *
     * @var string
     */
    protected $_template = 'order/attributes/form.phtml';

    /**
     * Core registry object.
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * Shipping object.
     *
     * @var Shipping
     */
    protected $shipping;

    /**
     * Rate request factory object.
     *
     * @var RateRequestFactory
     */
    protected $rateRequestFactory;

    /**
     * Pricing helper object.
     *
     * @var PricingHelper
     */
    protected $pricingHelper;

    /**
     * Quote create object.
     *
     * @var QuoteFactory
     */
    protected $quoteFactory;

    /**
     * Shipping Config.
     *
     * @var ShippingConfig
     */
    protected $shippingConfig;

    /**
     * Form constructor.
     *
     * @param Context $context
     * @param Quote $sessionQuote
     * @param Create $orderCreate
     * @param PriceCurrencyInterface $priceCurrency
     * @param FormFactory $formFactory
     * @param DataObjectProcessor $dataObjectProcessor
     * @param Registry $registry
     * @param Shipping $shipping
     * @param RateRequestFactory $rateRequestFactory
     * @param PricingHelper $pricingHelper
     * @param QuoteFactory $quoteFactory
     * @param ShippingConfig $shippingConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        Quote $sessionQuote,
        Create $orderCreate,
        PriceCurrencyInterface $priceCurrency,
        FormFactory $formFactory,
        DataObjectProcessor $dataObjectProcessor,
        Registry $registry,
        Shipping $shipping,
        RateRequestFactory $rateRequestFactory,
        PricingHelper $pricingHelper,
        QuoteFactory $quoteFactory,
        ShippingConfig $shippingConfig,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $sessionQuote,
            $orderCreate,
            $priceCurrency,
            $formFactory,
            $dataObjectProcessor,
            $data
        );

        $this->coreRegistry = $registry;
        $this->shipping = $shipping;
        $this->rateRequestFactory = $rateRequestFactory;
        $this->pricingHelper = $pricingHelper;
        $this->quoteFactory = $quoteFactory;
        $this->shippingConfig = $shippingConfig;
    }

    /**
     * Order getter.
     *
     * @return Order
     */
    protected function getOrder()
    {
        return $this->coreRegistry->registry('trueditorder_order');
    }

    /**
     * Define form attributes (id, method, action).
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $this->_form->setId('edit_form');
        $this->_form->setMethod('post');
        $this->_form->setAction(
            $this->getUrl(
                'trueeditorder/*/savePost',
                ['order_id' => $this->getOrder()->getId()]
            )
        );
        $this->_form->setUseContainer(true);

        $fieldset = $this->_form->addFieldset('main', ['no_container' => true]);

        $fieldset->addField(
            'order_id',
            'hidden',
            [
                'name' => 'order_id',
                'value' => $this->getOrder()->getId(),
            ]
        );

        $doctorName = $this->getOrder()->getDoctorName();
        $age        = $this->getOrder()->getAge();
        $gender     = $this->getOrder()->getCustomerGender();
        $genderOptions = $this->getGendersOptionArray();
       
        // if price isn't custom, than do not output anything here
        $fieldset->addField(
            'doctor_name',
            'text',
            [
                'name' => 'doctor_name',
                'label' => __('Doctor Name'),
                'title' => __('Doctor Name'),
                'required' => true,
                'class' => 'required-entry',
                'value' => $doctorName
            ]
        );

        $fieldset->addField(
            'customer_gender',
            'select',
            [
                'name' => 'customer_gender',
                'label' => __('Gender'),
                'title' => __('Gender'),
                'values' => $genderOptions,
                'required' => false,
                'value' => $gender
            ]
        );
        
        // if price isn't custom, than do not output anything here
        $fieldset->addField(
            'age',
            'text',
            [
                'name' => 'age',
                'label' => __('Age'),
                'title' => __('Age'),
                'required' => false,
                'class' => 'validate-number',
                'value' => $age
            ]
        );
        

        return $this;
    }

    /**
     * Form header text getter.
     *
     * @return Phrase
     */
    public function getHeaderText()
    {
        return __('Order Attributes');
    }

    /**
     * Retrieve genders.
     *
     * @return array
     */
    protected function getGendersOptionArray()
    {
        $optionArray = [
            '0' => 'Select Gender',
            '1' => 'Male',
            '2' => 'Female',
            '3' => 'Not Specified',
        ];
        return $optionArray;
    }
}
