<?php
namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\View\Tab\Info\Payment;

use Cminds\TrueEditOrder\Block\Adminhtml\Order\View\Tab\Info\Buttons\EditPaymentMethod;
use Cminds\TrueEditOrder\Model\Config as TrueOrderConfig;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Payment\Model\Config;

class Edit extends EditPaymentMethod
{
    /**
     * @var \Magento\Payment\Helper\Config
     */
    protected $paymentConfig;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * Edit constructor.
     *
     * @param Context $context
     * @param TrueOrderConfig $moduleConfig
     * @param Config $paymentConfig
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context,
        TrueOrderConfig $moduleConfig,
        Config $paymentConfig,
        Registry $registry,
        array $data = []
    ) {
        $this->paymentConfig = $paymentConfig;
        $this->registry = $registry;
        parent::__construct($context, $moduleConfig, $data);
    }

    /**
     * @return array
     */
    public function getPaymentOptions()
    {
        $list = [];
        foreach($this->paymentConfig->getActiveMethods() as $paymentMethod){
            $list[$paymentMethod->getCode()] = $paymentMethod->getTitle();
        }
        return $list;
    }

    /**
     * Retrieve order model instance
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->registry->registry('current_order');
    }
}
