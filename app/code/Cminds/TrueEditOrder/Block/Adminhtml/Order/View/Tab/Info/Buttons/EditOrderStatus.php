<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\View\Tab\Info\Buttons;

class EditOrderStatus extends AbstractButton
{
    /**
     * Prepare html output.
     *
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->moduleConfig->isActive() === false
            || $this->moduleConfig->isOrderShippingMethodEditionEnabled() === false
        ) {
            return '';
        }

        $aclResource = 'Cminds_TrueEditOrder::edit_order_status';
        if ($this->authorization->isAllowed($aclResource) === false) {
            return '';
        }

        return parent::_toHtml();
    }

    /**
     * Retrieve shipping method edit url.
     *
     * @return string
     */
    public function getEditUrl()
    {
        return $this->getUrl(
            'trueeditorder/order_status/edit',
            ['order_id' => $this->getRequest()->getParam('order_id')]
        );
    }
}
