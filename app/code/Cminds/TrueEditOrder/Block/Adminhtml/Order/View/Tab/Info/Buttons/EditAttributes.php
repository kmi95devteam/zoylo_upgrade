<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\View\Tab\Info\Buttons;

/**
 * Cminds TrueEditOrder adminhtml order view tab info edit attributes
 * button block.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class EditAttributes extends AbstractButton
{
    /**
     * Prepare html output.
     *
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->moduleConfig->isActive() === false
            || $this->moduleConfig->isOrderAttributeEditionEnabled() === false
        ) {
            return '';
        }

        $aclResource = 'Cminds_TrueEditOrder::edit_order_attributes';
        if ($this->authorization->isAllowed($aclResource) === false) {
            return '';
        }

        return parent::_toHtml();
    }

    /**
     * Retrieve attribute edit url.
     *
     * @return string
     */
    public function getEditUrl()
    {
        return $this->getUrl(
            'trueeditorder/order_attribute/edit',
            ['order_id' => $this->getRequest()->getParam('order_id')]
        );
    }
}
