<?php
/**
 * Remove product order items list form
 */
namespace Cminds\TrueEditOrder\Block\Adminhtml\Order\View\Items;

use Magento\Backend\Block\Template;

/**
 * Adminhtml order items grid extended
 *
 */
class Remove extends Template
{
    /**
     * Retrieve order product item remove url.
     *
     * @return string
     */
    public function getRemoveUrl()
    {
        return $this->getUrl(
            'trueeditorder/order_items/removePost'
        );
    }
}
