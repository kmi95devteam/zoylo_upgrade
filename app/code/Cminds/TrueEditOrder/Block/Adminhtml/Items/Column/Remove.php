<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Items\Column;

use Magento\Sales\Block\Adminhtml\Items\Column\DefaultColumn;
use Magento\Backend\Block\Template\Context;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\Framework\Registry;
use Magento\Catalog\Model\Product\OptionFactory;
use Cminds\TrueEditOrder\Model\Config as ModuleConfig;
use Magento\Framework\DataObject;
use Magento\Framework\AuthorizationInterface;

/**
 * Cminds TrueEditOrder adminhtml items remove column renderer block.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class Remove extends DefaultColumn
{
    /**
     * Module config object.
     *
     * @var ModuleConfig
     */
    protected $moduleConfig;

    /**
     * Authorization object.
     *
     * @var AuthorizationInterface
     */
    protected $authorization;

    /**
     * Object constructor.
     *
     * @param Context                     $context            Context object.
     * @param StockRegistryInterface      $stockRegistry      Stock registry object.
     * @param StockConfigurationInterface $stockConfiguration Stock configuration object.
     * @param Registry                    $registry           Registry object.
     * @param OptionFactory               $optionFactory      Option factory object.
     * @param ModuleConfig                $moduleConfig       Module config object.
     * @param array                       $data               Data array.
     */
    public function __construct(
        Context $context,
        StockRegistryInterface $stockRegistry,
        StockConfigurationInterface $stockConfiguration,
        Registry $registry,
        OptionFactory $optionFactory,
        ModuleConfig $moduleConfig,
        array $data = []
    ) {
        $this->moduleConfig = $moduleConfig;
        $this->authorization = $context->getAuthorization();

        parent::__construct(
            $context,
            $stockRegistry,
            $stockConfiguration,
            $registry,
            $optionFactory,
            $data
        );
    }

    /**
     * Prepare html output.
     *
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->moduleConfig->isActive() === false
            || $this->moduleConfig->isOrderItemsEditionEnabled() === false
        ) {
            return '';
        }

        $aclResource = 'Cminds_TrueEditOrder::edit_order_items';
        if ($this->authorization->isAllowed($aclResource) === false) {
            return '';
        }

        return parent::_toHtml();
    }
}
