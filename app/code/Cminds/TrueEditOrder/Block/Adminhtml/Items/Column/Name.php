<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Items\Column;

use Magento\Sales\Model\Order\Item;
use Magento\Backend\Block\Widget\Button;

class Name extends \Magento\Sales\Block\Adminhtml\Items\Column\Name
{
    /**
     * Return html button which calls configure window
     *
     * @param Item $item
     *
     * @return string
     */
    public function getConfigureButtonHtml($item)
    {
        $product = $item->getProduct();

        $options = ['label' => __('Configure')];
        /** TODO: To enable button for configurable products also, replace if statement to $product->canConfigure()  */
        if (!empty($product->getOptions())) {
            $options['onclick'] = sprintf(
                'order.showQuoteItemConfiguration(%s)',
                $item->getId()
            );
        } else {
            $options['class'] = ' disabled';
            $options['title'] = __('This product does not have any configurable options');
        }

        return $this->getLayout()->createBlock(Button::class)->setData($options)->toHtml();
    }
}
