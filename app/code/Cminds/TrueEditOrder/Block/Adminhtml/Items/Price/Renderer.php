<?php

namespace Cminds\TrueEditOrder\Block\Adminhtml\Items\Price;

use Magento\Weee\Block\Item\Price\Renderer as ItemPriceRenderer;
use Magento\Quote\Model\QuoteRepository;
use Magento\Backend\Block\Template\Context;
use Magento\Sales\Block\Adminhtml\Items\Column\DefaultColumn;
use Magento\Tax\Helper\Data;

class Renderer extends \Magento\Weee\Block\Adminhtml\Items\Price\Renderer
{
    /**
     * @var QuoteRepository
     */
    protected $quoteRepository;

    /**
     * Renderer constructor.
     *
     * @param Context $context
     * @param DefaultColumn $defaultColumnRenderer
     * @param Data $taxHelper
     * @param ItemPriceRenderer $itemPriceRenderer
     * @param QuoteRepository $quoteRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        DefaultColumn $defaultColumnRenderer,
        Data $taxHelper,
        ItemPriceRenderer $itemPriceRenderer,
        QuoteRepository $quoteRepository,
        array $data = []
    ) {
        $this->quoteRepository = $quoteRepository;

        parent::__construct($context, $defaultColumnRenderer, $taxHelper, $itemPriceRenderer, $data);
    }

    /**
     * Return unit price excl tax
     *
     * @return string
     */
    public function getUnitPriceExclTax()
    {
        return $this->itemPriceRenderer->getUnitDisplayPriceExclTax();
    }

    /**
     * Define if specified item has already applied custom price
     *
     * @param \Magento\Sales\Model\Order\Item $orderItem
     *
     * @return bool
     */
    public function usedCustomPriceForItem($orderItem)
    {
        try {
            $order = $orderItem->getOrder();
            if ($quoteId = $order->getQuoteId()) {
                $quote = $this->quoteRepository->get($quoteId);
                $quoteItem = $quote->getItemById($orderItem->getQuoteItemId());
                return $quoteItem && $quoteItem->hasCustomPrice();
            }
        } catch (\Exception $e) {
            return false;
        }
    }
}
