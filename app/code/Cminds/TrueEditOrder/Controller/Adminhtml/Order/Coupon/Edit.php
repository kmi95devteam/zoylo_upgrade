<?php

namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order\Coupon;

use Cminds\TrueEditOrder\Controller\Adminhtml\Order\ShippingMethod\Edit as EditParent;

class Edit extends EditParent
{
    /**
     * {@inheritdoc}
     */
    const ADMIN_RESOURCE = 'Cminds_TrueEditOrder::edit_order_coupon';
}
