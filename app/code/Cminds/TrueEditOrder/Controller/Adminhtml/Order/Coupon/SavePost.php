<?php

namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order\Coupon;

use Cminds\TrueEditOrder\Controller\Adminhtml\Order\ShippingMethod\SavePost as SavePostParent;
use Cminds\TrueEditOrder\Model\Config;
use Cminds\TrueEditOrder\Model\Order\EditValidator;
use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteFactory;
use Magento\Sales\Model\Order;
use Magento\Shipping\Model\Config as ShippingConfig;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\SalesRule\Model\Coupon;
use Cminds\TrueEditOrder\Controller\Adminhtml\Order\Items\EditPost;

class SavePost extends SavePostParent
{
    /**
     * @var Coupon
     */
    protected $coupon;

    /**
     * Edit Post Controller.
     *
     * @var EditPost
     */
    protected $editPostController;

    /**
     * SavePost constructor.
     *
     * @param Context $context
     * @param OrderFactory $orderFactory
     * @param QuoteFactory $quoteFactory
     * @param Config $moduleConfig
     * @param EditValidator $editValidator
     * @param Coupon $coupon
     * @param ShippingConfig $shippingConfig
     * @param ScopeConfigInterface $scopeConfig
     * @param EditPost $editPostController
     */
    public function __construct(
        Context $context,
        OrderFactory $orderFactory,
        QuoteFactory $quoteFactory,
        Config $moduleConfig,
        EditValidator $editValidator,
        Coupon $coupon,
        ShippingConfig $shippingConfig,
        ScopeConfigInterface $scopeConfig,
        EditPost $editPostController
    ) {
        $this->orderFactory = $orderFactory;
        $this->quoteFactory = $quoteFactory;
        $this->moduleConfig = $moduleConfig;
        $this->editValidator = $editValidator;
        $this->coupon = $coupon;
        $this->editPostController = $editPostController;

        parent::__construct(
            $context,
            $orderFactory,
            $quoteFactory,
            $moduleConfig,
            $editValidator,
            $shippingConfig,
            $scopeConfig
        );
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $couponCode = $this->getRequest()->getParam('coupon_code');

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('sales/order/view', ['order_id' => $orderId]);

        if (!($this->getRequest()->isPost() &&
            $this->moduleConfig->isActive() &&
            $this->moduleConfig->orderCouponEditingEnabled() &&
            $this->_isAllowed('Cminds_TrueEditOrder::edit_order_coupon')
        )
        ) {
            return $resultRedirect;
        }

        if ($couponCode && !$this->coupon->loadByCode($couponCode)->getRuleId()) {
            $this->messageManager->addErrorMessage(
                __('Coupon code is not valid.')
            );

            return $resultRedirect;
        }

        /** @var Order $order */
        $order = $this->orderFactory->create()->load($orderId);

        if (!$this->editValidator->validateStatus($order)) {
            $this->messageManager->addErrorMessage(
                __('Modifying orders with %1 status is not allowed.', $order->getStatus())
            );

            return $resultRedirect;
        }

        $quoteId = $order->getQuoteId();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteFactory->create()->load($quoteId);
        $shippingAmount = $quote
            ->getShippingAddress()
            ->getShippingAmount();

        // if quote shipping amount differs from order amount
        if($shippingAmount != $order->getShippingAmount()) {
            // set custom shipping price    
            $quote
                ->getShippingAddress()
                ->setShippingAmount($order->getShippingAmount());
                
            $shippingRates = $quote
                ->getShippingAddress()
                ->getAllShippingRates();
            $currentSavedMethod = $quote
                ->getShippingAddress()
                ->getShippingMethod();
            
                // go through available rates
            foreach ($shippingRates as $rate) {
                // find selected rate
                if($rate->getCode() === $currentSavedMethod){
                    // change rates price
                    $rate->setPrice($order->getShippingAmount());
                    break;
                }
            }
        }

        $quote->setCouponCode($couponCode);

        $quote->setTotalsCollectedFlag(false);
        $quote->collectTotals();
        $discountAmount = $quote->getShippingAddress()->getDiscountAmount();
        if ($couponCode && !$discountAmount) {
            $this->messageManager->addErrorMessage(
                __('Coupon cannot be applied due to required coupon conditions.')
            );

            return $resultRedirect;
        }
        $quote->save();

        $order->setGrandTotal($quote->getGrandTotal());
        $order->setBaseGrandTotal($quote->getBaseGrandTotal());
        $order->setShippingAmount($quote->getShippingAddress()->getShippingAmount());
        $order->setSubtotal($quote->getSubtotal());
        $order->setBaseSubtotal($quote->getBaseSubtotal());
        $order->setCouponCode($couponCode);
        $order->setDiscountDescription($couponCode);
        $order->setDiscountAmount($discountAmount);
        $order->save();

        $this->updateOrderItemsDiscountAmount($quote, $order);

        $this->messageManager->addSuccessMessage(
            __('Coupon has been updated.')
        );

        return $resultRedirect;
    }

    /**
     * Update Order Items Discount Amount.
     *
     * @param Quote $quote
     * @param Order $order
     */
    private function updateOrderItemsDiscountAmount(Quote $quote, Order $order)
    {
        $quoteItems = $quote->getItemsCollection() ?: [];
        $orderItems = $order->getItemsCollection() ?: [];

        foreach ($orderItems as $orderItem) {
            foreach ($quoteItems as $quoteItem) {
                if ($orderItem->getQuoteItemId() === $quoteItem->getItemId()) {
                    $orderItem
                        ->setDiscountAmount($quoteItem->getDiscountAmount())
                        ->setDiscountPercent($quoteItem->getDiscountPercent())
                        ->save();
                }
            }
        }
    }
}
