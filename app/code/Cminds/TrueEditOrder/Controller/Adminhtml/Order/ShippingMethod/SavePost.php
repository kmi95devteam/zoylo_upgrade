<?php

namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order\ShippingMethod;

use Cminds\TrueEditOrder\Model\Config;
use Cminds\TrueEditOrder\Model\Order\EditValidator;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Shipping\Model\Config as ShippingConfig;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteFactory;
use Magento\Sales\Model\OrderFactory;

/**
 * Cminds TrueEditOrder admin order shipping method save controller.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class SavePost extends Action
{
    /**
     * Order factory object.
     *
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * Quote factory object.
     *
     * @var QuoteFactory
     */
    protected $quoteFactory;

    /**
     * Module config object.
     *
     * @var Config
     */
    protected $moduleConfig;

    /**
     * Order edit validator.
     *
     * @var EditValidator
     */
    protected $editValidator;

    /**
     * Shipping Model Config.
     *
     * @var ShippingConfig
     */
    protected $shippingModelConfig;

    /**
     * Scope Config.
     *
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * SavePost constructor.
     *
     * @param Context $context
     * @param OrderFactory $orderFactory
     * @param QuoteFactory $quoteFactory
     * @param Config $moduleConfig
     * @param EditValidator $editValidator
     * @param ShippingConfig $shippingModelConfig
     */
    public function __construct(
        Context $context,
        OrderFactory $orderFactory,
        QuoteFactory $quoteFactory,
        Config $moduleConfig,
        EditValidator $editValidator,
        ShippingConfig $shippingModelConfig,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->orderFactory = $orderFactory;
        $this->quoteFactory = $quoteFactory;
        $this->moduleConfig = $moduleConfig;
        $this->editValidator = $editValidator;
        $this->shippingModelConfig = $shippingModelConfig;
        $this->scopeConfig = $scopeConfig;

        parent::__construct($context);
    }

    /**
     * Dispatch request.
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $orderId = $this
            ->getRequest()
            ->getParam('order_id');

        $shippingMethodCode = $this
            ->getRequest()
            ->getParam('shipping_method');

        $customShippingPrice = $this
            ->getRequest()
            ->getParam('custom_shipping_price');

        $customShippingPrice = str_replace(',', '.', $customShippingPrice);
        $useCustomShippingPrice = $this
            ->getRequest()
            ->getParam('use_custom_shipping_price');

        $resultRedirect = $this->resultRedirectFactory->create()
            ->setPath('sales/order/view', ['order_id' => $orderId]);

        $isPost = $this
            ->getRequest()
            ->isPost();
        if ($isPost === false) {
            return $resultRedirect;
        }

        if ($this->moduleConfig->isActive() === false
            || $this->moduleConfig->isOrderShippingMethodEditionEnabled() === false
        ) {
            return $resultRedirect;
        }

        $aclResource = 'Cminds_TrueEditOrder::edit_order_shipping_method';
        if ($this->_isAllowed($aclResource) === false) {
            return $resultRedirect;
        }

        if (empty($shippingMethodCode)) {
            $this->messageManager->addErrorMessage(
                __('Shipping method has been not selected.')
            );

            return $resultRedirect;
        }

        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderFactory->create()
            ->load($orderId);


        //This validation can be placed in the observer on the quote save.
        if (!$this->editValidator->validateStatus($order)) {
            $this->messageManager->addErrorMessage(
                __('Modifying orders with %1 status is not allowed.', $order->getStatus())
            );

            return $resultRedirect;
        }

        $quoteId = $order->getQuoteId();
        $quote = $this->quoteFactory->create()
            ->load($quoteId);

        $shippingAddress = $quote->getShippingAddress();

        //
        // Collect quote totals fills address data with free_method_weight data
        // which is required to make table rate method available
        // Logic is done in Magento\Quote\Model\Quote\Address\Total\Shipping collect() method
        //
        $quote->collectTotals();

        $oldShippingMethod = $shippingAddress->getShippingDescription();
        $shippingAddress
            ->setShippingMethod($shippingMethodCode)
            ->setCollectShippingRates(true)
            ->collectShippingRates();

        // if we do not want to use custom price
        $shippingAmount = ( $customShippingPrice === '' || !$useCustomShippingPrice ) ?
            $shippingAddress->getShippingAmount() :
            floatval($customShippingPrice);
        $defaultShippingAmount = $shippingAddress->getShippingAmount();

        if( $defaultShippingAmount != $shippingAmount ) {
            // set custom shipping price    
            $quote
                ->getShippingAddress()
                ->setShippingAmount( $shippingAmount );
            $shippingRates = $quote
                ->getShippingAddress()
                ->getAllShippingRates();

            foreach ($shippingRates as $rate) {
                // find selected rate
                if( $rate->getCode() === $shippingMethodCode ){
                    // change rates price
                    $rate->setPrice( $shippingAmount );
                    
                    break;
                }
            }
        }
        $quote->setTotalsCollectedFlag(false);
        $quote->collectTotals();
        $quote->save();

        $shippingMethodDescription = $this->getShippingMethodDescription($shippingMethodCode);

        $order->setShippingMethod($shippingMethodCode);
        $order->setShippingDescription($shippingMethodDescription);

        $order->setBaseShippingAmount( $defaultShippingAmount );
        $order->setShippingAmount($shippingAmount);

        $order->setGrandTotal( $quote->getGrandTotal() );
        $order->setBaseGrandTotal($quote->getBaseGrandTotal());

        $order->setSubtotal($quote->getSubtotal());
        $order->setBaseSubtotal($quote->getBaseSubtotal());

        $order->setDiscountAmount( 
            $quote
                ->getShippingAddress()
                ->getDiscountAmount() 
        );
        $order->setBaseDiscountAmount( 
            $quote
                ->getShippingAddress()
                ->getBaseDiscountAmount()
        );

        unset(
            $aclResource,
            $orderId,
            $quote,
            $quoteId,
            $shippingAddress,
            $shippingMethodCode
        );

        $order->save();

        $this->_eventManager->dispatch(
            'cminds_trueeditorder_order_edit_shipping_method_update',
            [
                'order' => $order,
                'old_shipping_method' => $oldShippingMethod,
                'new_shipping_method' => $order->getShippingDescription()
            ]
        );

        $this->messageManager->addSuccessMessage(
            __('Shipping method has been updated.')
        );

        return $resultRedirect;
    }

    /**
     * Get Shipping Method Description.
     *
     * @param $shippingMethodCode
     *
     * @return string
     */
    private function getShippingMethodDescription($shippingMethodCode)
    {
        $activeCarriers = $this->shippingModelConfig->getActiveCarriers() ?: [];
        foreach($activeCarriers as $carrierCode => $carrierModel) {
            $carrierMethods = $carrierModel->getAllowedMethods() ?: [];
            if ($carrierMethods) {
                foreach ($carrierMethods as $methodCode => $method) {
                    $code = $carrierCode . '_' . $methodCode;
                    if ($code !== $shippingMethodCode) {
                        continue;
                    }

                    $carrierTitle = $this->scopeConfig->getValue('carriers/' . $carrierCode . '/title');
                    $shippingDescription = $carrierTitle . ' - ' . $method;

                    return trim($shippingDescription, ' -');
                }
            }
        }

        return '';
    }
}
