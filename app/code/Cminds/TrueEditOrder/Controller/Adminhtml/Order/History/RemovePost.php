<?php

namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order\History;

use Cminds\TrueEditOrder\Model\Config;
use Cminds\TrueEditOrder\Model\Order\EditValidator;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Sales\Model\Order\Status\HistoryFactory;
use Magento\Sales\Model\OrderFactory;

/**
 * Cminds TrueEditOrder admin order history remove controller.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class RemovePost extends Action
{
    /**
     * Module config object.
     *
     * @var Config
     */
    protected $moduleConfig;

    /**
     * History factory object.
     *
     * @var HistoryFactory
     */
    protected $historyFactory;

    /**
     * Order factory object.
     *
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * Order edit validator.
     *
     * @var EditValidator
     */
    protected $editValidator;

    /**
     * Object constructor.
     *
     * @param Context        $context        Context object.
     * @param Config         $moduleConfig   Module config object.
     * @param HistoryFactory $historyFactory History factory object.
     * @param OrderFactory   $orderFactory   Order factory object.
     * @param EditValidator  $editValidator   Order edit validator.
     */
    public function __construct(
        Context $context,
        Config $moduleConfig,
        HistoryFactory $historyFactory,
        OrderFactory $orderFactory,
        EditValidator $editValidator
    ) {
        $this->moduleConfig = $moduleConfig;
        $this->historyFactory = $historyFactory;
        $this->orderFactory = $orderFactory;
        $this->editValidator = $editValidator;

        parent::__construct($context);
    }

    /**
     * Dispatch request.
     *
     * @return \Magento\Framework\Controller\ResultInterface|\Magento\Framework\App\ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        // TODO: Handle remove as post action.
        $noteId = $this->getRequest()->getParam('note_id');

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());

        // TODO: Check if request is post action.

        if ($this->moduleConfig->isActive() === false
            || $this->moduleConfig->isOrderNotesRemovingEnabled() === false
        ) {
            return $resultRedirect;
        }

        $aclResource = 'Cminds_TrueEditOrder::remove_order_notes';
        if ($this->_isAllowed($aclResource) === false) {
            return $resultRedirect;
        }

        $history = $this->historyFactory->create()->load($noteId);
        if ($history->getId()) {
            $order = $this->orderFactory->create()->load($history->getParentId());

            if (!$this->editValidator->validateStatus($order)) {
                $this->messageManager->addErrorMessage(
                    __('Modyfying orders with %1 status is not allowed.', $order->getStatus())
                );

                return $resultRedirect;
            }

            $history->delete();

            $this->messageManager->addSuccessMessage(
                __('Order note has been removed.')
            );
        } else {
            $this->messageManager->addErrorMessage(
                __('Order note does not exists.')
            );
        }

        return $resultRedirect;
    }
}
