<?php

namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order\Items;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Model\Order\ItemRepository as OrderItemRepository;
use Magento\Catalog\Helper\Product;
use Magento\Framework\Escaper;

class ConfigureOrderItems extends \Magento\Sales\Controller\Adminhtml\Order\Create
{
    /**
     * @var OrderItemRepository
     */
    private $orderItemRepository;

    /**
     * ConfigureQuoteItems constructor.
     *
     * @param Action\Context $context
     * @param Product $productHelper
     * @param Escaper $escaper
     * @param PageFactory $resultPageFactory
     * @param ForwardFactory $resultForwardFactory
     * @param OrderItemRepository $orderItemRepository
     */
    public function __construct(
        Action\Context $context,
        Product $productHelper,
        Escaper $escaper,
        PageFactory $resultPageFactory,
        ForwardFactory $resultForwardFactory,
        OrderItemRepository $orderItemRepository
    ) {
        $this->orderItemRepository = $orderItemRepository;

        parent::__construct($context, $productHelper, $escaper, $resultPageFactory, $resultForwardFactory);
    }

    /**
     * Ajax handler to response configuration fieldset of composite product in order items
     *
     * @return \Magento\Framework\View\Result\Layout
     */
    public function execute()
    {
        // Prepare data
        $configureResult = new \Magento\Framework\DataObject();
        try {
            $orderItemId = (int)$this->getRequest()->getParam('id');
            $orderItem = $this->orderItemRepository->get($orderItemId);
            $quoteItemId = $orderItem->getQuoteItemId();
            if (!$quoteItemId) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Quote item id is not received.'));
            }

            $quoteItem = $this->_objectManager->create(\Magento\Quote\Model\Quote\Item::class)->load($quoteItemId);
            if (!$quoteItem->getId()) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Quote item is not loaded.'));
            }

            $configureResult->setOk(true);
            $optionCollection = $this->_objectManager->create(\Magento\Quote\Model\Quote\Item\Option::class)
                ->getCollection()
                ->addItemFilter([$quoteItemId]);
            $quoteItem->setOptions($optionCollection->getOptionsByItem($quoteItem));

            $configureResult->setBuyRequest($quoteItem->getBuyRequest());
            $configureResult->setCurrentStoreId($quoteItem->getStoreId());
            $configureResult->setProductId($quoteItem->getProductId());
            $sessionQuote = $this->_objectManager->get(\Magento\Backend\Model\Session\Quote::class);
            $configureResult->setCurrentCustomerId($sessionQuote->getCustomerId());
        } catch (\Exception $e) {
            $configureResult->setError(true);
            $configureResult->setMessage($e->getMessage());
        }

        // Render page
        /** @var \Magento\Catalog\Helper\Product\Composite $helper */
        $helper = $this->_objectManager->get(\Magento\Catalog\Helper\Product\Composite::class);

        return $helper->renderConfigureResult($configureResult);
    }
}
