<?php

namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order\Items;

use Cminds\TrueEditOrder\Model\Config;
use Cminds\TrueEditOrder\Model\Order\EditValidator;
use Cminds\TrueEditOrder\Model\Order\Tax;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteFactory;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Sales\Model\Order\ItemFactory as OrderItemFactory;
use Magento\Quote\Model\Quote\ItemFactory as QuoteItemFactory;
use Magento\CatalogInventory\Api\StockRegistryInterface as StockRegistry;
use Magento\Framework\App\Config\ScopeConfigInterface as ScopeConfig;

/**
 * Cminds TrueEditOrder admin order items remove controller.
 *
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 * @author   Piotr Pierzak <piotrek.pierzak@gmail.com>
 */
class RemovePost extends Action
{
    /**
     * Order factory object.
     *
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * Quote factory object.
     *
     * @var QuoteFactory
     */
    protected $quoteFactory;

    /**
     * Module config object.
     *
     * @var Config
     */
    protected $moduleConfig;

    /**
     * Order item factory object.
     *
     * @var OrderItemFactory
     */
    protected $orderItemFactory;

    /**
     * Quote item factory object.
     *
     * @var QuoteItemFactory
     */
    protected $quoteItemFactory;

    /**
     * Order edit validator.
     *
     * @var EditValidator
     */
    protected $editValidator;

    /**
     * @var Tax
     */
    protected $cmindsTax;

    /**
     * @var StockRegistry
     */
    protected $stockRegistry;

    /**
     * @var ScopeConfig
     */
    protected $scopeConfig;


    /**
     * Object constructor.
     *
     * @param Context $context Context object.
     * @param OrderFactory $orderFactory Order factory object.
     * @param QuoteFactory $quoteFactory Quote factory object.
     * @param Config $moduleConfig Module config object.
     * @param OrderItemFactory $orderItemFactory Order item factory object.
     * @param QuoteItemFactory $quoteItemFactory Quote item factory.
     * @param EditValidator $editValidator Order edit validator.
     * @param Tax $cmindsTax
     * @param StockRegistry $stockRegistry
     */

    public function __construct(
        Context $context,
        OrderFactory $orderFactory,
        QuoteFactory $quoteFactory,
        Config $moduleConfig,
        OrderItemFactory $orderItemFactory,
        QuoteItemFactory $quoteItemFactory,
        EditValidator $editValidator,
        Tax $cmindsTax,
        StockRegistry  $stockRegistry,
        ScopeConfig $scopeConfig
    ) {
        $this->orderFactory = $orderFactory;
        $this->quoteFactory = $quoteFactory;
        $this->moduleConfig = $moduleConfig;
        $this->orderItemFactory = $orderItemFactory;
        $this->quoteItemFactory = $quoteItemFactory;
        $this->editValidator = $editValidator;
        $this->cmindsTax = $cmindsTax;
        $this->stockRegistry = $stockRegistry;
        $this->scopeConfig   = $scopeConfig;

        parent::__construct($context);
    }

    /**
     * Dispatch request.
     *
     * @return Redirect
     * @throws \Exception
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());

        if ($this->getRequest()->isPost() === false) {
            return $resultRedirect;
        }

        if ($this->moduleConfig->isActive() === false
            || $this->moduleConfig->isOrderItemsEditionEnabled() === false
        ) {
            return $resultRedirect;
        }

        $aclResource = 'Cminds_TrueEditOrder::edit_order_items';
        if ($this->_isAllowed($aclResource) === false) {
            return $resultRedirect;
        }

        $orderItemId = $this->getRequest()->getParam('order_item_id');
        /** @var Order\Item $orderItem */
        $orderItem = $this->orderItemFactory->create()->load($orderItemId);
        $orderId = $orderItem->getOrderId();
        /** @var Order $order */
        $order = $this->orderFactory->create()->load($orderId);

        if (!$this->editValidator->validateStatus($order)) {
            $this->messageManager->addErrorMessage(
                __('Modyfying orders with %1 status is not allowed.', $order->getStatus())
            );
            $resultRedirect->setPath('sales/order/view', ['order_id' => $orderId]);
            return $resultRedirect;
        }

        $orderItem->delete();

        $quoteItemId = $orderItem->getQuoteItemId();
        /** @var Quote\Item $quoteItem */
        $quoteItem = $this->quoteItemFactory->create()->load($quoteItemId);

        $productSku = $quoteItem->getSku();
        $productQty = $quoteItem->getQty();

        if ($quoteItem->getParentItem()) {
            $quoteItem->getParentItem()->delete();
        }
        $quoteItem->delete();

        // update product Quantity
        $this->updateProductStock($productSku, $productQty);

        $quoteId = $order->getQuoteId();
        /** @var Quote $quote */
        $quote = $this->quoteFactory->create()->load($quoteId);

        $quote->setTotalsCollectedFlag(false);
        $quote->collectTotals();
        $quote->save();
        $baseDiscount = $this->updateOrderItems($quote,$order);
                $baseDiscount = array_sum ($baseDiscount);
        $discountAmount = -1 * abs($quote->getSubtotal()-$quote->getSubtotalWithDiscount());
        $baseDiscountAmount = -1 * abs($quote->getSubtotal()-$quote->getBaseSubtotalWithDiscount());
        
        $order
            ->setGrandTotal($quote->getGrandTotal())
            ->setBaseGrandTotal($quote->getBaseGrandTotal())
            ->setSubtotal($quote->getSubtotal())
            ->setBaseSubtotal($quote->getBaseSubtotal())
           ->setDiscountAmount(-$baseDiscount)
            ->setBaseDiscountAmount(-$baseDiscount);
        $updateAction = 0;
        $checkStatus = 'order_validated_cs';
        $histories = $order->getStatusHistories();
            if (count($histories) > 0) {
                foreach ($histories as $key => $value) {
                    $orderHistroyStatus[] = $value->getStatus();
                }
                if (in_array($checkStatus, $orderHistroyStatus)) {
                    $updateAction = 1;
                }
            }
        if($updateAction == 1){
            $order->setStatus('order_modified_cs');
        }

        $this->cmindsTax->updateOrder($order);

        $order_status = $order->getStatus();
        if($order_status == 'order_placed' || $order_status == 'validation_on_hold_cs' || $order_status == 'generate_rx_cs' || $order_status == 'on_hold_cs' || $order_status == 'order_modified_cs'){
            $onlyFreePrescription = TRUE;
            foreach($order->getItems() as $eachItem)
            {
                if ($eachItem->getSku() != 'Free') // if any of the product is not a free product (prescription) then add shipping charges 
                {
                    $onlyFreePrescription = FALSE;
                }
            }
            $maximumPrice = $this->scopeConfig->getValue("carriers/ecomexpress/shipping_subtotal");
            $shippingHandlingCharges = 0;
            if($order->getSubtotal() < $maximumPrice && $onlyFreePrescription == FALSE)
            {
                $shippingHandlingCharges = $this->scopeConfig->getValue("carriers/ecomexpress/handling_charge");
            }
            $order->setShippingAmount($shippingHandlingCharges);
            $order->setBaseShippingAmount($shippingHandlingCharges);
            $order->setGrandTotal($order->getSubtotal()+$order->getDiscountAmount()+$shippingHandlingCharges);
            $order->setBaseGrandTotal($order->getSubtotal()+$order->getDiscountAmount()+$shippingHandlingCharges);
        }
        $order->save();

        $this->_eventManager->dispatch(
            'cminds_trueeditorder_order_edit_remove_items_after',
            [
                'order' => $order,
                'item' => $orderItem,
            ]
        );

        $this->_eventManager->dispatch(
            'sales_order_edit',
            [
                'order' => $order
            ]
        );
        
        $this->messageManager->addSuccessMessage(
            __('Products has been removed from order.')
        );

        $resultRedirect->setPath('sales/order/view', ['order_id' => $orderId]);
        return $resultRedirect;
    }

    /**
     * For Update stock of product
     *
     * @param string $productSku which stock you want to update
     * @param int $productQty your updated data
     * @return void
     */
    protected function updateProductStock($productSku, $productQty)
    {
        $stockItem = $this->stockRegistry->getStockItemBySku($productSku);
        $qty = $stockItem->getQty() + $productQty;
        $stockItem->setQty($qty);
        $stockItem->setIsInStock((bool)$qty);
        $this->stockRegistry->updateStockItemBySku($productSku, $stockItem);
    }
 public function updateOrderItems($quote, $order) {
        $orderDiscountAmount = array();
        foreach ($order->getAllItems() as $orderItem) {
            $quoteItem = $quote->getItemById($orderItem->getQuoteItemId());
//            $dummyOrderItem = $this->toOrderItem->convert($quoteItem, []);
            //  echo $orderItem->getQuoteItemId();echo $quoteItem->getQty();die;
            $percent = $orderItem->getDiscountPercent();
            $discountAmount  = ($quoteItem->getPrice() * $percent)/100;
            $discountAmount = $discountAmount * $quoteItem->getQty();
            $buyRequest = $orderItem->getBuyRequest()->setQty($quoteItem->getQty());
            $orderItem->setName($quoteItem->getName());
            $orderItem->setDescription($quoteItem->getDescription());
            $orderItem->setQtyOrdered($quoteItem->getQty());
            $orderItem->setBuyRequest($buyRequest);
            $orderItem->setPrice($quoteItem->getPrice());
            $orderItem->setBasePrice($quoteItem->getBasePrice());
            $orderItem->setRowTotal($quoteItem->getRowTotal());
            $orderItem->setBaseRowTotal($quoteItem->getBaseRowTotal());
            $orderItem->setRowTotalInclTax($quoteItem->getRowTotalInclTax());
            $orderItem->setBaseRowTotalInclTax($quoteItem->getBaseRowTotalInclTax());
            $orderItem->setTaxAmount($quoteItem->getTaxAmount());
            $orderItem->setProductOptions($quoteItem->getProductOptions());
             $orderItem->setDiscountAmount($discountAmount);

            $this->saveTransaction->addObject($orderItem);
            $orderDiscountAmount[] = $discountAmount;
        }
        return $orderDiscountAmount;
    }
}

