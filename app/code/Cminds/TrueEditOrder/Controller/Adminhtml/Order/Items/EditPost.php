<?php

namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order\Items;

use Cminds\TrueEditOrder\Helper\OrderEdit;
use Magento\Backend\App\Action;
use Cminds\TrueEditOrder\Model\Config;
use Cminds\TrueEditOrder\Model\Order\EditValidator;
use Magento\Sales\Model\OrderRepository;
use Magento\Quote\Model\QuoteRepository;
use Magento\Sales\Model\Order\ItemRepository as OrderItemRepository;
use Magento\Quote\Model\Quote\Item\ToOrderItem as ToOrderItemConverter;
use Magento\Framework\DB\TransactionFactory;
use Cminds\TrueEditOrder\Model\Order\Tax as TaxProcessor;
use Magento\Catalog\Model\Product\Type\AbstractType;
use Magento\Framework\DataObjectFactory;
use Magento\CatalogInventory\Api\StockRegistryInterface as StockRegistry;
use Magento\Framework\App\Config\ScopeConfigInterface as ScopeConfig;

class EditPost extends Action
{
    /**
     * @var Config
     */
    private $moduleConfig;

    /**
     * Order edit validator.
     *
     * @var EditValidator
     */
    private $editValidator;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var QuoteRepository
     */
    private $quoteRepository;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepository;

    /**
     * @var ToOrderItemConverter
     */
    private $toOrderItem;

    /**
     * @var \Magento\Framework\DB\Transaction
     */
    private $saveTransaction;

    /**
     * @var TaxProcessor
     */
    private $taxProcessor;

    /**
     * @var array
     */
    private $editedItems = [];

    /**
     * @var DataObjectFactory
     */
    private $dataObjectFactory;

    /**
     * @var OrderEdit
     */
    protected $editHelper;

    /**
     * @var StockRegistry
     */
    protected $stockRegistry;

    /**
     *
     * @var type@var ScopeConfig 
     */
    protected $scopeConfig;
    
    /**
     * @param Action\Context $context
     * @param Config $moduleConfig
     * @param EditValidator $editValidator
     * @param OrderRepository $orderRepository
     * @param QuoteRepository $quoteRepository
     * @param OrderItemRepository $orderItemRepository
     * @param ToOrderItemConverter $toOrderItem
     * @param TransactionFactory $transactionFactory
     * @param TaxProcessor $taxProcessor
     * @param DataObjectFactory $dataObjectFactory
     * @param OrderEdit $editHelper
     * @param StockRegistry $stockRegistry
     */
    public function __construct(
        Action\Context $context,
        Config $moduleConfig,
        EditValidator $editValidator,
        OrderRepository $orderRepository,
        QuoteRepository $quoteRepository,
        OrderItemRepository $orderItemRepository,
        ToOrderItemConverter $toOrderItem,
        TransactionFactory $transactionFactory,
        TaxProcessor $taxProcessor,
        DataObjectFactory $dataObjectFactory,
        OrderEdit $editHelper,
        StockRegistry  $stockRegistry,
        ScopeConfig $scopeConfig
    ) {
        $this->moduleConfig = $moduleConfig;
        $this->editValidator = $editValidator;
        $this->orderRepository = $orderRepository;
        $this->quoteRepository = $quoteRepository;
        $this->orderItemRepository = $orderItemRepository;
        $this->toOrderItem = $toOrderItem;
        $this->saveTransaction = $transactionFactory->create();
        $this->taxProcessor = $taxProcessor;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->editHelper = $editHelper;
        $this->stockRegistry = $stockRegistry;
        $this->scopeConfig = $scopeConfig;

        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $products = $this->getRequest()->getPost('item');
        $orderId = $this->getRequest()->getParam('order_id');

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('sales/order/view', ['order_id' => $orderId]);

        if ($this->validate($products) === false) {
            return $resultRedirect;
        }

        $order = $this->orderRepository->get($orderId);
        $oldGrandTotal = $order->getGrandTotal();
        if (!$this->editValidator->validateStatus($order)) {
            $this->messageManager->addErrorMessage(
                __('Modyfying orders with %1 status is not allowed.', $order->getStatus())
            );

            return $resultRedirect;
        }

        try {
            $quote = $this->updateQuote($order, $products);
            $baseDiscount = $this->updateOrderItems($quote);
            $baseDiscount = array_sum ($baseDiscount);
            $newGrandTotal = $quote->getGrandTotal();
            if ($oldGrandTotal > $newGrandTotal) {
                if ($this->moduleConfig->needToSendRefundMessage()) {
                    $customerEmail = $order->getCustomerEmail();
                    $this->editHelper->sendRefundMessage(
                        $this->moduleConfig->getRefundMessageTemplate(),
                        $customerEmail,
                        $order->getIncrementId(),
                        $oldGrandTotal,
                        $newGrandTotal
                    );
                }
                //@TODO refund process
            }

            $discountAmount = -1 * abs($quote->getSubtotal()-$quote->getSubtotalWithDiscount());
            $baseDiscountAmount = -1 * abs($quote->getSubtotal()-$quote->getBaseSubtotalWithDiscount());

            $order
                ->setGrandTotal($quote->getGrandTotal())
                ->setBaseGrandTotal($quote->getBaseGrandTotal())
                ->setSubtotal($quote->getSubtotal())
                ->setBaseSubtotal($quote->getBaseSubtotal())
                ->setDiscountAmount(-$baseDiscount)
                ->setBaseDiscountAmount(-$baseDiscount);
            $updateAction = 0;
            $checkStatus = 'order_validated_cs';
            $histories = $order->getStatusHistories();
                if (count($histories) > 0) {
                    foreach ($histories as $key => $value) {
                        $orderHistroyStatus[] = $value->getStatus();
                    }
                    if (in_array($checkStatus, $orderHistroyStatus)) {
                        $updateAction = 1;
                    }
                }
            if($updateAction == 1){
                $order->setStatus('order_modified_cs');
            }
            
            $order_status = $order->getStatus();
            if($order_status == 'order_placed' || $order_status == 'validation_on_hold_cs' || $order_status == 'generate_rx_cs' || $order_status == 'on_hold_cs' || $order_status == 'order_modified_cs'){
                $onlyFreePrescription = TRUE;
                foreach($order->getItems() as $eachItem)
                {
                    if ($eachItem->getSku() != 'Free') // if any of the product is not a free product (prescription) then add shipping charges 
                    {
                        $onlyFreePrescription = FALSE;
                    }
                }
                $maximumPrice = $this->scopeConfig->getValue("carriers/ecomexpress/shipping_subtotal");
                $shippingHandlingCharges = 0;
                $shippingHandlingChargesSet = 0;
                if($order->getSubtotal() < $maximumPrice && $onlyFreePrescription == FALSE)
                {
                    $shippingHandlingCharges = $this->scopeConfig->getValue("carriers/ecomexpress/handling_charge");
                    $shippingHandlingChargesSet = $this->scopeConfig->getValue("carriers/ecomexpress/handling_charge");
                }
                $order->setShippingAmount($shippingHandlingCharges);
                $order->setBaseShippingAmount($shippingHandlingCharges);
                $order->setGrandTotal($order->getSubtotal()+$order->getDiscountAmount()+$shippingHandlingCharges);
                $order->setBaseGrandTotal($order->getSubtotal()+$order->getDiscountAmount()+$shippingHandlingCharges);
            }
            $this->saveTransaction->addObject($order);
            $this->saveTransaction->save();
            $this->taxProcessor->updateTax($quote, $order);

            $this->_eventManager->dispatch(
                'cminds_trueeditorder_order_edit_order_items_after',
                [
                    'order' => $order,
                    'edited_items' => $this->editedItems,
                ]
            );
            $this->_eventManager->dispatch(
                'sales_order_edit',
                [
                    'order' => $order
                ]
            );

        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __($e->getMessage())
            );
        }

        return $resultRedirect;
    }

    /**
     * Validate edit availability.
     *
     * @param array $products
     *
     * @return bool
     */
    public function validate($products)
    {
        if ($this->getRequest()->isPost() === false) {
            return false;
        }

        if ($this->moduleConfig->isActive() === false
            || $this->moduleConfig->isOrderItemsEditionEnabled() === false
        ) {
            return false;
        }

        $aclResource = 'Cminds_TrueEditOrder::edit_order_items';
        if ($this->_isAllowed($aclResource) === false) {
            return false;
        }

        if (empty($products)) {
            $this->messageManager->addErrorMessage(
                __('No products has been selected.')
            );

            return false;
        }

        return true;
    }

    /**
     * Update quote.
     *
     * @param $order
     * @param $products
     *
     * @return \Magento\Quote\Api\Data\CartInterface|\Magento\Quote\Model\Quote
     */
    public function updateQuote($order, $products)
    {
        $quoteId = $order->getQuoteId();
        $quote = $this->quoteRepository->get($quoteId);

        foreach ($products as $orderItemId => $data) {
          
            $edited = false;
            /** @var \Magento\Sales\Model\Order\Item $orderItem */
            $orderItem = $order->getItemById($orderItemId);
            $quoteItem = $quote->getItemById($orderItem->getQuoteItemId());
            if ($quoteItem){
            if (isset($data['name']) && $data['name'] !== $quoteItem->getName()) {
                $quoteItem->setName($data['name']);
                $edited = true;
            }

            if (isset($data['description']) && $data['description'] !== $quoteItem->getDescription()) {
                $quoteItem->setDescription($data['description']);
                $edited = true;
            }

            if (isset($data['options'])) {
               // $this->updateCustomOptions($quoteItem, $data);
                $edited = true;
            }

            if (isset($data['custom_price']) && $quoteItem->getPrice() != $data['custom_price']) {
                $quoteItem->setCustomPrice($data['custom_price']);
                $quoteItem->setOriginalCustomPrice($data['custom_price']);
                $edited = true;
            }

            if (!isset($data['custom_price'])
                && ($quoteItem->getCustomPrice() || $quoteItem->getOriginalCustomPrice())
            ) {
                $quoteItem->setCustomPrice(null);
                $quoteItem->setOriginalCustomPrice(null);
                $edited = true;
            }

            if ($quoteItem->getQty() != $data['qty']) {
                $productSku = $orderItem->getSku();
                $oldQty = $quoteItem->getQty();
                $newQty = $data['qty'];
				$order_status = $order->getStatus();
				if($order_status != 'order_placed' && $order_status != 'validation_on_hold_cs' && $order_status != 'generate_rx_cs' && $order_status != 'on_hold_cs' && $order_status != 'order_modified_cs'){
    				if($newQty <= $oldQty){
                        // update product Quantity
                        $this->updateProductStock($productSku, $oldQty, $newQty);
                        $quoteItem->setQty($data['qty']);
                        $edited = true;
    				}else{
    					 $this->messageManager->addErrorMessage(
    						__('You are not allowed to increase the quantity.')
    					);
    				}
				}else{
					// update product Quantity
					$this->updateProductStock($productSku, $oldQty, $newQty);

					$quoteItem->setQty($data['qty']);
					$edited = true;
				}
            }

            if ($edited) {
                $this->editedItems[] = $orderItem;
                $quoteItem->save();
            }
            }
        }
        // check if order is assigned to a customer and this customer exists
        if( $quote->getBillingAddress() 
            // && $quote->getCustomerFirstname() // not registered customers do not have this value assigned
            && $quote->getCustomerGroupId() != 0 // default id for not logged in group is 0
            && !$quote->getCustomer()->getId() 
        ){
            $quote->getBillingAddress()->setCustomerAddressId(null);
            $quote->getBillingAddress()->setCustomerId(null);
        }

        $this->quoteRepository->save($quote);
        $quote->collectTotals();
        $quote->save();

        return $quote;
    }

    /**
     * Use updated quote to create a dummy order items and set required data fields for existed ones.
     *
     * @param $quote
     */
    public function updateOrderItems($quote)
    {
        foreach ($this->editedItems as $orderItem) {
            $quoteItem = $quote->getItemById($orderItem->getQuoteItemId());
            $dummyOrderItem = $this->toOrderItem->convert($quoteItem, []);
            $percent = $orderItem->getDiscountPercent();
            $discountAmount  = ($quoteItem->getPrice() * $percent)/100;
            $discountAmount = $discountAmount * $dummyOrderItem->getQtyOrdered();
            $buyRequest = $orderItem->getBuyRequest()->setQty($dummyOrderItem->getQtyOrdered());
            $orderItem->setName($dummyOrderItem->getName());
            $orderItem->setDescription($dummyOrderItem->getDescription());
            $orderItem->setQtyOrdered($dummyOrderItem->getQtyOrdered());
            $orderItem->setBuyRequest($buyRequest);
            $orderItem->setPrice($dummyOrderItem->getPrice());
            $orderItem->setBasePrice($dummyOrderItem->getBasePrice());
            $orderItem->setRowTotal($dummyOrderItem->getRowTotal());
            $orderItem->setBaseRowTotal($dummyOrderItem->getBaseRowTotal());
            $orderItem->setRowTotalInclTax($dummyOrderItem->getRowTotalInclTax());
            $orderItem->setBaseRowTotalInclTax($dummyOrderItem->getBaseRowTotalInclTax());
            $orderItem->setTaxAmount($dummyOrderItem->getTaxAmount());
            $orderItem->setProductOptions($dummyOrderItem->getProductOptions());
           $orderItem->setDiscountAmount($discountAmount);
            $this->saveTransaction->addObject($orderItem);
            $orderDiscountAmount[] = $discountAmount;
            
        }
        
        return $orderDiscountAmount;
    }

    /**
     * Update custom options of the quote item, use dummy quote item to get custom option values.
     *
     * @param \Magento\Quote\Model\Quote\Item $quoteItem
     * @param array $data
     *
     * @return string|EditPost
     */
    public function updateCustomOptions($quoteItem, $data)
    {
        $product = $quoteItem->getProduct();
        $config = $this->dataObjectFactory->create()->setData($data);
        $dummyCartCandidate = $product->getTypeInstance()->prepareForCartAdvanced(
            $config,
            $quoteItem->getProduct(),
            AbstractType::PROCESS_MODE_FULL
        );

        /**
         * Error message
         */
        if (is_string($dummyCartCandidate) || $dummyCartCandidate instanceof \Magento\Framework\Phrase) {
            return strval($dummyCartCandidate);
        }

        /**
         * If prepare process return one object
         */
        if (!is_array($dummyCartCandidate)) {
            $dummyCartCandidate = [$dummyCartCandidate];
        }

        foreach ($dummyCartCandidate as $candidate) {
            if ($candidate->getParentProductId()) {
                continue;
            }
            $quoteItem->setOptions($candidate->getCustomOptions());
            $quoteItem->saveItemOptions();
        }

        return $this;
    }

    /**
     * For Update stock of product
     *
     * @param string $productSku which stock you want to update
     * @param int $productQty your updated data
     * @return void
     */
    protected function updateProductStock($productSku, $oldQty, $newQty)
    {
        $stockItem = $this->stockRegistry->getStockItemBySku($productSku);
        $qty = $stockItem->getQty() + $oldQty - $newQty;
        $stockItem->setQty($qty);
        $stockItem->setIsInStock((bool)$qty);
        $this->stockRegistry->updateStockItemBySku($productSku, $stockItem);
    }
}
