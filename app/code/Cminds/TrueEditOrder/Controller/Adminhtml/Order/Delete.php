<?php

namespace Cminds\TrueEditOrder\Controller\Adminhtml\Order;

use Magento\Sales\Controller\Adminhtml\Order;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Psr\Log\LoggerInterface;
use Cminds\TrueEditOrder\Model\Config as ModuleConfig;
use Cminds\TrueEditOrder\Model\Order\Delete as OrderDeleteModel;

/**
 *  Cminds TrueEditOrder admin order delete controller.
 * 
 * @category Cminds
 * @package  Cminds_TrueEditOrder
 */
class Delete extends Order
{
    /**
     * @var \Cminds\TrueEditOrder\Model\Config
     */
    protected $config;
    
    /**
     * @var \Cminds\TrueEditOrder\Model\Order\Delete
     */
    protected $deleteModel;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\Translate\InlineInterface $translateInline
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param OrderManagementInterface $orderManagement
     * @param OrderRepositoryInterface $orderRepository
     * @param LoggerInterface $logger
     * @param ModuleConfig $moduleConfig
     * @param OrderDeleteModel $orderDeleteModel
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Translate\InlineInterface $translateInline,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        OrderManagementInterface $orderManagement,
        OrderRepositoryInterface $orderRepository,
        LoggerInterface $logger,
        ModuleConfig $moduleConfig,
        OrderDeleteModel $orderDeleteModel
    ) {
        $this->config = $moduleConfig;
        $this->deleteModel = $orderDeleteModel;

        parent::__construct($context, $coreRegistry, $fileFactory, $translateInline, 
            $resultPageFactory, $resultJsonFactory, $resultLayoutFactory, $resultRawFactory, 
            $orderManagement, $orderRepository, $logger );
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        if ( !$this->config->isActive() 
            || !$this->config->isOrderDeletionEnabled()
            || !$this->isValidPostRequest()
        ) {
            $this->messageManager->addError(__('Cannot delete the order.'));
            return $resultRedirect->setPath('sales/order/view', 
                ['order_id' => $this->getRequest()->getParam('order_id')]);
        }

        $orderObject = $this->_initOrder();
        if ( $orderObject ) {
            try {
                $message = __('Order #' . $orderObject->getIncrementId() . ' has been deleted.');
                $this->deleteModel->deleteOrder($orderObject);
                $this->messageManager->addSuccessMessage($message);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('sales/order/view', ['order_id' => $orderObject->getId()]);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('An error occurred while deleting the order. Please try again later.'));
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                return $resultRedirect->setPath('sales/order/view', ['order_id' => $orderObject->getId()]);
            }
        }

        return $resultRedirect->setPath('sales/*/');
    }
}
