<?php
namespace Zoylo\BusinessOperations\Controller\Adminhtml\Elapsed;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Tat extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return void
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Zoylo_BusinessOperations::helloworld');
        $resultPage->addBreadcrumb(__('Elapsed TAT'), __('Elapsed TAT'));
        $resultPage->addBreadcrumb(__('Elapsed TAT'), __('Elapsed TAT'));
        $resultPage->getConfig()->getTitle()->prepend(__('Elapsed TAT'));
        return $resultPage;
    }
}
