<?php
namespace Zoylo\BusinessOperations\Controller\Adminhtml\Elapsed;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class LoadOrders extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    
    protected $request;
    
    protected $resourceConnection;

    protected $urlHelper;
    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\Url $urlHelper,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->request = $request;
        $this->resourceConnection = $resourceConnection;
        $this->urlHelper = $urlHelper;
    }

    /**
     * Index action
     *
     * @return void
     */
    public function execute()
    {
        $connection = $this->resourceConnection->getConnection();
        $customerId = 0; // default value
        $orderItemsDetails = "";
        $allOrders = array();
        $allTimeRangeArr = $this->getRequest()->getParam('timerrange');
        $addedColumHeader = FALSE;
        $table = '';
        $tableTR = "";
        $totalOrdersCount = 0;
        foreach($allTimeRangeArr as $eachTimeRange)
        {
            if ($this->getRequest()->getParam('orderstatus') && $this->getRequest()->getParam('orderstatus') != '' && $this->getRequest()->getParam('timerrange') && $this->getRequest()->getParam('timerrange') != '')
            {
                $fromTimeRange = explode('-', $eachTimeRange)[0];
                $toTimeRange = explode('-', $eachTimeRange)[1];
                $DateTime1 = new \DateTime();
                if ($toTimeRange != '+')
                {
                    $fromDateTimeRange = $DateTime1->modify('-'.$toTimeRange.' hours');
                }
                $DateTime2 = new \DateTime();
                $toDateTimeRange = $DateTime2->modify('-'.$fromTimeRange.' hours');
                if ($toTimeRange == '+')
                {
                    $ordersQuery = "SELECT so.entity_id, AddTime(so.updated_at,'05:30:00') as updated_at, AddTime(so.created_at,'05:30:00') as created_at, so.increment_id , so.grand_total,so.status as order_status ,AddTime(so.created_at,'05:30:00') as order_created_at, sop.method as payment_method, so.shipping_description, so.shipping_method, so.shipping_amount,so.coupon_code,so.coupon_code,so.discount_amount,so.subtotal_incl_tax,so.shipping_address_id FROM ".$this->resourceConnection->getTableName('sales_order')." so LEFT JOIN ".$this->resourceConnection->getTableName('sales_order_payment')." sop ON so.entity_id = sop.parent_id WHERE so.status = '".$this->getRequest()->getParam('orderstatus')."' AND so.created_at <= '".$toDateTimeRange->format("Y-m-d H:i:s")."'  ";
                }
                else
                {
                    $ordersQuery = "SELECT so.entity_id, AddTime(so.updated_at,'05:30:00') as updated_at, AddTime(so.created_at,'05:30:00') as created_at, so.increment_id , so.grand_total,so.status as order_status ,AddTime(so.created_at,'05:30:00') as order_created_at, sop.method as payment_method, so.shipping_description, so.shipping_method, so.shipping_amount,so.coupon_code,so.coupon_code,so.discount_amount,so.subtotal_incl_tax,so.shipping_address_id FROM ".$this->resourceConnection->getTableName('sales_order')." so LEFT JOIN ".$this->resourceConnection->getTableName('sales_order_payment')." sop ON so.entity_id = sop.parent_id WHERE so.status = '".$this->getRequest()->getParam('orderstatus')."' AND so.created_at >= '".$fromDateTimeRange->format("Y-m-d H:i:s")."' AND so.created_at <=  '".$toDateTimeRange->format("Y-m-d H:i:s")."' ";
                }
                $allOrders = $connection->fetchAll($ordersQuery);
                $totalOrdersCount = $totalOrdersCount+count($allOrders);
                if ($addedColumHeader == FALSE)
                {
                    $table = "<h3>Number Of Orders: <span id = 'counterorder' > ".$totalOrdersCount."</span></h3><div id = 'idordercomments'></div><table border='1' style='width:100%;font-size:11px;'><thead><th>Order ID</th><th>Updated At</th><th>Created At</th><th>Ordered Items</th><th>Order Comments</th><th>Shipment Tracking Details</th><th>Shipping Charges</th><th>Shipping Address</th><th>Payment Method</th><th>Coupon</th><th>Order Total</th><th>Action</th></thead>";                                    
                    $addedColumHeader = TRUE;
                }

                foreach($allOrders as $eachOrder) // loop through all the orders to get the order items
                {
                    $allItems = []; // array to store all the order items
                    $orderItemsQuery = "SELECT name,sku,price_incl_tax,row_total_incl_tax,qty_ordered FROM ".$this->resourceConnection->getTableName('sales_order_item')." WHERE order_id = '".$eachOrder['entity_id']."' ";
                    $orderItemsResult = $connection->fetchAll($orderItemsQuery);
                    $itemCount = 0;
                    $orderItemsTable = "<table border='1' style='width:100%;' ><th>Name</th><th>Qty</th><th>Price</th>";
                    $containsItems = 0;
                    foreach($orderItemsResult as $eachItem)
                    {
                        if ($eachItem['name'] != '') // if the order items table contains any item
                        {
                            $containsItems = 1;
                        }
                        $orderItemsTable .= "<tr><td style='width:70%;' >".$eachItem['name']."</td><td style='width:15%;'>".$eachItem['qty_ordered']."</td><td style='width:15%;'>".$eachItem['row_total_incl_tax']."</td></tr>";
                        $allItems[$itemCount]['name'] = $eachItem['name']; 
                        $allItems[$itemCount]['sku']  = $eachItem['sku']; 
                        $itemCount++;
                    }  
                    $orderItemsTable .= "<tr><td colspan='3'><span style='float:right;' ><b>Subtotal:</b>".$eachOrder['subtotal_incl_tax']."</span></td></tr>";
                    $orderItemsTable .= "</table>";
                    if ($containsItems == 0) // if there where no items in the order then do not create the order items table
                    {
                        $orderItemsTable = '';
                    }
                    /*Query for the order status starts*/
                    $orderConfirmQuery = "SELECT AddTime(created_at,'05:30:00') as created_at FROM ".$this->resourceConnection->getTableName('sales_order_status_history')." WHERE parent_id = '".$eachOrder['entity_id']."' AND status = 'order_confirmed' ";
                    $orderConfirmResult = $connection->fetchAll($orderConfirmQuery);
                    /*Query for the order status ends*/

                    /*Query for the order comments starts*/
                    $orderCommentQuery  = "SELECT comment,  IF(is_customer_notified = 1, 'Notified', 'Not Notified') AS is_customer_notified, AddTime(created_at,'05:30:00') as created_at, status FROM ".$this->resourceConnection->getTableName('sales_order_status_history')." WHERE parent_id = '".$eachOrder['entity_id']."' ";
                    $orderCommentResult = $connection->fetchAll($orderCommentQuery);
                    /*Query for the order comments ends*/
                    
                    /*Query for the order status last updated starts*/
                    $orderStatusLastUpdated       = "SELECT AddTime(created_at,'05:30:00') as created_at, status FROM ".$this->resourceConnection->getTableName('sales_order_status_history')." WHERE parent_id = '".$eachOrder['entity_id']."' AND status  = '".$this->getRequest()->getParam('orderstatus')."' ORDER BY created_at DESC LIMIT 1 ";
                    $orderStatusLastUpdatedResult = $connection->fetchAll($orderStatusLastUpdated);
                    $statusUpdatedAt = '';
                    if ($orderStatusLastUpdatedResult[0]['created_at'])
                    {
                        $statusUpdatedAt = $orderStatusLastUpdatedResult[0]['created_at'];
                    }
                    /*Query for the order status last updated ends*/

                    /*Query for the order shipment comments starts*/
                    $orderShipCommentQuery  = "SELECT ssc.comment,  IF(ssc.is_customer_notified = 1, 'Notified', 'Not Notified') AS is_customer_notified, AddTime(ssc.created_at,'05:30:00') as created_at FROM ".$this->resourceConnection->getTableName('sales_shipment_comment')." ssc WHERE ssc.parent_id = (SELECT entity_id FROM  ".$this->resourceConnection->getTableName('sales_shipment')." ss WHERE ss.order_id = '".$eachOrder['entity_id']."') ";
                    $orderShipCommentResult = $connection->fetchAll($orderShipCommentQuery);
                    /*Query for the order shipment comments ends*/

                    /*Query for the order invoice comments starts*/
                    $orderInvoiceCommentQuery  = "SELECT sic.comment,  IF(sic.is_customer_notified = 1, 'Notified', 'Not Notified') AS is_customer_notified, AddTime(sic.created_at,'05:30:00') as created_at FROM ".$this->resourceConnection->getTableName('sales_invoice_comment')." sic WHERE sic.parent_id = (SELECT entity_id FROM  ".$this->resourceConnection->getTableName('sales_invoice')." si WHERE si.order_id = '".$eachOrder['entity_id']."') ";
                    $orderInvoiceCommentResult = $connection->fetchAll($orderInvoiceCommentQuery);
                    /*Query for the order invoice comments ends*/

                    /*Query for the order shipment tracking starts*/
                    $orderShipTracktQuery  = "SELECT sst.order_id, sst.track_number, sst.description, sst.title, sst.carrier_code, AddTime(sst.created_at,'05:30:00') as created_at, AddTime(sst.updated_at,'05:30:00') as updated_at FROM ".$this->resourceConnection->getTableName('sales_shipment_track')." sst WHERE sst.parent_id = (SELECT entity_id FROM  ".$this->resourceConnection->getTableName('sales_shipment')." ss WHERE ss.order_id = '".$eachOrder['entity_id']."') ";
                    $orderShipTracktResult = $connection->fetchAll($orderShipTracktQuery);
                    /*Query for the order shipment tracking ends*/

                    $orderComments = "<ul style = 'list-style-type: none;' >";
                    $orderComments .= "<li><h3>Notes for this Order</h3></li>";
                    foreach($orderCommentResult as $orderComm)
                    {
                        $orderComments .= "<li><span>".$orderComm['created_at']."</span>"." <span class = 'order-status-data'>".$orderComm['status']."</span>"." <span class  = 'order-status-data'> Customer <b>".$orderComm['is_customer_notified']."</b> </span>"."</li><div>".$orderComm['comment']."</div></br>";
                    }

                    $orderComments .= "<li><h3>Shipment Comments</h3></li>";
                    foreach($orderShipCommentResult as $orderCommShip)
                    {
                        $orderComments .= "<li><span>".$orderCommShip['created_at']."</span>"." <span class  = 'order-status-data'> Customer <b>".$orderCommShip['is_customer_notified']."</b> </span>"."</li><div>".$orderCommShip['comment']."</div></br>";
                    }
                    $orderComments .= "<li><h3>Invoice Comments</h3></li>";
                    foreach($orderInvoiceCommentResult as $orderCommInvoice)
                    {
                        $orderComments .= "<li><span>".$orderCommInvoice['created_at']."</span>"." <span class  = 'order-status-data'> Customer <b>".$orderCommInvoice['is_customer_notified']."</b> </span>"."</li><div>".$orderCommInvoice['comment']."</div></br>";
                    }

                    $orderComments .= "</ul>"; 
                    $orderShipmentTrack = "";
                    foreach ($orderShipTracktResult as $shipTracker)
                    {
                        $orderShipmentTrack .= "<p><b>Carrier:</b>".$shipTracker['carrier_code']."</p>"; 
                        $orderShipmentTrack .= "<p><b>Title:</b>".$shipTracker['title']."</p>"; 
                        $orderShipmentTrack .= "<p><b>Tracking Number:</b>".$shipTracker['track_number']."</p>";
                        $orderShipmentTrack .= "<p><b>Created at:</b>".$shipTracker['created_at']."</p>";
                        $orderShipmentTrack .= "<p><b>Updated at:</b>".$shipTracker['updated_at']."</p>";
                    }

                    /*Query for getting the customer order shipping address starts*/
                    $orderAddressQuery = "SELECT firstname, middlename, lastname, street, city, region, postcode, telephone FROM ".$this->resourceConnection->getTableName('sales_order_address')." WHERE parent_id = '".$eachOrder['entity_id']."' AND address_type = 'shipping'";
                    $orderAddressResult = $connection->fetchAll($orderAddressQuery);
                    $customerShippingAddress = "";
                    if (isset($orderAddressResult[0]))
                    $customerShippingAddress = "<p><b>".$orderAddressResult[0]['firstname']." ".$orderAddressResult[0]['lastname']." </b></p><p>".$orderAddressResult[0]['street']."</p><p>".$orderAddressResult[0]['city'].", ".$orderAddressResult[0]['region'].", ".$orderAddressResult[0]['postcode']."</p><p>India</p><p>T:".$orderAddressResult[0]['telephone']."</p>";
                    /*Query for getting the customer order shipping address ends*/
                    $orderConfirmDate = "";
                    if (!empty($orderConfirmResult))
                    {
                        if (isset($orderConfirmResult[0]['created_at']))
                        {
                            $orderConfirmDate = $orderConfirmResult[0]['created_at'];   
                        }
                    }
                    $couponData = "";
                    if ($eachOrder['coupon_code'] != '')
                    {
                        $couponData = "<p>Coupon Code:".$eachOrder['coupon_code']."</p><p>Discount Amount:".$eachOrder['discount_amount']."</p>";
                    }
                    if ($eachOrder['payment_method'] == 'free')
                    {
                        $eachOrder['payment_method'] = 'cashondelivery';
                    }

                    if ($this->getRequest()->getParam('orderstatus') == 'customer_payment_pending_cs')
                    {
                        $tableTR = $tableTR."<tr><td>".$eachOrder['increment_id']."</td><td>".$statusUpdatedAt."</td><td>".$eachOrder['created_at']."</td><td>".$orderItemsTable."</td><td><a href= '#' class = 'viewcomments' data-entityid = '".$eachOrder['entity_id']."' >View</a></td><td>".$orderShipmentTrack."</td><td><p>Shipping Cost:".$eachOrder['shipping_amount']."</p><p>Shipping Method:</p>".$eachOrder['shipping_method']."</td><td>".$customerShippingAddress."</td><td>".$eachOrder['payment_method']."</td><td>".$couponData."</td><td>".$eachOrder['grand_total']."</td><td> <a target = '_blank' href='".$this->_url->getUrl("sales/order/view/order_id/$eachOrder[entity_id]")."'> View </a> | <a href='javascript:void(0);' class = 'sendpaymentcls' data-entityid = ".$eachOrder['entity_id']." >Send Payment Link</a></td></tr><div id = 'idordercommentsdata".$eachOrder['entity_id']."' style  = 'display:none;'>".$orderComments."</div>"; 
                    }
                    else
                    {
                        $tableTR = $tableTR."<tr><td>".$eachOrder['increment_id']."</td><td>".$statusUpdatedAt."</td><td>".$eachOrder['created_at']."</td><td>".$orderItemsTable."</td><td><a href= '#' class = 'viewcomments' data-entityid = '".$eachOrder['entity_id']."' >View</a></td><td>".$orderShipmentTrack."</td><td><p>Shipping Cost:".$eachOrder['shipping_amount']."</p><p>Shipping Method:</p>".$eachOrder['shipping_method']."</td><td>".$customerShippingAddress."</td><td>".$eachOrder['payment_method']."</td><td>".$couponData."</td><td>".$eachOrder['grand_total']."</td><td> <a target = '_blank' href='".$this->_url->getUrl("sales/order/view/order_id/$eachOrder[entity_id]")."'> View </a> </td></tr><div id = 'idordercommentsdata".$eachOrder['entity_id']."' style  = 'display:none;'>".$orderComments."</div>";                     
                    }
                }
                
            }   
        }
        
        echo $table.$tableTR."</table> <input type = 'hidden' id='totalnumberoforders' value = '".$totalOrdersCount."' />"
                . "<script> require(['jquery','Magento_Ui/js/modal/modal'], function($,modal){ 
                $('.viewcomments').on('click',function(){ 
                    var orderentityid = $(this).data('entityid');
                    $('#idordercomments').html($('#idordercommentsdata'+orderentityid).html());

                    var options = {
                    type: 'popup',
                    responsive: true,
                    innerScroll: true,
                    title: 'Order Comments',
                    buttons: [{
                        text: $.mage.__('Continue'),
                        class: '',
                        click: function () {
                            this.closeModal();
                        }
                    }]
                };

                var popup = modal(options, $('#idordercomments'));

                $('#idordercomments').modal('openModal');
                });  
                $('.sendpaymentcls').on('click',function(){   
                $.ajax({
                url:'". $this->urlHelper->getUrl('csquare/index/payment')."' ,
                method:'GET',
                data: {id:$(this).data('entityid')},
                showLoader: true,
                complete: function(response)
                {
                    if(response.responseText.indexOf('true') != -1){ // we are not getting a  json response so using the find string to check if the link was sent successfully or not
                            alert('Payment Link Successfully.');
                    }
                    else
                    {
                        alert('Payment link not sent. Please check if payment link is generated.');
                    }
                },
                fail: function(response)
                {
                    alert('Failed To Send Payment Link.');
                }
            }); }); });</script>";
    }
}