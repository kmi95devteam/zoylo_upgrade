/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
var config = {
    map: {
        '*': {
            'Magento_Checkout/template/shipping-address/address-renderer/default.html': 'Zoylo_Editshipaddress/template/shipping-address/address-renderer/default.html'

        },
    }
};