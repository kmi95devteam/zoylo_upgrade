define([
    'jquery',
    'Magento_Checkout/js/action/get-totals',
    'Magento_Customer/js/customer-data',
  'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-rate-processor/new-address',
    'Magento_Checkout/js/model/shipping-rate-processor/customer-address',
    'Magento_Checkout/js/model/shipping-rate-registry',
    'Magento_Checkout/js/model/totals',
    'Magento_Checkout/js/action/get-payment-information',
    'Magento_Checkout/js/model/full-screen-loader',
    'ko',
    'Magento_Checkout/js/model/url-builder',
    'mage/url'
  
  
], function ($, getTotalsAction, customerData, quote, defaultProcessor, customerAddressProcessor, rateRegistry, totals, getPaymentInformationAction, fullScreenLoader, ko, urlBuilder, urlFormatter) {

    $(document).ready(function(){
        $(document).on('click', '.delete-ship-address-link', function(){

            if(confirm("Are you sure you want to delete this?")){
                var deleteFlag = true;
            }else{
                var deleteFlag = false;
                return false;
            }
        if(deleteFlag==true){
          var zoyloAjaxUrl = urlFormatter.build('editshipaddress/index/deleteshipaddress');
          var delete_ship_address_id = $(this).attr("id");
          if(delete_ship_address_id){
            var urlbui = zoyloAjaxUrl;
            $.ajax({
                url: urlbui,
                type: "POST",
                dataType: 'json',
                showLoader: 'true',
                data: {"ship_address_id": delete_ship_address_id},
                success: function (result) {
                  if(result.success=='true'){
                    $( "#maincontent .columns .main" ).append( result.delete_message );
                    $("#zoylo-med-shipping-address-id-"+delete_ship_address_id).empty();
                    $("#zoylo-med-shipping-address-id-"+delete_ship_address_id).hide();
                    /*deferred = $.Deferred();
                    totals.isLoading(true);
                    getPaymentInformationAction(deferred);
                    $.when(deferred).done(function () {
                        fullScreenLoader.stopLoader();
                        totals.isLoading(false);
                    });*/
                    /*setTimeout(function(){ 
                        window.location.reload();
                      }, 1000);*/
                  }else{
                    if(result.success=='false'){
                      $( "#maincontent .columns .main" ).append( result.delete_message_fail );
                      return false;
                    }
                  }
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            }); //Ajax request and Response Ends

          }
      }
          
        });
    });
});