<?php

namespace Zoylo\Editshipaddress\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
     /**
 * @var \Magento\Framework\Controller\Result\JsonFactory
 */
 protected $resultJsonFactory;
 protected $_countryCollectionFactory;
 protected $objectManager;

 /**
 * @param \Magento\Framework\App\Action\Context $context
 * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
 */
 public function __construct(
     \Magento\Framework\App\Action\Context $context,
     \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
     \Magento\Directory\Model\Country $country,
     \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory
 ) {
     $this->resultJsonFactory = $resultJsonFactory;
     $this->country = $country;
     $this->_countryCollectionFactory = $countryCollectionFactory;
     $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
     parent::__construct($context);
 }
    
    public function execute()
    {
        
        $resultJson = $this->resultJsonFactory->create();
		$shipAddressId = $this->getRequest()->getParam('ship_address_id');
                
                
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
                $address = $objectManager->get('Magento\Customer\Model\AddressFactory')->create()->load($shipAddressId);
                //echo "<pre>";
                //print_r($this->getCountries());
                //exit;
                
                
                $countriesCollection = $this->getCountries();
                array_shift($countriesCollection);
                /*echo '<pre>';
                    print_r($address->getData());
                echo '</pre>';
                exit;*/
                
                
                
                $countryId = "IN"; //$address['country_id'];
                $regionCollection = $this->getRegionsOfCountry($countryId);
                array_shift($regionCollection);
                 //print_r($regionCollection);
                    
                $street = $address['street'];
                if (!empty($street)) {
                    $s=ucfirst($street);
                    $bar = ucwords(strtolower($s));
                    $address['street']= preg_replace('/\s+/', ' ', $bar);
                    //$explodeStreet = explode(" ",$address['street']);
                }
                //echo '<pre>';
                //print_r($address['street']);
                //exit;
                
                $html='';
                $html .='<div id="opc-new-shipping-address" style="">';
                $html .='<form name="zoylo-custom-shipping-form" class="form form-shipping-address" id="co-shipping-form-custom" autocomplete="off">';
                $html .='<div id="shipping-new-address-form" class="fieldset address">';
		$html .='<div class="field required">
                                <label class="label"><span>First Name</span></label>
				<div class="control">
					<input class="input-text" type="text" name="firstname" value="'.$address['firstname'].'" id="firstname">
				</div>
                        </div>';

		$html .='<div class="field required">
                                <label class="label"><span>Last Name</span></label>
				<div class="control">
					<input class="input-text" type="text" name="lastname" value="'.$address['lastname'].'" id="lastname">
				</div>
                        </div>';
                
                /*$html .= '<div class="field" name="company">
                                <label class="label"><span>Company</span></label>
				<div class="control">
					<input class="input-text" type="text" name="company" value="'.$address['company'].'" id="company">
				</div>
                        </div>';
                */
                
                $html .='<div class="field _required" name="street" style="flex: 100%;">
                            <label class="label"><span>Street</span></label>
				<div class="control">
					<input class="input-text" type="text" name="street" value="'.$address['street'].'" id="street">
				</div>
                            </div>';
                
                /*$html .='<div class="field _required" name="street_1">
				<div class="control">
					<input class="input-text" type="text" name="street_1" value="'.(!empty($explodeStreet[1]))?$explodeStreet[1]:''.'" aria-required="true" aria-invalid="false" id="street_1">
				</div>
                            </div>';
                
                $html .='<div class="field _required" name="street_2">
				<div class="control">
					<input class="input-text" type="text" name="street_2" value="'.(!empty($explodeStreet[2]))?$explodeStreet[2]:''.'" aria-required="true" aria-invalid="false" id="street_2">
				</div>
                            </div>';
                */
                $html .= '<div class="field required" name="city">
                                <label class="label"><span>City</span></label>
				<div class="control">
					<input class="input-text" type="text" name="city" value="'.$address['city'].'" id="city">
				</div>
                        </div>';
                
                $html .='<div class="field _required" name="region_id">';
                $html .='<label class="label"><span>Region/State</span></label>';
		$html .='<select class="select" name="region_id" id="region_id" id="region_id">';
                    $html .='<option value="">Please select a region, state or province.</option>';
                    
                    foreach($regionCollection as $regionData){
                        
                        if($regionData['value'] == $address['region_id'] ){
                            $selectFlag = "selected";
                        }else{
                            $selectFlag = "";
                        }
                         $html .='<option data-title="'.$regionData['title'].'" value="'.$regionData['value'].'"'.$selectFlag.'>'.$regionData['label'].'</option>';
                     }
                $html .='</select>';
                $html .='</div>';
                
                $html .= '<div class="field _required" name="postcode">
                                <label class="label"><span>Post Code</span></label>
				<div class="control">
					<input class="input-text" type="text" name="postcode" value="'.$address['postcode'].'" aria-required="true" aria-invalid="false" id="postcode">
				</div>
                        </div>';
                
                
                $html .='<div class="field _required" name="country_id">';
                    $html .='<label class="label"><span>Country</span></label>';
                    $html .='<select class="select" name="country_id" aria-required="true" aria-invalid="false" id="country_id">';
                    $html .='<option value="">Please select a country.</option>';
                    
                            foreach($countriesCollection as $countryData){
                        
                        if($countryData['value'] == $address['country_id'] ){
                            $selectCountryFlag = "selected";
                        }else{
                            $selectCountryFlag = "";
                        }
                        if($countryData['value'] == "IN"):
                           $html .='<option data-title="'.$countryData['label'].'" value="'.$countryData['value'].'"'.$selectCountryFlag.'>'.$countryData['label'].'</option>';
                        endif;
                        }
                    //$html .='<option data-title="India" value="IN">India</option>';
                    //$html .='<option data-title="United States" value="US" selected>United States</option>';
                $html .='</select>';
                $html .='</div>';
                
                
                $html .= '<div class="field _required" name="telephone">
                                <label class="label"><span>Telephone</span></label>
				<div class="control">
					<input class="input-text" type="text" name="telephone" value="'.$address['telephone'].'" aria-required="true" aria-invalid="false" id="telephone">
				</div>
                        </div>';

                $workTypeArray = array();
                $workTypeArray[''] = 'Select Address Type';
                $workTypeArray['home'] = 'Home';
                $workTypeArray['work'] = 'Work';
                $workTypeArray['others'] = 'Others';

                $html .='<div class="field" name="customer_address_type">';
                    $html .='<label class="label"><span>Address Type</span></label>';
                    $html .='<select class="select" name="customer_address_type" aria-required="true" aria-invalid="false" id="customer_address_type">';
                    //$html .='<option value="">Please select a customer address type.</option>';
                    
                    $addressType = $address['customer_address_type'];
                    foreach($workTypeArray as $key => $workTypeValue){
                        if($key == $address['customer_address_type'] ){
                            $selectAddrTypeFlag = "selected";
                        }else{
                            $selectAddrTypeFlag = "";
                        }
                        $html .='<option data-title="'.$workTypeValue.'" value="'.$key.'"'.$selectAddrTypeFlag.'>'.$workTypeValue.'</option>';
                    }
                $html .='</select>';
                $html .='</div>';
                
                /*$html .='<div class="field choice">
                        <input type="checkbox" class="checkbox" id="shipping-save-in-address-book" data-bind="checked: saveInAddressBook">
                        <label class="label" for="shipping-save-in-address-book">
                            <span>Save in address book</span>
                        </label>
                        </div>';*/
                //$html .='<button class="action primary action-save-address zoylo-update-ship-address" id="'.$address['entity_id'].'" type="submit" data-role="action"><span>Save Address</span></button>';
                $html .='</div>';
                $html .='</form>';
                $html .='</div>';
                
                $html .='<footer class="modal-footer">
            <button class="action primary action-save-address zoylo-update-ship-address" id="'.$address['entity_id'].'" type="button" data-role="action"><span>Save Address</span></button>
            <button class="action secondary action-hide-popup" type="button" data-role="action"><span>Cancel</span></button></footer>';
                
                
                //$city = $address['city'];
                $resultJson->setData(['address' => $html]);
                //$resultJson->setData($address);
                return $resultJson;
    }
    
    public function getRegionsOfCountry($countryCode) {
        $regionCollection = $this->country->loadByCode($countryCode)->getRegions();
        $regions = $regionCollection->loadData()->toOptionArray(false);
        return $regions;
    }
    
    public function getCountryCollection()
    {
        $collection = $this->_countryCollectionFactory->create()->loadByStore();
        return $collection;
    }
    
    /**
     * Retrieve list of top destinations countries
     *
     * @return array
     */
    protected function getTopDestinations()
    {
        $scopeConfig = $this->objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');
        $destinations = (string)$scopeConfig->getValue(
            'general/country/destinations',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return !empty($destinations) ? explode(',', $destinations) : [];
    }

    /**
     * Retrieve list of countries in array option
     *
     * @return array
     */
    public function getCountries()
    {
        return $options = $this->getCountryCollection()
                ->setForegroundCountries($this->getTopDestinations())
                    ->toOptionArray();
    }
}