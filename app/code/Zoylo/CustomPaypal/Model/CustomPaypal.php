<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Zoylo\CustomPaypal\Model;



/**
 * Pay In Store payment method model
 */
class CustomPaypal extends \Magento\Payment\Model\Method\AbstractMethod
{

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'custompaypal';

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;


  

}
