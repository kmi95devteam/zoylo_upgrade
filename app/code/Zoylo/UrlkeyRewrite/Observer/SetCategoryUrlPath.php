<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Zoylo\UrlkeyRewrite\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 *  Set value for Special Price start date
 */
class SetCategoryUrlPath implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    private $localeDate;

    const OTC_PREFIX_ROUTE           = 'otc';
    protected $_logger;

    /**
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @codeCoverageIgnore
     */
    public function __construct(\Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,  \Psr\Log\LoggerInterface $logger)
    {
        $this->_logger = $logger;
        $this->localeDate = $localeDate;
    }

    /**
     * Set the current date to Special Price From attribute if it empty
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var  $product \Magento\Catalog\Model\Category */
        $category     = $observer->getEvent()->getCategory();
        $categoryType = $category->getcategory_type();

        if($categoryType == 3){            
            $this->_logger->error('test started1');
            /*$path = $category->getData('url_path');
            $path = str_replace('otc-', '', $path);
            $path = str_replace('otc/', '', $path);
            $path = self::OTC_PREFIX_ROUTE."/".$path;*/
            
            $categoryNames = [];
            $categoryUrl = '';
            $path = '';
            $categoryIds = explode('/',$category->getPath());
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            foreach($categoryIds as $categoryId){
                if($categoryId == 1 || $categoryId == 2 || $categoryId == 3)
                    continue;
                $cat = $objectManager->create('Magento\Catalog\Model\Category')->load($categoryId);
                $categoryNames[] = $category->formatUrlKey($cat->getName());
            }
            //print_r($categoryNames);die;
            $categoryUrl = implode("/", $categoryNames);
            //$path = $categoryUrl;
            $path = self::OTC_PREFIX_ROUTE."/".$categoryUrl;
            $this->_logger->error('Url Path: '.$path);
            $category->setData('url_path', $path);
            $name = $category->getName();
            $name = $category->formatUrlKey($name);
            $category->setData('url_key', $name);
            //$category->setData('url_key', $path);
           
        }else{
            $this->_logger->error('test started2');
            $path = $category->getData('url_path');
            $path = str_replace('otc-', '', $path);
            $path = str_replace('otc/', '', $path);
            $this->_logger->error('Url Path: '.$path);
            $category->setData('url_path', $path);
            $name = $category->getName();
            $name = $category->formatUrlKey($name);
            $category->setData('url_key', $name);
            //$category->setData('url_key', $path);
        }

        return $this;
    }
}
