<?php


namespace Zoylo\UrlkeyRewrite\Model;

use Magento\Config\Model\Config\Reader\Source\Deployed\DocumentRoot;
use Magento\Framework\App\ObjectManager;
use Magento\Robots\Model\Config\Value;
use Magento\Framework\DataObject;


class Sitemap extends \Magento\Sitemap\Model\Sitemap
{
    /**
     * Generate XML file
     *
     * @see http://www.sitemaps.org/protocol.html
     *
     * @return $this
     */
    public function generateXml()
    {
        $this->_initSitemapItems();

        /** @var $sitemapItem \Magento\Framework\DataObject */
        foreach ($this->_sitemapItems as $sitemapItem) {
            $changefreq = $sitemapItem->getChangefreq();
            $priority = $sitemapItem->getPriority();
            foreach ($sitemapItem->getCollection() as $item) {
                $itemURL = $item->getUrl();
                if (strpos($item->getUrl(), 'catalog/category/view/id') !== false){ // if category
                    $urlRewrite = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\UrlRewrite\Model\UrlRewrite');
                    $allCollection = $urlRewrite->getCollection()->addFieldToFilter('target_path',array('eq'=>$item->getUrl()))->getData();
                    if (isset($allCollection[0]['request_path']))
                    {
                        $itemURL = $allCollection[0]['request_path'];
                    }
                }
                $xml = $this->_getSitemapRow(
                    $itemURL,
                    $item->getUpdatedAt(),
                    $changefreq,
                    $priority,
                    $item->getImages()
                );
                if ($this->_isSplitRequired($xml) && $this->_sitemapIncrement > 0) {
                    $this->_finalizeSitemap();
                }
                if (!$this->_fileSize) {
                    $this->_createSitemap();
                }
                $this->_writeSitemapRow($xml);
                // Increase counters
                $this->_lineCount++;
                $this->_fileSize += strlen($xml);
            }
        }
        $this->_finalizeSitemap();

        if ($this->_sitemapIncrement == 1) {
            // In case when only one increment file was created use it as default sitemap
            $path = rtrim(
                $this->getSitemapPath(),
                '/'
            ) . '/' . $this->_getCurrentSitemapFilename(
                $this->_sitemapIncrement
            );
            $destination = rtrim($this->getSitemapPath(), '/') . '/' . $this->getSitemapFilename();

            $this->_directory->renameFile($path, $destination);
        } else {
            // Otherwise create index file with list of generated sitemaps
            $this->_createSitemapIndex();
        }

        $this->setSitemapTime($this->_dateModel->gmtDate('Y-m-d H:i:s'));
        $this->save();

        return $this;
    }

}
