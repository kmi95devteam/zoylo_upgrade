<?php
namespace Zoylo\CustomerAddressTypeNew\Plugin;
use Magento\Checkout\Block\Checkout\LayoutProcessor;
class CustomerAddressTypeNewPlugin
{
    public function afterProcess(LayoutProcessor $subject, $jsLayout) {
        $customAttributeCode = 'customer_address_type';
        $customField = [
            'component' => 'Magento_Ui/js/form/element/select',
            'config' => [
                // customScope is used to group elements within a single form (e.g. they can be validated separately)
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/select',
                'id' => 'drop-down',
            ],
            'dataScope' => 'shippingAddress.custom_attributes' . '.' . $customAttributeCode,
            'label' => 'Address Type',
            'provider' => 'checkoutProvider',
            'sortOrder' => 150,
            'validation' => [
                'required-entry' => false
            ],
            'id' => 'customer_address_type_zoylo',
            //'options' => 'Zoylo\Webservices\Model\Config\Source\AddressType',
            'options' => [
                [
                    'value' => '',
                    'label' => 'Select',
                ],
                [
                    'value' => 'home',
                    'label' => 'Home',
                ],
                [
                    'value' => 'work',
                    'label' => 'Work',
                ],
                [
                    'value' => 'others',
                    'label' => 'Others',
                ]
            ],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
            'class' => 'customer_address_type_zoylo_1'
        ];

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'][$customAttributeCode] = $customField;

        return $jsLayout;
    }
}