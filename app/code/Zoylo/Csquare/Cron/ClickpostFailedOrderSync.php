<?php
namespace Zoylo\Csquare\Cron;
use Zoylo\Csquare\Helper\Data;

class ClickpostFailedOrderSync
{

  protected $unicommerceFailedSync;
  
  protected $salesOrder;

  protected $scopeConfig;
  
  protected $resourceConnection;
  
  protected $timezoneDate;

  protected $orderConvert;
  
  protected $trackFactory;

  public function __construct(
   \Zoylo\Csquare\Model\UnicommerceFailedSyncFactory $unicommerceFailedSync,
   \Magento\Sales\Model\Order $salesOrder,
   \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
   \I95Dev\BranchInformation\Model\BranchFactory $branchFactory,
   \Magento\Framework\App\ResourceConnection $resourceConnection,
   \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneDate,
   \Magento\Sales\Model\Convert\Order $orderConvert,
   \Magento\Sales\Model\Order\Shipment\TrackFactory $trackFactory
  ){
    $this->unicommerceFailedSync = $unicommerceFailedSync;
    $this->salesOrder = $salesOrder;
    $this->scopeConfig = $scopeConfig;
    $this->branchFactory = $branchFactory;
    $this->resourceConnection = $resourceConnection;
    $this->timezoneDate = $timezoneDate;
    $this->orderConvert = $orderConvert;
    $this->trackFactory = $trackFactory;
  }
  
  public function execute()
  {
      try {
            $unicomFailedSynOrdersColl = $this->unicommerceFailedSync->create()->getCollection()->addFieldToFilter('status',array('neq'=>'200'))->addFieldToFilter('status',array('neq'=>'1'))->addFieldToFilter('status',array('neq'=>'0'))->addFieldToFilter('status',array('neq'=>'323')); // 323 order was already synced, 0 was used to unicommerce sync which failed, 1 was used to unicommerce sync which was successful, 200 means order synced successfully
            $failedOrderIdsArr = $unicomFailedSynOrdersColl->getData();
            foreach($failedOrderIdsArr as $eachOrder)
            {
                $mainOrder = $this->salesOrder->load($eachOrder['order_id']);
                $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                $pickupDate =  $this->timezoneDate->date()->format('Y-m-d'); // default date of today
                if ($this->timezoneDate->date()->format('H:i:s') < $this->timezoneDate->date('10:30:00')->format('H:i:s'))
                {
                   $pickupDate = date('Y-m-d',strtotime($this->timezoneDate->date()->format('Y-m-d').' +1 Day'));
                }
                else
                {
                    $pickupDate = date('Y-m-d',strtotime($this->timezoneDate->date()->format('Y-m-d').' +2 Day'));
                }
                $pickupDate = date ('Y-m-d H:i:s',strtotime($pickupDate." ".$this->scopeConfig->getValue('clickpost_settings/zoylo/clickpost_pickup_time', $storeScope)));
                $unicommerceFailedMdl = $this->unicommerceFailedSync->create()->load($mainOrder->getEntityId(),'order_id');
                if (!$unicommerceFailedMdl->getId())
                {
                    $unicommerceFailedMdl = $this->unicommerceFailedSync->create();
                }
                $paymentMethodDT = "COD";
                $codValue = $mainOrder->getGrandTotal();
                $condDataAll = $this->getConditionalRData($mainOrder);
                $cashPayMethods = array('cashondelivery','free');
                if (isset($condDataAll['payment_method']) && !in_array($condDataAll['payment_method'],$cashPayMethods) )
                {
                    $paymentMethodDT = "PREPAID";
                    $codValue = 0;
                }
                $shipPinCode = $mainOrder->getShippingAddress()->getPostCode();
                $branchCollection = $this->branchFactory->create()->getCollection();
                $branchCollection->getSelect()->joinLeft(array('facility_table'=>$this->resourceConnection->getTableName('facility_code_mapping')),'main_table.branch_code=facility_table.branch_id')->where("main_table.zip_code ='".$shipPinCode."'");
                $facilityCenter = '';
                if (isset($branchCollection->getData()[0]['facility_code']))
                {
                    $facilityCenter = $branchCollection->getData()[0]['facility_code'];
                }
                $customerToShipAddress = "";
                $customerToShipAddress = $customerToShipAddress.$mainOrder->getShippingAddress()->getFirstname().", ".$mainOrder->getShippingAddress()->getLastname();

                $streetAddress = $order->getShippingAddress()->getStreet();
                if (isset($streetAddress[0]) && $streetAddress[0] != '')
                {
                    $customerToShipAddress =  $customerToShipAddress.", ".$streetAddress[0];
                }
                if (isset($streetAddress[1]) && $streetAddress[1] != '')
                {
                    $customerToShipAddress =  $customerToShipAddress.", ".$streetAddress[1];
                }
                if (isset($streetAddress[2]) && $streetAddress[2] != '')
                {
                    $customerToShipAddress =  $customerToShipAddress.", ".$streetAddress[2];
                }
                $customerToShipAddress =  $customerToShipAddress.", ".$mainOrder->getShippingAddress()->getCity().", ".$mainOrder->getShippingAddress()->getRegion().", ".$mainOrder->getShippingAddress()->getPostcode().", INDIA, ".$mainOrder->getShippingAddress()->getTelephone();
                $dataString = array(array('pickup_pincode'=>$branchCollection->getData()[0]['pincode'],'drop_pincode'=>$mainOrder->getShippingAddress()->getPostcode(),'order_type'=>$paymentMethodDT,'reference_number'=>$mainOrder->getIncrementId(),'item'=>'medicines','invoice_value'=>$mainOrder->getGrandTotal(),'delivery_type'=>'FORWARD','weight'=>'1','height'=>'1','length'=>'1','breadth'=>'1'));
                $jsonReq = json_encode($dataString);
                $ch = curl_init($this->scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_base_url', $storeScope)."api/v1/recommendation_api/?key=".$this->scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_key', $storeScope));
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonReq);                                                                  
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                    'Content-Type: application/json',                                                                                
                    'Content-Length: ' . strlen($jsonReq)
                ));
                $result = curl_exec($ch);
                $result = json_decode($result);
                if ($result->meta->status == '200' && strtolower($result->meta->message) == 'success')
                {
                    $dataStringCreateSalesOrder = new \stdClass();
                    $dataStringCreateSalesOrder->pickup_info = new \stdClass();
                    if (isset($branchCollection->getData()[0]['state']))
                    {
                        $dataStringCreateSalesOrder->pickup_info->pickup_state = $branchCollection->getData()[0]['state'];
                    } 
                    if (isset($branchCollection->getData()[0]['address']))
                    {
                        $dataStringCreateSalesOrder->pickup_info->pickup_address = $branchCollection->getData()[0]['address'];                    
                    }
                    if (isset($branchCollection->getData()[0]['email']))
                    {
                        $dataStringCreateSalesOrder->pickup_info->email = $branchCollection->getData()[0]['email'];                    
                    }
                    if (isset($branchCollection->getData()[0]['pincode']))
                    {
                        $dataStringCreateSalesOrder->pickup_info->pickup_pincode = $branchCollection->getData()[0]['pincode'];                    
                    }
                    if (isset($branchCollection->getData()[0]['city']))
                    {
                        $dataStringCreateSalesOrder->pickup_info->pickup_city = $branchCollection->getData()[0]['city'];                    
                    }
                    if (isset($branchCollection->getData()[0]['name']))
                    {
                        $dataStringCreateSalesOrder->pickup_info->pickup_name = $branchCollection->getData()[0]['name'];                    
                    }
                    if (isset($branchCollection->getData()[0]['phone']))
                    {
                        $dataStringCreateSalesOrder->pickup_info->pickup_phone = $branchCollection->getData()[0]['phone'];                    
                    }

                    $dataStringCreateSalesOrder->pickup_info->pickup_time = $pickupDate;
                    $dataStringCreateSalesOrder->pickup_info->tin = "tin";
                    $dataStringCreateSalesOrder->pickup_info->pickup_country = "INDIA";
                    $dataStringCreateSalesOrder->drop_info = new \stdClass();
                    $dataStringCreateSalesOrder->drop_info->drop_address = $customerToShipAddress;
                    //$dataStringCreateSalesOrder->drop_info->drop_phone = "******".substr($mainOrder->getShippingAddress()->getTelephone(), -4);
                    $dataStringCreateSalesOrder->drop_info->drop_phone = $mainOrder->getShippingAddress()->getTelephone();
                    $dataStringCreateSalesOrder->drop_info->drop_country = "INDIA";
                    $dataStringCreateSalesOrder->drop_info->drop_state = $mainOrder->getShippingAddress()->getRegion();
                    $dataStringCreateSalesOrder->drop_info->drop_pincode = $mainOrder->getShippingAddress()->getPostcode();
                    $dataStringCreateSalesOrder->drop_info->drop_city = $mainOrder->getShippingAddress()->getCity();
                    $dataStringCreateSalesOrder->drop_info->drop_name = $mainOrder->getShippingAddress()->getFirstname()." ".$mainOrder->getShippingAddress()->getLastname();
                    $dataStringCreateSalesOrder->shipment_details = new \stdClass();
                    $dataStringCreateSalesOrder->shipment_details->height = "1";
                    $dataStringCreateSalesOrder->shipment_details->order_type = $paymentMethodDT;
                    $dataStringCreateSalesOrder->shipment_details->invoice_value = 1;
                    $dataStringCreateSalesOrder->shipment_details->invoice_number = $mainOrder->getIncrementId();
                    foreach ($mainOrder->getInvoiceCollection() as $invoice) //at present we are having only one invoice number
                    {
                        $dataStringCreateSalesOrder->shipment_details->invoice_number = $invoice->getIncrementId();
                    }
                    if (isset($mainOrder->getInvoiceCollection()->getData()[0]['created_at']))
                    {
                        $dataStringCreateSalesOrder->shipment_details->invoice_date = date('Y-m-d',  strtotime($mainOrder->getInvoiceCollection()->getData()[0]['created_at']));;                   
                    }
                    else // if invoice date is not set then we are sending the today's date 
                    {
                        $dataStringCreateSalesOrder->shipment_details->invoice_date = $this->timezoneDate->date()->format('Y-m-d');       
                    }

                    $dataStringCreateSalesOrder->shipment_details->reference_number = $mainOrder->getIncrementId();
                    $dataStringCreateSalesOrder->shipment_details->length = "1";
                    $dataStringCreateSalesOrder->shipment_details->breadth = "1";
                    $dataStringCreateSalesOrder->shipment_details->weight = "1";
                    $itemsObjData = new \stdClass();
                    $itemsObjData->product_url = "";
                    $itemsObjData->price = 1;
                    $itemsObjData->description = "Medicines";
                    $itemsObjData->quantity = "1";
                    $itemsObjData->sku = "zoylo-product";
                    $itemsObjData->additional = new \stdClass();
                    $itemsObjData->additional->images = "";
                    $itemsObjData->additional->weight = '1';
                    $itemsObjData->additional->return_days = "7";
                    $dataStringCreateSalesOrder->shipment_details->items = array($itemsObjData);
                    $dataStringCreateSalesOrder->shipment_details->cod_value = $codValue;

                    if (isset($result->result[0]->preference_array[0]->cp_id))
                    {
                        $dataStringCreateSalesOrder->shipment_details->courier_partner = $result->result[0]->preference_array[0]->cp_id;                    
                    }
                    else
                    {
                        $dataStringCreateSalesOrder->shipment_details->courier_partner = 3;                    
                    }

                    if (isset($result->result[0]->preference_array[0]->account_code))
                    {
                        $dataStringCreateSalesOrder->account_code = $result->result[0]->preference_array[0]->account_code;
                    }
                    else
                    {
                        $dataStringCreateSalesOrder->account_code = 'ECOMM Express';                    
                    }

                    $jsonReqCreateSalesOrder = json_encode($dataStringCreateSalesOrder);

                    $chCreateSalesOrder = curl_init($this->scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_base_url', $storeScope)."api/v3/create-order/?username=".$this->scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_username', $storeScope)."&key=".$this->scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_key', $storeScope));
                    curl_setopt($chCreateSalesOrder, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                    curl_setopt($chCreateSalesOrder, CURLOPT_POSTFIELDS, json_encode($dataStringCreateSalesOrder));                                                                  
                    curl_setopt($chCreateSalesOrder, CURLOPT_RETURNTRANSFER, true);                                                                      
                    curl_setopt($chCreateSalesOrder, CURLOPT_HTTPHEADER, array(                                                                          
                        'Content-Type: application/json',                                                                                
                        'Content-Length: ' . strlen(json_encode($dataStringCreateSalesOrder))
                    ));
                    $resultCreateSalesOrder = curl_exec($chCreateSalesOrder);
                    $resultCreateSalesOrder = json_decode($resultCreateSalesOrder);
                    if ($resultCreateSalesOrder->meta->status == '200') // order created successfully on clickpost
                    {
                        $unicommerceFailedMdl->setOrderId($mainOrder->getEntityId());
                        $unicommerceFailedMdl->setStatus($resultCreateSalesOrder->meta->status);
                        $unicommerceFailedMdl->setMessage($resultCreateSalesOrder->meta->message);
                        $unicommerceFailedMdl->setSortCode($resultCreateSalesOrder->result->sort_code);
                        $unicommerceFailedMdl->setLabel($resultCreateSalesOrder->result->label);
                        $unicommerceFailedMdl->setSecurityKey($resultCreateSalesOrder->result->security_key);
                        $unicommerceFailedMdl->setReferenceNumber($resultCreateSalesOrder->result->reference_number);
                        $unicommerceFailedMdl->setWaybill($resultCreateSalesOrder->result->waybill);
                        $unicommerceFailedMdl->setTrackingId($resultCreateSalesOrder->tracking_id);
                        $unicommerceFailedMdl->setClickpostOrderId($resultCreateSalesOrder->order_id);
                        $unicommerceFailedMdl->setIncrementId($mainOrder->getIncrementId());
                        $unicommerceFailedMdl->save();  

                        //code for create shippment starts
                        $mainOrder->setState('processing'); // temporary setting this state so that we can create shippment.
                        if ($mainOrder->canShip())
                        {
                            // Initialize the order shipment object
                            $convertOrder = $this->orderConvert;
                            $shipment = $convertOrder->toShipment($mainOrder);
                            // Loop through order items
                            foreach ($mainOrder->getAllItems() AS $orderItemEach) 
                            {
                                // Check if order item has qty to ship or is virtual
                                if (! $orderItemEach->getQtyToShip() || $orderItemEach->getIsVirtual()) 
                                {
                                    continue;
                                }
                                $qtyShipped = $orderItemEach->getQtyToShip();
                                // Create shipment item with qty
                                $shipmentItem = $convertOrder->itemToShipmentItem($orderItemEach)->setQty($qtyShipped);
                                // Add shipment item to shipment
                                $shipment->addItem($shipmentItem);
                            }
                            // Register shipment
                            $shipment->register();
                            $shipment->getOrder()->setIsInProcess(true);
                            try {
                                $trackData = array(
                                    'carrier_code' => 'ecomexpress',
                                    'title' => 'Ecom Express',
                                    'number' => $resultCreateSalesOrder->result->waybill, // Replace with your tracking number
                                );

                                $track = $this->trackFactory->create()->addData($trackData);
                                $shipment->addTrack($track)->save();
                                // Save created shipment and order
                                $shipment->save();
                                $shipment->getOrder()->save();

                                // Send email
                                //$objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
                                //    ->notify($shipment);
                                //$shipment->save();
                            } catch (\Exception $e) {
                               echo "Shipment Not Created". $e->getMessage(); exit;
                            }
                        }
                        $mainOrder->setState('payment_review'); // resetting the order state back to it's default state
                        //code for create shippment ends
                    }
                    else // order could not be created or was already created at clickpost
                    {
                        $unicommerceFailedMdl->setOrderId($mainOrder->getEntityId());
                        $unicommerceFailedMdl->setIncrementId($mainOrder->getIncrementId());
                        $unicommerceFailedMdl->setStatus($resultCreateSalesOrder->meta->status);
                        $unicommerceFailedMdl->setMessage($resultCreateSalesOrder->meta->message);
                        $unicommerceFailedMdl->save();
                    }
                }
                else
                {
                    $unicommerceFailedMdl->setOrderId($mainOrder->getEntityId());
                    $unicommerceFailedMdl->setIncrementId($mainOrder->getIncrementId());
                    $unicommerceFailedMdl->setStatus($result->meta->status);
                    $unicommerceFailedMdl->setMessage($result->meta->message);
                    $unicommerceFailedMdl->save();
                }
            }
      } catch (\Exception $e) {
          echo "Exception in Unicommerce Sync Cron:".$e->getMessage();
      }

  }
  
  public function getConditionalRData($order){
        $orderCondComb = [];
        $containsPresciption = FALSE;
        $orderPayment = $order->getPayment();
        $paymentMethod = $orderPayment->getMethodInstance();
        $paymentMethodCode = $paymentMethod->getCode();
        $orderAllItems = $order->getAllItems(); 
        if ($orderAllItems) {
            foreach ($orderAllItems as $item) {
                if ( $item->getProduct() && $item->getProduct()->getData('prescription_required') && $item->getProduct()->getData('prescription_required') == 'required')
                {
                   $containsPresciption = TRUE;
                }
            }
        }
        
        $orderCondComb['payment_method'] = $paymentMethodCode;
        $orderCondComb['prescription_required'] = $containsPresciption;

        return $orderCondComb;
    }
  
}