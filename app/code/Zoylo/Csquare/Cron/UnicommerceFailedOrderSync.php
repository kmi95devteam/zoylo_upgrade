<?php
namespace Zoylo\Csquare\Cron;
use Zoylo\Csquare\Helper\Data;

class UnicommerceFailedOrderSync
{

  protected $unicommerceFailedSync;
  
  protected $salesOrder;

  protected $scopeConfig;
  
  protected $resourceConnection;
  
  public function __construct(
   \Zoylo\Csquare\Model\UnicommerceFailedSyncFactory $unicommerceFailedSync,
   \Magento\Sales\Model\Order $salesOrder,
   \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
   \I95Dev\BranchInformation\Model\BranchFactory $branchFactory,
   \Magento\Framework\App\ResourceConnection $resourceConnection
  ){
    $this->unicommerceFailedSync = $unicommerceFailedSync;
    $this->salesOrder = $salesOrder;
    $this->scopeConfig = $scopeConfig;
    $this->branchFactory = $branchFactory;
    $this->resourceConnection = $resourceConnection;
  }
  
  public function execute()
  {
      try {
            $unicomFailedSynOrdersColl = $this->unicommerceFailedSync->create()->getCollection()->addFieldToFilter('status','0');
            $failedOrderIdsArr = $unicomFailedSynOrdersColl->getData();
            foreach($failedOrderIdsArr as $eachOrder)
            {
                $mainOrder = $this->salesOrder->load($eachOrder['order_id']);
                //if ($mainOrder->getStatus()  == 'ready_for_delivery_cs')
                //{
                    $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                    $headerDetailsCh = curl_init("https://zdpl.unicommerce.com/oauth/token?grant_type=password&client_id=my-trusted-client&username=".$this->scopeConfig->getValue('unicommerce_settings/zoylo/unicommerce_email', $storeScope)."&password=".$this->scopeConfig->getValue('unicommerce_settings/zoylo/unicommerce_password', $storeScope));
                    curl_setopt($headerDetailsCh, CURLOPT_CUSTOMREQUEST, "GET");   
                    curl_setopt($headerDetailsCh,CURLOPT_RETURNTRANSFER,1);
                    $tokenResult = curl_exec($headerDetailsCh);
                    $tokenResult2 = explode(',',$tokenResult);
                    $tokenResult3 = json_encode($tokenResult2[0]);
                    $tokenFinal = explode(":",$tokenResult2[0])[1];
                    $tokenFinal = str_replace('"', '', $tokenFinal);
                    curl_close($headerDetailsCh);
                    $cashOnDelivery = true;
                    $condDataAll = $this->getConditionalRData($mainOrder);
                    $cashPayMethods = array('cashondelivery','free');
                    if (isset($condDataAll['payment_method']) && !in_array($condDataAll['payment_method'],$cashPayMethods)) {
                        $cashOnDelivery = false;
                    }
                    $allOrderItems = $mainOrder->getAllItems();
                    $orderItemData = array();
                    $shipPinCode = $mainOrder->getShippingAddress()->getPostCode();                    
                    $branchCollection = $this->branchFactory->create()->getCollection();
                    $branchCollection->getSelect()->joinLeft(array('facility_table'=>$this->resourceConnection->getTableName('facility_code_mapping')),'main_table.branch_code=facility_table.branch_id')->where("main_table.zip_code ='".$shipPinCode."'");
                    $facilityCenter = '';
                    if (isset($branchCollection->getData()[0]['facility_code']))
                    {
                        $facilityCenter = $branchCollection->getData()[0]['facility_code'];
                    }
                    $streetArr = $mainOrder->getShippingAddress()->getStreet();
                    $street1 = "";
                    if (isset($streetArr[0]))
                    {
                        $street1 = $streetArr[0];
                    }
                    $street2 = "";
                    if (isset($streetArr[1]))
                    {
                        $street2 = $streetArr[1];
                    }
                    /*foreach($allOrderItems as $eachROrderItem) //keep this code commented until testing is going on, uncomment when we get go ahead for making the code dynamic
                    {
                        $orderItemData[] = array("itemSku"=>$eachROrderItem->getSku(),"sellingPrice"=>$eachROrderItem->getPrice(),"shippingMethodCode"=>"STD","totalPrice"=>$eachROrderItem->getRowTotal(),"code"=>"90004443","facilityCode"=>"VIVA-MASSABTANK");
                    }*/
                    //10200375
                    //die('came here');
                    $orderItemData[] = array("itemSku"=>'99999999',"sellingPrice"=>$mainOrder->getSubtotal()+$mainOrder->getDiscountAmount(),"shippingMethodCode"=>"STD","discount"=>abs($mainOrder->getDiscountAmount()),"shippingCharges"=>$mainOrder->getShippingAmount(),"totalPrice"=>$mainOrder->getGrandTotal(),"code"=>"90004443","facilityCode"=>$facilityCenter);
                    $dataString = array("saleOrder"=>array("code"=>$mainOrder->getIncrementId(),"displayOrderCode"=>$mainOrder->getIncrementId(),"cashOnDelivery"=>$cashOnDelivery,"addresses"=>array(array("id"=>"0","name"=>$mainOrder->getCustomerFirstname()." ".$mainOrder->getCustomerLastname() ,"addressLine1"=>$street1,"addressLine2"=>$street2,"city"=>$mainOrder->getShippingAddress()->getCity(),"state"=>$mainOrder->getShippingAddress()->getRegion(),"country"=>"India","pincode"=>str_replace(' ','',$mainOrder->getShippingAddress()->getPostCode()),"phone"=>$mainOrder->getShippingAddress()->getTelephone())),"billingAddress"=>array("referenceId"=>0),"shippingAddress"=>array("referenceId"=>0),"saleOrderItems"=>$orderItemData));
                    $jsonReq = json_encode($dataString);
                    $ch = curl_init("https://zdpl.unicommerce.com/services/rest/v1/oms/saleOrder/create");
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonReq);                                                                  
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                        'Content-Type: application/json',                                                                                
                        'Content-Length: ' . strlen($jsonReq),
                        'Authorization: Bearer '.$tokenFinal)
                    );
                    
                    $result = curl_exec($ch);
                    $responseString = str_replace('\n', '', $result);
                    $responseString = rtrim($responseString, ',');
                    $responseString = "[" . trim($responseString) . "]";
                    $json = json_decode($responseString, true);
                    if (curl_errno($ch)) { // if curl request failed
                        $unicommerceFailedMdl = $this->unicommerceFailedSync->create()->load($mainOrder->getEntityId(),'order_id');
                        if (!$unicommerceFailedMdl->getId())
                        {
                            $unicommerceFailedMdl = $this->unicommerceFailedSync->create();
                        }
                        $unicommerceFailedMdl->setOrderId($mainOrder->getEntityId());
                        $unicommerceFailedMdl->setStatus(0);
                        $unicommerceFailedMdl->setMessage('Cron Curl Connection Failed To Unicommerce');
                        $unicommerceFailedMdl->save();
                        //die("error in curl");
                    }
                    else if (isset($json[0]['successful']) && $json[0]['successful'] == true){ //everything correct
                        $unicommerceFailedMdl = $this->unicommerceFailedSync->create()->load($mainOrder->getEntityId(),'order_id');
                        if (!$unicommerceFailedMdl->getId())
                        {
                            $unicommerceFailedMdl = $this->unicommerceFailedSync->create();
                        }
                        $unicommerceFailedMdl->setOrderId($mainOrder->getEntityId());
                        $unicommerceFailedMdl->setStatus(1);
                        $unicommerceFailedMdl->setMessage('Cron Has Synced To Unicommerce Successfully.');
                        $unicommerceFailedMdl->save(); 
                    }
                    else{ // something went wrong
                        $unicommerceFailedMdl = $this->unicommerceFailedSync->create()->load($mainOrder->getEntityId(),'order_id');
                        if (!$unicommerceFailedMdl->getId())
                        {
                            $unicommerceFailedMdl = $this->unicommerceFailedSync->create();
                        }
                        $unicommerceFailedMdl->setOrderId($mainOrder->getEntityId());
                        $unicommerceFailedMdl->setStatus(0);
                        $unicommerceFailedMdl->setMessage('Cron Has Failed To Sync To Unicommerce. Reason: '.$result);
                        $unicommerceFailedMdl->save();
                    }
                    curl_close($ch);
                //}
            }
      } catch (\Exception $e) {
          echo "Exception in Unicommerce Sync Cron:".$e->getMessage();
      }

  }
  
  public function getConditionalRData($order){
        $orderCondComb = [];
        $containsPresciption = FALSE;
        $orderPayment = $order->getPayment();
        $paymentMethod = $orderPayment->getMethodInstance();
        $paymentMethodCode = $paymentMethod->getCode();
        $orderAllItems = $order->getAllItems(); 
        if ($orderAllItems) {
            foreach ($orderAllItems as $item) {
                if ( $item->getProduct() && $item->getProduct()->getData('prescription_required') && $item->getProduct()->getData('prescription_required') == 'required')
                {
                   $containsPresciption = TRUE;
                }
            }
        }
        
        $orderCondComb['payment_method'] = $paymentMethodCode;
        $orderCondComb['prescription_required'] = $containsPresciption;

        return $orderCondComb;
    }
  
}