<?php
namespace Zoylo\Csquare\Cron;
use Zoylo\Csquare\Helper\Data;

class UpdatePrice
{

  /**
   * @var Data
  */
  protected $data;

  /**
   * @var \Zoylo\Csquare\Model\UpdatePriceFactory
  */
    protected $updatePriceFactory;

  /**
   * 
   * @param Data $data
  */
  public function __construct(
    Data $data,
    \Zoylo\Csquare\Model\UpdatePriceFactory $updatePriceFactory
  ){
    $this->_data = $data;
    $this->updatePriceFactory = $updatePriceFactory;
  }
  
  public function updatePrice()
  {
    $dataLog = $this->_data;
	$entity = 'prce update';
    $dataLog->infoLog(__METHOD__, "Cron Job is Started For " . $entity . " ". date('Y-m-d H:i:s'), 'PriceUpdate');
    $result = $this->updatePriceFactory->create()->updatePrice();
    $dataLog->infoLog(__METHOD__, "Cron Job is End For " . $entity . " ". date('Y-m-d H:i:s'), 'PriceUpdate');
    return $this;
  }
}