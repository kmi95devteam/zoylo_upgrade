<?php
namespace Zoylo\Csquare\Cron;
use Zoylo\Csquare\Helper\Data;

class OrderSync
{

  /**
   * @var Data
  */
  protected $data;

  /**
   * @var \Zoylo\Csquare\Model\OrdersFactory
  */
    protected $csquareOrdersSyncFactory;

  /**
   * 
   * @param Data $data
  */
  public function __construct(
    Data $data,
    \Zoylo\Csquare\Model\OrderSyncFactory $csquareOrdersSyncFactory
  ){
    $this->_data = $data;
    $this->csquareOrdersSyncFactory = $csquareOrdersSyncFactory;
  }
  
  public function execute()
  {
    $dataLog = $this->_data;
    $entity = 'ordersync'; // based on entites we need to declre in xml file and read here dynamically
    $dataLog->infoLog(__METHOD__, "Cron Job is Started For " . $entity . " ". date('Y-m-d H:i:s'), 'OrderSync');
    $result = $this->csquareOrdersSyncFactory->create()->orderSync();
    $dataLog->infoLog(__METHOD__, "Cron Job is End For " . $entity . " ". date('Y-m-d H:i:s'), 'OrderSync');
    return $this;
  }

  public function checkStatus()
  {
    $dataLog = $this->_data;
    $invoiceIds = [];
    $manualSync = false;
    $entity = 'orderstatus'; // based on entites we need to declre in xml file and read here dynamically
    $dataLog->infoLog(__METHOD__, "Cron Job is Started For " . $entity . " ". date('Y-m-d H:i:s'), 'checkStatus');
    $result = $this->csquareOrdersSyncFactory->create()->checkStatus($invoiceIds, $manualSync);
    $dataLog->infoLog(__METHOD__, "Cron Job is End For " . $entity . " ". date('Y-m-d H:i:s'), 'checkStatus');
    return $this;
  }
  
  public function sendEmail()
  {
    $dataLog = $this->_data;
    $entity = 'sendEmail';
    $dataLog->infoLog(__METHOD__, "Cron Job is Started For " . $entity . " ". date('Y-m-d H:i:s'), 'sendEmail');
    $result = $this->csquareOrdersSyncFactory->create()->sendEmail();
    $dataLog->infoLog(__METHOD__, "Cron Job is End For " . $entity . " ". date('Y-m-d H:i:s'), 'sendEmail');
    return $this;
  }
}