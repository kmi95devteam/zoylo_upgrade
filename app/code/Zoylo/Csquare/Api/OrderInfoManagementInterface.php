<?php
namespace Zoylo\Csquare\Api;

interface OrderInfoManagementInterface
{

    /**
     * POST for saveOrderInfo api
     * @param mixed $order
     * @throws \Magento\Framework\Exception\CouldNotSaveException
	 * @return \Zoylo\Csquare\Api\Data\OrderDataInterface
     */
    public function saveOrderInfo($order);
}
