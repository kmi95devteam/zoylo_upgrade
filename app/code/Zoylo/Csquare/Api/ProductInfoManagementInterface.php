<?php

namespace Zoylo\Csquare\Api;

interface ProductInfoManagementInterface
{

    /**
     * POST for setProductInfo api
     * @param mixed $item
     * @throws \Magento\Framework\Exception\CouldNotSaveException
	 * @return \Zoylo\Csquare\Api\Data\ProductDataInterface
     */
    public function setProductInfo($item);
}
