<?php
namespace Zoylo\Csquare\Api\Data;
/**
 * @api
 *
 */
interface ProductDataInterface
{
    
    /**
     * Get Code
     *
     * @return int
     */
    public function getCode();

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess();

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success);

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage();

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * Get Id
     *
     * @return \Zoylo\Csquare\Api\Data\ProductDataInterface[]
     */
    public function getIds();

    /**
     * Set Id
     *
     * @param \Zoylo\Csquare\Api\Data\ProductDataInterface[] $ids
     * @return $this
     */
    public function setIds(array $ids = null);
}