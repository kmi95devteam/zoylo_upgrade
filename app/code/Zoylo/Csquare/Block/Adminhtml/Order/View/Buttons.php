<?php
namespace Zoylo\Csquare\Block\Adminhtml\Order\View;

class Buttons extends \Magento\Sales\Block\Adminhtml\Order\View
{    
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Model\Config $salesConfig,
        \Magento\Sales\Helper\Reorder $reorderHelper,
        array $data = []
    ) {
       
        parent::__construct($context, $registry, $salesConfig, $reorderHelper, $data);
    }

    public function addButtons()
    {
        $parentBlock = $this->getParentBlock();

        if(!$parentBlock instanceof \Magento\Backend\Block\Template || !$parentBlock->getOrderId()) {
            return;
        }
        $orderId = $parentBlock->getOrderId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderData = $objectManager->create('Magento\Sales\Model\Order')->load($orderId);
        $status = $orderData->getStatus();
        
        if($status == 'ready_for_packing_cs'){

        $buttonUrl = $this->_urlBuilder->getUrl(
            'quality/check',
            ['order_id' => $parentBlock->getOrderId()]
        );
        $message = __('Are you sure you want to do this?');

        $this->getToolbar()->addChild(
              'submit',
              \Magento\Backend\Block\Widget\Button::class,
              ['label' => __('QC Review'), 
			   'class' => 'popup-modal',
			  'on_click' => "showPopup('".$parentBlock->getOrderId()."')"]
            );
        }

        return $this; 
    }

}