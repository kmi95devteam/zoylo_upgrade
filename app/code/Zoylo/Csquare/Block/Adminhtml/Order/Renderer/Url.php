<?php

namespace Zoylo\Csquare\Block\Adminhtml\Order\Renderer;

use Magento\Framework\DataObject;

class Url extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
   

    /**
     * get order Id
     * @param  DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $data = $row->getData();
        $orderId = $data['entity_id'];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderUrl =  $objectManager->get('\Magento\Backend\Helper\Data')->getUrl('sales/order/view',['order_id' => $orderId]);

        if (!empty($orderId)){
            return '<a href="'.$orderUrl.'" target="_blank">View</a>';
        }
        else {
            return false;
        }
        
    }
}