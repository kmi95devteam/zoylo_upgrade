<?php

namespace Zoylo\Csquare\Block\Adminhtml\Order\Renderer;

use Magento\Framework\DataObject;

class Comments extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
   

    /**
     * get order Id
     * @param  DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $data = $row->getData();
        $orderId = $data['entity_id'];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderData =  $objectManager->get('Magento\Sales\Model\OrderFactory')->create()->load($orderId);
        $orderCommentHistory = $orderData->getStatusHistoryCollection()->getData();
        foreach($orderCommentHistory as $history){
            
            $comments = $history['comment'];
            if (strpos($comments, 'QC Failed') !== false) {
                
                echo $comments .'<br/>';
            }
        }
        
        
    }
}