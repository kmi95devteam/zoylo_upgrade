<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Zoylo\Csquare\Block\Adminhtml\Order\Status\Assign;

/**
 * Assign order status to order state form
 */
class Form extends \Magento\Sales\Block\Adminhtml\Order\Status\Assign\Form
{   
    protected function _prepareForm()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderConfig   = $objectManager->create('\Zoylo\Csquare\Model\Order\Config');
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(['data' => ['id' => 'edit_form', 'method' => 'post']]);

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Assignment Information')]);

        $statuses = $this->_collectionFactory->create()->toOptionArray();
        array_unshift($statuses, ['value' => '', 'label' => '']);

        $states = $orderConfig->getStatesCustom();;
        $states = array_merge(['' => ''], $states);
      
        $fieldset->addField(
            'status',
            'select',
            [
                'name' => 'status',
                'label' => __('Order Status'),
                'class' => 'required-entry',
                'values' => $statuses,
                'required' => true
            ]
        );

        $fieldset->addField(
            'state',
            'select',
            [
                'name' => 'state',
                'label' => __('Order State'),
                'class' => 'required-entry',
                'values' => $states,
                'required' => true
            ]
        );

        $fieldset->addField(
            'is_default',
            'checkbox',
            ['name' => 'is_default', 'label' => __('Use Order Status As Default'), 'value' => 1]
        );

        $fieldset->addField(
            'visible_on_front',
            'checkbox',
            ['name' => 'visible_on_front', 'label' => __('Visible On Storefront'), 'checked' => true, 'value' => 1]
        );

        $form->setAction($this->getUrl('sales/order_status/assignPost'));
        $form->setUseContainer(true);
        $this->setForm($form);

    }
}
