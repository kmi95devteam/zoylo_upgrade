<?php

namespace Zoylo\Csquare\Block\Adminhtml\OnHoldOrders\Renderer;

use Magento\Framework\DataObject;

class OrderCount extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
   

    /**
     * get order Id
     * @param  DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
		$data = $row->getData();
		$sku = $data['product_sku'];
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$collection = $objectManager->create('\Zoylo\Csquare\Model\OnHoldReport')
									->getCollection()
									->addFieldToFilter('product_sku', $sku);
		//$collection->printlogquery(true);
		if (count($collection)>0){
            return count($collection);
        }
        else {
            return false;
        }
        
    }
}