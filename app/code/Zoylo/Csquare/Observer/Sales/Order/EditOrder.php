<?php

namespace Zoylo\Csquare\Observer\Sales\Order;
use Magento\Framework\Event\ObserverInterface;

class EditOrder implements ObserverInterface
{
    
   /**
    * @param \Zoylo\Csquare\Model\OrdersFactory $csquareOrdersFactory
    */
    public function __construct(
        \Zoylo\Csquare\Model\OrdersFactory $csquareOrdersFactory     
    ) {       
        $this->_csquareOrdersFactory = $csquareOrdersFactory;
    }
	
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getOrder();
		$incrementId  = $order->getIncrementId();
		$collection = $this->_csquareOrdersFactory->create()->getCollection()
							->addFieldtoFilter('source_order_id', ['eq' => $incrementId]);
		
		if(count($collection) > 0){
			foreach($collection as $record){

				if($order->getStatus() != "order_modified_cs"){
					$record->setUpdatedBy('');
				}else{
					$record->setStatus(5);
					$record->setUpdatedBy('Magento');
				}
				$record->setFullfilmentStatus('Order Pending');
				$record->save();
				//echo  $record->getUpdatedBy();die;
			}
		}
		
        return $this;
	}
}

