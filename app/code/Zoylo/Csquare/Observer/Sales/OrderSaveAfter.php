<?php
namespace Zoylo\Csquare\Observer\Sales;
use Magento\Framework\Event\ObserverInterface;
use Zoylo\Csquare\Helper\Data;

class OrderSaveAfter implements ObserverInterface
{
    protected $_objectManager;

    /**
     * @var Data
    */
    protected $_data;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dataTime;

    /**
     * @var \Zoylo\Csquare\Model\OrdersFactory
     */
    protected $_csquareOrdersFactory;

    /**
     * @var \Zoylo\Csquare\Model\OrderSyncFactory
     */
    protected $_csquareOrderSyncFactory;

    protected $transportBuilder;
    
    protected $addressRenderer;
    
    protected $paymentHelper;
    
    protected $identityContainer;
    
    protected $resourceConnection;
    
    protected $unicommerceFailedSync;
    
    protected $scopeConfig;
    
    protected $branchFactory;
    
    protected $request;
    
    protected $backendSession;
    /**
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dataTime
     * @param \Zoylo\Csquare\Model\OrdersFactory $csquareOrdersFactory
     * @param \Zoylo\Csquare\Model\OrderSyncFactory $csquareOrderSyncFactory
     * @param Data $data
     */
    
    protected $adminSession;
    
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $dataTime,
        \Zoylo\Csquare\Model\OrdersFactory $csquareOrdersFactory,
        \Zoylo\Csquare\Model\OrderSyncFactory $csquareOrderSyncFactory,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Payment\Helper\Data $paymentHelper,
        \Magecomp\Smsfree\Helper\Data $smshelperdata,
        \Magecomp\Smsfree\Helper\Apicall $smshelperapi,
        \Magento\Sales\Model\Order\Email\Container\OrderIdentity $identityContainer,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Zoylo\Csquare\Model\UnicommerceFailedSyncFactory $unicommerceFailedSync,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \I95Dev\BranchInformation\Model\BranchFactory $branchFactory,
        \Magento\Sales\Model\Order $orderModel,
        \Magento\Framework\App\RequestInterface $request, 
        Data $data,
        \Magento\Backend\Model\Session $backendSession,
        \Magento\Backend\Model\Auth\Session $adminSession
    ) {
        $this->_objectManager        = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_dataTime             = $dataTime;
        $this->_csquareOrdersFactory = $csquareOrdersFactory;
        $this->_csquareOrderSyncFactory = $csquareOrderSyncFactory;
        $this->transportBuilder     = $transportBuilder;
        $this->_data            = $data;
        $this->addressRenderer         = $addressRenderer;
        $this->paymentHelper = $paymentHelper;
        $this->smshelperdata = $smshelperdata;
        $this->smshelperapi = $smshelperapi;
        $this->identityContainer = $identityContainer;
        $this->customerFactory = $customerFactory;
        $this->resourceConnection = $resourceConnection;
        $this->unicommerceFailedSync = $unicommerceFailedSync;
        $this->scopeConfig = $scopeConfig;
        $this->branchFactory = $branchFactory;
        $this->orderModel = $orderModel;
        $this->request = $request;
        $this->backendSession = $backendSession;
        $this->adminSession = $adminSession;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order          = $observer->getOrder();
        $status         = $order->getStatus();
        $incrementId    = $order->getIncrementId();
        $orderId        = $order->getId();
        $statusFrontendLabel = str_replace(' CS', '', $order->getStatusLabel());
        $firstLineCont = '';
        $smsContent = '';
        /*$statusQuery = "SELECT label FROM ".$this->resourceConnection->getTableName('sales_order_status_label')." WHERE status = '".$order->getStatus()."'";
        $statusQueryResult = $this->resourceConnection->getConnection()->fetchAll($statusQuery);
        if (isset($statusQueryResult[0]['label']))
        {
            $statusFrontendLabel = $statusQueryResult[0]['label'];    
        }*/
        
        
        if($status == 'order_validated_cs') {
            try{
                $collection = $this->_csquareOrdersFactory->create()->getCollection()
                    ->addFieldtoFilter('source_order_id' , $incrementId);
                if(count($collection) == 0 ){
                   
                    //Insert Records
                    $csquareOrderModel = $this->_csquareOrdersFactory->create();
                    $csquareOrderModel->setSourceOrderId($incrementId);
                    $csquareOrderModel->setEntityId($orderId);
                    $csquareOrderModel->setCreatedAt($this->_dataTime->gmtDate());
                    $csquareOrderModel->setUpdatedAt($this->_dataTime->gmtDate());
                    $csquareOrderModel->setStatus(1);
                    $csquareOrderModel->save();

                    $dataLog = $this->_data;
                    $entity = 'Ordersync Of '.$incrementId; // based on entites we need to declre in xml file and read here dynamically
                    $dataLog->infoLog(__METHOD__, "Ecogreen " . $entity . " Started ". date('Y-m-d H:i:s'), 'OrderSync');
                    // API Call
                    $apiCallResponse = $this->_csquareOrderSyncFactory->create()->orderSync(1);
                    $dataLog->infoLog(__METHOD__, "Ecogreen " . $entity . " Ended ". date('Y-m-d H:i:s'), 'OrderSync');

                }
                
                $condDataAll = $this->getConditionalRData($order);
                if (isset($condDataAll['payment_method']) && isset($condDataAll['prescription_required']))
                {
                   if ($condDataAll['payment_method'] == 'cashondelivery' && $condDataAll['prescription_required'] == TRUE)
                   {
                       $firstLineCont = ". Your order will be dispatched within 36 hours and we will share tracking details once your order is shipped.";
                       //$smsContent = "Your zoylo order". $order->getIncrementId()." has been validated. Your order will be dispatched within 36 hours and we will share tracking details once your order is shipped.";
                       $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status."_cod");
                       $this->sendSMS($order,$statusFrontendLabel,$smsContent);
                       $this->sendEmail($order,$statusFrontendLabel,$status,$firstLineCont);
                   }
                   
                   if ($condDataAll['payment_method'] == 'paylater' && $condDataAll['prescription_required'] == TRUE)
                   {
                       $firstLineCont = "and we will share the payment link with you shortly on your registered mobile number. Your order will be dispatched within 36 hours and we will share tracking details once your order is shipped.";
                       //$smsContent = "Your zoylo order". $order->getIncrementId()." has been validated and we will share the payment link with you shortly on your registered mobile number. ";
                       $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status."_prepaid");
                       $this->sendSMS($order,$statusFrontendLabel,$smsContent);
                       $this->sendEmail($order,$statusFrontendLabel,$status,$firstLineCont);
                   }
                }
                
                
            }catch(\Exception $e){
                echo $e->getMessage();
            }
        }
        
        else if ($status == 'pending_payment_cs')
        {
            $condDataAll = $this->getConditionalRData($order);
            if (isset($condDataAll['payment_method']) && $condDataAll['payment_method'] == 'cashondelivery' )
            {
               //$smsContent = "Dear Customer,Your payment for Zoylo order ".$order->getIncrementId()." is converted to COD. Please pay Rs.".$order->getIncrementId()." to the delivery executive. In case of any queries or assistance, please contact us.";
               $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status."_cod");
               $firstLineCont = "is converted to COD. Please pay Rs.".$order->getGrandTotal()." to the delivery executive";
               $this->sendSMS($order,$statusFrontendLabel,$smsContent);
               $this->sendEmail($order,$statusFrontendLabel,$status,$firstLineCont);
            }
            else if(isset($condDataAll['payment_method']))
            {
               //$smsContent = "Dear Customer, Your payment for Zoylo order ".$order->getIncrementId()." is pending. Please pay Rs.<value> by clicking on below url. <url>. Your order will be shipped only after payment. In case of any queries or assistance, please contact us.";
               $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status."_prepaid");
               $firstLineCont = '';
               $this->sendSMS($order,$statusFrontendLabel,$smsContent);
               $this->sendEmail($order,$statusFrontendLabel,$status.'_on',$firstLineCont); 
            }

        }
        else if ($status  == 'order_shipped_cs')
        {
            $tracksCollection = $this->orderModel->load($order->getEntityId())->getTracksCollection();
            if (isset($tracksCollection->getData()[0]['title']))
            {
                $firstLineCont = "";
                //$smsContent = "Your zoylo order ".$order->getIncrementId()." has been shipped through ".$tracksCollection->getData()[0]['title'].". You can track your order using AWB ".$tracksCollection->getData()[0]['track_number'].". ";
                $courier_name = $tracksCollection->getData()[0]['title'];
                $awb_number = $tracksCollection->getData()[0]['track_number'];
                $firstLineCont = str_replace(' ','',$awb_number);
                $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status);
                $smsContent = str_replace('$Courier_name', 'our courier partner', $smsContent);
                $smsContent = str_replace('$awb_number', $awb_number, $smsContent);
                $smsContent = $smsContent.". Track your order by clicking this link https://track.zoylo.com/?waybill=".$awb_number;
                $this->sendSMS($order,$statusFrontendLabel,$smsContent);
                $this->sendEmail($order,$statusFrontendLabel,$status,$firstLineCont);
            }
            else
            {
                $firstLineCont = '.';
                $smsContent = "Your Zoylo order ".$order->getIncrementId()." has been shipped.";
                $this->sendSMS($order,$statusFrontendLabel,$smsContent);
                $this->sendEmail($order,$statusFrontendLabel,$status,$firstLineCont);
            }
        }
        else if ($status == 'return_scan_cs')
        {
            
        }
        else if ($status == 'ready_for_delivery_cs')
        {
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            if ($this->scopeConfig->getValue('clickpost_settings/zoylo/enable_clickpost', $storeScope))
            {
                $this->_data->syncToClickPost($order);
            }
            else
            {
                $this->_data->syncToUniware($order);
            }
        }
        else if ($status == 'on_hold_cs')
        {
            
        }
        else if ($status  == 'shipped_cs')
        {
            
        }
        else if ($status == 'order_processing_cs')
        {
            
        }
        else if ($status == 'return_initiated_cs')
        {
            //$smsContent = "Return on your zoylo order ".$order->getIncrementId()." has been created and is being verified. We will share the return confirmation with you shortly.";
            $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status);
            $this->sendSMS($order,$statusFrontendLabel,$smsContent);
            $this->sendEmail($order,$statusFrontendLabel,$status,'');
        }
        else if ($status == 'ready_for_packing_cs')
        {
            
        }
        else if ($status == 'out_for_delivery_cs')
        {
            //$smsContent = "Your zoylo order ".$order->getIncrementId()." out for delivery through courier_name and will be delivered today by DA";
            $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status);
            $this->sendSMS($order,$statusFrontendLabel,$smsContent);
            $this->sendEmail($order,$statusFrontendLabel,$status,$smsContent);
        }
        else if ($status == 'return_picked_up_cs')
        {
            //$smsContent = "Return pickup on your zoylo order ".$order->getIncrementId()." has been completed. We will initiate your refund once the items reach our facility.";
            $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status);
            $this->sendSMS($order,$statusFrontendLabel,$smsContent);
            $this->sendEmail($order,$statusFrontendLabel,$status,'');
        }
        else if ($status == 'payment_canceled_cs')
        {
            
        }
        else if ($status == 'payment_initiated_cs')
        {
            
        }
        else if ($status == 'validation_on_hold_cs')
        {
            
        }
        else if ($status == 'order_placed')
        {
            //$smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status);
            //die("sms content::".$smsContent);
            //$smsContent = 'test please ignore the order';
            //$this->sendSMS($order,$statusFrontendLabel,$smsContent);
            //$this->sendEmail($order,$statusFrontendLabel,$status,'');
        }
        else if ($status == 'generate_prescription_cs')
        {
            
        }
        else if ($status == 'order_validate_cs')
        {
            $this->sendSMS($order,$statusFrontendLabel);
            $this->sendEmail($order,$statusFrontendLabel,$status,'');
        }
        else if ($status == 'on_hold_validation_cs')
        {
            
        }
        else if ($status == 'refund_processed_cs')
        {
            $condDataAll = $this->getConditionalRData($order);
            if (isset($condDataAll['payment_method']) && $condDataAll['payment_method'] != 'cashondelivery' ) // only prepaid order
            {
                $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status."_prepaid");
                $this->sendSMS($order,$statusFrontendLabel,$smsContent);
                $this->sendEmail($order,$statusFrontendLabel,$status,'');
            }
        }
        else if ($status == 'order_delivered_cs')
        {
            //$smsContent = "Order Delivered: We have successfully delivered your order ".$order->getIncrementId().". Thank you for using Zoylo.";
            $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status);
            $this->sendSMS($order,$statusFrontendLabel,$smsContent);
            $this->sendEmail($order,$statusFrontendLabel,$status,'');
        }
        else if ($status == 'cancelled_no_stock_cs')
        {
            $canceledApiResult = $this->cancelOrder($order);
            
            $condDataAll = $this->getConditionalRData($order);
            if (isset($condDataAll['payment_method']) && $condDataAll['payment_method'] == 'cashondelivery' )
            {
               //$smsContent = "Your zoylo order ".$order->getIncrementId()." has been cancelled upon your request.In case of any queries or assistance, please contact us.";
               $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status."_cod");
               $firstLineCont = ".";
            }
            else if(isset($condDataAll['payment_method']))
            {
               //$smsContent = "Your zoylo order ".$order->getIncrementId()." has been cancelled upon your request. Your refund will be triggered to source in 4-5 working days. In case of any queries or assistance, please contact us.";
               $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status."_prepaid");
               $firstLineCont = " .Your refund will be triggered to source in 4-5 working days"; 
            }
            $this->sendSMS($order,$statusFrontendLabel,$smsContent);
            $this->sendEmail($order,$statusFrontendLabel,$status,$firstLineCont);
        }
        else if ($status == 'order_cancelled_cs')
        {
            
        }
        else if ($status == 'canceled')
        {
            $roleData = $this->adminSession->getUser()->getRole()->getData();
            $userData = $this->adminSession->getUser()->getData();
            $orderCancelledBy = "Zoylo Team";
            if (isset($roleData['role_name']))
            {
                $orderCancelledBy = $orderCancelledBy."#".$roleData['role_name'];
            }
            if (isset($userData['username']))
            {
                $orderCancelledBy = $orderCancelledBy."#".$userData['username'];
            }
            $canceledApiResult = $this->cancelOrder($order);
            

            $condDataAll = $this->getConditionalRData($order);
            $cancelationReason = $this->request->getParam('cancel_reason');
            $isReasonOthers = FALSE;
            if ($cancelationReason == 'others' && $this->request->getParam('cancel_reason_text'))
            {
                $isReasonOthers = TRUE;
                $cancelationReason = $this->request->getParam('cancel_reason_text');
            }
            
            $order->setCancellationReason($cancelationReason);
            $order->setOrderCancelledBy($orderCancelledBy);
            if (isset($condDataAll['payment_method']) && $condDataAll['payment_method'] == 'cashondelivery' )
            {
               //$smsContent = "Your zoylo order ".$order->getIncrementId()." has been cancelled upon your request.In case of any queries or assistance, please contact us.";
               $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status."_cod");
               $smsContent = str_replace('{reason}',$cancelationReason,$smsContent);
               $firstLineCont = "has been cancelled due to ".$cancelationReason.". Your refund, if applicable will be triggered to source in 4-5 working days";
            }
            else if(isset($condDataAll['payment_method']))
            {
               //$smsContent = "Your zoylo order ".$order->getIncrementId()." has been cancelled upon your request. Your refund will be triggered to source in 4-5 working days. In case of any queries or assistance, please contact us.";
               $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status."_prepaid");
               $smsContent = str_replace('{reason}',$cancelationReason,$smsContent);
               $firstLineCont = "has been cancelled due to ".$cancelationReason.". Your refund, if applicable will be triggered to source in 4-5 working days";
            }
            if ($isReasonOthers == TRUE) // separate content of the mail for the cancel reason that are others
            {
                $firstLineCont = "has been cancelled upon your request. Your refund, if applicable will be triggered to source in 4-5 working days";
                $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status."_prepaid");
                $smsContent = str_replace('cancelled due to {reason}','has been cancelled upon your request',$smsContent);
            }
            if ($this->backendSession->getCanceledOrderIdSent() != $order->getEntityId()) // logic to prevent twice triggering of cancellation mail
            {
                $this->sendSMS($order,$statusFrontendLabel,$smsContent);
                $this->sendEmail($order,$statusFrontendLabel,$status,$firstLineCont);    
            }
            $this->backendSession->setCanceledOrderIdSent($order->getEntityId());
        }
        else if ($status == 'undelivered_cs')
        {
            
        }
        else if ($status == 'order_rejected_cs')
        {
            $condDataAll = $this->getConditionalRData($order);
            if (isset($condDataAll['prescription_required']) && $condDataAll['prescription_required'] == TRUE)
            {
                //$smsContent = "We regret to inform that your zoylo order ".$order->getIncrementId()." has been cancelled due to prescription/regulatory issues. ";
                $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status."_rx");
                $this->sendSMS($order,$statusFrontendLabel,$smsContent);
                $this->sendEmail($order,$statusFrontendLabel,$status,$firstLineCont);   
            }
            
        }
        else if ($status == 'ready_to_deliver_cs')
        {
            
        }
        else if ($status == 'invoiced_cs')
        {
            
        }
        else if ($status == 'customer_payment_pending_cs')
        {
            $condDataAll = $this->getConditionalRData($order);
            if (isset($condDataAll['payment_method']) && $condDataAll['payment_method'] == 'cashondelivery' )
            {
               $firstLineCont = "is converted to COD. Please pay Rs.".$order->getGrandTotal()." to the delivery executive";
               $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status."_cod");
               $smsContent = str_replace('<value>',$order->getGrandTotal(),$smsContent);
               $smsContent = str_replace('<url>',"<a href='#'>Url</a>",$smsContent);
            }
            else if(isset($condDataAll['payment_method']))
            {
               //$firstLineCont = " is pending. Please pay Rs.".$order->getGrandTotal()." by clicking on below url. <br> <a href='#'>Pay</a>"; 
               $firstLineCont = " is pending. SMS/E-Mail will be sent to you on your registered mobile number with the payment link to complete the payment."; 
               $smsContent = str_replace('<value>',$order->getGrandTotal(),$smsContent);
               $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($status."_prepaid");
            }
            $this->sendSMS($order,$statusFrontendLabel,$smsContent);
            $this->sendEmail($order,$statusFrontendLabel,$status,$firstLineCont);
        }
        else if ($status == 'order_in_progress_cs')
        {
            
        }
        else if ($status == 'hold_prescription_cs')
        {
            
        }
        else if ($status == 'payment_failed_revise_payment_cs')
        {
            
        }
        else if ($status == 'convert_to_cod_cs')
        {
            
        }
        else if ($status == 'return_approved_cs')
        {
            $orderReturnReason = $this->request->getParam('order_return_reason');
            if ($orderReturnReason == 'others' && $this->request->getParam('order_return_reason_text'))
            {
                $orderReturnReason = $this->request->getParam('order_return_reason_text');
            }
            $order->setOrderReturnReason($orderReturnReason);
            if ($order->getOrderReturnDateTime() == '')
                $order->setOrderReturnDateTime($this->_dataTime->gmtDate());
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            if ($this->scopeConfig->getValue('clickpost_settings/zoylo/enable_clickpost', $storeScope))
            {
                $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($order->getStatus());
                $this->_data->syncReturnOrderToClickPost($order,$orderReturnReason);
                $this->sendEmail($order, $statusFrontendLabel, $status, $firstLineCont);
                $this->sendSMS($order, $statusFrontendLabel, $smsContent);
            }
        }
        else if ($status == 'return_declined_cs')
        {
            $smsContent = $this->smshelperdata->getOrderStatusTemplateForUser($order->getStatus());
            $this->sendEmail($order, $statusFrontendLabel, $status, $firstLineCont);
            $this->sendSMS($order, $statusFrontendLabel, $smsContent);
        }
    }
    
    /**
     * @param Order $order
     * @return string|null
     */
    public function getFormattedShippingAddress($order)
    {
        return $order->getIsVirtual()
            ? null
            : $this->addressRenderer->format($order->getShippingAddress(), 'html');
    }

    /**
     * @param Order $order
     * @return string|null
     */
    public function getFormattedBillingAddress($order)
    {
        return $this->addressRenderer->format($order->getBillingAddress(), 'html');
    }

    /**
     * Get payment info block as html
     *
     * @param Order $order
     * @return string
     */
    public function getPaymentHtml($order)
    {
        return $this->paymentHelper->getInfoBlockHtml(
            $order->getPayment(),
            $this->identityContainer->getStore()->getStoreId()
        );
    }
    
    public function sendEmail($order,$frontendStatus,$statusSet,$firstLineCont)
    {
        $vars = [
            'order' => $order,
            'billing' => $order->getBillingAddress(),
            'payment_html' => $this->getPaymentHtml($order),
            'store' => $order->getStore(),
            'formattedShippingAddress' => $this->getFormattedShippingAddress($order),
            'formattedBillingAddress' => $this->getFormattedBillingAddress($order),
            'frontend_order_status' => $frontendStatus,
            'firstline'=>$firstLineCont
        ];
        
        $sender         = array('email' => 'noreply@zoylo.com','name' => 'Zoylo');
        $transport      = $this->transportBuilder->setTemplateIdentifier($statusSet)
            ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND,'store' => 1])
            ->setTemplateVars($vars)
            ->setFrom($sender)
            ->addTo(array('To' => $order->getCustomerEmail()));
        $transport = $transport->getTransport();
        $transport->sendMessage();
    }
    
    public function sendSMS($order,$statusFrontendLabel,$smsToSend)
    {
        if ($smsToSend != '')
        {
            if ($this->smshelperdata->isEnabled() && $this->smshelperdata->isOrderPlaceForUserEnabled()) {
            $status         = $order->getStatus();
            $customerId     = $order->getCustomerId();
            $customer       = $this->customerFactory->create()->load($customerId);
            $mobile         = $customer->getCustomerNumber();
            $smsToSend = sprintf($smsToSend, $order->getIncrementId());
            $this->smshelperapi->callApiUrl($mobile, $smsToSend);
            return true;
        }
        return false;
        }
        else
        {
            return false;
        }
    }

    public function cancelOrder($order){

        $incrementId = $order->getIncrementId();
        $entityApiClass = $this->_csquareOrdersFactory->create();
        $getRecordsO = $entityApiClass->getCollection()
            ->addFieldtoFilter('source_order_id', $incrementId);             
        $getRecords = $getRecordsO->getData();
        if ($getRecordsO->getSize() > 0) {
            // API Call
            $apiCallResponse = $this->_csquareOrderSyncFactory->create()->cancelOrder($order,$getRecords);
            return true;
        }else{
            return false;
        }
    }
    
    public function getConditionalRData($order){
        $orderCondComb = [];
        $containsPresciption = FALSE;
        $orderPayment = $order->getPayment();
        $paymentMethod = $orderPayment->getMethodInstance();
        $paymentMethodCode = $paymentMethod->getCode();
        $orderAllItems = $order->getAllItems(); 
        if ($orderAllItems) {
            foreach ($orderAllItems as $item) {
                if ( $item->getProduct() && $item->getProduct()->getData('prescription_required') && $item->getProduct()->getData('prescription_required') == 'required')
                {
                   $containsPresciption = TRUE;
                }
            }
        }
        
        $orderCondComb['payment_method'] = $paymentMethodCode;
        $orderCondComb['prescription_required'] = $containsPresciption;

        return $orderCondComb;
    }
}