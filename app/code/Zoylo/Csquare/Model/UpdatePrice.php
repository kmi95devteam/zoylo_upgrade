<?php
namespace Zoylo\Csquare\Model;
use Zoylo\Csquare\Helper\Data;

class UpdatePrice {
	
    /**
     * @var Data
     */
    protected $_data;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dataTime;

    /**
     * scopeConfig for system Congiguration
     *
     * @var string
     */
    protected $_scopeConfig;
    protected $objectManager;
	protected $_productsFactory;

    /**
     * 
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dataTime
     * @param Data $data
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param InvoiceSender $invoiceSender
     */
    public function __construct(
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Framework\Stdlib\DateTime\DateTime $dataTime,
		Data $data,
		\Zoylo\Csquare\Model\ProductsFactory $productsFactory,
		\Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_data = $data;
        $this->_dataTime = $dataTime;
        $this->objectManager = $objectManager;
		$this->_productsFactory = $productsFactory;
    }

    public function updatePrice() {
        $isEnabled = $this->_scopeConfig->getValue('csquare/settings/enabled', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if ($isEnabled) {
           try {
				//API Call
                $apiUrl = $this->_scopeConfig->getValue('csquare/settings/product_price', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                if(!empty($apiUrl)){
					$productsCollection = $this->_productsFactory->create()->getCollection()
						->addFieldtoFilter('status', ['in' => [1]]);
						//$productsCollection->setPageSize(50)->setCurPage(1);
					//echo  $productsCollection->getSelect();die;
					if( $productsCollection->getSize() >0){
						$uCodeList = '';
						$catalogReportIds = [];
						foreach($productsCollection as $product){
							$uCodeList .= "'".$product->getUcodeId()."',";
							$catalogReportIds[$product->getUcodeId()] = $product->getId();
						}
						$uCodeList = rtrim($uCodeList,',');
						//echo $uCodeList;die;
						$requestArray = [
							"idx" =>  100,
							"uCodeList" => $uCodeList,
						];
						$requestText = json_encode($requestArray);
						echo $requestText;
						$ch = curl_init($apiUrl);
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
						curl_setopt($ch, CURLOPT_POSTFIELDS, $requestText);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt($ch, CURLOPT_HTTPHEADER, array(
							'Content-Type: application/json'
							//'Content-Length: ' . strlen($data_string)
							)
						);
						$response = curl_exec($ch);
						//$this->_data->infoLog(__METHOD__, "Ecogreen " . $entity . " has been created " . date('Y-m-d H:i:s'), 'OrderSync');
						$result = json_decode($response, true);
						if(isset($result['status'])){
							$apiStatus = $result['status'];
							$itemDetails = $result['itemDetails'];
							if( $apiStatus == 'success' && count($itemDetails) > 0 ){
								foreach($itemDetails as $key => $value){
									$ucodeId =  $value['itemCode'];
									$price =  $value['mrp'];
									if(array_key_exists($ucodeId, $catalogReportIds)){
										$reportId = $catalogReportIds[$ucodeId];
										$productReport = $this->_productsFactory->create()->load($reportId);
										$productReport->setPrice($price);
										$productReport->setUpdatedAt($this->_dataTime->gmtDate());
										$productReport->setStatus(2);
										$productReport->save();
									}
								}
							}
						}
					}
				}else{
				
				}
            } catch (\Exception $ex) {
				echo $ex->getMessage();die;
				$this->_data->criticalLog(__METHOD__, $ex->getMessage(), "OrderSyncApiException");
			} 
        }
    }
}
