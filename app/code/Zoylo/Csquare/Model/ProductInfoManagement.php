<?php
namespace Zoylo\Csquare\Model;

use Zoylo\Csquare\Helper\Data;
use Zoylo\Csquare\Api\Data\ProductDataInterfaceFactory;
class ProductInfoManagement implements \Zoylo\Csquare\Api\ProductInfoManagementInterface
{
	protected $productsfactory;
	
	/**
	* @var Data
	*/
	protected $data;
	
	
	/**
	* @var \Zoylo\Csquare\Api\Data\ProductDataInterfaceFactory
	*
	*/
	protected $productDataInterfaceFactory;
	
	/**
		* 
		* @param \Magento\Framework\Stdlib\DateTime\DateTime $date
		* @param Data $data
		*
	*/
	
	public function __construct(\Magento\Framework\Stdlib\DateTime\DateTime $date,
	\Zoylo\Csquare\Model\ProductsFactory  $productsfactory,
	Data $data,
	ProductDataInterfaceFactory $productDataInterfaceFactory) {
		$this->date = $date;
		$this->_productsfactory = $productsfactory;
		$this->data = $data;
		$this->productDataInterfaceFactory = $productDataInterfaceFactory;
	} 
	
	/**
		* {@inheritdoc}
	*/
	public function setProductInfo($item)
	{
		$dataLog = $this->data;
		
		try {
			
			$is_enabled = $this->data->isEnabled();
			
			if ($is_enabled == 0) 
			{
				throw new \Exception("Please Enable the C-square Module");
			}
			
			$dataLog->infoLog(__METHOD__, "" . json_encode($item), "ProductInfo");
			$result = [];
			foreach ($item as $key => $items) {
				
				$sku = $items['sku'];
				$ucodeId = $items['ucodeId'];
				$productdata = $items['productdata'];
				$productdata = json_encode($productdata);
				
				$product = $this->_productsfactory->create();
				$product->setSku($sku);
				$product->setUcodeId($ucodeId);
				$product->setProductData($productdata);
				$product->setCreatedAt($this->date->gmtDate());
				$product->setUpdatedAt($this->date->gmtDate());
				$product->setStatus(1);
				$product->save();
				$id = $product->getId();
				$result[] = $id;
				
			}
			
			$message = __('Product Data Info');
			$data = [
			'code'             		=> 1000,
			'success'               => 'true',
			'message'               => $message,
			'ids'  					=> $result
			];
			$result = $this->productDataInterfaceFactory->create();
			$result->setData($data);
			
			
		}
		catch (\Magento\Framework\Exception\LocalizedException $e) {
			$message = $e->getMessage();
			$data = [
			'code'                  => 1000,
			'success'               => 'false',
			'message'               => $message,
			'ids'  					=> ''
			];
			$result = $this->productDataInterfaceFactory->create();
			$result->setData($data);
			$dataLog->criticalLog(__METHOD__, $e->getMessage(), "ProductInfoException");
			} catch (\Exception $e) {
			$message = $e->getMessage();
			$data = [
			'code'                  => 1000,
			'success'               => 'false',
			'message'               => $message,
			'ids'   				=> ''
			];
			$result = $this->productDataInterfaceFactory->create();
			$result->setData($data);
			$dataLog->criticalLog(__METHOD__, $e->getMessage(), "ProductInfoException");
			}catch (CouldNotSaveException $e) {
			$message = $e->getMessage();
			$data = [
			'code'                  => 1000,
			'success'               => 'false',
			'message'               => $message,
			'ids'  					=> ''
			];
			$result = $this->productDataInterfaceFactory->create();
			$result->setData($data);
			$dataLog->criticalLog(__METHOD__, $e->getMessage(), "ProductInfoException");
		}
		
		return $result;
	}
}
