<?php 
namespace Zoylo\Csquare\Model\CsquareToMagento;

use Zoylo\Csquare\Helper\Data;
use Zoylo\Csquare\Model\ProductsFactory;

class Product extends \Magento\Framework\App\Http implements \Magento\Framework\AppInterface {
	
	const I95EXC = 'productApiException';
	const CSQUARELOGNAME = 'CsquareToMagento';

	protected $data;
    protected $_modelEntityUpdateDataFactory;
    protected $productFactory;
    protected $productRepository;
    protected $productFactory1;

    /**
     *
     * @param \Magento\Catalog\Api\ProductRepositoryInterface          $productRepository
     * @param \Magento\Catalog\Model\Product                           $productFactory
     * @param \Magento\Catalog\Model\ProductFactory                    $productFactory1
	 * @param Data                                                     $data
     * @param ProductsFactory                                  $productsFactory
     */
    public function __construct(
		Data $data,
        ProductsFactory $productsFactory,
		\Magento\Catalog\Model\Product $productFactory,
		\Magento\Catalog\Model\ProductFactory $productFactory1
    ) {
		$this->data = $data;
        $this->_productsFactory = $productsFactory;
		$this->productFactory = $productFactory;
		$this->productFactory1 = $productFactory1;
    }
	
	public function createProductsList($arrMsgIds)
    {
	   try {
			foreach ($arrMsgIds as $dataId) {
				 $productErp = $this->_productsFactory->create()
                        ->getCollection()->addFieldtoFilter('id', $dataId)->getData();
						
					if ($productErp) {
					$productErp = isset($productErp[0]) ? $productErp[0] : [];
                    $productErp = json_decode($productErp['product_data'], 1);
                    $this->data->infoLog(__METHOD__, json_encode($productErp), self::CSQUARELOGNAME);
					$shortDescription = isset($productErp["shortDescription"]) ? $productErp["shortDescription"] : "";
                    $description = isset($productErp["description"]) ? $productErp["description"] : "";
					if($productErp["status"] == 'ACTIVE'){
						$status = 1;
					}else{
						$status = 2;
					}
					$simple_productId = $this->productFactory->getIdBySku(trim($productErp["sku"]));
					if (!$simple_productId && $simple_productId == '') {
					 // create new product
						$simple_product = $this->productFactory1->create();
						$simple_product->setSku($productErp["sku"]);
						$simple_product->setAttributeSetId(4);
						$simple_product->setTypeId('simple');
						$simple_product->setWebsiteIds([1]);
						$simple_product->setCategoryIds();
						$simple_product->setName($productErp["name"]);
						$simple_product->setStatus($status);
						$simple_product->setVisibility(
							\Magento\Catalog\Model\Product\Visibility::VISIBILITY_NOT_VISIBLE
						);
						$simple_product->setStockData(
							['use_config_manage_stock' => 0, 'manage_stock' => 1, //manage stock
							'is_in_stock' => 1, 'qty' => $productErp["qty"]
							]
						);
						$simple_product->setDescription($description);
						$simple_product->setShortDescription($shortDescription);
						$simple_product->setSkuGroupName($productErp["sku_group_name"]);
						$simple_product->setSkuDisplayName($productErp["sku_display_name"]);
						$simple_product->setSkuSpeed($productErp["sku_speed"]);
						$simple_product->setManufacturer($productErp["manufacturer"]);
						$simple_product->setUcodePrice($productErp["ucode_price"]);
						$simple_product->setPrice($productErp["price"]);
						$simple_product->setMaxMargin($productErp["max_margin"]);
						$simple_product->setMsrp($productErp["msrp"]);
						$simple_product->setZoyloSkuGroup($productErp["zoylo_sku_group"]);
						$simple_product->setZoyloProductGroup($productErp["zoylo_product_group"]);
						$simple_product->setZoyloProductCategory($productErp["zoylo_product_category"]);
						$simple_product->setZoyloProductSubcat($productErp["zoylo_product_subcat"]);
						$simple_product->setConsumeGroup($productErp["consume_group"]);
						$simple_product->setConsumeType($productErp["consume_type"]);
						$simple_product->setDrugType($productErp["drug_type"]);
						$simple_product->setPackType($productErp["pack_type"]);
						$simple_product->setQtyPerPack($productErp["qty_per_pack"]);
						$simple_product->setPackSize($productErp["pack_size"]);
						$simple_product->setDisplayPackSize($productErp["display_pack_size"]);
						$simple_product->setComposition($productErp["composition"]);
						$simple_product->setDrugSchedule($productErp["drug_schedule"]);
						$simple_product->setPrescriptionRequired($productErp["prescription_required"]);
						$simple_product->setSellableFlag($productErp["sellable_flag"]);
						$simple_product->setHowToUse($productErp["how_to_use"]);
						$simple_product->setIngredients($productErp["ingredients"]);
						$simple_product->setAdditionalInfo($productErp["additional_info"]);
						$simple_product->setIsReturnable($productErp["is_returnable"]);
						$simple_product->setStorageType($productErp["storage_type"]);
						$simple_product->setTopSku($productErp["top_sku"]);
						$simple_product->setQuantityLimit($productErp["quantity_limit"]);
						$simple_product->setHsnCode($productErp["hsn_code"]);
						
						$simple_product->save();
						$simple_productId = $simple_product->getId();
					}else{
					 // update product	
						$simple_product = $this->productFactory1->create()->load($simple_productId);
						$simple_product->setStockData(
                                ['use_config_manage_stock' => 0, 'manage_stock' => 1, //manage stock
                                'is_in_stock' => 1, 'qty' => $productErp["qty"]
                                ]
                            );
						$simple_product->setDescription($description);
						$simple_product->setShortDescription($shortDescription);
						$simple_product->setSkuGroupName($productErp["sku_group_name"]);
						$simple_product->setSkuDisplayName($productErp["sku_display_name"]);
						$simple_product->setSkuSpeed($productErp["sku_speed"]);
						$simple_product->setManufacturer($productErp["manufacturer"]);
						$simple_product->setUcodePrice($productErp["ucode_price"]);
						$simple_product->setPrice($productErp["price"]);
						$simple_product->setMaxMargin($productErp["max_margin"]);
						$simple_product->setMsrp($productErp["msrp"]);
						$simple_product->setZoyloSkuGroup($productErp["zoylo_sku_group"]);
						$simple_product->setZoyloProductGroup($productErp["zoylo_product_group"]);
						$simple_product->setZoyloProductCategory($productErp["zoylo_product_category"]);
						$simple_product->setZoyloProductSubcat($productErp["zoylo_product_subcat"]);
						$simple_product->setConsumeGroup($productErp["consume_group"]);
						$simple_product->setConsumeType($productErp["consume_type"]);
						$simple_product->setDrugType($productErp["drug_type"]);
						$simple_product->setPackType($productErp["pack_type"]);
						$simple_product->setQtyPerPack($productErp["qty_per_pack"]);
						$simple_product->setPackSize($productErp["pack_size"]);
						$simple_product->setDisplayPackSize($productErp["display_pack_size"]);
						$simple_product->setComposition($productErp["composition"]);
						$simple_product->setDrugSchedule($productErp["drug_schedule"]);
						$simple_product->setPrescriptionRequired($productErp["prescription_required"]);
						$simple_product->setSellableFlag($productErp["sellable_flag"]);
						$simple_product->setHowToUse($productErp["how_to_use"]);
						$simple_product->setIngredients($productErp["ingredients"]);
						$simple_product->setAdditionalInfo($productErp["additional_info"]);
						$simple_product->setIsReturnable($productErp["is_returnable"]);
						$simple_product->setStorageType($productErp["storage_type"]);
						$simple_product->setTopSku($productErp["top_sku"]);
						$simple_product->setQuantityLimit($productErp["quantity_limit"]);
						$simple_product->setHsnCode($productErp["hsn_code"]);
					    $simple_product->save();
					 }
					}
			}
	   
	   } catch (Exception $e) {
            $this->data->criticalLog(__METHOD__, $e->getMessage(), self::I95EXC);
        }
	}
}