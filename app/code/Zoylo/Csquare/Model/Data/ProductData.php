<?php

namespace Zoylo\Csquare\Model\Data;

class ProductData extends \Magento\Framework\Model\AbstractModel implements
    \Zoylo\Csquare\Api\Data\ProductDataInterface
{
    const KEY_CODE = 'code';
    const KEY_Success = 'success';
    const KEY_Message = 'message';
    const KEY_Ids = 'ids';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Get Code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->_getData(self::KEY_CODE);
    }

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code)
    {
        return $this->setData(self::KEY_CODE, $code);
    }

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess()
    {
        return $this->_getData(self::KEY_Success);
    }

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success)
    {
        return $this->setData(self::KEY_Success, $success);
    }

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->_getData(self::KEY_Message);
    }

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        return $this->setData(self::KEY_Message, $message);
    }

    /**
     * Get Id
     *
     * @return \Zoylo\Csquare\Api\Data\ProductDataInterface[]
     */
    public function getIds()
    {
        return $this->_getData(self::KEY_Ids);
    }

     /**
     * Set Id
     *
     * @param \Zoylo\Csquare\Api\Data\ProductDataInterface[] $ids
     * @return $this
     */
    public function setIds(array $ids = null)
    {
        return $this->setData(self::KEY_Ids, $ids);
    }
}