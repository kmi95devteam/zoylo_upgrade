<?php
namespace Zoylo\Csquare\Model;
class Facility extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'zoylo_csquare_facility';

	protected $_cacheTag = 'zoylo_csquare_facility';

	protected $_eventPrefix = 'zoylo_csquare_facility';

	protected function _construct()
	{
		$this->_init('Zoylo\Csquare\Model\ResourceModel\Facility');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}