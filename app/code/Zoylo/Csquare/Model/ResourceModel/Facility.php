<?php
namespace Zoylo\Csquare\Model\ResourceModel;


class Facility extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}
	
	protected function _construct()
	{
		$this->_init('facility_code_mapping', 'id');
	}
	
}