<?php
namespace Zoylo\Csquare\Model\Product\Attribute\Source;

class ConsumeType extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
    
		$this->_options=[ ['label'=>'-- Please Select --', 'value'=>''],
			['label'=>'SWALLOW', 'value'=>'swallow'],
			['label'=>'DRINK', 'value'=>'drink'],
			['label'=>'AYURVEDIC', 'value'=>'ayurvedic'],
			['label'=>'CONSUMER', 'value'=>'consumer'],
			['label'=>'APPLY(INT)', 'value'=>'apply_internal'],
			['label'=>'DISPERSIBLE', 'value'=>'dispersible'],
			['label'=>'APPLY(EXT)', 'value'=>'apply_external'],
			['label'=>'SURGICAL', 'value'=>'surgical'],
			['label'=>'GENERAL', 'value'=>'general'],
			['label'=>'MEDICINE', 'value'=>'medicine'],
			['label'=>'CHEW', 'value'=>'chew'],
			['label'=>'CHEWABLE', 'value'=>'CHEWABLE'],
			['label'=>'VETERINARY', 'value'=>'veterinary'],
			['label'=>'SUBLINGUAL', 'value'=>'sublingual'],
			['label'=>'MOUTH DISSOLVE', 'value'=>'mouth_dissolve'],
			['label'=>'INHALATION', 'value'=>'inhalation'],
			['label'=>'Device', 'value'=>'device'],
			['label'=>'Dissolve and drink', 'value'=>'dissolve_and_drink'],
			['label'=>'DISSOLVE IN WATER', 'value'=>'dissolve_in_water'],
			['label'=>'Eatable', 'value'=>'eatable'],
			['label'=>'GARGLE', 'value'=>'gargle'],
			['label'=>'Injectible', 'value'=>'injectible'],
			['label'=>'Insert Internal', 'value'=>'insert_internal'],
			['label'=>'Insert External', 'value'=>'insert_external'],
			['label'=>'FOOD PRODUCTS', 'value'=>'food_products'],
			['label'=>'INSERT', 'value'=>'insert'],
			['label'=>'SPLINTS', 'value'=>'splints'],
			['label'=>'Wear', 'value'=>'wear']

		];
        return $this->_options;
    }
}
