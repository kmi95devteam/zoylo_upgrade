<?php
namespace Zoylo\Csquare\Model\Product\Attribute\Source;

class ConsumeGroup extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        $this->_options=[ ['label'=>'-- Please Select --', 'value'=>''],
			['label'=>'ORAL', 'value'=>'oral'],
			['label'=>'Device', 'value'=>'device'],
			['label'=>'Injectible', 'value'=>'injectible'],
			['label'=>'Insert', 'value'=>'insert'],
			['label'=>'Topical', 'value'=>'topical'],
			['label'=>'AYURVEDIC', 'value'=>'ayurvedic'],
			['label'=>'CONSUMER', 'value'=>'consumer'],
			['label'=>'INTERNAL', 'value'=>'internal'],
			['label'=>'EXTERNAL', 'value'=>'external'],
			['label'=>'SURGICAL', 'value'=>'surgical'],
			['label'=>'GENERAL', 'value'=>'general'],
			['label'=>'MEDICINE', 'value'=>'medicine'],
			['label'=>'VETERINARY PRODUCTS', 'value'=>'veterinary_products'],
			['label'=>'FOOD PRODUCTS', 'value'=>'food_products'],
			['label'=>'WEAR', 'value'=>'wear']
		];
        return $this->_options;
    }
}
