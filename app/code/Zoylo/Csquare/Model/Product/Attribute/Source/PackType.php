<?php
namespace Zoylo\Csquare\Model\Product\Attribute\Source;

class PackType extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
    
		$this->_options=[ ['label'=>'-- Please Select --', 'value'=>''],
						['label'=>'BAG', 'value'=>'bag'],
						['label'=>'BAR', 'value'=>'bar'],
						['label'=>'BOTTLE', 'value'=>'bottle'],
						['label'=>'BOX', 'value'=>'box'],
						['label'=>'CREAM', 'value'=>'cream'],
						['label'=>'CONTAINER', 'value'=>'container'],
						['label'=>'DEVICE', 'value'=>'device'],
						['label'=>'DROPS', 'value'=>'drops'],
						['label'=>'FRIDGE', 'value'=>'fridge'],
						['label'=>'INHALER', 'value'=>'inhaler'],
						['label'=>'INJECTION', 'value'=>'injection'],
						['label'=>'JAR', 'value'=>'jar'],
						['label'=>'LIQUID', 'value'=>'liquid'],
						['label'=>'PACK', 'value'=>'pack'],
						['label'=>'POUCH', 'value'=>'pouch'],
						['label'=>'POWDER', 'value'=>'powder'],
						['label'=>'Sachet', 'value'=>'sachet'],
						['label'=>'STRIP', 'value'=>'strip'],
						['label'=>'STRIPS', 'value'=>'strips'],
						['label'=>'SUGAR STRIPS', 'value'=>'sugar_strips'],
						['label'=>'TETRAPACK', 'value'=>'tetrapack'],
						['label'=>'TIN', 'value'=>'tin'],
						['label'=>'TUBE', 'value'=>'tube'],
						['label'=>'VIAL', 'value'=>'vial'],
						['label'=>'MOUTH PAINT', 'value'=>'mouth_paint'],
						['label'=>'MOUTH WASH', 'value'=>'mouth_wash'],
						['label'=>'NASAL DROPS', 'value'=>'nasal_drops'],
						['label'=>'NASAL SPRAY', 'value'=>'nasal_spray'],
						['label'=>'ENEMA', 'value'=>'enema'],
						['label'=>'EYE DROPS', 'value'=>'eye_drops'],
						['label'=>'EYE EAR DROPS', 'value'=>'eye_ear_drops'],
						['label'=>'GEL(TOPICAL)', 'value'=>'gel_topical'],
						['label'=>'KIT', 'value'=>'kit'],
						['label'=>'DUSTING POWDER', 'value'=>'dusting_powder'],
						['label'=>'OINTMENT', 'value'=>'ointment'],
						['label'=>'PAIN SPRAY', 'value'=>'pain_spray'],
						['label'=>'PASTE', 'value'=>'paste'],
						['label'=>'RESPIRATORY SOLUTIO', 'value'=>'respiratory_solutio'],
						['label'=>'RESPULES', 'value'=>'respules'],
						['label'=>'ROTACAPS', 'value'=>'rotacaps'],
						['label'=>'SHAMPOO', 'value'=>'shampoo'],
						['label'=>'SOAP', 'value'=>'soap'],
						['label'=>'SOLUTION', 'value'=>'solution'],
						['label'=>'SPRAY', 'value'=>'spray'],
						['label'=>'SUPPOSITORIES', 'value'=>'suppositories'],
						['label'=>'VACCINE', 'value'=>'vaccine']
		];
        return $this->_options;
    }
}
