<?php


namespace Zoylo\Csquare\Model\Product\Attribute\Source;

class ShortType extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        $this->_options = [
        
        ];
        return $this->_options;
    }
}
