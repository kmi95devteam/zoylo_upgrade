<?php

namespace Zoylo\Csquare\Model;

class Status
{
    /**#@+
     * Blog Status values
     */
    const STATUS_PENDING = 1;

    const STATUS_PROCESSING = 2;
	
    const STATUS_ERROR = 3;
	
    const STATUS_COMPLETE = 4;
    
    const STATUS_MODIFY = 5;

    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public static function getOptionArray()
    {
        return [self::STATUS_PENDING => __('Pending'), self::STATUS_PROCESSING => __('Processing'), self::STATUS_ERROR => __('Error'), self::STATUS_COMPLETE => __('Complete'), self::STATUS_MODIFY => __('Modify')];
    }

    /**
     * Retrieve option array with empty value
     *
     * @return string[]
     */
    public function getAllOptions()
    {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * Retrieve option text by option value
     *
     * @param string $optionId
     * @return string
     */
    public function getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}