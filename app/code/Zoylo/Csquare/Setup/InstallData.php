<?php
namespace Zoylo\Csquare\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;

class InstallData implements InstallDataInterface
{

    private $eavSetupFactory;

    /**
     * Constructor
     *
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
		
		if (version_compare($context->getVersion(), '2.0.5', '<')) {
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'sku_group_name');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'sku_group_name',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'SKU Group Name',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare',
                'option' => ['values' => [""]]
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'sku_strength');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'sku_strength',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'SKU Strength',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare',
                'option' => ['values' => [""]]
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'consume_type');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'consume_type',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Consume Type',
                'input' => 'select',
                'class' => '',
                'source' => '\Zoylo\Csquare\Model\Product\Attribute\Source\ConsumeType',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare'
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'consume_group');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'consume_group',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Consume Group',
                'input' => 'select',
                'class' => '',
                'source' => '\Zoylo\Csquare\Model\Product\Attribute\Source\ConsumeGroup',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare'
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'qty_per_pack');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'qty_per_pack',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Quantity per Pack',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare',
                'option' => ['values' => [""]]
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'max_margin');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'max_margin',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Max Margin',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare',
                'option' => ['values' => [""]]
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'storage_type');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'storage_type',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Storage Type',
                'input' => 'select',
                'class' => '',
                'source' => '\Zoylo\Csquare\Model\Product\Attribute\Source\StorageType',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare'
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'molecule_name');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'molecule_name',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Molecule Name',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare',
                'option' => ['values' => [""]]
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'molecule_strength');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'molecule_strength',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Molecule Strength',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare',
                'option' => ['values' => [""]]
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'m_strength_unit');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'm_strength_unit',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Molecule Strength Unit',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare',
                'option' => ['values' => [""]]
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'m_short_form');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'm_short_form',
            [
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'Molecule Absorption Type ShortForm',
                'input' => 'select',
                'class' => '',
                'source' => '\Zoylo\Csquare\Model\Product\Attribute\Source\ShortType',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare'
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'m_absorption_type');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'm_absorption_type',
            [
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'Molecule Absorption Type',
                'input' => 'select',
                'class' => '',
                'source' => '\Zoylo\Csquare\Model\Product\Attribute\Source\AbsorptionType',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare'
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'hsn_code');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'hsn_code',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'HSN Code',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare',
                'option' => ['values' => [""]]
            ]
        );
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'ucode_id');	
		$eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'ucode_id',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Ucode ID',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare',
                'option' => ['values' => [""]]
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'sku_id');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'sku_id',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'SKU ID',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare',
                'option' => ['values' => [""]]
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'sku_display_name');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'sku_display_name',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'SKU Display Name',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare',
                'option' => ['values' => [""]]
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'sku_speed');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'sku_speed',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'SKU Speed',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare',
                'option' => ['values' => [""]]
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'sellable_flag');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'sellable_flag',
            [
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'Sellable Flag',
                'input' => 'select',
                'class' => '',
                'source' => '\Zoylo\Csquare\Model\Product\Attribute\Source\SellableFlag',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare'
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'drug_schedule');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'drug_schedule',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Drug Schedule',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare',
                'option' => ['values' => [""]]
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'how_to_use');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'how_to_use',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'How to Use',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare',
                'option' => ['values' => [""]]
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'ingredients');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'ingredients',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Ingredients',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare',
                'option' => ['values' => [""]]
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'additional_info');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'additional_info',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Additional Information',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare',
                'option' => ['values' => [""]]
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'zoylo_sku_group');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'zoylo_sku_group',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Zoylo SKU Group',
                'input' => 'select',
                'class' => '',
                'source' => '\Zoylo\Csquare\Model\Product\Attribute\Source\ZoyloSkuGroup',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare'
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'zoylo_product_group');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'zoylo_product_group',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Zoylo Product Group',
                'input' => 'select',
                'class' => '',
                'source' => '\Zoylo\Csquare\Model\Product\Attribute\Source\ZoyloProductGroup',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare'
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'zoylo_category');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'zoylo_category',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Zoylo Category',
                'input' => 'select',
                'class' => '',
                'source' => '\Zoylo\Csquare\Model\Product\Attribute\Source\ZoyloCategory',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare'
            ]
        );
        
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'zoylo_sub_category');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'zoylo_sub_category',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Zoylo Subcategory',
                'input' => 'text',
                'class' => '',
                'source' => '\Zoylo\Csquare\Model\Product\Attribute\Source\ZoyloSubCategory',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare'
            ]
        );
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'drug_type');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'drug_type',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Drug Type',
                'input' => 'select',
                'class' => '',
                'source' => '\Zoylo\Csquare\Model\Product\Attribute\Source\DrugType',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare'
            ]
        );
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'pack_type');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'pack_type',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Pack Type',
                'input' => 'select',
                'class' => '',
                'source' => '\Zoylo\Csquare\Model\Product\Attribute\Source\PackType',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare'
            ]
        );
		
		
		
			$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'display_pack_size');
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'display_pack_size',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Display Pack Size',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => 1,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => null,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'unique' => false,
                'apply_to' => '',
                'system' => 1,
                'group' => 'CSquare'
            ]
        );
		} 

    }
}
