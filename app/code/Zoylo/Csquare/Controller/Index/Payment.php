<?php

namespace Zoylo\Csquare\Controller\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Payment extends \Magento\Framework\App\Action\Action {

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    protected $helperdata;
    protected $helperapi;
    protected $objectManager;
    protected $_scopeConfig;
    protected $transportBuilder;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
    Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Framework\ObjectManagerInterface $objectManager, \Magecomp\Smsfree\Helper\Data $helperdata, \Magecomp\Smsfree\Helper\Apicall $helperapi, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,\Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->objectManager = $objectManager;
        $this->helperdata = $helperdata;
        $this->helperapi = $helperapi;
        $this->_scopeConfig = $scopeConfig;
        $this->transportBuilder = $transportBuilder;
    }

    /**
     * Index action
     *
     * @return void
     */
    public function execute() {
        try {

            $resultJson = $this->resultJsonFactory->create();
            $params = $this->getRequest()->getParams();
            $order_id = $params['id'];
            $order = $this->objectManager->create('Magento\Sales\Model\Order')->load($order_id);
            $paymentUrl = $order->getPaymentLink();
           
            if (isset($paymentUrl) && $paymentUrl != '') {

                $customer = $this->objectManager->create('Magento\Customer\Model\Customer')->load($order->getCustomerId());
                $message = $this->_scopeConfig->getValue('csquare/settings/payment_notify');
                $sms = sprintf($message, $customer->getFirstname(), $order->getIncrementId(), $order->getBaseGrandTotal(), $paymentUrl);
                $mobile = $customer->getCustomerNumber();
                $this->helperapi->callApiUrl($mobile, $sms);
                $firstLineCont = "Please <a href='".$paymentUrl."'>Click Here to Make Payment</a>";
                $this->sendEmail($order,$firstLineCont);
                return $resultJson->setData(['success' => 'true']);
          
            } else {

                $domains = $this->objectManager->get('Magento\Framework\App\DeploymentConfig')->getConfigData('serverurl');
                if (!empty($domains)) {
                    $domain = $domains['domain'] . '.';
                    $url = "https://" . $domain . $_SERVER['SERVER_NAME'] . "/payment/" . $params['string'];
                } else {
                    $url = "https://$_SERVER[SERVER_NAME]/payment/" . $params['string'];
                }
                
                $data = array("url" => $url);
                $data_string = json_encode($data);
                $apiUrl = $this->_scopeConfig->getValue('csquare/settings/url_shorten');
                $ch = curl_init($apiUrl);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json')
                );

                $result = curl_exec($ch);
                $resultData = json_decode($result, true);

                if ($resultData['success'] == 1) {
                    $shortUrl = $resultData['data']['url'];
                    $order->setPaymentLink($shortUrl);
					$order->addStatusHistoryComment(__('Payment link ( '.$shortUrl.' ) is sent to customer'));
                    $order->save();
                    $customer = $this->objectManager->create('Magento\Customer\Model\Customer')->load($order->getCustomerId());
                    $message = $this->_scopeConfig->getValue('csquare/settings/payment_notify');
                    $sms = sprintf($message, $customer->getFirstname(), $order->getIncrementId(), $order->getBaseGrandTotal(), $shortUrl);
                    $mobile = $customer->getCustomerNumber();
                    $this->helperapi->callApiUrl($mobile, $sms);
					$firstLineCont = "Please <a href='".$shortUrl."'>Click Here to Make Payment</a>";
					$this->sendEmail($order,$firstLineCont);
                    return $resultJson->setData(['success' => 'true']);
                }
            }
        } catch (\Exception $e) {
            return $resultJson->setData(['success' => $e->getMessage()]);
        }
    }
    
    public function sendEmail($order,$firstLineCont)
    {
        $vars = [
            'order' => $order,
            'store' => $order->getStore(),
            'firstline'=>$firstLineCont
        ];
        
        $sender         = array('email' => 'noreply@zoylo.com','name' => 'Zoylo');
        $transport      = $this->transportBuilder->setTemplateIdentifier('customer_payment_pending_link')
            ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND,'store' => 1])
            ->setTemplateVars($vars)
            ->setFrom($sender)
            ->addTo(array('To' => $order->getCustomerEmail()));
        $transport = $transport->getTransport();
        $transport->sendMessage();
    }

}
