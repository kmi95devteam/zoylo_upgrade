<?php
/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Zoylo\Csquare\Controller\Adminhtml\Csquare\Facility;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Builder $productBuilder
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $faciltiyFactory;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Zoylo\Csquare\Model\FacilityFactory $faciltiyFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->faciltiyFactory = $faciltiyFactory;
    }

    
    public function execute()
    {
        $this->_request->getParam('');
        $resultPage = $this->resultPageFactory->create();   
        $resultPage->getConfig()->getTitle()->prepend((__('Facility and Branch Mapping')));
        $facility = $this->faciltiyFactory->create()->load($this->_request->getParam('id'));
        if (!$facility->getId())
        {
            $facility = $this->faciltiyFactory->create();
        }
        $facility->setBranchId($this->_request->getParam('branch_id'));
        $facility->setFacilityCode($this->_request->getParam('facility_code'));
        $facility->setUnicommerceStore($this->_request->getParam('unicommerce_store'));
        $facility->setAddress($this->_request->getParam('address'));
        $facility->setReturnAddress($this->_request->getParam('return_address'));
        $facility->setEmail($this->_request->getParam('email'));
        $facility->setPincode($this->_request->getParam('pincode'));
        $facility->setCity($this->_request->getParam('city'));
        $facility->setCountry('INDIA');
        $facility->setPhone($this->_request->getParam('phone'));
        $facility->setState($this->_request->getParam('state'));
        $facility->setName($this->_request->getParam('name'));
        $facility->save();
        $this->messageManager->addSuccessMessage("Record with branch id:".$this->_request->getParam('branch_id')." updated successfully");
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setUrl($this->_url->getUrl("csquare/csquare/facility_grid"));
        return $resultRedirect;
    }
}
