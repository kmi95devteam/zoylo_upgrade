<?php
/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Zoylo\Csquare\Controller\Adminhtml\Clickpost;

class SyncFailedData extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Builder $productBuilder
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    
    protected $orderModel;
    
    protected $unicommerceFailedSync;
    
    protected $resourceConnection;
    
    protected $orderConvert;
    
    protected $trackFactory;
    
    protected $timezoneDate;

    protected $csquareHelper;
    
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Sales\Model\OrderFactory $orderModel,
        \Zoylo\Csquare\Model\UnicommerceFailedSyncFactory $unicommerceFailedSync,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \I95Dev\BranchInformation\Model\BranchFactory $branchFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneDate,
        \Magento\Sales\Model\Convert\Order $orderConvert,
        \Magento\Sales\Model\Order\Shipment\TrackFactory $trackFactory,
        \Zoylo\Csquare\Helper\Data $csquareHelper
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->orderModel = $orderModel;
        $this->unicommerceFailedSync = $unicommerceFailedSync;
        $this->scopeConfig = $scopeConfig;
        $this->branchFactory = $branchFactory;
        $this->resourceConnection = $resourceConnection;
        $this->orderConvert = $orderConvert;
        $this->trackFactory = $trackFactory;
        $this->timezoneDate = $timezoneDate;
        $this->csquareHelper = $csquareHelper;
    }

    /**
     * Product grid for AJAX request
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $incrementId = $this->_request->getParam('increment_id');
        $order = $this->orderModel->create()->load($incrementId,'increment_id');
        /*try{
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $pickupDate =  $this->timezoneDate->date()->format('Y-m-d'); // default date of today
            if ($this->timezoneDate->date()->format('H:i:s') < $this->timezoneDate->date('10:30:00')->format('H:i:s'))
            {
               $pickupDate = date('Y-m-d',strtotime($this->timezoneDate->date()->format('Y-m-d').' +1 Day'));
            }
            else
            {
                $pickupDate = date('Y-m-d',strtotime($this->timezoneDate->date()->format('Y-m-d').' +2 Day'));
            }
            $pickupDate = date ('Y-m-d H:i:s',strtotime($pickupDate." ".$this->scopeConfig->getValue('clickpost_settings/zoylo/clickpost_pickup_time', $storeScope)));
            $unicommerceFailedMdl = $this->unicommerceFailedSync->create()->load($order->getEntityId(),'order_id');
            if (!$unicommerceFailedMdl->getId())
            {
                $unicommerceFailedMdl = $this->unicommerceFailedSync->create();
            }
            $paymentMethodDT = "COD";
            $codValue = $order->getGrandTotal();
            $condDataAll = $this->getConditionalRData($order);
            $cashPayMethods = array('cashondelivery','free');
            if (isset($condDataAll['payment_method']) && !in_array($condDataAll['payment_method'],$cashPayMethods) )
            {
                $paymentMethodDT = "PREPAID";
                $codValue = 0;
            }
            $shipPinCode = $order->getShippingAddress()->getPostCode();
            $branchCollection = $this->branchFactory->create()->getCollection();
            $branchCollection->getSelect()->joinLeft(array('facility_table'=>$this->resourceConnection->getTableName('facility_code_mapping')),'main_table.branch_code=facility_table.branch_id')->where("main_table.zip_code ='".$shipPinCode."'");
            $facilityCenter = '';
            if (isset($branchCollection->getData()[0]['facility_code']))
            {
                $facilityCenter = $branchCollection->getData()[0]['facility_code'];
            }
            $customerToShipAddress = "";
            $customerToShipAddress = $customerToShipAddress.$order->getShippingAddress()->getFirstname().", ".$order->getShippingAddress()->getLastname();

            $streetAddress = $order->getShippingAddress()->getStreet();
            if (isset($streetAddress[0]) && $streetAddress[0] != '')
            {
                $customerToShipAddress =  $customerToShipAddress.", ".$streetAddress[0];
            }
            if (isset($streetAddress[1]) && $streetAddress[1] != '')
            {
                $customerToShipAddress =  $customerToShipAddress.", ".$streetAddress[1];
            }
            if (isset($streetAddress[2]) && $streetAddress[2] != '')
            {
                $customerToShipAddress =  $customerToShipAddress.", ".$streetAddress[2];
            }
            $customerToShipAddress =  $customerToShipAddress.", ".$order->getShippingAddress()->getCity().", ".$order->getShippingAddress()->getRegion().", ".$order->getShippingAddress()->getPostcode().", INDIA, ".$order->getShippingAddress()->getTelephone();
            $dataString = array(array('pickup_pincode'=>$branchCollection->getData()[0]['pincode'],'drop_pincode'=>$order->getShippingAddress()->getPostcode(),'order_type'=>$paymentMethodDT,'reference_number'=>$order->getIncrementId(),'item'=>'medicines','invoice_value'=>$order->getGrandTotal(),'delivery_type'=>'FORWARD','weight'=>'1','height'=>'1','length'=>'1','breadth'=>'1'));
            $jsonReq = json_encode($dataString);
            $ch = curl_init($this->scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_base_url', $storeScope)."api/v1/recommendation_api/?key=".$this->scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_key', $storeScope));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonReq);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($jsonReq)
            ));
            $result = curl_exec($ch);
            $result = json_decode($result);
            if (isset($result->meta->status) && $result->meta->status == '200' && strtolower($result->meta->message) == 'success')
            {
                $dataStringCreateSalesOrder = new \stdClass();
                $dataStringCreateSalesOrder->pickup_info = new \stdClass();
                if (isset($branchCollection->getData()[0]['state']))
                {
                    $dataStringCreateSalesOrder->pickup_info->pickup_state = $branchCollection->getData()[0]['state'];
                } 
                if (isset($branchCollection->getData()[0]['address']))
                {
                    $dataStringCreateSalesOrder->pickup_info->pickup_address = $branchCollection->getData()[0]['address'];                    
                }
                if (isset($branchCollection->getData()[0]['email']))
                {
                    $dataStringCreateSalesOrder->pickup_info->email = $branchCollection->getData()[0]['email'];                    
                }
                if (isset($branchCollection->getData()[0]['pincode']))
                {
                    $dataStringCreateSalesOrder->pickup_info->pickup_pincode = $branchCollection->getData()[0]['pincode'];                    
                }
                if (isset($branchCollection->getData()[0]['city']))
                {
                    $dataStringCreateSalesOrder->pickup_info->pickup_city = $branchCollection->getData()[0]['city'];                    
                }
                if (isset($branchCollection->getData()[0]['name']))
                {
                    $dataStringCreateSalesOrder->pickup_info->pickup_name = $branchCollection->getData()[0]['name'];                    
                }
                if (isset($branchCollection->getData()[0]['phone']))
                {
                    $dataStringCreateSalesOrder->pickup_info->pickup_phone = $branchCollection->getData()[0]['phone'];                    
                }

                $dataStringCreateSalesOrder->pickup_info->pickup_time = $pickupDate;
                $dataStringCreateSalesOrder->pickup_info->tin = "tin";
                $dataStringCreateSalesOrder->pickup_info->pickup_country = "INDIA";
                $dataStringCreateSalesOrder->drop_info = new \stdClass();
                $dataStringCreateSalesOrder->drop_info->drop_address = $customerToShipAddress;
                $dataStringCreateSalesOrder->drop_info->drop_phone = "******".substr($order->getShippingAddress()->getTelephone(), -4);
                $dataStringCreateSalesOrder->drop_info->drop_country = "INDIA";
                $dataStringCreateSalesOrder->drop_info->drop_state = $order->getShippingAddress()->getRegion();
                $dataStringCreateSalesOrder->drop_info->drop_pincode = $order->getShippingAddress()->getPostcode();
                $dataStringCreateSalesOrder->drop_info->drop_city = $order->getShippingAddress()->getCity();
                $dataStringCreateSalesOrder->drop_info->drop_name = $order->getShippingAddress()->getFirstname()." ".$order->getShippingAddress()->getLastname();
                $dataStringCreateSalesOrder->shipment_details = new \stdClass();
                $dataStringCreateSalesOrder->shipment_details->height = "1";
                $dataStringCreateSalesOrder->shipment_details->order_type = $paymentMethodDT;
                $dataStringCreateSalesOrder->shipment_details->invoice_value = 1;
                $dataStringCreateSalesOrder->shipment_details->invoice_number = $order->getIncrementId();
                foreach ($order->getInvoiceCollection() as $invoice) //at present we are having only one invoice number
                {
                    $dataStringCreateSalesOrder->shipment_details->invoice_number = $invoice->getIncrementId();
                }
                if (isset($order->getInvoiceCollection()->getData()[0]['created_at']))
                {
                    $dataStringCreateSalesOrder->shipment_details->invoice_date = date('Y-m-d',  strtotime($order->getInvoiceCollection()->getData()[0]['created_at']));;                   
                }
                else // if invoice date is not set then we are sending the today's date 
                {
                    $dataStringCreateSalesOrder->shipment_details->invoice_date = $this->timezoneDate->date()->format('Y-m-d');       
                }
 
                $dataStringCreateSalesOrder->shipment_details->reference_number = $order->getIncrementId();
                $dataStringCreateSalesOrder->shipment_details->length = "1";
                $dataStringCreateSalesOrder->shipment_details->breadth = "1";
                $dataStringCreateSalesOrder->shipment_details->weight = "1";
                $itemsObjData = new \stdClass();
                $itemsObjData->product_url = "";
                $itemsObjData->price = 1;
                $itemsObjData->description = "Medicines";
                $itemsObjData->quantity = "1";
                $itemsObjData->sku = "zoylo-product";
                $itemsObjData->additional = new \stdClass();
                $itemsObjData->additional->images = "";
                $itemsObjData->additional->weight = '1';
                $itemsObjData->additional->return_days = "7";
                $dataStringCreateSalesOrder->shipment_details->items = array($itemsObjData);
                $dataStringCreateSalesOrder->shipment_details->cod_value = $codValue;
                
                if (isset($result->result[0]->preference_array[0]->cp_id))
                {
                    $dataStringCreateSalesOrder->shipment_details->courier_partner = $result->result[0]->preference_array[0]->cp_id;                    
                }
                else
                {
                    $dataStringCreateSalesOrder->shipment_details->courier_partner = 3;                    
                }

                if (isset($result->result[0]->preference_array[0]->account_code))
                {
                    $dataStringCreateSalesOrder->account_code = $result->result[0]->preference_array[0]->account_code;
                }
                else
                {
                    $dataStringCreateSalesOrder->account_code = 'ECOMM Express';                    
                }

                $jsonReqCreateSalesOrder = json_encode($dataStringCreateSalesOrder);
                
                $chCreateSalesOrder = curl_init($this->scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_base_url', $storeScope)."api/v3/create-order/?username=".$this->scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_username', $storeScope)."&key=".$this->scopeConfig->getValue('clickpost_settings/zoylo/clickpost_api_key', $storeScope));
                curl_setopt($chCreateSalesOrder, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($chCreateSalesOrder, CURLOPT_POSTFIELDS, json_encode($dataStringCreateSalesOrder));                                                                  
                curl_setopt($chCreateSalesOrder, CURLOPT_RETURNTRANSFER, true);                                                                      
                curl_setopt($chCreateSalesOrder, CURLOPT_HTTPHEADER, array(                                                                          
                    'Content-Type: application/json',                                                                                
                    'Content-Length: ' . strlen(json_encode($dataStringCreateSalesOrder))
                ));
                $resultCreateSalesOrder = curl_exec($chCreateSalesOrder);
                $resultCreateSalesOrder = json_decode($resultCreateSalesOrder);
                if (isset($resultCreateSalesOrder->meta->status) && $resultCreateSalesOrder->meta->status == '200') // order created successfully on clickpost
                {
                    $unicommerceFailedMdl->setOrderId($order->getEntityId());
                    $unicommerceFailedMdl->setStatus($resultCreateSalesOrder->meta->status);
                    $unicommerceFailedMdl->setMessage($resultCreateSalesOrder->meta->message);
                    $unicommerceFailedMdl->setSortCode($resultCreateSalesOrder->result->sort_code);
                    $unicommerceFailedMdl->setLabel($resultCreateSalesOrder->result->label);
                    $unicommerceFailedMdl->setSecurityKey($resultCreateSalesOrder->result->security_key);
                    $unicommerceFailedMdl->setReferenceNumber($resultCreateSalesOrder->result->reference_number);
                    $unicommerceFailedMdl->setWaybill($resultCreateSalesOrder->result->waybill);
                    $unicommerceFailedMdl->setTrackingId($resultCreateSalesOrder->tracking_id);
                    $unicommerceFailedMdl->setClickpostOrderId($resultCreateSalesOrder->order_id);
                    $unicommerceFailedMdl->setIncrementId($order->getIncrementId());
                    $unicommerceFailedMdl->save();  
                    
                    //code for create shippment starts
                    $order->setState('processing'); // temporary setting this state so that we can create shippment.
                    if ($order->canShip())
                    {
                        // Initialize the order shipment object
                        $convertOrder = $this->orderConvert;
                        $shipment = $convertOrder->toShipment($order);
                        // Loop through order items
                        foreach ($order->getAllItems() AS $orderItemEach) 
                        {
                            // Check if order item has qty to ship or is virtual
                            if (! $orderItemEach->getQtyToShip() || $orderItemEach->getIsVirtual()) 
                            {
                                continue;
                            }
                            $qtyShipped = $orderItemEach->getQtyToShip();
                            // Create shipment item with qty
                            $shipmentItem = $convertOrder->itemToShipmentItem($orderItemEach)->setQty($qtyShipped);
                            // Add shipment item to shipment
                            $shipment->addItem($shipmentItem);
                        }
                        // Register shipment
                        $shipment->register();
                        $shipment->getOrder()->setIsInProcess(true);
                        try {
                            $trackData = array(
                                'carrier_code' => 'ecomexpress',
                                'title' => 'Ecom Express',
                                'number' => $resultCreateSalesOrder->result->waybill, // Replace with your tracking number
                            );

                            $track = $this->trackFactory->create()->addData($trackData);
                            $shipment->addTrack($track)->save();
                            // Save created shipment and order
                            $shipment->save();
                            $shipment->getOrder()->save();

                            // Send email
                            //$objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
                            //    ->notify($shipment);
                            //$shipment->save();
                        } catch (\Exception $e) {
                           echo "Shipment Not Created". $e->getMessage(); exit;
                        }
                    }
                    $order->setState('payment_review'); // resetting the order state back to it's default state
                    echo "Synced to clickpost successfully.";
                    //code for create shippment ends
                }
                else // order could not be created or was already created at clickpost
                {
                    $unicommerceFailedMdl->setOrderId($order->getEntityId());
                    $unicommerceFailedMdl->setIncrementId($order->getIncrementId());
                    $unicommerceFailedMdl->setStatus($resultCreateSalesOrder->meta->status);
                    $unicommerceFailedMdl->setMessage($resultCreateSalesOrder->meta->message);
                    $unicommerceFailedMdl->save();
                    echo "Could not sync to clickpost, please check if order is already synced to click post.";
                }
            }
            else
            {
                $unicommerceFailedMdl->setOrderId($order->getEntityId());
                $unicommerceFailedMdl->setIncrementId($order->getIncrementId());
                $unicommerceFailedMdl->setStatus($result->meta->status);
                $unicommerceFailedMdl->setMessage($result->meta->message);
                $unicommerceFailedMdl->save();
                echo "Could not sync to clickpost.";
            }
            }catch(\Exception $e){
                echo "Exception:".$e->getMessage();
            }*/
            
            if ($this->_request->getParam('is_return_order') == 'on')
            {
                $this->csquareHelper->syncReturnOrderToClickPost($order,$order->getOrderReturnReason());
                echo "Synced Return Order To Clickpost Successfully.";
            }
            else
            {
                $this->csquareHelper->syncToClickPost($order);       
                echo "Synced Order To Clickpost Successfully.";
            }
        

            
    }
    public function getConditionalRData($order){
        $orderCondComb = [];
        $containsPresciption = FALSE;
        $orderPayment = $order->getPayment();
        $paymentMethod = $orderPayment->getMethodInstance();
        $paymentMethodCode = $paymentMethod->getCode();
        $orderAllItems = $order->getAllItems(); 
        if ($orderAllItems) {
            foreach ($orderAllItems as $item) {
                if ( $item->getProduct() && $item->getProduct()->getData('prescription_required') && $item->getProduct()->getData('prescription_required') == 'required')
                {
                   $containsPresciption = TRUE;
                }
            }
        }
        
        $orderCondComb['payment_method'] = $paymentMethodCode;
        $orderCondComb['prescription_required'] = $containsPresciption;

        return $orderCondComb;
    }
}
