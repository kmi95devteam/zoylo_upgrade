<?php
/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Zoylo\Csquare\Controller\Adminhtml\Clickpost\Pickupaddress;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Builder $productBuilder
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $pickupAddressFactory;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Zoylo\Csquare\Model\PickupFacilityFactory $pickupAddressFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->pickupAddressFactory = $pickupAddressFactory;
    }

    
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();   
        $resultPage->getConfig()->getTitle()->prepend((__('Save Pickup Address')));
        $pickupAddress = $this->pickupAddressFactory->create()->load($this->_request->getParam('id'));
        if (!$pickupAddress->getId())
        {
            $pickupAddress = $this->pickupAddressFactory->create();
        }
        $pickupAddress->setAddress($this->_request->getParam('address'));
        $pickupAddress->setEmail($this->_request->getParam('email'));
        $pickupAddress->setPincode($this->_request->getParam('pincode'));
        $pickupAddress->setCity($this->_request->getParam('city'));
        $pickupAddress->setCountry('INDIA');
        $pickupAddress->setPhone($this->_request->getParam('phone'));
        $pickupAddress->setState($this->_request->getParam('state'));
        $pickupAddress->setName($this->_request->getParam('name'));
        $pickupAddress->save();
        $this->messageManager->addSuccessMessage("Record with pick up address id:".$this->_request->getParam('id')." updated successfully");
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setUrl($this->_url->getUrl("csquare/clickpost/pickupaddress_grid"));
        return $resultRedirect;
    }
}
