<?php
/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Zoylo\Csquare\Controller\Adminhtml\Unicommerce;

class SyncFailedData extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Builder $productBuilder
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    
    protected $orderModel;
    
    protected $unicommerceFailedSync;
    
    protected $resourceConnection;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Sales\Model\OrderFactory $orderModel,
        \Zoylo\Csquare\Model\UnicommerceFailedSyncFactory $unicommerceFailedSync,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \I95Dev\BranchInformation\Model\BranchFactory $branchFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->orderModel = $orderModel;
        $this->unicommerceFailedSync = $unicommerceFailedSync;
        $this->scopeConfig = $scopeConfig;
        $this->branchFactory = $branchFactory;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Product grid for AJAX request
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $incrementId = $this->_request->getParam('increment_id');
        $order = $this->orderModel->create()->load($incrementId,'increment_id');
        try{
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $headerDetailsCh = curl_init("https://zdpl.unicommerce.com/oauth/token?grant_type=password&client_id=my-trusted-client&username=".$this->scopeConfig->getValue('unicommerce_settings/zoylo/unicommerce_email', $storeScope)."&password=".$this->scopeConfig->getValue('unicommerce_settings/zoylo/unicommerce_password', $storeScope));
            curl_setopt($headerDetailsCh, CURLOPT_CUSTOMREQUEST, "GET");   
            curl_setopt($headerDetailsCh,CURLOPT_RETURNTRANSFER,1);
            $tokenResult = curl_exec($headerDetailsCh);
            $tokenResult2 = explode(',',$tokenResult);
            $tokenResult3 = json_encode($tokenResult2[0]);
            $tokenFinal = explode(":",$tokenResult2[0])[1];
            $tokenFinal = str_replace('"', '', $tokenFinal);
            curl_close($headerDetailsCh);

            $cashOnDelivery = true;
            $condDataAll = $this->getConditionalRData($order);
            $cashPayMethods = array('cashondelivery','free');
            if (isset($condDataAll['payment_method']) && !in_array($condDataAll['payment_method'],$cashPayMethods) )
            {
                $cashOnDelivery = false;
            }
            $allOrderItems = $order->getAllItems();
            $orderItemData = array();
            $shipPinCode = $order->getShippingAddress()->getPostCode();
            $branchCollection = $this->branchFactory->create()->getCollection();
            $branchCollection->getSelect()->joinLeft(array('facility_table'=>$this->resourceConnection->getTableName('facility_code_mapping')),'main_table.branch_code=facility_table.branch_id')->where("main_table.zip_code ='".$shipPinCode."'");
            $facilityCenter = '';
            if (isset($branchCollection->getData()[0]['facility_code']))
            {
                $facilityCenter = $branchCollection->getData()[0]['facility_code'];
            }
            /*foreach($allOrderItems as $eachROrderItem) //keep this code commented until testing is going on, uncomment when we get go ahead for making the code dynamic
            {
                $orderItemData[] = array("itemSku"=>$eachROrderItem->getSku(),"sellingPrice"=>$eachROrderItem->getPrice(),"shippingMethodCode"=>"STD","totalPrice"=>$eachROrderItem->getRowTotal(),"code"=>"90004443","facilityCode"=>"VIVA-MASSABTANK");
            }*/
            //10200375
            //$facilityCenter = 'Warehouse';
            //die('firstnmae:'.$order->getShippingAddress()->getFirstName());
            //die('discount:'.$order->getDiscountAmount());
            $streetArr = $order->getShippingAddress()->getStreet();
            $street1 = "";
            if (isset($streetArr[0]))
            {
                $street1 = $streetArr[0];
            }
            $street2 = "";
            if (isset($streetArr[1]))
            {
                $street2 = $streetArr[1];
            }
            //die('street1:'.$street1."::street::".$street2);
            $orderItemData[] = array("itemSku"=>'99999999',"sellingPrice"=>$order->getSubtotal()+$order->getDiscountAmount(),"shippingMethodCode"=>"STD","discount"=>abs($order->getDiscountAmount()),"shippingCharges"=>$order->getShippingAmount(),"totalPrice"=>$order->getGrandTotal(),"code"=>"90004443","facilityCode"=>$facilityCenter);
            $dataString = array("saleOrder"=>array("code"=>$order->getIncrementId(),"displayOrderCode"=>$order->getIncrementId(),"cashOnDelivery"=>$cashOnDelivery,"addresses"=>array(array("id"=>"0","name"=>$order->getCustomerFirstname()." ".$order->getCustomerLastname() ,"addressLine1"=>$street1,"addressLine2"=>$street2,"city"=>$order->getShippingAddress()->getCity(),"state"=>$order->getShippingAddress()->getRegion(),"country"=>"India","pincode"=>$order->getShippingAddress()->getPostCode(),"phone"=>$order->getShippingAddress()->getTelephone())),"billingAddress"=>array("referenceId"=>0),"shippingAddress"=>array("referenceId"=>0),"saleOrderItems"=>$orderItemData));
            $jsonReq = json_encode($dataString);
            $ch = curl_init("https://zdpl.unicommerce.com/services/rest/v1/oms/saleOrder/create");
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonReq);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($jsonReq),
                'Authorization: Bearer '.$tokenFinal)
            );
            $result = curl_exec($ch);
            //echo "<pre>";
            //die($result);
            $responseString = str_replace('\n', '', $result);
            $responseString = rtrim($responseString, ',');
            $responseString = "[" . trim($responseString) . "]";
            $json = json_decode($responseString, true);

            if (curl_errno($ch)) { // if curl request failed
                $unicommerceFailedMdl = $this->unicommerceFailedSync->create()->load($order->getEntityId(),'order_id');
                if (!$unicommerceFailedMdl->getId())
                {
                    $unicommerceFailedMdl = $this->unicommerceFailedSync->create();
                }
                $unicommerceFailedMdl->setOrderId($order->getEntityId());
                $unicommerceFailedMdl->setStatus(0);
                $unicommerceFailedMdl->setMessage('Curl Connection Failed');
                $unicommerceFailedMdl->save();
                //die("error in curl");
            }
            else if ( isset($json[0]['successful']) && $json[0]['successful'] == true  ){ // everything correct
                $unicommerceFailedMdl = $this->unicommerceFailedSync->create()->load($order->getEntityId(),'order_id');
                if (!$unicommerceFailedMdl->getId())
                {
                    $unicommerceFailedMdl = $this->unicommerceFailedSync->create();
                }
                $unicommerceFailedMdl->setOrderId($order->getEntityId());
                $unicommerceFailedMdl->setStatus(1);
                $unicommerceFailedMdl->setMessage('Synced Successfully.');
                $unicommerceFailedMdl->save();
                echo "Successfully Synced.";
            }
            else{ // something wrong
                $unicommerceFailedMdl = $this->unicommerceFailedSync->create()->load($order->getEntityId(),'order_id');
                if (!$unicommerceFailedMdl->getId())
                {
                    $unicommerceFailedMdl = $this->unicommerceFailedSync->create();
                }
                $unicommerceFailedMdl->setOrderId($order->getEntityId());
                $unicommerceFailedMdl->setStatus(0);
                $unicommerceFailedMdl->setMessage('Failed To Sync To Unicommerce. Reason: '.$result);
                $unicommerceFailedMdl->save();
                echo "Failed to sync the order to unicommerce.";
            }
            curl_close($ch);
            }catch(\Exception $e){
                echo "Exception:".$e->getMessage();
            }
    }
    public function getConditionalRData($order){
        $orderCondComb = [];
        $containsPresciption = FALSE;
        $orderPayment = $order->getPayment();
        $paymentMethod = $orderPayment->getMethodInstance();
        $paymentMethodCode = $paymentMethod->getCode();
        $orderAllItems = $order->getAllItems(); 
        if ($orderAllItems) {
            foreach ($orderAllItems as $item) {
                if ( $item->getProduct() && $item->getProduct()->getData('prescription_required') && $item->getProduct()->getData('prescription_required') == 'required')
                {
                   $containsPresciption = TRUE;
                }
            }
        }
        
        $orderCondComb['payment_method'] = $paymentMethodCode;
        $orderCondComb['prescription_required'] = $containsPresciption;

        return $orderCondComb;
    }
}
