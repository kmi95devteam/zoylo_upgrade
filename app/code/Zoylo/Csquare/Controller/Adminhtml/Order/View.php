<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Zoylo\Csquare\Controller\Adminhtml\Order;

use Magento\Backend\App\Action;

class View extends \Magento\Sales\Controller\Adminhtml\Order\View
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Sales::actions_view';

    /**
     * View order detail
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $this->_initOrder();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($order) {
            try {
                $resultPage = $this->_initAction();
                $resultPage->getConfig()->getTitle()->prepend(__('Orders'));
                $qcStatus = $this->getRequest()->getParam('qc');
                $orderId = $order->getId();
                if(isset($qcStatus) && $qcStatus !=""){
                    
                    $user =  $objectManager->get('\Magento\Backend\Model\Auth\Session')->getUser()->getUsername();
                    $qcComments = $this->getRequest()->getParam('comments');
                    if($qcStatus == 'pass'){
                        $order->setTest($user);
                        if(isset($qcComments) && $qcComments!= ''){
                            $string = base64_decode($qcComments);
                            $string = 'QC Passed :&nbsp;'. $string;
                            $order->addStatusHistoryComment($string);
                        }
                        $order->setQcStatus('Pass');
                        $order->setOrderQualityReview($user);
                        $order->setStatus('ready_for_delivery_cs');
                        $order->save();
                    }
                    if($qcStatus == 'fail'){
                        $order->setQcStatus('Failed');
                        $order->setTest($user);
                        if(isset($qcComments) && $qcComments!= ''){
                            $string = base64_decode($qcComments);
                            $string = 'QC Failed :&nbsp;'. $string;
                            $order->addStatusHistoryComment($string); 
                        }
                        $order->save();
                    }
                    $helper = $objectManager->get('Zoylo\Csquare\Helper\Data')->syncToUniware($order);
                    $resultRedirect->setPath('sales/order/view',['order_id' => $orderId]);
                    return $resultRedirect;
                } 
            } catch (\Exception $e) {
                $this->logger->critical($e);
                echo $e->getMessage();
                $this->messageManager->addError($e->getMessage());
                $resultRedirect->setPath('sales/order/index');
                return $resultRedirect;
            }
            $resultPage->getConfig()->getTitle()->prepend(sprintf("#%s", $order->getIncrementId()));
            return $resultPage;
        }
        $resultRedirect->setPath('sales/*/');
        return $resultRedirect;
    }
}
