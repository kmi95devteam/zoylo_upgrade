<?php
namespace Zoylo\Csquare\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;

class MassStatus extends \Magento\Backend\App\Action
{
    /**
     * Update blog post(s) status action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $product_ids = $this->getRequest()->getParam('product_ids');
        if (!is_array($product_ids) || empty($product_ids)) {
            $this->messageManager->addError(__('Please select the record.'));
        } else {
            try {
                $status = (int) $this->getRequest()->getParam('status');
                foreach ($product_ids as $product_id) {
                    $CsqProductModel = $this->_objectManager->get('Zoylo\Csquare\Model\Products')->load($product_id);
                    $CsqProductModel->setStatus($status)->save();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been updated.', count($product_ids))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory->create()->setPath('csquare/*/index');
    }

}
