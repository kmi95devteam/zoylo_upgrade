<?php
namespace Zoylo\Csquare\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Data extends \Magento\Backend\App\Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    protected $_productsFactory;
    /**
     * @param Context     $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
		\Zoylo\Csquare\Model\ProductsFactory $productsFactory) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
		$this->_productsFactory = $productsFactory;
		}

    /**
     * Customer groups list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
		$data = $this->getRequest()->getPostValue();
        $dataId = $data['data_id'];
        $mesageQueueData = $this->_productsFactory->create()->load($dataId)->getData();
        $data = $mesageQueueData['product_data'];
		$data = json_encode(json_decode($data), JSON_PRETTY_PRINT);
		$this->getResponse()->setBody($data);
    }
}
