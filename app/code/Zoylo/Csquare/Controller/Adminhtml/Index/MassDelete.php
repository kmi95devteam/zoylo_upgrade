<?php
namespace Zoylo\Csquare\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;

/**
 * Class MassDelete
 */
class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $product_ids = $this->getRequest()->getParam('product_ids');
		
        if (!is_array($product_ids) || empty($product_ids)) {
            $this->messageManager->addError(__('Please select the record.'));
        } else {
            try {
                foreach ($product_ids as $product_id) {
                    $CsqProductModel = $this->_objectManager->get('Zoylo\Csquare\Model\Products')->load($product_id);
                    $CsqProductModel->delete();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been deleted.', count($product_ids))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory->create()->setPath('csquare/*/index');
    }
}
