<?php
namespace Zoylo\Csquare\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return void
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Zoylo_Csquare::csquare');
        $resultPage->addBreadcrumb(__('Zoylo'), __('Zoylo'));
        $resultPage->addBreadcrumb(__('Csquare Product Reports'), __('Csquare Product Reports'));
        $resultPage->getConfig()->getTitle()->prepend(__('Csquare Product Reports'));
        return $resultPage;
    }
}
