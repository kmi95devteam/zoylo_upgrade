<?php
namespace Zoylo\Csquare\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;

/**
 * Class CreateProduct
 */
class CreateProduct extends \Magento\Backend\App\Action
{
	
	protected $productFactory;
    protected $productFactory1;

    /**
     *
     * @param \Magento\Catalog\Model\Product                           $productFactory
     * @param \Magento\Catalog\Model\ProductFactory                    $productFactory1
     */
    public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Catalog\Model\Product $productFactory,
		\Magento\Catalog\Model\ProductFactory $productFactory1
    ) {
		parent::__construct($context);
		$this->productFactory = $productFactory;
		$this->productFactory1 = $productFactory1;
    }
	
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
		$ids = array();
        $product_ids = $this->getRequest()->getParam('product_ids');
	//	$product_ids = explode(",", $ids);
	//	echo "<pre>"; print_r($product_ids);exit;
        if (!is_array($product_ids) || empty($product_ids)) {
            $this->messageManager->addError(__('Please select the record.'));
        } else { 
            try {
                foreach ($product_ids as $product_id) {
                    $CsqProductModel = $this->_objectManager->get('Zoylo\Csquare\Model\Products')->load($product_id);
                    $prodSku = $CsqProductModel->getSku();
                    $ucodeId = $CsqProductModel->getUcodeId();
                    $prodData = $CsqProductModel->getProductData();
					$prodData = json_decode($prodData);
					// echo "<pre>";print_r($prodData); echo $prodData->sku; exit;
					$simple_productId = $this->productFactory->getIdBySku(trim($prodSku));
					if (!$simple_productId && $simple_productId == '') {
					 // create new product
					 
						$simple_product = $this->productFactory1->create();
						$simple_product->setSku($prodData->sku);
						$simple_product->setAttributeSetId(4);
						$simple_product->setTypeId('simple');
						$simple_product->setWebsiteIds([1]);
						$simple_product->setCategoryIds();
						$simple_product->setName($prodData->name);
					//	$simple_product->setStatus($prodData->status);
						$simple_product->setVisibility(
							\Magento\Catalog\Model\Product\Visibility::VISIBILITY_NOT_VISIBLE
						);
						$simple_product->setStockData(
							['use_config_manage_stock' => 0, 'manage_stock' => 1, //manage stock
							'is_in_stock' => 1, 'qty' => $prodData->qty
							]
						);
					//	$simple_product->setDescription($description);
					//	$simple_product->setShortDescription($shortDescription);
					//	$simple_product->setSkuGroupName($prodData->sku_group_name);
					//	$simple_product->setSkuDisplayName($prodData->sku_display_name);
					//	$simple_product->setSkuSpeed($prodData->sku_speed);
					//	$simple_product->setManufacturer($prodData->manufacturer);
						$simple_product->setUcodeId($ucodeId);
						$simple_product->setPrice($prodData->price);
					//	$simple_product->setMaxMargin($prodData->max_margin);
					//	$simple_product->setMsrp($prodData->msrp);
						$simple_product->setZoyloSkuGroup($prodData->zoylo_sku_group);
						$simple_product->setZoyloProductGroup($prodData->zoylo_product_group);
					//	$simple_product->setZoyloProductCategory($prodData["zoylo_product_category);
					//	$simple_product->setZoyloProductSubcat($prodData["zoylo_product_subcat);
						$simple_product->setConsumeGroup($prodData->consume_group);
						$simple_product->setConsumeType($prodData->consume_type);
						$simple_product->setDrugType($prodData->drug_type);
						$simple_product->setPackType($prodData->pack_type);
						$simple_product->setQtyPerPack($prodData->qty_per_pack);
						$simple_product->setPackSize($prodData->pack_size);
					//	$simple_product->setDisplayPackSize($prodData->display_pack_size);
						$simple_product->setSaltComposition($prodData->salt_composition);
					//	$simple_product->setDrugSchedule($prodData->drug_schedule);
					//	$simple_product->setPrescriptionRequired($prodData->prescription_required);
					//	$simple_product->setSellableFlag($prodData->sellable_flag);
					//	$simple_product->setReturnable($prodData->returnable);
					//	$simple_product->setStorageType($prodData->storage_type);
					//	$simple_product->setTopSku($prodData->top_sku);
						$simple_product->setHsnCode($prodData->hsn_code);
						
						$simple_product->save();
						$simple_productId = $simple_product->getId();
						$CsqProductModel = $this->_objectManager->get('Zoylo\Csquare\Model\Products')->load($product_id);
						$CsqProductModel->setStatus(4);
						$CsqProductModel->setResponse($simple_productId);
						$CsqProductModel->save();
                }
                $this->messageManager->addSuccess(
                    __('Product Id %1 has been created.', $simple_productId)
                );
            } 
        }catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
         }
        return $this->resultRedirectFactory->create()->setPath('csquare/*/index');
    }
}

}
