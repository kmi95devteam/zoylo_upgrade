<?php
namespace Zoylo\Csquare\Controller\Adminhtml\OnHoldOrders;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class ExportOnHoldReportCsv extends \Magento\Reports\Controller\Adminhtml\Report\Review
{
    public function execute()
    {
        $fileName = 'sku_on_hold_report.csv';
        $content = $this->_view->getLayout()->createBlock(
            'Zoylo\Csquare\Block\Adminhtml\OnHoldOrders\Grid'
        )->getCsv();

        return $this->_fileFactory->create($fileName, $content, DirectoryList::VAR_DIR);
    }

}
