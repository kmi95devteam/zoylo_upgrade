<?php
/**
* Zoylo_AdminUserRole - Version 1.0.0
* Copyright © 2019 Zoylo All rights reserved.
* Author : Ajeshbabu Gowrisetti
*/
namespace Zoylo\AdminUserRole\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Psr\Log\LoggerInterface as Logger;
class ConfigObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var Logger
     */
    protected $logger;
    /**
     * @var userFactory
     */
    protected $userFactory;
    /**
     * @var userCollectionFactory
     */
    protected $userCollectionFactory;
    /**
     * @var scopeConfigInterface
     */
    protected $scopeConfigInterface;

    /**
     * @param Logger $logger
     */
    public function __construct(
        Logger $logger,
        \Magento\User\Model\UserFactory $userFactory,
        \Magento\User\Model\ResourceModel\User\CollectionFactory $userCollectionFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface
    ) {
        $this->logger = $logger;
        $this->userFactory = $userFactory;
        $this->userCollectionFactory = $userCollectionFactory;
        $this->scopeConfigInterface = $scopeConfigInterface;
    }
    /*
     * update user role name with value when onfiguration save
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $adminUserRoleEnableStatus =$this->scopeConfigInterface->getValue('adminuserrole/adminuserrole_configurations/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $adminUserUpdateRolenameStatus =$this->scopeConfigInterface->getValue('adminuserrole/adminuserrole_configurations/updaterolename', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if($adminUserRoleEnableStatus==1 && $adminUserUpdateRolenameStatus==1){
            //$userFactory = $this->userFactory->create();
            $userCollectionFactory = $this->userCollectionFactory->create();
            
            //Active users for
            $userCollectionFactory->addFieldToFilter('is_active',1);
            $userIdsActive = array();
            foreach($userCollectionFactory as $user){
                $userIdsActive[] = $user['user_id'];
            }
            //echo "<pre>";print_r($userIdsActive);
            if(!empty($userIdsActive)){
                $roleName='';
                foreach($userIdsActive as $uid){
                    //echo "<br>".$uid;
                    if($uid!=''){
                        $user = $this->userFactory->create()->load($uid);
                        $role = $user->getRole();
                        $roleData = $role->getData();
                        
                        if (isset($roleData['role_name']) && $roleData['role_name']!='') {
                            $roleName = $roleData['role_name'];
                            $user->setUserRoleName($roleName);
                            $user->save();
                        }
                    }
                }
            }

            //Inactive users for
            $userCollectionFactory = $this->userCollectionFactory->create();
            $userCollectionFactory->addFieldToFilter('is_active',0);
            $userIdsInActive = array();
            foreach($userCollectionFactory as $user){
                $userIdsInActive[] = $user['user_id'];
            }
            //echo "<pre>";print_r($userIdsInActive);
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('admin_user');
            if(!empty($userIdsInActive)){
                foreach($userIdsInActive as $inactiveUid){
                    if($inactiveUid!=''){
                        $user = $this->userFactory->create()->load($inactiveUid);
                        $role = $user->getRole();
                        $roleData = $role->getData();
                        //$roleName = $roleData['role_name'];
                        if(isset($roleData['role_name']) && $roleData['role_name']!='') {
                            $roleName = $roleData['role_name'];
                            $sql = "Update $tableName SET user_role_name = '".$roleName."' where user_id = ".$inactiveUid;
                            $connection->query($sql);
                        }
                        
                    }
                    
                }
            }    
        }    
    }    
}