<?php
namespace Zoylo\Webservices\Model\Autocomplete;

use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Helper\Image;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\Search\SearchCriteriaFactory as FullTextSearchCriteriaFactory;
use Magento\Framework\Api\Search\SearchInterface as FullTextSearchApi;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Search\Model\Autocomplete\DataProviderInterface;
use Magento\Search\Model\Autocomplete\ItemFactory;
use Magento\Search\Model\QueryFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\Api\SortOrder;
/**
 *  Full text search implementation of autocomplete.
 *
 * @package        Zoylo\Webservices
 * @author         srinivasrao.p@zoylo.com
 * @copyright      Copyright (c) 2018, Zoylo. All rights reserved
 */
class SearchDataProvider implements DataProviderInterface
{
    const PRODUCTS_NUMBER_IN_SUGGEST = 5;

    /** @var QueryFactory */
    protected $queryFactory;

    /** @var ItemFactory */
    protected $itemFactory;

    /** @var \Magento\Framework\Api\Search\SearchInterface */
    protected $fullTextSearchApi;

    /** @var FullTextSearchCriteriaFactory */
    protected $fullTextSearchCriteriaFactory;

    /** @var FilterGroupBuilder */
    protected $searchFilterGroupBuilder;

    /** @var FilterBuilder */
    protected $filterBuilder;

    /** @var ProductRepositoryInterface */
    protected $productRepository;

    /** @var SearchCriteriaBuilder */
    protected $searchCriteriaBuilder;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var ProductHelper
     */
    protected $productHelper;

    /** @var \Magento\Catalog\Helper\Image */
    protected $imageHelper;

    /** @var \Magento\Catalog\Model\Product\Attribute\Repository $_productAttributeRepository */
    protected $_productAttributeRepository;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Framework\Api\SortOrderBuilder $sortOrderBuilder
     */
    protected $sortOrderBuilder;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $stockState;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
    */
    protected $_productCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
    */
    protected $_categoryFactory;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * Initialize dependencies.
     *
     * @param QueryFactory                                      $queryFactory
     * @param ItemFactory                                       $itemFactory
     * @param FullTextSearchApi                                 $search
     * @param FullTextSearchCriteriaFactory                     $searchCriteriaFactory
     * @param FilterGroupBuilder                                $searchFilterGroupBuilder
     * @param FilterBuilder                                     $filterBuilder
     * @param ProductRepositoryInterface                        $productRepository
     * @param SearchCriteriaBuilder                             $searchCriteriaBuilder
     * @param StoreManagerInterface                             $storeManager
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\Catalog\Helper\Image                     $imageHelper
     * @param \Magento\Catalog\Model\Product\Attribute\Repository $productAttributeRepository
     */
    public function __construct(
        QueryFactory $queryFactory,
        ItemFactory $itemFactory,
        FullTextSearchApi $search,
        FullTextSearchCriteriaFactory $searchCriteriaFactory,
        FilterGroupBuilder $searchFilterGroupBuilder,
        FilterBuilder $filterBuilder,
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        StoreManagerInterface $storeManager,
        PriceCurrencyInterface $priceCurrency,
        Image $imageHelper,
        StockRegistryInterface $stockState,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Api\SortOrderBuilder $sortOrderBuilder,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Catalog\Model\Product\Attribute\Repository $productAttributeRepository
    )
    {
        $this->queryFactory                  = $queryFactory;
        $this->itemFactory                   = $itemFactory;
        $this->fullTextSearchApi             = $search;
        $this->fullTextSearchCriteriaFactory = $searchCriteriaFactory;
        $this->filterBuilder                 = $filterBuilder;
        $this->searchFilterGroupBuilder      = $searchFilterGroupBuilder;
        $this->productRepository             = $productRepository;
        $this->searchCriteriaBuilder         = $searchCriteriaBuilder;
        $this->storeManager                  = $storeManager;
        $this->priceCurrency                 = $priceCurrency;
        $this->imageHelper                   = $imageHelper;
        $this->_productAttributeRepository   = $productAttributeRepository;
        $this->_objectManager                = $objectManager;
        $this->sortOrderBuilder              = $sortOrderBuilder;
        $this->stockState                    = $stockState;
        $this->_categoryFactory              = $categoryFactory;
        $this->_productCollectionFactory     = $productCollectionFactory;
        $this->orderRepository = $orderRepository;
    }

    /**
     * getItems method
     *
     * @return array
     */
    public function getItems()
    {
        $pageSize   = self::PRODUCTS_NUMBER_IN_SUGGEST;
        $pageLimit  = 1;
        $categoryId = '';
        $result     = [ ];
        $query      = $this->queryFactory->get()->getQueryText();
        $productIds = $this->searchProductsFullText($query, $categoryId, $pageSize, $pageLimit);

        // Check if products are found
        if ( $productIds )
        {
            $searchCriteria = $this->searchCriteriaBuilder->addFilter('entity_id', $productIds, 'in')->create();
            $products       = $this->productRepository->getList($searchCriteria);

            foreach ( $products->getItems() as $product )
            {
              if(!$product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue())continue;
                $image = $this->imageHelper->init($product, 'product_page_image_small')->getUrl();

                $resultItem = $this->itemFactory->create([
                    'title'             => $product->getName(),
                    'price'             => $this->priceCurrency->format($product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue(),false),
                    'special_price'     => $this->priceCurrency->format($product->getPriceInfo()->getPrice('special_price')->getAmount()->getValue(),false),
                    'has_special_price' => $product->getSpecialPrice() > 0 ? true : false,
                    'image'             => $image,
                    'url'               => $product->getProductUrl()
                ]);
                $result[]   = $resultItem;
            }
        }
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('catalog_category_entity_varchar'); //gives table name with prefix

        //Select Data from table
        $sql = "Select attribute_id,entity_id FROM " . $tableName ." where value like '%".$query."%'";
        $queryresult = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
        $categoryResult = [ ];
        if(count($queryresult))
        {
            foreach($queryresult as $row)
            {
                if($row['attribute_id']!=42)continue;
                $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $category = $_objectManager->create('Magento\Catalog\Model\Category')
                ->load($row['entity_id']);
                $resultItem = $this->itemFactory->create([
                    'title'             => $category->getName(),
                    'price'             => 0,
                    'special_price'     => 0,
                    'has_special_price' => false,
                    'image'             => '',
                    'url'               => $category->getUrl()
                ]);
                $categoryResult[]   = $resultItem;
            }
        }
        $productCategoryArrayResult = array_merge($result,$categoryResult);
        $manufacturerOptions = $this->_productAttributeRepository->get('product_brand')->getOptions();
        $brandResult = [ ];
        if($manufacturerOptions){
            $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            $baseUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
            foreach ($manufacturerOptions as $manufacturerOption) {
                if(strpos($manufacturerOption->getLabel(),$query)!==false){
                    $brandItem = $this->itemFactory->create([
                        'title'             => $manufacturerOption->getLabel(),
                        'price'             => 0,
                        'special_price'     => 0,
                        'has_special_price' => false,
                        'image'             => '',
                        'url'               => ''
                    ]);
                    $brandResult[]   = $brandItem;
                }
            }
            $productCategoryBrandResult = array_merge($productCategoryArrayResult,$brandResult);
        }
        if(count($brandResult)>0){
            return $productCategoryBrandResult;
        }else{
             return $productCategoryArrayResult;
        }
        die;

        //return $categoryResult;
        //return $result;
    }

    /**
     * Perform full text search and find IDs of matching products.
     *
     * @param $query
     *
     * @return array
     */
    public function searchProductsFullText($query, $categoryId = null, $pageSize = null, $pageLimit = null)
    {
        $productIds    = [ ];
        if($query){
            $searchCriteria = $this->fullTextSearchCriteriaFactory->create();

            /** To get list of available request names see Magento/CatalogSearch/etc/search_request.xml */
            $searchCriteria->setRequestName('quick_search_container');
            $filter      = $this->filterBuilder->setField('search_term')->setValue($query)->setConditionType('like')->create();
            $filterGroup = $this->searchFilterGroupBuilder->addFilter($filter)->create();
           
            $searchCriteria->setFilterGroups([ $filterGroup ]);
                //->setCurrentPage($pageLimit)
                //->setPageSize($pageSize);
            
            $searchResults = $this->fullTextSearchApi->search($searchCriteria);
            
            /**
             * Full text search returns document IDs (in this case product IDs),
             * so to get products information we need to load them using filtration by these IDs
             */
            foreach ( $searchResults->getItems() as $searchDocument )
            {
                $productIds[] = $searchDocument->getId();
            }
        }elseif($categoryId){
            $collection = $this->getProductCollection($categoryId);
            foreach ($collection as $product) {
                $productIds[] = $product->getId();
            }
        }
        return $productIds;
    }
    public function getSearchRelatedItems($searchData)
    {
        $categoryId     = $searchData['category_id'];
        $query          = $searchData['query'];
        $pageSize       = $searchData['page_size'];
        $pageLimit      = $searchData['page_limit'];
        $sortLable      = $searchData['sort_lable'];
        $sortDirection  = $searchData['sort_order'];
        $productBrand   = $searchData['product_brand'];
        $filterArray    = $searchData['filters'];
        $filterBuild    = 0;
        if($query !=''){
            $productsIDs = $this->searchProductsFullText($query, $categoryId, $pageSize, $pageLimit);
            if($productsIDs){
                $filterArray[]  = ['attribute_code' =>'entity_id', 'values' => $productsIDs,'filter_condition' => 'in'];
                $filterArray[]  = ['attribute_code' =>'status', 'values' => 1,'filter_condition' => 'eq'];
                $filterArray[]  = ['attribute_code' =>'visibility', 'values' => 4,'filter_condition' => 'eq'];
                $filterBuild = 1;
            }
        }elseif ($categoryId != '') {
            $filterArray[]  = ['attribute_code' =>'category_id', 'values' => $categoryId,'filter_condition' => 'eq'];
            $filterArray[]  = ['attribute_code' =>'status', 'values' => 1,'filter_condition' => 'eq'];
            $filterArray[]  = ['attribute_code' =>'visibility', 'values' => 4,'filter_condition' => 'eq'];
            $filterBuild = 1;
        }elseif ($productBrand != '') {
            $filterArray[]  = ['attribute_code' =>'product_brand', 'values' => $productBrand,'filter_condition' => 'in'];
            $filterArray[]  = ['attribute_code' =>'status', 'values' => 1,'filter_condition' => 'eq'];
            $filterArray[]  = ['attribute_code' =>'visibility', 'values' => 4,'filter_condition' => 'eq'];
            $filterBuild = 1;
        }
        $result = [ ];
        // Check if products are found
        if ( $filterBuild == 1)
        {
            foreach ($filterArray as $key => $value) {
                $attributeCode      = $value['attribute_code'];
                $filterValue        = $value['values'];
                $filter_condition   = $value['filter_condition'];
                $filter             = $this->filterBuilder->setField($attributeCode)->setValue($filterValue)->setConditionType($filter_condition)->create();
                $filterGroup[]      = $this->searchFilterGroupBuilder->addFilter($filter)->create();
            }
            $searchCriteria = $this->searchCriteriaBuilder->setFilterGroups($filterGroup)->create();
            $sortOrder      = $this->sortOrderBuilder->setField($sortLable)->setDirection($sortDirection)->create();
            $searchCriteria->setSortOrders([$sortOrder]);
            if($pageLimit != '')
                $searchCriteria->setCurrentPage($pageLimit);
            if($pageSize != '')
                $searchCriteria->setPageSize($pageSize);
            //print_r($searchCriteria);die;
            $products       = $this->productRepository->getList($searchCriteria);

            $mediaUrl       = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            $baseUrl        = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);

            foreach ( $products->getItems() as $product )
            {
                $sku = $product->getSku();
                $productRepository = $this->_objectManager->create('Magento\Catalog\Model\ProductRepository');
                $product = $productRepository->get($sku);
                
                if(!$product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue())
                    $regular_price = 0;
                else
                    $regular_price = $product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue();

                //Special Price
				$orgprice = $product->getPrice();
				$specialprice = $product->getSpecialPrice();
				$finalPrice = $product->getFinalPrice();
				if($finalPrice != $orgprice){
					$specialprice = $finalPrice;
				}
				$specialfromdate = $product->getSpecialFromDate();
				$specialtodate = $product->getSpecialToDate();
				$today = time();
				$save_percent = 0;
				if ($specialprice < $orgprice && ($specialprice)) {
					$save_percent = 100 - round(($specialprice / $orgprice) * 100);
				}
				if($save_percent > 0){
					$special_price = $specialprice;
				}else{
					$special_price = null;
				}

                $productType = $product->getAttributeText('zoylo_sku_group');

                $drug_type = $product->getResource()->getAttribute('drug_type')->getFrontend()->getValue($product);
                $drugTypeUrl = '';
                if (isset($drug_type) && !is_array($drug_type) && $drug_type != '' && $drug_type != 'no_selection' ) {
                    $drugTypeUrl = $mediaUrl . 'packtype/' . ucwords(strtolower($drug_type)) . '.png';
                }elseif(strtolower($productType)  == "otc"){
                    $categoryIds= $product->getCategoryIds();
                    $_categoryImgUrl = '';
                    foreach ($categoryIds as $catId){
                        if($catId == 1 || $catId == 2 || $catId == 3 || $catId == 6 ||$catId == 7)
                            continue;
                        
                        $category = $this->_categoryFactory->create()->load($catId);
                        if($category->getLevel() == 3) {
                            $_categoryImgUrl = $category->getImageUrl();
                        }
                    }
                    if($_categoryImgUrl){
                        $drugTypeUrl = $_categoryImgUrl;
                    }else{
                        $drugTypeUrl = $mediaUrl . 'packtype/OTC.png';
                    }
                }

                $image = '';
                if($product->getImage() && $product->getImage() != "no_selection"){
                    $image =  $mediaUrl. 'catalog/product' . $product->getImage();
                }
                if ( $image == '' && $drugTypeUrl) {
                    $image = $drugTypeUrl;
                }

                if($image == ''){
                    $image = $baseUrl.'pub/static/frontend/Smartwave/porto/en_US/Magento_Catalog/images/product/placeholder/image.jpg';
                }
                $manufacture = '';
                $manufacture = $product->getAttributeText('product_brand');

                $stockItem  = $this->stockState->getStockItem(
                        $product->getId(),
                        $product->getStore()->getWebsiteId()
                    );
                
                if($product->getPrescriptionRequired() == "required")
                    $prescription_status = true;
                else
                    $prescription_status = false;

                $productName        = '';
                $name               = $product->getResource()->getAttribute('name')->getFrontend()->getValue($product);
                $displayName        = $product->getResource()->getAttribute('sku_display_name')->getFrontend()->getValue($product);
                if ($displayName == '') {
                    $productName = $name;
                } else {
                    $productName = $displayName;
                }
                
				$sale_status_msg = '';
				$sale_status = false;
				$saleProduct = $product->getSellableFlag();
				if (isset($saleProduct) && $saleProduct == 1) {
					if ($product->isSaleable()) {
						$sale_status = true;
						$sale_status_msg = __('Add To Cart');
					} else {
						$sale_status_msg = __('Out of stock');
					}
				} else {
					if (isset($saleProduct) && $saleProduct == 0) {
						$sale_status_msg = __('Not For Sale');
					} elseif (isset($saleProduct) && $saleProduct == 2) {
						$sale_status_msg = __('Discontinued');
					} else {
						if ($product->isSaleable()) {
							$sale_status = true;
							$sale_status_msg = __('Add To Cart');
						}
					}
				}
					
                $resultItem = array(
                    'title'                 => $productName,
                    'sku'                   => $product->getSku(),
                    'status'                => $product->getStatus(),
                    'visibility'            => $product->getVisibility(),
                    'productType'           => $productType,
                    'price'                 => $regular_price,
                    'special_price'         => $special_price,
                    'has_special_price'     => $product->getSpecialPrice() > 0 ? true : false,
                    'image'                 => $image,
                    'pack'                  => $product->getPackSize(),
                    'pack_type'             => $product->getPackType(),
                    'manufacture'           => $manufacture,
                    'prescription_status'   => $prescription_status,
                    'total_products_count'  => $products->getTotalCount(),
                    'qty'                   => $stockItem->getQty(),
                    'sale_status'           => $sale_status,
                    'sale_status_msg'       => $sale_status_msg,
                    'CategoryFlag'          => false
                );
                $result[]   = $resultItem;
            }
        }
        /*$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('catalog_category_entity_varchar'); //gives table name with prefix

        //Select Data from table
        $sql = "Select * FROM " . $tableName ." where value like '%".$query."%'";
        $queryresult = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
        //print_r($queryresult);
        $categoryResult = [ ];
        if(count($queryresult))
        {
            foreach($queryresult as $row)
            {
                if($row['attribute_id']!=42)continue;
                $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $category = $_objectManager->create('Magento\Catalog\Model\Category')
                ->load($row['entity_id']);
                $resultItem = array(
                    'title'             => $category->getName(),
                    'CategoryFlag'      => true,
                    'url'               => $category->getUrl()
                );
                $categoryResult[]   = $resultItem;
            }
        }
        $productCategoryArrayResultAPI = array_merge($result,$categoryResult);*/
        return $result;
    }

    public function getOrdersList($customerId,$searchData)
    {
        $result = [ ];
        $pageSize       = $searchData['page_size'];
        $pageLimit      = $searchData['page_limit'];
        $sortLable      = $searchData['sort_lable'];
        $sortDirection  = $searchData['sort_order'];
        $attributeCode  = "customer_id";
        $filterValue    = $customerId;
        $filter_condition = "eq";
        $filter         = $this->filterBuilder->setField($attributeCode)->setValue($filterValue)->setConditionType($filter_condition)->create();
        $filterGroup[]  = $this->searchFilterGroupBuilder->addFilter($filter)->create();
        $searchCriteria = $this->searchCriteriaBuilder->setFilterGroups($filterGroup)->create();
        if($sortLable  != '' && $sortDirection != ''){
            $sortOrder = $this->sortOrderBuilder->setField($sortLable)->setDirection($sortDirection)->create();
            $searchCriteria->setSortOrders([$sortOrder]);
        }
        
        if($pageLimit != '')
            $searchCriteria->setCurrentPage($pageLimit);
        if($pageSize != '')
            $searchCriteria->setPageSize($pageSize);
        //print_r($searchCriteria);die;
        $orders       = $this->orderRepository->getList($searchCriteria);
        foreach ( $orders->getItems() as $order )
        {
            $items = [];
            foreach ( $order->getItems() as $item) {
                
                $sku = $item->getSku();
                $productName        = '';
                $product = $this->_objectManager->get('Magento\Catalog\Model\Product');
                if($product->getIdBySku($sku)){
                    $productRepository = $this->_objectManager->create('Magento\Catalog\Api\ProductRepositoryInterface')->get($sku);
                    $name           = $productRepository->getName();
                    $displayName    = $productRepository->getsku_display_name();
                    if ($displayName == '') {
                        $productName = $name;
                    } else {
                        $productName = $displayName;
                    }
               }else{
                    $productName = $item->getName();
               }
                
                $orderedQty = round($item->getqty_ordered());
                $unitPrice = number_format($item->getprice(), 2, '.', '');
                $totalPrice = $orderedQty * $unitPrice;
                $discountprice = $item->getDiscountAmount();
                $items[] = [
                    'name'          => $productName,
                    'qty_ordered'   => round($item->getqty_ordered()),
                    'price'         => number_format($item->getprice(), 2, '.', ''),
                    'total_price'   => $totalPrice,
                    'discountprice' => $discountprice,
                    'product_id'    => $item->getproduct_id(),
                    'sku'           => $item->getSku(),
                ];
            }
            $base_grand_total   = number_format($order->getbase_grand_total(), 2, '.', '');
            $sub_total        = number_format($order->getSubTotal(), 2, '.', '');
            $grand_total        = number_format($order->getgrand_total(), 2, '.', '');
            $shipping_amount    = number_format($order->getshipping_amount(), 2, '.', '');
            $discount_amount    = number_format($order->getdiscount_amount(), 2, '.', '');
            $discount_canceled  = number_format($order->getdiscount_canceled(), 2, '.', '');
            $status             = $order->getStatus();
            $status_title       = $order->getStatusLabel();
            $payment            = $order->getPayment();
            $additionalInformation = $payment->getAdditionalInformation();
            $paymentMethodTitle = ucwords($payment->getMethod());
            if(isset($additionalInformation['method_title']))
                $paymentMethodTitle = $additionalInformation['method_title'];
            $resultItem = [
                'increment_id'          => $order->getIncrementId(),
                'entity_id'             => $order->getEntityId(),
                'customer_email'        => $order->getCustomerEmail(),
                'sub_total'             => $sub_total,
                'grand_total'           => $grand_total,
                'shipping_amount'       => $shipping_amount,
                'status'                => $status,
                'status_title'          => $status_title,
                'coupon_code'           => $order->getCouponCode(),
                'discount_amount'       => $discount_amount,
                'discount_canceled'     => $discount_canceled,
                'discount_description'  => $order->getDiscountDescription(),
                'payment_method_title'  => $paymentMethodTitle,
                'created_at'            => $order->getcreated_at(),
                'updated_at'            => $order->getupdated_at(),
                'total_item_count'      => round($order->gettotal_item_count()),
                'total_qty_ordered'     => round($order->gettotal_qty_ordered()),
                'payment_link'          => $order->getPaymentLink(),
                'items'                 => $items,
                'shipping_address'      => $order->getShippingAddress()->getData()
            ];
            $result[]   = $resultItem;
        }
        return $result;
    }

    public function getProductCollection($categoryId)
    {
        $category = $this->_categoryFactory->create()->load($categoryId);
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect(array('entity_id'));
        $collection->addCategoryFilter($category);
        $collection->addAttributeToFilter('visibility', \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
        $collection->addAttributeToFilter('status',\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
        return $collection;
    }
	
	public function getAffiliateOrdersList($searchData)
    {
        $result = [ ];
        $pageSize       = $searchData['page_size'];
        $pageLimit      = $searchData['page_limit'];
        $sortLable      = $searchData['sort_lable'];
        $sortDirection  = $searchData['sort_order'];
        $order_by    	= $searchData['order_by'];
		$order_id       = $searchData['order_id'];
        $created_from   = $searchData['created_from'];
        $created_to   	= $searchData['created_to'];
        $order_status   = $searchData['order_status'];

		if($order_by != ''){
			$filter         = $this->filterBuilder->setField("order_by")->setValue($order_by)->setConditionType("eq")->create();
			$filterGroup[]  = $this->searchFilterGroupBuilder->addFilter($filter)->create();
		}
		
		if($order_id != ''){
			$filter         = $this->filterBuilder->setField("increment_id")->setValue($order_id)->setConditionType("eq")->create();
			$filterGroup[]  = $this->searchFilterGroupBuilder->addFilter($filter)->create();
		}
		
		if($order_status != ''){
			$filter         = $this->filterBuilder->setField("status")->setValue($order_status)->setConditionType("eq")->create();
			$filterGroup[]  = $this->searchFilterGroupBuilder->addFilter($filter)->create();
		}
		
		if($created_from != '' && $created_to != ''){
			$filter         = $this->filterBuilder->setField("created_at")->setValue($created_from)->setConditionType("from")->create();
			$filterGroup[]  = $this->searchFilterGroupBuilder->addFilter($filter)->create();
			
			$filter         = $this->filterBuilder->setField("created_at")->setValue($created_to)->setConditionType("to")->create();
			$filterGroup[]  = $this->searchFilterGroupBuilder->addFilter($filter)->create();
		}
		
        $searchCriteria = $this->searchCriteriaBuilder->setFilterGroups($filterGroup)->create();
        if($sortLable  != '' && $sortDirection != ''){
            $sortOrder = $this->sortOrderBuilder->setField($sortLable)->setDirection($sortDirection)->create();
            $searchCriteria->setSortOrders([$sortOrder]);
        }

        $orders       = $this->orderRepository->getList($searchCriteria);
        $allOrderCount = count($orders);
        $result['TotalOrderCount'] = $allOrderCount;
        
        if($pageLimit != '')
            $searchCriteria->setCurrentPage($pageLimit);
        if($pageSize != '')
            $searchCriteria->setPageSize($pageSize);
        //print_r($searchCriteria);die;
        $orders       = $this->orderRepository->getList($searchCriteria);
        foreach ( $orders->getItems() as $order )
        {
            $items = [];
            foreach ( $order->getItems() as $item) {
                
                $sku = $item->getSku();
                $productName        = '';
                $product = $this->_objectManager->get('Magento\Catalog\Model\Product');
                if($product->getIdBySku($sku)){
                    $productRepository = $this->_objectManager->create('Magento\Catalog\Api\ProductRepositoryInterface')->get($sku);
                    $name           = $productRepository->getName();
                    $displayName    = $productRepository->getsku_display_name();
                    if ($displayName == '') {
                        $productName = $name;
                    } else {
                        $productName = $displayName;
                    }
               }else{
                    $productName = $item->getName();
               }
                
                $orderedQty = round($item->getqty_ordered());
                $unitPrice = number_format($item->getprice(), 2, '.', '');
                $totalPrice = $orderedQty * $unitPrice;
                $discountprice = $item->getDiscountAmount();
                $items[] = [
                    'name'          => $productName,
                    'qty_ordered'   => round($item->getqty_ordered()),
                    'price'         => number_format($item->getprice(), 2, '.', ''),
                    'total_price'   => $totalPrice,
                    'discountprice' => $discountprice,
                    'product_id'    => $item->getproduct_id(),
                    'sku'           => $item->getSku(),
                ];
            }
            $base_grand_total   = number_format($order->getbase_grand_total(), 2, '.', '');
            $sub_total        = number_format($order->getSubTotal(), 2, '.', '');
            $grand_total        = number_format($order->getgrand_total(), 2, '.', '');
            $shipping_amount    = number_format($order->getshipping_amount(), 2, '.', '');
            $discount_amount    = number_format($order->getdiscount_amount(), 2, '.', '');
            $discount_canceled  = number_format($order->getdiscount_canceled(), 2, '.', '');
            $status             = $order->getStatus();
            $status_title       = $order->getStatusLabel();
            $payment            = $order->getPayment();
            $additionalInformation = $payment->getAdditionalInformation();
            $paymentMethodTitle = ucwords($payment->getMethod());
            if(isset($additionalInformation['method_title']))
                $paymentMethodTitle = $additionalInformation['method_title'];
            $resultItem = [
                'increment_id'          => $order->getIncrementId(),
                'entity_id'             => $order->getEntityId(),
                'customer_email'        => $order->getCustomerEmail(),
                'sub_total'             => $sub_total,
                'grand_total'           => $grand_total,
                'shipping_amount'       => $shipping_amount,
                'status'                => $status,
                'status_title'          => $status_title,
                'coupon_code'           => $order->getCouponCode(),
                'discount_amount'       => $discount_amount,
                'discount_canceled'     => $discount_canceled,
                'discount_description'  => $order->getDiscountDescription(),
                'payment_method_title'  => $paymentMethodTitle,
                'created_at'            => $order->getcreated_at(),
                'updated_at'            => $order->getupdated_at(),
                'total_item_count'      => round($order->gettotal_item_count()),
                'total_qty_ordered'     => round($order->gettotal_qty_ordered()),
                'payment_link'          => $order->getPaymentLink(),
                'items'                 => $items,
                'shipping_address'      => $order->getShippingAddress()->getData()
            ];
            $result['page_size'] = $pageSize;
            $result[]   = $resultItem;
        }
        return $result;
    }
}
