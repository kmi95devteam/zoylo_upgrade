<?php

namespace Zoylo\Webservices\Model\Data;

class CustomerCartId extends \Magento\Framework\Model\AbstractModel implements
    \Zoylo\Webservices\Api\Data\CustomerCartIdInterface
{
    const KEY_CODE      = 'code';
    const KEY_Success   = 'success';
    const KEY_Message   = 'message';
    const KEY_CartId    = 'cart_id';
    const KEY_ItemsCount= 'items_count';
    const KEY_ItemsQty  = 'items_qty';
    const KEY_Items     = 'items';
    const KEY_Totals    = 'totals';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Get Code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->_getData(self::KEY_CODE);
    }

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code)
    {
        return $this->setData(self::KEY_CODE, $code);
    }

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess()
    {
        return $this->_getData(self::KEY_Success);
    }

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success)
    {
        return $this->setData(self::KEY_Success, $success);
    }

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->_getData(self::KEY_Message);
    }

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        return $this->setData(self::KEY_Message, $message);
    }

    /**
     * Get Guest Cart ID
     *
     * @return string
     */
    public function getCartId()
    {
        return $this->_getData(self::KEY_CartId);
    }

    /**
     * Set Guest Cart ID
     *
     * @param string $cartId
     * @return $this
     */
    public function setCartId($cartId)
    {
        return $this->setData(self::KEY_CartId, $cartId);
    }

    /**
     * Get Total Qty Count
     *
     * @return int
     */
    public function getItemsQty()
    {
        return $this->_getData(self::KEY_ItemsQty);
    }

    /**
     * Set Total Qty Count
     *
     * @param int $totalQuantity
     * @return $this
     */
    public function setItemsQty($totalQuantity)
    {
        return $this->setData(self::KEY_ItemsQty, $totalQuantity);
    }

    /**
     * Get Total Items Count
     *
     * @return int
     */
    public function getItemsCount()
    {
        return $this->_getData(self::KEY_ItemsCount);
    }

    /**
     * Set Total Items Count
     *
     * @param int $totalItems
     * @return $this
     */
    public function setItemsCount($totalItems)
    {
        return $this->setData(self::KEY_ItemsCount, $totalItems);
    }
    
    /**
     * Get Items Data
     *
     * @return \Magento\Quote\Api\Data\CartInterface
     */
    public function getItems()
    {
        return $this->_getData(self::KEY_Items);
    }

    /**
     * Set Items Data
     *
     * @param \Magento\Quote\Api\Data\CartInterface $quote
     * @return $this
     */
    public function setItems(array $quote = null)
    {
        return $this->setData(self::KEY_Items, $quote);
    }
    
    /**
     * Get totals Data
     *
     * @return \Magento\Quote\Api\Data\TotalsInterface
     */
    public function getTotals()
    {
        return $this->_getData(self::KEY_Totals);
    }

    /**
     * Set Totals Data
     *
     * @param \Magento\Quote\Api\Data\TotalsInterface $totals
     * @return $this
     */
    public function setTotals(array $totals = null)
    {
        return $this->setData(self::KEY_Totals, $totals);
    }
}