<?php

namespace Zoylo\Webservices\Model\Data;

class AffiliateUsers extends \Magento\Framework\Model\AbstractModel implements
    \Zoylo\Webservices\Api\Data\AffiliateUsersInterFace
{
    const KEY_CODE = 'code';
    const KEY_Success = 'success';
    const KEY_Token = 'token';
    const KEY_AffiliateUserStatus = 'affiliate_user_status';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Get Code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->_getData(self::KEY_CODE);
    }

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code)
    {
        return $this->setData(self::KEY_CODE, $code);
    }

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess()
    {
        return $this->_getData(self::KEY_Success);
    }

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success)
    {
        return $this->setData(self::KEY_Success, $success);
    }

    /**
     * Get Token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->_getData(self::KEY_Token);
    }

    /**
     * Set Token
     *
     * @param string $token
     * @return $this
     */
    public function setToken($token)
    {
        return $this->setData(self::KEY_Token, $token);
    }
    
    /**
     * Get affiliate user status
     *
     * @return string
     */
    public function getAffiliateUserStatus()
    {
        return $this->_getData(self::KEY_AffiliateUserStatus);
    }

    /**
     * Set Affiliate user status
     *
     * @param string $affiliateUserStatus
     * @return $this
     */
    public function setAffiliateUserStatus($affiliateUserStatus)
    {
        return $this->setData(self::KEY_AffiliateUserStatus, $affiliateUserStatus);
    }
}