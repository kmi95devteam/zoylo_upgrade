<?php

namespace Zoylo\Webservices\Model\Data;

class HomeModel extends \Magento\Framework\Model\AbstractModel implements
    \Zoylo\Webservices\Api\Data\HomePageInterface
{
    const KEY_CODE = 'code';
    const KEY_Success = 'success';
    const KEY_Message = 'message';
    const KEY_FeaturedCategories = 'featured_categories';
    const KEY_BrandsData = 'brands_data';
    const KEY_ManufacturerData = 'manufacturer_data';
    const KEY_SymtomsData = 'symtoms_data';
    const KEY_PopularProducts = 'popular_products';
    const KEY_BannersData = 'banners_data';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Get Code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->_getData(self::KEY_CODE);
    }

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code)
    {
        return $this->setData(self::KEY_CODE, $code);
    }

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess()
    {
        return $this->_getData(self::KEY_Success);
    }

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success)
    {
        return $this->setData(self::KEY_Success, $success);
    }

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->_getData(self::KEY_Message);
    }

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        return $this->setData(self::KEY_Message, $message);
    }

    /**
     * @return \Zoylo\Webservices\Api\Data\HomePageInterface[]
     */
    public function getFeaturedCategories()
    {
        return $this->_getData(self::KEY_FeaturedCategories);
    }

    /**
     * @param \Zoylo\Webservices\Api\Data\HomePageInterface[] $categoriesList
     * @return $this
     */
    public function setFeaturedCategories(array $categoriesList = null)
    {
        return $this->setData(self::KEY_FeaturedCategories, $categoriesList);
    }

    /**
     * @return \Zoylo\Webservices\Api\Data\HomePageInterface[]
     */
    public function getBrandsData()
    {
        return $this->_getData(self::KEY_BrandsData);
    }

    /**
     * @param \Zoylo\Webservices\Api\Data\HomePageInterface[] $brandsData
     * @return $this
     */
    public function setBrandsData(array $brandsData = null)
    {
        return $this->setData(self::KEY_BrandsData, $brandsData);
    }

    /**
     * @return \Zoylo\Webservices\Api\Data\HomePageInterface[]
     */
    public function getManufacturerData()
    {
        return $this->_getData(self::KEY_ManufacturerData);
    }

    /**
     * @param \Zoylo\Webservices\Api\Data\HomePageInterface[] $manufacturerData
     * @return $this
     */
    public function setManufacturerData(array $manufacturerData = null)
    {
        return $this->setData(self::KEY_ManufacturerData, $manufacturerData);
    }

    /**
     * @return \Zoylo\Webservices\Api\Data\HomePageInterface[]
     */
    public function getSymtomsData()
    {
        return $this->_getData(self::KEY_SymtomsData);
    }

    /**
     * @param \Zoylo\Webservices\Api\Data\HomePageInterface[] $symtomsData
     * @return $this
     */
    public function setSymtomsData(array $symtomsData = null)
    {
        return $this->setData(self::KEY_SymtomsData, $symtomsData);
    }

    /**
     * @return \Zoylo\Webservices\Api\Data\HomePageInterface[]
     */
    public function getPopularProducts()
    {
        return $this->_getData(self::KEY_PopularProducts);
    }

    /**
     * @param \Zoylo\Webservices\Api\Data\HomePageInterface[] $productsList
     * @return $this
     */
    public function setPopularProducts(array $productsList = null)
    {
        return $this->setData(self::KEY_PopularProducts, $productsList);
    }

    /**
     * @return \Zoylo\Webservices\Api\Data\HomePageInterface[]
     */
    public function getBannersData()
    {
        return $this->_getData(self::KEY_BannersData);
    }

    /**
     * @param \Zoylo\Webservices\Api\Data\HomePageInterface[] $bannersList
     * @return $this
     */
    public function setBannersData(array $bannersList = null)
    {
        return $this->setData(self::KEY_BannersData, $bannersList);
    }
}