<?php

namespace Zoylo\Webservices\Model\Data;

class CountriesInfo extends \Magento\Framework\Model\AbstractModel implements
    \Zoylo\Webservices\Api\Data\CountriesInfoInterface
{
    const KEY_CODE = 'code';
    const KEY_Success = 'success';
    const KEY_Message = 'message';
    const KEY_CountriesData = 'countries_data';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Get Code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->_getData(self::KEY_CODE);
    }

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code)
    {
        return $this->setData(self::KEY_CODE, $code);
    }

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess()
    {
        return $this->_getData(self::KEY_Success);
    }

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success)
    {
        return $this->setData(self::KEY_Success, $success);
    }

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->_getData(self::KEY_Message);
    }

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        return $this->setData(self::KEY_Message, $message);
    }

    /**
     * @return \Zoylo\Webservices\Api\Data\CountriesInfoInterface[]
     */
    public function getCountriesData()
    {
        return $this->_getData(self::KEY_CountriesData);
    }

    /**
     * @param \Zoylo\Webservices\Api\Data\CountriesInfoInterface[] $countriesList
     * @return $this
     */
    public function setCountriesData(array $countriesList = null)
    {
        return $this->setData(self::KEY_CountriesData, $countriesList);
    }
}