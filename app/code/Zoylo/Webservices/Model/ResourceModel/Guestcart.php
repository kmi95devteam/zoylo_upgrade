<?php
namespace Zoylo\Webservices\Model\ResourceModel;

class Guestcart extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('guest_customer_cart_id', 'id');
    }
}
?>