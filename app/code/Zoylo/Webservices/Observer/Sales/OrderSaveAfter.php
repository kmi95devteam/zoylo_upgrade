<?php
namespace Zoylo\Webservices\Observer\Sales;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order\Address\Renderer;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Sales\Model\Order\Email\Container\OrderIdentity;

class OrderSaveAfter implements ObserverInterface
{
    
    /**
     * @var PaymentHelper
     */
    protected $paymentHelper;

    /**
     * @var Renderer
     */
    protected $addressRenderer;

    /**
     * @var objectManager
     */
    protected $objectManager;

    /**
     * @var \Magecomp\Smsfree\Helper\Data
     */
    protected $smshelperdata;

    /**
     * @var \Magecomp\Smsfree\Helper\Apicall
     */
    protected $smshelperapi;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $dataTime;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var IdentityInterface
     */
    protected $identityContainer;

    /**
     * Generate_Prescription_API_URL
     *
     * @deprecated
     */
    const Generate_Prescription_API_URL =  "https://eprescriptionservices.zoylo.com/addpatient";

    /**
     * @param IdentityInterface $identityContainer
     * @param Renderer $addressRenderer
     */
    public function __construct(
        \Magecomp\Smsfree\Helper\Data $smshelperdata,
        \Magecomp\Smsfree\Helper\Apicall $smshelperapi,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Stdlib\DateTime\DateTime $dataTime,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        OrderIdentity $identityContainer,
        PaymentHelper $paymentHelper,
        Renderer $addressRenderer
    ) {
        $this->objectManager        = \Magento\Framework\App\ObjectManager::getInstance();
        $this->paymentHelper        = $paymentHelper;
        $this->addressRenderer      = $addressRenderer;
        $this->smshelperdata        = $smshelperdata;
        $this->smshelperapi         = $smshelperapi;
        $this->productRepository    = $productRepository;
        $this->dataTime             = $dataTime;
        $this->customerFactory      = $customerFactory;
        $this->transportBuilder     = $transportBuilder;
        $this->identityContainer    = $identityContainer;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order          = $observer->getOrder();
        $status         = $order->getStatus();
        $orderId        = $order->getId();
		$customerId     = $order->getCustomerId();
		if($customerId == ''){
			$order      = $this->objectManager->create('\Magento\Sales\Model\Order')->load($orderId);
		}
        $templateId     = '';
        $smsTemplateId  = '';
        $rx             = 0;
        $qtys           = array();
        $skus           = array();
        $itemsCount     = count($order->getAllItems());
        foreach ($order->getAllItems() as $item) {
            $skus[]     = $item->getSku();
            $qtys[$item->getSku()] = intval($item->getQtyOrdered());
            if($itemsCount == 1 && $item->getSku() == "Free"){
                $rx = 1;
            }
        }
        $payment        = $order->getPayment();
        $paymentMethod  = $payment->getMethod();

        if($status == 'generate_prescription'){
            $productCollection = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
            $collection = $productCollection->addAttributeToSelect('*')
                ->addAttributeToFilter( 'sku', array( 'in' => $skus))->load();
            $productParameters = array();

            if( count($collection) > 0 ){
                foreach ($collection as $product) {
                    $productType = $product->getAttributeText('zoylo_product_type');
                    $sku        = $product->getSku();
                    $products = $this->productRepository->get($sku);
                    if($products->getPrescriptionRequired() == "required"){
                        $name = $product->getName();
                        $productParameters[] = [
                            //'sku'   => $sku,
                            'medicineName'  => $name,
                            'dosage'   => $qtys[$sku]
                        ];
                   }
                }

                //API Call
                if(count($productParameters) > 0){
                    $date           = $this->dataTime->gmtDate();
                    $customerId     = $order->getCustomerId();
                    $customer       = $this->customerFactory->create()->load($customerId);

                    $orderId        = $order->getIncrementId();
                    $email          = $customer->getEmail();
                    $age            = '';
                    $mobileNumber   = $customer->getCustomerNumber();
                    $firstname      = $customer->getFirstname();
                    $lastname       = $customer->getLastname();
                    $customerName   = $firstname.' '.$lastname;
                    $gender         = '';
                    if($customer->getGender() == 1)
                        $gender     = "Male";
                    elseif($customer->getGender() == 2)
                        $gender     = "Female";
                    elseif($customer->getGender() == 3)
                        $gender     = "Not Specified";
                    
                    $postParameters = [
                        'patientDetails' => [
                            'name'      => $customerName,
                            'email'     => $email,
                            'age'       => $age,
                            'gender'    => $gender,
                            'mobileNumber' => $mobileNumber
                        ],
                        'medicineList'  => $productParameters,
                        'order_id'      => $orderId,
                        'date'          => $date
                    ];
                    $apiUrl = self::Generate_Prescription_API_URL;
                    //print_r(json_encode($postParameters));
                    $ch = curl_init($apiUrl);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postParameters));                                                                  
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json'                                                   
                        //'Content-Length: ' . strlen($data_string)
                        )                                                                       
                    );       
                    $data = curl_exec($ch);

                    if(!$data){
                        echo ("Connection Failure");
                        return;
                    }
                    curl_close($ch);
                    //$result = json_decode($data);
                    //print_r($data);

                    $smsTemplateId = $status;
                }
                
            }
            
        }elseif($status == 'processing'){
            //$order->setState('new');
            //$order->save();
            $smsTemplateId = 'order_processing';
        }elseif($status == 'order_processing'){
            $smsTemplateId = $status;
            $templateId = 19;
        }elseif($status == 'order_rejected'){
            if($rx == 1){
                $smsTemplateId = $status.'_rx';
                $templateId = 11;
            }else{
                if($paymentMethod == "cashondelivery"){
                    $smsTemplateId = $status.'_cart_cod';
                    $templateId = 10;
                }else{
                    $smsTemplateId = $status.'_cart_prepaid';
                    $templateId = 2;
                }
            }
            
        }elseif ($status == 'order_cancelled') {
            
            if($paymentMethod == "cashondelivery"){
                $smsTemplateId = $status.'_cod';
                $templateId = 12;
            }else{
                $smsTemplateId = $status.'_prepaid';
                $templateId = 3;
            }
        }elseif ($status == 'canceled') {
            
            if($paymentMethod == "cashondelivery"){
                $smsTemplateId = 'order_cancelled_cod';
                $templateId = 12;
            }else{
                $smsTemplateId = 'order_cancelled_prepaid';
                $templateId = 3;
            }
        }elseif ($status == 'order_confirmed') {
            if($rx == 1){
                $smsTemplateId = $status.'_out_station_rx';
                $templateId = 7;
            }else{
                $smsTemplateId = $status.'_out_station_cart';
                $templateId = 4;
            }
        }elseif ($status == 'cancelled_no_stock') {
            $templateId = 5;
            $smsTemplateId = $status;
        }elseif ($status == 'out_for_delivery') {
            $smsTemplateId = $status.'_out_station';
            $templateId     = 14;
        }elseif ($status == 'order_shipped') {
            $smsTemplateId = $status.'_out_station';
            $templateId     = 13;
        }elseif ($status == 'return_initiated') {
            $smsTemplateId = $status;
            $templateId     = 16;
        }elseif ($status == 'return_picked_up') {
            $smsTemplateId = $status.'_cod';
            $templateId     = 17;
        }elseif ($status == 'refund_processed') {
            $smsTemplateId = $status.'_prepaid';
            $templateId     = 18;
        }elseif ($status == 'order_delivered') {
            $smsTemplateId = $status;
            $templateId     = 20;
        }

        // Send Email
        if($templateId != ''){
            $sendEmail = $this->sendEmail($order,$templateId);
        }

        //Send SMS
        if($smsTemplateId != ''){
            $smsResult  = $this->sendSMS($order,$smsTemplateId);
        }
    }

    /**
    * Sending SMS to customer
    *
    **/
    public function sendSMS($order,$smsTemplateId){
        if ($this->smshelperdata->isEnabled() && $this->smshelperdata->isOrderPlaceForUserEnabled()) {
            $status         = $order->getStatus();
            $customerId     = $order->getCustomerId();
            $customer       = $this->customerFactory->create()->load($customerId);
            $message        = $this->smshelperdata->getOrderStatusTemplateForUser($smsTemplateId);
            if($message != ''){
                $sms            =  sprintf($message, $order->getIncrementId());
                $mobile         = $customer->getCustomerNumber();
                $this->smshelperapi->callApiUrl($mobile, $sms);
                return true;  
            }else{
                return false;
            }
        }
    }

    /**
    * Sending Email to customer
    *
    **/
    public function sendEmail($order,$templateId){
        $sender = array('email' => 'noreply@zoylo.com','name' => 'Zoylo');
        $customerEmailId = $order->getCustomerEmail();
        $formattedShippingAddress = $this->getFormattedShippingAddress($order);
        $vars = [
            'order' => $order,
            'billing' => $order->getBillingAddress(),
            'payment_html' => $this->getPaymentHtml($order),
            'store' => $order->getStore(),
            'formattedShippingAddress' => $this->getFormattedShippingAddress($order),
            'formattedBillingAddress' => $this->getFormattedBillingAddress($order)
        ];

        $transport = $this->transportBuilder->setTemplateIdentifier($templateId)
            ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND,'store' => 1])
            ->setTemplateVars($vars)
            ->setFrom($sender)
            ->addTo(array('To' => $customerEmailId));

        $transport = $transport->getTransport();
        $transport->sendMessage();
        return true;
    }

     /**
     * @param Order $order
     * @return string|null
     */
    public function getFormattedShippingAddress($order)
    {
        return $order->getIsVirtual()
            ? null
            : $this->addressRenderer->format($order->getShippingAddress(), 'html');
    }

    /**
     * @param Order $order
     * @return string|null
     */
    public function getFormattedBillingAddress($order)
    {
        return $this->addressRenderer->format($order->getBillingAddress(), 'html');
    }

    /**
     * Get payment info block as html
     *
     * @param Order $order
     * @return string
     */
    public function getPaymentHtml($order)
    {
        return $this->paymentHelper->getInfoBlockHtml(
            $order->getPayment(),
            $this->identityContainer->getStore()->getStoreId()
        );
    }
}