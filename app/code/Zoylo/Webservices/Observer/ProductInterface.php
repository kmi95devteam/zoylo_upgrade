<?php
/**
 * Copyright © 2018-2019 Hopescode, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Zoylo\Webservices\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Api\ProductRepositoryInterfaceFactory as ProductRepository;
use Magento\Catalog\Helper\ImageFactory as ProductImageHelper;
use Magento\Store\Model\StoreManagerInterface as StoreManager;
use Magento\Store\Model\App\Emulation as AppEmulation;
use Magento\Quote\Api\Data\CartItemExtensionFactory;

class ProductInterface implements ObserverInterface
{   
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     *@var \Magento\Catalog\Helper\ImageFactory
     */
    protected $productImageHelper;

    /**
     *@var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     *@var \Magento\Store\Model\App\Emulation
     */
    protected $appEmulation;

    /**
     * @var CartItemExtensionFactory
     */
    protected $extensionFactory;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param ProductRepository $productRepository
     * @param \Magento\Catalog\Helper\ImageFactory
     * @param \Magento\Store\Model\StoreManagerInterface
     * @param \Magento\Store\Model\App\Emulation
     * @param CartItemExtensionFactory $extensionFactory
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        ProductRepository $productRepository,
        ProductImageHelper $productImageHelper,
        StoreManager $storeManager,
        AppEmulation $appEmulation,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        CartItemExtensionFactory $extensionFactory
    ) {
        $this->_objectManager = $objectManager;
        $this->productRepository = $productRepository;
        $this->productImageHelper = $productImageHelper;
        $this->storeManager = $storeManager;
        $this->appEmulation = $appEmulation;
        $this->extensionFactory = $extensionFactory;
        $this->categoryRepository = $categoryRepository;
    }

public function execute(\Magento\Framework\Event\Observer $observer, string $imageType = NULL)
    {
        $quote = $observer->getQuote();
        $mediaUrl = $this->storeManager
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        $baseUrl = $this->storeManager->getStore()->getBaseUrl();

       /**
         * Code to add the items attribute to extension_attributes
         */
        foreach ($quote->getAllItems() as $quoteItem) {
            $product = $this->productRepository->create()->getById($quoteItem->getProductId());
            $itemExtAttr = $quoteItem->getExtensionAttributes();
            if ($itemExtAttr === null) {
                $itemExtAttr = $this->extensionFactory->create();
            }

            $zoyloProductType = $product->getAttributeText('zoylo_sku_group');

            $drug_type = $product->getResource()->getAttribute('drug_type')->getFrontend()->getValue($product);
            $drugTypeUrl = '';
            if (isset($drug_type) && !is_array($drug_type) && $drug_type != '' && $drug_type != 'no_selection' ) {
                $drugTypeUrl = $mediaUrl . 'packtype/' . ucwords(strtolower($drug_type)) . '.png';
            }elseif(strtolower($zoyloProductType)  == "otc"){
                $categoryIds= $product->getCategoryIds();
                $_categoryImgUrl = '';
                foreach ($categoryIds as $catId){
                    if($catId == 1 || $catId == 2 || $catId == 3 || $catId == 6 ||$catId == 7)
                        continue;
                    
                    $category = $this->categoryRepository->get($catId);
                    if($category->getLevel() == 3) {
                        $_categoryImgUrl = $category->getImageUrl();
                    }
                }
                if($_categoryImgUrl){
                    $drugTypeUrl = $_categoryImgUrl;
                }else{
                    $drugTypeUrl = $mediaUrl . 'packtype/OTC.png';
                }
            }

            $imageurl = '';
            if($product->getImage() && $product->getImage() != "no_selection"){
                $imageurl =  $mediaUrl. 'catalog/product' . $product->getImage();
            }
            if ( $imageurl == '' && $drugTypeUrl) {
                $imageurl = $drugTypeUrl;
            }

            if($imageurl == ''){
                $imageurl = $baseUrl.'pub/static/frontend/Smartwave/porto/en_US/Magento_Catalog/images/product/placeholder/image.jpg';
            }

            $itemExtAttr->setImageUrl($imageurl);

            $sku = $product->getSku();
            $productRepository = $this->_objectManager->create('Magento\Catalog\Model\ProductRepository');
            $products = $productRepository->get($sku);
            if($products->getPrescriptionRequired() == "required"){
                $producType = 1;
            }else{
                $producType = 0;
            }

            $itemExtAttr->setProductType($producType);
            $itemExtAttr->setZoyloProductType($zoyloProductType);

            //Pack Type
            $packType = $product->getPackSize();
            if(empty($packType))
                $packType = 'null';
            $itemExtAttr->setPackType($packType);

            //Special Price
            $specialPrice = $product->getFinalPrice();
            $price = $product->getPrice();
            if(empty($specialPrice) || $price == $specialPrice)
                    $specialPrice = 'null';
            $itemExtAttr->setSpecialPrice($specialPrice);
            $itemExtAttr->setPrice($price);

            $quoteItem->setExtensionAttributes($itemExtAttr);
        }
        return;
    }

    /**
     * Helper function that provides full cache image url
     * @param \Magento\Catalog\Model\Product
     * @return string
     */
    protected function getImageUrl($product, string $imageType = NULL)
    {
        $storeId = $this->storeManager->getStore()->getId();
        $mediaUrl = $this->storeManager
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        $baseUrl = $this->storeManager->getStore()->getBaseUrl();

        $this->appEmulation->startEnvironmentEmulation($storeId, \Magento\Framework\App\Area::AREA_FRONTEND, true);
        
        //$imageurl =$this->productImageHelper->create()->init($product, 'product_thumbnail_image')->setImageFile($product->getThumbnail())->getUrl();

        $productType = $product->getAttributeText('zoylo_sku_group');

        $drug_type = $product->getResource()->getAttribute('drug_type')->getFrontend()->getValue($product);
        $productFormatUrl = '';
        if (isset($drug_type) && !is_array($drug_type) && $drug_type != '' && $drug_type != 'no_selection' ) {
            $productFormatUrl = $mediaUrl . 'packtype/' . ucwords(strtolower($drug_type)) . '.png';
        }elseif(strtolower($productType)  == "otc"){
            $categoryIds= $product->getCategoryIds();
            $_categoryImgUrl = '';
            foreach ($categoryIds as $catId){
                if($catId == 1 || $catId == 2 || $catId == 3 || $catId == 6 ||$catId == 7)
                    continue;
                
                $category = $this->categoryRepository->get($catId);
                if($category->getLevel() == 3) {
                    $_categoryImgUrl = $category->getImageUrl();
                }
            }
            if($_categoryImgUrl){
                $productFormatUrl = $_categoryImgUrl;
            }else{
                $productFormatUrl = $mediaUrl . 'packtype/OTC.png';
            }
        }

        $imageurl = '';
        if($product->getImage() && $product->getImage() != "no_selection"){
            $imageurl =  $mediaUrl. 'catalog/product' . $product->getImage();
        }
        if ( $imageurl == '' && $productFormatUrl) {
            $imageurl = $productFormatUrl;
        }

        if($imageurl == ''){
            $imageurl = $baseUrl.'pub/static/frontend/Smartwave/porto/en_US/Magento_Catalog/images/product/placeholder/image.jpg';
        }

        $this->appEmulation->stopEnvironmentEmulation();

        return $imageUrl;
    }

    /**
     * Helper function that provides Product Type
     * @param \Magento\Catalog\Model\Product
     * @return string
     */
    protected function getProductType($product)
    {
        $storeId = $this->storeManager->getStore()->getId();
        
        $this->appEmulation->startEnvironmentEmulation($storeId, \Magento\Framework\App\Area::AREA_FRONTEND, true);

        $sku = $product->getSku();
        $productRepository = $this->_objectManager->create('Magento\Catalog\Model\ProductRepository');
        $products = $productRepository->get($sku);
        if($products->getPrescriptionRequired() == "required"){
            $producType = 1;
        }else{
            $producType = 0;
        }
        $this->appEmulation->stopEnvironmentEmulation();

        return $producType;
    }

    /**
     * Helper function that provides Zoylo Product Type
     * @param \Magento\Catalog\Model\Product
     * @return string
     */
    protected function getZoyloProductType($product)
    {
        $storeId = $this->storeManager->getStore()->getId();
        
        $this->appEmulation->startEnvironmentEmulation($storeId, \Magento\Framework\App\Area::AREA_FRONTEND, true);

        $zoyloProductType = $product->getAttributeText('zoylo_sku_group');

        $this->appEmulation->stopEnvironmentEmulation();

        return $zoyloProductType;
    }

    /**
     * Helper function that provides Pack Type
     * @param \Magento\Catalog\Model\Product
     * @return string
     */
    protected function getPackType($product)
    {
        $storeId = $this->storeManager->getStore()->getId();
        
        $this->appEmulation->startEnvironmentEmulation($storeId, \Magento\Framework\App\Area::AREA_FRONTEND, true);
        $packType = $product->getPackSize();
        if(empty($packType))
                $packType = 'null';
        $this->appEmulation->stopEnvironmentEmulation();

        return $packType;
    }

    /**
     * Helper function that provides Pack Type
     * @param \Magento\Catalog\Model\Product
     * @return floatval(var)|null Product price. Otherwise, null.
     */
    protected function getSpecialPrice($product)
    {
        $storeId = $this->storeManager->getStore()->getId();
        
        $this->appEmulation->startEnvironmentEmulation($storeId, \Magento\Framework\App\Area::AREA_FRONTEND, true);
        //Special Price
        $specialPrice = $product->getFinalPrice();
        if(empty($specialPrice)  || $price == $specialPrice)
                $specialPrice = 'null';
        $this->appEmulation->stopEnvironmentEmulation();

        return $specialPrice;
    }

    /**
     * Helper function that provides Pack Type
     * @param \Magento\Catalog\Model\Product
     * @return floatval(var)|null Product price. Otherwise, null.
     */
    protected function getPrice($product)
    {
        $storeId = $this->storeManager->getStore()->getId();
        
        $this->appEmulation->startEnvironmentEmulation($storeId, \Magento\Framework\App\Area::AREA_FRONTEND, true);
        //Special Price
        $price = $product->getPrice();
        if(empty($price))
                $price = 'null';
        $this->appEmulation->stopEnvironmentEmulation();

        return $price;
    }
}