<?php
namespace Zoylo\Webservices\Api;

interface WebservicesInterface
{
	/**
     * Create access token for admin given the customer credentials.
     *
     * @param string $username
     * @return \Zoylo\Webservices\Api\Data\LoginInterFace containing Login objects
     * @throws \Magento\Framework\Exception\AuthenticationException
     */
    public function login($username);
	
	/**
     * Return home collection
     * @api
     * @return \Zoylo\Webservices\Api\Data\HomePageInterface containing homePage objects
     */
	public function homepage();

    /**
     * Return Product Information
     * @api
     * @param string $sku
     * @return \Zoylo\Webservices\Api\Data\ProductInfoInterface containing Products objects
     * @throws NoSuchEntityException
     */
    public function getProductInfo($sku);

    /**
     * Return Product Filter collection
     * @api
     * @param int $cartId
     * @param string $query
     * @return \Zoylo\Webservices\Api\Data\ProductFiltersInterface containing Filter objects
     * @throws NoSuchEntityException
     */
    public function getFilters($catId = null, $query = null);

    /**
     * Return Product Search Result
     * @api
     * @param mixed $searchData
     * @return \Zoylo\Webservices\Api\Data\ProductSearchInterface containing Filter objects
     * @throws NoSuchEntityException
     */
    public function getSearchList($searchData);

    /**
     * Save/Update customer address.
     *
     * @param \Magento\Customer\Api\Data\AddressInterface $address
     * @return \Magento\Customer\Api\Data\AddressInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function updateAddresses(\Magento\Customer\Api\Data\AddressInterface $address);

    /**
     * Return Zipcode Availability
     * @api
     * @param int $zipcode
     * @return \Zoylo\Webservices\Api\Data\ZipcodeSearchInterface containing Availability objects
     * @throws NoSuchEntityException
     */
    public function zipcodeChecker($zipcode = null);

    /**
     * Return Coupons List
     * @api
     * @return \Zoylo\Webservices\Api\Data\CouponsListInterface containing Coupons List objects
     * @throws NoSuchEntityException
     */
    public function getCouponsList();

    /**
     * Return Country collection
     * @api
     * @return \Zoylo\Webservices\Api\Data\CountriesInfoInterface containing Country objects
     */
    public function getCountriesInfo();

    /**
     * Return Regions collection
     * @api
     * @return \Zoylo\Webservices\Api\Data\CountriesInfoInterface containing Country objects
     */
    public function getRegionsInfo();

    /**
     * Return Guest Cart ID
     * @api
     * @param string $deviceId
     * @return \Zoylo\Webservices\Api\Data\GuestCartIdInterface containing Guest Cart ID objects
     */
    public function getGuestCartId($deviceId);

    /**
     * Return Login Customer Cart ID
     * @api
     * @param int $customerId
     * @return \Zoylo\Webservices\Api\Data\CustomerCartIdInterface containing Login Customer Cart ID objects
     */
    public function getCustomerCartId($customerId);

    /**
     * It will clear the cart Items
     * @api
     * @param int $customerId
     * @return \Zoylo\Webservices\Api\Data\ClearCartItemsInterface containing Cart objects
     */
    public function clearCartItems($customerId);

    /**
     * Create customer account. Perform necessary business operations like sending email.
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @param string $password
     * @param string $redirectUrl
     * @return \Zoylo\Webservices\Api\Data\CustomerCreateInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createAccount(
        \Magento\Customer\Api\Data\CustomerInterface $customer,
        $password = null,
        $redirectUrl = ''
    );
    
    /**
     * Merge guest cart to logged in customer cart
     *
     * @param string $guestQuoteId
     * @param int $customerId
     * @param int $storeId
     * @return \Zoylo\Webservices\Api\Data\MergeCartInterface containing Cart information objects
     */
    public function mergeCart($cartId, $customerId, $storeId);

    /**
     * Reorder API
     *
     * @param int $customerId
     * @param string $orderId
     * @return \Zoylo\Webservices\Api\Data\ReorderInterface containing Reorder information objects
     */
    public function reorder($customerId, $orderId);

     /**
     * Retrieve list of categories
     *
     * @param int $rootCategoryId
     * @param int $depth
     * @throws \Magento\Framework\Exception\NoSuchEntityException If ID is not found
     * @return \Zoylo\Webservices\Api\Data\CategoryTreeInterface containing Tree objects
     */
    public function getTree($rootCategoryId = null, $depth = null);

    /**
    * Admin Token function
    *
    * @api
    * @return array
    */
    public function adminToken();

    /**
     * Set payment information for a specified cart.
     *
     * @param int $prescription_comment
     * @param int $payment_method
     * @param int $cartId
     * @param string $order_comment
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return \Zoylo\Webservices\Api\Data\OrderInterface containing Order objects
     */
    public function createOrder($cartId, $prescription_comment = null, $payment_method = null ,$order_comment);

    /**
     * Update order information.
     *
     * @param mixed $orderData
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return \Zoylo\Webservices\Api\Data\UpdateOrderInterface containing Update Order objects
     */
    public function updateOrder($orderData);
	
     /**
     * Cancel order.
     *
     * @param int $customerId
     * @param mixed $orderData
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return \Zoylo\Webservices\Api\Data\CancelOrderInterface containing Update Order objects
     */
    public function cancelOrder($customerId, $orderData);
    
    /**
     * Order Status Update.
     *
     * @param int $customerId
     * @param mixed $orderData
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return \Zoylo\Webservices\Api\Data\CancelOrderInterface containing Update Order objects
     */
    public function orderStatusUpdate($customerId, $orderData);
    
    /**
     * Order Status Update.
     *
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return \Zoylo\Webservices\Api\Data\CancelOrderInterface containing Update Order objects
     */
    public function clickpostStatusUpdate();

    /**
     * Return the Customer orders collection.
     *
     * @param int $customerId
     * @param mixed $searchData
     * @return \Zoylo\Webservices\Api\Data\OrdersListInterface containing Orders objects
     */
    public function getOrdersList($customerId,$searchData);

    /**
     * Return the order information.
     *
     * @param string $incrementId
     * @throws \Magento\Framework\Exception\NoSuchEntityException If ID is not found
     * @return \Zoylo\Webservices\Api\Data\OrderInterface containing Order objects
     */
    public function getOrderInfo($incrementId);

    /**
     * Retrun the Products list as per the serach criteria.
     *
     * @param \Zoylo\Webservices\Api\ProductSearchInformationInterface[] $search
     * @return mixed[]
     */
    public function getList($search = null);
    
    /**
     * Retun the customer information.
     *
     * @param string $username
     * @return \Zoylo\Webservices\Api\Data\CustomerInfoInterFace containing CustomerInfo objects
     * @throws \Magento\Framework\Exception\AuthenticationException
     */
    public function getCustomerInfo($username);
    
    /**
     * Set payment information for a specified cart for creating affiliate create order
     *
     * @param int $prescription_comment
     * @param int $payment_method
     * @param int $cartId
     * @param string $order_comment
     * @param string $order_by
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return \Zoylo\Webservices\Api\Data\AffiliateCreateOrderInterface containing Order objects
     */
    public function affiliateCreateOrder($cartId, $prescription_comment = null, $payment_method = null ,$order_comment,$order_by);
}