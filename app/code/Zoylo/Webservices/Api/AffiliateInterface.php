<?php
namespace Zoylo\Webservices\Api;

interface AffiliateInterface
{
	/**
     * Return access token for admin given the customer credentials.
     *
     * @param string $username
     * @param string $email
     * @return \Zoylo\Webservices\Api\Data\AffiliateUsersInterFace containing Users objects
     * @throws \Magento\Framework\Exception\AuthenticationException
     */
    public function checkAffiliateUsers($username,$email);
	
	/**
     * Retun the customer information.
     *
     * @param string $username
     * @return \Zoylo\Webservices\Api\Data\AffiliateLoginInterFace containing Login objects
     * @throws \Magento\Framework\Exception\AuthenticationException
     */
    public function getCustomerInfo($username);
	
	/**
     * Return Coupons List
     * @api
     * @return \Zoylo\Webservices\Api\Data\CouponsListInterface containing Coupons List objects
     * @throws NoSuchEntityException
     */
    public function getCouponsList();
	
	 /**
     * Create order information.
     *
     * @param mixed $orderData
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return \Zoylo\Webservices\Api\Data\AffiliateOrderInterFace containing Create Order objects
     */
    public function createOrder($orderData);
	
	/**
     * Return Regions collection
     * @api
     * @return \Zoylo\Webservices\Api\Data\CountriesInfoInterface containing Country objects
     */
    public function getRegionsInfo();
	
	 /**
     * Return the Affiliate Admin orders collection.
     *
     * @param mixed $searchData
     * @return \Zoylo\Webservices\Api\Data\OrdersListInterface containing Orders objects
     */
    public function getOrdersList($searchData);
}