<?php
namespace Zoylo\Webservices\Api\Data;
/**
 * @api
 *
 */
interface MergeCartInterface
{
    
    /**
     * Get Code
     *
     * @return int
     */
    public function getCode();

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess();

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success);

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage();

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * Get Total Qty Count
     *
     * @return int
     */
    public function getItemsQty();

    /**
     * Set Total Qty Count
     *
     * @param int $totalQuantity
     * @return $this
     */
    public function setItemsQty($totalQuantity);

    /**
     * Get Total Items Count
     *
     * @return int
     */
    public function getItemsCount();

     /**
     * Set Total Items Count
     *
     * @param int $totalItems
     * @return $this
     */
    public function setItemsCount($totalItems);
}