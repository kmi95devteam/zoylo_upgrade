<?php
namespace Zoylo\Webservices\Api\Data;
/**
 * @api
 *
 */
interface CustomerInfoInterFace
{
    
    /**
     * Get Code
     *
     * @return int
     */
    public function getCode();

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess();

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success);

/**
     * Get Login Data
     *
     * @return \Zoylo\Webservices\Api\Data\CustomerInfoInterFace[]
     */
    public function getLoginData();

     /**
     * Set Login Data
     *
     * @param \Zoylo\Webservices\Api\Data\CustomerInfoInterFace[] $data
     * @return $this
     */
    public function setLoginData($data);

    /**
     * Get Token
     *
     * @return string
     */
    public function getToken();

    /**
     * Set Token
     *
     * @param string $token
     * @return $this
     */
    public function setToken($token);
}
