<?php
namespace Zoylo\Webservices\Api\Data;
/**
 * @api
 *
 */
interface CustomerCreateInterface
{
    
    /**
     * Get Code
     *
     * @return int
     */
    public function getCode();

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess();

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success);

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage();

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * Get Customer Data
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    public function getCustomerData();

     /**
     * Set Customer Data
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $data
     * @return $this
     */
    public function setCustomerData(\Magento\Customer\Api\Data\CustomerInterface $data);

    /**
     * Get Token
     *
     * @return string
     */
    public function getToken();

    /**
     * Set Token
     *
     * @param string $token
     * @return $this
     */
    public function setToken($token);
}
