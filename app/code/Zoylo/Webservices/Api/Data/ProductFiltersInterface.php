<?php
namespace Zoylo\Webservices\Api\Data;
/**
 * @api
 *
 */
interface ProductFiltersInterface
{
    
    /**
     * Get Code
     *
     * @return int
     */
    public function getCode();

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess();

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success);

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage();

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * Get Filters Data
     *
     * @return \Zoylo\Webservices\Api\Data\ProductFiltersInterface[]
     */
    public function getFiltersData();

    /**
     * Set Filters Data
     *
     * @param \Zoylo\Webservices\Api\Data\ProductFiltersInterface[] $filtersList
     * @return $this
     */
    public function setFiltersData(array $filtersList = null);
}