<?php
namespace Zoylo\Webservices\Api\Data;
/**
 * @api
 *
 */
interface AffiliateUsersInterFace
{
    
    /**
     * Get Code
     *
     * @return int
     */
    public function getCode();

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess();

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success);

    /**
     * Get Token
     *
     * @return string
     */
    public function getToken();

    /**
     * Set Token
     *
     * @param string $token
     * @return $this
     */
    public function setToken($token);
    
    /**
     * Get affiliate user status
     *
     * @return string
     */
    public function getAffiliateUserStatus();

    /**
     * Set affiliate user status
     *
     * @param string $affiliateUserStatus
     * @return $this
     */
    public function setAffiliateUserStatus($affiliateUserStatus);
}
