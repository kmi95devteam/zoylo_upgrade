<?php

namespace Zoylo\Webservices\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface {


   /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeSetFactory $attributeSetFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }


	public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
		{
			
    		$customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

            $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer_address');
            $attributeSetId = $customerEntity->getDefaultAttributeSetId();

            $attributeSet = $this->attributeSetFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
    		
            $customerSetup->addAttribute('customer_address', 'customer_address_type', [
                'type' => 'varchar',
                'label' => 'Address Type',
                'input' => 'select',
    			'source' => 'Zoylo\Webservices\Model\Config\Source\AddressType',
    			'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'required' => false,
                'visible' => true,
                'visible_on_front' => true,
                'user_defined' => false,
                'sort_order' => 43,
                'position' => 43,
                'system' => 0,
            ]);

            $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'customer_address_type')
                ->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_customer_address', 'customer_address_edit', 'customer_register_address', 'customer_address'],
                ]);
            $attribute->save();

            $customerSetup->addAttribute('customer_address', 'parent_address_id', [
                'type' => 'varchar',
                'label' => 'Parent Address Id',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'visible_on_front' => false,
                'user_defined' => false,
                'sort_order' => 43,
                'position' => 43,
                'system' => 0,
            ]);

            $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'parent_address_id')
                ->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_customer_address'],
                ]);
            $attribute->save();
		}

}
