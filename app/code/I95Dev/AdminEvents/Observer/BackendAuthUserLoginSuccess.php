<?php

namespace I95Dev\AdminEvents\Observer;

use Magento\Framework\Event\ObserverInterface;

class BackendAuthUserLoginSuccess implements ObserverInterface
{    
	public function __construct(\Magento\User\Model\UserFactory $userFactory)
	{
		$this->userFactory = $userFactory;
	}

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $userName = $observer->getUser()->getUsername();
        $userId = $observer->getUser()->getUserId();
			if(isset($userId)){
				 $user = $this->userFactory->create()->load($userId);
				 $user->setIsOnline(1);
				 $user->save();
			}
			return $this;
    }   
}