<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace I95Dev\AdminEvents\Controller\Adminhtml\Auth;

class Logout extends \Magento\Backend\Controller\Adminhtml\Auth
{
	
	protected $authSession;
	
	protected $userFactory;
	
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Backend\Model\Auth\Session $authSession,
		\Magento\User\Model\UserFactory $userFactory
	) {
		parent::__construct($context);
		$this->authSession = $authSession;
		$this->userFactory = $userFactory;
	}


    /**
     * Administrator logout action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
	
		$userId = $this->authSession->getUser()->getUserId();
			if(isset($userId)){
				 $user = $this->userFactory->create()->load($userId);
				 $user->setIsOnline(0);
				 $user->save();
			}
			
        $this->_auth->logout();
        $this->messageManager->addSuccessMessage(__('You have logged out.'));
	
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath($this->_helper->getHomePageUrl());
    }
}
