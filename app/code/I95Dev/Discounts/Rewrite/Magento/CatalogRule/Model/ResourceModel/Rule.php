<?php


namespace I95Dev\Discounts\Rewrite\Magento\CatalogRule\Model\ResourceModel;

class Rule extends \Magento\CatalogRule\Model\ResourceModel\Rule
{
/**
     * Retrieve product prices by catalog rule for specific date, website and customer group
     * Collect data with  product Id => price pairs
     *
     * @param \DateTimeInterface $date
     * @param int $websiteId
     * @param int $customerGroupId
     * @param array $productIds
     * @return array
     */
    public function getRulePrices(\DateTimeInterface $date, $websiteId, $customerGroupId, $productIds) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $products = array();
        foreach ($productIds as $productId) {
            $_product = $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
            $applyRules = $_product->getApplyCatalogRules();
            if (isset($applyRules) & $applyRules != 1) {
                $products[] = $productId;
            }
        }
        
        $connection = $this->getConnection();
        $select = $connection->select()
                ->from($this->getTable('catalogrule_product_price'), ['product_id', 'rule_price'])
                ->where('rule_date = ?', $date->format('Y-m-d'))
                ->where('website_id = ?', $websiteId)
                ->where('customer_group_id = ?', $customerGroupId)
                ->where('product_id IN(?)', $products);

        return $connection->fetchPairs($select);
    }
}
