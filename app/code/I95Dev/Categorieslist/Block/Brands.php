<?php

namespace I95Dev\Categorieslist\Block;

class Brands extends \Magento\Framework\View\Element\Template {

    protected $_categoryHelper;
    protected $_categoryFlatConfig;
    protected $_topMenu;
    protected $_categoryFactory;
    protected $_helper;
    protected $_filterProvider;
    protected $_blockFactory;
    protected $_megamenuConfig;
    protected $_storeManager;

    /**
     * Group Collection
     */
    protected $_brandCollection;

    /**
     * Symptoms Collection
     */
    protected $_symptomsCollection;
    protected $_registry;

    /**
     * @var \Magento\Catalog\Helper\Category
     */
    protected $_brandHelper;

    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, \Ves\Brand\Helper\Data $brandHelper, \Ves\Brand\Model\Brand $brandCollection, \I95Dev\Symptoms\Model\Symptoms $symptomsCollection, \Magento\Catalog\Helper\Category $categoryHelper, \Smartwave\Megamenu\Helper\Data $helper, \Magento\Catalog\Model\Indexer\Category\Flat\State $categoryFlatState, \Magento\Catalog\Model\CategoryFactory $categoryFactory, \Magento\Theme\Block\Html\Topmenu $topMenu, \Magento\Cms\Model\Template\FilterProvider $filterProvider, \Magento\Cms\Model\BlockFactory $blockFactory, \Magento\Framework\Registry $registry
    ) {

        $this->_categoryHelper = $categoryHelper;
        $this->_categoryFlatConfig = $categoryFlatState;
        $this->_categoryFactory = $categoryFactory;
        $this->_topMenu = $topMenu;
        $this->_helper = $helper;
        $this->_filterProvider = $filterProvider;
        $this->_blockFactory = $blockFactory;
        $this->_storeManager = $context->getStoreManager();
        $this->_brandCollection = $brandCollection;
        $this->_symptomsCollection = $symptomsCollection;
        $this->_brandHelper = $brandHelper;
        $this->_registry = $registry;


        parent::__construct($context);
    }

    function getBrandCollection() {
        $number_item = $this->getConfig('brand_block/number_item');
        $brandGroups = $this->getConfig('brand_block/brand_groups');
        $collection = $this->_brandCollection->getCollection()
                ->addFieldToFilter('position', 2)
                ->addFieldToFilter('status', 1);
        
        return $collection;
    }

    function getSymptomsCollection() {

        $symptomscollection = $this->_symptomsCollection->getCollection()
        ->addFieldToFilter('popular', 1);
        $symptomscollection->setOrder('name', 'ASC');
        $symptomscollection->addFieldToFilter('popular', 1);
        $symptomscollection->setPageSize(20);
        return $symptomscollection;
    }

    public function getCurrentProduct() {
        return $this->_registry->registry('current_product');
    }

    public function getRelatedproductcategories() {
        $level2 = array();
        $level3 = array();
        $currentProduct = $this->getCurrentProduct();
        $currentactids = $currentProduct->getCategoryIds();
        foreach ($currentactids as $currentactid) {
            $category = $this->_categoryFactory->create()->load($currentactid);
            $parantcat = $category->getParentCategories();

            foreach ($category->getParentCategories() as $parent) {
                if (($parent->getLevel() == 2)) {
                    $parantcategory = $this->_categoryFactory->create()->load($parent->getId());
                    $level2[] = $parantcategory->getChildrenCategories();
                }
                if (($parent->getLevel() == 3)) {
                    $parantcategory = $this->_categoryFactory->create()->load($parent->getId());
                    $level3[] = $parantcategory->getChildrenCategories();
                }
            }
        }
        $attcat = array_merge($level2, $level3);
      foreach ($attcat as $category) {}
        return $category;
    }

    public function getCategoryImageUrl($id) {
        $category = $this->_categoryFactory->create()->load($id);

        return $category->getImageUrl();
    }

}
