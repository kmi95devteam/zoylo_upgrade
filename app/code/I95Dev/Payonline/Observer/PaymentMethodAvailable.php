<?php

namespace I95Dev\Payonline\Observer;

use Magento\Framework\Event\ObserverInterface;

class PaymentMethodAvailable implements ObserverInterface {

    protected $_checkoutSession;

    public function __construct(
    \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * payment_method_is_active event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
       
        $quote = $observer->getEvent()->getQuote();
        $quoteItems = $this->_checkoutSession->getQuote()->getAllVisibleItems();
        foreach ($quoteItems as $item) {
            $items[] = $item->getSku();
            $sku = $item->getSku();
            
        }
        return false;
        $count = count($items);
        if ($count == 1 && $sku == 'Free') {
            $checkResult = $observer->getEvent()->getResult();
                    $checkResult->setData('is_available', false); 
           
        } else {
           
            $checkResult = $observer->getEvent()->getResult();
                    $checkResult->setData('is_available', true); 
        }
        
     
    }

}
