<?php
namespace I95Dev\Salts\Controller\Adminhtml\Salt;

use I95Dev\Salts\Controller\Adminhtml\Salt;

class Delete extends Salt
{
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('salt_id');
        if ($id) {
            $name = "";
            try {
                /** @var \I95Dev\Salts\Model\Salt $salt */
                $salt = $this->saltFactory->create();
                $salt->load($id);
                $name = $salt->getName();
                $salt->delete();
                $this->messageManager->addSuccess(__('The salt has been deleted.'));
                $this->_eventManager->dispatch(
                    'adminhtml_i95dev_salts_salt_on_delete',
                    ['name' => $name, 'status' => 'success']
                );
                $resultRedirect->setPath('i95dev_salts/*/');
                return $resultRedirect;
            } catch (\Exception $e) {
                $this->_eventManager->dispatch(
                    'adminhtml_i95dev_salts_salt_on_delete',
                    ['name' => $name, 'status' => 'fail']
                );
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                $resultRedirect->setPath('i95dev_salts/*/edit', ['salt_id' => $id]);
                return $resultRedirect;
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a salt to delete.'));
        // go to grid
        $resultRedirect->setPath('i95dev_salts/*/');
        return $resultRedirect;
    }
}
