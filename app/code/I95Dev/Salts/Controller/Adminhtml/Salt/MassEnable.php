<?php
namespace I95Dev\Salts\Controller\Adminhtml\Salt;

class MassEnable extends MassDisable
{
    /**
     * @var string
     */
    protected $successMessage = 'A total of %1 salts have been enabled';
    /**
     * @var string
     */
    protected $errorMessage = 'An error occurred while enabling salts.';
    /**
     * @var bool
     */
    protected $isActive = true;
}
