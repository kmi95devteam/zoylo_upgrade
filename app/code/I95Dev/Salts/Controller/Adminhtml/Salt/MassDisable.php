<?php
namespace I95Dev\Salts\Controller\Adminhtml\Salt;

use I95Dev\Salts\Model\Salt;
class MassDisable extends MassAction
{
    /**
     * @var string
     */
    protected $successMessage = 'A total of %1 salts have been disabled';
    /**
     * @var string
     */
    protected $errorMessage = 'An error occurred while disabling salts.';
    /**
     * @var bool
     */
    protected $isActive = false;

    /**
     * @param Salt $salt
     * @return $this
     */
    protected function doTheAction(Salt $salt)
    {
        $salt->setIsActive($this->isActive);
        $salt->save();
        return $this;
    }
}
