<?php

namespace I95Dev\Salts\Block\Salt;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;

class View extends Template {

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \I95Dev\Salts\Model\SaltFactory
     */
    protected $saltCollectionFactory;

    public $_storeManager;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
    Context $context, Registry $registry, \I95Dev\Salts\Model\SaltFactory $saltCollectionFactory,\Magento\Store\Model\StoreManagerInterface $storeManager, array $data = []
    ) {
        $this->saltCollectionFactory = $saltCollectionFactory;
          $this->_storeManager=$storeManager;

        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * get current salt
     *
     * @return \I95Dev\Salts\Model\Salt
     */
    public function getCurrentSalt() {
        return $this->coreRegistry->registry('current_salt');
    }

    public function getSaltUrl($salt,$drugType) {
         echo $drugType;
        $saltUrlKey = '';
        $collection = $this->saltCollectionFactory->create()->getCollection()->addFieldToFilter('name', array('eq' => $salt))
                ->addFieldToFilter('drug_type', array('like' => '%'. $drugType.'%'));
        $collection->getSelect()->limit(1);
        if (!empty($collection)) {
            foreach ($collection as $data) {
                
                $saltUrlKey = $data['url_key'];
            }

            return $saltUrlKey;
        } else {
            return $saltUrlKey;
        }
    }
    
    public function getFullSaltUrl($salt,$drugType) {
         
        $saltUrlKey = '';
        $collection = $this->saltCollectionFactory->create()->getCollection()->addFieldToFilter('name', array('eq' => $salt))
                ->addFieldToFilter('drug_type', array('like' => '%'. $drugType.'%'));
        $collection->getSelect()->limit(1);
        if (!empty($collection)) {
            foreach ($collection as $data) {
                
                $saltUrlKey = $data['url_key'];
            }

            return $saltUrlKey;
        } else {
            return $saltUrlKey;
        }
    }
    public function getAdvice($salt) {
        $saltUrlKey = '';
        $collection = $this->saltCollectionFactory->create()->getCollection()->addFieldToFilter('name', array('like' => $salt))->getData();
        if (!empty($collection)) {
            foreach ($collection as $data) {
                
                $saltUrlKey = $data['expert_advice'];
            }

            return $saltUrlKey;
        } else {
            return $saltUrlKey;
        }
    }

}
