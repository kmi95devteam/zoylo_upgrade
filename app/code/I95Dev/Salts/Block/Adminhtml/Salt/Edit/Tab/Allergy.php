<?php
namespace I95Dev\Salts\Block\Adminhtml\Salt\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic as GenericForm;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config as WysiwygConfig;

class Allergy extends GenericForm implements TabInterface
{
    
	 /**
     * @var WysiwygConfig
     */
    protected $wysiwygConfig;

    /**
     * @param WysiwygConfig $wysiwygConfig
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        WysiwygConfig $wysiwygConfig,
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
            \I95Dev\Salts\Model\Salt\Source\Options $options,
        array $data = []
            
    )
    {
         $this->options = $options;
        $this->wysiwygConfig    = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }
	
	/**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \I95Dev\Salts\Model\Salt $salt */
        $salt = $this->_coreRegistry->registry('i95dev_salts');
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('salt_');
        $form->setFieldNameSuffix('salt');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend'    =>__('Allergy Interaction '),
                'class'     => 'fieldset-wide'
            ]
        );
        $fieldset->addField(
            'antibiotics',
            'select',
            [
                'label'     => __('Antibiotics (Eg:Penicilin)ng'),
                'title'     => __('Antibiotics (Eg:Penicilin)ng'),
                'name'      => 'antibiotics',
                
                'options'   => $this->options->getOptions(),
            ]
        );
        $fieldset->addField(
            'pain_killers',
            'select',
            [
                'label'     => __('Pain Killers'),
                'title'     => __('Pain Killers'),
                'name'      => 'pain_killers',
                
               'options'   => $this->options->getOptions(),
            ]
        );
        $fieldset->addField(
            'chemotherapy_drugs',
            'select',
            [
                'label'     => __('Chemotherapy drugs'),
                'title'     => __('Chemotherapy drugs'),
                'name'      => 'chemotherapy_drugs',
                
                'options'   => $this->options->getOptions(),
            ]
        );
        $fieldset->addField(
            'anticoagulants',
            'select',
            [
                'label'     => __('Anticoagulants'),
                'title'     => __('Anticoagulants'),
                'name'      => 'anticoagulants',
                
                'options'   => $this->options->getOptions(),
            ]
        );
        $fieldset->addField(
            'anticonvulsants',
            'select',
            [
                'label'     => __('Anticonvulsants'),
                'title'     => __('Anticonvulsants'),
                'name'      => 'anticonvulsants',
                
                'options'   => $this->options->getOptions(),
            ]
        );
        $fieldset->addField(
            'sulfa_drugs',
            'select',
            [
                'label'     => __('Sulfa Drugs'),
                'title'     => __('Sulfa Drugs'),
                'name'      => 'sulfa_drugs',
                
                'options'   => $this->options->getOptions(),
            ]
        );
       
       
        $form->setValues($salt->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Allergy Interaction');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
}
