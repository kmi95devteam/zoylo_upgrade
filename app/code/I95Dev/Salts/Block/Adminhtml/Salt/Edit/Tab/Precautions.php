<?php
namespace I95Dev\Salts\Block\Adminhtml\Salt\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic as GenericForm;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config as WysiwygConfig;

class Precautions extends GenericForm implements TabInterface
{
    
	 /**
     * @var WysiwygConfig
     */
    protected $wysiwygConfig;

    /**
     * @param WysiwygConfig $wysiwygConfig
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        WysiwygConfig $wysiwygConfig,
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
            \I95Dev\Salts\Model\Salt\Source\Options $options,
        array $data = []
            
    )
    {
         $this->options = $options;
        $this->wysiwygConfig    = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }
	
	/**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \I95Dev\Salts\Model\Salt $salt */
        $salt = $this->_coreRegistry->registry('i95dev_salts');
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('salt_');
        $form->setFieldNameSuffix('salt');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend'    =>__('Precautions'),
                'class'     => 'fieldset-wide'
            ]
        );
        $fieldset->addField(
            'smoking',
            'select',
            [
                'label'     => __('Smoking'),
                'title'     => __('Smoking'),
                'name'      => 'smoking',
                
                'options'   => $this->options->getOptions(),
            ]
        );
        $fieldset->addField(
            'alcohol',
            'select',
            [
                'label'     => __('Alcohol'),
                'title'     => __('Alcohol'),
                'name'      => 'alcohol',
                
                'options'   => $this->options->getOptions(),
            ]
        );
        $fieldset->addField(
            'caffeine',
            'select',
            [
                'label'     => __('Caffeine'),
                'title'     => __('Caffeine'),
                'name'      => 'caffeine',
                
                'options'   => $this->options->getOptions(),
            ]
        );
        $fieldset->addField(
            'vision',
            'select',
            [
                'label'     => __('Vision'),
                'title'     => __('Vision'),
                'name'      => 'vision',
                
                'options'   => $this->options->getOptions(),
            ]
        );
        $fieldset->addField(
            'overdose',
            'select',
            [
                'label'     => __('Overdose'),
                'title'     => __('Overdose'),
                'name'      => 'overdose',
                
                'options'   => $this->options->getOptions(),
            ]
        );
        $fieldset->addField(
            'driving',
            'select',
            [
                'label'     => __('Driving'),
                'title'     => __('Driving'),
                'name'      => 'driving',
                
                'options'   => $this->options->getOptions(),
            ]
        );
        $fieldset->addField(
            'exercise',
            'select',
            [
                'label'     => __('Exercise'),
                'title'     => __('Exercise'),
                'name'      => 'exercise',
                
                'options'   => $this->options->getOptions(),
            ]
        );
 
        $fieldset->addField(
            'operating_heavy_achinery',
            'select',
            [
                'label'     => __('Operating Heavy Machinery'),
                'title'     => __('Operating Heavy Machinery'),
                'name'      => 'operating_heavy_achinery',
                
                'options'   => $this->options->getOptions(),
            ]
        );
       
        $form->setValues($salt->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Precautions');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
}
