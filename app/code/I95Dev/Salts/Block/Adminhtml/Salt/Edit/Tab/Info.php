<?php
namespace I95Dev\Salts\Block\Adminhtml\Salt\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic as GenericForm;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config as WysiwygConfig;

class Info extends GenericForm implements TabInterface
{
    /**
     * @var WysiwygConfig
     */
    protected $wysiwygConfig;
    
    public $sideeffects;
    
    public $types;

    /**
     * @param WysiwygConfig $wysiwygConfig
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        WysiwygConfig $wysiwygConfig,
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        \I95Dev\Symptoms\Model\Config\Source\Options $options,
        \I95Dev\Salts\Model\Salt\Source\Options $types,
        \I95Dev\SideEffects\Model\Config\Source\Options $sideeffects,
        array $data = []
    )
    {
        $this->wysiwygConfig    = $wysiwygConfig;
        $this->options = $options;
        $this->types = $types;
        $this->sideeffects = $sideeffects;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \I95Dev\Salts\Model\Salt $salt */
        $salt = $this->_coreRegistry->registry('i95dev_salts');

        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('salt_');
        $form->setFieldNameSuffix('salt');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend' => __('Salts Information'),
                'class'  => 'fieldset-wide'
            ]
        );


        if ($salt->getId()) {
            $fieldset->addField(
                'salt_id',
                'hidden',
                ['name' => 'salt_id']
            );
        }
        $fieldset->addField(
            'name',
            'text',
            [
                'name'      => 'name',
                'label'     => __('Name'),
                'title'     => __('Name'),
                'required'  => true,
            ]
        );
		$fieldset->addField(
            'short_name',
            'text',
            [
                'name'      => 'short_name',
                'label'     => __('Short Name'),
                'title'     => __('Short Name'),
                'required'  => true,
            ]
        );
        $fieldset->addField(
            'url_key',
            'text',
            [
                'name'      => 'url_key',
                'label'     => __('URL Key'),
                'title'     => __('URL Key'),
            ]
        );
	
        	$fieldset->addField(
            'side_effects',
           'editor',
            [
                'name'      => 'side_effects',
                'label'     => __('Side Effects'),
                'title'     => __('Side Effects'),
                'style'     => 'height:5em',
                'required'  => true,
                'config'    => $this->wysiwygConfig->getConfig()
            ]
        );
                
//         $fieldset->addField(
//           'side_effects',
//           'multiselect',
//           [
//               'name'      => 'side_effects',
//               'label'     => __('Side Effects'),
//               'title'     => __('Side Effects'),
//              'values'    => $this->sideeffects->getAllOptions()
//              
//           ]
//       );       
    
     $fieldset->addField(
           'drug_type',
           'select',
           [
               'name'      => 'drug_type',
               'label'     => __('Drug Type'),
               'title'     => __('Drug Type'),
              'values'    => $this->types->getAllOptions()
              
           ]
       );    
     
         
                
		$fieldset->addField(
            'uses',
           'editor',
            [
                'name'      => 'uses',
                'label'     => __('Uses'),
                'title'     => __('Uses'),
                'style'     => 'height:15em',
                'required'  => true,
                'config'    => $this->wysiwygConfig->getConfig()
            ]
        );
        if ($this->_storeManager->isSingleStoreMode()) {
            $fieldset->addField(
                'store_id',
                'hidden',
                [
                    'name'      => 'stores[]',
                    'value'     => $this->_storeManager->getStore(true)->getId()
                ]
            );
            $salt->setStoreId($this->_storeManager->getStore(true)->getId());
        }
        $fieldset->addField(
            'is_active',
            'select',
            [
                'label'     => __('Is Active'),
                'title'     => __('Is Active'),
                'name'      => 'is_active',
                'required'  => true,
                'options'   => $salt->getAvailableStatuses(),
            ]
        );
        $fieldset->addField(
            'expert_advice',
            'editor',
            [
                'name'      => 'expert_advice',
                'label'     => __('Expert Advice'),
                'title'     => __('Expert Advice'),
                'style'     => 'height:15em',
                'required'  => true,
                'config'    => $this->wysiwygConfig->getConfig()
            ]
        );
          $fieldset->addField(
            'rx',
            'select',
            [
                'label'     => __('Rx'),
                'title'     => __('Rx'),
                'name'      => 'rx',
                'required'  => true,
                'options'   => $salt->getAvailableStatuses(),
            ]
        );
              $fieldset->addField(
            'not_for_sale',
            'select',
            [
                'label'     => __('Not for Sale'),
                'title'     => __('Not for Sale'),
                'name'      => 'not_for_sale',
                'required'  => true,
                'options'   => $salt->getAvailableStatuses(),
            ]
        );
//         $fieldset->addField(
//           'ailments',
//           'multiselect',
//           [
//               'name'      => 'ailments',
//               'label'     => __('Symptoms'),
//               'title'     => __('Ailments'),
//              'values'    => $this->options->getAllOptions()
//              
//           ]
//       );
        
        

        $saltData = $this->_session->getData('i95dev_salts', true);
        if ($saltData) {
            $salt->addData($saltData);
        } else {
            if (!$salt->getId()) {
                $salt->addData($salt->getDefaultValues());
            }
        }
        $form->addValues($salt->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
}
