<?php
namespace I95Dev\Salts\Block\Adminhtml\Salt\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic as GenericForm;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config as WysiwygConfig;

class Symptoms extends GenericForm implements TabInterface
{
	/**
     * @var WysiwygConfig
     */
//    protected $wysiwygConfig;
    
      const SALT_TEMPLATE = 'salt/symptoms.phtml';
      protected $_template = 'I95Dev_Salts::salt/symptoms.phtml';
	
    /**
     * @var WysiwygConfig
     */
    protected $wysiwygConfig;
    
    public $sideeffects;
    
    public $types;

    /**
     * @param WysiwygConfig $wysiwygConfig
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        WysiwygConfig $wysiwygConfig,
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        \I95Dev\Symptoms\Model\Config\Source\Options $options,
        \I95Dev\Salts\Model\Salt\Source\Options $types,
        \I95Dev\SideEffects\Model\Config\Source\Options $sideeffects,
        array $data = []
    )
    {
        $this->wysiwygConfig    = $wysiwygConfig;
        $this->options = $options;
        $this->types = $types;
        $this->sideeffects = $sideeffects;
        parent::__construct($context, $registry, $formFactory, $data);
    }

  

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Symptoms');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return !$this->_storeManager->isSingleStoreMode();
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
    
  

    /**
     * Set JS template to itself.
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate(static::SALT_TEMPLATE);
        }

        return $this;
    }
   
}
