<?php

namespace I95Dev\Customermobilelogin\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;

class SaveMobileNumber implements ObserverInterface {

    protected $request;

    /** @var CustomerRepositoryInterface */
    protected $customerRepository;

    /**
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
    \Magento\Framework\App\Request\Http $request, CustomerRepositoryInterface $customerRepository
    ) {
        $this->request = $request;
        $this->customerRepository = $customerRepository;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $post = $this->request->getPost();
        $customer = $observer->getCustomer();
        $mobile = $post->mobile_number;
        if ($mobile != '') {
            $customer->setCustomAttribute('mobile_number', $mobile);
            $this->customerRepository->save($customer);
        }
    }

}
