<?php
namespace I95Dev\Customermobilelogin\Setup;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
class InstallData implements InstallDataInterface
{ 

    private $customerSetupFactory;      

    public function __construct(\Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory)
    {
      $this->customerSetupFactory = $customerSetupFactory;
    }

    public function install(ModuleDataSetupInterface
    $setup, ModuleContextInterface $context)
    {
        /** @var CustomerSetup $customerSetup */

        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        $setup->startSetup();

        $attributeCode = "mobile_number";

        $customerSetup->removeAttribute(\Magento\Customer\Model\Customer::ENTITY, $attributeCode);          

        $customerSetup->addAttribute('customer',
        'mobile_number', [
        'label' => 'Mobile Number',
        'type' => 'text',
        'frontend_input' => 'text',
        'required' => true,
        'visible' => true,
         'system'=> 0,
        'position' => 105,
        ]);

        $loyaltyAttribute = $customerSetup->getEavConfig()->getAttribute('customer', 'mobile_number');
        $loyaltyAttribute->setData('used_in_forms',['adminhtml_checkout','adminhtml_customer','adminhtml_customer_address','customer_account_edit','customer_address_edit','customer_register_address']);
        $loyaltyAttribute->save();

        $setup->endSetup();
    }
}