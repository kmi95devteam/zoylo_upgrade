<?php
 
namespace I95Dev\QuickOrder\Setup;
 
use Magento\Eav\Setup\EavSetup; 
use Magento\Eav\Setup\EavSetupFactory /* For Attribute create  */;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
 
/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;
    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory; 
        /* assign object to class global variable for use in other class methods */
    }
 
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
         
        /**
         * Add attributes to the eav/attribute
         */		
		
		
		 /**
         * Add attributes to the eav/attribute
         */		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'salt_name');
		    
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'salt_name', 
					[					 
					'group' => 'Zoylo Custom Attributes',					 
					'type' => 'varchar',					 
					'backend' => '',					 
					'frontend' => '',					 
					'label' => 'Salt Name',					 
					'input' => 'select',					 
					'class' => '',	
					'source' => 'I95Dev\Customproductattributes\Model\Config\Source\Options',
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable', 
			]	
		);
		
    }
}