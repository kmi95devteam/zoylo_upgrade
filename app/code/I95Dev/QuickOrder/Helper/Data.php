<?php
namespace I95Dev\QuickOrder\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	
	protected $directoryBlock;
    protected $_isScopePrivate;
    
    public function __construct(
		\Magento\Framework\App\Helper\Context $context,
		\Magento\Directory\Block\Data $directoryBlock
		)
		{
		parent::__construct($context);
			$this->_isScopePrivate = true;
			$this->directoryBlock = $directoryBlock;
		}
 
		public function getCountries()
			{
				$country = $this->directoryBlock->getCountryHtmlSelect();
				return $country;
			}
		public function getRegion()
			{
				$region = $this->directoryBlock->getRegionHtmlSelect();
				return $region;
			}
		public function getCountryAction()
			{
			return $this->getUrl('extension/extension/country', ['_secure' => true]);
			}
 
	
}