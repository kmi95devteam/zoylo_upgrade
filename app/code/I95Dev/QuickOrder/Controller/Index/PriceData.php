<?php
namespace I95Dev\QuickOrder\Controller\Index;

use Magento\Framework\App\Action\Action;

class PriceData extends Action {
	 /**
	 * @var \Magento\Framework\Controller\Result\JsonFactory
	 */
	 protected $resultJsonFactory;
	 /**
	 * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
	 */
	 protected $_productCollectionFactory;
         
         protected $resourceConnection;
         public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,       
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
                \Magento\Framework\App\ResourceConnection $resourceConnection
	   
	)
	{    
		$this->resultJsonFactory = $resultJsonFactory;    
		$this->_productCollectionFactory = $productCollectionFactory;    
                $this->resourceConnection = $resourceConnection;
		parent::__construct($context);
	}
	public function execute()
	{
		$price = '';
		$id = '';
		$rx = '';
		$result = $this->resultJsonFactory->create();
		$skuname = $this->getRequest()->getParam('skuname'); 
                if (isset(explode('--',$skuname)[0]))
                {
                    $skuname = explode('--',$skuname)[0];
                }
		$product = $this->_productCollectionFactory->create()->addAttributeToSelect('*');
		$productPriceBySku = $product->addFieldToFilter('name', $skuname);
		$priceData = $productPriceBySku->getData();
		if(!empty($priceData)){
			foreach($productPriceBySku as $data){
				$price = $data->getPrice();
				$id = $data->getId();
				$required = $data->getPrescriptionRequired();
				if($required == 'required'){
					$rx = 'Yes';
				} else {
					$rx = 'No';
				}
			}
			
		}
                
                if (isset($product->getData()[0]['entity_id'])) // code written to avoid the data to be picked up from the flat tables if the flat tables are enabled.
                {
                    $storeId = 0;
                    $productPriceQry = "SELECT value as price FROM ".$this->resourceConnection->getTableName('catalog_product_entity_decimal')."  WHERE attribute_id = (SELECT attribute_id FROM ".$this->resourceConnection->getTableName('eav_attribute')." WHERE attribute_code = 'price' AND entity_type_id = '4' ) AND entity_id = ".$product->getData()[0]['entity_id']."  AND store_id = '".$storeId."' ";
                    $productPriceRes = $this->resourceConnection->getConnection()->fetchAll($productPriceQry);    
                    if (isset($productPriceRes[0]['price']))
                    {
                        $price = $productPriceRes[0]['price'];
                    }
                }
                $rx = 'No';
                if (isset($product->getData()[0]['entity_id'])) // code written to avoid the data to be picked up from the flat tables if the flat tables are enabled.
                {
                    $storeId = 0;
                    $productRXQry = "SELECT value as rx FROM ".$this->resourceConnection->getTableName('catalog_product_entity_varchar')."  WHERE attribute_id = (SELECT attribute_id FROM ".$this->resourceConnection->getTableName('eav_attribute')." WHERE attribute_code = 'prescription_required' AND entity_type_id = '4' ) AND entity_id = ".$product->getData()[0]['entity_id']."  AND store_id = '".$storeId."' ";
                    $productRXRes = $this->resourceConnection->getConnection()->fetchAll($productRXQry);    
                    if (isset($productRXRes[0]['rx']) && $productRXRes[0]['rx'] == 'required')
                    {
                        $rx = 'Yes';
                    }
                }
		return $result->setData(['price' => $price,'id' => $id,'rx'=>$rx]);
	}
}