<?php
namespace I95Dev\Quickorder\Controller\Index;

class LoadCustomerAddress extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;

        protected $resourceConnection;
        public function __construct(
            \Magento\Framework\App\Action\Context $context,
            \Magento\Framework\App\ResourceConnection $resourceConnection
        )
	{
                $this->resourceConnection = $resourceConnection;
		return parent::__construct($context);
	}

	public function execute()
	{
            $customerId = 0; // default value
            if (isset($_POST['customer_id']) && $_POST['customer_id'] != '')
            {
                $customerId = $_POST['customer_id'];
            }
            $connection = $this->resourceConnection->getConnection();
            $query = "SELECT * FROM ".$this->resourceConnection->getTableName('customer_address_entity')." WHERE parent_id = ".$customerId."";
            $result = $connection->fetchAll($query);
            echo json_encode($result);
	}
}