<?php
namespace I95Dev\QuickOrder\Controller\Adminhtml\Index;

class LoadCustomerAddress extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;

        protected $resourceConnection;
        public function __construct(
            \Magento\Framework\App\Action\Context $context,
            \Magento\Framework\App\ResourceConnection $resourceConnection
        )
	{
                $this->resourceConnection = $resourceConnection;
		return parent::__construct($context);
	}

	public function execute()
	{
            $connection = $this->resourceConnection->getConnection();
            $customerId = 0; // default value
            $result = array();
            if ($this->getRequest()->getParam('mobile') && $this->getRequest()->getParam('mobile') != '')
            {
                $customerQuery = "SELECT entity_id FROM ".$this->resourceConnection->getTableName('customer_entity_varchar')." WHERE attribute_id = (SELECT attribute_id FROM ".$this->resourceConnection->getTableName('eav_attribute')." WHERE attribute_code = 'customer_number' AND entity_type_id='1'  ) AND value LIKE '%".$this->getRequest()->getParam('mobile')."%'";
                $customerQueryResult = $connection->fetchAll($customerQuery);
                if (isset($customerQueryResult[0]['entity_id']))
                {
                    $customerId = $customerQueryResult[0]['entity_id'];    
                }
            }
            
            $query = "SELECT entity_id,firstname,middlename,lastname,region_id,postcode,street,city,region,country_id,telephone FROM ".$this->resourceConnection->getTableName('customer_address_entity')." WHERE parent_id = ".$customerId."";
            $result = $connection->fetchAll($query);
            echo json_encode($result);
	}
}