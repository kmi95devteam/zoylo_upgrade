<?php
namespace I95Dev\QuickOrder\Controller\Adminhtml\Index;

class ValidatePincode extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;

        protected $resourceConnection;
        
        protected $pincode;
        
        public function __construct(
            \Magento\Framework\App\Action\Context $context,
            \Magento\Framework\App\ResourceConnection $resourceConnection,
            \Ecom\Ecomexpress\Model\Pincode $pincode
        )
	{
                $this->resourceConnection = $resourceConnection;
                $this->pincode = $pincode;
		return parent::__construct($context);
	}

	public function execute()
	{
            $pincodeNumber = $this->_request->getParam('pincode');
            if ($pincodeNumber != '')
            {
                $pinCodeCollection = $this->pincode->getCollection()->addFieldToFilter('pincode', array('eq' => $pincodeNumber));
                if (empty($pinCodeCollection->getData()))
                {
                    echo "Pincode: $pincodeNumber is not serviceable";
                }
                else
                {
                    echo "Pincode: $pincodeNumber is serviceable";
                }
            }
            else
            {
                echo "Please enter a pincode value";
            }

	}
}