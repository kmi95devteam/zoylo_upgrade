<?php
/**
 * Copyright © 2015 I95Dev. All rights reserved.
 */

namespace I95Dev\BranchInformation\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
	
        $installer = $setup;

        $installer->startSetup();

		/**
         * Create table 'branchinformation_branch'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('branchinformation_branch')
        )
		->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'branchinformation_branch'
        )
		->addColumn(
            'zip_code',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'zip_code'
        )
		->addColumn(
            'branch_code',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => false],
            'branch_code'
        )
		->addColumn(
            'allow_types',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'allow_types'
        )
		/*{{CedAddTableColumn}}}*/
		
		
        ->setComment(
            'I95Dev BranchInformation branchinformation_branch'
        );
		
		$installer->getConnection()->createTable($table);
		/*{{CedAddTable}}*/

        $installer->endSetup();

    }
}
