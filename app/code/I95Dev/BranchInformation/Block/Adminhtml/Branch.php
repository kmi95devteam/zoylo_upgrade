<?php
namespace I95Dev\BranchInformation\Block\Adminhtml;
class Branch extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
		
        $this->_controller = 'adminhtml_branch';/*block grid.php directory*/
        $this->_blockGroup = 'I95Dev_BranchInformation';
        $this->_headerText = __('Branch');
        $this->_addButtonLabel = __('Add New'); 
        parent::_construct();
		
    }
}
