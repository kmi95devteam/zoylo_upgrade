<?php
namespace I95Dev\BranchInformation\Block\Adminhtml\Branch\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected function _construct()
    {
		
        parent::_construct();
        $this->setId('checkmodule_branch_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Branch Information'));
    }
}