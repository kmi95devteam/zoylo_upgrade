<?php
namespace I95Dev\BranchInformation\Block\Adminhtml\Branch\Edit\Tab;
class Information extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = array()
    ) {
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
		/* @var $model \Magento\Cms\Model\Page */
        $model = $this->_coreRegistry->registry('branchinformation_branch');
		$isElementDisabled = false;
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', array('legend' => __('Information')));

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array('name' => 'id'));
        }

//		$fieldset->addField(
//            'id',
//            'text',
//            array(
//                'name' => 'id',
//                'label' => __('Information'),
//                'title' => __('Information'),
//                /*'required' => true,*/
//            )
//        );
        
                	$fieldset->addField(
            'zip_code',
            'text',
            array(
                'name' => 'zip_code',
                'label' => __('Pin Code'),
                'title' => __('Pin Code'),
                /*'required' => true,*/
            )
        );
                        
                        $fieldset->addField(
            'branch_code',
            'text',
            array(
                'name' => 'branch_code',
                'label' => __('Branch Code'),
                'title' => __('Branch Code'),
                /*'required' => true,*/
            )
        );
                        
                                         $fieldset->addField(
            'allow_types',
            'editor',
            array(
                'name' => 'allow_types',
                'label' => __('Allowed Types'),
                'title' => __('Allowed Types'),
                /*'required' => true,*/
            )
        );
                
                
		/*{{CedAddFormField}}*/
        
        if (!$model->getId()) {
            $model->setData('status', $isElementDisabled ? '2' : '1');
        }

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();   
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
