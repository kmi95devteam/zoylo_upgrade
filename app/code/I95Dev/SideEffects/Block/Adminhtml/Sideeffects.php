<?php
namespace I95Dev\SideEffects\Block\Adminhtml;
class Sideeffects extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
		
        $this->_controller = 'adminhtml_sideeffects';/*block grid.php directory*/
        $this->_blockGroup = 'I95Dev_SideEffects';
        $this->_headerText = __('Sideeffects');
        $this->_addButtonLabel = __('Add New '); 
        parent::_construct();
		
    }
}
