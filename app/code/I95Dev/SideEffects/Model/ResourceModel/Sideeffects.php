<?php
/**
 * Copyright © 2015 I95Dev. All rights reserved.
 */
namespace I95Dev\SideEffects\Model\ResourceModel;

/**
 * Sideeffects resource
 */
class Sideeffects extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('i95dev_sideeffects', 'id');
    }

  
}
