<?php
/**
 * Copyright © 2015 I95Dev. All rights reserved.
 */

namespace I95Dev\SideEffects\Model;

use Magento\Framework\Exception\SideeffectsException;

/**
 * Sideeffectstab sideeffects model
 */
class Sideeffects extends \Magento\Framework\Model\AbstractModel
{

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\Db $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init('I95Dev\SideEffects\Model\ResourceModel\Sideeffects');
    }

   
}