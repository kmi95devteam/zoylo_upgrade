<?php
namespace I95Dev\UploadPrescription\Block\Adminhtml\Index;

use Magento\Backend\Block\Template;
class Index extends Template
{

	/**
     * @var \Magento\Sales\Model\Order
     */
    protected $salesOrderModel;
	
	protected $_prescription;
 
    public function __construct(\I95Dev\UploadPrescription\Model\PrescriptionFactory  $prescription,
	\Magento\Sales\Model\Order $salesOrderModel,
	\Magento\Backend\Block\Template\Context $context, array $data = []) {

        parent::__construct($context, $data);
		$this->_prescription = $prescription;
		$this->salesOrderModel = $salesOrderModel;
	 }
	 
    public function getOrderPrescriptions($prescription_id)
    {
        $collection = $this->_prescription->create()
									->getCollection()
									->addFieldToFilter('prescription_id',array('eq' => $prescription_id));
		return $collection;
    }
	
	public function getOrderData($orderId){
        $orderData=$this->salesOrderModel->load($orderId);
		return $orderData;
    }
	

}