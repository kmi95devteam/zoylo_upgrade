<?php
namespace I95Dev\UploadPrescription\Block\Adminhtml\Order\Create;

class MyPrescription extends \Magento\Sales\Block\Adminhtml\Order\Create\AbstractCreate
{
    /**
     * Data Form object
     *
     * @var \Magento\Framework\Data\Form
     */
    protected $_form;

    /**
     * Get header css class
     *
     * @return string
     */
    public function getHeaderCssClass()
    {
        return 'head-comment';
    }

    /**
     * Get header text
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        return __('Ramesh Kumar');
    }

    
	
}
