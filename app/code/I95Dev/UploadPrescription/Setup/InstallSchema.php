<?php 
namespace I95Dev\UploadPrescription\Setup;
 
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
 
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
 
        $installer->startSetup();
		
		if (!$installer->tableExists('i95dev_prescription_details')) {
        $table = $installer->getConnection()
            ->newTable($installer->getTable('i95dev_prescription_details'))
            ->addColumn(
                'prescription_id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true,'comment' => 'Prescription Id'],
                'ID'
            )
			->addColumn(
                'customer_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Customer ID'
            )
              ->addColumn(
                'prescription_name',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false, 'default' => '',],
                'Prescription Name'
            )
              ->addColumn(
                'label',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false, 'default' => ''],
                'Label'
            )
            ->addColumn(
                'value',
                Table::TYPE_TEXT,
                null,
                [],
                'Value'
            )->addColumn(
				'created_date',
				\Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
				null,
				['nullable' => true],
				'Created At'
			)->addColumn(
				'updated_at',
				\Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
				null,
				['nullable' => true],
				'Updated At'
			)
            ->setComment('Prescriptions Details Table');
        $installer->getConnection()->createTable($table);
         
        $installer->endSetup();
		}
	}
}