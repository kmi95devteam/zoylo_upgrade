<?php

namespace I95Dev\UploadPrescription\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $quote = 'quote';
        $orderTable = 'sales_order';
	
	if (version_compare($context->getVersion(), '1.0.8', '=')) {
	
        $setup->getConnection()
            ->addColumn(
                $setup->getTable($quote),
                'prescription_values',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Prescription Value'
                ]
            );
        //Order table
        $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderTable),
                'prescription_values',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Prescription Value'
                ]
            );

		$setup->getConnection()
		->addColumn(
			$setup->getTable($quote),
			'order_comments',
			[
				'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
				'length' => 255,
				'comment' =>'Order Comments'
			]
		);
	}
		// version compare 
		if (version_compare($context->getVersion(), '1.0.10', '<')) {
	
		$setup->getConnection()
		->addColumn(
			$setup->getTable('i95dev_prescription_details'),
			'status',
			[
				'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
				'length' => 1,
				'comment' =>'Status'
			]
		);
		
		}
			
        $setup->endSetup();
    }
}