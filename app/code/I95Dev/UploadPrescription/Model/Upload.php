<?php 
namespace I95Dev\UploadPrescription\Model;

use I95Dev\UploadPrescription\Api\Data\PrescriptionsInfoInterfaceFactory;
use I95Dev\UploadPrescription\Api\Data\PrescriptionsOrderInterfaceFactory;
use I95Dev\UploadPrescription\Api\Data\PartnerPrescriptionsOrderInterfaceFactory;
use I95Dev\UploadPrescription\Api\Data\UpdateOrderInterfaceFactory;
use I95Dev\UploadPrescription\Api\UploadInterface;
use Magento\Framework\Exception\CouldNotSaveException;

class Upload implements UploadInterface {

	protected $_prescription;
	protected $_quoteRepo;
    protected $_checkoutSession; 
    protected $productRepository; 
	private $storeManager;
	private $item;
	private $customer;
	private $address;
    protected $_messageManager;
    protected $prescriptionsOrderInterfaceFactory;

    /**
    * @param \Magento\Framework\App\ObjectManager
    */
    public $objectManager;

    /**
    * @param \Zoylo\Webservices\Logger\Logger $logger
    */
    protected $logger;

    /**
     * @var \Zoylo\Webservices\Api\Data\UpdateOrderInterfaceFactory
     *
     */
    protected $updateOrderInterfaceFactory;

	/**
     * @var \I95Dev\UploadPrescription\Api\Data\PrescriptionsInfoInterfaceFactory
     */
    protected $prescriptionsInfoInterfaceFactory;

    /**
     * @var \I95Dev\UploadPrescription\Api\Data\PartnerPrescriptionsOrderInterfaceFactory
     */
    protected $partnerPrescriptionsOrderInterfaceFactory;

    protected $cartRepositoryInterface;
    protected $cartManagementInterface;
    protected $customerFactory;
    protected $customerRepository;
    protected $orderCollectionFactory;
    protected $directoryList;
    protected $paymentInterface;

    /**
     * @var QuoteFactory
     */
    protected $quoteFactory;
	
    public function __construct(\I95Dev\UploadPrescription\Model\PrescriptionFactory  $prescription,
			\Magento\Quote\Model\QuoteRepository  $quoteRepo,
	        \Magento\Checkout\Model\Session $checkoutSession,
			\Magento\Store\Model\StoreManagerInterface $storeManager,
			PrescriptionsInfoInterfaceFactory $prescriptionsInfoInterfaceFactory,
			PrescriptionsOrderInterfaceFactory $prescriptionsOrderInterfaceFactory,
			PartnerPrescriptionsOrderInterfaceFactory $partnerPrescriptionsOrderInterfaceFactory,
			\Magento\Framework\Message\ManagerInterface $messageManager,
			\Magento\Quote\Model\Quote\Item $item,
			\Magento\Customer\Model\Customer $customer,
			\Magento\Customer\Model\Address $address,
			\Zoylo\Webservices\Logger\Logger $logger,
			\Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
	        \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
	        \Magento\Customer\Model\CustomerFactory $customerFactory,
	        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
			UpdateOrderInterfaceFactory $updateOrderInterfaceFactory,
			\Magento\Quote\Model\QuoteFactory $quoteFactory,
			\Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
			\Magento\Framework\Filesystem\DirectoryList $directoryList,
			\Magento\Quote\Api\Data\PaymentInterface $paymentInterface,
	        \Magento\Catalog\Model\ProductRepository $productRepository) {
    	$this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$this->_prescription = $prescription;
		$this->_quoteRepo = $quoteRepo;
		$this->_checkoutSession = $checkoutSession;
		$this->productRepository = $productRepository;
		$this->storeManager = $storeManager;
		$this->item = $item;
		$this->customer = $customer;
		$this->address = $address;
		$this->prescriptionsInfoInterfaceFactory = $prescriptionsInfoInterfaceFactory;
		$this->prescriptionsOrderInterfaceFactory = $prescriptionsOrderInterfaceFactory;
		$this->partnerPrescriptionsOrderInterfaceFactory = $partnerPrescriptionsOrderInterfaceFactory;
		$this->_messageManager = $messageManager;
		$this->logger = $logger;
		$this->updateOrderInterfaceFactory = $updateOrderInterfaceFactory;
		$this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->cartManagementInterface = $cartManagementInterface;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->quoteFactory = $quoteFactory;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->directoryList = $directoryList;
        $this->paymentInterface = $paymentInterface;
		} 
	 
	/**
	 * GET for Post api
	 * @param int $customerId
	 * @return \I95Dev\UploadPrescription\Api\Data\PrescriptionsInfoInterface
	 */
	public function getPrescriptionsByCustomerId($customerId)
		{
		try{
			$result = array();
			$prescriptioncollection = $this->_prescription->create()->getCollection()
				->addFieldToFilter('customer_id', array('eq' => $customerId))
				->addFieldToFilter('status', 1)
				->setOrder('prescription_id', 'DESC');
			if (count($prescriptioncollection) > 0) {
				foreach($prescriptioncollection as $prescription) {
					$prescriptionList[] = array(
						'prescription_id' => $prescription->getPrescriptionId() ,
						'customer_id' => $prescription->getCustomerId() ,
						'prescription_name' => $prescription->getPrescriptionName() ,
						'prescription_Label' => $prescription->getLabel() ,
						'prescription' => $prescription->getValue() ,
						'created_date' => $prescription->getCreatedDate()
					);
				}
				$data = [
	                'code' => 150,
	                'success' => 'true',
	                'message' => __('Prescriptions Information'),
	                'prescriptions_data' => $prescriptionList
	            ];
	            $result = $this->prescriptionsInfoInterfaceFactory->create();
	            $result->setData($data);
			}
			else {
				$data = [
	                'code' => 150,
	                'success' => 'false',
	                'message' => __('No Prescriptions'),
	                'prescriptions_data' => ''
	            ];
	            $result = $this->prescriptionsInfoInterfaceFactory->create();
	            $result->setData($data);
			}
			
		} catch(\Exception $e)
		{ 
			$message = $e->getMessage();
			$data = [
	                'code' => 150,
	                'success' => 'false',
	                'message' => $message,
	                'prescriptions_data' => ''
	            ];
	            $result = $this->prescriptionsInfoInterfaceFactory->create();
	            $result->setData($data);
		}
		return $result;
	}
	
	/**
	 * Delete Prescription
	 * @param int $customerId
	 * @param string $prescriptionId
	 * @return \I95Dev\UploadPrescription\Api\Data\PrescriptionsInfoInterface
	 */
	public function deletePrescription($customerId,$prescriptionId)
	{
		try{
			$prescriptions = $this->objectManager->get('I95Dev\UploadPrescription\Model\Prescription')->load($prescriptionId);
			$customer_id = $prescriptions->getCustomerId();
			if($customerId != $customer_id)
				throw new \Exception("Please provide valid customer token..");
			if($prescriptionId){
				$prescriptions->setStatus(0);
				$prescriptions->save();
				$result = true;
				$result = $this->getPrescriptionsByCustomerId($customer_id);
			}
		}catch(\Exception $e)
		{
			$message = $e->getMessage();
			$data = [
	                'code' => 150,
	                'success' => 'false',
	                'message' => $message,
	                'prescriptions_data' => ''
	            ];
            $result = $this->prescriptionsInfoInterfaceFactory->create();
            $result->setData($data);
		}
		return $result;
	}

	public function saveFileToDb($customerId, $fileName, $label, $imageType){
		try{
			$fileName 	= $this->seo_friendly_url($fileName);
			$mediaUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
			$baseTmpPath = "prescriptions/$customerId/";
			$filepullpath = $mediaUrl.$baseTmpPath.$fileName.'.'.$imageType;
			$prescriptionmodel = $this->_prescription->create();
			$prescriptionmodel->setCustomerId($customerId);
			$prescriptionmodel->setPrescriptionName($fileName);
			$prescriptionmodel->setLabel($label);
			$prescriptionmodel->setValue($filepullpath);
			$prescriptionmodel->setCreatedDate(date('Y-m-d H:i:s', time()));
			$prescriptionmodel->setStatus(1);
			$prescriptionmodel->save();
			$prescription_id = $prescriptionmodel->getPrescriptionId();
				
		} catch(\Exception $e) {
			die($e->getMessage());
		}
		return $prescription_id;		
	}
		
	/**
	 * @param string $customerId
     * @param mixed $entry
     * @return \I95Dev\UploadPrescription\Api\Data\PrescriptionsInfoInterface
     */
    public function saveFileToDir($customerId,$entry) {
		try{
			error_reporting(0);
			$this->logger->info('Upload Prescriptions: ');
			$customer_id = $entry['customer_id'];
			if($customerId != $customer_id)
				throw new \Exception("Please provide valid customer token..");
			$media_type = explode('/', $entry['media_type']);
			$imageType = $media_type[1];
			if($imageType == 'jpeg')
				$imageType = 'jpg';
			$label = $entry['label'];
			$fileName = $entry['name'];
			$fileName 	= $this->seo_friendly_url($fileName);
			
			$this->logger->info('Customer Id: '.$customer_id);
			$this->logger->info('Media Type: '.$imageType);
			$this->logger->info('Label: '.$label);
			$this->logger->info('File Name: '.$fileName);

			$rootPath  =  $this->directoryList->getRoot();
			$folderpath = $rootPath.'/pub/media/prescriptions/'.$customer_id;
			if(!is_dir($folderpath)){
				if (!mkdir($folderpath, 0777, true))
				{
		            throw new \Exception("Unable to create directory '{$folderpath}'.");
		        }
	   		}

			$prescription_id = $this->saveFileToDb($customer_id, $fileName, $label, $imageType);
			$data = $entry['content']['base64_encoded_data'];
			$data = str_replace(' ', '+', $data);
			$data = base64_decode($data);
			$file = $rootPath.'/pub/media/prescriptions/'.$customer_id.'/'.$fileName.'.'.$imageType;
			$success = file_put_contents($file, $data);
			$data = base64_decode($data); 
			$source_img = imagecreatefromstring($data);
			$file = $rootPath.'/pub/media/prescriptions/'.$customer_id.'/'.$fileName.'.'.$imageType;
			$imageSave = imagejpeg($source_img, $file, 10);
			
			$uploaddFilePath = 'prescriptions/'.$customer_id.'/'.$fileName.'.'.$imageType;
			$uploader = $this->objectManager->create('I95Dev\S3\Model\MediaStorage\File\Storage\S3');
			$ddddd = $uploader->saveFile($uploaddFilePath);
			imagedestroy($source_img);
            $result = $this->getPrescriptionsByCustomerId($customer_id);
			
		} catch(\Exception $e) {
			$message = $e->getMessage();
			$data = [
	                'code' => 150,
	                'success' => 'false',
	                'message' => $message,
	                'prescriptions_data' => ''
	            ];
            $result = $this->prescriptionsInfoInterfaceFactory->create();
            $result->setData($data);
		}
		return $result;
    }
	
	
	public function prescriptionsProcess($data)
	{
		try {
			$customer_id = $data['customer_id'];
			$quote_id = $data['quote_id'];
			$prescription_ids = $data['prescription_id'];
			$test_status = $data['test_status'];
			$all_items = $data['all_items'];
			$some_items = $data['some_items'];
			$type = $data['type'];
			$fromCart = $data['from_cart'];
			
			if (isset($all_items) && $all_items[0]['status'] == 1) {
				$comments = $all_items[0]['comment'];
			}

			if (isset($some_items) && $some_items[0]['status'] == 1) {
				$comments = $some_items[0]['comment'];
			}

			if ($test_status == 1) {
				$comments = "Tests included in the attached prescription." . $comments;
			}
			else {
				$comments = $comments;
			}

			$quoteModel = $this->_quoteRepo->get($quote_id);

			if($fromCart == 0){
				//Removing items from cart
	            $itemsCount  = $quoteModel->getItemsCount();
	            $quoteAllItems = $quoteModel->getAllItems();
	            if($itemsCount > 0){
	                foreach ($quoteAllItems as $item) {
	                    $sku = $item->getSku();
	                    //$quote->removeItem($item->getId())->save();
	                    $itemId = $item->getItemId();//item id of particular item
	                    $quoteItem=$this->getItemModel()->load($itemId);//load particular item which you want to delete by his item id
	                    $quoteItem->delete();//deletes the item
	                }
	                $this->_quoteRepo->save($quoteModel);
	                $message = __('All cart items has been deleted');
	            }else{
	                $message = __('No items to delete in the cart');
	            }
			}
			
			if ($type == 1) {
				
				$productRepository = $this->productRepository;
				$product = $productRepository->get('Free');
				$product->setPrice(0);
	        	$quoteModel->addProduct($product, 1);
	        	$this->paymentInterface->setMethod('cashondelivery');	
				$quoteModel->setPayment($this->paymentInterface);

			}

			$quoteModel->setPrescriptionValues($prescription_ids);
			$quoteModel->setOrderComments($comments);
			$quoteModel->setOrigin('MobileApp');
			$quoteModel->save();
			return true;
		}

		catch(Exception $e) {
			return false;
			die($e->getMessage());
		}
	}

	public function getItemModel(){
        $itemModel = $this->objectManager->create('Magento\Quote\Model\Quote\Item');//Quote item model to load quote item
        return $itemModel;
    }

	/**
     * Update order information.
     *
     * @param mixed $orderData
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return \I95Dev\UploadPrescription\Api\Data\UpdateOrderInterface containing Update Order objects
     */
    public function updateOrder($orderData){
    	try{
			error_reporting(0);
			$this->logger->info('Update Prescriptions Order : ');
			$orderId = $orderData['order_id'];
			$orderStatus = $orderData['status'];
			$order = $this->objectManager->create('\Magento\Sales\Model\Order')->loadByIncrementId($orderId);
//			if(!$order->getIncrementId())
//				throw new \Exception(__('This order no longer exists.'));
			$customer_id = $order->getCustomerId();
			$media_type = explode('/', $orderData['media_type']);
			$imageType = $media_type[1];
			if($imageType == 'jpeg')
				$imageType = 'jpg';
			$label = $orderData['label'];
			$fileName = $orderData['name'];
			$fileName = preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileName);
			$fileName = $this->seo_friendly_url($fileName);
			
			$this->logger->info('Order Id: '.$orderId);
			$this->logger->info('Customer Id: '.$customer_id);
			$this->logger->info('Media Type: '.$imageType);
			$this->logger->info('Label: '.$label);
			$this->logger->info('File Name: '.$fileName);

			$rootPath  =  $this->directoryList->getRoot();
			$folderpath = $rootPath.'/pub/media/prescriptions/'.$customer_id;
			if(!is_dir($folderpath)){
				if (!mkdir($folderpath, 0777, true))
				{
		            throw new \Exception("Unable to create directory '{$folderpath}'.");
		        }
	   		}
			$prescription_id = $this->saveFileToDb($customer_id, $fileName, $label, $imageType);
			$data = $orderData['content']['base64_encoded_data'];
			$data = str_replace(' ', '+', $data);
			$data = base64_decode($data);
			$file = $rootPath.'/pub/media/prescriptions/'.$customer_id.'/'.$fileName.'.'.$imageType;
			$success = file_put_contents($file, $data);
			//$data = base64_decode($data); 
			$source_img = imagecreatefromstring($data);
			$file = $rootPath.'/pub/media/prescriptions/'.$customer_id.'/'.$fileName.'.'.$imageType;
			$imageSave = imagejpeg($source_img, $file, 10);
			$uploaddFilePath = 'prescriptions/'.$customer_id.'/'.$fileName.'.'.$imageType;
			$uploader = $this->objectManager->create('I95Dev\S3\Model\MediaStorage\File\Storage\S3');
			$ddddd = $uploader->saveFile($uploaddFilePath);
			imagedestroy($source_img);

			$old_prescriptionValues = $order->getData('prescription_values');
			$newIds = $old_prescriptionValues.",".$prescription_id;
			$order->setPrescriptionValues($newIds);
			if($orderStatus != '' && $orderStatus != NULL && $orderStatus != null && $orderStatus != 'undefined')
				$order->setStatus($orderStatus);
			$order->save();

			$prescriptionValues = $order->getData('prescription_values');
            $message = __('Order Info');
            $data = [
                'code'                  => 170,
                'success'               => 'true',
                'message'               => $message,
                'status'                => $status,
                'prescription_values'   => $prescriptionValues,
                'order_info'            => $order
            ];
            $result = $this->updateOrderInterfaceFactory->create();
            $result->setData($data);

        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $message = $e->getMessage();
            $data = [
                'code'                  => 170,
                'success'               => 'false',
                'message'               => $message,
                'status'                => '',
                'prescription_values'   => '',
                'order_info'             => ''
            ];
            $result = $this->updateOrderInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $data = [
                'code'                  => 170,
                'success'               => 'false',
                'message'               => $message,
                'status'                => '',
                'prescription_values'   => '',
                'order_info'             => ''
            ];
            $result = $this->updateOrderInterfaceFactory->create();
            $result->setData($data);
        }catch (CouldNotSaveException $e) {
            $message = $e->getMessage();
            $data = [
                'code'                  => 170,
                'success'               => 'false',
                'message'               => $message,
                'status'                => '',
                'prescription_values'   => '',
                'order_info'             => ''
            ];
            $result = $this->updateOrderInterfaceFactory->create();
            $result->setData($data);
        }
        return $result;
    }

	/**
	 * Prescriptions Order Create
	 * @param mixed $data
	 * @throws \Magento\Framework\Exception\CouldNotSaveException
	 * @return \I95Dev\UploadPrescription\Api\Data\PrescriptionsOrderInterface
	 */
	public function createOrder($data){
		
		try{

			$customer_id = $data['customer_id'];
			$quote_id = $data['quote_id'];
			$customer_address_id = $data['customer_address_id'];
			
			$customer = $this->customer->load($customer_id);
			$customerData=$customer->getData();
			
			$customerAddress = $this->address->load($customer_address_id);
			$customerAddressData=$customerAddress->getData();
			
			$billingAddress=$this->objectManager->create('Magento\Quote\Api\Data\AddressInterface');
			$billingAddress->setCustomerId($customerData['entity_id']);
			$billingAddress->setCustomerAddressId($customerAddressData['entity_id']);
			$billingAddress->setRegionId($customerAddressData['region_id']);
			$billingAddress->setRegion($customerAddressData['region']);
			$billingAddress->setPostcode($customerAddressData['postcode']);
			$billingAddress->setFax($customerAddressData['fax']);
			$billingAddress->setStreet($customerAddressData['street']);
			$billingAddress->setLastname($customerAddressData['lastname']);
			$billingAddress->setCity($customerAddressData['city']);
			$billingAddress->setEmail($customerData['email']);
			$billingAddress->setTelephone($customerAddressData['telephone']);
			$billingAddress->setFirstname($customerAddressData['firstname']);
			$billingAddress->setPrefix($customerAddressData['prefix']);
			$billingAddress->setSuffix($customerAddressData['suffix']);
			$billingAddress->setMiddlename($customerAddressData['middlename']);
			$billingAddress->setCountryId($customerAddressData['country_id']);	
			$billingAddress->setAddressType("billing");
	
			$shippingAddress=$this->objectManager->create('Magento\Quote\Api\Data\AddressInterface');
			$shippingAddress->setCustomerId($customerData['entity_id']);
			$shippingAddress->setCustomerAddressId($customerAddressData['entity_id']);
			$shippingAddress->setRegionId($customerAddressData['region_id']);
			$shippingAddress->setRegion($customerAddressData['region']);
			$shippingAddress->setPostcode($customerAddressData['postcode']);
			$shippingAddress->setFax($customerAddressData['fax']);
			$shippingAddress->setStreet($customerAddressData['street']);
			$shippingAddress->setLastname($customerAddressData['lastname']);
			$shippingAddress->setCity($customerAddressData['city']);
			$shippingAddress->setEmail($customerData['email']);
			$shippingAddress->setTelephone($customerAddressData['telephone']);
			$shippingAddress->setFirstname($customerAddressData['firstname']);
			$shippingAddress->setPrefix($customerAddressData['prefix']);
			$shippingAddress->setSuffix($customerAddressData['suffix']);
			$shippingAddress->setMiddlename($customerAddressData['middlename']);
			$shippingAddress->setCountryId($customerAddressData['country_id']);	
			$shippingAddress->setAddressType("shipping");
	
			$quote = $this->_quoteRepo->get($quote_id);
			$quote->setBillingAddress($billingAddress);
			$quote->setShippingAddress($shippingAddress);			
			$quote->setCustomerEmail($customerData['email']);
			
			
			$this->paymentInterface->setMethod('cashondelivery');	
			$quote->setPayment($this->paymentInterface);
			//$quote->setOrigin('MobileApp');
			if(isset($data['order_by'])){
				$quote->setSource('Affiliate');
				$quote->setOrderBy($data['order_by']);
				$quote->setOrigin('Affiliate');
			}else{
				$quote->setOrigin('MobileApp');
				$quote->setOrderBy('');
			}
			//$quote->setOrderBy(isset($data['order_by']) ? $data['order_by']: '');
			
			$quote->getShippingAddress()->setShippingMethod('ecomexpress_ecomexpress')->setCollectShippingRates(true);
			$quote->getShippingAddress()->collectShippingRates();
			$quote->setInventoryProcessed(false);
			$quote->collectTotals();
			$quote->save();
			
			$order = $this->cartManagementInterface->submit($quote);

			if ($order == null) {
				throw new CouldNotSaveException(
                    __('An error occurred on the server. Please try to place the order again1.')
                );
			}

			$id = $order->getId();

 			$status = 'success';
 			$message = __('Order Info');
            $data = [
                'code'          => 160,
                'success'       => 'true',
                'message'       => $message,
                'status'        => $status,
                'order_info'    => $order
            ];
            $result = $this->prescriptionsOrderInterfaceFactory->create();
            $result->setData($data);

		  
		 } catch (\Magento\Framework\Exception\LocalizedException $e) {
			$status = 'cancel';
        	$message = $e->getMessage();
        	$success = 'false';
            $data = [
                'code'          => 160,
                'success'       => $success,
                'message'       => $message,
                'status'        => $status,
                'order_info'    => ''
            ];
            $result = $this->prescriptionsOrderInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
        	
            $status = 'cancel';
            $message = $e->getMessage();
            $data = [
                'code'          => 160,
                'success'       => 'false',
                'message'       => $message,
                'status'        => $status,
                'order_info'    => ''
            ];
            $result = $this->prescriptionsOrderInterfaceFactory->create();
            $result->setData($data);
        }catch (CouldNotSaveException $e) {
        	$status = 'cancel';
            $message = $e->getMessage();
            $data = [
                'code'          => 160,
                'success'       => 'false',
                'message'       => $message,
                'status'        => $status,
                'order_info'    => ''
            ];
            $result = $this->prescriptionsOrderInterfaceFactory->create();
            $result->setData($data);
        }
        return $result;
	}

	/**
	 * Prescriptions Order Create
	 * @param mixed $orderData
	 * @throws \Magento\Framework\Exception\CouldNotSaveException
	 * @return \I95Dev\UploadPrescription\Api\Data\PartnerPrescriptionsOrderInterface
	 */
	public function prescriptionCreateOrder($orderData){
		
		try{
			$email = $orderData['email'];
			$firstname = $orderData['firstname'];
			$lastname = $orderData['lastname'];
			$password = "Zoylo@321#";
			$telephone = $orderData['telephone'];
			$telephone = substr($telephone,-10);
			$company = $orderData['company'];
			$street = $orderData['street'];
			$city = $orderData['city'];
			$region = $orderData['region'];
			$region_id = $region['region_id'];
			$country_id = $orderData['country_id'];
			$postcode = $orderData['postcode'];
			$gender = $orderData['gender'];
			$media_type = explode('/', $orderData['media_type']);
			$data = $orderData['content']['base64_encoded_data'];
			$customerData = [
				"addresses" => [
			        "region" => $region,
			        "region_id" => $region_id,
			        "country_id" => $country_id,
			        "street" => $street,
			        "company" => $company,
			        "telephone" => $telephone,
			        "postcode" => $postcode,
			        "city" => $city,
			        "firstname" => $firstname,
			        "lastname" => $lastname,
			        "default_shipping" => 1,
			        "default_billing" => 1
				]
			];
			
			if($telephone == ''){
				throw new \Exception("Please provide the mobilenumber..");
			}elseif(!\Zend_Validate::is(trim($email), 'EmailAddress')){
				throw new \Exception("Please provide the email..");
			}elseif(!\Zend_Validate::is(trim($firstname), 'NotEmpty')){
				throw new \Exception("Please provide the firstname..");
			}elseif(!\Zend_Validate::is(trim($lastname), 'NotEmpty')){
				throw new \Exception("Please provide the lastname..");
			}elseif(!\Zend_Validate::is(trim($company), 'NotEmpty')){
				throw new \Exception("Please provide the company..");
			}elseif(!\Zend_Validate::is($street, 'NotEmpty')){
				throw new \Exception("Please provide the street..");
			}elseif(!\Zend_Validate::is(trim($city), 'NotEmpty')){
				throw new \Exception("Please provide the city..");
			}elseif(!\Zend_Validate::is($region, 'NotEmpty')){
				throw new \Exception("Please provide the region..");
			}elseif(!\Zend_Validate::is(trim($region_id), 'NotEmpty')){
				throw new \Exception("Please provide the region_id..");
			}elseif(!\Zend_Validate::is(trim($country_id), 'NotEmpty')){
				throw new \Exception("Please provide the country_id..");
			}elseif(!\Zend_Validate::is(trim($postcode), 'NotEmpty')){
				throw new \Exception("Please provide the postcode..");
			}elseif(!\Zend_Validate::is($media_type, 'NotEmpty')){
				throw new \Exception("Please provide the media type..");
			}elseif(!\Zend_Validate::is($data, 'NotEmpty')){
				throw new \Exception("Please provide the base64 encoded content..");
			}
			error_reporting(0);
			$customerId = '';
			$origin 	= $orderData['origin'];
			$source 	= $orderData['source'];
			$store=$this->storeManager->getStore();
	        $websiteId = $this->storeManager->getStore()->getWebsiteId();

	        //Save Customer
	        $customerCollection = $this->customerFactory->create()
	        	->getCollection()
	        	->addAttributeToSelect("*")
           		->addAttributeToFilter("customer_number", array("eq" => $telephone));
	        //$customer->loadByEmail($email);// load customet by email address
           	if (count($customerCollection) > 0) {
	            foreach ($customerCollection as $customer) {
	            }
	        }else{
	        	//If not avilable then create this customer 
	        	$customer = $this->customerFactory->create();
	            $customer->setWebsiteId($websiteId)
	                    ->setStore($store)
	                    ->setFirstname($firstname)
	                    ->setLastname($lastname)
	                    ->setEmail($email) 
	                    ->setPassword($password)
	                    ->setCustomerNumber($telephone)
	                    ->setMobileNumber($telephone);
	            $customer->save();

	            $addresss = $this->objectManager->get('\Magento\Customer\Model\AddressFactory');
	            $address = $addresss->create();
	            $address->setCustomerId($customer->getId())
	                    ->setFirstname($firstname)
	                    ->setLastname($lastname)
	                    ->setCountryId($country_id)
	                    ->setPostcode($postcode)
	                    ->setCity($city)
	                    ->setRegion($region)
	                    ->setRegionId($region_id)
	                    ->setTelephone($telephone)
	                    ->setCompany($company)
	                    ->setStreet($street)
	                    ->setCustomerAddressType('home')
	                    ->setIsDefaultBilling('1')
	                    ->setIsDefaultShipping('1')
	                    ->setSaveInAddressBook('1');
	            $address->save();

	        }
	        $customerId = $customer->getId();

	        //Save Prescription Image
			$imageType = $media_type[1];
			if($imageType == 'jpeg')
				$imageType = 'jpg';
			$label 		= $orderData['prescription_name'];
			$label 		= $this->seo_friendly_url($label);
			$fileName 	= $orderData['prescription_name'];
			$fileName 	= preg_replace('/\\.[^.\\s]{3,4}$/', '', $fileName);
			$fileName 	= $this->seo_friendly_url($fileName);

			$this->logger->info('Customer Id: '.$customerId);
			$this->logger->info('Media Type: '.$imageType);
			$this->logger->info('Label: '.$label);
			$this->logger->info('File Name: '.$fileName);

			$rootPath  =  $this->directoryList->getRoot();
			$folderpath = $rootPath.'/pub/media/prescriptions/'.$customerId;
			if(!is_dir($folderpath)){
				if (!mkdir($folderpath, 0777, true))
				{
		            throw new \Exception("Unable to create directory '{$folderpath}'.");
		        }
	   		}
			$prescriptionId = $this->saveFileToDb($customerId, $fileName, $label, $imageType);
			
			$data = str_replace(' ', '+', $data);
			$data = base64_decode($data);
			$file = $rootPath.'/pub/media/prescriptions/'.$customerId.'/'.$fileName.'.'.$imageType;
			$success = file_put_contents($file, $data);
			//$data = base64_decode($data); 
			$source_img = imagecreatefromstring($data);
			$file = $rootPath.'/pub/media/prescriptions/'.$customerId.'/'.$fileName.'.'.$imageType;
			$imageSave = imagejpeg($source_img, $file, 10);
			$uploaddFilePath = 'prescriptions/'.$customerId.'/'.$fileName.'.'.$imageType;
			$uploader = $this->objectManager->create('I95Dev\S3\Model\MediaStorage\File\Storage\S3');
			$ddddd = $uploader->saveFile($uploaddFilePath);
			imagedestroy($source_img);

			//Create Quote
	        $quote = $this->quoteFactory->create();
	        $quote->setStore($store);
	        // if you have allready buyer id then you can load customer directly 
	        $customer= $this->customerRepository->getById($customer->getEntityId());
	        $quote->setCurrency();
	        $quote->assignCustomer($customer); //Assign quote to customer
	 		
	 		$productRepository = $this->productRepository;
			$product = $productRepository->get('Free');
			$product->setPrice(0);
	        $quote->addProduct($product, 1);
	        
	        //Set Address to quote
	        $quote->getBillingAddress()->addData($customerData['addresses']);
	        $quote->getShippingAddress()->addData($customerData['addresses']);
			$quote->setCustomerEmail($email);
			$quote->setOrigin($origin);
			$quote->setSource($source);
			$quote->setOrderBy(isset($orderData['order_by']) ? $orderData['order_by']: ''); //Update order_by columm
			$quote->setPrescriptionValues($prescriptionId);
			$this->paymentInterface->setMethod('cashondelivery');	
			$quote->setPayment($this->paymentInterface);
			$quote->setInventoryProcessed(false);
			$quote->collectTotals();
			$quote->save();
			

			$quote_id = $quote->getEntityId();
			$quote = $this->_quoteRepo->get($quote_id);
			$quote->getShippingAddress()->setShippingMethod('ecomexpress_ecomexpress')->setCollectShippingRates(true);
			$quote->getShippingAddress()->collectShippingRates();
			$quote->collectTotals();
			$quote->save();
			
			//Create Order
			$order = $this->cartManagementInterface->submit($quote);

			if ($order == null) {
				throw new CouldNotSaveException(
                    __('An error occurred on the server. Please try to place the order again1.')
                );
			}

 			$status = $order->getStatus();
 			$incrementId = $order->getIncrementId();
 			$message = __('Order Info');
            $data = [
                'success'       => 'true',
                'message'       => $message,
                'status'        => $status,
                'order_id'    	=> $incrementId
            ];
            $result = $this->partnerPrescriptionsOrderInterfaceFactory->create();
            $result->setData($data);

		  
		 } catch (\Magento\Framework\Exception\LocalizedException $e) {
		 	//Recent Order Object
		 	$order 			= '';
		 	$incrementId 	= '';
		 	$orderCollection = $this->orderCollectionFactory->create()->addFieldToSelect('*')
		 		->addFieldToFilter('customer_id',$customerId)
		 		->setOrder('created_at','desc')->setPageSize(1);
		 	if(count($orderCollection) > 0){
		 		foreach ($orderCollection as $order) {
		 			
		 		}
				if($order->getId()){
					$status 		= $order->getStatus();
 					$incrementId 	= $order->getIncrementId();
	 				$message 		= __('Order Info');
	 				$success 		= 'true';
				}
		 	}else{
				$status 		= '';
            	$message 		= $e->getMessage();
            	$success 		= 'false';
			}
		 	
            $data = [
                'success'       => $success,
                'message'       => $message,
                'status'        => $status,
                'order_id'    	=> $incrementId
            ];
            $result = $this->partnerPrescriptionsOrderInterfaceFactory->create();
            $result->setData($data);
        } catch (\Exception $e) {
        	
            $status = '';
            $message = $e->getMessage();
            $data = [
                'success'       => 'false',
                'message'       => $message,
                'status'        => $status,
                'order_id'    	=> ''
            ];
            $result = $this->partnerPrescriptionsOrderInterfaceFactory->create();
            $result->setData($data);
        }catch (CouldNotSaveException $e) {
        	$status = '';
            $message = $e->getMessage();
            $data = [
                'success'       => 'false',
                'message'       => $message,
                'status'        => $status,
                'order_id'    	=> ''
            ];
            $result = $this->partnerPrescriptionsOrderInterfaceFactory->create();
            $result->setData($data);
        }
        return $result;
	}
	
	public function seo_friendly_url($string){
		$string = str_replace(array('[\', \']'), '', $string);
		$string = preg_replace('/\[.*\]/U', '', $string);
		$string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
		$string = htmlentities($string, ENT_COMPAT, 'utf-8');
		$string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
		$string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
		return strtolower(trim($string, '-'));
	}
}