<?php

namespace I95Dev\UploadPrescription\Model\Data;

class PartnerPrescriptionsOrder extends \Magento\Framework\Model\AbstractModel implements
    \I95Dev\UploadPrescription\Api\Data\PartnerPrescriptionsOrderInterface
{
    const KEY_Success       = 'success';
    const KEY_Message       = 'message';
    const KEY_Status        = 'status';
    const KEY_OrderId       = 'order_id';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
    
    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess()
    {
        return $this->_getData(self::KEY_Success);
    }

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success)
    {
        return $this->setData(self::KEY_Success, $success);
    }

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->_getData(self::KEY_Message);
    }

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        return $this->setData(self::KEY_Message, $message);
    }

    /**
     * Get Status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->_getData(self::KEY_Status);
    }

    /**
     * Set Status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        return $this->setData(self::KEY_Status, $status);
    }

    /**
     * Get OrderId
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->_getData(self::KEY_OrderId);
    }

    /**
     * Set Order Id
     *
     * @param string $status
     * @return $this
     */
    public function setOrderId($incrementId)
    {
        return $this->setData(self::KEY_OrderId, $incrementId);
    }
}