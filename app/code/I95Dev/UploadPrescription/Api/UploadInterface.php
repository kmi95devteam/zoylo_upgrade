<?php 
namespace I95Dev\UploadPrescription\Api;

interface UploadInterface {

	/**
	 * GET for Post api
	 * @param string $customerId
	 * @return \I95Dev\UploadPrescription\Api\Data\PrescriptionsInfoInterface
	 */
	public function getPrescriptionsByCustomerId($customerId);
	
	/**
	 * GET for Post api
	 * @param mixed $data
	 * @return string
	 */
	public function prescriptionsProcess($data);
	
	/**
	 * @param string $customerId
     * @param mixed $entry
     * @return \I95Dev\UploadPrescription\Api\Data\PrescriptionsInfoInterface
     */
    public function saveFileToDir($customerId,$entry);
	
	/**
	 * Prescriptions Order Create
	 * @param mixed $data
	 * @throws \Magento\Framework\Exception\CouldNotSaveException
	 * @return \I95Dev\UploadPrescription\Api\Data\PrescriptionsOrderInterface
	 */
	public function createOrder($data);

	/**
	 * Delete Prescription
	 * @param int $customerId
	 * @param string $prescriptionId
	 * @return \I95Dev\UploadPrescription\Api\Data\PrescriptionsInfoInterface
	 */
	public function deletePrescription($customerId,$prescriptionId);

	/**
	 * Prescriptions Order Create
	 * @param mixed $orderData
	 * @throws \Magento\Framework\Exception\CouldNotSaveException
	 * @return \I95Dev\UploadPrescription\Api\Data\PartnerPrescriptionsOrderInterface
	 */
	public function prescriptionCreateOrder($orderData);
	
	/**
     * Update order information.
     *
     * @param mixed $orderData
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return \I95Dev\UploadPrescription\Api\Data\UpdateOrderInterface containing Update Order objects
     */
    public function updateOrder($orderData);
}