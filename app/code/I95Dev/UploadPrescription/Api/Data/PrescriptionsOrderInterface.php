<?php
namespace I95Dev\UploadPrescription\Api\Data;
/**
 * @api
 *
 */
interface PrescriptionsOrderInterface
{
    
    /**
     * Get Code
     *
     * @return int
     */
    public function getCode();

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess();

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success);

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage();

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * Get Status
     *
     * @return string
     */
    public function getStatus();

    /**
     * Set Status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * Get OrderInfo
     *
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function getOrderInfo();

    /**
     * Set OrderInfo
     *
     * @param \Magento\Sales\Api\Data\OrderInterface
     * @return $this
     */
    public function setOrderInfo(array $order = null);
}