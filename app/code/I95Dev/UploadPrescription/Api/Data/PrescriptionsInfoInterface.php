<?php
namespace I95Dev\UploadPrescription\Api\Data;
/**
 * @api
 *
 */
interface PrescriptionsInfoInterface
{
    
    /**
     * Get Code
     *
     * @return int
     */
    public function getCode();

    /**
     * Set Response Code
     *
     * @param int $code
     * @return $this
     */
    public function setCode($code);

    /**
     * Get Success Status
     *
     * @return string
     */
    public function getSuccess();

    /**
     * Set Success Status
     *
     * @param string $success
     * @return $this
     */
    public function setSuccess($success);

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage();

    /**
     * Set Message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message);

    /**
     * Set Prescriptions Data
     *
     * @return \I95Dev\UploadPrescription\Api\Data\PrescriptionsInfoInterface[]
     */
    public function getPrescriptionsData();

    /**
     * Get Prescriptions Data
     *
     * @param \I95Dev\UploadPrescription\Api\Data\PrescriptionsInfoInterface[] $prescriptionsList
     * @return $this
     */
    public function setPrescriptionsData(array $prescriptionsList = null);
}