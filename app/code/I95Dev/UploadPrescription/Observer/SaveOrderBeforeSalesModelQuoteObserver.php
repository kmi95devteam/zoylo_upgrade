<?php

namespace I95Dev\UploadPrescription\Observer;

use Magento\Framework\Event\ObserverInterface;

class SaveOrderBeforeSalesModelQuoteObserver implements ObserverInterface
{

    /**
     * List of attributes that should be added to an order.
     *
     * @var array
     */
    private $attributes = [
        'prescription_values',
        'origin',
        'source',
        'order_by'
    ];

    /**
     *
     * @var Http 
     */
    protected $request;
    
    /**
     *
     * @var UrlInterface 
     */
    protected $urlInterface;
    
    public function __construct(\Magento\Framework\App\Request\Http $request,\Magento\Framework\UrlInterface $urlInterface)
    {
        $this->request = $request;
        $this->urlInterface = $urlInterface;
    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /* @var Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getData('order');
        /* @var Magento\Quote\Model\Quote $quote */
        $quote = $observer->getEvent()->getData('quote');

        foreach ($this->attributes as $attribute) {
            if ($quote->hasData($attribute)) {
                $order->setData($attribute, $quote->getData($attribute));
            }
        }
        $comment = $quote->getOrderComments() ? $quote->getOrderComments() : $quote->getEcomteckOrderComment(); // frontend fix
        if ($comment == '' && strpos($this->urlInterface->getCurrentUrl(), 'admin/sales/order_create/save') !== false ) // if the comments is empty then our custom code will execute and refill the order comment that was not saved the quote table and we are checking that the request url is from admin order create so that impact area of this custom code remains limited to admin side order functionality 
        {
            $reqComment = $this->request->getParam('order');
            if (isset($reqComment['comment']['customer_note']))
            {
                $comment = $reqComment['comment']['customer_note'];
            }
        }
        if (isset($comment)) {
                $order->addStatusHistoryComment($comment);
        }
        return $this;
    }
}