<?php

/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace I95Dev\UploadPrescription\Controller\Checkout;

class Index extends \Magento\Checkout\Controller\Onepage {

    /**
     * Checkout page
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {

        /** @var \Magento\Checkout\Helper\Data $checkoutHelper */
        $checkoutHelper = $this->_objectManager->get(\Magento\Checkout\Helper\Data::class);
		

        if (!$checkoutHelper->canOnepageCheckout()) {
            $this->messageManager->addError(__('One-page checkout is turned off.'));
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }
        $data = $this->getRequest()->getPost();
        $quote = $this->getOnepage()->getQuote();
        $prescription_values = '';
        if (isset($_POST['checkboxvar'])) {
            foreach ($_POST['checkboxvar'] as $check) {
                $resultstr[] = $check;
            }
            $prescription_values = implode(",", $resultstr);
        } ?>
		
		<?php if ($quote->hasItems()) {
            
            $quote = $this->getOnepage()->getQuote();
            if (isset($_POST['submit'])) {
                if (isset($_POST['presc-option'])) {
                    $order_comments = 'I will submit at the time of delivery';
                    $quote->setOrderComments((isset($order_comments) ? $order_comments : ''));
                    $quote->save();
                }
            } else {
                
                $allItems = $quote->getAllItems();
                    foreach ($allItems as $item) {
                        $sku = $item->getSku();
                        $productRepository = $this->_objectManager->create('Magento\Catalog\Model\ProductRepository');
                        $product = $productRepository->get($sku);
                        $prescriptionAction = $product->getPrescriptionRequired();
                        if (isset($prescriptionAction) && $prescriptionAction == 'required') {
                            $required[] = $sku;
                        }
                    }
                    if (!empty($required) && empty($resultstr)) {
                        //$this->messageManager->addWarning(__('Please select prescription options.'));
                        return $this->resultRedirectFactory->create()->setPath('checkout/cart');
                    }
            }
          //  $quote->setPrescriptionValues((isset($prescription_values) ? $prescription_values : ''));
          //  $quote->save();
        }
		?>
<div style="display:none;"><?php echo $prescription_values;?></div> 
<?php
        if (isset($_POST['order_comment']) && $_POST['order_comment'] != '') {
            $order_comments = "All - " . $_POST['order_comment'];
        }
        if (isset($_POST['medicine_name']) && $_POST['medicine_name'] != '') {
            $count_val = count($_POST['medicine_name']);
            $medicine_name = $_POST['medicine_name'];
            $medicine_qty = $_POST['medicine_qty'];
            if (isset($_POST['medicine_name']) && $_POST['medicine_name'] != '') {
                for ($i = 0; $i < $count_val; $i++) {
                    $medicine_namestr[] = $medicine_name[$i] . ' - ' . $medicine_qty[$i];
                }
            }
            $medicine_namestr_values = implode(",", $medicine_namestr);
            if (isset($_POST['order_comment']) && $_POST['order_comment'] != '') {
                $order_comments = $order_comments . ' - ' . $medicine_namestr_values;
            } else {
                $order_comments = $medicine_namestr_values;
            }
        }

        if (isset($_POST['specify'])) {
            if ($_POST['specify'] == 1) {
                $specify = "Tests included in the attached prescription.";
                if (@$order_comments != '') {
                    $order_comments = $specify . ' -- ' . $order_comments;
                } else {
                    $order_comments = $specify;
                }
            } else {
                $order_comments;
            }
        }

        

        if (isset($_POST['onlyuploadflow'])) {
            if ($_POST['onlyuploadflow'] == 1) {
                if (!$quote->hasItems()) {

                    $product = $this->_objectManager->get('Magento\Catalog\Model\ProductRepository')->get('Free');
                    $formKey = $this->_objectManager->get('Magento\Framework\Data\Form\FormKey');
                    $params = array(
                        'form_key' => $formKey,
                        'product' => $product->getId(),
                        'qty' => 1
                    );
                    $cart = $this->_objectManager->get('\Magento\Checkout\Model\Cart')->addProduct($product, $params);
                    $cart->save();
                    $cart->setCartWasUpdated(true);

                    // Load the Quote,QuoteId and update the prescription values
                    $quote = $this->getOnepage()->getQuote();

                    $quote->getShippingAddress()->setShippingMethod('ecomexpress_ecomexpress')->setCollectShippingRates(true);
                    $quote->getShippingAddress()->collectShippingRates();
                    $quote->collectTotals();
                    $quote->setInventoryProcessed(true);
                    $quote->setOrderComments((isset($order_comments) ? $order_comments : ''));
                    $quote->setPrescriptionValues((isset($prescription_values) ? $prescription_values : ''));
                    $quote->save();
                    $this->getOnepage()->initCheckout();
                    $resultPage = $this->resultPageFactory->create();
                    $resultPage->getConfig()->getTitle()->set(__('Checkout'));
                    return $resultPage;
                }
            }
        }
        if (!$quote->hasItems() || $quote->getHasError() || !$quote->validateMinimumAmount()) {
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }
        if (!$this->_customerSession->isLoggedIn() && !$checkoutHelper->isAllowedGuestCheckout($quote)) {
            $this->messageManager->addError(__('Guest checkout is disabled.'));
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }
        // generate session ID only if connection is unsecure according to issues in session_regenerate_id function.
        // @see http://php.net/manual/en/function.session-regenerate-id.php
        if (!$this->isSecureRequest()) {
            $this->_customerSession->regenerateId();
        }
        $this->_objectManager->get(\Magento\Checkout\Model\Session::class)->setCartWasUpdated(false);
        $this->getOnepage()->initCheckout();
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Checkout'));
        return $resultPage;
    }

    /**
     * Checks if current request uses SSL and referer also is secure.
     *
     * @return bool
     */
    private function isSecureRequest(): bool {
        $request = $this->getRequest();

        $referrer = $request->getHeader('referer');
        $secure = false;

        if ($referrer) {
            $scheme = parse_url($referrer, PHP_URL_SCHEME);
            $secure = $scheme === 'https';
        }

        return $secure && $request->isSecure();
    }

}
