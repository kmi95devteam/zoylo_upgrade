<?php

/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace I95Dev\UploadPrescription\Controller\Checkout;

use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Session;
use Magento\Quote\Model\Quote;

class Removeall extends \Magento\Framework\App\Action\Action {
	

	protected $checkoutsession;
	protected $quote;

    public function __construct(
		Context $context,
        Session $checkoutsession,
        Quote $quote
    )
    {
		parent::__construct($context);
        $this->checkoutsession = $checkoutsession;       
        $this->quote = $quote;   			
    }
	
    /**
     * Delete shopping cart item action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute() {

        $quoteId = $this->checkoutsession->getQuote()->getId();
        $quoteModel = $this->quote;
        $quoteItem = $quoteModel->load($quoteId);
        $quoteItem->delete();
		$resultRedirect = $this->resultRedirectFactory->create();
		$resultRedirect->setPath('uploadprescription/index/index');
		return $resultRedirect;

    }

}
