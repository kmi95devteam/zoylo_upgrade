<?php

namespace I95Dev\UploadPrescription\Controller\Index;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Customer;

class Customers extends \Magento\Framework\App\Action\Action
{
    protected $customerSession;

    protected $customers;

    public function __construct(
        Context $context,
        Session $customerSession,
        Customer $customers
    )
    {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->customers = $customers;       
    }

    public function execute()
    {   
        if ($this->customerSession->isLoggedIn()) {

            //Get customer by customerID.
            $customerId = $this->customerSession->getCustomerData()->getId();
            $customer = $this->customers->load($customerId);
            
			$this->_view->loadLayout();
			$this->_view->getLayout()->initMessages();
			// $this->_view->getLayout()->getBlock('page.main.title')->setPageTitle('Test');
			$this->_view->renderLayout(); 
						

        }
        else{

            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('customer/account/login/');
            return $resultRedirect;        
        } 
    }
}   