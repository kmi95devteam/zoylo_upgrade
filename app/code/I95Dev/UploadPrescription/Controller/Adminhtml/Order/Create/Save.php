<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace I95Dev\UploadPrescription\Controller\Adminhtml\Order\Create;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\PaymentException;

class Save extends \Magento\Sales\Controller\Adminhtml\Order\Create
{ 	 
    /**
     * Saving quote and create order
     *
     * @return \Magento\Framework\Controller\ResultInterface
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {
        $path = 'sales/*/';
        $pathParams = [];
		
        try {
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			
			if (isset($_FILES["pic"]["name"]) && $_FILES["pic"]["name"] != "") {
					
				// Count # of uploaded files in array
				$total1 = $_FILES["pic"]["name"][0];
				 $total = count($_FILES['pic']['name']);
				 if($total1 != ''){
				 if($total > 0){
			
				// Loop through each file
					for( $i=0 ; $i < $total ; $i++ ) {
						if (isset($_FILES["pic"]["name"]) && $_FILES["pic"]["name"] != "") {
							// move file to folder
							$customer_id = $this->_getSession()->getCustomerId();
							$fileName = $_FILES["pic"]["name"][$i];
							$fileName = str_replace(' ', '-', $fileName); 
							
							$fileId = "pic[$i]";	
							if (isset($customer_id)&& ($customer_id != 0)) {
								$baseTmpPath = "prescriptions/$customer_id";
							} else {
								$baseTmpPath = "prescriptions";
							}
							$uploader = $objectManager->create('\Magento\MediaStorage\Model\File\UploaderFactory')->create(['fileId' => $fileId]);
							$uploader->setAllowCreateFolders(true);
							$uploader->setAllowRenameFiles(true);
							$uploader->setFilesDispersion(false);
							// $uploader->getFileExtension();
							$result = $uploader->save($objectManager->get('\Magento\Framework\Filesystem')->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath($baseTmpPath),$fileName);
							 $prescription_id[] = $this->saveFileToDb($customer_id, $fileName);
						
						}
					}
					$prescription_ids = implode(',',  $prescription_id);
					$quoteId = $this->_getSession()->getQuote()->getEntityId();
					$this->_getOrderCreateModel()->getQuote()->setPrescriptionValues($prescription_ids);
					
				}
			} }
			if (isset($_POST['currentuser'])){
				$currentuser = $_POST['currentuser'];
				$this->_getOrderCreateModel()->getQuote()->setOrderBy($currentuser);
			}
            // check if the creation of a new customer is allowed
            if (!$this->_authorization->isAllowed('Magento_Customer::manage')
                && !$this->_getSession()->getCustomerId()
                && !$this->_getSession()->getQuote()->getCustomerIsGuest()
            ) {
                return $this->resultForwardFactory->create()->forward('denied');
            }
			
            $this->_getOrderCreateModel()->getQuote()->setCustomerId($this->_getSession()->getCustomerId());
            $this->_processActionData('save');
            $paymentData = $this->getRequest()->getPost('payment');
            if ($paymentData) {
                $paymentData['checks'] = [
                    \Magento\Payment\Model\Method\AbstractMethod::CHECK_USE_INTERNAL,
                    \Magento\Payment\Model\Method\AbstractMethod::CHECK_USE_FOR_COUNTRY,
                    \Magento\Payment\Model\Method\AbstractMethod::CHECK_USE_FOR_CURRENCY,
                    \Magento\Payment\Model\Method\AbstractMethod::CHECK_ORDER_TOTAL_MIN_MAX,
                    \Magento\Payment\Model\Method\AbstractMethod::CHECK_ZERO_TOTAL,
                ];
                $this->_getOrderCreateModel()->setPaymentData($paymentData);
                $this->_getOrderCreateModel()->getQuote()->getPayment()->addData($paymentData);
            }

            $order = $this->_getOrderCreateModel()
                ->setIsValidate(true)
                ->importPostData($this->getRequest()->getPost('order'))
                ->createOrder();

            $this->_getSession()->clearStorage();
            $this->messageManager->addSuccessMessage(__('You created the order.'));
            if ($this->_authorization->isAllowed('Magento_Sales::actions_view')) {
                $pathParams = ['order_id' => $order->getId()];
                $path = 'sales/order/view';
            } else {
                $path = 'sales/order/index';
            }
        } catch (PaymentException $e) {
            $this->_getOrderCreateModel()->saveQuote();
            $message = $e->getMessage();
            if (!empty($message)) {
                $this->messageManager->addErrorMessage($message);
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            // customer can be created before place order flow is completed and should be stored in current session
            $this->_getSession()->setCustomerId((int)$this->_getSession()->getQuote()->getCustomerId());
            $message = $e->getMessage();
            if (!empty($message)) {
                $this->messageManager->addErrorMessage($message);
            }
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Order saving error: %1', $e->getMessage()));
        }
        /* Updating order status by Ajesh 3rd Sep 2019 Starts */
        $orderId = $order->getId();
        if($orderId!=''){
        	$orderStatus = $order->getStatus();
        	$this->updateHistoryStatus($orderId,$orderStatus);
        }
        /* Updating order status by Ajesh 3rd Sep 2019 Ends */
        return $this->resultRedirectFactory->create()->setPath($path, $pathParams);
    }
	
	public function saveFileToDb($customerId, $fileName){
		try{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$mediaUrl = $objectManager->get('Magento\Store\Model\StoreManagerInterface')
                    ->getStore()
                    ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
			$baseTmpPath = "prescriptions/$customerId/";
			$filepullpath = $mediaUrl.$baseTmpPath.$fileName;
			$prescriptionmodel = $objectManager->create('\I95Dev\UploadPrescription\Model\Prescription');
			$prescriptionmodel->setCustomerId($customerId);
			$prescriptionmodel->setPrescriptionName($fileName);
			$prescriptionmodel->setLabel($fileName);
			$prescriptionmodel->setValue($filepullpath);
			$prescriptionmodel->setCreatedDate(date('Y-m-d H:i:s', time()));
			$prescriptionmodel->save();
			$prescription_id = $prescriptionmodel->getPrescriptionId();
				
		} catch(\Exception $e) {
			die($e->getMessage());
		}
		return $prescription_id;		
	}
        
        /*
	* @Params OrderId and OrderStatus
	* Update the status column in sales_order_status_history table using entity_id
	*/
	public function updateHistoryStatus($orderId,$orderStatus){
		try {
			if($orderId!=''){
	            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	            $orderHistory = $objectManager->create('\Magento\Sales\Model\ResourceModel\Order\Status\History\Collection');
	            $orderHistory->addFieldToFilter('parent_id', array('eq' => $orderId));
	            $orderSize = $orderHistory->getSize();
	            $finalStatusHistoryId='';
	            if($orderSize>0){
	                $orderHistoryData = $orderHistory->getData();
	                foreach($orderHistoryData as $orderHistoryValue){
	                    $finalStatusHistoryId = $orderHistoryValue['entity_id'];
	                }
	                if($finalStatusHistoryId!=''){
	                    $orderHistoryObj = $objectManager->create('\Magento\Sales\Model\Order\Status\History')->load($finalStatusHistoryId);
	                    $orderHistoryObj->setStatus($orderStatus);  
	                    $orderHistoryObj->save();
	                }
	            }
	        }
    	} catch(\Exception $e) {
			die($e->getMessage());
		}
	} //end

}
