<?php
namespace I95Dev\Dailydeals\Block\Product;
class Dealsofbanners extends \Magento\Framework\View\Element\Template
{    
    protected $_categoryFactory;
	//protected $productRepository; 
    protected $_productloader;  	
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,        
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
		//ProductRepositoryInterface $productRepository,
		\Magento\Catalog\Model\ProductFactory $_productloader,
        array $data = []
    )
    {    
        $this->_categoryFactory = $categoryFactory;
		//$this->productRepository = $productRepository;
		$this->_productloader = $_productloader;
        parent::__construct($context, $data);
    }
    
	
	  public function isCountdownEnabled2($proId)
    {
		$countEnabled="";
		$product = $this->_productloader->create()->load($proId);
		$countEnabled = $product->getData('countdown_enabled');
		return $countEnabled;
    }
	
	public function getCountdownEndDate2($proId){
		$todate="";
		$product = $this->_productloader->create()->load($proId);
		$todate  =  $product->getSpecialToDate();
        return $todate;
    }
	 public function getTitle()
    {
       return $this->_scopeConfig->getValue('countdown/general/title');
    }

    public function getCountdownStartDate2($proId){
		    $fromdate="";
		    $product = $this->_productloader->create()->load($proId);
			$fromdate =  $product->getSpecialFromDate();
	        return $fromdate;
    }
	
	public function getPriceCountDown2($proId){
		
		$fromdate=$todate="";
        if($this->_scopeConfig->getValue('countdown/general/enabled')){
            $currentDate =  date('d-m-Y');
			
			$product = $this->_productloader->create()->load($proId);
			$todate      =  $product->getSpecialToDate();
            $fromdate    =  $product->getSpecialFromDate();
			
			if($product->getSpecialPrice() != 0 || $product->getSpecialPrice()) {
                if($product->getSpecialToDate() != null) {
                    if(strtotime($todate) >= strtotime($currentDate) && strtotime($fromdate) <= strtotime($currentDate)){
                        return true;
                    }   
                }
            }
        }
        return false;
    }
	
	
	
    public function getCategory($categoryId) 
    {
        $category = $this->_categoryFactory->create();
        $category->load($categoryId);
        return $category;
    }
    
    public function getCategoryProducts($categoryId) 
    {
        $products = $this->getCategory($categoryId)->getProductCollection();
        $products->addAttributeToSelect('*');
        return $products;
    }
	
public function getCatogeryId(){
		$category_id = $this->getData("category_id");
		return $category_id;
	}
	  
   

    

    
	
	
}
?>