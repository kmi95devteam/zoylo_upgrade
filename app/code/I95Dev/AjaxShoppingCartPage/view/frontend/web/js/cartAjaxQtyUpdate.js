define([
    'jquery',
    'Magento_Checkout/js/action/get-totals',
    'Magento_Customer/js/customer-data',
	'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-rate-processor/new-address',
    'Magento_Checkout/js/model/shipping-rate-processor/customer-address',
    'Magento_Checkout/js/model/shipping-rate-registry'
	
	
], function ($, getTotalsAction, customerData, quote, defaultProcessor, customerAddressProcessor, rateRegistry) {

    $(document).ready(function(){
        $(document).on('change', 'input[name$="[qty]"]', function(){
			if($('input[name$="[qty]"]').val() != '0'){
            
            var form = $('form#form-validate');
            $.ajax({
                url: form.attr('action'),
                data: form.serialize(),
                showLoader: true,
                success: function (res) {
					window.location.reload();
                    var parsedResponse = $.parseHTML(res);
                    var result = $(parsedResponse).find("#form-validate");
                    var sections = ['cart'];

                    $("#form-validate").replaceWith(result);

                    // The mini cart reloading
                    customerData.reload(sections, true);

                    // The totals summary block reloading
                    var deferred = $.Deferred();
                    getTotalsAction([], deferred);
					
					//reload shipping-rate-registry
					  var processors = [];

       rateRegistry.set(quote.shippingAddress().getCacheKey(), null);

       processors.default =  defaultProcessor;
       processors['customer-address'] = customerAddressProcessor;

       var type = quote.shippingAddress().getType();

       if (processors[type]) {
          processors[type].getRates(quote.shippingAddress());
       } else {
          processors.default.getRates(quote.shippingAddress());
       }
					
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
		}
        });
    });
});