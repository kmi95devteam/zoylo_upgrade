<?php
/**
 * MageVision Blog10
 *
 * @category     MageVision
 * @package      MageVision_Blog10
 * @author       MageVision Team
 * @copyright    Copyright (c) 2016 MageVision (https://www.magevision.com)
 * @license      http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace I95Dev\AjaxShoppingCartPage\Plugin\Checkout\CustomerData;
use Magento\Quote\Model\Quote\Item;

class DefaultItem
{
    public function aroundGetItemData($subject, \Closure $proceed, Item $item)
    {
        $data = $proceed($item);
        $product = $item->getProduct();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of Object Manager
        $priceHelper = $objectManager->create('Magento\Framework\Pricing\Helper\Data'); // Instance of Pricing Helper
        $price = $priceHelper->currency($item->getRowTotal(),true,false);
        
        //echo "<pre>"; print_r($product);
		

        $atts = [
		   	"display_pack_size" => $product->getData('display_pack_size'),
		   	"sku_display_name" => $product->getData('sku_display_name'),
            "prescription_action" => $product->getAttributeText('prescription_required'),
             "row_total" => $price
        ];

        return array_merge($data, $atts);
    }
}
