<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace I95Dev\AjaxShoppingCartPage\Controller\Cart;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CouponPost extends \Magento\Checkout\Controller\Cart\CouponPost 
{
    /**
     * Sales quote repository
     *
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * Coupon factory
     *
     * @var \Magento\SalesRule\Model\CouponFactory
     */
    protected $couponFactory;
	
	/*Custom code*/
	protected $resultJsonFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\SalesRule\Model\CouponFactory $couponFactory
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\SalesRule\Model\CouponFactory $couponFactory,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
		\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct(
            $context,
            $scopeConfig,
            $checkoutSession,
            $storeManager,
            $formKeyValidator,
            $cart,
			$couponFactory,
			$quoteRepository,
			$resultJsonFactory			
        );
        $this->couponFactory = $couponFactory;
        $this->quoteRepository = $quoteRepository;
		$this->resultJsonFactory = $resultJsonFactory;
    }

    /**
     * Initialize coupon
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
		$couponMessageSuccess = $couponMessagecancel = $couponMessageInvalid  = "";		
		$resultJson = $this->resultJsonFactory->create();
        $this->getRequest()->getParam('remove');		
		
        $couponCode = $this->getRequest()->getParam('remove') == 1 ? '': trim($this->getRequest()->getParam('coupon_code'));

        $cartQuote = $this->cart->getQuote();
        $oldCouponCode = $cartQuote->getCouponCode();

        $codeLength = strlen($couponCode);
        if (!$codeLength && !strlen($oldCouponCode)) {
            return $this->_goBack();
           
        }

        try {
            $isCodeLengthValid = $codeLength && $codeLength <= \Magento\Checkout\Helper\Cart::COUPON_CODE_MAX_LENGTH;

            $itemsCount = $cartQuote->getItemsCount();
            if ($itemsCount) {
                $cartQuote->getShippingAddress()->setCollectShippingRates(true);
                $cartQuote->setCouponCode($isCodeLengthValid ? $couponCode : '')->collectTotals();
                $this->quoteRepository->save($cartQuote);
            }

            if ($codeLength) {
                $escaper = $this->_objectManager->get(\Magento\Framework\Escaper::class);
                $coupon = $this->couponFactory->create();
                $coupon->load($couponCode, 'code');
               
                if (!$itemsCount) {
                    if ($isCodeLengthValid && $coupon->getId()) {
                        $this->_checkoutSession->getQuote()->setCouponCode($couponCode)->save();
 
						$couponMessageSuccess = '<div class="messages">
						<div class="message-success success message">
							<div>You used coupon code '.$escaper->escapeHtml($couponCode).'</div></div></div>';
 
 
						//$couponMessageSuccess = 'You used coupon code '.$escaper->escapeHtml($couponCode);
						$couponSuccessMsg = $this->getCartItems();
						$resultJson->setData(['success' => 'true','coupon_message_success'=> $couponMessageSuccess,'cart_coupon_item_success'=> $couponSuccessMsg]);
                    } else {
						$couponCancelMsg = $this->getCartItems();
						//$couponMessageDelete = 'The coupon code is not valid.'.$escaper->escapeHtml($couponCode);
						$couponMessageInvalid = '<div class="messages">
						<div class="message message-error error">
							<div>Coupon code is not valid</div></div></div>';
						$resultJson->setData(['success' => 'false','coupon_message_invalid'=> $couponMessageInvalid,'cart_coupon_item_cancel'=> $couponCancelMsg]);						
                    }
                } else {
                    if ($isCodeLengthValid && $coupon->getId() && $couponCode == $cartQuote->getCouponCode()) {
						//$couponMessageSuccess = 'You used coupon code '.$escaper->escapeHtml($couponCode);
						$couponMessageSuccess = '<div class="messages">
						<div class="message-success success message">
							<div>You used coupon code '.$escaper->escapeHtml($couponCode).'</div></div></div>';
						$couponSuccessMsg = $this->getCartItems();
						$resultJson->setData(['success' => 'true','coupon_message_success'=> $couponMessageSuccess,'cart_coupon_item_success'=> $couponSuccessMsg]);
						
                    } else {            
						$couponCancelMsg = $this->getCartItems();
						//$couponMessageDelete = 'The coupon code is not valid.'.$escaper->escapeHtml($couponCode);
						$couponMessageInvalid = '<div class="messages">
						<div class="message message-error error">
							<div>Coupon code is not valid</div></div></div>';
						$resultJson->setData(['success' => 'false','coupon_message_invalid'=> $couponMessageInvalid,'cart_coupon_item_cancel'=> $couponCancelMsg]);
                    }
                }
            } else {
				$couponMessagecancel = '<div class="messages">
						<div class="message-success success message">
							<div>You canceled the coupon code</div></div></div>';
                $resultJson->setData(['success' => 'false','coupon_message_cancel'=>$couponMessagecancel,'cart_coupon_item_cancel'=> 'You canceled the coupon code']);
                ///$this->messageManager->addSuccess(__('You canceled the coupon code.'));
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addError(__('We cannot apply the coupon code.'));
            $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
        }
		return $resultJson;
        //return $this->_goBack();
    }
	
	public function getCartItems()
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$cart = $objectManager->get('\Magento\Checkout\Model\Cart');
		$itemsCollection = $cart->getQuote()->getItemsCollection();
		$itemsVisible = $cart->getQuote()->getAllVisibleItems();
		$items = $cart->getQuote()->getAllItems();
		$couponSuccessMsg = array();
		$i=0;
		foreach($items as $item) {
				$productEntityId = $item->getId();
				$couponSuccessMsg[$i]['cart_item_id']= $productEntityId;
				$i++;
		}
		return $couponSuccessMsg;
	}

	

}
