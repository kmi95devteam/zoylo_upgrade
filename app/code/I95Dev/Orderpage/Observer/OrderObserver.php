<?php

namespace I95Dev\Orderpage\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\QuoteFactory;

class OrderObserver implements ObserverInterface {

    protected $helperdata;
    protected $helperapi;
    protected $objectManager;
    /**
     * @param QuoteFactory $quoteFactory
     */
    public function __construct(
    QuoteFactory $quoteFactory, \Magecomp\Smsfree\Helper\Data $helperdata, \Magecomp\Smsfree\Helper\Apicall $helperapi
    ) {
        $this->quoteFactory = $quoteFactory;
        $this->helperdata = $helperdata;
        $this->helperapi = $helperapi;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();

    }

    public function execute(Observer $observer) {
        /** @var $orderInstance Order */
        $prescription = false;
        $order = $observer->getOrder();
        $remoteIp = $order->getRemoteIp();
        $quoteId = $order->getQuoteId();
        $quote = $this->quoteFactory->create()->load($quoteId);
        $paymentcode = $quote->getPayment()->getMethodInstance()->getCode();
        $all_quote_items = $quote->getAllItems();
        $customer = $this->objectManager->get('Magento\Customer\Model\Customer')->load($order->getCustomerId());
        foreach ($all_quote_items as $item) {
            $sku = $item->getSku();
            if ($sku == "Free") {
                $prescription = true;
            }
        }
        if ($remoteIp == "") { //admin order
            $quoteOrigin = $quote->getOrigin();
            if ($quoteOrigin != "") {
                $order->setOrigin($quoteOrigin);
            } else {
                $order->setOrigin('Digitization');
            }
        } else {
            $order->setOrigin('Website');
        }
        if ($prescription){
          
            $order->setStatus('order_placed');
            $order->setState('new');
            //return $this;
        }
        if($paymentcode == 'cashondelivery' || $paymentcode == 'free' || $paymentcode == 'paylater'){
           
            $order->setStatus('order_placed');
            $order->setState('new');
            $message = $this->helperdata->getOrderPlaceTemplateForUser();
            $sms =  sprintf($message, $order->getIncrementId());
            $this->helperapi->callApiUrl($customer->getCustomerNumber(), $sms);
        }
        else{
            $order->setStatus('pending_payment_cs');
            $order->setState('new');
        }
        $mobile = $customer->getCustomerNumber();
        $order->setCustomerMobileNumber($mobile);
        $rolesConfig = $this->objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
        $roleName =  $rolesConfig->getValue('user_assignment_role/role_user/users_rold', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $roleModel = $this->objectManager->create('Magento\Authorization\Model\Role');
        $userModel = $this->objectManager->create('Magento\User\Model\User');
        $roleModel = $roleModel->load($roleName, 'role_name');
        if ($roleModel->getId()) {
            $userIds = $roleModel->getRoleUsers();
               foreach ($userIds as $userId) {
                $user = $userModel->load($userId);
                $today = date('Y-m-d'); //date variable
                $isOnline = $user->getIsOnline();
                //$isOnline = $user->getLogdate();
                //$logDate = date("Y-m-d", strtotime($logDate)); // output 2013-08-14
                if ($isOnline == 1 ) {
                    $userArray[] = $user->getUsername();
                }
            }
            if(!empty($userArray)){
                shuffle($userArray);
                $order->setAssignedUser($userArray[0]);
            }else {
                $order->setAssignedUser('Yet to Assign');
            }
        }
        return $this;
    }

}
