<?php

namespace I95Dev\Orderpage\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

		/**
		* Custom Attribute Renderer
		*/

class ZoyloProductType extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

		/**
		* @var OptionFactory
		*/

		protected $optionFactory;

		/**
		* @param OptionFactory $optionFactory
		*/
		/**
		* Get all options
		*
		* @return array
		*/

		public function getAllOptions()

		{
			/* your Attribute options list*/

			$this->_options=[ ['label'=>'', 'value'=>''],
			['label'=>'Medicine', 'value'=>'Medicine'],
			['label'=>'OTCMedicine', 'value'=>'OTCMedicine'],
			['label'=>'OTC', 'value'=>'OTC']
			];

			return $this->_options;

		}

}