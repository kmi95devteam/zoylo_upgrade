<?php

namespace I95Dev\Orderpage\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

		/**
		* Custom Attribute Renderer
		*/

class PackType extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

		/**
		* @var OptionFactory
		*/

		protected $optionFactory;

		/**
		* @param OptionFactory $optionFactory
		*/
		/**
		* Get all options
		*
		* @return array
		*/

		public function getAllOptions()

		{
			/* your Attribute options list*/

			$this->_options=[ ['label'=>' ', 'value'=>' '],
			['label'=>'Tablets per strip', 'value'=>'Tablets_per_strip'],
			['label'=>'Capsules per Strip', 'value'=>'Capsules_per_Strip'],
			['label'=>'Tube', 'value'=>'Tube'],
			['label'=>'Bottle', 'value'=>'Bottle'],
			['label'=>'Injections per vial', 'value'=>'Vials'],
			['label'=>'Patches per pack', 'value'=>'Patches_per_pack']
			];

			return $this->_options;

		}

}