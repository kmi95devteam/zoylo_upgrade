<?php
namespace I95Dev\Orderpage\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	
	protected $_storeManager;

	protected $wishlist;
	
	protected $product;

	protected $cart;

	protected $formkey;

	protected $wishlistItem;

	protected $resourceConnection;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Session $customerSession,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Wishlist\Model\Wishlist $wishlist,
		\Magento\Catalog\Model\Product $product,
		\Magento\Checkout\Model\Cart $cart,
		\Magento\Framework\Data\Form\FormKey $formkey,
		\Magento\Wishlist\Model\Item $wishlistItem,
		\Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->customerSession = $customerSession;
		$this->_storeManager = $storeManager;
		$this->wishlist = $wishlist;
		$this->product = $product;
		$this->cart = $cart;
		$this->formkey = $formkey;
		$this->wishlistItem = $wishlistItem;
		$this->resourceConnection = $resourceConnection;
		$this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        parent::__construct($context);
    }
	
    public function moveWishlistItemsToCart($customerId){
        try {
            //$this->logger()->info('Move Customer Id:'.$customerId);
            $wishlistItemsArray = array();
            $wishlistId = "";
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $wishlistObj = $objectManager->create('\Magento\Wishlist\Model\Wishlist');
            $wishlistCollection = $wishlistObj->loadByCustomerId($customerId, true)->getItemCollection();
            $wishlistItemSize = $wishlistCollection->getSize();
            //$this->logger()->info('Move wishlist size:'.$wishlistItemSize);
            if($wishlistItemSize>0){
                foreach ($wishlistCollection as $item) {
                    //$this->logger()->info('Move current Product Item:'.$item->getProduct()->getName());
                    //$this->logger()->info('Move current Product id:'.$item->getProductId());
                    //$this->logger()->info('Move current Product qty:'.$item->getQty());
                    try {
                    $productId = $item->getProductId();
                    $product = $objectManager->create('\Magento\Catalog\Model\Product')->load($productId);
                    $cart = $objectManager->create('\Magento\Checkout\Model\Cart');  
                    $formkeyObj = $objectManager->create('\Magento\Framework\Data\Form\FormKey');
                    $params = array();      
                    $params['form_key'] = $formkeyObj->getFormKey();
                    $params['qty'] = $item->getQty();
                    $params['product'] = $item->getProductId();
                    $cart->addProduct($product, $params);
                    $cart->save();
                    $wishlistItemsArray[] = $item->getWishlistItemId();
                    $wishlistId = $item->getWishlistId();
                    //$this->logger()->info('Move wishlsit Cart items created successfully'.$item->getWishlistItemId());
                    } catch (\Exception $e) {
                        $this->logger()->info("Exception foreach inside : ".$e->getMessage());
                    }
                }
                if($wishlistItemsArray!='' && !empty($wishlistItemsArray) && count($wishlistItemsArray)>0 && $wishlistId!=''){
                	$this->deleteWishlistItemsByCustomerId($customerId,$wishlistItemsArray,$wishlistId);
                }
            }
        } catch(\Exception $e) {
            $this->logger()->info("Exception move wishlist items to cart main: ".$e->getMessage());
        }
    }


    public function deleteWishlistItemsByCustomerId($customerId,$wishlistItemsArray,$wishlistId){
        try {
            $this->logger()->info('**Customer Id:'.$customerId);
            	foreach($wishlistItemsArray as $wishlistItem){
            		$wishlsitItemId =  $wishlistItem;
            		if($wishlsitItemId!='') {
                        try {
                            $this->deleteWishlistItem($wishlsitItemId);
                        } catch (\Exception $e) {
                            $this->logger()->info("Exception Delete wishlist item id foreach inside : ".$e->getMessage());
                        }
                    }
            	}
            	if($wishlistId!=''){
                    //$this->logger()->info('**Wishlist id:'.$wishlistId);
                    $this->deleteWishlistId($wishlistId);
                }
        }catch(\Exception $e) {
            $this->logger()->info("Exception delete wishlist item out of foreach : ".$e->getMessage());
        }
    }


    public function deleteWishlistItem($wishlsitItemId){
        try {
	        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	        $connection = $resource->getConnection();
	        $tableName = "wishlist_item";
	        $wishlistItemDeleteSql = "Delete FROM " . $tableName." Where wishlist_item_id = '".$wishlsitItemId."'";
	        $connection->query($wishlistItemDeleteSql);
	        //$this->logger()->info("Deleted wishlist item id ".$wishlsitItemId." success");
        }catch(\Exception $e) {
            $this->logger()->info("Exception delete wishlist item Id mysql query : ".$e->getMessage());
        }
    }
    public function deleteWishlistId($wishlistId){
    	try {
	        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	        $connection = $resource->getConnection();
	        $tableName = "wishlist";
	        $wishlistDeleteSql = "Delete FROM " . $tableName." Where wishlist_id = '".$wishlistId."'";
	        $connection->query($wishlistDeleteSql);
	        //$this->logger()->info("Deleted wishlist id ".$wishlistId." success");
        }catch(\Exception $e) {
            $this->logger()->info("Exception delete wishlist Id mysql query : ".$e->getMessage());
        }
    }

    public function logger(){
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/wishlistItemUpdate216.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        return $logger;
    }
	
	
}