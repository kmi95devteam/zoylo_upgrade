<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace I95Dev\Orderpage\Controller\Onepage;
use Magento\Sales\Api\Data\OrderAddressInterface;
class Success extends \Magento\Checkout\Controller\Onepage {

    /**
     * Order success action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        
        $zoyloUrl = $_SERVER['HTTP_HOST'];
        
        $session = $this->getOnepage()->getCheckout();
        if (!$this->_objectManager->get(\Magento\Checkout\Model\Session\SuccessValidator::class)->isValid()) {
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }
        $session->clearQuote();
        //@todo: Refactor it to match CQRS
        $resultPage = $this->resultPageFactory->create();
        $this->_eventManager->dispatch(
                'checkout_onepage_controller_success_action', ['order_ids' => [$session->getLastOrderId()]]
        );
        $storeManager = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $url = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $order = $this->_objectManager->get('\Magento\Sales\Model\Order')->load($session->getLastOrderId());
        $payment = $order->getPayment();
        $method = $payment->getMethod();
        
        /*Code written by Ajesh*/
        $orderBillingId = $order->getBillingAddressId();
        if($orderBillingId!=''){
            $shipData = $order->getShippingAddress()->getData();
            if(isset($shipData) && $shipData!=''){
                $shipArray = array();
                $shipArray['firstname'] = $shipData['firstname'];
                $shipArray['lastname'] = $shipData['lastname'];
                $shipArray['street'] = $shipData['street'];
                $shipArray['city'] = $shipData['city'];
                $shipArray['country_id'] = $shipData['country_id'];
                $shipArray['region'] = $shipData['region'];
                $shipArray['region_id'] = $shipData['region_id'];
                $shipArray['postcode'] = $shipData['postcode'];
                $shipArray['telephone'] = $shipData['telephone'];
                $shipArray['fax'] = $shipData['fax'];
                $billAddress = $this->_objectManager->create(OrderAddressInterface::class)->load($orderBillingId);
                $billId = $billAddress->getId();
                if($billId!='' && isset($shipArray) && $shipArray!=''){
                    $billAddress->addData($shipArray);
                    try {
                        $billAddress->save();
                    } catch (\Exception $e) {
                        $this->messageManager->addException($e, __('We can\'t update the order address right now.'));
                    }
                }
            }
        }
        /* Updating order status by Ajesh 3rd Sep 2019 Starts */
        $currentOrderId = $order->getId();
        if($currentOrderId!=''){
            $orderStatus = $order->getStatus();
            $this->updateHistoryStatus($currentOrderId,$orderStatus);
        }
        /* Updating order status by Ajesh 3rd Sep 2019 Ends */
        $customerId = $order->getCustomerId();
        if($customerId!=''){
            $orderpageHelper = $this->_objectManager->get('\I95Dev\Orderpage\Helper\Data');
            $orderpageHelper->moveWishlistItemsToCart($customerId);
        }
        /*Code written ends by Ajesh*/
        if ($method == 'payonline') {
            ?>

            <script src ="<?php echo $url; ?>pub/media/crypto-js.js"></script>
            <script type="text/javascript">
                encrypt();
                var CryptoJS;
                function encrypt() {
                    const stringfyJSON = JSON.stringify({
                        appointmentId: '<?php echo $order->getIncrementId(); ?>',
                        platform: 'ecom',
                        bookingType: 'ecom',
                        promotionId: '',
                        promotionCode: '',
                    });


                    // PROCESS
                    const encryptedString = CryptoJS.enc.Utf8.parse(stringfyJSON);
                    const encrypted = CryptoJS.enc.Base64.stringify(encryptedString);
                    var url = 'https://<?php echo $zoyloUrl;?>/payment/' + encrypted;
                    window.location.href = url;
                }
            </script>
            <?php
        } else {
            
            return $resultPage;
        }
    }
    
    /*
    * @Params OrderId and OrderStatus
    * Update the status column in sales_order_status_history table using entity_id
    */
    public function updateHistoryStatus($orderId,$orderStatus){
        try {
            if($orderId!=''){
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $dateTimeObj = $this->_objectManager->get('\Magento\Framework\Stdlib\DateTime\DateTime');
                $currentDateTime = $dateTimeObj->gmtDate();
                $orderHistory = $objectManager->create('\Magento\Sales\Model\ResourceModel\Order\Status\History\Collection');
                $orderHistory->addFieldToFilter('parent_id', array('eq' => $orderId));
                $orderSize = $orderHistory->getSize();
                $finalStatusHistoryId='';
                if($orderSize>0){
                    $orderHistoryData = $orderHistory->getData();
                    foreach($orderHistoryData as $orderHistoryValue){
                        $finalStatusHistoryId = $orderHistoryValue['entity_id'];
                    }
                    if($finalStatusHistoryId!=''){
                        $orderHistoryObj = $objectManager->create('\Magento\Sales\Model\Order\Status\History')->load($finalStatusHistoryId);
                        $orderHistoryObj->setStatus($orderStatus);  
                        $orderHistoryObj->save();
                    }
                }else{
                    $insertDataNoComments = array('parent_id'=>$orderId,'is_customer_notified'=>'','is_visible_on_front'=>'','comment'=>'','status'=>$orderStatus,'created_at'=>$currentDateTime,'entity_name'=>'order');
                    $orderHistoryObjInsert = $objectManager->create('\Magento\Sales\Model\Order\Status\History');
                    $orderHistoryObjInsert->setData($insertDataNoComments);
                    $orderHistoryObjInsert->save();
                }
            }
        } catch(\Exception $e) {
            die($e->getMessage());
        }
    } //end

}
