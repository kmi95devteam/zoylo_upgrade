<?php
 
namespace I95Dev\Orderpage\Setup;
 
use Magento\Eav\Setup\EavSetup; 
use Magento\Eav\Setup\EavSetupFactory /* For Attribute create  */;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
 
/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;
    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory; 
        /* assign object to class global variable for use in other class methods */
    }
 
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        if (version_compare($context->getVersion(), '2.0.0', '=')) {         
        /**
         * Add attributes to the eav/attribute
         */		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'prescription_required');
		    
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'prescription_required', 
					[					 
					'group' => 'Zoylo Custom Attributes',					 
					'type' => 'varchar',					 
					'backend' => '',					 
					'frontend' => '',					 
					'label' => 'Prescription Required',					 
					'input' => 'select',					 
					'class' => '',
					'source' => 'I95Dev\Orderpage\Model\Config\Source\Options',
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable', 
			]	
		);
		
		 /**
         * Add attributes to the eav/attribute
         */		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'salt');
		    
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'salt', 
					[					 
					'group' => 'Zoylo Custom Attributes',					 
					'type' => 'varchar',					 
					'backend' => '',					 
					'frontend' => '',					 
					'label' => 'Salt Name',					 
					'input' => 'multiselect',					 
					'class' => '',	
					'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
					'source' => 'I95Dev\Customproductattributes\Model\Config\Source\Options',
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable', 
			]	
		);
		
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'pack_size');
			$eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY,'pack_size', [
                        'type' => 'varchar',
                        'label' => 'Pack Size',
                        'input' => 'text',
						'required' => false,
                        'sort_order' => 110,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'group' => 'Zoylo Custom Attributes',
						"default" => "",
						"class"    => "",
						"note"       => "",
						'searchable' => false,					
						'filterable' => false,					 
						'comparable' => false,					 
						'visible_on_front' => false,					 
						'used_in_product_listing' => true,	
						'is_used_in_grid' => true,
						'is_visible_in_grid' => true,
						'is_filterable_in_grid' => true,
						'is_searchable_in_grid' => true,					
						'unique' => false,
						'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
			]
			);
					
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'pack_type');

			$eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY,'pack_type', [
                        'type' => 'varchar',
                        'label' => 'Pack Type',
                        'input' => 'select',
                        'source' => 'I95Dev\Orderpage\Model\Config\Source\PackType',
						'required' => false,
                        'sort_order' => 120,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'group' => 'Zoylo Custom Attributes',
						"default" => "",
						"class"    => "",
						"note"       => "",
						'searchable' => false,					
						'filterable' => false,					 
						'comparable' => false,					 
						'visible_on_front' => false,					 
						'used_in_product_listing' => true,	
						'is_used_in_grid' => true,
						'is_visible_in_grid' => true,
						'is_filterable_in_grid' => true,
						'is_searchable_in_grid' => true,					
						'unique' => false,
						'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
			]
			);

		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'prescription_action');

			$eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY,'prescription_action', [
                        'type' => 'varchar',
                        'label' => 'Prescription Action',
                        'input' => 'select',
                        'source' => 'I95Dev\Orderpage\Model\Config\Source\PrescriptionAction',
						'required' => false,
                        'sort_order' => 130,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'group' => 'Zoylo Custom Attributes',
						"default" => "",
						"class"    => "",
						"note"       => "",
						'searchable' => false,					
						'filterable' => false,					 
						'comparable' => false,					 
						'visible_on_front' => false,					 
						'used_in_product_listing' => true,	
						'is_used_in_grid' => true,
						'is_visible_in_grid' => true,
						'is_filterable_in_grid' => true,
						'is_searchable_in_grid' => true,					
						'unique' => false,
						'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
			]
			);
					
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'schedule_type');
	
			$eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY,'schedule_type', [
                        'type' => 'varchar',
                        'label' => 'Schedule Type',
                        'input' => 'select',
                        'source' => 'I95Dev\Orderpage\Model\Config\Source\ScheduleType',
						'required' => false,
                        'sort_order' => 140,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'group' => 'Zoylo Custom Attributes',
						"default" => "",
						"class"    => "",
						"note"       => "",
						'searchable' => false,					
						'filterable' => false,					 
						'comparable' => false,					 
						'visible_on_front' => false,					 
						'used_in_product_listing' => true,	
						'is_used_in_grid' => true,
						'is_visible_in_grid' => true,
						'is_filterable_in_grid' => true,
						'is_searchable_in_grid' => true,					
						'unique' => false,
						'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
			]
			);
					
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'schedule_category');

			$eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY,'schedule_category', [
                        'type' => 'varchar',
                        'label' => 'Schedule Category',
                        'input' => 'select',
                        'source' => 'I95Dev\Orderpage\Model\Config\Source\ScheduleCategory',
						'required' => false,
                        'sort_order' => 150,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'group' => 'Zoylo Custom Attributes',
						"default" => "",
						"class"    => "",
						"note"       => "",
						'searchable' => false,					
						'filterable' => false,					 
						'comparable' => false,					 
						'visible_on_front' => false,					 
						'used_in_product_listing' => true,	
						'is_used_in_grid' => true,
						'is_visible_in_grid' => true,
						'is_filterable_in_grid' => true,
						'is_searchable_in_grid' => true,					
						'unique' => false,
						'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
			]
			);
					

		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'primary_symptoms');
		
			$eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY,'primary_symptoms', [
                        'type' => 'varchar',
                        'label' => 'Primary Symptoms',
                        'input' => 'text',
						'required' => false,
                        'sort_order' => 160,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'group' => 'Zoylo Custom Attributes',
						"default" => "",
						"class"    => "",
						"note"       => "",
						'searchable' => false,					
						'filterable' => false,					 
						'comparable' => false,					 
						'visible_on_front' => false,					 
						'used_in_product_listing' => true,	
						'is_used_in_grid' => true,
						'is_visible_in_grid' => true,
						'is_filterable_in_grid' => true,
						'is_searchable_in_grid' => true,					
						'unique' => false,
						'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
			]
			);
					
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'chronic_flag');

			$eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY,'chronic_flag', [
                        'type' => 'varchar',
                        'label' => 'Chronic Flag',
                        'input' => 'select',
                        'source' => 'I95Dev\Orderpage\Model\Config\Source\ChronicFlag',
						'required' => false,
                        'sort_order' => 170,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'group' => 'Zoylo Custom Attributes',
						"default" => "",
						"class"    => "",
						"note"       => "",
						'searchable' => false,					
						'filterable' => false,					 
						'comparable' => false,					 
						'visible_on_front' => false,					 
						'used_in_product_listing' => true,	
						'is_used_in_grid' => true,
						'is_visible_in_grid' => true,
						'is_filterable_in_grid' => true,
						'is_searchable_in_grid' => true,					
						'unique' => false,
						'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
			]
			);
					

		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'product_format');

			$eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY,'product_format', [
                        'type' => 'varchar',
                        'label' => 'Product Format',
                        'input' => 'select',
                       /* 'source' => 'I95Dev\Orderpage\Model\Config\Source\ProductFormat',*/
						'required' => false,
                        'sort_order' => 180,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'group' => 'Zoylo Custom Attributes',
						"default" => "",
						"class"    => "",
						"note"       => "",
						'searchable' => false,					
						'filterable' => false,					 
						'comparable' => false,					 
						'visible_on_front' => false,					 
						'used_in_product_listing' => true,	
						'is_used_in_grid' => true,
						'is_visible_in_grid' => true,
						'is_filterable_in_grid' => true,
						'is_searchable_in_grid' => true,					
						'unique' => false,
						'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
			]
			);
					

		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'zoylo_product_type');

			$eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY,'zoylo_product_type', [
                        'type' => 'varchar',
                        'label' => 'Product Type',
                        'input' => 'select',
                        'source' => 'I95Dev\Orderpage\Model\Config\Source\ZoyloProductType',
						'required' => false,
                        'sort_order' => 190,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'group' => 'Zoylo Custom Attributes',
						"default" => "",
						"class"    => "",
						"note"       => "",
						'searchable' => false,					
						'filterable' => false,					 
						'comparable' => false,					 
						'visible_on_front' => false,					 
						'used_in_product_listing' => true,	
						'is_used_in_grid' => true,
						'is_visible_in_grid' => true,
						'is_filterable_in_grid' => true,
						'is_searchable_in_grid' => true,					
						'unique' => false,
						'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
			]
			);
					


		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'how_to_use');

			$eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY,'how_to_use', [
                        'type' => 'text',
                        'label' => 'How to Use',
                        'input' => 'textarea',
						'required' => false,
                        'sort_order' => 200,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'wysiwyg_enabled' => true,
                        'is_html_allowed_on_front' => false,
                        'group' => 'Medical Overview',
						"default" => "",
						"class"    => "",
						"note"       => "",
						'searchable' => false,					
						'filterable' => false,					 
						'comparable' => false,					 
						'visible_on_front' => false,					 
						'used_in_product_listing' => true,	
						'is_used_in_grid' => true,
						'is_visible_in_grid' => true,
						'is_filterable_in_grid' => true,
						'is_searchable_in_grid' => true,					
						'unique' => false,
						'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
			]
			);
					

		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'expert_advice');
		
			$eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY,'expert_advice', [
                        'type' => 'text',
                        'label' => 'Expert Advice',
                        'input' => 'textarea',
						'required' => false,
                        'sort_order' => 210,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'wysiwyg_enabled' => true,
                        'is_html_allowed_on_front' => false,
                        'group' => 'Medical Overview',
						"default" => "",
						"class"    => "",
						"note"       => "",
						'searchable' => false,					
						'filterable' => false,					 
						'comparable' => false,					 
						'visible_on_front' => false,					 
						'used_in_product_listing' => true,	
						'is_used_in_grid' => true,
						'is_visible_in_grid' => true,
						'is_filterable_in_grid' => true,
						'is_searchable_in_grid' => true,					
						'unique' => false,
						'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
			]
			);
					
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'dosage');
		
			$eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY,'dosage', [
                        'type' => 'text',
                        'label' => 'Dosage',
                        'input' => 'textarea',
						'required' => false,
                        'sort_order' => 220,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'wysiwyg_enabled' => true,
                        'is_html_allowed_on_front' => false,
                        'group' => 'Medical Overview',
						"default" => "",
						"class"    => "",
						"note"       => "",
						'searchable' => false,					
						'filterable' => false,					 
						'comparable' => false,					 
						'visible_on_front' => false,					 
						'used_in_product_listing' => true,	
						'is_used_in_grid' => true,
						'is_visible_in_grid' => true,
						'is_filterable_in_grid' => true,
						'is_searchable_in_grid' => true,					
						'unique' => false,
						'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
			]
			);

		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'zoylo_faq');

			$eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY,'zoylo_faq', [
                        'type' => 'text',
                        'label' => 'FAQs',
                        'input' => 'textarea',
						'required' => false,
                        'sort_order' => 230,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'wysiwyg_enabled' => true,
                        'is_html_allowed_on_front' => false,
                        'group' => 'Medical Overview',
						"default" => "",
						"class"    => "",
						"note"       => "",
						'searchable' => false,					
						'filterable' => false,					 
						'comparable' => false,					 
						'visible_on_front' => false,					 
						'used_in_product_listing' => true,	
						'is_used_in_grid' => true,
						'is_visible_in_grid' => true,
						'is_filterable_in_grid' => true,
						'is_searchable_in_grid' => true,					
						'unique' => false,
						'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
			]
			);
					
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'discount_percentage');
			$eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY,'discount_percentage', [
                        'type' => 'varchar',
                        'label' => 'Discount Percentage',
                        'input' => 'text',
						'required' => false,
                        'sort_order' => 10,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'General Information',
						"default" => "",
						"class"    => "",
						"note"       => "",
						'searchable' => false,					
						'filterable' => false,					 
						'comparable' => false,					 
						'visible_on_front' => false,					 
						'used_in_product_listing' => true,	
						'is_used_in_grid' => true,
						'is_visible_in_grid' => true,
						'is_filterable_in_grid' => true,
						'is_searchable_in_grid' => true,					
						'unique' => false,
						'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
			]
			);
		}
    }
}