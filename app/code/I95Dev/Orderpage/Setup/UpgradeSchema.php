<?php

namespace I95Dev\Orderpage\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $quote = 'quote';
        $orderTable = 'sales_order';

        if (version_compare($context->getVersion(), '2.0.0', '=')) {
			$setup->getConnection()
				->addColumn(
					$setup->getTable($quote),
					'age',
					[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'Age'
					]
				);
			
            $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderTable),
                'age',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Age'
                ]
            );

            

			$setup->getConnection()
			->addColumn(
				$setup->getTable($quote),
				'patient_name',
				[
					'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					'length' => 255,
					'comment' =>'Patient Name'
				]
			);
			
            $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderTable),
                'patient_name',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Patient Name'
                ]
            );
			
            

			$setup->getConnection()
            ->addColumn(
                $setup->getTable($quote),
                'doctor_name',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Doctor Name'
                ]
            );
			
            $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderTable),
                'doctor_name',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Doctor Name'
                ]
            );

            
			
			$setup->getConnection()
            ->addColumn(
                $setup->getTable($quote),
                'order_by',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Order By'
                ]
            );
			
            $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderTable),
                'order_by',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Order By'
                ]
            );
			
            

			$setup->getConnection()
            ->addColumn(
                $setup->getTable($quote),
                'source',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Source'
                ]
            );
			
            $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderTable),
                'source',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Source'
                ]
            );
			
            $orderGridTable = 'sales_order_grid';

            $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderGridTable),
                'age',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Age'
                ]
            );

            $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderGridTable),
                'patient_name',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Patient Name'
                ]
            );

            $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderGridTable),
                'doctor_name',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Doctor Name'
                ]
            );

            $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderGridTable),
                'order_by',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Order By'
                ]
            );

            $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderGridTable),
                'source',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Source'
                ]
            );
        }
        if (version_compare($context->getVersion(), '2.0.16', '=')) {
            $orderGridTable = 'sales_order_grid';

            $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderGridTable),
                'customer_mobile_number',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Customer Mobile Number'
                ]
            );

            $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderTable),
                'customer_mobile_number',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Customer Mobile Number'
                ]
            );
        }

        $setup->endSetup();
    }
}