<?php
/**
 * Copyright © 2015 I95Dev. All rights reserved.
 */
namespace I95Dev\Symptoms\Model\ResourceModel;

/**
 * Symptoms resource
 */
class Symptoms extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('i95dev_symptoms', 'id');
    }

  
}
