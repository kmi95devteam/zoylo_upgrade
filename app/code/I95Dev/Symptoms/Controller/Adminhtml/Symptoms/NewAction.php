<?php
namespace I95Dev\Symptoms\Controller\Adminhtml\Symptoms;
use Magento\Backend\App\Action;
class NewAction extends \Magento\Backend\App\Action
{
     public function execute()
    {
		$this->_forward('edit');
    }
}
