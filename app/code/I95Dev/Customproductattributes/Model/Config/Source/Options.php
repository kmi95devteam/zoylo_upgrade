<?php
namespace I95Dev\Customproductattributes\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;
use I95Dev\Salts\Model\ResourceModel\Salt\CollectionFactory;
/**

* Custom Attribute Renderer

*/

class Options extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

	/**
    * @var OptionFactory
    */
    public $optionFactory;

    /**
     * @var saltsCollectionFactory
     */
    public $saltsCollectionFactory;

    public function __construct(
        OptionFactory $optionFactory,
        CollectionFactory $saltsCollectionFactory
    ) {
        $this->optionFactory = $optionFactory;
        $this->saltsCollectionFactory = $saltsCollectionFactory;
    }

	/**
	* Get all options
	*
	* @return array
	*/
	public function getAllOptions()

	{

        $this->_options = [];
        $datas = $this->getSaltsCollection();
        $this->_options[] = [
                'label' => '-- Please Select --',
                'value' => '',
            ];
        foreach ($datas as $data) {
            $this->_options[] = [
                'label' => __($data->getName()),
                'value' => $data->getSaltId(),
            ];
        }

        return $this->_options;

	}
	
	
    public function getSaltsCollection()
    {
        /** @var $collection \I95Dev\Salts\Model\ResourceModel\Salt\CollectionFactory */
        $collection = $this->saltsCollectionFactory->create();
        $collection->getSelect()->group('name');
        return $collection;
    }
}

