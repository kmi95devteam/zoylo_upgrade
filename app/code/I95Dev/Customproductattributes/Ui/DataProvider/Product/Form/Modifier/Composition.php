<?php

namespace I95Dev\Customproductattributes\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Catalog\Controller\Adminhtml\Product\Initialization\StockDataFilter;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;

use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Api\StockConfigurationInterface;

use Magento\Ui\Component\Container;
use Magento\Ui\Component\Form\Element\DataType\Number;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\Textarea;
use Magento\Ui\Component\Form\Element\Checkbox;
use Magento\Ui\Component\Form\Element\Select;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Modal;

/**
 * Data provider for attraction highlights field
 */
class Composition extends AbstractModifier
{
    const ATTRACTION_HIGHLIGHTS_FIELD = 'composition';

    /**
     * @var LocatorInterface
     */
    private $locator;

    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * @var array
     */
    private $meta = [];

    public $salts;
    /**
     * @var string
     */
    protected $scopeName;   
    
    	/**
    * @var OptionFactory
    */
    public $optionFactory;

    /**
     * @param LocatorInterface $locator
     * @param ArrayManager $arrayManager
     */
    public function __construct(
        LocatorInterface $locator,
        ArrayManager $arrayManager,
        \I95Dev\Salts\Model\ResourceModel\Salt\CollectionFactory $salts,
        OptionFactory $optionFactory,
        $scopeName = ''
    ) {
        $this->salts = $salts;
        $this->locator = $locator;
        $this->arrayManager = $arrayManager;
        $this->scopeName = $scopeName;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        $fieldCode = self::ATTRACTION_HIGHLIGHTS_FIELD;

        $model = $this->locator->getProduct();
        $modelId = $model->getId();

        $highlightsData = $model->getAttractionHighlights();

        if ($highlightsData) {
            $highlightsData = json_decode($highlightsData, true);
            $path = $modelId . '/' . self::DATA_SOURCE_DEFAULT . '/'. self::ATTRACTION_HIGHLIGHTS_FIELD;
            $data = $this->arrayManager->set($path, $data, $highlightsData);
        }
        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $this->meta = $meta;
        $this -> initAttractionHighlightFields();
        return $this->meta;
    }

    /**
     * Customize attraction highlights field
     *
     * @return $this
     */
    protected function initAttractionHighlightFields()
    {
        $highlightsPath = $this->arrayManager->findPath(
            self::ATTRACTION_HIGHLIGHTS_FIELD,
            $this->meta,
            null,
            'children'
        );

        if ($highlightsPath) {
            $this->meta = $this->arrayManager->merge(
                $highlightsPath,
                $this->meta,
                $this->initHighlightFieldStructure($highlightsPath)
            );
            $this->meta = $this->arrayManager->set(
                $this->arrayManager->slicePath($highlightsPath, 0, -3)
                . '/' . self::ATTRACTION_HIGHLIGHTS_FIELD,
                $this->meta,
                $this->arrayManager->get($highlightsPath, $this->meta)
            );
            $this->meta = $this->arrayManager->remove(
                $this->arrayManager->slicePath($highlightsPath, 0, -2),
                $this->meta
            );
        }

        return $this;
    }   


    /**
     * Get attraction highlights dynamic rows structure
     *
     * @param string $highlightsPath
     * @return array
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function initHighlightFieldStructure($highlightsPath)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => 'dynamicRows',
                        'label' => __('Add Composition'),
                        'renderDefaultRecord' => false,
                        'recordTemplate' => 'record',
                        'dataScope' => '',
                        'dndConfig' => [
                            'enabled' => false,
                        ],
                        'disabled' => false,
                        'sortOrder' =>
                            $this->arrayManager->get($highlightsPath . '/arguments/data/config/sortOrder', $this->meta),
                    ],
                ],
            ],
            'children' => [
                'record' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'isTemplate' => true,
                                'is_collection' => true,
                                'component' => 'Magento_Ui/js/dynamic-rows/record',
                                'dataScope' => '',
                            ],
                        ],
                    ],
                    'children' => [
                        'title' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'formElement' => Select::NAME,
                                        'componentType' => Field::NAME,
                                        'dataType' => Text::NAME,
                                        'label' => __('Salts'),
                                        'dataScope' => 'salts',
                                        'require' => '1',
                                        'options' => $this->getSaltsInformation(),
                                    ],
                                ],
                            ],
                        ],

                        'description' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'formElement' => Input::NAME,
                                        'componentType' => Field::NAME,
                                        'dataType' => Text::NAME,
                                        'label' => __('Weight'),
                                        'dataScope' => 'weight',
                                        'require' => '1',
                                    ],
                                ],
                            ],
                        ],

                        'primary' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'formElement' => Select::NAME,
                                        'componentType' => Field::NAME,
                                        'dataType' => Text::NAME,
                                        'label' => __('Primary'),
                                        'dataScope' => 'primary',
                                        'require' => '1',
                                        'options' => [
                    [
                        'label' => __('No'),
                        'value' => 0
                    ],
                    [
                        'label' => __('Yes'),
                        'value' => 1
                    ],
                ],
                                    ],
                                ],
                            ],
                        ],                                               

                        'actionDelete' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'componentType' => 'actionDelete',
                                        'dataType' => Text::NAME,
                                        'label' => '',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    
    public function getSaltsInformation(){
        
       $salts = $this->salts->create();
       $this->_options = [];
       foreach($salts as $salt){
           $this->_options[] = [
                'label' =>$salt->getName(),
                'value' => $salt->getName(),
            ];
           
       }
       return $this->_options; 
    }
}
?>