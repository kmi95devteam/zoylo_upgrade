<?php

/**
 * Copyright � 2017 BORN . All rights reserved.
 */

namespace I95Dev\Customproductattributes\Observer;

use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;

class SerializedComposition implements ObserverInterface {

    const ATTR_ATTRACTION_HIGHLIGHTS_CODE = 'composition';

    /**
     * @var  \Magento\Framework\App\RequestInterface
     */
    protected $request;
    public $salts;

    /**
     * Constructor
     */
    public function __construct(
    \Magento\Framework\App\RequestInterface $request, \I95Dev\Salts\Model\ResourceModel\Salt\CollectionFactory $salts
    ) {
        $this->salts = $salts;
        $this->request = $request;
    }

    public function execute(Observer $observer) {
        /** @var $product \Magento\Catalog\Model\Product */
        $product = $observer->getEvent()->getDataObject();
        $type = $product->getScheduleType();
        $category = $product->getScheduleCategory();

        /* custom logic for Prescrition required */
        if (isset($type) && $type != '') {

            if (isset($category) && $category != '') {

                $typeAttribute = $product->getResource()->getAttribute('schedule_type');
                if ($typeAttribute->usesSource()) {
                    $scheduleType = $typeAttribute->getSource()->getOptionText($type);
                }

                $prescriptionAttribute = $product->getResource()->getAttribute('prescription_action');
                if ($scheduleType == 'J') {
                    if ($prescriptionAttribute->usesSource()) {
                        $prescriptionValue = $prescriptionAttribute->getSource()->getOptionId('Not for Sale');
                    }
                    $product->setPrescriptionAction($prescriptionValue);
                }

                $category = $product->getScheduleCategory();
                $categoryAttribute = $product->getResource()->getAttribute('schedule_category');
                if ($categoryAttribute->usesSource()) {
                    $scheduleCategory = $categoryAttribute->getSource()->getOptionText($category);
                }
                $typeArray = array('G', 'H', 'X', 'W');
                if (in_array($scheduleType, $typeArray) && $scheduleCategory == 'Dangerous') {

                    if ($prescriptionAttribute->usesSource()) {
                        $prescriptionValue = $prescriptionAttribute->getSource()->getOptionId('Not for Sale');
                    }

                    $product->setPrescriptionAction($prescriptionValue);
                }

                if ($scheduleType == 'G') {

                    if ($scheduleCategory == 'Potentially Dangerous') {
                        if ($prescriptionAttribute->usesSource()) {
                            $prescriptionValue = $prescriptionAttribute->getSource()->getOptionId('Not for Sale');
                        }
                    }

                    if ($scheduleCategory == 'Mildly Unsafe' || $scheduleCategory == 'Safe') {

                        if ($prescriptionAttribute->usesSource()) {
                            $prescriptionValue = $prescriptionAttribute->getSource()->getOptionId('Required');
                        }
                    }
                    $product->setPrescriptionAction($prescriptionValue);
                }

                if ($scheduleType == 'H') {
                    if ($scheduleCategory == 'Mildly Unsafe' || $scheduleCategory == 'Safe' || $scheduleCategory == 'Potentially Dangerous') {
                        if ($prescriptionAttribute->usesSource()) {
                            $prescriptionValue = $prescriptionAttribute->getSource()->getOptionId('Required');
                        }
                    }
                    $product->setPrescriptionAction($prescriptionValue);
                }

                if ($scheduleType == 'X') {
                    if ($scheduleCategory == 'Mildly Unsafe' || $scheduleCategory == 'Safe' || $scheduleCategory == 'Potentially Dangerous') {
                        if ($prescriptionAttribute->usesSource()) {
                            $prescriptionValue = $prescriptionAttribute->getSource()->getOptionId('Required');
                        }
                    }
                    $product->setPrescriptionAction($prescriptionValue);
                }

                if ($scheduleType == 'W') {
                    if ($scheduleCategory == 'Mildly Unsafe' || $scheduleCategory == 'Safe' || $scheduleCategory == 'Potentially Dangerous') {
                        if ($prescriptionAttribute->usesSource()) {
                            $prescriptionValue = $prescriptionAttribute->getSource()->getOptionId('Required');
                        }
                    }
                    $product->setPrescriptionAction($prescriptionValue);
                }
            } else {
                $product->setPrescriptionAction();
            }
        } else {
            $product->setPrescriptionAction('');
        }
        $post = $this->request->getPost();
        $post = $post['product'];
        $composition = isset($post[self::ATTR_ATTRACTION_HIGHLIGHTS_CODE]) ? $post[self::ATTR_ATTRACTION_HIGHLIGHTS_CODE] : '';
        if (!empty($composition)) {
            $i = 0;
            $saltData = '';
            foreach ($composition as $data) {
                if ($data['weight'] != '') {
                    if ($i == 0) {
                        $saltData .= $data['salts'] . ' [' . $data['weight'] . ']';
                    } else {
                        $saltData .= ' + ' . $data['salts'] . ' [' . $data['weight'] . ']';
                    }
                    if ($data['primary'] == 1) {
                        $symptoms[] = $data['salts'];
                    }
                }
                $i++;
            }
            if (!empty($saltData)) {
                $product->setSaltComposition($saltData);
            }
            if (!empty($symptoms)) {
                $saltSymptom = '';
                $salts = $this->salts->create()->addFieldToFilter('name', array('in' => $symptoms));
                $i = 0;
                foreach ($salts as $salt) {

                    if ($i == 0) {
                        $saltSymptom .= $salt->getAilments();
                    } else {
                        $saltSymptom .= ',' . $salt->getAilments();
                    }
                    $i++;
                }
                $product->setPrimarySymptoms($saltSymptom);
            }
        }

        /* save discount code */
        $price = $product->getPrice();
        $specialPrice = $product->getSpecialPrice();
        if ($specialPrice != 0) {
            $discount = $this->saveDiscountcode($price, $specialPrice);
            $product->setDiscountPercentage($discount);
        }
        
        
        /*  */
      $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
      $user  = $objectManager->get('Magento\Backend\Model\Auth\Session')->getUser()->getRole()->getData();
      if($user['role_name'] == 'Catalog Executive'){
          $product->setStatus(0);
      }
    }

    /**
     * Function to remove empty array from the multi dimensional array
     *
     * @return Array
     */
    private function removeEmptyArray($attractionData, $requiredParams) {

        $requiredParams = array_combine($requiredParams, $requiredParams);
        $reqCount = count($requiredParams);

        foreach ($attractionData as $key => $values) {
            $values = array_filter($values);
            $inersectCount = count(array_intersect_key($values, $requiredParams));
            if ($reqCount != $inersectCount) {
                unset($attractionData[$key]);
            }
        }
        return $attractionData;
    }

    /**
     * calculate discount based on price and specail price 
     *
     */
    public function saveDiscountcode($price, $specialPrice) {
		
		$price = str_replace(',', '', $price);
		$specialPrice = str_replace(',', '', $specialPrice);
		$discountPercentage = 100 * $specialPrice / $price;
		$discountPercentage = (int)$discountPercentage;
        return $discountPercentage;
    }

}
