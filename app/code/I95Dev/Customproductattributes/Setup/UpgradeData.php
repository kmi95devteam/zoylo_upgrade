<?php

namespace I95Dev\Customproductattributes\Setup;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

class UpgradeData implements UpgradeDataInterface {

    private $eavSetupFactory;
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
		// Interaction with common conditions
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'kidney');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'kidney', 
					[					 
					'group' => 'Interaction with common conditions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 20,					
					'frontend' => '',					 
					'label' => 'Kidney',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'pregnancy');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'pregnancy', 
					[					 
					'group' => 'Interaction with common conditions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 20,					
					'frontend' => '',					 
					'label' => 'Pregnancy',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'high_blood_pressure');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'high_blood_pressure', 
					[					 
					'group' => 'Interaction with common conditions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 20,					
					'frontend' => '',					 
					'label' => 'High Blood Pressure',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'low_blood_pressure');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'low_blood_pressure', 
					[					 
					'group' => 'Interaction with common conditions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 20,					
					'frontend' => '',					 
					'label' => 'Low Blood Pressure',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'diabetes');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'diabetes', 
					[					 
					'group' => 'Interaction with common conditions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 20,					
					'frontend' => '',					 
					'label' => 'Diabetes',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'liver');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'liver', 
					[					 
					'group' => 'Interaction with common conditions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 20,					
					'frontend' => '',					 
					'label' => 'Liver',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'lactation');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'lactation', 
					[					 
					'group' => 'Interaction with common conditions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 20,					
					'frontend' => '',					 
					'label' => 'Lactation',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'oral_contraceptives');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'oral_contraceptives', 
					[					 
					'group' => 'Interaction with common conditions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 20,					
					'frontend' => '',					 
					'label' => 'Oral Contraceptives',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'children');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'children', 
					[					 
					'group' => 'Interaction with common conditions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 20,					
					'frontend' => '',					 
					'label' => 'Children',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'seniors');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'seniors', 
					[					 
					'group' => 'Interaction with common conditions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 20,					
					'frontend' => '',					 
					'label' => 'Seniors',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		// Precautions
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'smoking');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'smoking', 
					[					 
					'group' => 'Precautions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 30,					
					'frontend' => '',					 
					'label' => 'Smoking',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);	
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'alcohol');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'alcohol', 
					[					 
					'group' => 'Precautions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 30,					
					'frontend' => '',					 
					'label' => 'Alcohol',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'tolerance');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'tolerance', 
					[					 
					'group' => 'Precautions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 30,					
					'frontend' => '',					 
					'label' => 'Tolerance',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'vision');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'vision', 
					[					 
					'group' => 'Precautions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 30,					
					'frontend' => '',					 
					'label' => 'Vision',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'overdose');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'overdose', 
					[					 
					'group' => 'Precautions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 30,					
					'frontend' => '',					 
					'label' => 'Overdose',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'driving');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'driving', 
					[					 
					'group' => 'Precautions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 30,					
					'frontend' => '',					 
					'label' => 'Driving',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'exercise');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'exercise', 
					[					 
					'group' => 'Precautions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 30,					
					'frontend' => '',					 
					'label' => 'Exercise',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'operating_heavy_machinery');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'operating_heavy_machinery', 
					[					 
					'group' => 'Precautions',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 30,					
					'frontend' => '',					 
					'label' => 'Operating Heavy Machinery',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		// Allergy Interaction
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'antibiotics');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'antibiotics', 
					[					 
					'group' => 'Allergy Interaction',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 40,					
					'frontend' => '',					 
					'label' => 'Antibiotics (Eg:Penicilin)',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'anti_inflammatory');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'anti_inflammatory', 
					[					 
					'group' => 'Allergy Interaction',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 40,					
					'frontend' => '',					 
					'label' => 'Anti-inflammatory drugs like Aspirin',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'chemotherapy_drugs');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'chemotherapy_drugs', 
					[					 
					'group' => 'Allergy Interaction',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 40,					
					'frontend' => '',					 
					'label' => 'Chemotherapy drugs',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'anticonvulsants');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'anticonvulsants', 
					[					 
					'group' => 'Allergy Interaction',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 40,					
					'frontend' => '',					 
					'label' => 'Anticonvulsants',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
		$eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'sulfa_drugs');
				$eavSetup->addAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
					'sulfa_drugs', 
					[					 
					'group' => 'Allergy Interaction',					 
					'type' => 'varchar',					 
					'backend' => '',	
					'sort_order' => 40,					
					'frontend' => '',					 
					'label' => 'Sulfa Drugs',					 
					'input' => 'select',					 
					'class' => '',	
					'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,					 
					'visible' => true,					 
					'required' => false,					 
					'user_defined' => true,					 
					'default' => '',					 
					'searchable' => false,					
					'filterable' => false,					 
					'comparable' => false,					 
					'visible_on_front' => false,					 
					'used_in_product_listing' => true,	
					'is_used_in_grid' => true,
					'is_visible_in_grid' => true,
					'is_filterable_in_grid' => true,
					'is_searchable_in_grid' => true,					
					'unique' => false,
					'apply_to' => 'simple,configurable,virtual,bundle,downloadable',
					'option'   => ['values' => [ 0 => 'Safe',1 => 'UnSafe',2 => 'Unknown',3 => 'NA']],
			]	
		);
		
    }

}
