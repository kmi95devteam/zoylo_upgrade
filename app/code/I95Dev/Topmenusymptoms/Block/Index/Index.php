<?php

namespace I95Dev\Topmenusymptoms\Block\Index;


class Index extends \Magento\Framework\View\Element\Template
{    
    protected $_productCollectionFactory;
        
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,        
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,        
        array $data = []
    )
    {    
        $this->_productCollectionFactory = $productCollectionFactory;    
        parent::__construct($context, $data);
    }
    
    public function getProductCollection()
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->setPageSize(3); // fetching only 3 products
        return $collection;
    }
    
    	public function getProductsBySympotoms($symptomsdata, $product_id)
    {
		$productCollection = $this->_productCollectionFactory
								->create()
								->addAttributeToSelect('*')
								->addAttributeToFilter('symptoms_primary_use', $symptomsdata)
								->addAttributeToFilter('entity_id', array('neq' => $product_id))
								->load();
		return $productCollection;
    }
}