<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace I95Dev\Paytm\Model;



/**
 * Pay In Store payment method model
 */
class Paytm extends \Magento\Payment\Model\Method\AbstractMethod
{

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'paytm';

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;


  

}
