<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Seo
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     http://mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Seo\Plugin;

use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Event\Manager;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Page\Config as PageConfig;
use Magento\Framework\View\Page\Config\Renderer;
use Magento\Review\Model\ReviewFactory;
use Magento\Search\Helper\Data as SearchHelper;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\Seo\Helper\Data as HelperData;
use Magento\Catalog\Helper\Data as CatalogHelperData;
use Magento\Catalog\Helper\Image;

/**
 * Class SeoBeforeRender
 * @package Mageplaza\Seo\Plugin
 */
class SeoRender
{
    const GOOLE_SITE_VERIFICATION = 'google-site-verification';
    const MSVALIDATE_01           = 'msvalidate.01';
    const P_DOMAIN_VERIFY         = 'p:domain_verify';
    const YANDEX_VERIFICATION     = 'yandex-verification';
    const SCHEMA_ORG_URL          = 'https://schema.org/';

    /**
     * @var \Magento\Framework\View\Page\Config
     */
    protected $pageConfig;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Mageplaza\Seo\Helper\Data
     */
    protected $helperData;

    /**
     * @var \Magento\CatalogInventory\Model\Stock\StockItemRepository
     */
    protected $stockItemRepository;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Review\Model\ReviewFactory
     */
    protected $reviewFactory;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $stockState;

    /**
     * @var \Magento\Search\Helper\Data
     */
    protected $_searchHelper;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $_priceHelper;

    /**
     * @var \Magento\Framework\Event\Manager
     */
    protected $_eventManager;

    /**
     * @var \Magento\Catalog\Helper\Data
     */
    protected $catalogHelperData;

    private $layerResolver;

    /**
     * Symptoms Collection
     */
    protected $_symptomsCollection;

    protected $productCollectionFactory;

    /** @var \Magento\Catalog\Helper\Image */
    protected $imageHelper;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * SeoRender constructor.
     * @param \Magento\Framework\View\Page\Config $pageConfig
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Mageplaza\Seo\Helper\Data $helpData
     * @param \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Review\Model\ReviewFactory $reviewFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockState
     * @param \Magento\Search\Helper\Data $searchHelper
     * @param \Magento\Framework\Pricing\Helper\Data $priceHelper
     * @param \Magento\Framework\Event\Manager $eventManager
     */
    public function __construct(
        PageConfig $pageConfig,
        Http $request,
        HelperData $helpData,
        CatalogHelperData $catalogHelperData,
        StockItemRepository $stockItemRepository,
        Registry $registry,
        ReviewFactory $reviewFactory,
        StoreManagerInterface $storeManager,
        UrlInterface $urlBuilder,
        ProductFactory $productFactory,
        ManagerInterface $messageManager,
        StockRegistryInterface $stockState,
        SearchHelper $searchHelper,
        PriceHelper $priceHelper,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        \I95Dev\Symptoms\Model\Symptoms $symptomsCollection,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        Manager $eventManager
    )
    {
        $this->pageConfig          = $pageConfig;
        $this->request             = $request;
        $this->helperData          = $helpData;
        $this->catalogHelperData   = $catalogHelperData;
        $this->stockItemRepository = $stockItemRepository;
        $this->registry            = $registry;
        $this->_storeManager       = $storeManager;
        $this->reviewFactory       = $reviewFactory;
        $this->_urlBuilder         = $urlBuilder;
        $this->productFactory      = $productFactory;
        $this->messageManager      = $messageManager;
        $this->stockState          = $stockState;
        $this->_searchHelper       = $searchHelper;
        $this->_priceHelper        = $priceHelper;
        $this->_eventManager       = $eventManager;
        $this->layerResolver       = $layerResolver;
        $this->_symptomsCollection = $symptomsCollection;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->imageHelper          = $imageHelper;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param Renderer $subject
     */
    public function beforeRenderMetadata(Renderer $subject)
    {
        if ($this->helperData->isEnabled()) {
            $this->showVerifications();

            $pages = [
                'catalogsearch_result_index',
                'cms_noroute_index',
                'catalogsearch_advanced_result'
            ];
            if (in_array($this->getFullActionName(), $pages)) {
                $this->pageConfig->setMetadata('robots', 'NOINDEX,NOFOLLOW');
            }

            $metaStatus = false;
            if($this->getFullActionName() == "catalog_product_view"){

                $currentProduct = $this->getProduct();
                $productType    = $currentProduct->getAttributeText('zoylo_sku_group');
				$productType 	= strtolower($productType);
                $name           = $currentProduct->getName();
                if($productType == "medicine" || $productType == "otcmedicine"){
                    $groupId    = "rx_non_rx_medicines";
                }else{
                    $groupId    = "otc";
                    $categories = $currentProduct->getCategoryIds();/*will return category ids array*/
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    foreach($categories as $category){
                        $cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category);
                        $categoryName = $cat->getName();
                    }
                }
                
                $title       = $this->helperData->getSeoMetaConfig($groupId.'/meta_title');
                $title       = str_replace('{sku_name}', $name, $title);
                $keywords    = $this->helperData->getSeoMetaConfig($groupId.'/meta_keywords');
                $keywords    = str_replace('{sku_name}', $name, $keywords);
                if(!empty($categoryName)){
                    $keywords    = str_replace('{product_category_name}', $categoryName, $keywords);
                }
                $description = $this->helperData->getSeoMetaConfig($groupId.'/meta_description');
                $description = str_replace('{sku_name}', $name, $description);
                $metaStatus  = true;

            }elseif($this->getFullActionName() == "vesbrand_brand_view"){
                $groupId     = 'manufacturer';
                $brand       = $this->getBrand()->getName();
                $title       = $this->helperData->getSeoMetaConfig($groupId.'/meta_title');
                $title       = str_replace('{manufacturer_name}', $brand, $title);
                $keywords    = $this->helperData->getSeoMetaConfig($groupId.'/meta_keywords');
                $keywords    = str_replace('{manufacturer_name}', $brand, $keywords);
                $description = $this->helperData->getSeoMetaConfig($groupId.'/meta_description');
                $description = str_replace('{manufacturer_name}', $brand, $description);
                $metaStatus  = true;
            }elseif($this->getFullActionName() == "catalogsearch_result_index"){
                $symptomscollection = $this->_symptomsCollection->getCollection();
                $query = $this->request->getParam('q');
                $query = preg_replace('/[^A-Za-z0-9\-]/', '', $query); // Removes special chars.
                foreach ($symptomscollection as $symptom) {
                    $name = $symptom->getName();
                    if( preg_match("/\b".$query."\b/",$name) ) {
                        $groupId     = 'symptoms';
                        $title       = $this->helperData->getSeoMetaConfig($groupId.'/meta_title');
                        $title       = str_replace('{symptom_name}', $name, $title);
                        $keywords    = $this->helperData->getSeoMetaConfig($groupId.'/meta_keywords');
                        $keywords    = str_replace('{symptom_name}', $name, $keywords);
                        $description = $this->helperData->getSeoMetaConfig($groupId.'/meta_description');
                        $description = str_replace('{symptom_name}', $name, $description);
                        $metaStatus  = true;
                    }
                }
            }elseif ($this->getFullActionName() == "i95dev_salts_salt_view") {
                $groupId     = 'salt';
                $saltName    = $this->getSalt()->getName();
                $title       = $this->helperData->getSeoMetaConfig($groupId.'/meta_title');
                $title       = str_replace('{salt_name}', $saltName, $title);
                $keywords    = $this->helperData->getSeoMetaConfig($groupId.'/meta_keywords');
                $keywords    = str_replace('{salt_name}', $saltName, $keywords);
                $description = $this->helperData->getSeoMetaConfig($groupId.'/meta_description');
                $description = str_replace('{salt_name}', $saltName, $description);
                $metaStatus  = true;
            }elseif ($this->getFullActionName() == "uploadprescription_index_index") {
                $title       = "Upload Prescription & Order Medicines Online at Best Prices - Zoylo";
                $keywords    = "Best Online Pharmacy Store in India, Best Pharmacy Online in India, Buy medicines Online, Online Doctor Prescription, Online Medicine App, Online Medicine Delivery, Online Medicines Home Delivery, Online Medicines Purchase, online medicines store, Online Pharmacy, Order Medicines Online, Pharmacy online, Prescription Upload Online, Purchase Medicines Online, Upload Medical Prescription Online, Upload Prescription Online, Zoylo";
                $description = "Upload your prescription to order medicine online through Zoylo and Get flat 25% discount on all prescription medicines & drugs. We deliver medicine at your doorstep.";
                $metaStatus  = true;
            }

            if($metaStatus){
                $this->pageConfig->setMetadata('description', $description);
                $this->pageConfig->setMetadata('keywords', $keywords);
                $this->pageConfig->setMetadata('title', $title);
                $this->pageConfig->getTitle()->set(__($title));
            }
        }
    }

    /**
     * @param Renderer $subject
     * @param $result
     * @return string
     */
    public function afterRenderHeadContent(Renderer $subject, $result)
    {
        if ($this->helperData->isEnabled()) {
            $fullActionname = $this->getFullActionName();
            $action = '';
            switch ($fullActionname) {
                case 'catalog_category_view':
                    if ($this->helperData->getInfoConfig('enable')) {
                        $result = $result . $this->showBusinessStructuredData();
                    }
                    /*if ($this->helperData->getRichsnippetsConfig('enable_site_link')) {
                        $result = $result . $this->showSiteLinksStructuredData();
                    }*/
                    /*if ($this->helperData->getRichsnippetsConfig('enable_category')) {
                        $categoryStructuredData = $this->showCategoryStructuredData();
                        $result                = $result . $categoryStructuredData;
                    }*/
                    $breadcrumbtStructuredData = $this->getBreadcrumbData($action = 'category');
                    $result                = $result . $breadcrumbtStructuredData;
                    break;
                case 'catalog_product_view':
                    if ($this->helperData->getInfoConfig('enable')) {
                        $result = $result . $this->showBusinessStructuredData();
                    }
                    /*if ($this->helperData->getRichsnippetsConfig('enable_site_link')) {
                        $result = $result . $this->showSiteLinksStructuredData();
                    }*/
                    if ($this->helperData->getRichsnippetsConfig('enable_product')) {
                        $productStructuredData = $this->showProductStructuredData();
                        $result                = $result . $productStructuredData;
                    }
                    $breadcrumbtStructuredData = $this->getBreadcrumbData($action = 'product');
                    $result                = $result . $breadcrumbtStructuredData;

                   /* $articleStructuredData = $this->getArticleStructuredData();
                    $result                = $result . $articleStructuredData;

                    $faqStructuredData = $this->getFaqStructuredData();
                    $result            = $result . $faqStructuredData;*/

                    $drugStructuredData = $this->getDrugStructuredData();
                    $result            = $result . $drugStructuredData;

                    break;
                case 'cms_index_index':
                    if ($this->helperData->getInfoConfig('enable')) {
                        $result = $result . $this->showBusinessStructuredData();
                    }
                    if ($this->helperData->getRichsnippetsConfig('enable_site_link')) {
                        $result = $result . $this->showSiteLinksStructuredData();
                    }

                    $result = $result . $this->getNavigationStructuredData();
                    break;
            }
        }

        return $result;
    }

    /**
     *  Show verifications from config
     */
    public function showVerifications()
    {
        $this->pageConfig->setMetadata(self::GOOLE_SITE_VERIFICATION, $this->helperData->getVerficationConfig('google'));
        $this->pageConfig->setMetadata(self::MSVALIDATE_01, $this->helperData->getVerficationConfig('bing'));
        $this->pageConfig->setMetadata(self::P_DOMAIN_VERIFY, $this->helperData->getVerficationConfig('pinterest'));
        $this->pageConfig->setMetadata(self::YANDEX_VERIFICATION, $this->helperData->getVerficationConfig('yandex'));
    }

    /**
     * Get full action name
     * @return string
     */
    public function getFullActionName()
    {
        return $this->request->getFullActionName();
    }

    /**
     * Get current product
     * @return mixed
     */
    public function getProduct()
    {
        return $this->registry->registry('current_product');
    }

    /**
     * Get current brand
     * @return mixed
     */
    public function getBrand()
    {
        return $this->registry->registry('current_brand');
    }

    /**
     * Get current salt
     * @return mixed
     */
    public function getSalt()
    {
        return $this->registry->registry('current_salt');
    }

    /**
     * Get Url
     * @param string $route
     * @param array $params
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->_urlBuilder->getUrl($route, $params);
    }

    /**
     * @param $productId
     * @return \Magento\CatalogInventory\Api\Data\StockItemInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductStock($productId)
    {
        return $this->stockItemRepository->get($productId);
    }

    /**
     * Get review count
     * @return mixed
     */
    public function getReviewCount()
    {
        if (!$this->getProduct()->getRatingSummary()) {
            $this->getEntitySummary($this->getProduct());
        }

        return $this->getProduct()->getRatingSummary()->getReviewsCount();
    }

    /**
     * Get rating summary
     * @return mixed
     */
    public function getRatingSummary()
    {
        if (!$this->getProduct()->getRatingSummary()) {
            $this->getEntitySummary($this->getProduct());
        }

        return $this->getProduct()->getRatingSummary()->getRatingSummary();
    }

    /**
     * @param $product
     * @return mixed
     */
    public function getEntitySummary($product)
    {
        $this->reviewFactory->create()->getEntitySummary($product, $this->_storeManager->getStore()->getId());
    }


    public function getCurrentCategory()
    {
        return $this->layerResolver->get()->getCurrentCategory();
    }

    public function getCurrentCategoryId()
    {
        return $this->getCurrentCategory()->getId();
    }

    public function getProductsByComposition($composition, $productId, $drug_type)
    {
        $productCollection = $this->productCollectionFactory
                            ->create()
                            ->addAttributeToSelect(array('name','url_key'))
                            ->addAttributeToFilter('salt_composition', $composition)
                            ->addAttributeToFilter('drug_type',array('like' => '%'.$drug_type.'%'))
                            ->addAttributeToFilter('entity_id', array('neq' => $productId))
                            ->addAttributeToSort('price', 'asc')
                            ->setPageSize(6)
                            ->load();
        return $productCollection;
    }

    /**
     * @return mixed
     */
    public function getBreadcrumbData($action)
    {
        
        try{
            $breadcrumbPath = $this->catalogHelperData->getBreadcrumbPath();
            //$breadcrumbPath['pharmacy'] = array('label' => 'Pharmacy', 'link' => $this->getUrl());
            if (array_key_exists("category3",$breadcrumbPath)){
                $breadcrumbPath['home'] = array('label' => 'Medicine', 'link' => $this->getUrl());
                unset($breadcrumbPath['category3']);
                $breadcrumbPath = array_reverse($breadcrumbPath);
            }
            //print_r($breadcrumbPath);die;
            //$breadcrumbPath = array_reverse($breadcrumbPath);

            $breadcrumbtStructuredData = [
                '@context'    => self::SCHEMA_ORG_URL,
                '@type'       => 'BreadcrumbList'
            ];

            $i =1;
            foreach ($breadcrumbPath as $value) {
                if(count($breadcrumbPath) > 0){
                    if(isset($value['link']) && !empty($value['link'])){
                        $link = $value['link'];
                     }else{
                        if($action == 'category'){
                            $link = $this->getCurrentCategory()->getData()['url'];
                        }else{
                            $link = $this->getProduct()->getProductUrl();
                        }
                     }   
                    $label = $value['label'];
                    $itemStructuredData[] = [
                        '@type'    => 'ListItem',
                        'position' => $i,
                        'item'     => [
                            '@id'  => $link,
                            'name' => $label
                        ]
                    ];
                    $i++;
                }
            }
            $breadcrumbtStructuredData['itemListElement'] = $itemStructuredData;
            return $this->helperData->createStructuredData($breadcrumbtStructuredData, '<!-- Breadcrumb Structured Data by Zoylo SEO-->');
        } catch (\Exception $e) {
            //$this->messageManager->addError(__('Can not add structured data'));
        }
    }

    public function getNavigationStructuredData(){

        try{
            $navigationList = array('prescription_medicines', 'upload_prescription', 'otc_medicines', 'healthcare_products', 'generate_prescription', 'customer_login', 'download_app', 'contact_us');
            $i = 1;
            foreach ($navigationList as $key => $value) {
                $navigationStructuredData[] = [
                    '@context'      => self::SCHEMA_ORG_URL,
                    '@type'         => 'SiteNavigationElement',
                    'position'      =>  $i,
                    'name'          =>  $this->helperData->getSeoNavigationConfig($value.'/name'),
                    'description'   =>  $this->helperData->getSeoNavigationConfig($value.'/description'),
                    'url'           =>  $this->helperData->getSeoNavigationConfig($value.'/url')
                ];
                $i++;
            }
            
            return $this->helperData->createStructuredData($navigationStructuredData, '<!-- Navigation Structured Data by Zoylo SEO-->');
        } catch (\Exception $e) {
            //$this->messageManager->addError(__('Can not add structured data'));
        }
    }

    public function getArticleStructuredData(){
        if ($currentProduct = $this->getProduct()) {
            $productType    = $currentProduct->getAttributeText('zoylo_sku_group');
			$productType 	= strtolower($productType);
            $isRelatedArticle = false;
            if( $productType == "medicine"){
                $isRelatedArticle   = true;
            }elseif( $productType == "otcmedicine"){
                $isRelatedArticle   = true;
            }elseif( $productType == "otc"){
                $isRelatedArticle   = true;
            }
            try{
                if($isRelatedArticle){
                    $imageUrl = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'wysiwyg/Blogs_Article_1Img.png';
                    $articleCollection = array('image'=>$imageUrl,'title'=>'Post Format – Video','content'=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non placerat mi…','url'=>'');
                    //foreach ($articleCollection as $key => $value) {
                    for($a=1;$a<=3;$a++){
                        $articleStructuredData[] = [
                                "@context" => self::SCHEMA_ORG_URL,
                                '@type' => 'Article',
                                'headline'  => 'Be Safe from food Poisoning',
                                'url'   => 'https://www.zoylo.com/blog/topic/be-safe-from-foodpoisoning',
                                'image' => $imageUrl,
                                'mainEntityOfPage' => 'https://www.zoylo.com/blog/topic/be-safe-from-foodpoisoning',
                                "dateCreated" => "Oct 26, 2018",
                                "datePublished" => "Oct 26, 2018",
                                "dateModified" => "Oct 26, 2018",
                                "articleSection" => "Remedy",
                                "articleBody" => "Anybody can get food poisoning, but most people don’t think about food safety until they or someone they know gets sick after eating contaminated food.",
                                "keywords" => "Sinusitis, Yoga for Sinusitis, Yoga Poses for Sinus Relief",
                                "isFamilyFriendly" => true,
                                "author" => [
                                    "@type" => "Person",
                                    "name" => "Zoylo",
                                    "url" => $this->getUrl()
                                ],
                                "publisher" => [
                                    "@type" =>"Organization", 
                                    "name" => "Zoylo Digihealth Pvt Ltd", 
                                    "logo" => [
                                        "@type" => "ImageObject",
                                        "url" => $this->helperData->getLogo(),
                                        "width" => "157",
                                        "height" => "53"
                                    ],
                                    "url" => $this->getUrl()
                                ]
                            ];
                    }
                    //}

                    return $this->helperData->createStructuredData($articleStructuredData, '<!-- Article Structured Data by Zoylo SEO-->');
                }
            } catch (\Exception $e) {
                //$this->messageManager->addError(__('Can not add structured data'));
            }
        }
    }

    public function getFaqStructuredData(){
        if ($currentProduct = $this->getProduct()) {
            $productType    = $currentProduct->getAttributeText('zoylo_sku_group');
			$productType 	= strtolower($productType);
            $isFaQ          = false;
            if( $productType == "medicine"){
                $isFaQ   = true;
            }

            try{
                if($isFaQ){
                    $faqdata = $currentProduct->getData('zoylo_faq');
                    $faqCollectionArray = explode('$', $faqdata);
                    //print_r($faqCollectionArray);
                    /*foreach ($faqCollectionArray as $key => $value) {
                        $finalData = explode('?', $value);
                        foreach ($finalData as $question => $ans) {
                            $faqStructuredData[] = [
                            "@context" => 'https://schema.org/',
                            '@type' => 'Question',
                            "name" => $question,
                            "acceptedAnswer" => [
                                "@type" => "Answer",
                                "text" => $ans
                                ]
                            ];
                        }
                    }*/

                    for($a=1;$a<=3;$a++){
                        $faqStructuredData[] = [
                            "@context" => self::SCHEMA_ORG_URL,
                            '@type' => 'Question',
                            "name" => "What is xylometazoline hydrochloride",
                            "acceptedAnswer" => [
                                "@type" => "Answer",
                                "text" => "Xylometazoline nasal is a decongestant that shrinks blood vessels in the nasal passages. Dilated blood vessels can cause nasal congestion (stuffy nose). Xylometazoline nasal (for use in the nose) is used to treat stuffy nose caused by allergies, sinus irritation, or the common cold."
                            ]
                        ];
                    }
                    return $this->helperData->createStructuredData($faqStructuredData, '<!-- FAQ Structured Data by Zoylo SEO-->');
                }
            } catch (\Exception $e) {
                //$this->messageManager->addError(__('Can not add structured data'));
            }
        }
    }

    public function getDrugStructuredData(){
        if ($currentProduct = $this->getProduct()) {
            $productType    = $currentProduct->getAttributeText('zoylo_sku_group');
			$productType 	= strtolower($productType);
            $isDrug         = false;
            $isRealted      = false;
            if( $productType == "medicine"){
                $isDrug      = true;
                $isRealted   = true;
                $legalStatus      = 'Prescription medicine';
                $prescriptionStatus = 'PrescriptionOnly';
            }elseif ($productType == "otcmedicine") {
                $isDrug           = true;
                $isRealted        = true;
                $legalStatus      = 'Non-Prescription medicine';
                $prescriptionStatus = 'OTC';
            }

            try{
                if($isDrug){
                    $name           = $currentProduct->getName();
                    $productUrl     = $currentProduct->getProductUrl();
                    $description    = $currentProduct->getDescription();
                    $productId      = $currentProduct->getId() ? $currentProduct->getId() : $this->request->getParam('id');
                    $composition    = $currentProduct->getResource()->getAttribute('salt_composition')->getFrontend()->getValue($currentProduct);
                    $drug_type      = $currentProduct->getResource()->getAttribute('drug_type')->getFrontend()->getValue($currentProduct);
                    $saltName       = $currentProduct->getResource()->getAttribute('salt_name')->getFrontend()->getValue($currentProduct);
                    if($saltName == '-- Please Select --'){
                        $saltName = ''; 
                    }else {
                        $saltName = $saltName;
                    }
                    $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();

                    $aliasType = '';
                    if($drug_type == 'TABLETS' || $drug_type == 'CAPSULES'){
                        $aliasType = 'Tablet';
                    }

                    else if($drug_type == 'DRY SYP'){
                        $aliasType = 'Dry Syrup';
                    }

                    else if($drug_type == 'Syrup' || $drug_type == 'Suspension' || $drug_type == 'Gel (Syrup)' || $drug_type == 'Expectorant'){
                        $aliasType = 'Syrup';
                    }

                    else if($drug_type == 'Balm' || $drug_type == 'Cream' || $drug_type == 'Ointment' || $drug_type == 'Oil' || $drug_type == 'Lotion' || $drug_type == 'Gel (Paste)' || $drug_type == 'Gel (Topical)' || $drug_type == 'Gel' || $drug_type == 'Liniment' || $drug_type == 'Serum' ) {
                            $aliasType = 'Gel';
                    }
                    
                    else if($drug_type == 'Face Wash' || $drug_type == 'Foam' || $drug_type == 'Soap'){
                        $aliasType = 'Soap';
                    }

                    else if($drug_type == 'Abdominal Binder' || $drug_type == 'Antiseptic Powder' || $drug_type == 'Dusting Powder' || $drug_type == 'Enema' || $drug_type == 'Granules' || $drug_type == 'Powder') {
                        $aliasType = 'Powder';
                    }

                    else if($drug_type == 'Suppositories'){
                        $aliasType = 'Suppositories';
                    }

                    else if($drug_type == 'Inhaler'){
                        $aliasType = 'Inhaler';
                    }

                    else if($drug_type == 'Injectible' || $drug_type == 'Injection') {
                        $aliasType = 'Injection';
                    }

                    else if($drug_type == 'Eye Ear Drops' || $drug_type == 'Eye Drops' || $drug_type == 'Ear Drops' || $drug_type == 'Nasal Drops' || $drug_type == 'Oral Drops' || $drug_type == 'Gel (Paste)' || $drug_type == 'Gel (Topical)' || $drug_type == 'Gel' || $drug_type == 'Liniment' || $drug_type == 'Serum' ) {
                        $aliasType = 'Drops';
                    }

                    else if($drug_type == 'Liquid' || $drug_type == 'Respules' || $drug_type == 'Mouth Wash' || $drug_type == 'Solution') {
                        $aliasType = 'Liquid';
                    }

                    else if($drug_type == 'Spray' || $drug_type == 'Nasal Spray') {
                        $aliasType = 'Spray';
                    }
                    
                    $saltInformation = $objectManager->create('I95Dev\Salts\Model\Salt')->getCollection()->addFieldToFilter('name',  array('like' => $saltName))
                        ->addFieldToFilter('drug_type', array('like' => '%'.$aliasType.'%'));
                    $saltInformation->getSelect()->limit(1);
                    $alcohol        = '';
                    $overdose       = '';
                    $pregnancy      = '';
                    $lactation      = '';
                    foreach ($saltInformation as $information) {
                        $alcohol    = $information->getKidney();
                        $overdose   = $information->getOverdose();
                        $pregnancy  = $information->getPregnancy();
                        $lactation  = $information->getLactation();
                    }

                    $saltArray = [];
                    $compositionCollection = explode('+',$composition);

                    if(count($compositionCollection) > 1 ){
                        foreach($compositionCollection as $salt) {
                            if( preg_match('#\[(.*?)\]#', $salt, $match) )
                                $text = $match[1];//die;
                            $unit = substr($text, 0, -2);
                            $value = substr($text, -2);
                            $saltArray[] = [
                                'strengthUnit' => $unit,
                                '@type' => 'DrugStrength',
                                'strengthValue' => $value,
                                'activeIngredient' => ''
                            ];
                        }
                    }
                    
                    $alcoholtext = $this->helperData->getConfig('precautions/alcohol/'.strtolower($alcohol));
                    $overdosetext = $this->helperData->getConfig('precautions/overdose/'.strtolower($overdose));

                    if($alcohol == 'Unknown') { 
                        $alcoholtext = sprintf($alcoholtext,$currentProduct->getName(),$currentProduct->getName());
                    }else{ 
                        $alcoholtext = sprintf($alcoholtext, $currentProduct->getName()); 
                    }

                    if($overdose == 'Unknown') { 
                        $overdosetext = sprintf($overdosetext,$currentProduct->getName(),$currentProduct->getName());
                    }else{ 
                        $overdosetext = sprintf($overdosetext, $currentProduct->getName()); 
                    }

                    $brand = $currentProduct->getAttributeText('product_brand');

                    $drugStructuredData = [
                        "@context" => self::SCHEMA_ORG_URL,
                        '@type' => 'Drug',
                        'name' => $name,
                        'prescriptionStatus' => self::SCHEMA_ORG_URL.$prescriptionStatus,
                        //'dosageForm' => ["ML"],
                        //'drugUnit' => '10ml',
                        'isAvailableGenerically' =>  true,
                        'isProprietary' => true,
                        'activeIngredient' => '',
                        'administrationRoute' => '',
                        'warning' => '',
                        'alcoholWarning' => $alcoholtext,
                        'overdosage' => $overdosetext,
                        'foodWarning' => '',
                        'pregnancyWarning' => $pregnancy,
                        'breastfeedingWarning' => $lactation,
                        'mechanismOfAction' => '',
                        'mainEntityOfPage' => $productUrl,
                        'url' => $productUrl,
                        'description' => $description,
                        'availableStrength' => $saltArray,
                        /*'doseSchedule' => [
                            '@type' => 'DoseSchedule',
                            'doseValue' => '10',
                            'doseUnit' => 'ml'
                        ],*/
                        'relevantSpecialty' => [
                            '@type' => 'MedicalSpecialty',
                            'name' => ''
                        ],
                        'legalStatus' => [
                            '@type' => 'DrugLegalStatus',
                            'applicableLocation' => 'India',
                            'name' => $legalStatus
                        ],
                        'manufacturer' => [
                            '@type' => 'Organization',
                            'name' => $brand
                        ]
                    ];
                    
                    if($isRealted){
                        $relatedCollection = $this->getProductsByComposition($composition, $productId, $drug_type);
                        if(count($relatedCollection)>0){
                            foreach($relatedCollection as $related) {
                                $relatedStrctureData[] = [
                                    '@type' => 'Drug',
                                    'name'  => $related->getName(),
                                    'url'   => $related->getProductUrl()
                                ];
                            }
                            $drugStructuredData['relatedDrug'] = $relatedStrctureData;
                        }
                    }

                    return $this->helperData->createStructuredData($drugStructuredData, '<!-- Drug Structured Data by Zoylo SEO-->');
                }
            } catch (\Exception $e) {
                //$this->messageManager->addError(__('Can not add structured data'));
            }
        }
    }
    
    /**
     * Show Category structured data
     * @return string
     *
     * Learn more: https://developers.google.com/structured-data/rich-snippets/products#single_product_page
     */
    public function showCategoryStructuredData()
    {
        if ($currentCategory =  $this->getCurrentCategory()) {
            try {
                $categoryStructuredData = [
                    '@context'    => self::SCHEMA_ORG_URL,
                    '@type'       => 'Category',
                    'name'        => $currentCategory->getName()
                    //'description' => $currentCategory->getDescription()

                ];
                return $this->helperData->createStructuredData($categoryStructuredData, '<!-- Category Structured Data by Zoylo SEO-->');
            } catch (\Exception $e) {
                //$this->messageManager->addError(__('Can not add structured data'));
            }
        }
    }

    /**
     * Show product structured data
     * @return string
     *
     * Learn more: https://developers.google.com/structured-data/rich-snippets/products#single_product_page
     */
    public function showProductStructuredData()
    {
        if ($currentProduct = $this->getProduct()) {
            try {
                $productId = $currentProduct->getId() ? $currentProduct->getId() : $this->request->getParam('id');

                //$product         = $this->productFactory->create()->load($productId);
                $availability    = $currentProduct->isAvailable() ? 'InStock' : 'OutOfStock';
                $stockItem       = $this->stockState->getStockItem(
                    $currentProduct->getId(),
                    $currentProduct->getStore()->getWebsiteId()
                );
                $priceValidUntil = $currentProduct->getSpecialToDate();

                $productType      = $currentProduct->getAttributeText('zoylo_sku_group');
				$productType 	  = strtolower($productType);
                $isComboPack      = false;

                if( $productType == "medicine"){
                    $category           = "RX";
                    $brand        = $currentProduct->getAttributeText('manufacturer');
                }elseif( $productType == "otcmedicine"){
                    $category     = "OTC";
                    $brand        = $currentProduct->getAttributeText('manufacturer');
                }elseif ($productType == "otc") {
                    $category     = "Medical Webpage";
                    $isComboPack  = true;
                    $brand        = $currentProduct->getAttributeText('product_brand');
                }
                $tomorrow = date("Y-m-d", strtotime("+1 day"));

                $mediaUrl   = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                $baseUrl    = $this->_storeManager->getStore()->getBaseUrl();
                
                $drug_type = $currentProduct->getResource()->getAttribute('drug_type')->getFrontend()->getValue($currentProduct);
                $drugTypeUrl = '';
                if (isset($drug_type) && !is_array($drug_type) && $drug_type != '' && $drug_type != 'no_selection' ) {
                    $drugTypeUrl = $mediaUrl . 'packtype/' . ucwords(strtolower($drug_type)) . '.png';
                }elseif($productType  == "otc"){
                    $categoryIds= $currentProduct->getCategoryIds();
                    $_categoryImgUrl = '';
                    foreach ($categoryIds as $catId){
                        if($catId == 1 || $catId == 2 || $catId == 3 || $catId == 6 ||$catId == 7)
                            continue;
                        
                        $category = $this->categoryRepository->get($catId);
                        if($category->getLevel() == 3) {
                            $_categoryImgUrl = $category->getImageUrl();
                        }
                    }
                    if($_categoryImgUrl){
                        $drugTypeUrl = $_categoryImgUrl;
                    }else{
                        $drugTypeUrl = $mediaUrl . 'packtype/OTC.png';
                    }
                }

                $image = '';
                if($currentProduct->getImage() && $currentProduct->getImage() != "no_selection"){
                    $image =  $mediaUrl. 'catalog/product' . $currentProduct->getImage();
                }
                if ( $image == '' && $drugTypeUrl) {
                    $image = $drugTypeUrl;
                }

                if($image == ''){
                    $image = $baseUrl.'pub/static/frontend/Smartwave/porto/en_US/Magento_Catalog/images/product/placeholder/image.jpg';
                }

                /*if($currentProduct->getImage()){
                    $image = $this->getUrl('pub/media/catalog') . 'product' . $currentProduct->getImage();
                }elseif($drugTypeUrl) {
                    $image = $drugTypeUrl;
                }else{
                    $image = $this->_urlBuilder->getBaseUrl().'pub/static/frontend/Smartwave/porto/en_US/Magento_Catalog/images/product/placeholder/small_image.jpg';
                }*/

                $productStructuredData = [
                    '@context'    => self::SCHEMA_ORG_URL,
                    '@type'       => 'Product',
                    'name'        => $currentProduct->getName(),
                    'category'     => $category,
                    'description' => trim(strip_tags($currentProduct->getDescription())),
                    'brand'        => ['name' => $brand],
                    'sku'         => $currentProduct->getSku(),
                    //'url'         => $currentProduct->getProductUrl(),
                    'image'       => $image,
                    'mpn'         => $currentProduct->getSku(),
                    'offers'      => [
                        '@type'         => 'Offer',
                        'priceCurrency' => $this->_storeManager->getStore()->getCurrentCurrencyCode(),
                        'price'         => $currentProduct->getPriceInfo()->getPrice('final_price')->getValue(),
                        'url'           => $currentProduct->getProductUrl(),
                        //'itemOffered'   => $stockItem->getQty(),
                        //'itemOffered'   => ['name' => $brand],
                        'priceValidUntil' => $tomorrow,
                        'itemCondition' => self::SCHEMA_ORG_URL .'NewCondition',
                        'availability'  => self::SCHEMA_ORG_URL . $availability
                    ]
                ];
                $productStructuredData = $this->addProductStructuredDataByType($currentProduct->getTypeId(), $currentProduct, $productStructuredData);

                if (!empty($priceValidUntil)) {
                    $productStructuredData['offers']['priceValidUntil'] = $priceValidUntil;
                }

                if ($this->getReviewCount()) {
                    $productStructuredData['aggregateRating']['@type']       = 'AggregateRating';
                    $productStructuredData['aggregateRating']['bestRating']  = 100;
                    $productStructuredData['aggregateRating']['worstRating'] = 0;
                    $productStructuredData['aggregateRating']['ratingValue'] = $this->getRatingSummary();
                    $productStructuredData['aggregateRating']['reviewCount'] = $this->getReviewCount();
                }

                $objectStructuredData = new \Magento\Framework\DataObject(['mpdata' => $productStructuredData]);
                $this->_eventManager->dispatch('mp_seo_product_structured_data', ['structured_data' => $objectStructuredData]);
                $productStructuredData = $objectStructuredData->getMpdata();

                return $this->helperData->createStructuredData($productStructuredData, '<!-- Product Structured Data by Zoylo SEO-->');
            } catch (\Exception $e) {
                //$this->messageManager->addError(__('Can not add structured data'));
            }
        }
    }

    /**
     * get Business Structured Data
     *
     * @return string
     */
    public function showBusinessStructuredData()
    {
        $businessStructuredData = [
            '@context'              => self::SCHEMA_ORG_URL,
            '@type'                 => 'MedicalOrganization',
            'url'                   => $this->getUrl(),
            'logo'                  => $this->helperData->getLogo(),
            'name'                  => $this->helperData->getInfoConfig('business_name'),
            'awards'                => $this->helperData->getInfoConfig('awards'),
            'foundingLocation'      => $this->helperData->getInfoConfig('founding_location'),
            'founder'               => $this->helperData->getInfoConfig('founder'),
            'foundingDate'          => $this->helperData->getInfoConfig('founding_date'),
            'numberOfEmployees'     => $this->helperData->getInfoConfig('number_of_employees'),
            'email'                 => $this->helperData->getInfoConfig('email'),
            'hasMap'                => $this->helperData->getInfoConfig('has_map'),
            'description'           => $this->helperData->getInfoConfig('description'),
            'openingHours'          => $this->helperData->getInfoConfig('opening_hours')
        ];
        if (!empty($this->getSocialProfiles())) {
            $businessStructuredData['sameAs'] = $this->getSocialProfiles();
        }
        $businessStructuredData['contactPoint'] = [];
        $businessStructuredData['address'] = [];

        // get customer service info
        if ($this->helperData->getInfoConfig('customer_service_phone')
            || $this->helperData->getInfoConfig('customer_service_contact_option')
            || $this->helperData->getInfoConfig('customer_service_area_serve')
        ) {
            $customerService = [
                '@type'         => 'ContactPoint',
                'telephone'     => $this->helperData->getInfoConfig('customer_service_phone'),
                'contactType'   => 'customer service',
                'contactOption' => $this->helperData->getInfoConfig('customer_service_contact_option'),
                'areaServed'    => $this->helperData->getInfoConfig('customer_service_area_serve')
            ];
            array_push($businessStructuredData['contactPoint'], $customerService);
        }
        // get technical support info
        if ($this->helperData->getInfoConfig('technical_support_phone')
            || $this->helperData->getInfoConfig('technical_support_contact_option')
            || $this->helperData->getInfoConfig('technical_support_area_serve')
        ) {
            $technicalSupport = [
                '@type'         => 'ContactPoint',
                'telephone'     => $this->helperData->getInfoConfig('technical_support_phone'),
                'contactType'   => 'technical support',
                'contactOption' => $this->helperData->getInfoConfig('technical_support_contact_option'),
                'areaServed'    => $this->helperData->getInfoConfig('technical_support_area_serve')
            ];
            array_push($businessStructuredData['contactPoint'], $technicalSupport);
        }
        // get sales info
        if ($this->helperData->getInfoConfig('sales_phone')
            || $this->helperData->getInfoConfig('sales_contact_option')
            || $this->helperData->getInfoConfig('sales_area_serve')
        ) {
            $sales = [
                '@type'         => 'ContactPoint',
                'telephone'     => $this->helperData->getInfoConfig('sales_phone'),
                'contactType'   => 'sales',
                'contactOption' => $this->helperData->getInfoConfig('sales_contact_option'),
                'areaServed'    => $this->helperData->getInfoConfig('sales_area_serve')
            ];
            array_push($businessStructuredData['contactPoint'], $sales);
        }

        // get Address info
        if ($this->helperData->getInfoConfig('address_locality')
            || $this->helperData->getInfoConfig('postal_code')
            || $this->helperData->getInfoConfig('street_address')
        ) {
            $address = [
                '@type'             => 'PostalAddress',
                'addressLocality'   => $this->helperData->getInfoConfig('address_locality'),
                'postalCode'        => $this->helperData->getInfoConfig('postal_code'),
                'streetAddress'     => $this->helperData->getInfoConfig('street_address')
            ];
            array_push($businessStructuredData['address'], $address);
        }

        return $this->helperData->createStructuredData($businessStructuredData, '<!-- Business Structured Data by Zoylo SEO-->');
    }

    /**
     * get Social Profiles config
     *
     * @return array|string
     */

    public function getSocialProfiles()
    {
        $lines    = [];
        if ($profiles = $this->helperData->getSocialProfiles()) {
            foreach ($profiles as $_profile) {
                if ($_profile) {
                    $lines[] = $_profile;
                }
            }
        }

        return $lines;
    }

    /**
     * get Sitelinks Searchbox Structured Data
     *
     * @return string
     */
    public function showSiteLinksStructuredData()
    {
        $siteLinksStructureData = [
            '@context'        => self::SCHEMA_ORG_URL,
            '@type'           => 'WebSite',
            'url'             => $this->_urlBuilder->getBaseUrl(),
            'potentialAction' => [[
                '@type'       => 'SearchAction',
                'target'      => $this->_searchHelper->getResultUrl() . '?q={searchbox_target}',
                'query-input' => 'required name=searchbox_target'
             ],[
                '@type'       => 'SearchAction',
                'target'      => 'android-app://com.example/'.$this->_searchHelper->getResultUrl() . '?q={searchbox_target}',
                'query-input' => 'required name=searchbox_target'
             ]
            ]
        ];

        return $this->helperData->createStructuredData($siteLinksStructureData, '<!-- Sitelinks Searchbox Structured Data by Zoylo SEO-->');
    }

    /**
     * add Grouped Product Structured Data
     *
     * @param $currentProduct
     * @param $productStructuredData
     * @return mixed
     */
    public function getGroupedProductStructuredData($currentProduct, $productStructuredData)
    {
        $productStructuredData['offers']['@type'] = 'AggregateOffer';
        $childrenPrice                            = [];
        $offerData                                = [];
        $typeInstance                             = $currentProduct->getTypeInstance();
        $childProductCollection                   = $typeInstance->getAssociatedProducts($currentProduct);
        foreach ($childProductCollection as $child) {
            $imageUrl = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA)
                . 'catalog/product' . $child->getImage();

            $offerData[]     = [
                '@type' => "Offer",
                'name'  => $child->getName(),
                'price' => $this->_priceHelper->currency($child->getPrice(), false),
                'sku'   => $child->getSku(),
                'image' => $imageUrl
            ];
            $childrenPrice[] = $this->_priceHelper->currency($child->getPrice(), false);
        }

        $productStructuredData['offers']['highPrice'] = array_sum($childrenPrice);
        $productStructuredData['offers']['lowPrice']  = min($childrenPrice);
        unset($productStructuredData['offers']['price']);

        if (!empty($offerData)) {
            $productStructuredData['offers']['offers'] = $offerData;
        }

        return $productStructuredData;
    }

    /**
     * add Downloadable Product Structured Data
     *
     * @param $currentProduct
     * @param $productStructuredData
     * @return mixed
     */
    public function getDownloadableProductStructuredData($currentProduct, $productStructuredData)
    {
        $productStructuredData['offers']['@type'] = 'AggregateOffer';

        $typeInstance           = $currentProduct->getTypeInstance();
        $childProductCollection = $typeInstance->getLinks($currentProduct);
        $childrenPrice          = [];
        foreach ($childProductCollection as $child) {
            $offerData[]     = [
                '@type' => "Offer",
                'name'  => $child->getTitle(),
                'price' => $this->_priceHelper->currency($child->getPrice(), false)
            ];
            $childrenPrice[] = $this->_priceHelper->currency($child->getPrice(), false);
        }
        $productStructuredData['offers']['highPrice'] = array_sum($childrenPrice);
        $productStructuredData['offers']['lowPrice']  = min($childrenPrice);

        if (!empty($offerData)) {
            $productStructuredData['offers']['offers'] = $offerData;
        }

        return $productStructuredData;
    }

    /**
     * add Configurable Product Structured Data
     *
     * @param $currentProduct
     * @param $productStructuredData
     * @return mixed
     */
    public function getConfigurableProductStructuredData($currentProduct, $productStructuredData)
    {
        $productStructuredData['offers']['@type']     = 'AggregateOffer';
        $productStructuredData['offers']['highPrice'] = $currentProduct->getPriceInfo()->getPrice('regular_price')->getMaxRegularAmount()->getValue();
        $productStructuredData['offers']['lowPrice']  = $currentProduct->getPriceInfo()->getPrice('regular_price')->getMinRegularAmount()->getValue();
        $offerData                                    = [];
        $typeInstance                                 = $currentProduct->getTypeInstance();
        $childProductCollection                       = $typeInstance->getUsedProductCollection($currentProduct)->addAttributeToSelect('*');
        foreach ($childProductCollection as $child) {
            $imageUrl = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA)
                . 'catalog/product' . $child->getImage();

            $offerData[] = [
                '@type' => "Offer",
                'name'  => $child->getName(),
                'price' => $this->_priceHelper->currency($child->getPrice(), false),
                'sku'   => $child->getSku(),
                'image' => $imageUrl
            ];
        }
        if (!empty($offerData)) {
            $productStructuredData['offers']['offers'] = $offerData;
        }

        return $productStructuredData;
    }

    /**
     * add Bundle Product Structured Data
     *
     * @param $currentProduct
     * @param $productStructuredData
     * @return mixed
     */
    public function getBundleProductStructuredData($currentProduct, $productStructuredData)
    {
        $productStructuredData['offers']['@type']     = 'AggregateOffer';
        $productStructuredData['offers']['highPrice'] = $currentProduct->getPriceInfo()->getPrice('regular_price')->getMaximalPrice()->getValue();
        $productStructuredData['offers']['lowPrice']  = $currentProduct->getPriceInfo()->getPrice('regular_price')->getMinimalPrice()->getValue();
        unset($productStructuredData['offers']['price']);
        $offerData              = [];
        $typeInstance           = $currentProduct->getTypeInstance();
        $childProductCollection = $typeInstance->getSelectionsCollection($typeInstance->getOptionsIds($currentProduct), $currentProduct);
        foreach ($childProductCollection as $child) {
            $imageUrl = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA)
                . 'catalog/product' . $child->getImage();

            $offerData[] = [
                '@type' => "Offer",
                'name'  => $child->getName(),
                'price' => $this->_priceHelper->currency($child->getPrice(), false),
                'sku'   => $child->getSku(),
                'image' => $imageUrl
            ];
        }
        if (!empty($offerData)) {
            $productStructuredData['offers']['offers'] = $offerData;
        }

        return $productStructuredData;
    }

    /**
     * add Product Structured Data By Type
     *
     * @param $productType
     * @param $currentProduct
     * @param $productStructuredData
     * @return mixed
     */
    public function addProductStructuredDataByType($productType, $currentProduct, $productStructuredData)
    {
        switch ($productType) {
            case 'grouped':
                $productStructuredData = $this->getGroupedProductStructuredData($currentProduct, $productStructuredData);
                break;
            case 'bundle':
                $productStructuredData = $this->getBundleProductStructuredData($currentProduct, $productStructuredData);
                break;
            case 'downloadable':
                $productStructuredData = $this->getDownloadableProductStructuredData($currentProduct, $productStructuredData);
                break;
            case 'configurable':
                $productStructuredData = $this->getConfigurableProductStructuredData($currentProduct, $productStructuredData);
                break;
        }

        return $productStructuredData;
    }
}
