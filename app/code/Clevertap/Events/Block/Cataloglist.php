<?php
namespace Clevertap\Events\Block;
class Cataloglist extends \Magento\Framework\View\Element\Template {
    protected $clevertapHelper;
    private $layerResolver;
    protected $request;
    public function __construct(\Magento\Catalog\Block\Product\Context $context,
            \Clevertap\Events\Helper\Data $clevertapHelper,
            \Magento\Catalog\Model\Layer\Resolver $layerResolver,
            \Magento\Framework\App\Request\Http $request,
            array $data = []) 
    {
        $this->clevertapHelper = $clevertapHelper;
        $this->layerResolver = $layerResolver;
        $this->request = $request;
        parent::__construct($context, $data);

    }
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    
    public function getClevertapStatus(){
        return $this->clevertapHelper->getCategoryListStatus();
    }
    
    public function getCurrentCategory()
    {   $clevertapStatus = $this->getClevertapStatus();
        if($clevertapStatus==1){
            return $category = $this->layerResolver->get()->getCurrentCategory();
            //return $category->getName();
        }else{
            return false;
        }
    }
    public function getParentCategory()
    {
        //$parent_category = $this->getCurrentCategory()->getParentCategory();
        $parentCategoryObj = $this->getCurrentCategory();
        if($parentCategoryObj!=''){
            return $parent_category=$parentCategoryObj->getParentCategory();
        }else{
            return '';
        }
        
    }
    
    public function getParentCategoryName()
    {
        $parentCategory = $this->getParentCategory();
        if($parentCategory!=''){
            return $parentCategory->getName();
        }else{
            return '';
        }
        
    }
    
    public function getCurrentCategoryId()
    {
        return $this->getCurrentCategory()->getId();
    }
    
    public function getCurrentCategoryName()
    {
        $clevertapStatus = $this->getClevertapStatus();
        if($clevertapStatus==1){
            return $this->getCurrentCategory()->getName();
        }else{
            return false;
        }
    }
    public function getCategoryNameForClevertap()
    {
        $finalCatName = "";
        $getRequestCatId = $this->request->getParam('cat');
        if($getRequestCatId!=''){
            $explodeCatId = explode(",",$getRequestCatId);
            if(isset($explodeCatId) && !empty($explodeCatId)){
                 $finalCatId = end($explodeCatId);
                 if($finalCatId>0 && $finalCatId!=''){
                    $finalCatName = $this->getParentCatName($finalCatId);
                 }
            }
        }else{
            $parentCatName = $this->getParentCategoryName();
            if($parentCatName=='Default Category'):
                $finalCatName = $this->getCurrentCategoryName();
            endif;
        }
        return $finalCatName;
    }

    public function getParentCatName($finalCatId){
        $parentCatName = "";
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of Object Manager
        $categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');// Instance of Category Model
        $category = $categoryFactory->create()->load($finalCatId);
        $parentCategories = $category->getParentCategory();
        if(isset($parentCategories) && !empty($parentCategories)){
            $parentCatId = $parentCategories->getId();
            $parentCategoryObj = $categoryFactory->create()->load($parentCatId);
            if(isset($parentCategoryObj) && $parentCategoryObj!=''){
                $parentCatName = $parentCategoryObj->getName();
            }
         }
         return $parentCatName;
    }
    
    public function getParamsStatusForSubcat(){
        $getRequestCatId = $this->request->getParam('cat');
        if($getRequestCatId!=''){
            return true;
        }else{
            return false;
        }
    }
}