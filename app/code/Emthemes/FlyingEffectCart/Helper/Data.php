<?php
/**
 * Copyright © 2015 Emthemes . All rights reserved.
 */
namespace Emthemes\FlyingEffectCart\Helper;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Checkout\Model\Session;
use Magento\Catalog\Model\ProductRepository;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	protected $_storeManager;
	protected $_scopeConfig;
	protected $cart;
	protected $checkoutsession;
	/**
     * @param \Magento\Framework\App\Helper\Context $context
     */
	public function __construct(
		\Magento\Framework\App\Helper\Context $context,
		Session $checkoutsession,
		ProductRepository $productrepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager
		//\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	) {
		parent::__construct($context);
		$this->_storeManager = $storeManager;
		$this->checkoutsession = $checkoutsession; 
		$this->productrepository = $productrepository;
		$this->_scopeConfig = $context->getScopeConfig();//$scopeConfig;
	}
	public function getConfig($fullPath){
		return $this->_scopeConfig->getValue($fullPath, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
	public function getBaseUrl(){
		return $this->_storeManager->getStore()->getBaseUrl();
	}
	
	public function getprescriptionItemsCount(){
		
		$checkoutSession = $this->checkoutsession;
		$allItems = $checkoutSession->getQuote()->getAllItems();
		
		echo "<pre>";print_r(get_class_methods($checkoutSession->getQuote()));
		echo count($allItems);echo "asd";exit;
		$ids ='';
		foreach($allItems as $item) {
			echo $sku = $item->getSku();
			$itemId = $item->getItemId(); //item id of particular item
			$productRepository = $this->productrepository;
			$product = $productRepository->get(trim($sku));
			$prescriptionAction = $product->getPrescriptionAction(); 
			if(isset($prescriptionAction) && $prescriptionAction == 584)
				{
					$ids[] = $prescriptionAction;
				} 
		}
		$presction_items = count($ids);
		return $presction_items;
	}
}