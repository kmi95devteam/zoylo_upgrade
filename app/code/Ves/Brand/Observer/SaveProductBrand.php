<?php
/**
 * Venustheme
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Venustheme.com license that is
 * available through the world-wide-web at this URL:
 * http://www.venustheme.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Venustheme
 * @package    Ves_Brand
 * @copyright  Copyright (c) 2014 Venustheme (http://www.venustheme.com/)
 * @license    http://www.venustheme.com/LICENSE-1.0.html
 */
namespace Ves\Brand\Observer;

use Magento\Framework\Event\ObserverInterface;

class SaveProductBrand implements ObserverInterface
{
    /**
     * Catalog data
     *
     * @var \Magento\Catalog\Helper\Data
     */
    protected $catalogData;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Framework\App\ResourceConnection  $resource
     * @param \Magento\Framework\Registry                         $coreRegistry         [description]
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Registry $coreRegistry
        )
    {
        $this->_resource = $resource;
        $this->_coreRegistry = $coreRegistry;
    }

    /**
     * Checking whether the using static urls in WYSIWYG allowed event
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $connection = $this->_resource->getConnection();
        $table_name = $this->_resource->getTableName('ves_brand_product');
        $productController = $observer->getController();
        $productId = $productController->getRequest()->getParam('id');
        $data = $productController->getRequest()->getPostValue();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $_product = $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
        $applyRule = $_product->getApplyCatalogRules();
        if(isset($applyRule) && $applyRule == 1){
            $price =  $_product->getPrice();
            $percentage = $_product->getPercentage();
            if($percentage !=''){
            $newPrice = ($percentage / 100) * $price;
            $discountPrice = $price - $newPrice;
            $_product->setSpecialPrice($discountPrice);
            }
            
        }else {
           $dodProduct = $_product->getCountdownEnabled();
           if($dodProduct !=1){
           $_product->setSpecialPrice();
           $_product->setPercentage();
           }
        }
        $_product->save();
        $label = $_product->getResource()->getAttribute('product_brand')->getFrontend()->getValue($_product);
        if($label){
            $selectQuery = 'SELECT brand_id FROM ves_brand WHERE name =  "' . $label .'"';
            $selectBrandResult = $connection->fetchAll($selectQuery);
            $barndId = $selectBrandResult[0]['brand_id'];
        }
        $this->_coreRegistry->register('current_post_product', $data);

        $is_saved_brand = $this->_coreRegistry->registry('fired_save_action');
        if(!$is_saved_brand) {
            if($productId) {
                //$selectQuery = 'SELECT * FROM ' . $table_name . ' WHERE product_id =  ' . (int)$productId;
                //$selectBrandResult = $connection->fetchAll($selectQuery);
                //if(!empty($selectBrandResult)){
                    $connection->query('DELETE FROM ' . $table_name . ' WHERE product_id =  ' . (int)$productId . ' ');
                //}
            }
            if(isset($barndId) && $productId){
                $productBrands = $barndId;
                if(!is_array($productBrands)){
                    $productBrands = array();
                    $productBrands[] = (int)$barndId;
                }
                foreach ($productBrands as $k => $v) {
                    if($v) {
                        $connection->query('INSERT INTO ' . $table_name . ' VALUES ( ' . $v . ', ' . (int)$productId . ',0)');
                    }
                }
                $this->_coreRegistry->register('fired_save_action', true);
            }
        }
    }
}
