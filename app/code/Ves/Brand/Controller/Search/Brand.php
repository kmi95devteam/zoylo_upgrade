<?php

namespace Ves\Brand\Controller\Search;


use Magento\Customer\Controller\AccountInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Brand extends \Magento\Framework\App\Action\Action
{
    	/**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $_response;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;

    /**
     * @var \Ves\Brand\Helper\Data
     */
    protected $_brandHelper;

    /**
     * @var \Magento\Framework\Controller\Result\ForwardFactory
     */
    protected $resultForwardFactory;
      protected $_brandCollection;


    /**
     * @param \Ves\Brand\Controller\Search\Context
     * @param \Magento\Store\Model\StoreManager
     * @param \Magento\Framework\View\Result\PageFactory
     * @param \Ves\Brand\Helper\Data
     * @param \Magento\Framework\Controller\Result\ForwardFactory
     * @param \Magento\Framework\Registry
     */
    public function __construct(
        Context $context,
        \Magento\Store\Model\StoreManager $storeManager,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Ves\Brand\Helper\Data $brandHelper,
        \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\Registry $registry,
         \Ves\Brand\Model\Brand $brandCollection
        ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_brandHelper = $brandHelper;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->_coreRegistry = $registry;
        $this->_brandCollection = $brandCollection;
        parent::__construct($context);
        $this->_request = $context->getRequest();
    }
    
    public function execute()
    {
            $responce = array();
            
           $search_text = $this->getRequest()->getParam('search_text');
           
           if($search_text != ""){
       // $search_text = 'Ci';

        $collection = $this->_brandCollection->getCollection();
        $collection->addFieldToFilter('name',array("like" => '%'.$search_text.'%'));
        foreach ($collection as $data){
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $baseurl = $objectManager->get('Magento\Store\Model\StoreManagerInterface')
            ->getStore()
            ->getBaseUrl();

    $url_key = $baseurl.'brand/' . $data['url_key'] . '.html';
    
    
            $responce[]=  '<ul class="brands-search-results"><li><a href="'.$url_key.'">'.$data['name'].'</a></li><ul>';
        }

        echo json_encode($responce);
           }else{
               $responce="";
         echo json_encode($responce);
               
           }
    }
}