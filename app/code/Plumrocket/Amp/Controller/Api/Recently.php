<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket Amp v2.x.x
 * @copyright   Copyright (c) 2018 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

namespace Plumrocket\Amp\Controller\Api;

use Magento\Framework\Controller\ResultFactory;

class Recently extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Plumrocket\Amp\Block\Catalog\Product\Widget\RecentlyViewed|null
     */
    private $recentlyViewed = null;

    /**
     * @var null|\Plumrocket\Amp\Helper\Data
     */
    private $dataHelper = null;

    /**
     * Recently constructor.
     *
     * @param \Magento\Framework\App\Action\Context                $context
     * @param \Plumrocket\Amp\Block\Catalog\Product\Widget\RecentlyViewed $recentlyViewed
     * @param \Plumrocket\Amp\Helper\Data                          $dataHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Plumrocket\Amp\Block\Catalog\Product\Widget\RecentlyViewed $recentlyViewed,
        \Plumrocket\Amp\Helper\Data $dataHelper
    ) {
        $this->recentlyViewed = $recentlyViewed;
        $this->dataHelper = $dataHelper;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $recentlyViewed = $this->recentlyViewed
            ->initPageSize()
            ->getList();

        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        if ($recentlyViewed) {
            $result->setData(['items' => $recentlyViewed]);
        } else {
            $result->setData([]);
        }

        $this->dataHelper->sanitizeHttpHeaders();
        return $result;
    }
}
