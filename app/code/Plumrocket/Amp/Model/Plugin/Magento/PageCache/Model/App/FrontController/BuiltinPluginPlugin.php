<?php
/**
 * Plumrocket Inc.
 * NOTICE OF LICENSE
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket Amp
 * @copyright   Copyright (c) 2018 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

namespace Plumrocket\Amp\Model\Plugin\Magento\PageCache\Model\App\FrontController;

class BuiltinPluginPlugin
{
    /**
     * @var \Plumrocket\Amp\Helper\Data
     */
    private $dataHelper;

    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    private $cookieManager;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var \Magento\PageCache\Model\Cache\Type
     */
    private $fullPageCache;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    private $productMetadata;

    /**
     * BuiltinPluginPlugin constructor.
     *
     * Fix for Magento FPC.
     *
     * @param \Plumrocket\Amp\Helper\Data                            $data
     * @param \Magento\Framework\Stdlib\CookieManagerInterface       $cookieManager
     * @param \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory
     */
    public function __construct(
        \Plumrocket\Amp\Helper\Data $data,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\PageCache\Model\Cache\Type $fullPageCache,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata
    ) {
        $this->dataHelper = $data;
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->request = $request;
        $this->fullPageCache = $fullPageCache;
        $this->scopeConfig = $scopeConfig;
        $this->productMetadata = $productMetadata;
    }

    /**
     * @param \Magento\PageCache\Model\App\FrontController\BuiltinPlugin $subject
     * @param                                                            $result
     * @return mixed
     */
    public function afterAroundDispatch(\Magento\PageCache\Model\App\FrontController\BuiltinPlugin $subject, $result)
    {
        if ($this->dataHelper->isAmpRequest()
            || $this->request->getFullActionName() === 'pramp_cart_add'
            || ($this->request->getParam('amp') === '1' && $this->dataHelper->moduleEnabled())
        ) {
            $this->deleteCookie(\Plumrocket\Amp\Block\Page\Html\Messages::MESSAGES_COOKIES_NAME);
            if ($this->isMagentoVersionBelow('2.2.0') && !$this->isRedirectToCart()) {
                $this->clearCacheByProductId($this->request->getParam('added_product'));
            }
        }

        return $result;
    }

    /**
     * @param $coolieName
     * @return $this
     */
    private function deleteCookie($coolieName)
    {
        $publicCookieMetadata = $this->cookieMetadataFactory->createPublicCookieMetadata();
        $publicCookieMetadata->setDuration(0);
        $publicCookieMetadata->setPath('/');
        $publicCookieMetadata->setDomain('');

        $this->cookieManager->deleteCookie(
            $coolieName,
            $publicCookieMetadata
        );

        return $this;
    }

    /**
     * @param int $productId
     * @return $this
     */
    private function clearCacheByProductId($productId)
    {
        if ($productId) {
            $this->fullPageCache->clean(
                \Zend_Cache::CLEANING_MODE_MATCHING_TAG,
                [\Magento\Catalog\Model\Product::CACHE_TAG . '_' . $productId]
            );
        }

        return $this;
    }

    /**
     * @return bool
     */
    private function isRedirectToCart()
    {
        return (bool)$this->scopeConfig->getValue(
            'checkout/cart/redirect_to_cart',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param string $version
     * @return bool
     */
    private function isMagentoVersionBelow($version)
    {
        return -1 === version_compare($this->productMetadata->getVersion(), $version);
    }
}
