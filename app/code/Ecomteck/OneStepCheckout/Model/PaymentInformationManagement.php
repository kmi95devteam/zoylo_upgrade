<?php

namespace Ecomteck\OneStepCheckout\Model;

use Magento\Framework\Exception\CouldNotSaveException;

class PaymentInformationManagement extends \Magento\Checkout\Model\PaymentInformationManagement {

    /**
     * @var \Magento\Quote\Api\BillingAddressManagementInterface
     * @deprecated 100.2.0 This call was substituted to eliminate extra quote::save call
     */
    protected $billingAddressManagement;

    /**
     * @var \Magento\Quote\Api\PaymentMethodManagementInterface
     */
    protected $paymentMethodManagement;

    /**
     * @var \Magento\Quote\Api\CartManagementInterface
     */
    protected $cartManagement;

    /**
     * @var PaymentDetailsFactory
     */
    protected $paymentDetailsFactory;

    /**
     * @var \Magento\Quote\Api\CartTotalRepositoryInterface
     */
    protected $cartTotalsRepository;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    protected $_objectManager;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;
    protected $_checkoutSession;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * scopeConfig for system Congiguration
     *
     * @var string
     */
    protected $_scopeConfig;

    /**
     * @param \Magento\Quote\Api\BillingAddressManagementInterface $billingAddressManagement
     * @param \Magento\Quote\Api\PaymentMethodManagementInterface $paymentMethodManagement
     * @param \Magento\Quote\Api\CartManagementInterface $cartManagement
     * @param PaymentDetailsFactory $paymentDetailsFactory
     * @param \Magento\Quote\Api\CartTotalRepositoryInterface $cartTotalsRepository
     * @codeCoverageIgnore
     */
    public function __construct(
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Quote\Api\BillingAddressManagementInterface $billingAddressManagement, \Magento\Quote\Api\PaymentMethodManagementInterface $paymentMethodManagement, \Magento\Quote\Api\CartManagementInterface $cartManagement, \Magento\Checkout\Model\PaymentDetailsFactory $paymentDetailsFactory, \Magento\Quote\Api\CartTotalRepositoryInterface $cartTotalsRepository, \Magento\Framework\ObjectManagerInterface $objectManager, \Magento\Framework\Message\ManagerInterface $messageManager, \Magento\Checkout\Model\Session $_checkoutSession
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->billingAddressManagement = $billingAddressManagement;
        $this->paymentMethodManagement = $paymentMethodManagement;
        $this->cartManagement = $cartManagement;
        $this->paymentDetailsFactory = $paymentDetailsFactory;
        $this->cartTotalsRepository = $cartTotalsRepository;
        $this->_objectManager = $objectManager;
        $this->_messageManager = $messageManager;
        $this->_checkoutSession = $_checkoutSession;
    }

    /**
     * {@inheritDoc}
     */
    public function savePaymentInformationAndPlaceOrder(
    $cartId, \Magento\Quote\Api\Data\PaymentInterface $paymentMethod, \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        $quote = $this->_checkoutSession->getQuote();
        $method = $paymentMethod->getMethod();
        $Couponcode = $quote->getCouponCode();
        $code = strtolower($Couponcode);
        $this->getLogger()->critical($code);
        $payLaterOrders = $this->_scopeConfig->getValue('affliate_coupons/coupons_codes/codes', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $payLaterOrders = strtolower($payLaterOrders);
        $methods = explode(",", $payLaterOrders);
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(print_r($method . '----' . $code, true));

        if ($method == 'cashondelivery' && in_array($code, $methods)) {
            $this->_messageManager->addError("$code is not applicable for Cash on Delivery");
            throw new CouldNotSaveException(__('Error'));
            return false;
        } else {
            $this->savePaymentInformation($cartId, $paymentMethod, $billingAddress);
            try {
                $orderId = $this->cartManagement->placeOrder($cartId);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                throw new CouldNotSaveException(
                __($e->getMessage()), $e
                );
            } catch (\Exception $e) {
                $this->getLogger()->critical($e);
                throw new CouldNotSaveException(
                __('An error occurred on the server. Please try to place the order again.'), $e
                );
            }
            return $orderId;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function savePaymentInformation(
    $cartId, \Magento\Quote\Api\Data\PaymentInterface $paymentMethod, \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        if ($billingAddress) {
            /** @var \Magento\Quote\Api\CartRepositoryInterface $quoteRepository */
            $quoteRepository = $this->getCartRepository();
            /** @var \Magento\Quote\Model\Quote $quote */
            $quote = $quoteRepository->getActive($cartId);
            $items = $quote->getAllItems();
            $quote->removeAddress($quote->getBillingAddress()->getId());
            $quote->setBillingAddress($billingAddress);
            $quote->setDataChanges(true);
            $shippingAddress = $quote->getShippingAddress();
            $zipCode = $shippingAddress->getPostcode();
            $storagetypeCheck = 0;
            $objectmanager = \Magento\Framework\App\ObjectManager::getInstance();
            foreach ($items as $item) {
                $quoteId = $item->getQuoteId();
                $sku = $item->getSku();
                $productRepository = $objectmanager->create('Magento\Catalog\Api\ProductRepositoryInterface')->get($sku);
                $storagetype = $productRepository->getResource()->getAttribute('storage_type')->getFrontend()->getValue($productRepository);
                if ($storagetype == 'Cold Storage') {
                    $storagetypeCheck = 1;
                    $name = $productRepository->getName();
                }
            }
            if ($storagetypeCheck == 1) {
                $serviceblemodel = $objectmanager->get('I95Dev\Serviceablepincodes\Model\DataPincodes')->getCollection()->addFieldToFilter('pincode', array('eq' => $zipCode));
                if ($serviceblemodel->count() <= 0) {
                    $message = 'Your Pincode is not serviceble for the item ' . $name;
                    $this->_messageManager->addError($message);
                    return false;
                }
            }

            if ($shippingAddress && $shippingAddress->getShippingMethod()) {
                $shippingDataArray = explode('_', $shippingAddress->getShippingMethod());
                $shippingCarrier = array_shift($shippingDataArray);
                $shippingAddress->setLimitCarrier($shippingCarrier);
            }
        }
        $this->paymentMethodManagement->set($cartId, $paymentMethod);
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function getPaymentInformation($cartId) {
        /** @var \Magento\Checkout\Api\Data\PaymentDetailsInterface $paymentDetails */
        $paymentDetails = $this->paymentDetailsFactory->create();
        $paymentDetails->setPaymentMethods($this->paymentMethodManagement->getList($cartId));
        $paymentDetails->setTotals($this->cartTotalsRepository->get($cartId));
        return $paymentDetails;
    }

    /**
     * Get logger instance
     *
     * @return \Psr\Log\LoggerInterface
     * @deprecated 100.2.0
     */
    private function getLogger() {
        if (!$this->logger) {
            $this->logger = \Magento\Framework\App\ObjectManager::getInstance()->get(\Psr\Log\LoggerInterface::class);
        }
        return $this->logger;
    }

    /**
     * Get Cart repository
     *
     * @return \Magento\Quote\Api\CartRepositoryInterface
     * @deprecated 100.2.0
     */
    private function getCartRepository() {
        if (!$this->cartRepository) {
            $this->cartRepository = \Magento\Framework\App\ObjectManager::getInstance()
                    ->get(\Magento\Quote\Api\CartRepositoryInterface::class);
        }
        return $this->cartRepository;
    }

}
