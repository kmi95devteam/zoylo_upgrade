define([
    'jquery',
    'Magento_Checkout/js/action/get-totals',
    'Magento_Customer/js/customer-data',
  'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-rate-processor/new-address',
    'Magento_Checkout/js/model/shipping-rate-processor/customer-address',
    'Magento_Checkout/js/model/shipping-rate-registry',
    'Magento_Checkout/js/model/totals',
    'Magento_Checkout/js/action/get-payment-information',
    'Magento_Checkout/js/model/full-screen-loader',
    'ko',
    'Magento_Checkout/js/model/url-builder',
    'mage/url'
  
  
], function ($, getTotalsAction, customerData, quote, defaultProcessor, customerAddressProcessor, rateRegistry, totals, getPaymentInformationAction, fullScreenLoader, ko, urlBuilder, urlFormatter) {

    $(document).ready(function(){
        $(document).on('click', '#zoylo-new-customer-new-address', function(){
          var zoyloAjaxUrl = urlFormatter.build('editshipaddress/index/customshipaddressupdate');
          var valdiationFlag = false;
          var shippingAddressForm = $('#co-shipping-form').get(0);
          if(shippingAddressForm){
            var shipping = ko.dataFor(shippingAddressForm);
            var shippingValidateResult = shipping.validateShippingInformation();
            var validateResult = shippingValidateResult;
            if(validateResult){
               var valdiationFlag = true;
            }else{
              console.log('New customer new shipping address fields are mandatory!!');
              return false;
            }
          }
            
          if(valdiationFlag==true){
            var urlbui = zoyloAjaxUrl;
            var form = $('form#form-validate');
            var quote_id = window.checkoutConfig.quoteData.entity_id;
            var ship_address_id = "New";
            $.ajax({
                url: urlbui,
                type: "POST",
                dataType: 'json',
                showLoader: 'true',
                data: {"address_type": "newcustomer_newaddress","ship_address_id": ship_address_id,"quote_id": quote_id,"formdata":$('#co-shipping-form').serialize()},
                showLoader: true,
                success: function (result) {
                  if(result.success=='true' && result.pincode_valid!=''){
                    $( "#maincontent .columns .main" ).append( result.ship_address_new_add );
                    deferred = $.Deferred();
                    totals.isLoading(true);
                    getPaymentInformationAction(deferred);
                    $.when(deferred).done(function () {
                        fullScreenLoader.stopLoader();
                        totals.isLoading(false);
                    });
                     setTimeout(function(){ 
                        window.location.reload();
                      }, 1000);
                  }else{
                    if(result.success=='false'){
                      $( "#maincontent .columns .main" ).append( result.ship_address_new_addfail );
                      return false;
                    }
                  }
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
          }
          jQuery(window).scrollTop(0);
        });
    });
});