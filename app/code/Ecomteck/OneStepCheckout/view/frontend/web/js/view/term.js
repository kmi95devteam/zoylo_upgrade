/**
 * Copyright © 2017 Ecomteck. All rights reserved.
 * See LICENSE.txt for license details.
 */
define(
    [
        'jquery',
        'uiComponent',
        'Magento_Ui/js/modal/modal',
        'mage/translate'
    ],
    function ($,Component,modal,$t) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Ecomteck_OneStepCheckout/term'
            },

            isEnabled: function () {
                return window.checkoutConfig.onestepcheckout.termEnabled;
            },

            isCheckedByDefault: function () {
                return window.checkoutConfig.onestepcheckout.termChecked;
            },

            getCheckoutLabel: function () {
                return window.checkoutConfig.onestepcheckout.termLabel;
            },
            showPopup: function(){
                if(!window.checkoutConfig.onestepcheckout.termPopupContent){
                    return;
                }
                var element = $('<div></div>').html(window.checkoutConfig.onestepcheckout.termPopupContent)
                var options = {
                    type: 'popup',
                    responsive: true,
                    innerScroll: true,
                    buttons: []
                };
    
                var popup = modal(options, element);
                element.modal('openModal');
            }
        });
    }
);